select date, category, measure_name, measure_group, measure_category, measure
from 
(
select 
report_date as date, 
category, 
measure_name, 
measure_group, 
'Actual' as measure_category,
measure
from bi_overall_measure
where measure_name not in ('SNS ADH', 'SNS COM')
and weekday(report_date) = 3

union all 

select sprintdate as date, 
category, 
measurename as measure_name, 
measuregroup as measure_group, 
'Target' as measure_category, 
sprinttarget as measure
from bi_overall_sprint_target where (sprinttarget <> '' and sprinttarget is not null)
) sor
order by date