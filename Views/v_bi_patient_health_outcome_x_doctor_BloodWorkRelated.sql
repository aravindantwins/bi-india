alter view v_bi_patient_health_outcome_x_doctor_BloodWorkRelated as 
select distinct s1.clientid, doctorId, s1.doctorname, s1.treatmentdays,
s1.start_CHOL_medicine_drugs, s1.current_CHOL_medicine_drugs, s1.start_tg_hdl, s1.latest_tg_hdl, s1.start_alt, s1.latest_alt, 
s1.start_homa2B, s1.latest_homa2B,
s1.start_hsCRP, s1.latest_hsCRP
from 
(
	select a.clientid, a.doctorname, b.doctorId, treatmentdays, start_CHOL_medicine_drugs, 
    current_CHOL_medicine_drugs,
	max(cast((case when a.firstBloodWorkDateITZ = itz(c.bloodworktime) then c.triglycerides/c.hdlCholesterol else null end) as decimal(10,2))) as start_tg_hdl, 
    max(cast((case when a.latestBloodWorkDateITZ = itz(c.bloodworktime) then c.triglycerides/c.hdlCholesterol else null end) as decimal(10,2))) as latest_tg_hdl,
	max(cast((case when a.firstBloodWorkDateITZ = itz(c.bloodworktime) then c.alt else null end) as decimal(10,2))) as start_alt, 
    max(cast((case when a.latestBloodWorkDateITZ = itz(c.bloodworktime) then c.alt else null end) as decimal(10,2))) as latest_alt,
	max(cast((case when a.firstBloodWorkDateITZ = itz(c.bloodworktime) then c.homa2B else null end) as decimal(10,2))) as start_homa2B, 
    max(cast((case when a.latestBloodWorkDateITZ = itz(c.bloodworktime) then c.homa2B else null end) as decimal(10,2))) as latest_homa2B,
    max(cast((case when a.firstBloodWorkDateITZ = itz(c.bloodworktime) then c.hsCReactive else null end) as decimal(10,2))) as start_hsCRP, 
    max(cast((case when a.latestBloodWorkDateITZ = itz(c.bloodworktime) then c.hsCReactive else null end) as decimal(10,2))) as latest_hsCRP

	from dim_client a left join v_allClient_list b on a.clientid = b.clientid 
					  left join clinictestresults c on a.clientid = c.clientid and (a.firstBloodWorkDateITZ = itz(c.bloodworktime) or a.latestBloodWorkDateITZ = itz(c.bloodworktime))
	where is_row_current = 'y' and a.status = 'active' 
    and a.isRCT is null
    group by a.clientid
) s1
; 

-- alt, triglycerides, hdlCholesterol, homa2B, hsCReactive


