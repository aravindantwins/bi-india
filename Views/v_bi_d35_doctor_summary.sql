create view v_bi_d35_doctor_summary
as
select s1.date, s1.doctorname, s1.d35_success_count, s2.d35_completed_count
from 
(
    select a.date, b.doctorname, count(case when success_ind_new = 'Yes' then b.clientid else null end) as d35_success_count
	from v_date_to_date a
	left join v_bi_d35_patient_details b on b.day35 <= a.date
	where a.date >= '2019-06-19'
    and b.doctorname is not null
    group by date, b.doctorname
) s1 left join 
(
	select date, doctorname, count(clientid) as d35_completed_count 
	from v_date_to_date a 
	left join 
		(
				select distinct b.clientid, c.doctornameshort as doctorname,
				date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) as day35
                from
				(
						select clientid, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
						max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
						from bi_patient_status b
						group by clientid, status
				) b left join v_Client_tmp c on b.clientid = c.clientid
				where b.status = 'active' 
				and datediff(status_end_date, if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ))) >= 35
				and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) >= '2019-06-19'
				and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) <= date(itz(now()))
		) c on c.day35 <= a.date
	where a.date >= '2019-06-19'
    and doctorname is not null
	group by date, doctorname
) s2 on s1.date = s2.date and s1.doctorname = s2.doctorname 
;
