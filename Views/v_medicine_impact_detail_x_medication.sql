create view v_medicine_impact_detail_x_medication as
select 
a.clientid, 
v.patientname, 
v.doctorname,
a.medication, 
ifnull(if(mh.clientId is null and mh.measure = 'Automated', 'Accepted', if(mh.recommendmedication is null or mh.recommendmedication = '' , mh.measure,mh.recommendmedication)), 'No Data') as plt_recm,
a.medication_startdate, 
a.medication_enddate, 
cgm5dg_before_med, 
ketone5dk_before_med, 
spike5d_before_med, 
netCarb5d_before_med, 
cgm5dg_lastday, 
ketone5dk_lastday, 
spike5d_lastday, 
netCarb5d_lastday
from bi_medicine_impact_detail_x_medication a
inner join v_active_clients v on a.clientid = v.clientid
left join bi_medicationrecommend_history mh on a.clientid = mh.clientid and a.medication_startdate between mh.measure_startdate and mh.measure_enddate
where a.medication_startdate <= date_sub(date(itz(now())), interval 1 day) 
order by clientid, medication_startdate
;
