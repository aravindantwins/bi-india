create view v_bi_twin_company_growth_x_daily as 
	select a.date, a.doctorname as doctor, a.active_cnt, b.enrolled, b1.enrolled_14day, c.counselled, d.total_doctors_launched -- , s.counselled as cumulative_counselled, s2.counselled, (ifnull(a.active_cnt,0) + ifnull(a.pending_active_cnt,0))*p.annual_fee as estimated_earnings, p.annual_fee
	from v_bi_patient_count_x_status a left join
	(
				select doctorname, s1.date, count(distinct clientid) as enrolled
				from
				v_date_to_date s1 left join
				(
					select distinct 'enrolled' as category, doctor_name as doctorname, date(enrollmentdateitz) as date, clientid from bi_patient_status where status in ('active')
                    
                    union all 
                    
                    select distinct 'enrolled' as category, doctor_name as doctor, date(enrollmentdateitz) as date, clientid from bi_patient_status where status in ('pending_active') 
                    and clientid not in (select distinct clientid from bi_patient_status where status = 'active')
				) s2 on s2.date = s1.date
				where doctorname is not null
                and s1.date >= date_sub(date(itz(now())), interval 90 day)
                group by doctorname, s1.date
	) b on a.doctorname = b.doctorname and a.date = b.date
	left join
	(
				select doctorname, s1.date, count(distinct clientid) as enrolled_14day
				from
				v_date_to_date s1 left join
				(
					select distinct 'enrolled_14day' as category, doctor_name as doctorname, date(enrollmentdateitz) as date, clientid from bi_patient_status where status in ('active')
                    
                    union all 
                    
                    select distinct 'enrolled_14day' as category, doctor_name as doctor, date(enrollmentdateitz) as date, clientid from bi_patient_status where status in ('pending_active') 
                    and clientid not in (select distinct clientid from bi_patient_status where status = 'active')
				) s2 on s2.date >= date_sub(s1.date, interval 13 day) and s2.date <= s1.date 
				where doctorname is not null
                and s1.date >= date_sub(date(itz(now())), interval 90 day)
                group by doctorname, s1.date
	) b1 on a.doctorname = b1.doctorname and a.date = b1.date
    left join 
    (
				select doctorname, s1.date, sum(total_couselled) as counselled
				from
				v_date_to_date s1 left join
				(
								select distinct a.clientid, c.doctorname, a.recentdate as date, count(distinct a.clientid) as total_couselled 
								from 
								(
									select clientid, max(date(eventdate)) as recentdate from bi_doctor_counsel_data group by clientid
								) a inner join bi_doctor_counsel_data b on a.clientid = b.clientid and a.recentdate = date(b.eventdate)
									inner join v_allClient_list c on a.clientid = c.clientid 
								group by a.clientid, c.doctorname, a.recentdate				
				) s2 on s2.date = s1.date 
                where doctorname is not null
                and s1.date >= date_sub(date(itz(now())), interval 90 day)
				group by doctorname, s1.date
    ) c on a.doctorname = c.doctorname and a.date = c.date
    left join 
    (
				select s1.date, doctorname, count(distinct s2.doctorname) as total_doctors_launched
				from v_date_to_date s1
				left join 
                
                (select distinct doctor_name as doctorname, min(status_start_date) as first_service_activation from bi_patient_status where status = 'active' group by doctor_name) s2 
                -- (select date(itz(dateadded)) as date, id, UPPER(CONCAT(firstName, ' ', lastName)) AS doctorName from doctors where test = FALSE) s2 
                on s2.first_service_activation <= s1.date 
				where doctorname is not null
				and s1.date >= date_sub(date(itz(now())), interval 90 day)
				group by s1.date, doctorname
    ) d on a.doctorname = d.doctorname and a.date = d.date
where a.date >= date_sub(date(itz(now())), interval 90 day)
-- and weekday(a.date) = 2
and a.date <= date_sub(date(itz(now())), interval 1 day)