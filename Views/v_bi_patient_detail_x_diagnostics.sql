SET collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_detail_x_diagnostics
as
		select d.date, c.clientId, c.status_start_date as pending_active_start_date, ct.patientname, ct.gender, ct.age, ct.email, ct.mobilenumber, ct.address, ct.postalCode, ct.city, date_add(c.status_start_date, interval 1 day) as appointment_date, '7.30 - 8 am' as appointment_time, 
        ct.plancode, ct.labels, ct.coachnameshort AS coach, if(toDo.inclusion = 'FINISHED' AND dc.start_bmi IS NOT null, 'Yes' , 'No') AS eligible_for_blooddraw
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date = c.status_start_date and c.status = 'pending_active'
		left join  v_client_tmp ct on c.clientid = ct.clientid    
		LEFT JOIN (	
						select clientid, 
						max(case when toDotype = 'INCLUSION_CRITERIA_DETAILS' then status else null end) as inclusion, 
						max(case when toDotype = 'VITALS_CONDITIONS' then status else null end) as vitals
						from opstodoitems o
						group by clientid
				  ) toDo ON c.clientid = toDo.clientid
        LEFT JOIN dim_client dc ON c.clientid = dc.clientid AND dc.is_row_current = 'Y'
		where d.date >= date_sub(date(itz(now())), interval 1 month) 
        and ct.status not in ('registration')
        ;
        
        
