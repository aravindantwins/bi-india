alter  view v_allClients_RCT as 
select c.id as clientid, 
        UPPER(CONCAT(`cp`.`firstName`, ' ', `cp`.`lastName`)) AS `patientName`,
        UPPER(CONCAT(`cp`.`firstName`,
                        ' ',
                        LEFT(`cp`.`lastName`, 1))) AS `patientNameShort`,
        `cp`.`dateOfBirth`,                
        ((YEAR(CURDATE()) - YEAR(`cp`.`dateOfBirth`)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(`cp`.`dateOfBirth`, '%m%d'))) AS `age`,
        d.id as doctorId, 
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`, ' ', `d`.`lastName`)))) AS `doctorName`,
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`,
                        ' ',
                        LEFT(`d`.`lastName`, 1))))) AS `doctorNameShort`,
		d.clinicName,
        UPPER(CONCAT(`h`.`firstName`, ' ', `h`.`lastName`)) AS `coachName`,
        UPPER(CONCAT(`h`.`firstName`,
                        ' ',
		LEFT(`h`.`lastName`, 1))) AS `coachNameShort`,
        TO_DAYS(ITZ(NOW())) - TO_DAYS(`c`.`enrollmentDate`) as daysEnrolled,
		c.labels, 
        c.status, 
        c.gender,
        date(itz(c.enrollmentdate)) as enrollmentdate,
        c.sourceType,
        dc.isRCT_CONTROL, 
        dc.isRCT_PRIME,
        dc.durationYears_diabetes
from clients c 
			JOIN `twinspii`.`clientspii` `cp` ON ((`c`.`ID` = `cp`.`ID`))
			JOIN `twins`.`doctors` `d` ON ((`c`.`doctorId` = `d`.`ID`))
			LEFT JOIN `twins`.`coaches` `h` ON ((`c`.`coachId` = `h`.`ID`))
            left join dim_client dc on c.id = dc.clientid 
WHERE (
			`c`.`deleted` = 0
			and dc.is_row_current = 'y'
            and dc.isRCT = 'Yes'
	   );
       
       
