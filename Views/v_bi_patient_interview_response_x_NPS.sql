create view v_bi_patient_interview_response_x_NPS
as 
select s.patientID, date(s1.timestamp) as report_date, EnrolledDay, if(s1.NPS='',null, cast(s1.NPS as unsigned)) as NPS,
case when if(s1.NPS='',null, cast(s1.NPS as unsigned)) between 1 and 6 then 'D'
	 when if(s1.NPS='',null, cast(s1.NPS as unsigned)) between 9 and 10 then 'P'
     when if(s1.NPS='',null, cast(s1.NPS as unsigned)) is null then NULL
     else 'PA' end as NPS_Scale
from 
(
select patientid, max(timestamp) as recentinterview from bi_patient_interview_response group by patientid
) s inner join bi_patient_interview_response s1 on s.patientid = s1.patientid and s.recentinterview = s1.timestamp
;

