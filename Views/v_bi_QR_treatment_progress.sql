alter view v_bi_QR_treatment_progress as 
select a.clientid, ac.patientname, ac.coachNameShort as coach, a.date, cgm_5d, as_5d, macro, micro, biota
from 
(
	select clientid, date 
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_Date and b.status = 'active'
	where date >= date_sub(date(itz(now())), interval 30 day)
) a left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.date = b.date 
	left join nutritionscores c on a.clientid = c.clientid and a.date = c.date
    inner join v_active_clients ac on a.clientid = ac.clientid
where b.treatmentdays > 1
;