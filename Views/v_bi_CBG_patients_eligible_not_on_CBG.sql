-- show variables
alter view v_bi_CBG_patients_eligible_not_on_CBG as 
select clientid, patientname, coach, email, mobilenumber, city, treatmentdays, leadCoach, plancode, TTP_Status, isCBGScheduled_upcoming, CBGScheduled_startDate, isEnteringFirstTime, isAppSuspended
from (	
			select a.clientid, b.patientname, b.coachnameShort as coach, leadCoachNameShort as leadCoach, b.plancode, 
            -- if (d.isM1 = 'Yes','M1','M2') as TTP_Status,
            d.isM1 as TTP_Status,
            email, mobilenumber, city, s.date, s.treatmentdays, as_1d, as_5d, isCBGEligible, isCBG, is_MetOnly_by_OPS, is_MetOnly, 
            if(b.labels like '%prime%' or b.labels like '%control%', 'Yes', 'No') as isRCT,
            -- if(fpc.clientid is not null, 'Yes', null) as isCBGScheduled_upcoming,
            -- if(e.clientid is not null, 'Yes' , null) as isEnteringFirstTime,
            -- if(f.clientid is not null, 'Yes', null) as isAppSuspended,
			fpc.clientid as isCBGScheduled_upcoming,
            fpc.startdate as CBGScheduled_startDate,
            e.clientid as isEnteringFirstTime ,
            f.clientid as isAppSuspended
			from 
			(
					select a.date, b.clientId, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
					from twins.v_date_to_date a
					inner join
					(
						select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
						from
						(
							select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
							from bi_patient_status
							where status = 'active'
							group by clientid, date(visitdateitz)
						) s
					) b on a.date between b.startDate and b.endDate
					where a.date =  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now()))) 
            ) s left join bi_patient_reversal_state_gmi_x_date a on s.clientid = a.clientid and s.date = a.date 
				left join v_Client_tmp b on s.clientid = b.clientid 
                left join (select distinct clientid, startdate from predictedcgmcycles where startdate > date(itz(now()))) fpc on a.clientid = fpc.clientid 
                left join dim_client d ON a.clientid = d.clientid and d.is_row_current = 'Y'
                left join (
								select clientid, min(date) as firstDayEntered
								from bi_patient_reversal_state_gmi_x_date where isCBGEligible = 'Y' and isCBG = 'N'
								group by clientid 
							) e on a.clientid = e.clientid and a.date = e.firstDayEntered
				left join bi_patient_suspension_tracker f on a.clientid = f.clientid and (a.date between f.startdate and f.enddate) and f.status = 1
			where s.treatmentdays >= 35 and isCBGEligible = 'Y' and isCBG = 'N'
	  ) s
;


