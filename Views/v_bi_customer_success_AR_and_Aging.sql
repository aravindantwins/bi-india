set collation_connection = 'latin1_swedish_ci';

alter view v_bi_customer_success_AR_and_Aging as 
select 	
		'Overdue' as Category,
        a.ClientId,
        a.patientNameShort as Patient_Name,
        a.city,
        a.gender,
        a.age,
        c.durationYears_diabetes as Years_Diabetic,
        c.start_symptoms as Pre_Twin_Symptoms, 
       case when c.start_symptoms not like 'None' 
			then
				((LENGTH(c.start_symptoms) - LENGTH(REPLACE(c.start_symptoms,
                    ',',
                    ''))) + 1)
			else 0 end as Pre_Twin_Symptoms_Count,
       -- c.start_medicine_diabetes AS startmedicine,
        ((LENGTH(c.start_medicine_diabetes_drugs) - LENGTH(REPLACE(c.start_medicine_diabetes_drugs,
                    ',',
                    ''))) + 1) AS Pre_Twin_Med,  -- startMedicineCount,
		c.first_available_cgm_5d as Pre_Twin_5dg, 
        a.coachNameShort as Coach,
        a.leadCoachNameShort as Lead_Coach,
        a.doctorNameShort as Doctor,

        c.treatmentDays,
        b.Is_Achieved_Reversal,
        b.Is_Current_Reversal, 
        
		b.Latest_Invoice_Date,
        b.Next_Billing_Date,
        a.TermEndDate,
		a.PlanCode,
       
        b.InvoiceStatus,
        b.Aging_Days,
        d.IMT_Report_Date,
        d.subtype_L1 as IMT_Dis_Subtype_L1,
		case when d.clientid is not null then 'Yes' else 'No' end as Is_IMT_Dis_Open,
        case when b.suspended=1 then 'Yes' else 'No' end as Is_Suspended,

        case when c.isM1 ='Yes' then 'M1'
			 when c.isM2Converted='Yes' then 'M1-M2'
             else 'M2' end  AS EnrollType,  --  TTP_or_Regular_Tag,
             
	   case when a.labels like '%TEMP_DISCHARGE%' then 'Yes' else 'No' end as Is_Temp_Discharge
  
	from
	(
			select a.id as clientid ,
            b.invoice_date_overdue_min  as latest_invoice_date,
            b.next_billing_date,a.invoicestatus,a.suspended,a.Is_Achieved_Reversal,a.Is_Current_Reversal,
            DATEDIFF(date(ITZ(now())), b.invoice_date_overdue_min) as Aging_Days
			from 
		       (  
 				   select a.clientid,a.invoice_date_overdue_min,a.invoice_date_paid_max, b.latest_next_billing_date as next_billing_date
                   from
					(
						select a.customer_id,a.clientid,a.invoice_date_overdue_min,b.invoice_date_paid_max
						from 
						(
							select  a.customer_id,a.cf_clientid_unformatted as clientid, min(date(invoice_date)) as invoice_date_overdue_min from importedbilling.zoho_customers a  join importedbilling.zoho_invoices b on a.customer_id=b.customer_id
							where   b.status='overdue'  
							AND b.cf_service_purchased_unformatted IS null
							group by a.customer_id
						) a 
						left join 
						(	
							select  a.customer_id,a.cf_clientid_unformatted as clientid, max(date(invoice_date)) as invoice_date_paid_max from importedbilling.zoho_customers a join importedbilling.zoho_invoices b on a.customer_id=b.customer_id
						 	where    b.status='paid' 
						 	AND  b.cf_service_purchased_unformatted IS null
						 	group by a.customer_id
						) b on a.clientid=b.clientid
						where (a.invoice_date_overdue_min > ifnull(b.invoice_date_paid_max, '1900-01-01'))
                     ) a  inner join clients c on a.clientid = c.id and c.status in ('active')
                     	  left join (select customer_id, max(date(if(next_billing_at='', null, next_billing_at))) as latest_next_billing_date from importedbilling.zoho_subscriptions group by customer_id) b on a.customer_id = b.customer_id 
                ) b 
                join 
                (
					select a.id, a.invoicestatus, a.suspended,
						 case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
							  else 'No' end as Is_Achieved_Reversal,
						 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
							  else 'No' end as Is_Current_Reversal
					from clientauxfields a inner join clients b on a.id=b.id 
                    where b.status='active'
               ) a on a.id=b.clientid
	
            /* OLD clientauxFields method to identify overdue   
			select a.id as clientid ,a.latest_invoice_date,a.next_billing_date,a.invoicestatus,a.Aging_Days,a.suspended,b.TreatmentDays,a.Is_Achieved_Reversal,a.Is_Current_Reversal
			from 
			(
				select a.id, cast(a.invoiceDate as date) as latest_invoice_date
				,a.nextBillingAt as next_billing_date, a.invoicestatus, 
                 DATEDIFF(date(ITZ(now())), a.invoiceDate) as Aging_Days,a.suspended,
                 case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
					  else 'No' end as Is_Achieved_Reversal,
                 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
                      else 'No' end as Is_Current_Reversal
				from clientauxfields a inner join clients b on a.id=b.id 
                where a.invoicestatus='Overdue'
                and b.status = 'active'
                ) a
				left join bi_patient_reversal_state_gmi_x_date b on a.id=b.clientid and a.latest_invoice_date=b.date
			*/
	) b join v_client_Tmp a on b.clientid = a.clientid  
	left join dim_client c on (a.clientid=c.clientid and c.is_row_current='Y')
	left join (select distinct clientid, date(itz(report_time)) as IMT_Report_Date, subtype_L1 from bi_incident_mgmt where category = 'disenrollment' and status = 'open') d on (b.clientid=d.clientid)

union all


select 	'Risk' as Category,
		a.ClientId,
        a.patientNameShort ,
        a.city,
        a.gender,
        a.age,
        c.durationYears_diabetes as Years_Diabetic,
        c.start_symptoms, 
        case when c.start_symptoms not like 'None' 
			then
				((LENGTH(c.start_symptoms) - LENGTH(REPLACE(c.start_symptoms,
                    ',',
                    ''))) + 1)
			else 0 end as startSymptomCount,
        -- c.start_medicine_diabetes AS startmedicine,
        ((LENGTH(c.start_medicine_diabetes_drugs) - LENGTH(REPLACE(c.start_medicine_diabetes_drugs,
                    ',',
                    ''))) + 1) AS startMedicineCount,
		c.first_available_cgm_5d , 
        a.coachNameShort,
        a.leadCoachNameShort,
        a.doctorNameShort,
        c.TreatmentDays,
        b.Is_Achieved_Reversal,
        b.Is_Current_Reversal, 
        
       	b.Latest_Invoice_Date,
        b.Next_Billing_Date,
        a.TermEndDate,
		a.PlanCode,
 
        b.InvoiceStatus,
        DATEDIFF(date(ITZ(now())), d.IMT_report_date) as Aging_Days,
        d.IMT_report_date as IMT_Report_Date,
        d.subtype_L1 as IMT_Dis_Subtype_L1,
		case when d.clientid is not null then 'Yes' else 'No' end as Is_IMT_Dis_Open,
        case when b.suspended=1 then 'Yes' else 'No' end as Is_Suspended,

        case when c.isM1 ='Yes' then 'M1'
			 when c.isM2Converted='Yes' then 'M1-M2'
             else 'M2' end  AS EnrollType,  --  TTP_or_Regular_Tag,
	   
	   case when a.labels like '%TEMP_DISCHARGE%' then 'Yes' else 'No' end as Is_Temp_Discharge
	        
from 
	(  	
		select s.clientid,s.IMT_report_date,s.subtype_L1 -- ,b.TreatmentDays 
		from
		(
				select distinct a.clientid,a.Max_IMT_report_date as IMT_report_date,c.subtype_L1
				from 
				(		
		        		select clientid,max(Date(ITZ(report_time))) as Max_IMT_report_date
					 	from bi_incident_mgmt where category ='disenrollment' and status='open' 
					 	group by clientid 
				) a 
		        inner join bi_incident_mgmt c on a.clientid=c.clientid and a.Max_IMT_report_date=date(itz(c.report_time)) AND c.category ='disenrollment'
        ) s         
        -- left join bi_patient_reversal_state_gmi_x_date b on s.clientid=b.clientid and s.IMT_report_date=b.date
   ) d 
 join
 (	
		select a.id, cast(invoiceDate as date) as latest_invoice_date
			,nextBillingAt as next_billing_date, invoicestatus,suspended,
            case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
					  else 'No' end as Is_Achieved_Reversal,
                 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
                      else 'No' end as Is_Current_Reversal
		from clientauxfields a inner join clients b on a.id=b.id 
		where ifnull(a.invoicestatus,'blank') not in ( 'Overdue') 
		and b.status = 'active'
 )  b on d.clientid=b.id
 join v_client_tmp a on d.clientid = a.clientid 
 left join dim_client c on (a.clientid=c.clientid and c.is_row_current='Y')
 
 UNION ALL 
 
 select 'PotentialRisk' as Category,
		a.ClientId,
        a.patientNameShort ,
        a.city,
        a.gender,
        a.age,
        c.durationYears_diabetes as Years_Diabetic,
        c.start_symptoms, 
        case when c.start_symptoms not like 'None' 
			then
				((LENGTH(c.start_symptoms) - LENGTH(REPLACE(c.start_symptoms,
                    ',',
                    ''))) + 1)
			else 0 end as startSymptomCount,
        -- c.start_medicine_diabetes AS startmedicine,
        ((LENGTH(c.start_medicine_diabetes_drugs) - LENGTH(REPLACE(c.start_medicine_diabetes_drugs,
                    ',',
                    ''))) + 1) AS startMedicineCount,
		c.first_available_cgm_5d , 
        a.coachNameShort,
        a.leadCoachNameShort,
        a.doctorNameShort,
        c.TreatmentDays,
        b.Is_Achieved_Reversal,
        b.Is_Current_Reversal, 
        
       	b.Latest_Invoice_Date,
        b.nextBillingDate as next_billing_date,
        a.TermEndDate,
		a.PlanCode,
 
        b.InvoiceStatus,
        null as Aging_Days,
        null as IMT_Report_Date,
        null as IMT_Dis_Subtype_L1,
		cast('No' as char(5)) as Is_IMT_Dis_Open,
        case when b.suspended=1 then 'Yes' else 'No' end as Is_Suspended,

        case when c.isM1 ='Yes' then 'M1'
			 when c.isM2Converted='Yes' then 'M1-M2'
             else 'M2' end  AS EnrollType,  --  TTP_or_Regular_Tag,
	   
	   case when a.labels like '%TEMP_DISCHARGE%' then 'Yes' else 'No' end as Is_Temp_Discharge
	        
from 
(			
 		select s.id, s.latest_invoice_date, 
 		ifnull(if(next_billing_date='',null,next_billing_date), case when subscriptionType = 'Monthly' then date_add(s.latest_invoice_date, interval 1 month) 
 									   when subscriptionType = 'Quarterly' then date_add(s.latest_invoice_date, interval 3 month) 
 									   when subscriptionType = 'Annual' then date_add(s.latest_invoice_date, interval 1 year) 
 									   else date(itz(termenddate)) end) as nextBillingDate,
 		invoiceStatus, suspended, Is_Achieved_Reversal, Is_Current_Reversal
 		from 
 		(
		select a.id, cast(invoiceDate as date) as latest_invoice_date
			,nextBillingAt as next_billing_date, invoicestatus,suspended, b.termenddate,
            case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
					  else 'No' end as Is_Achieved_Reversal,
                 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
                      else 'No' end as Is_Current_Reversal,
            case when a.intervalUnit = 'YEARS' then 'Annual' 
        	 when a.intervalUnit = 'MONTHS' and a.paymentInterval = 3 then 'Quarterly'
        	 when a.intervalUnit = 'MONTHS' and a.paymentInterval <> 3 then 'Monthly'
        	 else a.intervalUnit end as subscriptionType
		from clientauxfields a inner join clients b on a.id=b.id 
		where ifnull(a.invoicestatus,'blank') not in ( 'Overdue') 
		and b.status = 'active'
		) s 
		where s.id not in 
		(select distinct clientid from bi_incident_mgmt where category= 'disenrollment' and status ='open')
 )  b 
 join v_allclient_list a on b.id = a.clientid 
 left join dim_client c on (a.clientid=c.clientid and c.is_row_current='Y')
 where b.nextBillingDate > date(itz(now())) and b.nextBillingDate <= date_add(date(itz(now())), interval 1 month)
 ;
