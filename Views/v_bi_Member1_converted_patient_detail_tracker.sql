alter view v_bi_Member1_converted_patient_detail_tracker as 
select s.clientid, s.patient, s.coach, s.Leadcoach, s.treatmentdays, s.converion_payment_invoice_date, s.conversion_payment_date, s.start_weight, s.start_glucose, s.start_medicine_count, s.start_symptoms_count, s.plancode,

s3.treatmentday as invoice_sent_day, s1.treatmentday as conversion_pay_day, 
s1.weight_1d as payday_weight_1d, s1.cgm_5d as pay_cgm_5d, s1.medCount as pay_medCount, s1.as_5d as pay_AS_5d, s1.nut_adh_syntax_5d as pay_nut_adh_5d, s1.IMT_outstanding as pay_IMT_outstanding, s1.MH_symptoms_cnt as pay_symptoms_count, 
s2.date as d10_treatment_date, s2.weight_1d as d10_weight_1d, s2.cgm_5d as d10_cgm_5d, s2.medCount as d10_medCount, s2.as_5d as d10_AS_5d, s2.nut_adh_syntax_5d as d10_nut_adh_5d, s2.IMT_outstanding as d10_IMT_outstanding, s2.MH_symptoms_cnt as d10_symptoms_count,

case when s1.treatmentdays <= 10 and (s1.cgm_5d < 140 or (start_glucose - s1.cgm_5d >= 50)) and (ifnull(if(s1.med_adh = 'NO',0,s1.medCount),0) = 0 or (ifnull(s1.medCount,0) < ifnull(start_medicine_count,0))) and s1.AS_5d >= 90 and s1.nut_adh_syntax_5d >= 0.8 and ifnull(s1.IMT_outstanding,0) = 0 then 'IMMEDIATE'
	 when s1.treatmentdays <= 10 and (s1.cgm_5d < 140 or (start_glucose - s1.cgm_5d >= 50)) and (ifnull(if(s1.med_adh = 'NO',0,s1.medCount),0) = 0 or (ifnull(s1.medCount,0) < ifnull(start_medicine_count,0))) and s1.AS_5d >= 70 and ifnull(s1.IMT_outstanding,0) = 0 then 'HIGH'
     when s1.treatmentdays <= 10 and (s1.cgm_5d < start_glucose) and (ifnull(if(s1.med_adh = 'NO',0,s1.medCount),0) = 0 or (ifnull(s1.medCount,0) <= ifnull(start_medicine_count,0))) and (s1.AS_5d >= 60) and ifnull(s1.IMT_outstanding,0) >= 0 then 'MED'

	 when s1.treatmentdays > 10 and (s2.cgm_5d < 140 or (start_glucose - s2.cgm_5d >= 50)) and (ifnull(if(s2.med_adh = 'NO',0,s2.medCount),0) = 0 or (ifnull(s2.medCount,0) < ifnull(start_medicine_count,0))) and s2.AS_5d >= 90 and s2.nut_adh_syntax_5d >= 0.8 and ifnull(s2.IMT_outstanding,0) = 0 then 'IMMEDIATE'
     when s1.treatmentdays > 10 and (s2.cgm_5d < 140 or (start_glucose - s2.cgm_5d >= 50)) and (ifnull(if(s2.med_adh = 'NO',0,s2.medCount),0) = 0 or (ifnull(s2.medCount,0) < ifnull(start_medicine_count,0))) and s2.AS_5d >= 70 and ifnull(s2.IMT_outstanding,0) = 0 then 'HIGH'
	 when s1.treatmentdays > 10 and (s2.cgm_5d < start_glucose) and (ifnull(if(s2.med_adh = 'NO',0,s2.medCount),0) = 0 or (ifnull(s2.medCount,0) <= ifnull(start_medicine_count,0))) and (s2.AS_5d >= 60) and ifnull(s2.IMT_outstanding,0) >= 0 then 'MED' -- and ifnull(s2.IMT_outstanding,0) > 0

	 when (ifnull(s1.cgm_5d, s2.cgm_5d) is null or ifnull(s1.nut_adh_syntax_5d, s2.nut_adh_syntax_5d) is null) then 'ND' 
     else 'LOW' 
     END as conversionProbability
from 
(
	select a.clientid, b.patientname as patient, b.coachnameShort as coach, b.leadCoachNameShort as Leadcoach, c.conversion_payment_date, c.converion_payment_invoice_date, a.treatmentdays, 
    start_weight, cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose, start_labA1C, 
    replace(replace(start_medicine_diabetes_drugs,'(DIABETES)',''), '(INSULIN)','') as start_diabetes_medicines_drugs, 
    length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count, 
    start_symptoms, b.plancode, 
    length(start_symptoms) - length(replace(start_symptoms,',','')) + 1 as start_symptoms_count
	from dim_client a inner join v_client_tmp b on a.clientid = b.clientid 
					  inner join bi_ttp_enrollments c on a.clientid = c.clientid 
	where a.is_row_current = 'y'
	and b.patientname not like '%obsolete%'
    and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
	and c.is_patient_converted = 'Yes' 
    and c.conversion_payment_date >= '2021-01-01'
) s left join 
	(
			select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, treatmentdays, a.date, medicine_drugs, medicine, actual_medCount as medCount,
			cgm_1d, cgm_5d, energy_1d, weight_1d, nut_adh_syntax, nut_adh_syntax_5d, AS_1d, AS_5d, BMI, coach_rating, IMT_opened, IMT_resolved, IMT_opened - IMT_resolved as IMT_outstanding,
            isMedRecommendationAvailable, isMedRecommendationAdopted, isMedRecommendationAdopted24hrs, total_delight_moments, MH_symptoms, length(MH_symptoms) - length(replace(MH_symptoms,',','')) + 1 as MH_symptoms_cnt, med_adh
			from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
            where a.date = c.conversion_payment_date-- date_sub(date(itz(now())), interval 1 day)
            and c.is_patient_converted = 'Yes' 
			and c.conversion_payment_date >= '2021-01-01'
	) s1 on s.clientid = s1.clientid 
    left join 
	(
			select a.clientid, a.date, concat('D',(treatmentdays - 1)) as treatmentday, actual_medCount as medCount,
			cgm_5d, weight_1d, MH_symptoms, length(MH_symptoms) - length(replace(MH_symptoms,',','')) + 1 as MH_symptoms_cnt, nut_adh_syntax_5d, AS_5d, IMT_opened - IMT_resolved as IMT_outstanding, med_adh
			from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
            where treatmentdays = 11
			and c.is_patient_converted = 'Yes'
            and c.conversion_payment_date >= '2021-01-01'
	) s2 on s.clientid = s2.clientid 
    left join 
    (
		select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, treatmentdays, a.date 
        from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
        where a.date = c.converion_payment_invoice_date-- date_sub(date(itz(now())), interval 1 day)
            and c.is_patient_converted = 'Yes' 
			and c.conversion_payment_date >= '2021-01-01'
    ) s3 on s.clientid = s3.clientid 
;
