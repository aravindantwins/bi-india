-- alter view v_tmp_overall_business_measure as 
select date, category, measure_name, measure_group, max(actual_measure) as actual_measure, max(target_measure) as target_measure
from 
(
select date, category, measure_name, measure_group, measure_category, case when measure_category = 'Actual' then measure end as actual_measure,
case when measure_category = 'Target' then measure end as target_measure
from 
(
select 
report_date as date, 
category, 
measure_name, 
measure_group, 
'Actual' as measure_category,
measure
from bi_overall_measure
where measure_name not in ('SNS ADH', 'SNS COM')
-- and weekday(report_date) = 3

union all 

select case when sprintdate = '2019-07-18' then '2019-07-11'  
			when sprintdate = '2019-08-01' then '2019-07-12' else sprintdate end as date, 
category, 
measurename as measure_name, 
measuregroup as measure_group, 
'Target' as measure_category, 
sprinttarget as measure
from bi_overall_sprint_target -- where (sprinttarget <> '' and sprinttarget is not null)

union all 

select case when sprintdate = '2019-07-18' then '2019-07-10'  
			when sprintdate = '2019-08-01' then '2019-07-09' else sprintdate end as date, 
category, 
measurename as measure_name, 
measuregroup as measure_group, 
'Target' as measure_category, 
sprinttarget as measure
from bi_overall_sprint_target -- where (sprinttarget <> '' and sprinttarget is not null)

union all 

select case when sprintdate = '2019-07-18' then '2019-07-07'  
			when sprintdate = '2019-08-01' then '2019-07-08' else sprintdate end as date, 
category, 
measurename as measure_name, 
measuregroup as measure_group, 
'Target' as measure_category, 
sprinttarget as measure
from bi_overall_sprint_target -- where (sprinttarget <> '' and sprinttarget is not null)

) sor
order by date
) s 
where date in ( '2019-07-07','2019-07-08','2019-07-09','2019-07-10','2019-07-11', '2019-07-12')
group by date, category, measure_name, measure_group

