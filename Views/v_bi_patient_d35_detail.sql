-- set collation_connection = 'latin1_swedish_ci';
alter view v_bi_patient_d35_detail as 
select clientid, patientname, coach, leadCoach, cohortname, enrollmentType, day35, cgm_5dg, tac_5d, AS_5d, coach_rating_5d, nut_adh_5d, is_MetOnly, toIncludeOrNot, success_ind, medicine_drugs
from 
(														
			select clientid, patientname, coach, leadCoach, enrollmentType, day35, toIncludeOrNot, cgm_5dg, tac_5d, round(AS_5d) as AS_5d, E5_5d, coach_rating_5d,													
			nut_adh_5d, total_incidents, cohortname, is_MetOnly,medicine_drugs, 
			if ((if(nut_adh_5d = 'YES', 'YES', if(E5_5d >= .90, 'YES','NO')) = 'YES'														
					and ifnull(tac_5d,0) >= 2.5												
					and (if(AS_5d >= 85, 'Yes', if(AS_5d < 85 and total_incidents >= 1, 'Yes','No')) = 'Yes')												
					and ifnull(coach_rating_5d,4) >= 4												
				) or cohortname = 'In Reversal' or is_MetOnly = 'Yes', 'Yes','No') as success_ind												
			from														
			(														
			select a.clientid, ct.patientname, ct.coachNameShort as coach, ct.leadCoachNameShort as leadCoach, day35, 													
			re.cgm_5d as cgm_5dg, re.tac_5d as tac_5d, re.as_5d as AS_5d,														
			b4.average_measure as E5_5d, re.coach_rating_5d, -- (length(bm2.measure) - length(replace(bm2.measure, ',', ''))) + 1 as total_meds,														
			case when re.nut_adh_syntax_5d >= 0.8 then 'YES' else 'NO' end as nut_adh_5d, -- bh.nut_happy														
			re.cohortname, re.is_MetOnly_by_OPS as is_MetOnly, re.medicine_drugs,												
			case when dc.isM1 = 'Yes' then 'M1'														
				when dc.isM2Converted = 'Yes' then 'M1-M2'													
				else 'M2' end as enrollmentType													
			,count(distinct imt.incidentid) as total_incidents
            ,re.isNoCGM_14days, re.total_available_foodlog_days
            ,case when re.isNoCGM_14days = 'No' and re.total_available_foodlog_days >0 then 'Yes' else 'No' end toIncludeOrNot
			from														
				(													
						select clientid, day35 -- , case when total_cgm_synced_last_14_days > 0 and total_foodlog_synced_last_14_days > 0 then 'Yes' else 'No' end as toIncludeOrNot											
						from											
						(		select distinct b.clientid, visitdateitz, date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) as day35									
								from	(								
												select clientid, visitdateitz, status,					
												max(status_start_date) as status_start_date, max(status_end_date) as status_end_date					
												from bi_patient_status b					
												group by clientid, status					
										) b							
								where b.status = 'active'									
								and datediff(status_end_date, if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ))) >= 35									
								and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) >= date_sub(date(itz(now())), interval 4 month)									
								and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) <= date(itz(now()))									
						) s											
				) a													
				left join bi_measures b4 on a.clientid = b4.clientid and b4.measure_event_date = a.day35 and b4.measure_type = '5d' and b4.measure_name in ('E5%')													
				left join (select distinct incidentid, clientid, status, date(report_time) as report_date from bi_incident_mgmt where category = 'sensors') imt on a.clientid = imt.clientid and report_date <= a.day35													
				left join bi_patient_reversal_state_gmi_x_date re on a.clientid = re.clientid and a.day35 = re.date													
				left join v_client_tmp ct on a.clientid = ct.clientid													
				left join dim_client dc on a.clientid = dc.clientid and dc.is_row_Current = 'Y'													
				group by a.clientid													
			) s													
) s
;														

							
