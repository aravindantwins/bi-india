alter view v_daily_mealplan_details as

select planname, mealtype, measuregroup, measurecategory, foodlabel, E5foodGrading, food_measure, quantity, day, 
measurevalue
from bi_daily_mealplan_detail

/* Old logic moved to Stored procedure along with new measures - sp_load_bi_daily_mealplan_details 
select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity,concat('Day ',a.day) as day, 
convert(sum(a.netcarb_measurevalue), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L1' as measureGroup,'Netcarb' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then typicalconsumedquantity * (carb - fibre) 
			else (typicalconsumedquantity * d.conversionRate * (carb - fibre)) end as netcarb_measureValue,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.calories)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Calories' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * calories) 
													  else  (typicalconsumedquantity * d.conversionRate * calories) end as calories,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.ngc_measureValue)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'NGC' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0)))  
	 else (typicalconsumedquantity * d.conversionrate * (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0))) end as ngc_measureValue,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.fibre)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fibre' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * fibre) 
													  else (typicalconsumedquantity * d.conversionrate * fibre) end as fibre,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.fat)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fat' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * fat) 
						else (typicalconsumedquantity * d.conversionrate * fat) end as fat,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.protein)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Protein' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * protein) 
													  else (typicalconsumedquantity * d.conversionRate * protein) end as protein,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.carb)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Total carb' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
primarycolloquialmeasure as food_measure, 
typicalconsumedquantity as quantity, 
b.mealtype, 
case when measurementtype = primarycolloquialmeasure then (typicalconsumedquantity * carb) 
													  else (typicalconsumedquantity * d.conversionrate * carb) end as carb,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a inner join dailyplantemplatemappings b on a.dailyplantemplateid = b.dailyplantemplateid
						  inner join foods c on b.foodid = c.foodid
                          left join foodmeasureconversions d on c.foodid = d.foodid
where c.draftStatus = 'published' 
) a
group by a.planname, a.day, a.mealtype, foodlabel
*/