CREATE VIEW v_bi_vip_present_twin_condition AS
select v.clientid, co.medicine, sym.MH_symptoms symptoms, d.days_on_cgm, d.Days_Nut_Adh, d.days_action,
case when co.healthstatuscohort in ('Default','escalation') then 'High'
	 when co.healthstatuscohort  in ('In Reversal','In Reversal + M') and co.treatmentDays<=90 then 'Medium'
     when co.healthstatuscohort in ('In Reversal','In Reversal + M') and co.treatmentDays>90 then 'Low' end as coach_call
from v_client_vip_list v
left join bi_patient_reversal_state_gmi_x_date sym on (v.clientid=sym.clientid) and sym.date =v.dateITZ
left join v_bi_patient_coach_detail co on (v.clientid= co.clientid) and co.date =(v.dateITZ - INTERVAL 1 DAY)
left join (select clientid, sum(case when cgm_1d is not null then 1 else 0 end) as days_on_cgm, sum(case when AS_1d is not null then 1 else 0 end) as days_action,
		   sum(case when nut_adh_syntax='Yes' then 1 else 0 end) as Days_Nut_Adh
		   from bi_patient_reversal_state_gmi_x_date
           group by clientid) d on (v.clientid=d.clientid)
;