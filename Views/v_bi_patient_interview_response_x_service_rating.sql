create view v_bi_patient_interview_response_x_service_rating as 
select a.date, sum(case when floor(recent_rating) >= 4 then 1 else 0 end) as rating_above_4, 
sum(case when floor(recent_rating) = 4 then 1 else 0 end) as rating_4, 
sum(case when floor(recent_rating) = 5 then 1 else 0 end) as rating_5, 
sum(case when floor(recent_rating) < 4 then 1 else 0 end) as rating_less_4, 
sum(case when recent_rating is not null then 1 else 0 end) as total_rating 
from
(
		select a.patientId, a.date, b.enrolledDay, date(recentTimestamp) as interview_date, b.HealthImprovements, b.CoachingSupport, b.NutritionSatisfaction, b.DoctorSupport, b.DailyRoutine, b.LogisticSupport, b.BloodDrawService, b.AppFeedback,
		cast((b.HealthImprovements + b.CoachingSupport + b.NutritionSatisfaction + b.DoctorSupport + b.DailyRoutine + b.LogisticSupport + b.BloodDrawService + b.AppFeedback)/8 as decimal(4,1)) as recent_rating
		from 
		(
				select patientId, a.date, max(timestamp) as recentTimestamp 
				from v_date_to_date a left join bi_patient_interview_response b on date(timestamp) <= a.date
                where patientid is not null
				group by patientID, a.date
		) a 
		inner join bi_patient_interview_response b on a.patientId = b.patientId and b.timestamp = a.recentTimestamp
) a
-- where date >= date_sub(date(itz(now())), interval 90 day) and date <= date(now()) 
group by a.date
