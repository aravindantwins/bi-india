set collation_connection = 'latin1_swedish_ci'; 
alter view v_bi_patient_health_review_tracker as 
select distinct if(dayDue = 'D30', date, investigationUploadDate) as date, 
clientid, 
enrollType,
patientname, 
coach,
leadCoach,
current_status,
doctornameshort,
email, 
mobileNumber,
current_treatmentdays,
treatmentday,
dayDue,
investigationUploadDate,
dueDateToReviewSchedule,	
BW_schedule_date, -- Blood Work Schedule Date
BW_schedule_status,
case when HR_schedule_date is null and calllogType is null then 'Yes'
	 when HR_schedule_date > dueDateToReviewSchedule then 'Yes'
     else 'No' end  as isOverDue, -- Overdue to schedule Health Review call or not
total_incidents,
HR_Created_Date,
HR_schedule_date, -- Health Review Schedule Date
HR_schedule_status,
HR_Schedule_int_doctor,
ifnull(retention,'') as retention,
ifnull(friction,'') as friction,
ifnull(actions,'') as actions,
calllogtype
from 
(
		select a.date, a.clientid, a.enrollType, ct.patientname, ct.coachnameShort as coach, ct.leadCoachNameShort as leadCoach, ct.status as current_status,
        ct.doctornameshort,
		ct.email,
        ct.mobileNumber,a.treatmentday, a.dayDue, 
        date(if(b.clientid is not null, itz(eventtime), '')) as investigationUploadDate, date_add(date(if(b.clientid is not null, itz(eventtime), '')), interval 7 day) as dueDateToReviewSchedule, 
        date(a.bloodwork_Scheduled_date) as BW_schedule_date, a.bloodwork_Scheduled_status as BW_schedule_status,
		date(a.HR_Created_Date) as HR_Created_Date,date(a.HR_Scheduled_Date) as HR_schedule_date, a.HR_Scheduled_status as HR_schedule_status,
        imt.total_incidents,
        dc.treatmentdays as current_treatmentdays,
        if(rComm.clientid is not null, comments, '') as actions, 
		if(rComm.clientid is not null, willMemberContinue, '') as retention, 
        if(rComm.clientid is not null, anyFriction, '') as friction,
        rComm.calllogType,
        HR_Schedule_int_doctor
        
		from 
		(
			select s.date, s.clientid, s.enrollType, s.treatmentday, s.dayDue, 
				case when s.dayDue = 'D30' then D30_HR_Created_Date
					 when s.dayDue = 'D90' then D90_HR_Created_Date
					 when s.dayDue = 'D180' then D180_HR_Created_Date
					 when s.dayDue = 'D270' then D270_HR_Created_Date
					 when s.dayDue = 'D360' then D360_HR_Created_Date 
                     when s.dayDue = 'D450' then D450_HR_Created_Date 
					 when s.dayDue = 'D540' then D540_HR_Created_Date else null end as HR_Created_Date, 
                
                case when s.dayDue = 'D30' then D30_HR_Scheduled_Date
					 when s.dayDue = 'D90' then D90_HR_Scheduled_Date
					 when s.dayDue = 'D180' then D180_HR_Scheduled_Date
					 when s.dayDue = 'D270' then D270_HR_Scheduled_Date
					 when s.dayDue = 'D360' then D360_HR_Scheduled_Date 
                     when s.dayDue = 'D450' then D450_HR_Scheduled_Date 
					 when s.dayDue = 'D540' then D540_HR_Scheduled_Date else null end as HR_Scheduled_Date, 
                     
				case when s.dayDue = 'D30' then D30_HR_status
					 when s.dayDue = 'D90' then D90_HR_status
					 when s.dayDue = 'D180' then D180_HR_status
					 when s.dayDue = 'D270' then D270_HR_status
					 when s.dayDue = 'D360' then D360_HR_status 
                     when s.dayDue = 'D450' then D450_HR_status
					 when s.dayDue = 'D540' then D540_HR_status  else null end as HR_Scheduled_status,

				case when s.dayDue = 'D30' then D30_HR_int_doctor
					 when s.dayDue = 'D90' then D90_HR_int_doctor
					 when s.dayDue = 'D180' then D180_HR_int_doctor
					 when s.dayDue = 'D270' then D270_HR_int_doctor
					 when s.dayDue = 'D360' then D360_HR_int_doctor 
                     when s.dayDue = 'D450' then D450_HR_int_doctor
					 when s.dayDue = 'D540' then D540_HR_int_doctor  else null end as HR_Schedule_int_doctor,
                     
				case when s.dayDue = 'D30' then s.date
					 when s.dayDue = 'D90' then D90_BloodWork_Scheduled_Date
					 when s.dayDue = 'D180' then D180_BloodWork_Scheduled_Date
					 when s.dayDue = 'D270' then D270_BloodWork_Scheduled_Date
					 when s.dayDue = 'D360' then D360_BloodWork_Scheduled_Date else null end as bloodwork_Scheduled_date, 
				case when s.dayDue = 'D30' then ''
					 when s.dayDue = 'D90' then D90_bloodwork_status
					 when s.dayDue = 'D180' then D180_bloodwork_status
					 when s.dayDue = 'D270' then D270_bloodwork_status
					 when s.dayDue = 'D360' then D360_bloodwork_status else null end as bloodwork_Scheduled_status
			from 
			(
						-- Section 1 to consider all regular enrollments
                        select date, 'M2' as enrollType, clientid, concat('D',treatmentdays - 1) as treatmentday, 
						case when treatmentdays - 1 between '25' and '45' then 'D30' 
							 when treatmentdays - 1 = 90 then 'D90'
							 when treatmentdays - 1 = 180 then 'D180'
							 when treatmentdays - 1 = 270 then 'D270'
							 when treatmentdays - 1 = 360 then 'D360' end as dayDue
						from bi_patient_reversal_state_gmi_x_date 
						where date >= date_sub(date(itz(now())), interval 1 month) 
						and (treatmentdays in (91, 181, 271, 361) or (treatmentdays >= 26 and treatmentdays <= 46))
						and clientid not in (select clientid from bi_ttp_enrollments where is_patient_converted = 'Yes')
						
						union all
						
						-- Section 2 to consider all TTP converted to Regular enrollments
						select s1.date, 'M1-M2' as enrollType, s.clientid, concat('D', s1.treatmentdays - 1) as treatmentday, 
						case when s1.treatmentdays - 1 between '25' and '45' then 'D30' 
							 when s.d90 = s1.date then 'D90'
							 when s.d180 = s1.date then 'D180'
							 when s.d270 = s1.date then 'D270'
							 when s.d360 = s1.date then 'D360' end as dayDue
						from 
						(
							select clientid, conversion_payment_date as d0, date_add(conversion_payment_date, interval 90 day) as d90, 
							date_add(conversion_payment_date, interval 180 day) as d180, date_add(conversion_payment_date, interval 270 day) as d270, 
							date_add(conversion_payment_date, interval 360 day) as d360
							from bi_ttp_enrollments 
							where is_patient_converted = 'Yes'
						) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and (s.d90 = s1.date or s.d180 = s1.date or s.d270 = s1.date or s.d360 = s1.date or (treatmentdays >= 26 and treatmentdays <= 46))
						where date >= date_sub(date(itz(now())), interval 2 month) 
			) s left join 
				(
							select a.clientid,
                            
							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D90' then itz(a.eventTime) else null end) as D90_BloodWork_Scheduled_Date, 
							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D90' then a.status else null end) as D90_bloodwork_status,                        

							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D180' then itz(a.eventTime) else null end) as D180_BloodWork_Scheduled_Date, 
							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D180' then a.status else null end) as D180_bloodwork_status,
							
							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D270' then itz(a.eventTime) else null end) as D270_BloodWork_Scheduled_Date, 
							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D270' then a.status else null end) as D270_bloodwork_status,
							
							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D360' then itz(a.eventTime) else null end) as D360_BloodWork_Scheduled_Date, 
							max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D360' then a.status else null end) as D360_bloodwork_status
									   
							from clientappointments a left join bloodworkschedules c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
							where a.type = 'BLOOD_WORK'
							group by a.clientid 
				) s1 on s.clientid = s1.clientid
				left join 
                (
						select a.clientid, UPPER(CONCAT(ui.firstname, ' ', ui.lastname)) as internalDoctor,

						max(case when c.quarter = 0 and c.day = 1 then itz(a.dateadded) else null end) as D30_HR_Created_Date, 
						max(case when c.quarter = 0 and c.day = 1 then itz(a.eventTime) else null end) as D30_HR_Scheduled_Date, 
						max(case when c.quarter = 0 and c.day = 1 then a.status else null end) as D30_HR_status,
						max(case when c.quarter = 0 and c.day = 1 then UPPER(CONCAT(ui.firstname, ' ', ui.lastname)) else null end) as D30_HR_int_doctor,

						max(case when c.quarter = 0 and c.day = 3 then itz(a.dateadded) else null end) as D90_HR_Created_Date,
						max(case when c.quarter = 0 and c.day = 3 then itz(a.eventTime) else null end) as D90_HR_Scheduled_Date, 
						max(case when c.quarter = 0 and c.day = 3 then a.status else null end) as D90_HR_status,
						max(case when c.quarter = 0 and c.day = 3 then UPPER(CONCAT(ui.firstname, ' ', ui.lastname)) else null end) as D90_HR_int_doctor,

						max(case when c.quarter = 1 and c.day = 4 then itz(a.dateadded) else null end) as D180_HR_Created_Date,
						max(case when c.quarter = 1 and c.day = 4 then itz(a.eventTime) else null end) as D180_HR_Scheduled_Date, 
						max(case when c.quarter = 1 and c.day = 4 then a.status else null end) as D180_HR_status,
						max(case when c.quarter = 1 and c.day = 4 then UPPER(CONCAT(ui.firstname, ' ', ui.lastname)) else null end) as D180_HR_int_doctor,

						max(case when c.quarter = 2 and c.day = 5 then itz(a.dateadded) else null end) as D270_HR_Created_Date,
						max(case when c.quarter = 2 and c.day = 5 then itz(a.eventTime) else null end) as D270_HR_Scheduled_Date, 
						max(case when c.quarter = 2 and c.day = 5 then a.status else null end) as D270_HR_status,
						max(case when c.quarter = 2 and c.day = 5 then UPPER(CONCAT(ui.firstname, ' ', ui.lastname)) else null end) as D270_HR_int_doctor,

						max(case when c.quarter = 3 and c.day = 6 then itz(a.dateadded) else null end) as D360_HR_Created_Date,
						max(case when c.quarter = 3 and c.day = 6 then itz(a.eventTime) else null end) as D360_HR_Scheduled_Date, 
						max(case when c.quarter = 3 and c.day = 6 then a.status else null end) as D360_HR_status,
						max(case when c.quarter = 3 and c.day = 6 then UPPER(CONCAT(ui.firstname, ' ', ui.lastname)) else null end) as D360_HR_int_doctor,

						max(case when c.quarter = 4 and c.day = 7 then itz(a.dateadded) else null end) as D450_HR_Created_Date,
						max(case when c.quarter = 4 and c.day = 7 then itz(a.eventTime) else null end) as D450_HR_Scheduled_Date, 
						max(case when c.quarter = 4 and c.day = 7 then a.status else null end) as D450_HR_status,
						max(case when c.quarter = 4 and c.day = 7 then UPPER(CONCAT(ui.firstname, ' ', ui.lastname)) else null end) as D450_HR_int_doctor,

						max(case when c.quarter = 5 and c.day = 8 then itz(a.dateadded) else null end) as D540_HR_Created_Date,
						max(case when c.quarter = 5 and c.day = 8 then itz(a.eventTime) else null end) as D540_HR_Scheduled_Date, 
						max(case when c.quarter = 5 and c.day = 8 then a.status else null end) as D540_HR_status,
						max(case when c.quarter = 5 and c.day = 8 then a.status else null end) as D540_HR_int_doctor
								   
						from clientappointments a inner join progressreviews c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
												  left join twinspii.userspii ui on c.internalDoctorUserId = ui.id
						where a.type = 'PROGRESS_REVIEW'
						group by a.clientid
				) s2 on s.clientid = s2.clientid
		) a 
		left join 
		(			
			select clientid, bloodworktime, eventtime from clinictestresults where deleted = 0 
		) b on a.clientid = b.clientid and date(itz(b.bloodWorkTime)) between date_add(date(a.bloodwork_Scheduled_date), interval -1 day) and date_add(date(a.bloodwork_Scheduled_date), interval 14 day)
        
        left join 
        (
			select clientid, count(distinct incidentId) as total_incidents from bi_incident_mgmt where status = 'OPEN' and category in ('DISENROLLMENT') group by clientid
		) imt on a.clientid = imt.clientid
        
        left join v_Client_tmp ct on a.clientid = ct.clientid
        left join dim_client dc on a.clientid = dc.clientid and dc.is_row_Current = 'y'
        
       left join (
						select distinct a.clientid, a.calllogType, a.internalDoctorUserId, itz(a.eventtime) as calllogDate, 
                        case when a.calllogType like '%PROGRESS%D30' then 'D30' 
							 when a.calllogType like '%PROGRESS%D100%' then 'D90'
                             when a.calllogType like '%PROGRESS%180%' then 'D180'
                             when a.calllogType like '%PROGRESS%270%' then 'D270'
                             when a.calllogType like '%PROGRESS%350%' then 'D360' end as callLogCategory, 
						max(case when c.label = 'Will Member Continue' then value else null end) as willMemberContinue, 
						max(case when c.label = 'Any Friction' then value else null end) as anyFriction,
						group_concat(comments separator '/n') as comments
						from calllogs a inner join calllogdetails b on a.calllogId = b.calllogId
										inner join calllogfields c on b.calllogfieldId = c.calllogfieldID
						where c.label in ('Will Member Continue','Any Friction')
						group by a.clientid, itz(a.eventtime)
				  ) rComm on a.clientid = rComm.clientid and a.daydue = rComm.callLogCategory -- and date(calllogDate) between date(hr_scheduled_date) and date_add(date(hr_scheduled_date), interval 7 day) 
)  s where if(dayDue = 'D30', date, investigationUploadDate)  is not null
;

/*
select a.clientid,

max(case when c.quarter = 0 and c.day = 1 then itz(a.eventTime) else null end) as D10_HR_Scheduled_Date, 
max(case when c.quarter = 0 and c.day = 1 then a.status else null end) as D10_HR_status,

max(case when c.quarter = 0 and c.day = 2 then itz(a.eventTime) else null end) as D30_HR_Scheduled_Date, 
max(case when c.quarter = 0 and c.day = 2 then a.status else null end) as D30_HR_status,

max(case when c.quarter = 0 and c.day = 3 then itz(a.eventTime) else null end) as D90_HR_Scheduled_Date, 
max(case when c.quarter = 0 and c.day = 3 then a.status else null end) as D90_HR_status,

max(case when c.quarter = 1 and c.day = 4 then itz(a.eventTime) else null end) as D180_HR_Scheduled_Date, 
max(case when c.quarter = 1 and c.day = 4 then a.status else null end) as D180_HR_status,

max(case when c.quarter = 2 and c.day = 5 then itz(a.eventTime) else null end) as D270_HR_Scheduled_Date, 
max(case when c.quarter = 2 and c.day = 5 then a.status else null end) as D270_HR_status,

max(case when c.quarter = 3 and c.day = 6 then itz(a.eventTime) else null end) as D360_HR_Scheduled_Date, 
max(case when c.quarter = 3 and c.day = 6 then a.status else null end) as D360_HR_status,

max(case when c.quarter = 4 and c.day = 7 then itz(a.eventTime) else null end) as D450_HR_Scheduled_Date, 
max(case when c.quarter = 4 and c.day = 7 then a.status else null end) as D450_HR_status,

max(case when c.quarter = 5 and c.day = 8 then itz(a.eventTime) else null end) as D540_HR_Scheduled_Date, 
max(case when c.quarter = 5 and c.day = 8 then a.status else null end) as D540_HR_status
		   
from clientappointments a inner join progressreviews c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
where a.type = 'PROGRESS_REVIEW'
group by a.clientid
;
 
 
*/