create view v_allDoctors_list as  
select id as doctorID, 
        LTRIM(RTRIM(UPPER(CONCAT(`firstName`, ' ', `lastName`)))) AS `doctorName`,
        LTRIM(RTRIM(UPPER(CONCAT(`firstName`,
                        ' ',
                        LEFT(`lastName`, 1))))) AS `doctorNameShort`
from doctors
where test = 0
;