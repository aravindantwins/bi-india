alter view v_daily_nutritionplan_patient_details
as
select cd.clientid, cd.patientname, cd.doctorname, cd.coachname, cd.date as mealdate, 
ifnull(x.measureGroup,'L1') as measureGroup, 
ifnull(x.measureCategory,'Netcarb') as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(foodLabel,'Not Available') as foodlabel, 
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.measurevalue, 'no meal plan selected') as measureValue
from 
(
	select d.date, c.clientId, c.enrollmentDateITZ, c.patientname, c.doctorname, c.coachname
	from twins.ddate d
	cross join v_active_clients c
	where d.date <= date(ITZ(now())) + 7 and d.date >= date(ITZ(now())) - 14 
) cd
left outer join
(
select a.clientid, a.mealdate, a.mealtype, 'L1' as measureGroup,'Netcarb' as measureCategory, c.foodLabel, c.measure as food_measure, 
a.quantity, convert(sum((c.carb - c.fibre) * a.quantity), decimal(7,2)) as measurevalue
from dailynutritionplans a,
    foods c 
where a.foodid = c.foodid
and a.selected = 1
group by a.clientid, a.mealdate, a.mealtype, foodlabel
) x on (cd.clientid=x.clientid and cd.date = x.mealdate)

union all

select cd.clientid, cd.patientname, cd.doctorname, cd.coachname, cd.date as mealdate, 
ifnull(x.measureGroup,'L2') as measureGroup, 
ifnull(x.measureCategory,'Calories') as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(foodLabel,'Not Available') as foodlabel, 
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.measurevalue, 'no meal plan selected') as measureValue
from 
(
	select d.date, c.clientId, c.enrollmentDateITZ, c.patientname, c.doctorname, c.coachname
	from twins.ddate d
	cross join v_active_clients c
	where d.date <= date(ITZ(now())) + 7 and d.date >= date(ITZ(now())) - 14
) cd
left outer join
(
select clientid, a.mealdate, a.mealtype, 'L2' as measureGroup,'Calories' as measureCategory, c.foodLabel, c.measure as food_measure, 
a.quantity,convert((sum(c.calories) * a.quantity), decimal(7,2)) as measurevalue
from dailynutritionplans a,
    foods c 
where a.foodid = c.foodid
and a.selected = 1
group by a.clientid, a.mealdate, a.mealtype, foodlabel
) x on (cd.clientid=x.clientid and cd.date = x.mealdate)

union all

select cd.clientid, cd.patientname, cd.doctorname, cd.coachname, cd.date as mealdate, 
ifnull(x.measureGroup,'L2') as measureGroup, 
ifnull(x.measureCategory,'NGC') as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(foodLabel,'Not Available') as foodlabel, 
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.measurevalue, 'no meal plan selected') as measureValue
from 
(
	select d.date, c.clientId, c.enrollmentDateITZ, c.patientname, c.doctorname, c.coachname
	from twins.ddate d
	cross join v_active_clients c
	where d.date <= date(ITZ(now())) + 7 and d.date >= date(ITZ(now())) - 14
) cd
left outer join
(
select clientid, a.mealdate, a.mealtype, 'L2' as measureGroup,'NGC' as measureCategory, c.foodLabel, c.measure as food_measure, 
a.quantity, convert((sum((IFNULL((((carb - fibre) * glycemicIndex) / 55), 0))) * a.quantity), decimal(7,2)) as measurevalue
from dailynutritionplans a,
    foods c 
where a.foodid = c.foodid
and a.selected = 1
group by a.clientid, a.mealdate, a.mealtype, foodlabel
) x on (cd.clientid=x.clientid and cd.date = x.mealdate)

union all

select cd.clientid, cd.patientname, cd.doctorname, cd.coachname, cd.date as mealdate, 
ifnull(x.measureGroup,'L2') as measureGroup, 
ifnull(x.measureCategory,'Fibre') as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(foodLabel,'Not Available') as foodlabel, 
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.measurevalue, 'no meal plan selected') as measureValue
from 
(
	select d.date, c.clientId, c.enrollmentDateITZ, c.patientname, c.doctorname, c.coachname
	from twins.ddate d
	cross join v_active_clients c
	where d.date <= date(ITZ(now())) + 7 and d.date >= date(ITZ(now())) - 14
) cd
left outer join
(
select clientid, a.mealdate, a.mealtype, 'L2' as measureGroup,'Fibre' as measureCategory, c.foodLabel, c.measure as food_measure, 
a.quantity, convert((sum(c.fibre) * a.quantity), decimal(7,2)) as measurevalue
from dailynutritionplans a,
    foods c 
where a.foodid = c.foodid
and a.selected = 1
group by a.clientid, a.mealdate, a.mealtype, foodlabel
) x on (cd.clientid=x.clientid and cd.date = x.mealdate)

union all

select cd.clientid, cd.patientname, cd.doctorname, cd.coachname, cd.date as mealdate, 
ifnull(x.measureGroup,'L2') as measureGroup, 
ifnull(x.measureCategory,'Fat') as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(foodLabel,'Not Available') as foodlabel, 
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.measurevalue, 'no meal plan selected') as measureValue
from 
(
	select d.date, c.clientId, c.enrollmentDateITZ, c.patientname, c.doctorname, c.coachname
	from twins.ddate d
	cross join v_active_clients c
	where d.date <= date(ITZ(now())) + 7 and d.date >= date(ITZ(now())) - 14
) cd
left outer join
(
select clientid, a.mealdate, a.mealtype, 'L2' as measureGroup,'Fat' as measureCategory, c.foodLabel, c.measure as food_measure,
a.quantity, convert((sum(c.fat) * a.quantity), decimal(7,2)) as measurevalue
from dailynutritionplans a,
    foods c 
where a.foodid = c.foodid
and a.selected = 1
group by a.clientid, a.mealdate, a.mealtype, foodlabel
) x on (cd.clientid=x.clientid and cd.date = x.mealdate)

union all

select cd.clientid, cd.patientname, cd.doctorname, cd.coachname, cd.date as mealdate, 
ifnull(x.measureGroup,'L2') as measureGroup, 
ifnull(x.measureCategory,'Protein') as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(foodLabel,'Not Available') as foodlabel, 
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.measurevalue, 'no meal plan selected') as measureValue
from 
(
	select d.date, c.clientId, c.enrollmentDateITZ, c.patientname, c.doctorname, c.coachname
	from twins.ddate d
	cross join v_active_clients c
	where d.date <= date(ITZ(now())) + 7 and d.date >= date(ITZ(now())) - 14
) cd
left outer join
(
select clientid, a.mealdate, a.mealtype, 'L2' as measureGroup,'Protein' as measureCategory, c.foodLabel, c.measure as food_measure, 
a.quantity, convert((sum(c.protein) * a.quantity), decimal(7,2)) as measurevalue
from dailynutritionplans a,
    foods c 
where a.foodid = c.foodid
and a.selected = 1
group by a.clientid, a.mealdate, a.mealtype, foodlabel
) x on (cd.clientid=x.clientid and cd.date = x.mealdate)

union all

select cd.clientid, cd.patientname, cd.doctorname, cd.coachname, cd.date as mealdate, 
ifnull(x.measureGroup,'L2') as measureGroup, 
ifnull(x.measureCategory,'Total carb') as measureCategory,
ifnull(mealtype, 'Not Available') as mealtype, 
ifnull(foodLabel,'Not Available') as foodlabel, 
ifnull(food_measure,'Not Available') as food_measure, 
ifnull(quantity, 'Not Available') as quantity,
ifnull(x.measurevalue, 'no meal plan selected') as measureValue
from 
(
	select d.date, c.clientId, c.enrollmentDateITZ, c.patientname, c.doctorname, c.coachname
	from twins.ddate d
	cross join v_active_clients c
	where d.date <= date(ITZ(now())) + 7 and d.date >= date(ITZ(now())) - 14
) cd
left outer join
(
select clientid, a.mealdate, a.mealtype, 'L2' as measureGroup,'Total carb' as measureCategory, c.foodLabel, c.measure as food_measure,
 a.quantity, convert((sum(c.carb) * a.quantity), decimal(7,2)) as measurevalue
from dailynutritionplans a,
    foods c 
where a.foodid = c.foodid
and a.selected = 1
group by a.clientid, a.mealdate, a.mealtype, foodlabel
) x on (cd.clientid=x.clientid and cd.date = x.mealdate)
;

