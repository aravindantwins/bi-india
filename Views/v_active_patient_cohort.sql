-- alter view v_active_patient_cohort as
select cd.clientid, patientname, coachname, doctorname, cd.date as status_date, ifnull(a.measure,'Default') as cohortname
from 
(
		select d.date, c.clientId, patientname, coachname, doctorname
		from twins.v_date_to_date d
		inner join (
				select a.clientid, a.patientname, a.coachname, a.doctorname, status_start_date, status_end_date 
                from v_active_clients a inner join bi_patient_status b 
                on a.clientid = b.clientid and b.status = 'active' 
		) c
		where d.date between c.status_start_date and c.status_end_date
) cd left join bi_patient_monitor_measures a on cd.clientid = a.clientid and cd.date = a.measure_event_date and a.measure_name = 'Cohort'
;

/* OLD definition - since cohort is maintained in bi_patient_monitor_measures, we can bring it from there
select a.clientid, patientname, coachname, doctorname, a.date as status_date, ifnull(c.name,'Default') as cohortname
from 
(
    select cd.clientid, patientname, coachname, doctorname, cd.date, max(itz(starttime)) as change_timeITZ
	from 
    (
		select d.date, c.clientId, c.enrollmentDateITZ, patientname, coachname, doctorname
		from twins.v_date_to_date d
		cross join v_active_clients c
		where d.date >= date(c.enrollmentDateITZ)
	) cd 
	left join cohorthistories a on cd.clientid = a.clientid and date(itz(Starttime)) <= cd.date
	group by cd.clientid, cd.date
    ) a left join cohorthistories b on a.clientid = b.clientid and a.change_timeITZ = itz(b.starttime)
        left join cohorts c on b.cohortid = c.cohortid
;
*/
