set collation_connection = 'latin1_swedish_ci';

alter view v_bi_clinical_service_medpublish_live as 
select a.clientid as Clientid, b.patientName as patientname, 
if(publishstatus='new',b.poc,null ) as pocname, 
if( publishstatus='Repeated', b.coachNameShort,null) as coachname,b.doctorname as Doctors_Name, b.morningInternalDoctorName, b.healthCoordinator, b.leadCoachNameShort as leadCoach,
if (publishstatus='new',itz(c.dateAdded), null) as Appoinment_created_time,
if(publishstatus='new',itz(c.eventtime),null) as Appointment_schedule_time,
a.recommendationDate as Recommendationtime,
if(publishstatus='Repeated', timestampadd(hour,12,a.recommendationDate),timestampadd(hour,2,itz(c.eventtime))) as Meds_Publish_Half_Time_Trigger,
if(publishstatus='Repeated',timestampadd(hour,24,a.recommendationDate),timestampadd(hour,4,itz(c.eventtime))) as Meds_Publish_time_Expected,
a.actionedDate as Meds_Publish_time_Actual,
a.publishstatus
from
(
	select f1.clientid, recommendationDate , status, actionedDate ,
		case when date(f1.recommendationdate) <> f2.newpublishingdate then 'Repeated'
			 when date(f1.recommendationdate) = f2.newpublishingdate then 'New' else null end as publishStatus, 
		actionTime
	from
	(
			select a.clientid,  eventtime as recommendationdate, status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime
			from
			(
					select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId, type
					from medicationrecommendations a inner join v_client_tmp b on a.clientid = b.clientid 
													 left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'
					where date(itz(a.eventtime)) >= date_sub(date(itz(now())), interval 5 day) 
					and (a.medicationTierID is not NULL OR a.status = 'MANUAL')
					and (labels not like '%RESEARCH%' and labels not like '%unqualified%')
					and ifnull(c.is_type1like, 'No') <> 'Yes' 
                    and type = 'DIABETES'
			) a left join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
  ) f1 left join (
					select clientid, min(date(itz(eventtime))) as newpublishingdate from medicationrecommendations 
					where type = 'DIABETES' 
					AND (medicationTierID is not NULL OR status = 'MANUAL')
					group by clientid
				  ) f2 on f1.clientid = f2.clientid 
	
)a join v_allclient_list b on a.clientid=b.clientid 
	left join (
				select c.clientid, c.eventtime, d.dateadded 
				from clientappointments c inner join doctorappointments d on (c.clientid = d.clientid and c.clientAppointmentID = d.clientAppointmentId)
				where c.type='DOCTOR_APPOINTMENT' 
				and d.quarter = '0' and d.day = '0'
				and date(eventtime) >= curdate() - interval 10 day	
	) c on a.clientid = c.clientid 
; 	

            
            
