set collation_connection = 'latin1_swedish_ci'; 

alter view v_bi_customer_success_churn as 
select distinct 
		b.Category,
		a.date as Discharge_Date,
		b.ClientId,
        b.patientNameShort as Patient_Name,
        b.city,
        b.gender,
        b.age,
        b.Years_Diabetic,
        b.start_symptoms as Pre_Twin_Symptoms,
        b.startSymptomCount as Pre_Twin_Symptoms_Count, 
        b.startMedicineCount Pre_Twin_Med,
        b.first_available_cgm_5d as Pre_Twin_5dg,
        b.coachNameShort as Coach,
        b.leadCoachNameShort as Lead_Coach,
        b.doctorNameShort as Doctor,
        b.morningInternalDoctorName as twinMorningDoctor,      
        b.TreatmentDays,
        b.Is_Achieved_Reversal,
        b.Is_Current_Reversal, 
        b.Latest_Invoice_Date,
     
        b.InvoiceStatus,
        b.Aging_Days,
        b.IMT_Report_Date,
        b.IMT_Dis_Subtype_L1,
        b.PlanCode,
        b.EnrollType,
        b.dischargeReason,
		b.dischargeReasonL1,
        b.dischargeReasonL2
from
(select date from v_date_to_date where date between date_sub(date(ITZ(now())), interval 2 month) and date(ITZ(now())) )a
left join
(
	select 	
		'AR Churn' as Category,
		b.status_start_date as Discharge_Date, 
        b.ClientId,
        aa.patientNameShort,
        aa.city,
        aa.gender,
        aa.age,
        c.durationYears_diabetes as Years_Diabetic,
        c.start_symptoms, 
       case when c.start_symptoms not like 'None' 
			then
				((LENGTH(c.start_symptoms) - LENGTH(REPLACE(c.start_symptoms,
                    ',',
                    ''))) + 1)
			else 0 end as startSymptomCount,
        -- c.start_medicine_diabetes AS startmedicine,
        ((LENGTH(c.start_medicine_diabetes_drugs) - LENGTH(REPLACE(c.start_medicine_diabetes_drugs,
                    ',',
                    ''))) + 1) AS startMedicineCount,
		c.first_available_cgm_5d, 
        aa.coachNameShort,
        aa.leadCoachNameShort,
        aa.doctorNameShort,
        aa.morningInternalDoctorName,  	
        c.TreatmentDays,
        bb.Is_Achieved_Reversal,
        bb.Is_Current_Reversal, 
        
     	bb.Latest_Invoice_Date,
      --  bb.Latest_Payment_Date,
		bb.InvoiceStatus,
        DATEDIFF(b.status_start_date, bb.Latest_Invoice_Date) as Aging_Days,
        d.IMT_Report_Date,
        -- Date(ITZ(d.report_time)) as IMT_Report_Date,
        d.subtype_L1 as IMT_Dis_Subtype_L1,
        aa.PlanCode,
         case when c.isM1 ='Yes' then 'M1'
			 when c.isM2Converted='Yes' then 'M1-M2'
             else 'M2' end  AS EnrollType,
        aa.dischargeReason,
        aa.dischargeReasonL1,
       	aa.dischargeReasonL2
	         
      from
      (
		(select status_start_date, clientid from bi_patient_status  where status='DISCHARGED' and status_start_date>= date_sub(date(ITZ(now())), interval 70 day)) b 
		join
			(select a.id,a.latest_invoice_date,a.invoicestatus,a.Is_Achieved_Reversal,a.Is_Current_Reversal
			   from 
			    (
					select a.id, cast(a.invoiceDate as date) as latest_invoice_date
							, a.invoicestatus,
							case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
								  else 'No' end as Is_Achieved_Reversal,
							 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
								  else 'No' end as Is_Current_Reversal
					from clientauxfields a inner join clients b on a.id=b.id 
					where a.invoicestatus IN ('Overdue', 'VOID') -- and b.status='DISCHARGED'
				) a
				-- left join bi_patient_reversal_state_gmi_x_date b on a.id=b.clientid and a.latest_invoice_date=b.date
		    ) bb on b.clientid=bb.id
		left join v_allclient_list aa on b.clientid=aa.clientid	
		left join dim_client c on (b.clientid=c.clientid and c.is_row_current='Y')
		left join 
		(select distinct clientid, date(itz(report_time)) as IMT_Report_Date, subtype_L1 from bi_incident_mgmt where category = 'disenrollment' and status = 'open') d on (b.clientid=d.clientid)
		)
      

	union all

	select 	'Non AR Churn' as Category,
		b.status_start_date as Discharge_Date, 
        b.ClientId,
        aa.patientNameShort,
        aa.city,
        aa.gender,
        aa.age,
        c.durationYears_diabetes as Years_Diabetic,
        c.start_symptoms, 
       case when c.start_symptoms not like 'None' 
			then
				((LENGTH(c.start_symptoms) - LENGTH(REPLACE(c.start_symptoms,
                    ',',
                    ''))) + 1)
			else 0 end as startSymptomCount,
        -- c.start_medicine_diabetes AS startmedicine,
        ((LENGTH(c.start_medicine_diabetes_drugs) - LENGTH(REPLACE(c.start_medicine_diabetes_drugs,
                    ',',
                    ''))) + 1) AS startMedicineCount,
		c.first_available_cgm_5d, 
        aa.coachNameShort,
        aa.leadCoachNameShort,
        aa.doctorNameShort,
        aa.morningInternalDoctorName,
     	
        c.TreatmentDays,
        bb.Is_Achieved_Reversal,
        bb.Is_Current_Reversal, 
        
     	bb.Latest_Invoice_Date,
       -- bb.Latest_Payment_Date,
		bb.InvoiceStatus,
        DATEDIFF(b.status_start_date,  d.IMT_Report_Date) as Aging_Days,
        d.IMT_Report_Date,
       -- d.IMT_resolved_date, 
        d.subtype_L1 as IMT_Dis_Subtype_L1,
        aa.PlanCode,
        case when c.isM1 ='Yes' then 'M1'
			 when c.isM2Converted='Yes' then 'M1-M2'
             else 'M2' end  AS EnrollType,
        aa.dischargeReason,
        aa.dischargeReasonL1,
        aa.dischargeReasonL2
  from 
  (
	(select status_start_date , clientid from bi_patient_status  where status='DISCHARGED' and status_start_date>= date_sub(date(ITZ(now())), interval 70 day)) b 
    join
       (  select s.clientid,s.IMT_report_date,s.subtype_L1 -- ,b.TreatmentDays 
			from
			(
				 select a.clientid,a.Max_IMT_report_date as IMT_report_date,c.subtype_L1
				 from
				   ( 	select distinct clientid,max(Date(ITZ(report_time))) as Max_IMT_report_date
						from bi_incident_mgmt where category ='disenrollment'
						group by clientid 
					) a 
				  inner join bi_incident_mgmt c on a.clientid=c.clientid and a.Max_IMT_report_date=date(ITZ(c.report_time)) and c.category ='disenrollment'
			) s 
         -- left join bi_patient_reversal_state_gmi_x_date b on s.clientid=b.clientid and s.IMT_report_date=b.date
		) d on d.clientid=b.clientid
	join
		( select a.id, cast(a.invoiceDate as date) as latest_invoice_date, 
			 a.invoicestatus,a.suspended,
            case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
					  else 'No' end as Is_Achieved_Reversal,
                 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
                      else 'No' end as Is_Current_Reversal
			from clientauxfields a inner join clients b on a.id=b.id 
            where ifnull(invoicestatus,'blank') not in ( 'Overdue', 'VOID') -- and b.status='DISCHARGED'
		)  bb on b.clientid=bb.id
	left join v_allclient_list aa on b.clientid = aa.clientid 
	left join dim_client c on (b.clientid=c.clientid and c.is_row_current='Y')
  )
  ) b on (a.date = b.Discharge_Date)
where category is not null
;
