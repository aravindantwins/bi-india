create view v_bi_overall_business_target_measure
as 
select 
	date, 
	category, 
	measure_name,
	measure_group, 
	actual_measure, 
	target_measure
from bi_overall_business_target_measure
;