create view v_bi_patient_BS_health_milestone as 
select s.clientid, case when s1.start_medicine_diabetes_drugs like '%INSU%' then 'Yes' else 'No' end as is_Insulin_on_day0, 
s.category, s.BS_ReducedDays_within30, BS_ReducedDays_within60, BS_ReducedDays_within90, BS_ReducedDays_within120, 
s.MedReducedDays_within30, s.MedReducedDays_within60, s.MedReducedDays_within90, s.MedReducedDays_within120,
s.BS_Reduced_withNoMed_or_MetOnly_within30, s.BS_Reduced_withNoMed_or_MetOnly_within60, s.BS_Reduced_withNoMed_or_MetOnly_within90, s.BS_Reduced_withNoMed_or_MetOnly_within120,
s.BS_Consec30days_withNoMed_or_MetOnly_within30, s.BS_Consec30days_withNoMed_or_MetOnly_within60, s.BS_Consec30days_withNoMed_or_MetOnly_within90, s.BS_Consec30days_withNoMed_or_MetOnly_within120
from 
(
			select clientid, 'GMI' as category, 
			count(distinct case when treatmentdays between 1 and 30 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as BS_ReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as BS_ReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as BS_ReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as BS_ReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and GMI < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and GMI < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and GMI < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and GMI < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within120,

			count(distinct case when treatmentdays between 1 and 30 and consecutive_GMI_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and consecutive_GMI_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and consecutive_GMI_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and consecutive_GMI_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within120
			from bi_patient_reversal_state_GMI_x_date s 
			group by clientid 

			union 

			select clientid, 'eA1C-5dg' as category, 
			count(distinct case when treatmentdays between 1 and 30 and (eA1C_5dg < 6.5  or (start_LabA1C - eA1C_5dg) > 0.4) then date else null end) as BS_ReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and (eA1C_5dg < 6.5  or (start_LabA1C - eA1C_5dg) > 0.4) then date else null end) as BS_ReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and (eA1C_5dg < 6.5  or (start_LabA1C - eA1C_5dg) > 0.4) then date else null end) as BS_ReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and (eA1C_5dg < 6.5  or (start_LabA1C - eA1C_5dg) > 0.4) then date else null end) as BS_ReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and eA1C_5dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and eA1C_5dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and eA1C_5dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and eA1C_5dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within120,

			count(distinct case when treatmentdays between 1 and 30 and consecutive_eA1C_5dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and consecutive_eA1C_5dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and consecutive_eA1C_5dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and consecutive_eA1C_5dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within120
			from bi_patient_reversal_state_GMI_x_date s 
			group by clientid 

			union 

			select clientid, 'eA1C-14dg' as category, 
			count(distinct case when treatmentdays between 1 and 30 and (eA1C_14dg < 6.5  or (start_LabA1C - eA1C_14dg) > 0.4) then date else null end) as BS_ReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and (eA1C_14dg < 6.5  or (start_LabA1C - eA1C_14dg) > 0.4) then date else null end) as BS_ReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and (eA1C_14dg < 6.5  or (start_LabA1C - eA1C_14dg) > 0.4) then date else null end) as BS_ReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and (eA1C_14dg < 6.5  or (start_LabA1C - eA1C_14dg) > 0.4) then date else null end) as BS_ReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and eA1C_14dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and eA1C_14dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and eA1C_14dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and eA1C_14dg < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within120,

			count(distinct case when treatmentdays between 1 and 30 and consecutive_eA1C_14dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and consecutive_eA1C_14dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and consecutive_eA1C_14dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and consecutive_eA1C_14dg_threshold_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within120
			from bi_patient_reversal_state_GMI_x_date s 
			group by clientid 

			union 

			select clientid, 'labA1C' as category, 
			count(distinct case when treatmentdays between 1 and 30 and (recentLabA1C < 6.5  or (start_LabA1C - recentLabA1C) > 0.4) then date else null end) as BS_ReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and (recentLabA1C < 6.5  or (start_LabA1C - recentLabA1C) > 0.4) then date else null end) as BS_ReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and (recentLabA1C < 6.5  or (start_LabA1C - recentLabA1C) > 0.4) then date else null end) as BS_ReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and (recentLabA1C < 6.5  or (start_LabA1C - recentLabA1C) > 0.4) then date else null end) as BS_ReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within30,
			count(distinct case when treatmentdays between 1 and 60 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within60,
			count(distinct case when treatmentdays between 1 and 90 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within90,
			count(distinct case when treatmentdays between 1 and 120 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within120,

			count(distinct case when treatmentdays between 1 and 30 and recentLabA1C < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and recentLabA1C < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and recentLabA1C < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and recentLabA1C < 6.5 and (medicine_drugs is null or is_MetOnly = 'Yes') then date else null end) as BS_Reduced_withNoMed_or_MetOnly_within120,

			count(distinct case when treatmentdays between 1 and 30 and recentLabA1C < 6.5 and consecutive_Med_satisfied_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within30,
			count(distinct case when treatmentdays between 1 and 60 and recentLabA1C < 6.5 and consecutive_Med_satisfied_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within60,
			count(distinct case when treatmentdays between 1 and 90 and recentLabA1C < 6.5 and consecutive_Med_satisfied_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within90,
			count(distinct case when treatmentdays between 1 and 120 and recentLabA1C < 6.5 and consecutive_Med_satisfied_days >= 30 then date else null end) as BS_Consec30days_withNoMed_or_MetOnly_within120
			from bi_patient_reversal_state_GMI_x_date s 
			group by clientid 
) s left join dim_client s1 on s.clientid = s1.clientid and s1.is_row_current = 'Y'
;