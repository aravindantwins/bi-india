create view v_bi_patient_nut_happiness as 
select clientid, interview_score, total_nutrition_incident as total_nutrition_incidents, total_disenrollment_incident as total_disenrollment_incidents, food_rating, nut_happy
from bi_patient_nutrition_happiness
where current_status = 'active'
;

-- This definition is moved to a stored procedure to maintain the data in table.
/*
create view v_bi_patient_nut_happiness as 
select clientid, interview_score, total_nutrition_incidents, total_disenrollment_incidents, food_rating,					
case when interview_score = 'ND' and total_nutrition_incidents = 'ND' and total_disenrollment_incidents ='ND' and food_rating = 'ND' then 'ND'					
	 when if(total_nutrition_incidents='ND',0,total_nutrition_incidents) = 0 and if(total_disenrollment_incidents='ND',0,total_disenrollment_incidents) > 0 then 0 				
	 when if(interview_score='ND',5,interview_score) >= 4 and if(food_rating = 'ND',5,food_rating) >= 4 and if(total_nutrition_incidents='ND',0,total_nutrition_incidents) = 0 and if(total_disenrollment_incidents='ND',0,total_disenrollment_incidents) = 0 then 1 				
     else 0 end as nut_happy					
from 					
(					
		select clientid, ifnull(interview_nut_satisfaction_score,'ND') as interview_score, 			
		ifnull(total_nutrition_incidents,'ND') as total_nutrition_incidents,			
		ifnull(total_disenrollment_incidents,'ND') as total_disenrollment_incidents,			
		ifnull(food_rating,'ND') as food_rating			
		from 			
		(			
			select s1.clientid, cast(avg(s3.NutritionSatisfaction) as decimal(5,2)) as interview_nut_satisfaction_score, 		
			if(s4.incidentid is null, null, count(distinct s4.incidentid)) as total_nutrition_incidents, 		
            if(s5.incidentid is null, null, count(distinct s5.incidentid)) as total_disenrollment_incidents, 					
			cast(rating_provided as decimal(5,2)) as food_rating		
			from dim_client s1 left join 		
			( 		
				select s1.clientid, avg(s1.foodrating) as rating_provided	
				from personalizedfoods s1
                where foodrating is not null
                group by s1.clientid					
			) s2 on s1.clientid = s2.clientid		
			left join bi_stage_patient_interview_response s3 on s1.clientid = s3.patientid		
			left join bi_incident_mgmt s4 on s1.clientid = s4.clientid and s4.category = 'NUTRITION' and s4.status = 'OPEN'		
            left join bi_incident_mgmt s5 on s1.clientid = s5.clientid and s5.category = 'DISENROLLMENT' and s5.status = 'OPEN'					
					
			where s1.status in ('active') and s1.is_row_current = 'y'		
			group by s1.clientid		
		) s1 			
) s 					
;					
*/