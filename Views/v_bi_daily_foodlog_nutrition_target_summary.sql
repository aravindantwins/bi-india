-- select concat('(select ', 'concat(', 'greenStart,','''-''',',greenEnd)',' from bi_nutrition_target where foodsNutrient =', '''',foodsNutrient,''')', ' as ', foodsNutrient,'_range,')  from bi_nutrition_Target
-- select concat('sum(',foodsNutrient,') as ',foodsNutrient,',') from bi_nutrition_target

create view v_bi_daily_foodlog_nutrition_target_summary as 
select 
a.clientid, b.patientname, mealdate, category, net_carb, calories, netGlycemicCarb, 
fibre, fat, protein, Total_Carb, sodium, potassium, magnesium, calcium, chromium, 
omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids, improveInsulinSensitivity, 
inhibitGluconeogenis, inhibitCarbAbsorption, improveInsulinSecretion, improveBetaCellRegeneration, 
inhibitHunger, inhibitGlucoseKidneyReabsorption, lactococcus, lactobacillus, leuconostoc, streptococcus, 
bifidobacterium, saccharomyces, bacillus, fructose, glycemicIndex, saturatedFat, monounsaturatedFat, polyunsaturatedFat,
transFat, cholesterol, histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, 
threonine, valine, vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, 
vitaminB6, folate, copper, iron, zinc, manganese, phosphorus, selenium, omega6_3_ratio, zinc_copper_ratio, pot_sod_ratio, 
cal_mag_ratio, pralAlkalinity, improveBloodPressure, improveCholesterol, reduceWeight, improveRenalFunction, improveLiverFunction, 
improveThyroidFunction, improveArthritis, reduceUricAcid, veg, nonVeg, fruit, oil, spice, grain, legume, nuts, seeds, 
inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex, water
from bi_foodlog_nutrition_target_summary a inner join v_client_tmp b on a.clientid = b.clientid 
where a.mealdate >= date_sub(date(itz(now())), interval 45 day)
;

/* Moved to table
union all 

select 
s1.clientid, s1.patientname, date_add(date(itz(now())), interval 5 day) as mealdate, 'RecentLab' as category, null as net_carb, null as calories, null as netGlycemicCarb, 
null as fibre, null as fat, null as protein, null as Total_Carb, s2.sodium, s2.potassium, s2.magnesium, s2.calcium, null as chromium, 
null as omega3, null as omega6, null as alphaLipoicAcid, null as q10, null as biotin, null as flavonoids, null as improveInsulinSensitivity, 
null as inhibitGluconeogenis, null as inhibitCarbAbsorption, null as improveInsulinSecretion, null as improveBetaCellRegeneration, 
null as inhibitHunger, null as inhibitGlucoseKidneyReabsorption, null as lactococcus, null as lactobacillus, null as leuconostoc, null as streptococcus, 
null as bifidobacterium, null as saccharomyces, null as bacillus, null as fructose, null as glycemicIndex, null as saturatedFat, null as monounsaturatedFat, null as polyunsaturatedFat,
null as transFat, s2.cholesterol, null as histidine, null as isolecuine, null as lysine, null as methionineAndCysteine, null as phenylananineAndTyrosine, null as tryptophan, 
null as threonine, null as valine, null as vitaminA, null as vitaminC, s2.vitaminDTotal as vitaminD, null as vitaminE, null as vitaminK, null as vitaminB1, s2.vitaminB12, null as vitaminB2, null as vitaminB3, null as vitaminB5, 
null as vitaminB6, null as folate, s2.serumCopper as copper, s2.serumIron as iron, s2.serumZinc as zinc, null as manganese, s2.phosphorous, null as selenium, null as omega6_3_ratio, null as zinc_copper_ratio, null as pot_sod_ratio, 
null as cal_mag_ratio, null as pralAlkalinity, null as improveBloodPressure, null as improveCholesterol, null as reduceWeight, null as improveRenalFunction, null as improveLiverFunction, 
null as improveThyroidFunction, null as improveArthritis, null as reduceUricAcid, null as veg, null as nonVeg, null as fruit, null as oil, null as spice, null as grain, null as legume, null as nuts, null as seeds, 
null as inflammatoryIndex, null as oxidativeStressIndex, null as gluten, null as allergicIndex, null as water
from  
(
	select a.clientid, a.patientname, max(date(itz(bloodWorkTime))) as recent_labTestDate
	from v_active_clients a inner join clinictestResults b on a.clientid = b.clientid  
	where bloodworktime is not null
	group by a.clientid, a.patientname
) s1 inner join clinictestresults s2 on s1.clientid = s2.clientid and s1.recent_labTestDate = date(itz(s2.bloodWorkTime))
;
*/


/* Old logic - was showing only Actual, DailyTarget
create view v_bi_daily_foodlog_nutrition_target_summary as 
select distinct a.clientid, a.patientname, mealdate, 'Actual' as category,
sum(net_carb) as net_carb,
sum(calories) as calories,
sum(netGlycemicCarb) as netGlycemicCarb,
sum(fibre) as fibre,
sum(fat) as fat,
sum(protein) as protein,
sum(Total_Carb) as Total_Carb,
sum(sodium) as sodium,
sum(potassium) as potassium,
sum(magnesium) as magnesium,
sum(calcium) as calcium,
sum(chromium) as chromium,
sum(omega3) as omega3,
sum(omega6) as omega6,
sum(alphaLipoicAcid) as alphaLipoicAcid,
sum(q10) as q10,
sum(biotin) as biotin,
sum(flavonoids) as flavonoids,
sum(improveInsulinSensitivity) as improveInsulinSensitivity,
sum(inhibitGluconeogenis) as inhibitGluconeogenis,
sum(inhibitCarbAbsorption) as inhibitCarbAbsorption,
sum(improveInsulinSecretion) as improveInsulinSecretion,
sum(improveBetaCellRegeneration) as improveBetaCellRegeneration,
sum(inhibitHunger) as inhibitHunger,
sum(inhibitGlucoseKidneyReabsorption) as inhibitGlucoseKidneyReabsorption,
sum(lactococcus) as lactococcus,
sum(lactobacillus) as lactobacillus,
sum(leuconostoc) as leuconostoc,
sum(streptococcus) as streptococcus,
sum(bifidobacterium) as bifidobacterium,
sum(saccharomyces) as saccharomyces,
sum(bacillus) as bacillus,
sum(fructose) as fructose,
-- sum(lactose) as lactose,
sum(glycemicIndex) as glycemicIndex,
sum(saturatedFat) as saturatedFat,
sum(monounsaturatedFat) as monounsaturatedFat,
sum(polyunsaturatedFat) as polyunsaturatedFat,
sum(transFat) as transFat,
sum(cholesterol) as cholesterol,
sum(histidine) as histidine,
sum(isolecuine) as isolecuine,
sum(lysine) as lysine,
sum(methionineAndCysteine) as methionineAndCysteine,
sum(phenylananineAndTyrosine) as phenylananineAndTyrosine,
sum(tryptophan) as tryptophan,
sum(threonine) as threonine,
sum(valine) as valine,
sum(vitaminA) as vitaminA,
sum(vitaminC) as vitaminC,
sum(vitaminD) as vitaminD,
sum(vitaminE) as vitaminE,
sum(vitaminK) as vitaminK,
sum(vitaminB1) as vitaminB1,
sum(vitaminB12) as vitaminB12,
sum(vitaminB2) as vitaminB2,
sum(vitaminB3) as vitaminB3,
sum(vitaminB5) as vitaminB5,
sum(vitaminB6) as vitaminB6,
sum(folate) as folate,
sum(copper) as copper,
sum(iron) as iron,
sum(zinc) as zinc,
sum(manganese) as manganese,
sum(phosphorus) as phosphorus,
sum(selenium) as selenium,
sum(omega6_3_ratio) as omega6_3_ratio,
sum(zinc_copper_ratio) as zinc_copper_ratio,
sum(pot_sod_ratio) as pot_sod_ratio,
sum(cal_mag_ratio) as cal_mag_ratio,
sum(pralAlkalinity) as pralAlkalinity,
sum(improveBloodPressure) as improveBloodPressure,
sum(improveCholesterol) as improveCholesterol,
sum(reduceWeight) as reduceWeight,
sum(improveRenalFunction) as improveRenalFunction,
sum(improveLiverFunction) as improveLiverFunction,
sum(improveThyroidFunction) as improveThyroidFunction,
sum(improveArthritis) as improveArthritis,
sum(reduceUricAcid) as reduceUricAcid,
sum(veg) as veg,
sum(nonVeg) as nonVeg,
sum(fruit) as fruit,
sum(oil) as oil,
sum(spice) as spice,
sum(grain) as grain,
sum(legume) as legume,
sum(nuts) as nuts,
sum(seeds) as seeds,
sum(inflammatoryIndex) as inflammatoryIndex,
sum(oxidativeStressIndex) as oxidativeStressIndex,
sum(gluten) as gluten,
-- sum(lactose) as lactose,
sum(allergicIndex) as allergicIndex,
sum(water) as water
from foodlogs fl inner join bi_patient_status a on (fl.clientid = a.clientid and a.status='active' and fl.mealdate between a.status_start_date and a.status_end_date)
				 left join (
									select
									foodid, foodlabel,
									CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
										 WHEN (recommendationRating <= 1) THEN 'Red'
										 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
										 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
										 WHEN (recommendationRating > 3) THEN 'Green*'
									END AS E5foodgrading,
									measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb -- level1
									,calories, cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as decimal(10,5)) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre -- level 2
									,sodium, potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids -- level 3
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  -- level 4 glycemic controllers
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus -- level 5 biota
									,fructose, lactose, glycemicIndex  -- level 6 carb
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol -- ,  medium-chain  -- level 6 fat
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine -- level 6 protein
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  -- level 6 vitamins
									-- , taurine, vanadium -- level 6 nutraceuticals  -- carnitine, inositol,
									,  copper, iron, zinc, manganese, phosphorus, selenium  -- level 6 minerals-- iodine,

									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  -- level 7 nutrient balance

									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  -- level 8 metabolic improvers

									, vegetarian as veg, case when vegetarian = 0 then 1 end as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  -- level 9 food type -- nonVeg,

									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  -- level 10 cellular stressers

									, water -- level 11 hydration

									from foods a
						) b on fl.foodid = b.foodid
group by a.clientid, mealdate

union all 

select distinct a.clientid, a.patientname, mealdate, 'DailyTarget' as category,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='net_carb') as net_carb_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='calories') as calories_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='netGlycemicCarb') as netGlycemicCarb_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='fibre') as fibre_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='fat') as fat_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='protein') as protein_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='Total_Carb') as Total_Carb_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='sodium') as sodium_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='potassium') as potassium_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='magnesium') as magnesium_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='calcium') as calcium_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='chromium') as chromium_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='omega3') as omega3_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='omega6') as omega6_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='alphaLipoicAcid') as alphaLipoicAcid_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='q10') as q10_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='biotin') as biotin_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='flavonoids') as flavonoids_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveInsulinSensitivity') as improveInsulinSensitivity_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='inhibitGluconeogenis') as inhibitGluconeogenis_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='inhibitCarbAbsorption') as inhibitCarbAbsorption_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveInsulinSecretion') as improveInsulinSecretion_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveBetaCellRegeneration') as improveBetaCellRegeneration_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='inhibitHunger') as inhibitHunger_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='inhibitGlucoseKidneyReabsorption') as inhibitGlucoseKidneyReabsorption_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='lactococcus') as lactococcus_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='lactobacillus') as lactobacillus_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='leuconostoc') as leuconostoc_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='streptococcus') as streptococcus_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='bifidobacterium') as bifidobacterium_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='saccharomyces') as saccharomyces_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='bacillus') as bacillus_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='fructose') as fructose_range,
-- (select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='lactose') as lactose_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='glycemicIndex') as glycemicIndex_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='saturatedFat') as saturatedFat_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='monounsaturatedFat') as monounsaturatedFat_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='polyunsaturatedFat') as polyunsaturatedFat_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='transFat') as transFat_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='cholesterol') as cholesterol_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='histidine') as histidine_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='isolecuine') as isolecuine_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='lysine') as lysine_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='methionineAndCysteine') as methionineAndCysteine_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='phenylananineAndTyrosine') as phenylananineAndTyrosine_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='tryptophan') as tryptophan_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='threonine') as threonine_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='valine') as valine_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminA') as vitaminA_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminC') as vitaminC_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminD') as vitaminD_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminE') as vitaminE_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminK') as vitaminK_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminB1') as vitaminB1_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminB12') as vitaminB12_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminB2') as vitaminB2_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminB3') as vitaminB3_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminB5') as vitaminB5_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='vitaminB6') as vitaminB6_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='folate') as folate_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='copper') as copper_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='iron') as iron_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='zinc') as zinc_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='manganese') as manganese_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='phosphorus') as phosphorus_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='selenium') as selenium_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='omega6_3_ratio') as omega6_3_ratio_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='zinc_copper_ratio') as zinc_copper_ratio_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='pot_sod_ratio') as pot_sod_ratio_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='cal_mag_ratio') as cal_mag_ratio_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='pralAlkalinity') as pralAlkalinity_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveBloodPressure') as improveBloodPressure_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveCholesterol') as improveCholesterol_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='reduceWeight') as reduceWeight_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveRenalFunction') as improveRenalFunction_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveLiverFunction') as improveLiverFunction_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveThyroidFunction') as improveThyroidFunction_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='improveArthritis') as improveArthritis_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='reduceUricAcid') as reduceUricAcid_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='veg') as veg_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='nonVeg') as nonVeg_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='fruit') as fruit_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='oil') as oil_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='spice') as spice_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='grain') as grain_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='legume') as legume_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='nuts') as nuts_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='seeds') as seeds_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='inflammatoryIndex') as inflammatoryIndex_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='oxidativeStressIndex') as oxidativeStressIndex_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='gluten') as gluten_range,
-- (select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='lactose') as lactose_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='allergicIndex') as allergicIndex_range,
(select concat(greenStart,'-',greenEnd) from bi_nutrition_target where foodsNutrient ='water') as water_range

 -- # concat('(select ', 'concat(', 'greenStart,','''-''',',greenEnd)',' from bi_nutrition_target where foodsNutrient =', '''',foodsNutrient,''')', ' as ', foodsNutrient,'_range,')
from foodlogs fl inner join bi_patient_status a on (fl.clientid = a.clientid and a.status='active' and fl.mealdate between a.status_start_date and a.status_end_date)
				 left join (
									select
									foodid, foodlabel,
									CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
										 WHEN (recommendationRating <= 1) THEN 'Red'
										 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
										 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
										 WHEN (recommendationRating > 3) THEN 'Green*'
									END AS E5foodgrading,
									measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb -- level1
									,calories, cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as decimal(10,5)) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre -- level 2
									,sodium, potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids -- level 3
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  -- level 4 glycemic controllers
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus -- level 5 biota
									,fructose, lactose, glycemicIndex  -- level 6 carb
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol -- ,  medium-chain  -- level 6 fat
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine -- level 6 protein
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  -- level 6 vitamins
									-- , taurine, vanadium -- level 6 nutraceuticals  -- carnitine, inositol,
									,  copper, iron, zinc, manganese, phosphorus, selenium  -- level 6 minerals-- iodine,

									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  -- level 7 nutrient balance

									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  -- level 8 metabolic improvers

									, vegetarian as veg, case when vegetarian = 0 then 1 end as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  -- level 9 food type -- nonVeg,

									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  -- level 10 cellular stressers

									, water -- level 11 hydration

									from foods a
						) b on fl.foodid = b.foodid
*/