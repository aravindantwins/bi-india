alter view v_bi_netadds as
select date, organization,
total_enrolled,
total_clinic_visited,
total_declined,
case when total_enrolled = 0 and total_clinic_visited >0 and total_declined = 0 then total_clinic_visited
	 when total_enrolled = 0 and total_clinic_visited >0 and total_declined > 0 then total_clinic_visited - total_declined 
     else 0
     end as total_wait
from 
(
select date, organization, 
sum(case when status='enrolled' then total else 0 end) as total_enrolled,
sum(case when status='clinic_visited' then total else 0 end) as total_clinic_visited,
sum(case when status='declined' then total else 0 end) as total_declined
from 
(select date, if(organization is null or organization = '','Unassigned', organization) as organization, 'enrolled' as status, 
sum(case when date((b.enrollment_date)) <>'' then 1 else 0 end) as total
from v_date_to_date a left join v_bi_netadds_x_deals b on a.date = date((b.enrollment_date))
group by date, organization 

union all 

select date, if(organization is null or organization = '','Unassigned', organization) as organization, 'clinic_visited' as status, 
sum(case when date((b.clinic_visit_date)) <>'' then 1 else 0 end) as total
from v_date_to_date a left join v_bi_netadds_x_deals b on a.date = date((b.clinic_visit_date))
group by date, organization 


union all 

select date, if(organization is null or organization = '','Unassigned', organization) as organization, 'declined' as status, 
sum(case when date((b.declined_date)) <>'' then 1 else 0 end) as total
from v_date_to_date a left join v_bi_netadds_x_deals b on a.date = date((b.declined_date))
group by date, organization 
) a
group by date, organization
) s
;