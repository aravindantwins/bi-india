-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_ABI_B2B_member_detail as 
select a.clientid,
 cgm.first_cgm_1d_date_c as cgm_1d_start_date,
 cgm.first_cgm_1d_c as cgm_1d_start_value,
 cgm.last_cgm_1d_date_c as cgm_1d_last_date,
 cgm.last_cgm_1d_c as cgm_1d_last_value,
 
 cgm.first_cgm_5d_date_c as cgm_5d_start_date,
 cgm.first_cgm_5d_c as cgm_5d_start_value,
 cgm.last_cgm_5d_date_c as cgm_5d_last_date,
 cgm.last_cgm_5d_c as cgm_5d_last_value,
 ((length(b.start_medicine_diabetes_drugs) - length(replace(b.start_medicine_diabetes_drugs, ',', ''))) + 1) as Med_start,
 c.actual_medCount as Med_current,

 case when b.start_BMI > 25 and (b.start_weight - c.last_weight_value) > 0 then (b.start_weight - c.last_weight_value) else null end as Weight_loss,

 inc.first_value_available_bloodworkDate as start_value_available_bloodworkDate, 
 inc.start_HbA1c, 
 inc.last_value_available_bloodworkDate_c as last_value_available_bloodworkDate,
 inc.last_HbA1c_c as latest_HbA1c,
 inc.start_IR,
 inc.last_IR_c as latest_IR, 
 
 b.treatmentDays,
 c.status_summary,
 a.status
from
(select clientid, patientname, status from  v_allclient_list where plancode like '%ABI_%') a
left join dim_client b on (a.clientId = b.clientid  and b.is_row_current = 'Y')
left join
( 
	select a.date, a.clientid, a.actual_medCount, a.status_summary, a.last_available_1d_weight_date as last_weight_date, b.weight_1d as last_weight_value
	from 
  	(
		select date, clientid, actual_medCount,
			case when c.is_InReversal = 'Yes' or c.is_MetOnly_by_OPS = 'yes' then 'In Reversal' else 'Yet to Reverse' end as status_summary,
			last_available_1d_weight_date
		from bi_patient_reversal_state_gmi_x_date c where (c.date= cast(ITZ(now()) as date)-interval 1 day) 
  	) a     
 	left join bi_patient_reversal_state_gmi_x_date b on a.clientid=b.clientid and b.date = a.last_available_1d_weight_date
) c on c.clientid=a.clientid 
left join
	(
			select a.clientid,
				a.first_cgm_5d_date as first_cgm_5d_date_c,
				a.last_cgm_5d_date as last_cgm_5d_date_c,
				max(case when a.first_cgm_5d_date=b.date then b.cgm_5d else null end) as first_cgm_5d_c,
				max(case when a.last_cgm_5d_date =b.date then b.cgm_5d else null end) as last_cgm_5d_c,
                a.first_cgm_1d_date as first_cgm_1d_date_c,
				a.last_cgm_1d_date as last_cgm_1d_date_c,
				max(case when a.first_cgm_1d_date=b.date then b.cgm_1d else null end) as first_cgm_1d_c,
				max(case when a.last_cgm_1d_date =b.date then b.cgm_1d else null end) as last_cgm_1d_c
        	 from
			 (
			 	select clientid, max(case when cgm_5d is not null then date else null end) as last_cgm_5d_date, 
			 	min(case when cgm_5d is not null then date else null end) as first_cgm_5d_date, 
			 	max(case when cgm_1d is not null then date else null end) as last_cgm_1d_date, 
			 	min(case when cgm_1d is not null then date else null end) as first_cgm_1d_date
				from  bi_patient_reversal_state_gmi_x_date a inner join clients b on a.clientid = b.ID 
				where plancode like '%ABI_%'
				group by clientid
			) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid=b.clientid and (b.date=a.first_cgm_5d_date or b.date=a.last_cgm_5d_date  or b.date=a.first_cgm_1d_date or b.date=a.last_cgm_1d_date)
			group by a.clientid 
	 ) cgm on cgm.clientid=a.clientid
left join
	(
		select clientid,
			max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
			max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate_c,
			max(start_IR) as start_IR,
        	max(last_IR) as last_IR_c,

			max(start_HbA1c) as start_HbA1c,  
			max(last_HbA1c) as last_HbA1c_c
	   	from
		  (
			select s.clientid,
					case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
					case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then homa2IR else null end as start_IR,

					case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then veinhba1c else null end as start_HbA1c,
					
					case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate, 
					case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then homa2IR else null end as last_IR,
					case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then veinhba1c else null end as last_HbA1c 

			from
			   (
					select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate 
					from clinictestresults a inner join clients b on a.clientid = b.ID 
					where a.deleted = 0 
					and b.planCode like '%ABI_%'
					group by clientid
				) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
		) s
		group by clientid
	) inc on inc.clientid=a.clientid