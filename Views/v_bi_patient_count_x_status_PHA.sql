-- use twins;
create view v_bi_patient_count_x_status_PHA as 
select s1.date, s1.pha, 
max(case when s1.status = 'Enrolled' then measure end) as Enrolled_cnt, 
max(case when s1.status = 'Pending_active' then measure end) as Pending_Active_Cnt,
max(case when s1.status = 'Active' then measure end) as Active_Cnt
from 
(select date, pha, c.status
from v_date_to_date a cross join (select distinct pha from bi_patient_Status) b 
					  cross join (select 'Enrolled' as status union all select 'pending_active' as status union all select 'Active' as status) c
) s1
left join 
(
	select date, pha, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, enrollmentdateitz, pha, if(pha is null, null, 'Enrolled') as status from 
		bi_patient_status b 
		where status = 'active'
        and date(enrollmentdateitz) >= '2020-07-01'
        
        union all 
        
        -- Consider any patient who is in the system with PENDING_ACTIVE status but they have completed their enrollment. 
		select distinct clientid, enrollmentdateitz, pha, if(pha is null, null, 'Enrolled') as status from 
		bi_patient_status b 
		where status = 'PENDING_ACTIVE'
        and clientid not in (select distinct clientid from bi_patient_status where status = 'active')
        and clientid not in (select clientid from v_Client_tmp where status = 'registration')
        and date(enrollmentdateitz) >= '2020-07-01'
        
	) b on date(enrollmentdateitz) <= a.date
	group by date, pha, b.status

	union all 

	select date, pha, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, enrollmentdateitz, status_start_date, status_end_date, pha, if(pha is null, null, 'pending_active') as status from 
		bi_patient_status b 
		where status = 'pending_active'
        and date(enrollmentdateitz) >= '2020-07-01'
        and clientid not in (select clientid from v_Client_tmp where status = 'registration')
	) b on a.date between b.status_start_date and b.status_end_date
	group by date, pha, b.status
    
    union all 
    
	select date, pha, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, enrollmentdateitz, status_start_date, status_end_date, pha, if(pha is null, null, 'active') as status from 
		bi_patient_status b 
		where status = 'active'
        and date(enrollmentdateitz) >= '2020-07-01'
	) b on a.date between b.status_start_date and b.status_end_date
	group by date, pha, b.status
    
) s2
on s1.date = s2.date and s1.pha = s2.pha and s1.status = s2.status 
where s1.pha is not null and s1.date >= '2020-07-01'
group by s1.date, s1.pha
;

