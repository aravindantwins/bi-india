-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_coach_detail as 
select s.clientid, patientname, s.date, doctor, coach, leadCoach, treatmentDays, age, cohortname, cgm_5d, medicine, 
(length(medicine_drugs) - length(replace(medicine_drugs,',',''))) + 1 as med_count,
medicine_drugs, 
d10_success, 
d35_success,
healthStatusCohort, 
actionScore_5d, 
latest_nps as nps, 
latest_service_rating as serviceRating, 
ChurnRisk, 
nut_adh_5d, 
'' as nut_happy, 
case when healthStatusCohort = 'Default' and (nut_adh_5d = 'No' or actionScore_5d < 85 or churnRisk = 'Y' or cgm_5d > 140) then 'Y'
	 when healthStatusCohort = 'Escalation' then 'Y'
     when healthStatusCohort not in ('Default', 'Escalation') and (nut_adh_5d = 'No' or actionScore_5d < 85 or churnRisk = 'Y' or cgm_5d > 140) then 'Y'
     end as patientRisk,
planCode,
enrollmentType,
ChurnRisk as isDienrollOpen,
total_IMTs_open,
predicted_1d_cgm,
isCBG,
total_available_foodlog_days as foodlog_days,
tac_5d,
total_good_reversal_days as reversal_days,
suspended
-- isD30ReviewCompleted
from 
(
	select a.clientid, d.patientnameShort as patientname, a.date, d.doctornameShort as doctor,d.coachnameShort as coach, d.leadCoachNameShort as leadCoach, br.treatmentDays, age, br.cohortname, d.planCode,
	case when br.cohortname = 'In reversal' then 'In Reversal'
		 when br.cohortname = 'Default' then 'Default'
		 when br.cohortname not in ('In reversal','Default') then 'Escalation' end as cohort
	,br.cgm_5d, br.medicine as medicine, replace(replace(br.medicine_drugs,'(DIAB)',''),' (DIABETES)','')  as medicine_drugs,
	case when br.treatmentDays < 35 then 'Default'
		 when br.treatmentDays >= 35 and br.is_InReversal = 'Yes' then 'In Reversal'
		 when br.treatmentDays >= 35 and br.is_MetOnly_by_OPS = 'Yes' then 'In Reversal + M'
		 when br.treatmentDays >= 35 and br.cohortname not in ('In Reversal', 'Default') then 'Escalation' end as healthStatusCohort,
	br.as_5d as actionScore_5d,
	case when br.nut_adh_syntax_5d >= 0.8 then 'Yes' else 'No' end as nut_adh_5d,
	d1.d10_success, 
	d1.d35_success,
	bm.total_disenroll_incidents as open_disenrollment_incidents,
	case when bm.total_disenroll_incidents > 0 then 'Y' else 'N' end as ChurnRisk,
    bm.total_IMTs_open,
    br.total_available_foodlog_days,
    br.total_good_reversal_days,
    br.predicted_1d_cgm,
	case when d1.isM1 = 'Yes' then 'M1'														
		 when d1.isM2Converted = 'Yes' then 'M1-M2'													
		 else 'M2' end as enrollmentType,
	br.isCBG,
    br.tac_5d,
    if(ca.suspended is true, 'Yes', '') as suspended
    -- if(pr.clientid is not null, 'Yes', 'No') as isD30ReviewCompleted
	from
	(
		select a.date, b.clientId
		from twins.v_date_to_date a
		inner join
		(
			select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
			from
			(
			select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
			from bi_patient_status
			where status = 'active'
			group by clientid, date(visitdateitz)
			) s
		) b on a.date between b.startDate and b.endDate
		where a.date >= date_sub(date(itz(now())), interval 30 day) and a.date <= date_sub(date(itz(now())), interval 1 day)
	) a inner join v_client_tmp d on a.clientid = d.clientid
	left join dim_client d1 on a.clientid = d1.clientid and d1.is_row_current = 'y'
	left join bi_patient_reversal_state_gmi_x_date br on a.clientid = br.clientid and a.date = br.date
	left join (
					select clientid, 
                    count(distinct case when category = 'disenrollment' then incidentid else null end) as total_disenroll_incidents, 
                    count(distinct case when category not in ('APPROVAL') then incidentId else null end) as total_IMTs_open 
                    from bi_incident_mgmt
					where status = 'OPEN'
					group by clientid
			  ) bm on a.clientid = bm.clientid
	-- left join (select distinct clientid from calllogs where calllogType = 'PROGRESS_REPORT_D30') pr on a.clientid = pr.clientid
	left join clientauxfields ca on a.clientid = ca.id
	) s left join bi_patient_latest_interview_x_date inte on s.clientid = inte.clientid and s.date = inte.date
;


/*

select s.clientid, patientname, s.date, doctor, coach, leadCoach, treatmentDays, age, cohortname, cgm_5d, medicine, 
(length(medicine_drugs) - length(replace(medicine_drugs,',',''))) + 1 as med_count,
medicine_drugs, 
d10_success, 
d35_success,
healthStatusCohort, 
actionScore_5d, 
latest_nps as nps, 
latest_service_rating as serviceRating, 
ChurnRisk, 
nut_adh_5d, 
nut_happy, 
case when healthStatusCohort = 'Default' and (nut_adh_5d = 'No' or actionScore_5d < 80 or churnRisk = 'Y' or cgm_5d > 140) then 'Y'
	 when healthStatusCohort = 'Escalation' then 'Y'
     when healthStatusCohort not in ('Default', 'Escalation') and (nut_adh_5d = 'No' or actionScore_5d < 80 or churnRisk = 'Y' or cgm_5d > 140) then 'Y'
     end as patientRisk	 
from 
(
	select a.clientid, d.patientnameShort as patientname, a.date, d.doctornameShort as doctor,d.coachnameShort as coach, d.leadCoachNameShort as leadCoach, br.treatmentDays, age, br.cohortname,
	case when br.cohortname = 'In reversal' then 'In Reversal'
		 when br.cohortname = 'Default' then 'Default'
		 when br.cohortname not in ('In reversal','Default') then 'Escalation' end as cohort
	,br.cgm_5d, br.medicine as medicine, replace(replace(br.medicine_drugs,'(DIAB)',''),' (DIABETES)','')  as medicine_drugs,
	case when br.treatmentDays < 35 then 'Default'
		 when br.treatmentDays >= 35 and br.is_InReversal = 'Yes' then 'In Reversal'
		 when br.treatmentDays >= 35 and br.is_MetOnly_by_OPS = 'Yes' then 'In Reversal + M'
		 when br.treatmentDays >= 35 and br.cohortname not in ('In Reversal', 'Default') then 'Escalation' end as healthStatusCohort,
	br.as_5d as actionScore_5d,
	-- inte.nps, inte.recent_rating as serviceRating, 
	case when br.nut_adh_syntax_5d >= 0.8 then 'Yes' else 'No' end as nut_adh_5d,
	bh.nut_happy,
	d1.d10_success, 
	d1.d35_success,
	bm.total_incidents as open_disenrollment_incidents,
	case when bm.total_incidents > 0 then 'Y' else 'N' end as ChurnRisk
	from
	(
		select a.date, b.clientId
		from twins.v_date_to_date a
		inner join
		(
			select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
			from
			(
			select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
			from bi_patient_status
			where status = 'active'
			group by clientid, date(visitdateitz)
			) s
		) b on a.date between b.startDate and b.endDate
		where a.date >= date_sub(date(itz(now())), interval 30 day) and a.date <= date_sub(date(itz(now())), interval 1 day)
	) a inner join v_client_tmp d on a.clientid = d.clientid
	left join dim_client d1 on a.clientid = d1.clientid and d1.is_row_current = 'y'
	left join bi_patient_reversal_state_gmi_x_date br on a.clientid = br.clientid and a.date = br.date
	left join (
					select clientid, count(distinct incidentid) as total_incidents from bi_incident_mgmt
					where category = 'DISENROLLMENT' and status = 'OPEN'
					group by clientid
			  ) bm on a.clientid = bm.clientid
	left join bi_patient_nut_happiness_x_date bh on a.clientid = bh.clientid and a.date = bh.date
) s left join bi_patient_latest_interview_x_date inte on s.clientid = inte.clientid and s.date = inte.date
;
*/