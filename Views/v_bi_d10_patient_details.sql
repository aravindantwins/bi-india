alter view v_bi_d10_patient_details as
select clientid, patientname, doctorname, coachname, current_status, enrollmentdate, visitdateitz as visitdate, treatmentsetupdateITZ as treatmentsetupdate, day10 as day11, 
cgm_5dg, NC_5d, E5_5d, Erating_5d, Drating_5d, nut_com, medicine, total_meds, success_ind, 
Ketone_5d, tac_5d, AS_5d, cast(MH_syms as unsigned) as MH_syms, nut_sat, nut_adh_5d, success_ind_NEW,  
case when success_ind = 'Yes' then null 
	 when success_ind = 'No' and NC_5d < 46 and tac_5d >= 2.5 and (e5_5d = 1 or ketone_5d >= 1) then 'MED'
     when success_ind = 'No' and NC_5d > 46 and tac_5d >= 2.5 and e5_5d < 1 then 'NUT'
     else 'NUT,MED' end as fail_category
from 
(    
    select clientid, patientname, doctorname, coachname, current_status, enrollmentdate, visitdateitz, treatmentsetupdateITZ, day10, 
    cgm_5dg, tac_5d, AS_5d, NC_5d, E5_5d, Ketone_5d, Erating_5d, Drating_5d, MH_syms, nut_com, medicine, total_meds, ifnull(nut_sat,'ND') as nut_sat, nut_adh_5d,
	if ( (ifnull(cgm_5dg,0) <= 125
			and ifnull(tac_5d,0) >= 2.5 
			-- and ifnull(AS_5d,0) >= 90 
			and (if(ifnull(NC_5d,0) < 46, 'Yes', if(ifnull(E5_5d,0) = 1 or ifnull(Ketone_5d,0) >= 1, 'Yes', 'No')) = 'Yes' ) -- or (ifnull(E5_5d,0) >= .90 and ifnull(Ketone_5d,0) >= 1)) 
		 ), 'Yes', 'No') as success_ind
         ,
	if ( (if(nut_adh_5d = 'YES', 'YES', if(E5_5d >= .90, 'YES','NO')) = 'YES' and ifnull(tac_5d,0) >= 2.5 and ifnull(nut_sat,4) >= 4 and (if(AS_5d >= 85, 'Yes', if(AS_5d < 85 and total_incidents >= 1, 'Yes','No')) = 'Yes')) or cohortname = 'In Reversal' , 'Yes','No') as success_ind_NEW -- Happiness to be replaced by Nutrition Satification interview score
    from 
	(
	select a.clientid, a.patientname, a.doctorname, a.coachname, current_status, a.enrollmentdateitz as enrollmentdate, a.visitdateitz, a.treatmentsetupdateITZ, day10, 
	b.average_measure as cgm_5dg, b1.average_measure as tac_5d, b2.average_measure as AS_5d, b3.average_measure as NC_5d, 
	b4.average_measure as E5_5d, b5.average_measure as Ketone_5d, b6.average_measure as Erating_5d, b7.average_measure as Drating_5d, -- b8.measureValue as MH_syms,
    bm0.measure as MH_syms,
    bm1.measure as nut_com, bm2.measure as medicine, (length(bm2.measure) - length(replace(bm2.measure, ',', ''))) + 1 as total_meds,
    pa.nutritionSatisfaction as nut_sat, 
    case when bm3.measure >= 0.8 then 'YES' else 'NO' end as nut_adh_5d, 
    ac.cohortname,count(distinct imt.incidentid) as total_incidents
	from 
		(
				select distinct b.clientid, c.doctornameshort as doctorname, c.patientnameshort as patientname, c.coachnameshort as coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, c.status as current_status,
				-- date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11
				-- date(date_add(visitdateITZ, interval 10 day)) as day10
                date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 10 day)) as day10
                from
				(
					select clientid, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
					max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
					from bi_patient_status b
					group by clientid, status
				) b left join v_Client_tmp c on b.clientid = c.clientid
				where b.status = 'active' 
				and datediff(status_end_date, if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ))) >= 10
				-- and date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day))  >= '2019-06-19' 
				-- and date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day))  <= date(itz(now()))
				and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 10 day)) >= '2019-06-19'
				and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 10 day)) <= date(itz(now()))
		 ) a left join bi_measures b on a.clientid = b.clientid and b.measure_event_date = a.day10 and b.measure_type = '5d' and b.measure_name in ('CGM')
		 left join bi_measures b1 on a.clientid = b1.clientid and b1.measure_event_date = a.day10 and b1.measure_type = '5d' and b1.measure_name in ('TAC')
		 left join bi_measures b2 on a.clientid = b2.clientid and b2.measure_event_date = a.day10 and b2.measure_type = '5d' and b2.measure_name in ('Action Score')
		 left join bi_measures b3 on a.clientid = b3.clientid and b3.measure_event_date = a.day10 and b3.measure_type = '5d' and b3.measure_name in ('Netcarb')
		 left join bi_measures b4 on a.clientid = b4.clientid and b4.measure_event_date = a.day10 and b4.measure_type = '5d' and b4.measure_name in ('E5%')
		 left join bi_measures b5 on a.clientid = b5.clientid and b5.measure_event_date = a.day10 and b5.measure_type = '5d' and b5.measure_name in ('Ketone')
         left join bi_measures b6 on a.clientid = b6.clientid and b6.measure_event_date = a.day10 and b6.measure_type = '5d' and b6.measure_name in ('Star Rating')
         left join bi_measures b7 on a.clientid = b7.clientid and b7.measure_event_date = a.day10 and b7.measure_type = '5d' and b7.measure_name in ('D-rating')
         left join bi_patient_monitor_measures bm0 on a.clientid = bm0.clientid and bm0.measure_event_date = a.day10 and bm0.measure_name = 'Symptoms-M/H'
     /*    (
					select clientId, eventDateITZ, sum(if(measureValue is not null and measureValue >1, 1, 0)) as measureValue
					from selfreportdaily
					where measureType not in ('ENERGY', 'MOOD', 'COACH')
					group by clientId, eventDateITZ
					) b8 on a.clientid = b8.clientid and b8.eventDateITZ = a.day11
	 */
		 left join bi_patient_monitor_measures bm1 on a.clientid = bm1.clientid and bm1.measure_event_date = a.day10 and bm1.measure_name in ('NUT COM')
         left join bi_patient_monitor_measures bm2 on a.clientid = bm2.clientid and bm2.measure_event_date = a.day10 and bm2.measure_name in ('Medicine')
         left join bi_patient_monitor_measures bm3 on a.clientid = bm3.clientid and bm3.measure_event_date = a.day10 and bm3.measure_name in ('NUT_ADH_5d')
         left join (select patientId as clientid, nutritionSatisfaction from bi_patient_interview_response where enrolledDay = 'D10') pa on a.clientid = pa.clientid
		 left join (select distinct incidentid, clientid, status, date(report_time) as report_date from bi_incident_mgmt where category = 'sensors') imt on a.clientid = imt.clientid and report_date <= a.day10
         left join v_all_patient_cohort ac on a.clientid = ac.clientid and ac.status_date = a.day10
         group by a.clientid
	) s
) s1
;


