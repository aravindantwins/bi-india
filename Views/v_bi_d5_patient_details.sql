create view v_bi_d5_patient_details
as
select clientid, patientname, a.doctorname, coachname, enrollmentdate, visitdate, treatmentsetupdate, day5, cgm_5dg, tac_5d, AS_5d, NC_5d, E5_5d, Ketone_5d, success_ind,
fail_category from 
(select distinct doctorname_Short as doctorname from bi_patient_status where status = 'active') a 
left join 
(
select clientid, patientname, doctorname, coachname, enrollmentdate, visitdate, treatmentsetupdate, day5, cgm_5dg, tac_5d, AS_5d, NC_5d, E5_5d, Ketone_5d, success_ind, 
case when success_ind = 'Yes' then null 
	 when success_ind = 'No' and NC_5d < 46 and tac_5d >= 2.5 and (e5_5d = 1 or ketone_5d >= 1) then 'MED'
     when success_ind = 'No' and NC_5d > 46 and tac_5d >= 2.5 and e5_5d < 1 then 'NUT'
     else 'NUT,MED' end as fail_category
from 
(    
	select clientid, patientname, doctorname, coachname, enrollmentdate, visitdate, treatmentsetupdate, day5, cgm_5dg, tac_5d, AS_5d, NC_5d, E5_5d, Ketone_5d,
	if (ifnull(cgm_5dg,0) < 130, 'Yes', 'No') as success_ind
	from 
	(
	select a.clientid, cp.patientnameshort as patientname, cp.doctornameshort as doctorname, cp.coachnameshort as coachname, a.enrollmentdateitz as enrollmentdate, visitdateitz as visitdate, treatmentsetupdateITZ as treatmentsetupdate, day5, 
	b.average_measure as cgm_5dg, b1.average_measure as tac_5d, b2.average_measure as AS_5d, b3.average_measure as NC_5d, 
	b4.average_measure as E5_5d, b5.average_measure as Ketone_5d
	from 
		(
			select distinct b.clientid, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
            date(date_add(ifnull(if(treatmentsetupdateITZ > visitdateITZ, treatmentsetupdateITZ, visitdateITZ), b.enrollmentdateitz), interval 5 day)) as day5
			from
			bi_patient_status b 
			where b.status = 'active' 
			and datediff(status_end_date, status_Start_date) >= 5
			and date(date_add(ifnull(if(treatmentsetupdateITZ > visitdateITZ, treatmentsetupdateITZ, visitdateITZ), b.enrollmentdateitz), interval 5 day))  >= '2019-06-19' 
			and date(date_add(ifnull(if(treatmentsetupdateITZ > visitdateITZ, treatmentsetupdateITZ, visitdateITZ), b.enrollmentdateitz), interval 5 day)) <= date(itz(now()))
		 ) a  left join v_client_tmp cp on a.clientid = cp.clientid         
         left join bi_measures b on a.clientid = b.clientid and b.measure_event_date = a.day5 and b.measure_type = '5d' and b.measure_name in ('CGM')
		 left join bi_measures b1 on a.clientid = b1.clientid and b1.measure_event_date = a.day5 and b1.measure_type = '5d' and b1.measure_name in ('TAC')
		 left join bi_measures b2 on a.clientid = b2.clientid and b2.measure_event_date = a.day5 and b2.measure_type = '5d' and b2.measure_name in ('Action Score')
		 left join bi_measures b3 on a.clientid = b3.clientid and b3.measure_event_date = a.day5 and b3.measure_type = '5d' and b3.measure_name in ('Netcarb')
		 left join bi_measures b4 on a.clientid = b4.clientid and b4.measure_event_date = a.day5 and b4.measure_type = '5d' and b4.measure_name in ('E5%')
		 left join bi_measures b5 on a.clientid = b5.clientid and b5.measure_event_date = a.day5 and b5.measure_type = '5d' and b5.measure_name in ('Ketone')
	) s
) s1
) b on a.doctorname = b.doctorname
;