SET collation_connection = 'latin1_swedish_ci';

alter  VIEW v_bi_patient_reversal_management_summary AS 
select date, coach, leadCoach,
count(distinct case when treatmentdays > 35 and isTTP = 'No' then a.clientid else null end) as total_35D,
count(distinct case when treatmentdays > 35 and isTTP = 'No' and isUnqualified = 'Yes' then a.clientid else null end) as total_35D_Unqualified,
count(distinct case when treatmentdays > 35 and isTTP = 'No' and ((is_medical_exclusion = 'No' and is_Excluded_timetracked = 'No') or (isMedExclusion_AST_ALT_BMI = 'Yes') or (isMedExclusion_Age = 'Yes')) and isNABrittle = 'Yes' then a.clientid else null end) as total_35D_NABrittle, -- ignore medical exclusion AND show na brittle ONLY FOR those chosen

count(distinct case when treatmentdays > 35 and isTTP = 'No' and is_type1like = 'Yes' then a.clientid else null end) as total_35D_type1Like,
count(distinct case when treatmentdays > 35 and isTTP = 'No' and (is_medical_exclusion = 'Yes' or is_Excluded_timetracked = 'Yes') and isMedExclusion_AST_ALT_BMI = 'No' and isMedExclusion_Age = 'No' and is_type1like = 'No' then a.clientid else null end) as total_35D_medExclusion, -- ignore type1like also from medical exclusion because dashboard shows the type1like separately.
count(distinct case when treatmentdays > 35 and isTTP = 'No' and (isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') then a.clientid else null end) as total_35D_medExclusion_to_Include,


count(distinct case when treatmentdays > 35 and isTTP = 'No' and isNoCGM_14days = 'Yes' then a.clientid else null end) as total_35D_NoCGM_14days,
count(distinct case when treatmentdays > 35 and isTTP = 'No' and daysFromLast5d > 98 then a.clientid else null end) as total_35D_NoCGM_98days, 
count(distinct case when treatmentdays > 35 and isTTP = 'No' and daysFromLast5d > 120 then a.clientid else null end) as total_35D_NoCGM_120days,
count(distinct case when treatmentdays > 35 and isTTP = 'No' and daysFromLast5d > 150 then a.clientid else null end) as total_35D_NoCGM_150days,


count(distinct case when treatmentdays > 35 and isTTP = 'No' and is_app_suspended = 'Yes' then a.clientid else null end) as total_35Days_M2_suspended, 
count(distinct case when (treatmentdays between 35 and 50) and isTTP = 'No' and isM1_Converted = 'Yes' then a.clientid else null end) as total_M1toM2_less_50days,


-- D36 brand new inflow
count(distinct case when isM1_Converted = 'Yes' and treatmentdays = 36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then a.clientid 
					when isM1_Converted = 'No' and treatmentdays =36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then a.clientid else null end) as total_new_inflow,
						
count(distinct case when (isM1_Converted = 'Yes' and treatmentdays =36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes')) ) then a.clientid 
					when (isM1_Converted = 'No' and treatmentdays = 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal = 'Yes' or  is_MetOnly_by_OPS = 'Yes') )) then a.clientid  else null end) as total_new_inflow_inReversal, 

					
-- Members > 36 days in the program and turned reversal on this date but on escalation day before
count(distinct case when (isM1_Converted = 'Yes' and treatmentdays > 36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='No' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes'))) then a.clientid 
					when (isM1_Converted = 'No' and treatmentdays > 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='No' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes'))) then a.clientid  else null end) as total_D35above_inEscalation_dayBefore_commercial, 

-- Members > 36 days in the program and turned escalation on this date but on reversal day before					
count(distinct case when (isM1_Converted = 'Yes' and treatmentdays > 36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No'))) then a.clientid 
					when (isM1_Converted = 'No' and treatmentdays > 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No'))) then a.clientid  else null end) as total_D35above_InReversal_dayBefore_turned_escalation, 

-- Members > 36 days in the program and turned escalation on this date but on reversal day before but only CGM increased above 140 to make this shift					
count(distinct case when (isM1_Converted = 'Yes' and treatmentdays > 36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') and medicine_drugs is null and cgm_5d > 140)) then a.clientid 
					when (isM1_Converted = 'No' and treatmentdays > 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') and medicine_drugs is null and cgm_5d > 140)) then a.clientid  else null end) as total_D35above_InReversal_dayBefore_turned_escalation_CGMonly, 

/*							
		count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No' then a.clientid 
							when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No' then a.clientid else null end) as total_35D_base,
								
		count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No' and is_InReversal = 'Yes' then a.clientid 
							when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No' and is_InReversal = 'Yes' then a.clientid else null end) as total_35D_base_InReversal, 
		
		count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No'  and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No'  and is_InReversal is null and is_MetOnly_by_OPS = 'Yes' then a.clientid 
							when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No' and is_InReversal is null and is_MetOnly_by_OPS = 'Yes' then a.clientid else null end) as total_35D_base_InReversal_WMet,
*/						

-- Commercial numbers
count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then a.clientid 
					when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then a.clientid else null end) as total_35D_base_commercial,
						
count(distinct case when (isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal = 'Yes' and is_MetOnly_by_OPS = 'No') or is_CBGtoCGM_to_include = 'Yes')) then a.clientid 
					when (isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal = 'Yes' and is_MetOnly_by_OPS = 'No') or is_CBGtoCGM_to_include = 'Yes')) then a.clientid  else null end) as total_35D_base_InReversal_commercial, 

count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (is_InReversal is null and is_MetOnly_by_OPS = 'Yes') and is_CBGtoCGM_to_include = 'No' then a.clientid 
					when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (is_InReversal is null and is_MetOnly_by_OPS = 'Yes') and is_CBGtoCGM_to_include = 'No' then a.clientid   else null end) as total_35D_base_InReversal_WMet_commercial,

/* commercial version before CBG-CGM Grace period logic was implemented
	count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and isExcluded = 'No' and is_InReversal = 'Yes' then a.clientid 
						when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and isExcluded = 'No' and is_InReversal = 'Yes' then a.clientid else null end)  as total_35D_base_InReversal_commercial, 
	 
	count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and isExcluded = 'No'  and is_InReversal is null and is_MetOnly_by_OPS = 'Yes' then a.clientid 
						when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and isExcluded = 'No' and is_InReversal is null and is_MetOnly_by_OPS = 'Yes' then a.clientid else null end) as total_35D_base_InReversal_WMet_commercial,
*/
					
count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and is_CBGtoCGM_to_include = 'yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') then clientid 
					when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and is_CBGtoCGM_to_include = 'yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') then clientid else null end) as total_Excluded_due_to_cbgToCgmGrace_period,

-- Members who are discharged the next day
count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' then a.clientid 
					when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' then a.clientid else null end) as total_35D_discharged_nextDay,

-- Members who are discharged the next day but was inReversal			
count(distinct case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' or is_CBGtoCGM_to_include = 'Yes') then a.clientid 
					when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' or is_CBGtoCGM_to_include = 'Yes') then a.clientid  else null end) as total_35D_discharged_nextDay_With_ReversalState
	
					
from  
(
				select distinct a.clientid, coach, ct.leadCoachNameShort as leadCoach, a.treatmentDays, a.date, datediff(a.date, b.last_available_cgm_5d_date) as daysFromLast5d,datediff(a.date, ttp.conversion_payment_date) as daysFromConversionDate, 
				b.is_InReversal, 
				b.is_MetOnly_by_OPS,
				b.isNoCGM_14days,
				b.medicine_drugs, cgm_5d,
				if((ttp.is_patient_currently_TTP = 'yes' or (ttp.is_patient_currently_TTP is null and ttp.is_patient_converted is null)) and ttp.clientid is not null, 'Yes', 'No') as isTTP,
				if(ttp.is_patient_converted = 'yes' and ttp.clientid is not null, 'Yes', 'No') as isM1_Converted,    
			    -- cx.suspended,
				if(ifnull(b.start_medCount,0) > 0 or b.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
			    if(ct.labels like '%unqualified%', 'Yes', 'No') as isUnqualified,
			 	if(ct.labels like '%exclude%', 'Yes', 'No') as isExcluded,
				if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
			    b.nonAdh_brittle_category,
				ifnull(dc.is_type1like, 'No') as is_type1like,
			    dc.is_medical_exclusion,
			    case when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal = 'Yes' or b.is_MetOnly_by_OPS = 'Yes') then 'Yes' 
					 when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal is null and b.is_MetOnly_by_OPS = 'No') then 'No' 
			         else null end as isMedExclusion_to_include,
			         
			    case when dc.is_medical_exclusion = 'Yes' and ifnull(dc.is_type1like,'No') = 'No' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' -- we need to check type1like here because due to the change in type1like prediction logic, a member can first be excluded under something but can be tagged as type1like later. 
			         else 'No' end as isMedExclusion_AST_ALT_BMI,
			         
			    case when dc.is_medical_exclusion = 'Yes' and ifnull(dc.is_type1like,'No') = 'No' and medical_exclusion_cohort in ('Age') then 'Yes' 
			         else 'No' end as isMedExclusion_Age,
				if(at.clientid is not null, 'Yes', 'No') as is_app_suspended,
				if(cb.clientid is not null, 'Yes', 'No') as is_CBGtoCGM_to_include ,
				ifnull(b.isExcludeLabelOn, 'No') as is_Excluded_timetracked,
				
				if((LAG(is_InReversal,1) over (partition by clientid order by date) = 'Yes') or (LAG(is_MetOnly_by_OPS,1) over (partition by clientid order by date) = 'Yes'), 'Yes', 'No') as isInReversal_previous_day,
				
				if(dis.clientid is not null, 'Yes','No') isMemberDischargedNextDay
				from 
				(
					select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
					from twins.v_date_to_date a
					inner join
					(
						select clientid, coach, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
						from
						(
							select clientid, coachname_short as coach, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
							from bi_patient_status
							where status = 'active'
							group by clientid, date(visitdateitz)
						) s
					) b on a.date between b.startDate and b.endDate
					where a.date >= date_sub(date(itz(now())), interval 45 day) and a.date <= date(itz(now()))
				) a left join (select clientid, date, is_InReversal, is_MetOnly_by_OPS, last_available_cgm_5d_date,isNoCGM_14days, start_medcount, start_labA1c, nonAdh_brittle_category,isExcludeLabelOn, medicine_drugs, cgm_5d from bi_patient_reversal_state_gmi_x_date where date >= curdate() - interval 50 day) b on a.clientid = b.clientid and a.date = b.date 
					left join bi_ttp_enrollments ttp on a.clientid = ttp.clientid
			        -- left join clientauxfields cx on a.clientid = cx.id
			        left join v_client_tmp ct on a.clientid = ct.clientid 
			        left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
					left join bi_patient_suspension_tracker at on a.clientid = at.clientid and a.date between at.startdate and at.endDate and at.status = 1
			        left join (
			        				select a.clientid, a.date as cbgEndDate, date_add(a.date, interval 1 day) as day1_after_cbg_is_over, date_add(a.date, interval 7 day) as day7_after_cbg_is_over				
									from 						
									(						
										select distinct b.clientid,date 					
										from v_date_to_date a inner join predictedcgmcycles b on a.date = b.enddate					
															  inner join v_client_tmp c on b.clientid = c.clientid
										where a.date >= date_sub(date(itz(now())), interval 45 day)
			                            and datediff(b.enddate, b.startdate) >= 50
									) a  	
			        			/* this code will give once the member is out of CBG what was the first 5dg available and when, whether the member was on escalation or not. 
								select s.clientid, s.cbgEndDate, date_add(s.cbgenddate, interval 1 day) as day1_after_cbg_is_over, date_add(s.cbgenddate, interval 7 day) as day7_after_cbg_is_over, 
			                    s.dayFirst5dg_available, datediff(dayFirst5dg_available, cbgEndDate ) as dayFirst5dg_available_after_CBGisOver, 							
								s1.cgm_5d, -- s1.is_inReversal, s1.is_metonly_by_ops, 							
								case when is_InReversal is null and is_MetOnly_by_OPS = 'No' then 'Yes'							
									 when is_InReversal is null and is_MetOnly_by_OPS is null then 'NoCGM5d_yet'						
									 when is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' then 'inReversal'
									 else 'No' end is_Escalation_finder						
								from 							
								(							
									select a.clientid, a.date as cbgEndDate, min(case when cgm_5d is not null then b.date else null end) as dayFirst5dg_available						
									from 						
									(						
										select distinct b.clientid,date 					
										from v_date_to_date a inner join predictedcgmcycles b on a.date = b.enddate					
															  inner join v_client_tmp c on b.clientid = c.clientid
										where a.date >= date_sub(date(itz(now())), interval 45 day)
			                            and datediff(b.enddate, b.startdate) >= 50
									) a  						
									 left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date > a.date						
									 group by a.clientid, a.date						
								 ) s left join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.dayFirst5dg_available = s1.date 	
								 */				
								) cb on a.clientid = cb.clientid and a.date between cb.day1_after_cbg_is_over and cb.day7_after_cbg_is_over      
								
					left join ( 
							  		select clientid,dischargedDate,date_sub(dischargedDate, interval 1 day) as lastActiveStatusDay
									from 
									(					
										select clientid, max(Status_Start_Date) as dischargedDate,coachname_short as coach
										from bi_patient_status a
										where status = 'discharged'
										and exists (select 1 from bi_patient_Status b where a.clientid = b.clientid and b.status ='active')
										group by clientid
									) s 
									where s.dischargedDate >= curdate() - interval 50 day
					 		  ) dis on a.clientid = dis.clientid and a.date = dis.lastActiveStatusDay
			        where ct.patientname not like '%obsolete%'
) a 
where date >= curdate() - interval 40 day
group by date, coach, leadCoach
;


/* DETAIL CODE
drop temporary table if exists tmp_memberlist_for_analysis; 

create temporary table tmp_memberlist_for_analysis as 

select s.*,

case when (isM1_Converted = 'Yes' and treatmentdays > 36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='No' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes'))) then 'Yes' 
					when (isM1_Converted = 'No' and treatmentdays > 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='No' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes'))) then 'Yes' else null end as notInReversal_dayBefore, 

-- Members > 36 days in the program and turned escalation on this date but on reversal day before					
case when (isM1_Converted = 'Yes' and treatmentdays > 36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No'))) then 'Yes' 
					when (isM1_Converted = 'No' and treatmentdays > 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No'))) then 'Yes'  else null end as InReversal_dayBefore_but_turned_escalation, 

-- Members > 36 days in the program and turned escalation on this date but on reversal day before but only CGM increased above 140 to make this shift					
case when (isM1_Converted = 'Yes' and treatmentdays > 36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') and medicine_drugs is null and cgm_5d > 140)) then 'Yes'
					when (isM1_Converted = 'No' and treatmentdays > 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (isInReversal_previous_day='Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') and medicine_drugs is null and cgm_5d > 140)) then 'Yes' else null end as InReversal_dayBefore_but_turned_escalation_today_relapsed, 
					
					
-- Members who are discharged the next day
case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' then 'Yes'
					when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' then 'Yes' else null end as isDischarged,

-- Members who are discharged the next day but was inReversal			
case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' or is_CBGtoCGM_to_include = 'Yes') then 'Yes' 
					when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and isMemberDischargedNextDay = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' or is_CBGtoCGM_to_include = 'Yes') then 'Yes'  else null end as discharged_reversalState 
	
					
from 
(
		select distinct a.clientid, coach, ct.leadCoachNameShort as leadCoach, a.treatmentDays, a.date, datediff(a.date, b.last_available_cgm_5d_date) as daysFromLast5d,datediff(a.date, ttp.conversion_payment_date) as daysFromConversionDate, 
				b.is_InReversal, 
				b.is_MetOnly_by_OPS,
				b.isNoCGM_14days,
				b.medicine_drugs, cgm_5d,
				if((ttp.is_patient_currently_TTP = 'yes' or (ttp.is_patient_currently_TTP is null and ttp.is_patient_converted is null)) and ttp.clientid is not null, 'Yes', 'No') as isTTP,
				if(ttp.is_patient_converted = 'yes' and ttp.clientid is not null, 'Yes', 'No') as isM1_Converted,    
			    -- cx.suspended,
				if(ifnull(b.start_medCount,0) > 0 or b.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
			    if(ct.labels like '%unqualified%', 'Yes', 'No') as isUnqualified,
			 	if(ct.labels like '%exclude%', 'Yes', 'No') as isExcluded,
				if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
			    b.nonAdh_brittle_category,
				ifnull(dc.is_type1like, 'No') as is_type1like,
			    dc.is_medical_exclusion,
			    case when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal = 'Yes' or b.is_MetOnly_by_OPS = 'Yes') then 'Yes' 
					 when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal is null and b.is_MetOnly_by_OPS = 'No') then 'No' 
			         else null end as isMedExclusion_to_include,
			         
			    case when dc.is_medical_exclusion = 'Yes' and ifnull(dc.is_type1like,'No') = 'No' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' -- we need to check type1like here because due to the change in type1like prediction logic, a member can first be excluded under something but can be tagged as type1like later. 
			         else 'No' end as isMedExclusion_AST_ALT_BMI,
			         
			    case when dc.is_medical_exclusion = 'Yes' and ifnull(dc.is_type1like,'No') = 'No' and medical_exclusion_cohort in ('Age') then 'Yes' 
			         else 'No' end as isMedExclusion_Age,
				if(at.clientid is not null, 'Yes', 'No') as is_app_suspended,
				if(cb.clientid is not null, 'Yes', 'No') as is_CBGtoCGM_to_include ,
				ifnull(b.isExcludeLabelOn, 'No') as is_Excluded_timetracked,
				
				if((LAG(is_InReversal,1) over (partition by clientid order by date) = 'Yes') or (LAG(is_MetOnly_by_OPS,1) over (partition by clientid order by date) = 'Yes'), 'Yes', 'No') as isInReversal_previous_day,
				
				if(dis.clientid is not null, 'Yes','No') isMemberDischargedNextDay
				from 
				(
					select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
					from twins.v_date_to_date a
					inner join
					(
						select clientid, coach, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
						from
						(
							select clientid, coachname_short as coach, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
							from bi_patient_status
							where status = 'active'
							group by clientid, date(visitdateitz)
						) s
					) b on a.date between b.startDate and b.endDate
					where a.date >= date_sub(date(itz(now())), interval 10 day) and a.date <= date(itz(now()))
				) a left join (select clientid, date, is_InReversal, is_MetOnly_by_OPS, last_available_cgm_5d_date,isNoCGM_14days, start_medcount, start_labA1c, nonAdh_brittle_category,isExcludeLabelOn, medicine_drugs, cgm_5d from bi_patient_reversal_state_gmi_x_date where date >= curdate() - interval 50 day) b on a.clientid = b.clientid and a.date = b.date 
					left join bi_ttp_enrollments ttp on a.clientid = ttp.clientid
			        -- left join clientauxfields cx on a.clientid = cx.id
			        left join v_client_tmp ct on a.clientid = ct.clientid 
			        left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
					left join bi_patient_suspension_tracker at on a.clientid = at.clientid and a.date between at.startdate and at.endDate and at.status = 1
			        left join (
			        				select a.clientid, a.date as cbgEndDate, date_add(a.date, interval 1 day) as day1_after_cbg_is_over, date_add(a.date, interval 7 day) as day7_after_cbg_is_over				
									from 						
									(						
										select distinct b.clientid,date 					
										from v_date_to_date a inner join predictedcgmcycles b on a.date = b.enddate					
															  inner join v_client_tmp c on b.clientid = c.clientid
										where a.date >= date_sub(date(itz(now())), interval 45 day)
			                            and datediff(b.enddate, b.startdate) >= 50
									) a  	
								) cb on a.clientid = cb.clientid and a.date between cb.day1_after_cbg_is_over and cb.day7_after_cbg_is_over      
								
					left join ( 
							  		select clientid,dischargedDate,date_sub(dischargedDate, interval 1 day) as lastActiveStatusDay
									from 
									(					
										select clientid, max(Status_Start_Date) as dischargedDate,coachname_short as coach
										from bi_patient_status a
										where status = 'discharged'
										and exists (select 1 from bi_patient_Status b where a.clientid = b.clientid and b.status ='active')
										group by clientid
									) s 
									where s.dischargedDate >= curdate() - interval 50 day
					 		  ) dis on a.clientid = dis.clientid and a.date = dis.lastActiveStatusDay
			        where ct.patientname not like '%obsolete%'
	) s 
		;	       
*/