create view v_bi_d10_doctor_summary
as
select s1.date, s1.doctorname, s1.d10_success_count, s2.d10_completed_count 
from 
(
    select a.date, b.doctorname, count(case when success_ind_new = 'Yes' then b.clientid else null end) as d10_success_count
	from v_date_to_date a
	left join v_bi_d10_patient_details b on b.day11 <= a.date
	where a.date >= '2019-06-19'
    and b.doctorname is not null
    group by date, b.doctorname
) s1 left join 
(
	select date, doctorname, count(clientid) as d10_completed_count 
	from v_date_to_date a 
	left join 
		(
				select distinct b.clientid, c.doctornameshort as doctorname, c.patientnameshort as patientname, c.coachnameshort as coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, c.status as current_status,
                date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 10 day)) as day10
                from
				(
					select clientid, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
					max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
					from bi_patient_status b
					group by clientid, status
				) b left join v_Client_tmp c on b.clientid = c.clientid
				where b.status = 'active' 
				and datediff(status_end_date, if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ))) >= 10
				and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 10 day)) >= '2019-06-19'
				and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 10 day)) <= date(itz(now()))
		) c on c.day10 <= a.date
	where doctorname is not null
	group by date, doctorname
) s2 on s1.date = s2.date and s1.doctorname = s2.doctorname 
;
