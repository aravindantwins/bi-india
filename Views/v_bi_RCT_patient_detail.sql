alter view v_bi_RCT_patient_detail as 
select a.clientid, patientname, doctornameShort as doctorname, coachnameshort as coachname, enrollmentDates as daysEnrolled, measure_event_date, measure_name, measure_type, measure 
from bi_RCT_patient_measures a inner join v_active_clients b on a.clientid = b.clientid 
where a.measure_event_date >= date_sub(date(now()), interval 45 day) 
and (b.labels like '%twin_prime%' or b.labels like '%twin_control%' or b.labels like '%unknown%')
;



