-- Patient Level view
create view v_bi_patient_topmeal_actual_x_prediction as 
select a.clientid, b.patientname, b.doctornameShort, coachnameShort, 
category, mealID, foodsInMeal, total_logged_overall, total_logged, total_predicted, MeanActualAUC, MeanPredictedAUC, MeanAbsoluteError, 
count_measured_auc_less_than_threshold, count_predicted_auc_less_than_threshold, count_precision, count_recall
from bi_patient_topmeal_actual_x_prediction a inner join v_client_tmp b on a.clientid = b.clientid 
where total_logged > 1
;
