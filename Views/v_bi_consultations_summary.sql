set collation_connection = 'latin1_swedish_ci';

create view v_bi_consultations_summary as 
select a.date, b.type,
count(distinct(case when b.status in ('SCHEDULED','COMPLETED','CANCELLED') then b.clientappointmentid else null end)) as Total_scheduled,
count(distinct(case when b.status in ('COMPLETED') then b.clientappointmentid else null end)) as Total_completed,
count(distinct(case when b.status in ('CANCELLED') then b.clientappointmentid else null end)) as Total_cancelled
from v_date_to_date a
left join
(
	select a.clientid, a.clientappointmentid, itz(eventtime) , cast(itz(a.eventtime) as date) as Date_appointment, a.type, a.status
	from clientappointments a 
	where date(a.eventtime) >= curdate() - interval  35 day
	and a.type in ('DOCTOR_APPOINTMENT','FRICTION_HANDLING','PROGRESS_REVIEW')
) b on a.date = b.date_appointment
where a.date >= date_sub(date(now()), interval  1 month ) and a.date <= date(now())
and type is not null
group by a.date,b.type
;