-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_doctor_payout_detail as 
select a.doctorId, val.doctorName, a.eventDate, 

a.totalActiveEOM, 
a.totalActiveEOM_nonDoctorChannel, 
a.totalActiveEOM_DoctorChannel, 
a.totalActiveEOM_nonDoctorChannel_GoldPlatinum, 
a.totalActiveEOM_DoctorChannel_Gold, 
a.totalActiveEOM_DoctorChannel_Platinum, 

a.total_DoctorChannel_Gold_Qtr_Acquisition,
a.total_DoctorChannel_Gold_Qtr_Acquisition * 4000 as total_DoctorChannel_Gold_Qtr_Acquisition_amount,

a.total_DoctorChannel_Gold_Annual_Acquisition,
a.total_DoctorChannel_Gold_Annual_Acquisition * 7000 as total_DoctorChannel_Gold_Annual_Acquisition_amount,

a.total_DoctorChannel_Platinum_Qtr_Acquisition,
a.total_DoctorChannel_Platinum_Qtr_Acquisition * 8000 as total_DoctorChannel_Platinum_Qtr_Acquisition_amount,

a.total_DoctorChannel_Platinum_Annual_Acquisition,
a.total_DoctorChannel_Platinum_Annual_Acquisition * 17000 as total_DoctorChannel_Platinum_Annual_Acquisition_amount,

a.total_medpublish_actioned,
a.total_medpublish_actioned * 50 as total_medPublish_amount,

a.total_consult_appointment_completed,
a.total_consult_appointment_completed * 500 as total_consult_amount

from 
bi_doctor_payout_detail a inner join v_alldoctors_list val on a.doctorId = val.doctorID 
;