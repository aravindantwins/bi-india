alter view v_bi_patient_NA_brittle_detail as 
select s.* from 
(
	select clientid, date, treatmentdays, last_available_cgm_5d_date, nut_adh_syntax_5d, medicine_drugs, total_200above_days, if(medicine_drugs like '%insu%', 'Yes', 'No') is_on_Insulin,
	case 	 
		 when treatmentdays >= 35
				and last_available_cgm_5d > 175
				and medicine_drugs like '%insu%'
				and nut_adh_syntax_5d < 0.8 then 'NA-Brittle1'

		 when treatmentdays >= 7
				and total_200above_days = 7 
				and nut_adh_syntax_5d < 0.8
				and if((medicine_drugs like '%alpha Glucosidase Inhibitors%' or medicine_drugs like '%GLP-1 Receptor Agonist%' or
						medicine_drugs like '%Glinide%' or medicine_drugs like '%Sodium-Glucose Cotransporter 2 Inhibitor%' or medicine_drugs like '%Sulfonylurea%' or
						medicine_drugs like '%Thiazolidinedione%' or medicine_drugs like '%Diabetes Others%' or 
						(medicine_drugs like '%Biguanide%' and medicine_drugs like '%Dipeptidyl Peptidase 4 Inhibitor%')), 'Yes', 'No') = 'Yes' then 'NA-Brittle2'
					
		 when if(medicine_drugs like '%alpha Glucosidase Inhibitors%' or medicine_drugs like '%GLP-1 Receptor Agonist%' or
				medicine_drugs like '%Glinide%' or medicine_drugs like '%Sodium-Glucose Cotransporter 2 Inhibitor%' or medicine_drugs like '%Sulfonylurea%' or
				medicine_drugs like '%Thiazolidinedione%' or  medicine_drugs like '%Diabetes Others%', 'Yes', 'No') = 'Yes'
				and nut_adh_syntax_5d < 0.8
				and treatmentdays > 90 then 'NA-Brittle3' else null end as NABrittle_Category
			  
	from 
	(
		select s1.clientid, s1.date, s1.treatmentdays, s1.cgm_5d, s1.last_available_cgm_5d_date, s1.last_available_cgm_5d, s1.nut_adh_syntax_5d, s1.medicine_drugs, count(distinct case when s2.cgm_5d > 200 then s2.date else null end) as total_200above_days
		from 
		(select clientid, date, treatmentdays, cgm_5d, cgm_1d, last_available_cgm_5d_date,last_available_cgm_5d, nut_adh_syntax_5d, medicine_drugs from bi_patient_reversal_state_gmi_x_date where date = date_sub(date(itz(now())), interval 1 day)) s1
		inner join bi_patient_reversal_state_gmi_x_date s2 on s1.clientid = s2.clientid and s2.date between date_sub(s1.last_available_cgm_5d_date, interval 6 day) and s1.last_available_cgm_5d_date
		group by s1.clientid, s1.date
	) s
) s 
where NABrittle_Category is not null
;

