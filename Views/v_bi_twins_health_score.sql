create view v_bi_twins_health_score 
as 
select clientid, patientname, mealdate, macro_final_score, micro_final_score, biota_final_score, 
(ifnull(macro_final_score,0) + ifnull(micro_final_score,0) + ifnull(biota_final_score,0)) as nutrition_score
from 
(
	select s.clientid, c.patientname, mealdate, macro_final_score, 
	micro_final_score,	biota_final_score
	from 
    bi_twins_health_score_summary s left join v_client_tmp c on s.clientid = c.clientid
    where mealdate >= date_sub(date(itz(now())), interval 1 month)
) s1
;

/*
	(
		select clientid, mealdate, sum(case when score_Category = 'Macro' then ifnull(actualScore, 0) end) as macro_final_score,
		sum(case when score_Category = 'Micro' then ifnull(actualScore, 0) end) as Micro_final_score,
		sum(case when score_Category = 'Biota' then ifnull(actualScore, 0) end) as Biota_final_score
		from bi_twins_health_score 
		where mealdate >= date_sub(date(itz(now())), interval 1 month)
        and score_Category in ('Macro','Micro','Biota')
		group by clientid, mealdate
	) s
*/