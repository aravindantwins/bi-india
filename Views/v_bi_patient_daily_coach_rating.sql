SET collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_daily_coach_rating as 
SELECT
       cd.clientId AS clientId,
       cd.patientName AS patientName,
       cd.patientNameShort as patientNameShort,
       cd.coach,
       ifnull(cd.leadCoach,'') as leadCoach,
       cd.daysEnrolled,
       cd.date,
       a.coach_rating,
       as_5d,
       cd.startDate,
       if(imt.clientid is not null, 'Yes', '') as isDisenrollOpen,
       isTempDischarge,
       if((pst.clientid IS NOT NULL),'Yes','') AS isAppSuspended

   FROM
   (
	   select date, c.clientid, d.patientName, d.patientNameShort, d.coachNameShort as coach, c.enrollmentDateITZ, daysEnrolled, leadCoachNameShort as leadCoach, 
       if(date(visitdateitz) > status_start_date or status_start_date is null, date(visitdateitz), status_start_date) as startDate,
       if(d.labels like '%TEMP_DISCHARGE%', 'Yes', '') as isTempDischarge
	   from v_date_to_date a inner join bi_patient_status c on a.date between c.status_start_date and c.status_end_date and c.status = 'active'
							 inner join v_client_tmp d on c.clientid = d.clientid
	   where date >= date_sub(date(itz(now())), interval 21 day) 
	) cd left join bi_patient_reversal_state_gmi_x_date a on cd.clientid = a.clientid and cd.date = a.date 
		 left join (select clientid, count(distinct incidentId) as totalIncidents from bi_incident_mgmt where category = 'disenrollment' and status = 'OPEN' group by clientid) as imt on cd.clientid = imt.clientid
         left JOIN bi_patient_suspension_tracker pst on ((cd.date between pst.startDate and endDate) and (pst.clientid=cd.clientid) and (pst.status=1))
  ;