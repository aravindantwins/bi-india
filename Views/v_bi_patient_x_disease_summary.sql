alter view v_bi_patient_x_disease_summary
as
select a.clientid, b.patientname, a.diseaseCategory, a.probability, a.diseaseName, a.detectionRule, a.symptoms, a.markersNormalRange, a.eventdate, a.actual, total_diseases 
from  bi_disease_ref_summary a inner join v_Active_clients b on a.clientid = b.clientid 

union all 

select 1 as clientid, ' Choose Patient' as patientname, null as diseasecategory, null as probability, null as diseaseName, null as detectionRule, null as symptoms, null as markersNormalRange, null as eventdate, 'No Data' as actual, null total_diseases 


/* OLD Definition this logic is now moved to a stored procedure
select a.clientid, a.patientname, a.category as diseaseCategory, a.probability, b.diseaseName, b.detectionRule, b.markersNormalRange, a.eventdate, if(count(distinct b.diseaseName)=0, null, count(distinct b.diseaseName)) as total_diseases 
from 
(
	select clientid, patientname, eventdate, category, probability from 
    (select distinct clientid, patientname, eventdate from v_bi_patient_x_disease where eventdate is not null) s1 
    cross join 
	(select distinct diseasecategory as category from ref_disease where diseasecategory <> '') s2
	cross join (select 'Certain' as probability union all select 'Medium Risk' as probability union all select 'High Risk' as probability) s3
	order by clientid, category, probability
) a left join v_bi_patient_x_disease b on a.clientid = b.clientid and a.eventdate = b.eventdate and b.rule_result = 1 
			and a.category = b.diseaseCategory		
            and a.probability = b.probability
group by a.clientid, a.patientname, a.category, a.probability, b.diseaseName, b.detectionRule, b.markersNormalRange, a.eventdate

union all 

select 1 as clientid, ' Choose Patient' as patientname, null as diseasecategory, null as probability, null as diseaseName, null as detectionRule, null as markersNormalRange, null as eventdate, null total_diseases 
*/

/*
-- OLD definition when 21 disease dashboard created as 1st version
select a.clientid, a.patientname, a.category as diseaseCategory, a.probability, a.eventdate, if(count(distinct b.diseaseName)=0, null, count(distinct b.diseaseName)) as total_diseases 
from 
(
	select clientid, patientname, eventdate, category, probability from 
    (select distinct clientid, patientname, eventdate from v_bi_patient_x_disease where eventdate is not null) s1 
    cross join 
	(select 'Diabetes' as category union all select 'Cholesterol' as category union all select 'Hypertension' as category union all select 'Others' as category) s2
	cross join (select 'Certain' as probability union all select 'Medium Risk' as probability union all select 'High Risk' as probability) s3
	order by clientid, category, probability
) a left join v_bi_patient_x_disease b on a.clientid = b.clientid and a.eventdate = b.eventdate and b.rule_result = 1 
			and a.category = (case when b.diseaseCategory = 'Cholesterol' then 'Cholesterol'
								  when b.diseaseCategory = 'Diabetes' then 'Diabetes'
                                  when b.diseaseCategory = 'Hypertension' then 'Hypertension' 
                                  else 'Others' end)
			and a.probability = (case when b.probability = 'Certain' then 'Certain'
								  when b.probability = 'Medium Risk' then 'Medium Risk'
                                  when b.probability = 'High Risk' then 'High Risk' end)
group by a.clientid, a.patientname, a.category, a.probability, a.eventdate
*/