create view v_bi_patient_renewal as 
select 
a.clientid, 
patientname, 
doctorname,
coachname,
a.enrollmentdate, 
a.termenddate, 
enrolled_days,
current_term_no, 
total_renewal, 
termstartdate, 
renewed_date,
b1.status,
b.status as current_status,
case when b1.status = 'active' then b.lastStatuschangeITZ else b1.laststatuschange end as lastStatuschange,
b.renewalVisitScheduledDate,
dischargeReason,
if(date(itz(now())) between date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 78 day)) and date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 84 day)), 1, 0) as less_than_7days_ind,
if(date(itz(now())) between date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 78 day)) and date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 84 day)), 84 - datediff(date(itz(now())), coalesce(a.termstartdate, a.enrollmentdate)), 0) as days_left_7days,

if(date(itz(now())) between date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 71 day)) and date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 77 day)), 1, 0) as less_than_14days_ind,
if(date(itz(now())) between date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 71 day)) and date(date_add(coalesce(a.termstartdate, a.enrollmentdate), interval 77 day)), 84 - datediff(date(itz(now())), coalesce(a.termstartdate, a.enrollmentdate)), 0) as days_left_14days,

if(datediff(date(itz(now())), coalesce(a.termstartdate, a.enrollmentdate)) > 84, 
   datediff(date(itz(now())), date_add(date(coalesce(a.termstartdate, a.enrollmentdate)), interval 84 day)), 0) as days_overdue,
ifnull(c.likely_to_renew,'') as likely_to_renew
from bi_patient_renewal a inner join v_client_tmp b on a.clientid = b.clientid
inner join 
(
		-- Active Patients as of today 
        select clientid, 'active' as status, status_end_date as laststatuschange from bi_patient_status where status = 'active' and status_end_date = date(now())
		
        union all
        
        -- If a patient was made Inactive directly instead of going through the regular route - Active-Discharged-Inactive
		select a.clientid, status, status_start_date as laststatuschange
		from bi_patient_status a inner join 
		(select clientid, max(status_start_date) as date_to_match from bi_patient_status where status in ('inactive') group by clientid) b
		on a.clientid = b.clientid and a.status_start_date = b.date_to_match
		where a.clientid not in (select clientid from bi_patient_status where status = 'active' and status_end_date = date(now()))
        and a.clientid not in (select clientid from bi_patient_status where status = 'discharged')
		
        union all
		
        -- If a patient was made Inactive using regular route. Consider only the Discharged state date. Again some edge cases possible. 
        -- i.e. a patient might have discharged twice (either in the program or by mistake). So always take the recent discharge status start date. 
        select a.clientid, status, status_start_date as laststatuschange
		from bi_patient_status a inner join 
		(select clientid, max(status_start_date) as date_to_match from bi_patient_status where status in ('discharged') group by clientid) b
		on a.clientid = b.clientid and a.status_start_date = b.date_to_match
		where a.clientid not in (select clientid from bi_patient_status where status = 'active' and status_end_date = date(now()))
) b1 on a.clientid = b1.clientid
left join 
(
		select distinct a.clientid, likely_to_renew from bi_patient_commitment_detail a, 
		(select clientid, max(record_startdate) as recent_date from bi_patient_commitment_detail group by clientid) b
		where a.clientid = b.clientid
		and a.record_startdate = b.recent_date
) c on a.clientid = c.clientid
order by a.clientid
;


