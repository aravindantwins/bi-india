alter view v_bi_nutrition_L1_metrics as 
select date, 
count(distinct a.clientid) as total_clients, 
count(distinct case when nut_adh_syntax = 'Yes' then a.clientid else null end) as total_nut_adh, 
count(distinct case when macro >= 80 then a.clientid else null end) as total_macro_adh, 
count(distinct case when micro >= 80 then a.clientid else null end) as total_micro_adh, 
count(distinct case when biota >= 80 then a.clientid else null end) as total_biota_adh, 

cast(avg(tac_Score) as decimal(5,0)) as overall_tac_average,

count(distinct case when nut_syntax_color = 'Green' then a.clientid else null end) as total_green_syntax, 
count(distinct case when nut_syntax_color = 'Orange' then a.clientid else null end) as total_orange_syntax, 

count(distinct case when nut_adh_syntax = 'No' then a.clientid else null end) as total_non_nut_adh, 

count(distinct case when nut_adh_syntax = 'No' and tac_score < 3 then a.clientid else null end) as total_non_nut_adh_dueTo_TAC, 
count(distinct case when nut_adh_syntax = 'No' and tac_score = 3 and (macro < 70 or micro < 70 or biota < 70) then a.clientid else null end) as total_non_nut_adh_dueTo_MMB, 
count(distinct case when nut_adh_syntax = 'No' and tac_score = 3 and (macro >= 70 and micro >= 70 or biota >= 70) and nut_syntax_color <> 'GREEN' then a.clientid else null end) as total_non_nut_adh_dueTo_irregular_syntax,

count(distinct case when b.clientid is not null then a.clientid else null end) as total_disenroll_open,

client_logged,
coach_logged,

nutR.total_reported as total_nut_imt_reported,
nutRe.total_resolved as total_nut_imt_resolved,

count(distinct case when a.is_InReversal = 'Yes' or a.is_MetOnly_by_ops = 'Yes' then a.clientid else null end) as total_inReversal

from v_bi_patient_nut_adh_syntax a 	left join (select clientid, count(distinct incidentId) as totalincidents from bi_incident_mgmt where category = 'disenrollment' and status = 'OPEN' group by clientid) b on a.clientid = b.clientid 
									left join (
												select mealdate, count(distinct case when creatorRole = 'CLIENT' then clientid else null end) as client_logged, 
												count(distinct case when creatorRole = 'COACH' then clientid else null end) as coach_logged
												from foodlogs 
												where mealdate >= date_sub(date(now()), interval 45 day)
												group by mealdate
											  ) c on a.date = c.mealdate
									left join ( select date(itz(report_time)) as report_date, count(distinct incidentID) as total_reported from bi_incident_mgmt where category = 'nutrition' and report_time >= date_sub(now(), interval 50 day) group by date(itz(report_time)) ) nutR on a.date = nutR.report_date
									left join ( select date(itz(resolved_time)) as resolved_date, count(distinct incidentID) as total_resolved from bi_incident_mgmt where category = 'nutrition' and report_time >= date_sub(now(), interval 50 day) group by date(itz(resolved_time)) ) nutRe on a.date = nutRe.resolved_date
group by date
;