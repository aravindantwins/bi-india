create view v_daily_nutritionplan_patient_details as
select 
a.clientid, 
b.patientname,
b.doctorname, 
b.coachname, 
mealdate, 
measureGroup, 
measureCategory, 
mealtype, 
foodlabel, 
E5foodGrading, 
food_measure, 
quantity, 
measureValue
from bi_daily_nutritionplan_patient_detail a inner join v_active_clients b on a.clientid = b.clientid
;
