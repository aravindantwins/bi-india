-- set collation_connection = 'latin1_swedish_ci';
alter view v_bi_CBG_patient_summary as 
select date, coach, leadCoach, 
count(distinct case when treatmentdays > 35 then a.clientid else null end) as total_35days, 
count(distinct case when treatmentdays > 35 and isRCT = 'No' then a.clientid else null end) as total_35days_without_RCT, 

count(distinct case when treatmentdays > 35 and (isCBGEligible = 'Y' or isCBG = 'Y') then a.clientid else null end) as total_CBGEligible, -- (include patients who are CBG also because they usually go as notEligible during the predicted days because of foodlog)
count(distinct case when treatmentdays > 35 and (isCBGEligible = 'Y' or isCBG = 'Y') and isRCT = 'No' then a.clientid else null end) as total_CBGEligible_without_RCT, -- (include patients who are CBG also because they usually go as notEligible during the predicted days because of foodlog)

count(distinct case when (treatmentdays > 35 and treatmentdays <= 41) and (isCBGEligible = 'Y' or isCBG = 'Y') and isRCT = 'No' and (plancode like '%GoldPro_quarter%' or plancode like '%GoldPro_annual%') then a.clientid else null end) as total_CBGEligible_without_RCT_GPP_Members, -- (include patients who are CBG also because they usually go as notEligible during the predicted days because of foodlog)

count(distinct case when treatmentdays > 35 and isCBG = 'Y' then a.clientid else null end) as total_CBGEligible_and_On,
count(distinct case when treatmentdays > 35 and isCBG = 'Y' and isRCT = 'No' then a.clientid else null end) as total_CBGEligible_and_On_without_RCT,

count(distinct case when treatmentdays > 35 and isCBGEligible = 'Y' and isCBG = 'N' then a.clientid else null end) as total_CBGEligible_and_NotOnCBG,
count(distinct case when treatmentdays > 35 and isCBGEligible = 'Y' and isCBG = 'N' and is_future_Scheduled='Yes' then a.clientid else null end) as total_CBGEligible_and_NotOnCBG_and_futureScheduled,

count(distinct case when treatmentdays > 35 and isCBGEligible = 'Y' and isCBG = 'N'  and isRCT = 'No' then a.clientid else null end) as total_CBGEligible_and_NotOnCBG_without_RCT,

count(distinct case when isCBG = 'Y' and isIncidentPresent = 'Yes' and totalIncidents_other_than_PT > 0 then a.clientid else null end) as total_CBGOn_W_Friction,
count(distinct case when isCBG = 'Y' and isIncidentPresent = 'Yes' and totalIncidents_only_PT > 0 then a.clientid else null end) as total_CBGOn_W_PTOnly_Friction,

count(distinct case when treatmentdays >= 21 and treatmentdays < 35 and isCBGEligible = 'Y' then a.clientid else null end) as total_CBGEligible_D21_D34,

count(distinct case when isCBG = 'Y' and is_CBGOn_lastDay = 'Yes' then a.clientid else null end) as total_CBGOn_lastDay,

count(distinct case when treatmentdays > 35 AND (isCBGEligible = 'Y' or isCBG = 'Y') and isRCT = 'No' AND is_app_suspended = 'Yes' then a.clientid else null end) AS total_CBGEligible_app_suspended,

count(distinct case when treatmentdays > 35 AND isRCT = 'No' AND is_app_suspended = 'Yes' then a.clientid else null end) AS total_d35_app_suspended,

count(distinct case when treatmentdays > 35 and isRCT = 'No' and is_future_Scheduled='No' and isGPP_Member_betweenD36_42='No' and is_app_suspended='No' then a.clientid else null end) as total_35days_base_after_custom_exclusion
 
from
(
select a.clientid, a.coach, a.leadCoach, a.date, 
            a.treatmentdays, a.as_1d, a.as_5d, 
            a.isCBGEligible, 
            a.isCBG, 
            a.is_MetOnly_by_OPS,
            a.is_MetOnly, 
            a.isRCT,
            a.plancode,
            inc.totalIncidents_other_than_PT,
            inc.totalIncidents_only_PT,
            case when inc.clientid is not null then 'Yes' else 'No' end as isIncidentPresent,
			case when (treatmentdays > 35 and treatmentdays <= 41) and (plancode like '%GoldPro_quarter%' or plancode like '%GoldPro_annual%') then 'yes' else 'No' end as isGPP_Member_betweenD36_42,
			case when  st.clientid IS NOT NULL then 'Yes' else 'No' end AS is_app_suspended,
            case when fpc.clientid is not null then 'Yes' else 'No' end as is_future_Scheduled,
            
            case when pc.clientid is not null then 'Yes' else 'No' end as Is_CBGOn_lastDay
from (	
			
            select s.clientid, coachnameShort as coach, leadCoachNameShort as leadCoach, s.date, s.treatmentdays, as_1d, as_5d, isCBGEligible, isCBG, is_MetOnly_by_OPS, is_MetOnly, if(b.labels like '%prime%' or b.labels like '%control%' or b.labels like '%TWIN_HTN%', 'Yes', 'No') as isRCT, b.plancode
			from 
			(
					select a.date, b.clientId, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
					from twins.v_date_to_date a
					inner join
					(
						select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
						from
						(
							select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
							from bi_patient_status
							where status = 'active'
							group by clientid, date(visitdateitz)
						) s
					) b on a.date between b.startDate and b.endDate
					where a.date >= date_sub(date(itz(now())), interval 21 day)
            ) s left join bi_patient_reversal_state_gmi_x_date a on s.clientid = a.clientid and s.date = a.date 
				left join v_Client_tmp b on s.clientid = b.clientid 
			where s.treatmentdays >= 11
      ) a
      left join (
					select a.clientid, date(itz(report_time)) as reportDate, date(itz(resolved_time)) as resolvedDate, 
                    count(distinct case when category in ('NUTRITION', 'INVENTORY', 'SENSORS', 'TWINS_PRODUCT') then incidentId else null end) as totalIncidents_other_than_PT, 
                    count(distinct case when category in ('PATIENT_TREATMENT') then incidentId else null end) as totalIncidents_only_PT
					from bi_incident_mgmt a inner join predictedCGMCycles b on a.clientid = b.clientid and date(itz(report_time)) between b.startdate and b.enddate
					where category in ('NUTRITION', 'INVENTORY', 'SENSORS', 'TWINS_PRODUCT', 'PATIENT_TREATMENT') 
                    and date(itz(report_time)) >= date_sub(date(itz(now())), interval 24 day)
					group by clientid, date(itz(report_time)), date(itz(resolved_time))
				) inc on a.clientid = inc.clientid and a.date between reportDate and ifnull(resolvedDate,date(itz(now()))) 
		
	 left join predictedCGMCycles pc on a.clientid = pc.clientid and a.date = pc.enddate
     left join (select clientid from predictedcgmcycles where startdate > date(itz(now()))) fpc on a.clientid = fpc.clientid 
     LEFT JOIN bi_patient_suspension_tracker st ON a.clientid = st.clientid AND a.date BETWEEN st.startdate AND st.enddate AND st.status = 1
) a
group by date, coach, leadCoach
;




