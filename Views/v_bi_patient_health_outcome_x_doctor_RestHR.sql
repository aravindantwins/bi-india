create view v_bi_patient_health_outcome_x_doctor_RestHR as 
select distinct s1.clientid, s1.doctorId, s1.doctorname, s1.treatmentdays, s2.first_restHR, s2.last_restHR
from 
(
	select a.clientid, a.doctorname, b.doctorId, treatmentdays    
	from dim_client a left join v_AllClient_list b on a.clientid = b.clientid 
	where is_row_current = 'y' and a.status = 'active' 
    and a.isRCT is null
    group by a.clientid
) s1 left join 
(
	select s.clientid, 
	max(case when first_restHR_date = measure_event_date then average_measure else null end) as first_restHR,
	max(case when last_restHR_date = measure_event_date then average_measure else null end) as last_restHR
	from 
	(
		select clientid, min(measure_event_date) as first_restHR_date, max(measure_event_date) as last_restHR_date
		from bi_measures 
		where measure_name = 'rest-hr' and average_measure is not null
		group by clientid 
	) s left join bi_measures s1 on s.clientid = s1.clientid and (s.first_restHR_date = s1.measure_event_date or s.last_restHR_date = s1.measure_event_date) and s1.measure_name = 'rest-hr'
	group by s.clientid 
) s2 on s1.clientid = s2.clientid 
;