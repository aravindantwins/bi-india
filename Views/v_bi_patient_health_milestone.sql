create view v_bi_patient_health_milestone as 
select clientid, 
count(distinct case when treatmentdays between 1 and 30 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as GMIReducedDays_within30,
count(distinct case when treatmentdays between 1 and 60 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as GMIReducedDays_within60,
count(distinct case when treatmentdays between 1 and 90 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as GMIReducedDays_within90,
count(distinct case when treatmentdays between 1 and 120 and (GMI < 6.5  or (start_LabA1C - GMI) > 0.4) then date else null end) as GMIReducedDays_within120,

count(distinct case when treatmentdays between 1 and 30 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within30,
count(distinct case when treatmentdays between 1 and 60 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within60,
count(distinct case when treatmentdays between 1 and 90 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within90,
count(distinct case when treatmentdays between 1 and 120 and is_MedReduced = 'Yes' then date else null end) as MedReducedDays_within120,

count(distinct case when treatmentdays between 1 and 30 and GMI < 6.5 and (is_InReversal = 'Yes' or is_MetOnly = 'Yes') then date else null end) as GMIReduced_withNoMed_or_MetOnly_within30,
count(distinct case when treatmentdays between 1 and 60 and GMI < 6.5 and (is_InReversal = 'Yes' or is_MetOnly = 'Yes') then date else null end) as GMIReduced_withNoMed_or_MetOnly_within60,
count(distinct case when treatmentdays between 1 and 90 and GMI < 6.5 and (is_InReversal = 'Yes' or is_MetOnly = 'Yes') then date else null end) as GMIReduced_withNoMed_or_MetOnly_within90,
count(distinct case when treatmentdays between 1 and 120 and GMI < 6.5 and (is_InReversal = 'Yes' or is_MetOnly = 'Yes') then date else null end) as GMIReduced_withNoMed_or_MetOnly_within120,

count(distinct case when treatmentdays between 1 and 30 and consecutive_GMI_threshold_days >= 30 then date else null end) as GMIConsec30days_withNoMed_or_MetOnly_within30,
count(distinct case when treatmentdays between 1 and 60 and consecutive_GMI_threshold_days >= 30 then date else null end) as GMIConsec30days_withNoMed_or_MetOnly_within60,
count(distinct case when treatmentdays between 1 and 90 and consecutive_GMI_threshold_days >= 30 then date else null end) as GMIConsec30days_withNoMed_or_MetOnly_within90,
count(distinct case when treatmentdays between 1 and 120 and consecutive_GMI_threshold_days >= 30 then date else null end) as GMIConsec30days_withNoMed_or_MetOnly_within120
from 
(
		select a.*, programStartDate_analytics as day0, 
		date_sub(b.d35_date, interval 5 day) as day30, 
		date_add(b.d35_date, interval 25 day) as day60, 
		d90_date as day90, 
		date_add(b.d90_date, interval 30 day) as day120
		from 
		bi_patient_reversal_state_GMI_x_date a left join dim_client b on (a.clientid = b.clientid and b.is_row_current = 'y')
		where b.status in ('active','discharged','inactive')
) s 
group by clientid 
;