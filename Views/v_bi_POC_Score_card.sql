create view v_bi_POC_Score_card as 
SELECT a.report_date,
        a.poc,
        a.category,
        a.measure_name,
        a.measure,
        b.pocDateAdded
FROM
        bi_daily_onboarding_sla_x_poc a
        left join (select distinct poc, pocDateAdded from v_allclient_list) b on (a.poc=b.poc)
        WHERE
        (category = 'POC_Score') or measure_name in ('Pending Active','Activations','Deffered')
;