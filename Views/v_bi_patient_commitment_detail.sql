create view v_bi_patient_commitment_detail
as
select  c.clientid, 
		d1.date as report_date, 
        c.patientname, 
        c.coachname, 
        c.doctorname, 
        secured_commitment as sec_com, 
        is_following_nutrition as nut_flw,
        comments,
        is_following_medicine as med_flw,
        sustain_rating as sustain_rating_desc,
        left(sustain_rating,1) as sustain_rating,
        sustain_reason,
        likely_to_renew
from v_date_to_date d1 
left join bi_patient_commitment_detail d2 on d1.date between d2.record_startdate and d2.record_enddate
left join v_client_tmp c on d2.clientid = c.clientid
where d2.clientid is not null
order by clientid, d1.date