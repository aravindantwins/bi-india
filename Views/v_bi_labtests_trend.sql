alter view v_bi_labtests_trend 
as 
select s.clientid, 
b.patientName as patientName, 
b.doctorNameShort as doctorName, 
b.coachNameShort as coachName,
'ACTIVE' as current_status,
s.testName, 
s.testInstance,
s.eventDateTimeITZ,
s.measure 
from bi_labvalues_trend s inner join v_active_clients b on s.clientid = b.clientid 
-- order by clientid, testName, testInstance
;