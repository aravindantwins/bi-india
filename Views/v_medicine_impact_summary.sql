 create view v_medicine_impact_summary  as
select 
x.clientid, 
x.patientname, 
x.doctorname,
current_medication, 
last_date as date_used_for_running,
x.cgm5dg as 5Dg_running, 
x.ketone5dk as 5Dketone_running, 
x.spike5d as 5Dspike_running, 
x.netcarb5d as 5Dnetcarb_running,
c.eventdateitz as medication_change_date,
c.cgm5dg as 5Dg_on_med_change_date,
c.ketone5dk as 5Dketone_on_med_change_date,
c.spike5d as 5Dspike_on_med_change_date,
c.netcarb5d as 5Dnetcarb_on_med_change_date,
d.cgm5dg as 5Dg_day_before_med_change,
d.ketone5dk as 5Dketone_day_before_med_change,
d.spike5d as 5Dspike_day_before_med_change,
d.netcarb5d as 5Dnetcarb_day_before_med_change
from 
(select 	a.clientid, 
		a.patientname, 
        a.doctorname, 
		a.eventdateitz as last_date, 
		a.medication as current_medication, 
        cgm5dg, 
		cgm3dg, 
        ketone5dk, 
        spike5d, 
        netcarb5d
from v_medicine_impact_details a
inner join 
		(select clientid, date_sub(max(eventdateitz), interval 1 day) as lastdate from v_medicine_impact_details 
		 group by clientid
		) b
		on a.clientid = b.clientid and a.eventdateitz = b.lastdate
) x
left join (
		select a.clientid, a.medication, eventdateitz, medication_change_date_ind, cgm5dg, ketone5dk, spike5d, netcarb5d 
		from v_medicine_impact_details a
		inner join 
				(select clientid, medication, max(Eventdateitz) as recent_change_date_by_medication
						from v_medicine_impact_details 
                        where medication_change_date_ind = 1 
                        and medication is not null
				 group by clientid, medication) b
				 on a.clientid = b.clientid and a.medication = b.medication and a.eventdateitz = b.recent_change_date_by_medication
				where a.medication is not null
		) c on x.clientid = c.clientid and x.current_medication = c.medication
left join 
		(
		select distinct a.clientid, b.medication, cgm5dg, ketone5dk, spike5d, netcarb5d 
		from v_medicine_impact_details a
		inner join 
				(select clientid, medication,  max(Eventdateitz) as recent_change_date_by_medication, 
						date_sub(max(Eventdateitz), interval 1 day) as day_before_medication_change
						from v_medicine_impact_details 
                        where medication_change_date_ind = 1 
                        and medication is not null
				 group by clientid, medication) b
				 on a.clientid = b.clientid and a.eventdateitz = b.day_before_medication_change
		  ) D on x.clientid = d.clientid and x.current_medication = d.medication
        ;
