alter view v_bi_patient_overdue_invoices_Zoho as 
select a.cf_clientid_unformatted as clientid, c.patientname, c.coachnameShort as coach, c.leadCoachNameShort as leadCoach, c.email, c.city, c.mobilenumber, c.status as current_status, 
b.number as inv_number, b.invoice_date, b.total as invoice_amount, c.planCode, p.name as planName,
datediff(date(itz(now())), invoice_date) as overdueDays,

case when datediff(date(itz(now())), invoice_date) between 0 and 4 then 'D0-4'
	 when datediff(date(itz(now())), invoice_date) between 5 and 6 then 'D5-6'
	 when datediff(date(itz(now())), invoice_date) between 7 and 10 then 'D7-10'
	 when datediff(date(itz(now())), invoice_date) between 11 and 20 then 'D11-20'		
	 when datediff(date(itz(now())), invoice_date) between 21 and 30 then 'D21-30'
	 when datediff(date(itz(now())), invoice_date) > 30 then '>D30' end as overDueCategory
from 
importedbilling.zoho_customers a inner join v_client_tmp c on a.cf_clientid_unformatted = c.clientid 
								 inner join importedbilling.zoho_invoices b on a.customer_id = b.customer_id
								 left join importedbilling.zoho_plans p on c.plancode = p.plan_code
where b.status = 'Overdue' 
and c.status in ('active','pending_active')
;
