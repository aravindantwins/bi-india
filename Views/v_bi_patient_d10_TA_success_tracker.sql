alter view v_bi_patient_d10_TA_success_tracker as 
select s.clientid, c.patientname, c.coachnameShort as coach, c.leadCoachNameShort as leadCoach, 
case when d.clientid is null then 'M2'
	 when d.clientid is not null and is_patient_converted = 'yes' then 'M1-M2'
     when d.clientid is not null and is_patient_converted is null then 'M1'
     end as patient_category, 
s.TreatmentActivationDay as Treatmentday1, 
date_add(s.TreatmentActivationDay, interval 1 day) as Treatmentday2, 
max(case when s1.treatmentDays = 2 then nut_adh_syntax else null end) as Day1_NUTADH, 
max(case when s1.treatmentDays = 3 then nut_adh_syntax else null end) as Day2_NUTADH, 
max(case when s1.treatmentDays = 4 then nut_adh_syntax else null end) as Day3_NUTADH, 
max(case when s1.treatmentDays = 5 then nut_adh_syntax else null end) as Day4_NUTADH, 
max(case when s1.treatmentDays = 6 then nut_adh_syntax else null end) as Day5_NUTADH, 
max(case when s1.treatmentDays = 2 then as_1d else null end) as Day1_AS_1d, 
max(case when s1.treatmentDays = 3 then as_1d else null end) as Day2_AS_1d,
max(case when s1.treatmentDays = 4 then as_1d else null end) as Day3_AS_1d,
max(case when s1.treatmentDays = 5 then as_1d else null end) as Day4_AS_1d,
max(case when s1.treatmentDays = 6 then as_1d else null end) as Day5_AS_1d,
max(case when s1.treatmentDays = 2 then (s1.IMT_opened - s1.IMT_resolved) else null end) as Day1_IMT_outstanding,
max(case when s1.treatmentDays = 3 then (s1.IMT_opened - s1.IMT_resolved) else null end) as Day2_IMT_outstanding,
max(case when s1.treatmentDays = 4 then (s1.IMT_opened - s1.IMT_resolved) else null end) as Day3_IMT_outstanding,
max(case when s1.treatmentDays = 5 then (s1.IMT_opened - s1.IMT_resolved) else null end) as Day4_IMT_outstanding,
max(case when s1.treatmentDays = 6 then (s1.IMT_opened - s1.IMT_resolved) else null end) as Day5_IMT_outstanding

from 
(
	select a.clientid, max(date) as TreatmentActivationDay from bi_patient_reversal_state_gmi_x_date a 
	where treatmentdays = 2
    and date >= date_sub(date(itz(now())), interval 30 day)
    group by a.clientid
) s left join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s1.date >= s.TreatmentActivationDay and s1.treatmentdays in (2,3,4,5,6)
	left join v_client_tmp c on s.clientid = c.clientid
    left join bi_ttp_enrollments d on s.clientid = d.clientid
where c.status not in ('PENDING_ACTIVE') -- this is needed because patients are being sent to PENDING_ACTIVE state from ACTIVE state. so lets ignore such patients. 
group by s.clientid, s.TreatmentActivationDay
;
