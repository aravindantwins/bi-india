set collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_reversal_risk_management_detail as 
select date,
clientid, 
coach,
leadCoach,
isM1_Converted,
isNABrittle,
isTTP,
isExcluded,
is_InReversal,
is_MetOnly_by_OPS,
is_CBGtoCGM_to_include,
medicine_drugs,
last_cgm_5d,
last_available_cgm_5d_date,
cgm_5d, 
iscbg,
is_app_suspended,
isDisenroll_IMT_Open,
is_Excluded_timetracked,
-- Commercial numbers
case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then 'Yes'
	when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then 'Yes' else null end as isIncluded_inBase,
						
case when (isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal = 'Yes' and is_MetOnly_by_OPS = 'No') or is_CBGtoCGM_to_include = 'Yes')) then 'Yes' 
	when (isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal = 'Yes' and is_MetOnly_by_OPS = 'No') or is_CBGtoCGM_to_include = 'Yes')) then 'Yes' else null end as isIncluded_Inreversal_noMed, 

case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (is_InReversal is null and is_MetOnly_by_OPS = 'Yes') and is_CBGtoCGM_to_include = 'No' then 'Yes'
 	when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and (is_InReversal is null and is_MetOnly_by_OPS = 'Yes') and is_CBGtoCGM_to_include = 'No' then 'Yes'   else null end as isIncluded_InReversal_WMet,
-- To calculate relapsed cgm_5d >=140
case when (isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and is_MetOnly_by_OPS = 'No') and is_CBGtoCGM_to_include = 'No' and ( medicine_drugs is null and cgm_5d >=140))) then 'Yes'
	when (isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and is_MetOnly_by_OPS = 'No') and is_CBGtoCGM_to_include = 'No' and (medicine_drugs is null and cgm_5d >=140) )) then 'Yes' else null end as isIncluded_Escalation_with_noMed_but_failed_5dg, 

case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and is_MetOnly_by_OPS = 'No') and is_CBGtoCGM_to_include = 'No' and (medicine_drugs in ('METFORMIN','METFORMIN (DIAB)','Biguanide (DIABETES)') and cgm_5d >=140 )) then 'Yes' 
	when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and is_MetOnly_by_OPS = 'No') and is_CBGtoCGM_to_include = 'No' and  (medicine_drugs in ('METFORMIN','METFORMIN (DIAB)','Biguanide (DIABETES)') and cgm_5d >=140 )) then 'Yes' else null end as isIncluded_Escalation_with_Met_but_failed_5dg,

-- Entering D35 escalationStage 
case when isM1_Converted = 'Yes' and treatmentdays =36 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and is_MetOnly_by_OPS = 'No')  and is_CBGtoCGM_to_include = 'No'   )  then 'Yes'
 	when isM1_Converted = 'No' and treatmentdays = 36 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and is_MetOnly_by_OPS = 'No') and is_CBGtoCGM_to_include = 'No' )  then 'Yes'   else null end as isBrandNewEscalation,


-- To calculate relapsed no medicine 130-135.9, with met 136-139.9
case when (isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal='Yes' and is_MetOnly_by_OPS = 'No' ) and is_CBGtoCGM_to_include = 'No' and ( medicine_drugs is null and (cgm_5d >=130 and cgm_5d <136))) ) then 'Yes'
	when (isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal='Yes' and is_MetOnly_by_OPS = 'No') and is_CBGtoCGM_to_include = 'No' and (medicine_drugs is null and (cgm_5d >=130 and cgm_5d <136)))) then 'Yes'  else null end as is_relapseRisk_no_med_5dg_130_136, 

case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and   is_MetOnly_by_OPS = 'Yes') and is_CBGtoCGM_to_include = 'No' and (medicine_drugs in ('METFORMIN','METFORMIN (DIAB)','Biguanide (DIABETES)') and (cgm_5d>=136 and cgm_5d <140)))  then 'Yes'
	when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' and ((is_InReversal is null and  is_MetOnly_by_OPS = 'Yes') and is_CBGtoCGM_to_include = 'No' and (medicine_drugs in ('METFORMIN','METFORMIN (DIAB)','Biguanide (DIABETES)') and (cgm_5d >=136 and cgm_5d<140 )))  then 'Yes'   else null end as is_relapseRisk_WMet_5dg_136_140,

-- Glucose Prick
case when Is_glucose_prick='Yes' then 'Yes'  else null end  as Total_glucose_prick


from  
(
	select distinct a.clientid, coach, ct.leadCoachNameShort as leadCoach, a.treatmentDays, a.date, datediff(a.date, last_available_cgm_5d_date) as daysFromLast5d,datediff(a.date, ttp.conversion_payment_date) as daysFromConversionDate, 
	b.is_InReversal, 
	b.is_MetOnly_by_OPS,
	b.isNoCGM_14days,
    b.cgm_5d, 
    b.last_available_cgm_5d as last_cgm_5d,
    b.last_available_cgm_5d_date,
    b.iscbg,
    b.medicine_drugs,
    b.is_MoreThanMet,
    case when cg.clientid  is not null then 'Yes' else 'No' end as Is_glucose_prick,
	if((ttp.is_patient_currently_TTP = 'yes' or (ttp.is_patient_currently_TTP is null and ttp.is_patient_converted is null)) and ttp.clientid is not null, 'Yes', 'No') as isTTP,
	if(ttp.is_patient_converted = 'yes' and ttp.clientid is not null, 'Yes', 'No') as isM1_Converted,    
    -- cx.suspended,
	if(ifnull(b.start_medCount,0) > 0 or b.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
    if(ct.labels like '%unqualified%', 'Yes', 'No') as isUnqualified,
 	if(ct.labels like '%exclude%', 'Yes', 'No') as isExcluded,
	if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
    b.nonAdh_brittle_category,
	ifnull(dc.is_type1like, 'No') as is_type1like,
    dc.is_medical_exclusion,
    case when dc.is_medical_exclusion = 'Yes' and (is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes') then 'Yes' 
		 when dc.is_medical_exclusion = 'Yes' and (is_InReversal is null and is_MetOnly_by_OPS = 'No') then 'No' 
         else null end as isMedExclusion_to_include,
         
    case when dc.is_medical_exclusion = 'Yes' and ifnull(dc.is_type1like,'No') = 'No' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' -- we need to check type1like here because due to the change in type1like prediction logic, a member can first be excluded under something but can be tagged as type1like later. 
         else 'No' end as isMedExclusion_AST_ALT_BMI,
         
    case when dc.is_medical_exclusion = 'Yes' and ifnull(dc.is_type1like,'No') = 'No' and medical_exclusion_cohort in ('Age') then 'Yes' 
         else 'No' end as isMedExclusion_Age,
	if(at.clientid is not null, 'Yes', 'No') as is_app_suspended,
	if(cb.clientid is not null, 'Yes', 'No') as is_CBGtoCGM_to_include,
	if(imt.clientid is not null, 'Yes', 'No') as isDisenroll_IMT_Open,
	ifnull(b.isExcludeLabelOn, 'No') as is_Excluded_timetracked
	from 
	(
		select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
		from twins.v_date_to_date a
		inner join
		(
			select clientid, coach, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
			from
			(
				select clientid, coachname_short as coach, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
				from bi_patient_status
				where status = 'active'
				group by clientid, date(visitdateitz)
			) s
		) b on a.date between b.startDate and b.endDate
		where a.date >= date_sub(date(itz(now())), interval 28 day) and a.date <= date(itz(now()))
	) a left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.date = b.date 
		left join bi_ttp_enrollments ttp on a.clientid = ttp.clientid
        -- left join clientauxfields cx on a.clientid = cx.id
        left join v_client_tmp ct on a.clientid = ct.clientid 
        left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
		left join bi_patient_suspension_tracker at on a.clientid = at.clientid and a.date between at.startdate and at.endDate and at.status = 1
        left join (
					select s.clientid, s.cbgEndDate, date_add(s.cbgenddate, interval 1 day) as day1_after_cbg_is_over, date_add(s.cbgenddate, interval 7 day) as day7_after_cbg_is_over, 
                    s.dayFirst5dg_available, datediff(dayFirst5dg_available, cbgEndDate ) as dayFirst5dg_available_after_CBGisOver, 							
					s1.cgm_5d, -- s1.is_inReversal, s1.is_metonly_by_ops, 							
					case when is_InReversal is null and is_MetOnly_by_OPS = 'No' then 'Yes'							
						 when is_InReversal is null and is_MetOnly_by_OPS is null then 'NoCGM5d_yet'						
						 when is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' then 'inReversal'
						 else 'No' end is_Escalation_finder						
					from 							
					(							
						select a.clientid, a.date as cbgEndDate, min(case when cgm_5d is not null then b.date else null end) as dayFirst5dg_available						
						from 						
						(						
							select distinct b.clientid,date 					
							from v_date_to_date a inner join predictedcgmcycles b on a.date = b.enddate					
												  inner join v_client_tmp c on b.clientid = c.clientid
							where a.date >= date_sub(date(itz(now())), interval 45 day)
                            and datediff(b.enddate, b.startdate) >= 50
						) a  						
						 left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date > a.date						
						 group by a.clientid, a.date						
					 ) s left join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.dayFirst5dg_available = s1.date 			
					) cb on a.clientid = cb.clientid and a.date between cb.day1_after_cbg_is_over and cb.day7_after_cbg_is_over      
		left join ( select clientid,startdate,enddate  from  clienttodoschedules where category ='test' and type in ('GLUCOSE_KETONE','glucose') ) cg on cg.clientid=a.clientid and a.date between cg.startdate and cg.endDate
		left join (select distinct clientid from bi_incident_mgmt where category = 'disenrollment' and status = 'open') imt on a.clientid = imt.clientid 
        where ct.patientname not like '%obsolete%'
) a 
;


