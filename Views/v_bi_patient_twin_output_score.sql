create view v_bi_patient_twin_output_score as 
select c.clientid, e.patientname, c.category, c.subcategory, c.parameter, c.reference_range, c.weight, c.inc_exc, d.eventdate as date, c.date_category, d.measure, d.max_score, d.actualScore,
case when e.labels like '%exclude%' then 'yes' else 'No' end as exclude_flag
from (
		select distinct a.clientid, category, subcategory, parameter, reference_range, weight, inc_exc, 'D0' date_category from 
        bi_patient_status a cross join bi_stage_outputscore_parameters_lookup b
        where a.status in ('active') 
        and datediff(a.status_end_date, a.status_start_date) >= 80
        
        union all 
        
		select distinct a.clientid, category, subcategory, parameter, reference_range, weight, inc_exc, 'D35' date_category from 
        bi_patient_status a cross join bi_stage_outputscore_parameters_lookup b
        where a.status in ('active') 
        and datediff(a.status_end_date, a.status_start_date) >= 80
        
        union all 
        
		select distinct a.clientid, category, subcategory, parameter, reference_range, weight, inc_exc, 'D90' date_category from 
        bi_patient_status a cross join bi_stage_outputscore_parameters_lookup b
        where a.status in ('active') 
        and datediff(a.status_end_date, a.status_start_date) >= 80
        
) c left join 
(
	select a.clientid, date as eventdate, category, measure, a.max_score, actualScore, date_category
	from bi_twins_health_output_score_x_date_category a inner join dim_client d on a.clientid = d.clientid and d.is_row_current = 'y' 
) d on c.clientid = d.clientid and c.parameter = d.category and c.date_category = d.date_category
left join v_client_tmp e on c.clientid = e.clientid 
;

/* - OLD Definition 
select c.clientid, e.patientname, c.category, c.subcategory, c.parameter, c.reference_range, c.weight, c.inc_exc, c.date, c.date_category, d.measure, d.max_score, d.actualScore,
case when e.labels like '%exclude%' then 'yes' else 'No' end as exclude_flag
from (
		select a.clientid, category, subcategory, parameter, reference_range, weight, inc_exc, date(a.firstBloodWorkDateITZ) as date, 'D0' date_category from 
        dim_client a cross join bi_stage_outputscore_parameters_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
        union all 
        
        select a.clientid, category, subcategory, parameter, reference_range, weight, inc_exc, a.d35_date_lab as date, 'D35' as date_category from 
        dim_client a cross join bi_stage_outputscore_parameters_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
        union all 
        
        select a.clientid, category, subcategory, parameter, reference_range, weight, inc_exc, a.d90_date_lab as date, 'D90' as date_category from 
        dim_client a cross join bi_stage_outputscore_parameters_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
) c left join 
(
	select a.clientid, date as eventdate, category, measure, a.max_score, actualScore
	from bi_twins_health_output_score a inner join dim_client d on a.clientid = d.clientid and d.is_row_current = 'y' 
    and (a.date = date(firstBloodWorkDateITZ) or a.date = d.d35_date_lab or a.date = d.d90_date_lab)
) d on c.clientid = d.clientid and c.parameter = d.category and c.date = d.eventdate
left join v_client_tmp e on c.clientid = e.clientid 
;
*/