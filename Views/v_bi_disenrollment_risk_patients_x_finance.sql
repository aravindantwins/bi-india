alter view v_bi_disenrollment_risk_patients_x_finance as 
select s.clientid, s1.patientname, s1.doctornameShort as doctor, s1.coachnameshort as coach, s1.status, dc.treatmentdays, 
s1.email, s1.mobileNumber, s1.planCode, s.invoice_date, s.total as overdue_invoice_amount, 
datediff(date(itz(now())), invoice_date) as total_overdue_days,
last_payment_date,
suspended, latestSuspensionDate
from 
(
	select cf_clientid_unformatted as clientid, invoice_id, number as inv_number, invoice_date, due_date, total 
    from importedbilling.zoho_customers a inner join importedbilling.zoho_invoices b on a.customer_id = b.customer_id
    where b.status = 'overdue'
	and not exists 
	(select 1 from bi_ttp_enrollments b where a.cf_clientid_unformatted = b.clientid and b.is_patient_converted is null) 
    and total not in (1, 1990, 1950, 2499, 2450, 990, 1290, 1949, 2490, 890, 1450, 950, 1050) -- exclude all TTP in Overdue status
) s inner join v_allClient_list s1 on s.clientid = s1.clientid 
	left join dim_client dc on s.clientid = dc.clientid and is_row_current = 'y'
	left join (
		select clientid, max(payment_date) as last_payment_date
        from 
		(
			select cf_clientid_unformatted as clientid, b.invoice_id, c.date as payment_date
			from importedbilling.zoho_customers a inner join importedbilling.zoho_invoices b on a.customer_id = b.customer_id
												  inner join importedbilling.zoho_payments c on b.invoice_id = c.invoice_id
			where b.status = 'paid'
            and b.total not in (1, 1990, 1950, 2499, 2450, 990, 1290, 1949, 2490, 890, 1450, 950, 1050) -- exclude any TTP payment
        ) s 
        group by clientid
    ) lp on s.clientid = lp.clientid
    left join clientAuxFields cx on s.clientid = cx.id 
;


;


