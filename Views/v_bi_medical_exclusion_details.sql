create view v_bi_medical_exclusion_details as 
select a.clientid, b.patientnameShort as patientname, b.doctornameShort as doctorname, b.status as current_status, 
b.coachnameShort as coachname, 'Hba1c' as category, itz(bloodworktime) as investigation_date, veinhba1c as measure
from 
(
		select distinct a.clientid, b.bloodworktime, veinhba1c from dim_client a 
		inner join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
) a
inner join v_client_tmp b on a.clientid = b.clientid where veinhba1c < 5.7 

union all 

select a.clientid, b.patientnameShort as patientname, b.doctornameShort as doctorname, b.status as current_status, 
b.coachnameShort as coachname, 'BMI' as category, itz(bloodworktime) as investigation_date, bmi as measure
from 
(
        select distinct a.clientid, b.bloodworktime, cast(weight/(height*height) as decimal(15,5)) as bmi from dim_client a 
		inner join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
) a
inner join v_client_tmp b on a.clientid = b.clientid where bmi < 18.5

union all 

select a.clientid, b.patientnameShort as patientname, b.doctornameShort as doctorname, b.status as current_status, 
b.coachnameShort as coachname, 'Albumin' as category, itz(bloodworktime) as investigation_date, albumin as measure
from 
(
	select distinct a.clientid, b.bloodworktime, albumin from dim_client a 
    inner join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
) a
inner join v_client_tmp b on a.clientid = b.clientid where albumin < 3.5

union all 

select a.clientid, b.patientnameShort as patientname, b.doctornameShort as doctorname, b.status as current_status, 
b.coachnameShort as coachname, 'AST/ALT' as category, itz(bloodworktime) as investigation_date, ast_alt_ratio as measure
from 
(
	select distinct a.clientid, b.bloodworktime, cast(ast/alt as decimal(15,5)) as ast_alt_ratio from dim_client a 
    inner join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
) a
inner join v_client_tmp b on a.clientid = b.clientid where ast_alt_ratio > 3.0

union all 

select a.clientid, b.patientnameShort as patientname, b.doctornameShort as doctorname, b.status as current_status, 
b.coachnameShort as coachname, 'Creatinine' as category, itz(bloodworktime) as investigation_date, Creatinine as measure
from 
(
		select distinct a.clientid, b.bloodworktime, Creatinine from dim_client a 
		inner join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
) a
inner join v_client_tmp b on a.clientid = b.clientid where Creatinine > 1.5

union all 

select a.clientid, b.patientnameShort as patientname, b.doctornameShort as doctorname, b.status as current_status, 
b.coachnameShort as coachname, 'GFR' as category, itz(bloodworktime) as investigation_date, glomerularFiltrationRate as measure
from 
(
	select distinct a.clientid, b.bloodworktime, glomerularFiltrationRate from dim_client a 
    inner join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
) a
inner join v_client_tmp b on a.clientid = b.clientid where glomerularFiltrationRate < 60

union all 

select clientid, patientnameShort as patientname, doctornameShort as doctorname, status as current_status, 
coachnameShort as coachname, 'Age' as category, enrollmentDateITZ as investigation_date, age as measure
from v_client_tmp 
where (age > 70 or age < 18)
and status in ('active','discharged','inactive')