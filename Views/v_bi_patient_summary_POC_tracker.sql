alter view v_bi_patient_summary_POC_tracker as 
select s1.date, s1.poc, 
s1.total_assigned,
s1.total_PA,
s1.total_PA_less_5_days,
s1.total_PA_more_than_8_days,
s1.total_PA_between_6_to_8_days,

s1.total_PA_deffered, 
s1.total_PA_less_5_days_deffered, 
s1.total_PA_more_than_8_days_deferred, 
s1.total_PA_between_6_to_8_days_deferred, 

s1.total_Active,
s1.total_active_less_5_days,
s1.total_disenroll_risk,
s1.total_disenroll_risk_program_exclusion,
s1.total_doctorAssigned,
s1.total_imts,
s2.avg_pa_5d,
s3.avg_active_5d
from 
(
	select date, ifnull(s.poc,'') as poc, 
	ifnull(max(case when category = 'pa' then measure else null end),0) + ifnull(max(case when category = 'active' then measure else null end),0) as total_assigned, 
	ifnull(max(case when category = 'pa' then measure else null end),0) as total_PA, 
	ifnull(max(case when category = 'pa' then within_5_days else null end),0) as total_PA_less_5_days, 
	ifnull(max(case when category = 'pa' then more_than_8_days else null end),0) as total_PA_more_than_8_days, 
	ifnull(max(case when category = 'pa' then between_6_to_8_days else null end),0) as total_PA_between_6_to_8_days, 
	
    ifnull(max(case when category = 'pa' then deferred else null end),0) as total_PA_deffered, 
	ifnull(max(case when category = 'pa' then within_5_days_deferred else null end),0) as total_PA_less_5_days_deffered, 
	ifnull(max(case when category = 'pa' then more_than_8_days_deferred else null end),0) as total_PA_more_than_8_days_deferred, 
	ifnull(max(case when category = 'pa' then between_6_to_8_days_deferred else null end),0) as total_PA_between_6_to_8_days_deferred, 
    
	ifnull(max(case when category = 'active' then measure else null end),0) as total_Active, 
	ifnull(max(case when category = 'active' then within_5_days else null end),0) as total_active_less_5_days, 
	ifnull(max(case when category = 'pa' then disenroll_risk else null end),0) as total_disenroll_risk,
	ifnull(max(case when category = 'pa' then disenroll_risk_program_exclusion else null end),0) as total_disenroll_risk_program_exclusion,
	ifnull(max(case when category = 'pa' then doctorAssigned else null end),0) as total_doctorAssigned,
	ifnull(max(case when category = 'pa' then total_imts else null end),0) as total_imts
	from 
	(
		select date, b.poc, 'pa' as category, 
		count(distinct b.clientid) as measure, 
		count(distinct case when ifnull(c.labels,'') like '%SERVICE_PREFERENCE_2%' then b.clientid else null end) as deferred, 
		count(distinct b.doctor_name) as doctorAssigned, 
		count(distinct case when datediff(date, status_start_date) <= 5 then b.clientid else null end) as within_5_days,
		count(distinct case when datediff(date, status_start_date) <= 5 and ifnull(c.labels,'') like '%SERVICE_PREFERENCE_2%' then b.clientid else null end) as within_5_days_deferred,
		count(distinct case when datediff(date, status_start_date) > 8 then b.clientid else null end) as more_than_8_days, 
		count(distinct case when datediff(date, status_start_date) > 8 and ifnull(c.labels,'') like '%SERVICE_PREFERENCE_2%' then b.clientid else null end) as more_than_8_days_deferred, 

		count(distinct case when datediff(date, status_start_date) between 6 and 8 then b.clientid else null end) as between_6_to_8_days, 
		count(distinct case when datediff(date, status_start_date) between 6 and 8 and ifnull(c.labels,'') like '%SERVICE_PREFERENCE_2%' then b.clientid else null end) as between_6_to_8_days_deferred, 
        
		count(distinct if(imt.clientid is not null and category = 'DISENROLLMENT', b.clientid, null)) as disenroll_risk,
		count(distinct if(((imt.clientid is not null and subtype_l1 in ('DISENROLLMENT_RELOCATION','DISENROLLMENT_STANDARD')) or e.clientid is not null), b.clientid, null)) as disenroll_risk_program_exclusion,
		count(if(imt.clientid is not null, imt.clientid, null)) as total_imts

		from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
							  inner join v_client_tmp c on b.clientid = c.clientid 
							  left join (select distinct clientid, category, subtype_L1, date(report_time) as report_date, date(resolved_time) as resolved_date from bi_incident_mgmt where category not in ('Approval')) imt on b.clientid = imt.clientid and a.date between report_date and ifnull(resolved_date,date)
							  left join (select clientid from dim_client where medical_Exclusion_cohort is not null and is_medical_exclusion = 'Yes' and is_row_current = 'y') e on b.clientid = e.clientid

		where date >= date_sub(date(itz(now())), interval 30 day)
		and c.status not in ('registration')
		and c.patientname not like '%obsolete%'
		group by date, poc

		union all 

		select date, b.poc, 'active' as category, 
		count(distinct b.clientid) as measure, 
        null as deferred,
		count(distinct b.doctor_name) as doctorAssigned, 
		count(distinct case when datediff(status_start_date, date) <= 5 then b.clientid else null end) as within_5_days,
        null as within_5_days_deferred,
		-- count(distinct case when datediff(status_start_date, date) > 5 then b.clientid else null end) as more_than_5_days,
		null as more_than_8_days,
        null as more_than_8_days_deferred,
		null as between_6_to_8_days,
        null as between_6_to_8_days_deferred,
		0 as disenroll_risk,
		0 as disenroll_risk_program_exclusion,
		null as total_imts
		from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
							  inner join v_client_tmp c on b.clientid = c.clientid 
		where date >= date_sub(date(itz(now())), interval 30 day)
		and c.status not in ('registration')
		and c.patientname not like '%obsolete%'
		group by date, b.poc
	) s 
	group by date, s.poc
) s1 left join ( -- To get 5d Pending Active count :(
						select s1.date, s1.poc, floor(avg(s2.total_pa)) as avg_pa_5d
						from 
						(
							select date, b.poc, count(distinct b.clientid) as total_pa
							from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
												  inner join v_client_tmp c on b.clientid = c.clientid 
							where date >= date_sub(date(itz(now())), interval 30 day)
							and c.status not in ('registration')
							and c.patientname not like '%obsolete%'
							group by date, poc
						) s1 inner join 
						(
							select date, b.poc, count(distinct b.clientid) as total_pa
							from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
												  inner join v_client_tmp c on b.clientid = c.clientid 
							where date >= date_sub(date(itz(now())), interval 30 day)
							and c.status not in ('registration')
							and c.patientname not like '%obsolete%'
							group by date, poc
						) s2 on s1.poc = s2.poc and s2.date between date_sub(s1.date, interval 4 day) and s1.date
						group by s1.date, s1.poc
                ) s2 on s1.date = s2.date and s1.poc = s2.poc
	 left join ( -- To get 5d Active count :(
						select s1.date, s1.poc, floor(avg(s2.total_active)) as avg_active_5d
						from 
						(
							select date, b.poc, count(distinct b.clientid) as total_active
							from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
												  inner join v_client_tmp c on b.clientid = c.clientid 
							where date >= date_sub(date(itz(now())), interval 30 day)
							and c.status not in ('registration')
							and c.patientname not like '%obsolete%'
							group by date, poc
						) s1 inner join 
						(
							select date, b.poc, count(distinct b.clientid) as total_active
							from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
												  inner join v_client_tmp c on b.clientid = c.clientid 
							where date >= date_sub(date(itz(now())), interval 30 day)
							and c.status not in ('registration')
							and c.patientname not like '%obsolete%'
							group by date, poc
						) s2 on s1.poc = s2.poc and s2.date between date_sub(s1.date, interval 4 day) and s1.date
						group by s1.date, s1.poc
				) s3 on s1.date = s3.date and s1.poc = s3.poc
 ;