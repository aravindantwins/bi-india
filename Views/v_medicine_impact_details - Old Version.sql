alter view v_medicine_impact_details
as
select a.clientid, a.medication, 
a.actual_medication_Start as 'medication_start_date', 
'Measures_before_medication' as measure_classification, 
a.prev_day as 'measure_date', 
1 as '5d_med_indicator', 
b.glucose5dg, b.ketone5Dk, b.spike5d, b.netCarb5d, 
c.glucose3dg
from 
(
	select 	clientid, medication, 
			min(eventdateitz) as actual_medication_start, 
			max(Eventdateitz) as actual_medication_end,
			date_sub(min(Eventdateitz), interval 1 day) as prev_day
	from patientescalation 
	where medication is not null 
	group by medication, clientid
) a left join patientescalation b on a.clientid = b.clientid and a.prev_day = b.eventdateitz
	left join (
					select 	clientid, measure_event_date, 
							max(Glocose3d) as glucose3dg, 
							max(Ketone3d) as ketone3dk, 
							max(Spikes3d) as spike3d,
							max(Netcarb3d) as netcarb3d
					from 
					(
						select 	clientid, 
								case when measure_name = 'Glucose' then average_measure end as 'Glocose3d', 
								case when measure_name = 'Ketone' then average_measure end as 'Ketone3d',
								case when measure_name = 'Netcarb' then average_measure end as 'Netcarb3d',
								case when measure_name = 'Spikes' then average_measure end as 'Spikes3d'
								,measure_event_date
						from bi_measures
						where measure_type = '3d'
					) a
					group by clientid, measure_event_date
			) c on a.clientid = c.clientid and a.prev_day = c.measure_Event_Date

union all 

select a.clientid, a.medication, 
a.actual_medication_Start as 'medication_start_date', 
'Measures_after_medication' as measure_classification, 
b.eventdateitz as 'measure_date',
case when b.eventdateitz = a.future_5_days then 1 else 0 end as '5d_med_indicator',  
b.glucose5dg, b.ketone5Dk, b.spike5d, b.netCarb5d, 
c.glucose3dg
from 
(
	select 	clientid, medication, 
			min(eventdateitz) as actual_medication_start, 
			max(Eventdateitz) as actual_medication_end,
			date_add(min(Eventdateitz), interval 5 day) as future_5_days,
			date_add(min(eventdateitz), interval 3 day) as future_3_days
	from patientescalation
	where medication is not null  
	group by medication, clientid
) a left join patientescalation b on a.clientid = b.clientid and b.eventdateitz >= actual_medication_start and b.eventdateitz <= date(itz(now()))
	left join (
				select 	clientid, measure_event_date, 
						max(Glocose3d) as glucose3dg
                        -- ,max(Ketone3d) as ketone3dk, 
						-- max(Spikes3d) as spike3d,
						-- max(Netcarb3d) as netcarb3d
                from 
                (
						select 	clientid, 
								case when measure_name = 'Glucose' then average_measure end as 'Glocose3d', 
								case when measure_name = 'Ketone' then average_measure end as 'Ketone3d',
								case when measure_name = 'Netcarb' then average_measure end as 'Netcarb3d',
								case when measure_name = 'Spikes' then average_measure end as 'Spikes3d'
								,measure_event_date
						from bi_measures
						where measure_type = '3d'
                ) a
                group by clientid, measure_event_date
			) c on a.clientid = c.clientid and c.measure_Event_Date = b.eventdateitz
order by clientid asc, medication asc, measure_classification desc, measure_date asc 