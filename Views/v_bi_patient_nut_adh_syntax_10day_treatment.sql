create view v_bi_patient_nut_adh_syntax_10day_treatment as 
select s1.date, s1.clientid, s6.patientname, s1.treatmentday,  
round(s21.macro) as macro, 
round(s21.micro) as micro, 
round(s21.biota) as biota, 
s3.tac_score, 
s7.nut_syntax as allowed_syntax, 
s1.nut_syntax, s1.nut_syntax_color, 
s1.nut_adh_syntax, s1.nut_adh_syntax_5d
from 	
(
	select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, a.date, nut_syntax, nut_syntax_color, nut_adh_syntax_5d, nut_adh_syntax
	nut_adh_syntax
	from bi_patient_reversal_state_gmi_x_date a
	where a.treatmentdays between 1 and 11 
) s1 left join nutritionscores s21 on s1.clientid = s21.clientid and s1.date = s21.date
	 left join bi_tac_measure s3 on s1.clientid = s3.clientid and s1.date = s3.measure_date
     inner join v_client_tmp s6 on s1.clientid = s6.clientid
     left join dim_client s7 on s1.clientid = s7.clientid and s7.is_row_current = 'y'
; 
