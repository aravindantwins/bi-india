create view v_bi_overall_topmeal_actual_x_prediction as 
select category, mealID, foodsInMeal, total_logged_overall, total_logged, total_predicted, total_patients, MeanActualAUC, MeanPredictedAUC, MeanAbsoluteError, 
count_measured_auc_less_than_threshold, count_predicted_auc_less_than_threshold, count_precision, count_recall
from bi_overall_topmeal_actual_x_prediction
where total_logged > 1
;