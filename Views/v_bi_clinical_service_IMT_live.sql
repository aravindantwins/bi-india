set collation_connection = 'latin1_swedish_ci';


alter view v_bi_clinical_service_IMT_live as 
select a.clientid, b.patientName, b.coachNameShort as coach, b.morningInternalDoctorName, b.doctorname,
case when a.severity = 3 then 'Severe' 
	 when a.severity = 2 then 'Moderate'
	 when a.severity = 1 then 'Mild' end as severity,
     grievance,
incidentType, 
subtypel1,
itz(a.eventtime) as reportTime ,
case when a.incidenttype='SYMPTOMS_HANDLING' and a.severity=3 then timestampadd(minute,30,itz(a.eventtime)) 
     when a.incidenttype='SYMPTOMS_HANDLING' and a.severity=2 then timestampadd(hour,1,itz(a.eventtime))
     when a.incidenttype='SYMPTOMS_HANDLING' and a.severity=1 then timestampadd(hour,12,itz(a.eventtime))
     when a.incidenttype='PATIENT_TREATMENT' and (a.subtypel1='PATIENT_TREATMENT_BETA_HYDROXYBUTYRATE' or
												  a.subtypel1='PATIENT_TREATMENT_HYPERGLYCEMIA' or
                                                  a.subtypel1='PATIENT_TREATMENT_HYPOGLYCEMIA'  or
                                                  a.subtypel1='PATIENT_TREATMENT_BLOOD_PRESSURE') then timestampadd(minute,30,itz(a.eventtime)) 
	when a.incidenttype='PATIENT_TREATMENT' then timestampadd(hour,12,itz(a.eventtime)) end as halfTimeTrigger,
    
case when a.incidenttype='SYMPTOMS_HANDLING' and a.severity=3 then timestampadd(hour,1,itz(a.eventtime)) 
     when a.incidenttype='SYMPTOMS_HANDLING' and a.severity=2 then timestampadd(hour,2,itz(a.eventtime))
     when a.incidenttype='SYMPTOMS_HANDLING' and a.severity=1 then timestampadd(hour,24,itz(a.eventtime)) 
     when a.incidenttype='PATIENT_TREATMENT' and (a.subtypel1='PATIENT_TREATMENT_BETA_HYDROXYBUTYRATE' or
												  a.subtypel1='PATIENT_TREATMENT_HYPERGLYCEMIA' or
                                                  a.subtypel1='PATIENT_TREATMENT_HYPOGLYCEMIA'  or
                                                  a.subtypel1='PATIENT_TREATMENT_BLOOD_PRESSURE') then timestampadd(hour,1,itz(a.eventtime)) 
	 when a.incidenttype='PATIENT_TREATMENT' then timestampadd(hour,24,itz(a.eventtime)) end as FRT_Expected,
     c.first_response_time as FRT_Actual,
     timestampdiff(hour,itz(a.eventtime),itz(now())) as openTime,
if(a.incidentRecipientTypes like '%|DOCTOR|%', 'Yes', null) as isDoctorTagged
from incident a 
join v_allclient_list b on a.clientid=b.clientid 
left join (	select a.incidentId, a.owner_entry_date, min(itz(b.dateadded)) as first_response_time 
			from 
			(
				 select a.incidentId, min(itz(a.dateadded)) as owner_entry_date 
				 from incidentcomment a inner join incident b on a.incidentID = b.incidentID
				 where b.incidentstatus='open' and (b.incidenttype= 'PATIENT_TREATMENT' or b.incidenttype='SYMPTOMS_HANDLING')
				 and date(itz(b.eventtime)) >= date_sub(date(itz(now())), interval 7 day)
				 group by incidentId
			) a left join incidentcomment b on a.incidentId = b.incidentId and itz(b.dateadded) > a.owner_entry_date
			group by a.incidentId, a.owner_entry_date
		  )c on c.incidentId=a.incidentId
where a.incidentstatus='open' 
      and (a.incidenttype= 'PATIENT_TREATMENT' or  a.incidenttype='SYMPTOMS_HANDLING')
      and date(itz(a.eventtime)) >= date_sub(date(itz(now())), interval 7 day)
; 
