alter view v_bi_patient_service_activation_tracker_allPatients as
select a.clientid, concat(a.clientid,' | ',ifnull(plancode,''),
							', BP Meter - ',if(b2.clientid is not null, 'Yes', 'No'),
                            ', BHB Meter - ',if(b1.clientid is not null, 'Yes', 'No'), 
                            ifnull(concat(', ',i.latest_Medicine_Published),'')) as Task_description,
poc,
coach,
email, 
patientname, 
address,
city,
postalCode,
mobileNumber,
-- pd.homeVisit_scheduled_date as HV_scheduled_date, 
-- date_add(pd.homeVisit_scheduled_date, interval 2 hour) as HV_scheduled_date_plus_2_hrs,
plancode, 
a.status,
e.active_status_start,
workEmailId,  workPhoneNumber, homePhoneNumber,
IF(imt.clientid IS NOT NULL and total_disenroll_tickets > 0, 'Yes', 'No') AS is_disenroll_risk,
IF((imt.clientid IS NOT NULL and total_spl2_tickets > 0) OR (oh.clientid is not null), 'Yes', 'No') AS Previously_Deferred,
i.first_fitbit_sync_date,
i.first_medicine_published_date,
i.sensorActivation_schedule_date,
i.first_BCM_sync_date AS BCM_2nd_sync_date,
i.first_BP_sync_date as BP_first_sync_date,
i.first_BHB_sync_date as BHB_sync_date 
from 
(
	select c.clientid, c.patientname, c.poc,c.coachName as coach, address, city, postalCode, mobileNumber, email, c.plancode, c.status, workEmailId,  workPhoneNumber, homePhoneNumber
	from v_allclient_list c                                                                 
	where c.status IN ('PENDING_ACTIVE' , 'ACTIVE', 'DISCHARGED', 'On_hold', 'INACTIVE')
    and c.patientname not like '%obsolete%'
) a left join clienttodoschedules b1 on a.clientid = b1.clientid and b1.category = 'TEST' and b1.type in ('GLUCOSE_KETONE', 'KETONE','GLUCOSE') and b1.enddate > date(itz(now()))
	left join clienttodoschedules b2 on a.clientid = b2.clientid and b2.category = 'TEST' and b2.type = 'BLOOD_PRESSURE' and b2.enddate > date(itz(now()))
   /*
   LEFT JOIN 
	(
			select a.clientid, 
            max(itz(a.eventTime)) as homeVisit_scheduled_date
			from clientappointments a left join homevisits b on a.clientid = b.clientid and a.clientAppointmentId = b.clientAppointmentId
			where a.type = 'HOME_VISIT'
            group by a.clientid 
    ) pd on a.clientid = pd.clientid
   */
	      
	LEFT JOIN (
				SELECT clientid, 
                count(distinct case when category = 'disenrollment' and status = 'open' then incidentid else null end) as total_disenroll_tickets, 
				count(distinct case when category = 'Approval' and custom_subtype = 'SPL' then incidentid else null end) as total_spl2_tickets
				from bi_incident_mgmt 
				group by clientid 
			  ) imt on a.clientid = imt.clientid
  LEFT JOIN ( select clientid from bi_patient_status where status = 'on_hold') oh on a.clientid = oh.clientid 
  LEFT JOIN (SELECT 
            clientid,
                MAX(status_start_date) AS active_status_start
        FROM
            bi_patient_status
        WHERE
            status = 'active'
        GROUP BY clientid) e ON a.clientid = e.clientid
  LEFT JOIN bi_patient_initial_setup_x_dates i on (a.clientid=i.clientid)
; 

/* OLD Definition 

select a.clientid, concat(a.clientid,' | ',ifnull(plancode,''),
							', BP Meter - ',if(b2.clientid is not null, 'Yes', 'No'),
                            ', BHB Meter - ',if(b1.clientid is not null, 'Yes', 'No'), 
                            ifnull(concat(', ',pr.latest_Medicine_Published),'')) as Task_description,
poc,
coach,
email, 
patientname, 
address,
city,
postalCode,
mobileNumber,
-- pd.homeVisit_scheduled_date as HV_scheduled_date, 
-- date_add(pd.homeVisit_scheduled_date, interval 2 hour) as HV_scheduled_date_plus_2_hrs,
sa.sensorActivation_scheduled_date as SA_scheduled_date,
fb.first_fitbit_sync_Date,
plancode, 
a.status,
e.active_status_start,
workEmailId,  workPhoneNumber, homePhoneNumber,
IF(imt.clientid IS NOT NULL and total_disenroll_tickets > 0, 'Yes', 'No') AS is_disenroll_risk,
IF((imt.clientid IS NOT NULL and total_spl2_tickets > 0) OR (oh.clientid is not null), 'Yes', 'No') AS Previously_Deferred,
f.First_Medicine_Published_Date as First_Medicine_Published_Date,
bp.bp_sync_date as bp_first_sync_date,
bhb.BHB_sync_date as BHB_sync_date,
w.BCM_date AS BCM_2nd_sync_date
from 
(
	select c.clientid, c.patientname, c.poc, address, city, postalCode, mobileNumber, email, c.plancode, workEmailId,  workPhoneNumber, homePhoneNumber, status, coachnameShort as coach
	from v_allclient_list c                                                                 
	where c.status IN ('PENDING_ACTIVE' , 'ACTIVE', 'DISCHARGED', 'On_hold', 'INACTIVE')
    and c.patientname not like '%obsolete%'
) a left join clienttodoschedules b1 on a.clientid = b1.clientid and b1.category = 'TEST' and b1.type in ('GLUCOSE_KETONE', 'KETONE','GLUCOSE') and b1.enddate > date(itz(now()))
	left join clienttodoschedules b2 on a.clientid = b2.clientid and b2.category = 'TEST' and b2.type = 'BLOOD_PRESSURE' and b2.enddate > date(itz(now()))
  
   LEFT JOIN 
	(
			select a.clientid, 
            max(itz(a.eventTime)) as homeVisit_scheduled_date
			from clientappointments a left join homevisits b on a.clientid = b.clientid and a.clientAppointmentId = b.clientAppointmentId
			where a.type = 'HOME_VISIT'
            group by a.clientid 
    ) pd on a.clientid = pd.clientid
  LEFT JOIN 
	(
			select a.clientid, 
            max(itz(a.eventTime)) as sensorActivation_scheduled_date
			from clientappointments a left join homevisits b on a.clientid = b.clientid and a.clientAppointmentId = b.clientAppointmentId
			where a.type = 'SENSOR_ACTIVATION'
            group by a.clientid 
    ) sa on a.clientid = sa.clientid
    LEFT JOIN
    (
					select a.clientid, a.latest_medicine_Published_Date, ifnull(b.actualmedication,'') as latest_Medicine_Published
					from
					(
						Select clientid, max(itz(dateadded)) as latest_medicine_Published_Date from medicationhistories where type = 'DIABETES' group by clientid
					) a inner join medicationhistories b on a.clientid = b.clientid and a.latest_medicine_Published_Date = itz(dateadded) and type = 'DIABETES'
	) pr on a.clientid = pr.clientid
    left join 
    (
				select a.clientid, min(itz(a.dateadded)) as First_Fitbit_Sync_Date 
                from sensors a 
				where a.sensorModel in ('FITBIT')
				group by a.clientid
	) fb on a.clientid = fb.clientid 
    
	LEFT JOIN (
				SELECT clientid, 
                count(distinct case when category = 'disenrollment' and status = 'open' then incidentid else null end) as total_disenroll_tickets, 
				count(distinct case when category = 'Approval' and custom_subtype = 'SPL' then incidentid else null end) as total_spl2_tickets
				from bi_incident_mgmt 
				group by clientid 
			  ) imt on a.clientid = imt.clientid
    	
    LEFT JOIN (
					select clientid, min(itz(eventtime)) AS BCM_DATE
					from  weights
					where source in ('direct sync')
					group by clientid
			  ) w on (a.clientid = w.clientid)
              
   LEFT JOIN ( 
						select clientid, min(itz(eventtime)) as bp_sync_date 
						from bloodpressurevalues
						group by clientid 
			  ) BP on bp.clientid= a.clientid
   LEFT JOIN ( 
						select clientid, min(itz(eventtime)) as BHB_sync_date
						from  glucoseketones
						group by clientid
			 ) bhb on bhb.clientid = a.clientid
  LEFT JOIN (
				SELECT clientid, MIN(ITZ(dateAdded)) AS First_Medicine_Published_Date
				FROM
				medicationhistories
                group by clientid 
			) f ON f.clientid = a.clientid
  LEFT JOIN ( select clientid from bi_patient_status where status = 'on_hold') oh on a.clientid = oh.clientid 
  LEFT JOIN (
				SELECT clientid, MAX(status_start_date) AS active_status_start
				FROM
				bi_patient_status
				WHERE status = 'active'
				GROUP BY clientid
			) e ON a.clientid = e.clientid


*/
