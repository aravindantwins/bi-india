alter view v_bi_Member1_patient_summary_tracker_V2 as 
-- This view will track patients from D6 - 11
select s1.date, s1.coach,  s1.Leadcoach,

s4.D0_count as Active_D0_cnt,

s4.D6_11_count as Active_D6_11_cnt,
s4.D6_11_NoSchedule as Active_more_than_D6_noReview, 
s4.D6_11_Scheduled as D6_11_Scheduled, 
s4.D6_11_RC as D6_11_Scheduled_and_Completed,
s4.D6_11_RC_NoSchedule as D6_11_NoSchedule_and_Completed,
s4.D6_11_Declined_Schedule as D6_11_Declined_Scheduled,
s4.D6_11_Pending_Schedule as D6_11_Pending_Schedule,
s4.D6_11_Pending_Schedule_NotConverted as D6_11_Pending_Schedule_NotConverted,

s4.D6_11_NoSchedule as D6_11_NoSchedule, 
s4.D6_10_trial_outcome_achieved as D6_10_trial_outcome_achieved, 

s4.D10_count as Active_D10_cnt,
s4.D10_NoSchedule as Active_D10_No_Schedule,
s4.D10_RC + s4.D10_RC_NoSchedule as Total_review_completed_d10_reached,
s4.D11_RC + s4.D11_RC_NoSchedule as Total_review_completed_d11_reached,

s5.scheduled_measure as Scheduled_cnt, 
s5.Cancelled_measure as Cancelled_cnt,

s7.converted_measure as Converted_cnt, 
s7.coachConverted_measure as CoachConverted_cnt, 
s7.Converted_between_10_13_days_measure as Converted_10_13_days_cnt,
s7.Converted_within_10days_measure as Converted_within_10days_cnt,
s7.Converted_within_11days_measure as Converted_within_11days_cnt,

s9.review_completed_measure as Review_cnt,
s9.review_completed_with_LTC_measure as Review_with_LTC_cnt,

s8.measure as Discharged_cnt, 

s10.total_d10 as D10_reached_cnt,
s10.d10_success_measure as D10_success_cnt,
s10.d10_outcome_achieved_measure as d10_outcome_achieved_cnt,
s10.d10_schedule_not_created_yet as D10_schedule_not_created_yet

from 
(
	select date, b.coach, b.Leadcoach
	from v_date a cross join (select distinct coachnameShort as coach, leadCoachNameShort as Leadcoach from v_client_tmp) b 
	where date >= '2021-03-01' and (date between date_sub(date(now()), interval 60 day) and date_add(date(now()), interval 3 day))
    -- and b.coach is not null
) s1 left join 
            (
						select date, coach, 
                        count(distinct case when treatmentday in ('D0') then clientid else null end) as D0_count,
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') then clientid else null end) as D6_11_count, 
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D6_11_RC,
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and scheduled_review_date is null and is_d10_review_completed = 'Yes' then clientid else null end) as D6_11_RC_NoSchedule,
						
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and scheduled_review_date is not null then clientid else null end) as D6_11_Scheduled,
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and scheduled_review_date is null and is_d10_review_completed is null then clientid else null end) as D6_11_NoSchedule,
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and (scheduled_review_date is null or (scheduled_review_date is not null and d10_review_schedule_status = 'CANCELLED')) and is_d10_review_completed is null and isDisenrollOpen = 'Yes' then clientid else null end) as D6_11_Declined_Schedule,
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and (scheduled_review_date is null or (scheduled_review_date is not null and d10_review_schedule_status = 'CANCELLED')) and is_d10_review_completed is null and isDisenrollOpen = 'No' then clientid else null end) as D6_11_Pending_Schedule,
						-- count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and scheduled_review_date is null and is_d10_review_completed is null and isDisenrollOpen = 'No' and category = 'Not Converted' then clientid else null end) as D6_11_Pending_Schedule_NotConverted,
						-- count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and scheduled_review_date is null and is_d10_review_completed is null and isDisenrollOpen = 'No' and category = 'Converted' then clientid else null end) as D6_11_Pending_Schedule_Converted,

						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and (scheduled_review_date is null or (scheduled_review_date is not null and d10_review_schedule_status = 'CANCELLED')) and is_d10_review_completed is null and isDisenrollOpen = 'No' and category = 'Not Converted' then clientid else null end) as D6_11_Pending_Schedule_NotConverted,
						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and (scheduled_review_date is null or (scheduled_review_date is not null and d10_review_schedule_status = 'CANCELLED')) and is_d10_review_completed is null and isDisenrollOpen = 'No' and category = 'Converted' then clientid else null end) as D6_11_Pending_Schedule_Converted, 

						count(distinct case when treatmentday in ('D6','D7','D8','D9','D10','D11') and scheduled_review_date is not null and is_d10_review_completed is null then clientid else null end) as D6_11_ScheduleAvailable_yet_to_complete,
                        
                        count(distinct case when treatmentday in ('D6','D7','D8','D9','D10') and trialOutCome_Achieved = 'Yes' then clientid else null end) as D6_10_trial_outcome_achieved,
                        
                        count(distinct case when treatmentday in ('D10') then clientid else null end) as D10_count, 
						count(distinct case when treatmentday in ('D11') then clientid else null end) as D11_count, 
						count(distinct case when treatmentday in ('D10') and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D10_RC,
						count(distinct case when treatmentday in ('D11') and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D11_RC,
						count(distinct case when treatmentday in ('D10') and scheduled_review_date is null and is_d10_review_completed = 'Yes' then clientid else null end) as D10_RC_NoSchedule,
						count(distinct case when treatmentday in ('D11') and scheduled_review_date is null and is_d10_review_completed = 'Yes' then clientid else null end) as D11_RC_NoSchedule,
						count(distinct case when treatmentday in ('D11') and scheduled_review_date is not null then clientid else null end) as D11_Scheduled,

						count(distinct case when treatmentday in ('D10') and scheduled_review_date is null and is_d10_review_completed is null then clientid else null end) as D10_NoSchedule, 
						count(distinct case when treatmentday in ('D10') and scheduled_review_date is not null and is_d10_review_completed is null then clientid else null end) as D10_ScheduleAvailable_yet_to_complete

						from 
						(
								select distinct a.clientid, 'Not Converted' as category, concat('D',(a.treatmentdays - 1)) as treatmentday, d.coachnameShort as coach, a.date, c.d10_review_date as scheduled_review_date, c.is_d10_review_completed, c.is_member_likely_to_convert, c.d10_review_schedule_status, if(inc.clientid is not null, 'Yes', 'No') as isDisenrollOpen,
                                if((a.cgm_5d < 140 or ((cast((dc.start_labA1C * 28.7) - 46.7 as decimal(10,5))) - a.cgm_5d >= 50)) 
									and (ifnull(if(a.med_adh = 'NO', 0, a.actual_medCount),0) = 0 or (ifnull(a.actual_medCount,0) <= ifnull(a.start_medCount,0)))
									,'Yes', 'No') as trialOutCome_Achieved
								from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
																			inner join dim_client dc on c.clientid = dc.clientid and dc.is_row_current = 'y'
																			left join v_client_tmp d on a.clientid = d.clientid
																			left join (			
																						select s.clientid, date(itz(s1.report_time)) as openDate, if(s1.status = 'OPEN', date(itz(now())), date(itz(s1.resolved_time))) as resolvedDate 
																						from
																						(
																							select distinct clientid, max(report_time) as report_time 
																							from bi_incident_mgmt where category = 'Disenrollment' 
																							group by clientid
																						) s inner join bi_incident_mgmt s1 on s.clientid = s1.clientid and s.report_time = s1.report_time
																					   ) inc on a.clientid = inc.clientid and a.date <= ifnull(resolvedDate,date(itz(now())))
								where a.treatmentdays in (1,7,8,9,10,11,12) -- D6 - 11
								and c.is_patient_converted is null
								and d.patientname not like '%obsolete%'
								and d.status not in ('registration', 'pending_active')
								and (d.labels not like '%TWIN_PRIME%' and d.labels not like '%CONTROL%')
								
								union all
								
								select a.clientid, 'Converted' as category, concat('D',(a.treatmentdays - 1)) as treatmentday, d.coachnameShort as coach, a.date, c.d10_review_date as scheduled_review_date, c.is_d10_review_completed, is_member_likely_to_convert, c.d10_review_schedule_status, if(inc.clientid is not null, 'Yes', 'No') as isDisenrollOpen,
                                if((a.cgm_5d < 140 or ((cast((dc.start_labA1C * 28.7) - 46.7 as decimal(10,5))) - a.cgm_5d >= 50)) 
									and (ifnull(if(a.med_adh = 'NO', 0, a.actual_medCount),0) = 0 or (ifnull(a.actual_medCount,0) <= ifnull(a.start_medCount,0)))
									,'Yes', 'No') as trialOutCome_Achieved
								from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
																			inner join dim_client dc on c.clientid = dc.clientid and dc.is_row_current = 'y'
																			left join v_client_tmp d on a.clientid = d.clientid
																			left join (			
																						select s.clientid, date(itz(s1.report_time)) as openDate, if(s1.status = 'OPEN', date(itz(now())), date(itz(s1.resolved_time))) as resolvedDate 
																						from
																						(
																							select distinct clientid, max(report_time) as report_time 
																							from bi_incident_mgmt where category = 'Disenrollment' 
																							group by clientid
																						) s inner join bi_incident_mgmt s1 on s.clientid = s1.clientid and s.report_time = s1.report_time
																					   ) inc on a.clientid = inc.clientid and a.date <= ifnull(resolvedDate,date(itz(now())))
								where a.treatmentdays in (1,7,8,9,10,11,12) -- D6 - 11
								and c.is_patient_converted = 'Yes'
								and d.patientname not like  '%obsolete%'
								and d.status not in ('registration', 'pending_active')
								and (d.labels not like '%TWIN_PRIME%' and d.labels not like '%CONTROL%')
						) s1
						where date >= '2021-03-01' and (date between date_sub(date(now()), interval 60 day) and date_add(date(now()), interval 3 day))
						group by date, coach
            ) s4 on  s1.date = s4.date and s1.coach = s4.coach
	left join 
            (
					select date, coach, 
                    count(distinct case when d10_review_schedule_status in ('SCHEDULED', 'COMPLETED', 'CANCELLED') then clientid else null end) as scheduled_measure, 
                    count(distinct case when d10_review_schedule_status = 'CANCELLED' then clientid else null end) as cancelled_measure
					from v_date a left join -- Using v_date here because we want future 3 days
					(
						select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, d10_review_schedule_status from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
					) b on a.date = d10_review_schedule_date
					where date >= date_sub(date(now()), interval 60 day) and date <= date_add(date(now()), interval 3 day)
					group by date, coach
            ) s5 on  s1.date = s5.date and s1.coach = s5.coach
    	left join 
            (
				select date, coach, 'CONVERTED' as status, count(distinct b.clientid) as converted_measure, 
                count(distinct case when isCoachConverted = 'Yes' then clientid else null end) as coachConverted_measure, 
                count(distinct case when daysToConvert < 14 then clientid else null end) as Converted_within_14days_measure,
				count(distinct case when daysToConvert between 10 and 13 then clientid else null end) as Converted_between_10_13_days_measure,
                count(distinct case when daysToConvert < 10 then clientid else null end) as Converted_within_10days_measure,
				count(distinct case when daysToConvert >= 17 then clientid else null end) as Converted_after_17days_measure,
				count(distinct case when daysToConvert <= 11 then clientid else null end) as Converted_within_11days_measure 
				from v_date_to_date a inner join 
				(
					select a.clientid, b.coachnameShort as coach, conversion_payment_date, isCoachConverted, datediff(conversion_payment_date, if(visitDate>active_status_start_date, visitdate, active_status_start_date)) as daysToConvert from 
					bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
										 inner join (select distinct clientid, date(visitdateitz) as visitDate from bi_patient_status where status = 'active') c on a.clientid = c.clientid 
					where is_patient_converted = 'Yes'
					and b.patientname not like '%obsolete%'
                    and b.status not in ('registration', 'pending_active')
					and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
				) b on a.date = conversion_payment_date
				where date >= date_sub(date(now()), interval 60 day) 
				group by date, coach
            ) s7 on  s1.date = s7.date and s1.coach = s7.coach        
	left join 
            (
					select date, coach, 'DISCHARGED' as status, count(distinct b.clientid) as measure
					from v_date_to_date a inner join 
					(
						select clientid, coachname_short as coach, status_start_date, status_end_date from 
						bi_patient_status b 
						where status = 'discharged' 
						and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
						and clientid in (select clientid from bi_ttp_enrollments where status in ('discharged', 'inactive') and is_patient_converted is null  and is_program_exclusion is null)   -- to match the TTP tracker summary dashboard
						and exists (select 1 from bi_patient_status d where b.clientid = d.clientid and d.status = 'active')
                    ) b on a.date = b.status_start_date 
					where date >= date_sub(date(now()), interval 60 day) 
					-- and coach is not null
					group by date, coach
            ) s8 on  s1.date = s8.date and s1.coach = s8.coach
	left join 
            (
					select date, coach, 'REVIEW' as status, count(distinct case when is_d10_review_completed = 'Yes' then clientid else null end) as review_completed_measure, 
                    count(distinct case when is_d10_review_completed = 'Yes' and is_member_likely_to_convert in ('Yes') then clientid else null end) as review_completed_with_LTC_measure
					from v_date_to_date a inner join 
					(
						select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, is_d10_review_completed, is_member_likely_to_convert
                        from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
					) b on a.date = d10_review_schedule_date
					where date >= date_sub(date(now()), interval 60 day) 
					group by date, coach
            ) s9 on s1.date = s9.date and s1.coach = s9.coach
	left join 
            (
					select date, coach, 'D10 SUCCESS/OUTCOME' as status, 
					count(distinct clientid) as total_d10,
                    count(distinct case when is_d10_success = 'Yes' then clientid else null end) as d10_success_measure, 
                    count(distinct case when isD10_outcome_achieved = 'Yes' then clientid else null end) as d10_outcome_achieved_measure,
                    count(distinct case when d10_review_schedule_created_date is null then clientid else null end) as d10_schedule_not_created_yet
					from v_date_to_date a inner join 
					(
						select a.clientid, b.coachnameShort as coach, d10_date, is_d10_success, isD10_outcome_achieved, d10_review_schedule_created_date
                        from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
					) b on a.date = d10_date
					where date >= date_sub(date(now()), interval 60 day) 
					group by date, coach
            ) s10 on s1.date = s10.date and s1.coach = s10.coach

;