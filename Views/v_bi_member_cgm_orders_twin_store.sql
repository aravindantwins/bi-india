alter view v_bi_member_cgm_orders_twin_store as 
SELECT
         b.clientid,
		 b.patientName,
		 b.TSA,
         b.coach,
		 b.City,
		 b.Labels,
		 b.PlanCode,
		 ifnull(b.payment_date, b.invoice_date) AS CGM_Purchase_Date,
         b.No_Of_Week_Patch,
         b.Invoice_Amount,
		 a.treatmentDays,
         c.isCBGEligible
FROM
(SELECT
        d.clientid ,
            d.patientname,
            d.city_new AS City,
            d.labels AS Labels,
            d.plancode AS PlanCode,
            b.date AS Payment_Date,
            a.cf_service_purchased AS No_Of_Week_Patch,
            d.TSA,
            d.coachnameShort AS coach,
            a.total AS Invoice_Amount,
            a.invoice_date
    FROM
        importedbilling.zoho_invoices a
    LEFT JOIN importedbilling.zoho_payments b ON a.invoice_id = b.invoice_id
    LEFT JOIN importedbilling.zoho_customers c ON a.customer_id = c.customer_id
    LEFT JOIN twins.v_allclient_list D ON D.clientId = c.cf_clientid_unformatted
    WHERE
        a.cf_service_purchased IS NOT NULL
        and a.status = 'PAID'
    ) b
LEFT JOIN
    dim_client a ON b.clientid=a.clientid AND a.is_row_current = 'Y'
    LEFT JOIN   bi_patient_reversal_state_gmi_x_date c ON a.clientid=c.clientid AND c.date =DATE_SUB(date(now()), INTERVAL 1 DAY)
;