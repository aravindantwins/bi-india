-- set collation_connection = 'latin1_swedish_ci';

create VIEW v_bi_patient_blood_draw_completion_detail AS
select distinct a.clientid, v.patientName, v.email, v.mobileNumber, a.eventLocalTime as blooddrawdate, b.bloodworkday, 
case when c.labreportid is not null then itz(l.dateadded) else itz(a.eventtime) end as date_uploaded, 
datediff(case when c.labreportid is not null then itz(l.dateadded) else itz(a.eventtime) end, a.eventLocalTime) as TAT
from clientappointments a 
inner join bloodworkschedules b on a.clientAppointmentId = b.clientAppointmentId and a.type = 'BLOOD_WORK' and a.status='COMPLETED' 
inner join (
			select clientid, bloodworktime, eventtime, labreportid
			from clinictestresults
			where deleted=0
			) c on a.clientid=c.clientid and date(a.eventlocaltime) between date_sub(itz(c.bloodworktime), interval 2 day) and date_add(itz(c.bloodworktime), interval 7 day)
left join labreports l on c.labReportId = l.labReportId
inner join v_client_tmp v on (a.clientid=v.clientid) 
where a.eventlocaltime >= date_sub(itz(now()), interval 30 day)
;