alter view v_bi_TTP_patient_detail as 
select a.clientid, patientname, b.doctorname, pha, RPA, 
b.status, b.mobilenumber, a.enrollmentdateitz, is_patient_currently_TTP, is_patient_converted, conversion_payment_date, 
conversion_payment_amount, is_program_exclusion, active_status_start_date, b.plancode as current_plancode, 
treatmentDays, c.visitScheduledDate as visitDate, 
date_add(if(visitScheduledDate > active_status_start_date, visitScheduledDate, active_status_start_date), interval 1 day) as treatmentDay1
from bi_ttp_enrollments a inner join v_allClient_list b on a.clientid = b.clientid 
						  left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'
where patientname not like '%obsolete%'
;
