create view v_bi_patient_count_x_status
as 
select s1.date, s1.doctorname, 
max(case when s1.status = 'Enrolled' then measure end) as Enrolled_cnt, 
max(case when s1.status = 'Active' then measure end) as Active_cnt, 
max(case when s1.status = 'Discharged' then measure end) as Discharged_cnt, 
max(case when s1.status = 'Inactive' then measure end) as Inactive_Cnt,
max(case when s1.status = 'Pending_active' then measure end) as Pending_Active_Cnt,
max(case when s1.status = 'Cum_Active' then measure end) as Cumulative_Active_Cnt

-- max(case when s1.status = 'Re-active' then measure end) as Reactive_Cnt
from 
(select date, doctor_name as doctorname, c.status
from v_date_to_date a cross join (select distinct doctor_name from bi_patient_Status) b 
					  cross join (select 'Active' as status union all select 'Enrolled' as status union all select 'Inactive' as status union all select 'discharged' as status union all select 'Re-active' as status union all select 'pending_active' as status union all select 'Cum_Active' as status) c
) s1
left join 
(
	select date, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select s.clientid, s.enrollmentdateitz, s.doctorName, s.status
        from 
        (
				select distinct clientid, enrollmentdateitz, doctor_name as doctorName, if(doctor_name is null, null, 'Enrolled') as status from 
				bi_patient_status b 
				where status = 'active'
				and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
				
				union all 
				
				-- Consider any patient who is in the system with PENDING_ACTIVE status but they have completed their enrollment. 
				select distinct clientid, enrollmentdateitz, doctor_name as doctorName, if(doctor_name is null, null, 'Enrolled') as status from 
				bi_patient_status b 
				where status = 'PENDING_ACTIVE'
				and clientid not in (select distinct clientid from bi_patient_status where status = 'active')
				and clientid not in (select distinct clientid from v_client_Tmp where status = 'registration') -- patients are going back to registration status from pending_active but not getting captured in audit trail 
				and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
        ) s inner join clients s1 on s.clientid = s1.id 
        where s1.deleted = 0
	) b on date(enrollmentdateitz) <= a.date
	group by date, doctorName, b.status

	union all 
    
	select date, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, enrollmentdateitz, status_start_date, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'Active') as status from 
		bi_patient_status b 
		where status = 'active'
		and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
	) b on a.date between b.status_start_date and b.status_end_date
	group by date, doctorName, b.status

	union all 

	select date, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, enrollmentdateitz, status_start_date, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'pending_active') as status from 
		bi_patient_status 
		where status = 'pending_active'
        and clientid not in (select distinct clientid from v_client_tmp where status = 'registration')  -- patients are going back to registration status from pending_active but not getting captured in audit trail 
		and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
    ) b on a.date between b.status_start_date and b.status_end_date
	group by date, doctorName, b.status

    union all 
    
	select date, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select clientid, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status from 
		bi_patient_status b 
		where status = 'discharged' 
		and clientid not in (select clientid from bi_patient_Status where status = 'inactive')
        -- and clientid not in (select clientid from bi_patient_Status where status = 'active' and status_end_date = date(itz(now()))) -- old logic ==> not a good one because once the reactivated patient is set to discharged again, the old discharged record also comes into calculation because = NOW condition fails
        and not exists (select 1 from bi_patient_status a where a.clientid = b.clientid and a.status = 'active' and a.status_start_date >= b.status_end_date) -- to consider only the reactivated status record
        and not exists (select 1 from bi_patient_status c where b.clientid = c.clientid group by c.clientid having count(case when c.status in ('active', 'pending_active') then c.clientid else null end) = 0 ) -- to ignore patients who are getting discharged directly before reaching Pending active at least
		and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
    ) b on date(status_start_date) <= a.date
	group by date, doctorName, b.status
	
    union all 
    
	select date, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, date_add(status_end_date, interval 1 day) as status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'inactive') as status from 
		bi_patient_status b 
		where status = 'active' 
        and clientid in (select clientid from bi_patient_Status where status = 'inactive')
        and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
        
        union 
        
        select distinct clientid, date_add(status_end_date, interval 1 day) as status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'inactive') as status from -- Patients are getting discharged and inactivated directly from pending_active as well.
		bi_patient_status b 
		where status = 'pending_active' 
        and clientid in (select clientid from bi_patient_Status where status = 'inactive')
        and clientid in (select clientid from bi_patient_status group by clientid having count(case when status = 'active' then clientid else null end) = 0) -- So get those patients who had pending_active status and never reached active status.
		and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
	) b on date(status_end_date) <= a.date
	group by date, doctorName, b.status
    
    union all 
    
	select date, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, enrollmentdateitz, status_start_date, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'Cum_Active') as status from 
		bi_patient_status b 
		where status = 'active'
        and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
	) b on status_start_date <= a.date
	group by date, doctorName, b.status
    /*
	union all 

	select date, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select clientid, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'Re-active') as status from 
		bi_patient_status b 
		where status = 'discharged' 
		and clientid not in (select clientid from bi_patient_Status where status = 'inactive')
        and clientid in (select clientid from bi_patient_Status where status = 'active' and status_end_date = date(itz(now())))
	) b on date(status_end_date) <= a.date
	group by date, doctorName, b.status
    */
) s2
on s1.date = s2.date and s1.doctorname = s2.doctorname and s1.status = s2.status 
group by s1.date, s1.doctorname
;

