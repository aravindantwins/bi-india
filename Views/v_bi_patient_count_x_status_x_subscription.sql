alter view v_bi_patient_count_x_status_x_subscription as 

select category, date, a.doctorId, b.doctorname, Enrolled_cnt, Active_cnt, Discharged_cnt, Inactive_Cnt, Pending_Active_Cnt, onHold_cnt
from bi_patient_count_x_doctor_x_category a left join v_allDoctors_list b on a.doctorId = b.doctorId
;




/* OLD definition running for so much time - this whole logic is maintained in a stored procedure - sp_load_bi_patient_count_x_doctor_x_category
select category, date, b.doctorId, a.doctorname, Enrolled_cnt, Active_cnt, Discharged_cnt, Inactive_Cnt, Pending_Active_Cnt 
from v_bi_patient_count_x_status_ExcludeTTP a inner join (select distinct doctorid, doctorname from v_client_tmp) b on a.doctorname = b.doctorname

union all

select category, date, b.doctorId, a.doctorname, Enrolled_cnt, Active_cnt, Discharged_cnt, Inactive_Cnt, Pending_Active_Cnt 
from v_bi_patient_count_x_status_TTP a inner join (select distinct doctorid, doctorname from v_client_tmp) b on a.doctorname = b.doctorname
;
*/



