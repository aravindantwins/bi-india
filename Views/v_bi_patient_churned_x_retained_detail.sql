set collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_churned_x_retained_detail as 
select d.date, a.clientid, c.patientNameShort, c.coachNameShort as coach
from (select date from v_date_to_date where date between date_sub(now(), interval 2 month) and date(now())) d 
join 
	(
		select a.clientid, a.start_date, a.status  as status_cur
		from
		(
			select clientid, status, max(status_start_date) as start_date
			from bi_patient_status 
			where status in ('Active', 'Pending_active')
			group by clientid, status
		)a
	) a on a.start_date =d.date
join v_client_tmp c on a.clientid=c.clientid
where exists 
(
	select 1 from bi_patient_status dis where a.clientid = dis.clientid and (status = 'discharged' or status = 'inactive') and dis.status_start_date < a.start_date

)
and c.patientName not like '%obsolete%'
;
