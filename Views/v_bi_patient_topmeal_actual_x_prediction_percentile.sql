-- patient view percentile points
create view v_bi_patient_topmeal_actual_x_prediction_percentile as 
select clientid, category, mealID, measuredAUC_25p, measuredAUC_50p, measuredAUC_75p, measuredAUC_90p, 
predictedAUC_25p, predictedAUC_50p, predictedAUC_75p, predictedAUC_90p
from bi_patient_topmeal_actual_x_prediction
where total_logged > 1
;