alter view v_bi_Member1_patient_review_summary_x_doctor as 

	select a.date, a.reviewDoctor,
	a.review_completed_measure,
	a.review_completed_and_converted_measure,
	a.review_completed_with_LTC_measure,

	a.review_completed_and_discharged_measure,
	a.review_not_completed_and_converted_measure,
	a.review_completed_with_LTC_and_Converted_measure,

	sum(b.review_completed_measure) as review_completed_measure_7d,
	sum(b.review_completed_and_converted_measure) as review_completed_and_converted_measure_7d,
	sum(b.review_completed_with_LTC_measure) as review_completed_with_LTC_measure_7d

	from 
	(
			select date, reviewDoctor, 
			count(distinct case when is_d10_review_completed = 'Yes' then clientid else null end) as review_completed_measure, 
			count(distinct case when is_d10_review_completed = 'Yes' and is_patient_converted = 'yes' then clientid else null end) as review_completed_and_converted_measure, 
			count(distinct case when is_d10_review_completed is null and is_patient_converted = 'yes' then clientid else null end) as review_not_completed_and_converted_measure, 
			count(distinct case when is_d10_review_completed = 'Yes' and is_member_likely_to_convert in ('Yes') then clientid else null end) as review_completed_with_LTC_measure,
			count(distinct case when is_d10_review_completed = 'Yes' and is_member_likely_to_convert in ('Yes') and is_patient_converted = 'yes' then clientid else null end) as review_completed_with_LTC_and_Converted_measure,
			count(distinct case when is_d10_review_completed = 'Yes' and (status = 'discharged' or status = 'inactive') then clientid else null end) as review_completed_and_discharged_measure

			from v_date_to_date a left join 
			(
				select a.clientid, upper(trim(concat(c.firstname,' ',c.lastname))) as reviewDoctor, b.status, date(d10_review_date) as d10_review_schedule_date, is_d10_review_completed, is_member_likely_to_convert, is_patient_converted
				from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 	
										  left join twinspii.userspii c on a.reviewDoctorId = c.id 
			) b on a.date = d10_review_schedule_date
			where date >= '2021-03-07'
			group by date, reviewDoctor
	) a inner join 
	(
		select date, reviewDoctor, 
		count(distinct case when is_d10_review_completed = 'Yes' then clientid else null end) as review_completed_measure, 
		count(distinct case when is_d10_review_completed = 'Yes' and is_patient_converted = 'yes' then clientid else null end) as review_completed_and_converted_measure, 
		count(distinct case when is_d10_review_completed is null and is_patient_converted = 'yes' then clientid else null end) as review_not_completed_and_converted_measure, 
		count(distinct case when is_d10_review_completed = 'Yes' and is_member_likely_to_convert in ('Yes') then clientid else null end) as review_completed_with_LTC_measure,
		count(distinct case when is_d10_review_completed = 'Yes' and is_member_likely_to_convert in ('Yes') and is_patient_converted = 'yes' then clientid else null end) as review_completed_with_LTC_and_Converted_measure,
		count(distinct case when is_d10_review_completed = 'Yes' and (status = 'discharged' or status = 'inactive') then clientid else null end) as review_completed_and_discharged_measure

		from v_date_to_date a left join 
		(
			select a.clientid, upper(trim(concat(c.firstname,' ',c.lastname))) as reviewDoctor, b.status, date(d10_review_date) as d10_review_schedule_date, is_d10_review_completed, is_member_likely_to_convert, is_patient_converted
			from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 	
									  left join twinspii.userspii c on a.reviewDoctorId = c.id 
		) b on a.date = d10_review_schedule_date
		where date >= '2021-03-07'
		group by date, reviewDoctor
	) b  on ifnull(a.reviewDoctor,'') = ifnull(b.reviewDoctor,'') and b.date between date_sub(a.date, interval 6 day) and a.date 
	group by a.date, a.reviewDoctor
;


