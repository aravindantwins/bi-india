create view v_bi_patient_progress_detail as
select a.clientid, i.patientname, i.doctorname, i.age, a.treatmentDays, a.enrollmentdateitz, a.durationYears_diabetes as diabetic_years, 
a.start_medicine_diabetes as startmedicine, 
(length(a.start_medicine_diabetes_drugs) - length(replace(a.start_medicine_diabetes_drugs,',',''))) + 1 as startMedicineCount,
c.measure as currentmedicine, 
(length(a.current_medicine_drugs) - length(replace(a.current_medicine_drugs,',',''))) + 1 as currentMedicineCount,
cast(avg(d.cPeptide) as decimal(10,2)) as d0_cPeptide, 
cast(avg(d.veinhba1c) as decimal(10,2)) as start_a1c, 
cast(avg(d1.veinhba1c) as decimal(10,2)) as latest_a1c,
g.measure as startWeight, e.average_measure as currentWeight,
cast((f.average_measure + 46.7)/28.7 as decimal(10,2)) as current_eA1C, i.cohortGroup, 
a.is_currently_metformin_only, 
j.twinsService as ServiceRating, 
k.measure as nut_adh,
null as churn
from dim_client a left join bi_patient_monitor_measures c on a.clientid = c.clientid and c.measure_name = 'medicine' and c.measure_event_date = date_sub(date(itz(now())), interval 2 day)
                  left join clinictestresults d on a.clientid = d.clientid and date(a.firstBloodWorkDateITZ) = date(itz(d.bloodworktime))
                  left join clinictestresults d1 on a.clientid = d1.clientid and date(a.latestBloodWorkDateITZ) = date(itz(d1.bloodworktime))
                  left join bi_measures e on a.clientid = e.clientid and e.measure_name = 'weight' and e.measure_type = '1d' and e.measure_event_date = date_sub(date(itz(now())), interval 1 day)
				  left join bi_measures f on a.clientid = f.clientid and f.measure_name = 'cgm' and f.measure_type = '5d' and f.measure_event_date = date_sub(date(itz(now())), interval 1 day)
				  left join  (
										select s1.clientid, s1.measure_event_date, s3.average_measure as measure
										from 
										(
											select s1.clientid, min(s2.measure_event_date) as measure_event_date
											from 
											(
												select a.clientid, min(status_start_date) as status_start_date, min(status_end_date) as status_end_date, date_add(min(status_start_date), interval 10 day) as day10
												from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid 
												where a.status = 'active' and b.status = 'active'
												group by clientid
											 ) s1 left join bi_measures s2 on (s1.clientid = s2.clientid and (s2.measure_event_date between s1.status_start_date and s1.day10) 
																and s2.measure_name = 'weight' and s2.measure_type = '1d' and average_measure is not null)
											group by s1.clientid 
										) s1 left join bi_measures s3 on s1.clientid = s3.clientid and s1.measure_Event_Date = s3.measure_event_Date and s3.measure_name = 'weight' and s3.measure_type = '1d'
							)  g on a.clientid = g.clientid 
 					inner join v_active_clients i on a.clientid = i.clientid
                    left join stage_apprating_tracker j on a.clientid = j.clientid
                    left join v_bi_patient_escalation_detail h on a.clientid = h.clientid 
                    left join bi_patient_monitor_measures k on a.clientid = k.clientid and k.measure_name = 'NUT ADH' and k.measure_event_date = date_sub(date(itz(now())), interval 1 day)
where a.status = 'active' and a.is_row_current = 'y'
group by a.clientid
;

-- (length(a.start_medicine_diabetes) - length(replace(a.start_medicine_diabetes,',',''))) + 1 as startMedicineCount,
-- (length(c.measure) - length(replace(c.measure,',',''))) + 1 as currentMedicineCount,

/* users said to exclude these from the view. 
cast((28.7 * avg(d.veinhba1c) - 46.7) as decimal(7,2)) as startGlucose, 
f.average_measure as currentGlucose, 
case when ((avg(d.cPeptide) < 1 and a.start_medicine_diabetes_drugs like '%insulin%') or 
     (a.current_medicine_drugs like '%insulin%') or  
     (h.escalation_category = 'ILT')) then 'T1'
     else NULL end as inReversalFlag,
*/
