create view v_bi_patient_nut_journey_syntax as 
select s1.clientid, s1.patientname, s1.age, s1.vegetarian, s1.enrollmentdateITZ, s1.enrolleddays, s1.doctorname, s1.coachname, s1.cohortgroup,
ifnull(s1.journey,'J1') as journey, 
ifnull(s1.green_syntax,'1-4') as green_syntax, ifnull(s1.recent_consecutive_reversaldays,'') as recent_consecutive_reversaldays, 
ifnull(s1.orange_syntax,'5-7') as orange_syntax, 
ifnull(s1.red_syntax,'8-14') as red_syntax, 
ifnull(s2.current_nut_syntax,'NA') as current_nut_syntax, 
ifnull( case when currentManualSyntax = 'TWIN_SKIP_BREAKFAST_OR_DINNER' then 'S1 - twin+skip breakfast or dinner' 
	        when currentManualSyntax = 'TWIN_TWELVE_HOUR' then 'S2 - twin+12h'  
            when currentManualSyntax = 'TWIN_EGG_ONLY_BREAKFAST' then 'S3 - twin+egg only breakfast'  
            when currentManualSyntax = 'TWIN' then 'S4 - twin' 
            when currentManualSyntax = 'TWIN_SKIP_BREAKFAST_OR_DINNER_P_STAR_1' then 'S5 - twin+skip breakfast or dinner+p*1'
            when currentManualSyntax = 'TWIN_EGG_ONLY_BREAKFAST_P_STAR_1' then 'S6 - twin+egg only breakfast+p*1'
            when currentManualSyntax = 'TWIN_TWELVE_HOUR_P_STAR_1' then 'S7 - twin12h+p*1'
            when currentManualSyntax = 'TWIN_SKIP_BREAKFAST_OR_DINNER_P_STAR_2' then 'S8 - twin+skip breakfast or dinner+p*2'
            when currentManualSyntax = 'TWIN_EGG_ONLY_BREAKFAST_P_STAR_2' then 'S9 - twin+egg only breakfast+p*2'
            when currentManualSyntax = 'TWIN_TWELVE_HOUR_P_STAR_2' then 'S10 - twin12h+p*2'
            when currentManualSyntax = 'TWIN_SKIP_BREAKFAST_OR_DINNER_P_STAR_2_PCR' then 'S11 - twin+skip breakfast or dinner+p*2+pcr'
            when currentManualSyntax = 'TWIN_EGG_ONLY_BREAKFAST_P_STAR_2_PCR' then 'S12 - twin+egg only breakfast+p*2+pcr'
			when currentManualSyntax = 'TWIN_TWELVE_HOUR_P_STAR_2_PCR' then 'S13 - twin12h+p*2+pcr'
            when currentManualSyntax = 'TWIN_TWELVE_HOUR_P_STAR_2_PCR_MORE_THAN_ONCE' then 'S14 - twin12h+p*2+pcr (2 or more/day)'
            else null end, '') as currentManualSyntax -- ,s2.*
from 
(
	select a.clientid, patientname, age, vegetarian, a.enrollmentdateITZ, enrolleddays, a.doctorname, a.coachname, a.cohortgroup, 
	journey, nut_syntax as green_syntax, recent_consecutive_reversaldays,
	case when journey = 'J1' then '5-7' 
		 when journey = 'J2' then '8-10' 
		 when journey = 'J3' then '11-12' 
				else null end as orange_syntax,  
	case when journey = 'J1' then '8-14' 
		 when journey = 'J2' then '11-14' 
		 when journey = 'J3' then '13-14' 
				else null end as red_syntax
	from v_active_clients a left join dim_client b on a.clientid = b.clientid and b.is_row_current = 'Y'
) s1 left join 
( 
select s.clientid, s.mealdate, greenfood, redFood, pstarFood, B_eggFood, B_totalFood, 
B_pS1food, L_pS1food, D_pS1food, S_pS1food, B_pS2food, L_pS2food, D_pS2food, S_pS2food, B_PCRfood, L_PCRfood, D_PCRfood, S_PCRfood, 
is_on_mealSkip, is_on_timeRestrictionEating, currentManualSyntax, 
case when greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 0 then 1 
	 when greenfood > 0 and is_on_timeRestrictionEating = 1 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 then 2
     when greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 and (B_eggFood = B_totalFood = 1) then 3
     when greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 then 4
     
     when ((greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 1) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 5
	 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 and (B_eggFood = B_totalFood = 1)) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 6
 	 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 ) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 7

     when ((greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 1) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 8
	 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 and (B_eggFood = B_totalFood = 1)) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 9
 	 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 10
     
     when ((greenfood > 0 and is_on_mealskip = 1 and (redFood = 1 or pstarFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 11
	 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood = 1 or pstarFood = 1) and (B_eggFood = B_totalFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 12
 	 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood = 1 or pstarFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 13
 	 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood > 1 or pstarFood > 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) >= 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) >= 1)) then 14

     else 'UK'
     end as current_nut_syntax
from 
(
	select s1.clientid, s1.eventdateitz as mealdate, 
    ifnull(B_pS1food,0) as B_pS1food, ifnull(L_pS1food,0) as L_pS1food, ifnull(D_pS1food,0) as D_pS1food, ifnull(S_pS1food,0) as S_pS1food, 
    ifnull(B_pS2food,0) as B_pS2food, ifnull(L_pS2food,0) as L_pS2food, ifnull(D_pS2food,0) as D_pS2food, ifnull(S_pS2food,0) as S_pS2food, 
    ifnull(B_PCRfood,0) as B_PCRfood, ifnull(L_PCRfood,0) as L_PCRfood, ifnull(D_PCRfood,0) as D_PCRfood, ifnull(S_PCRfood,0) as S_PCRfood,
	if(s2.breakfast = 0 or s2.dinner = 0, 1, 0) as is_on_mealSkip, 
	if(s2.timeRestrictedEating = 1, 1, 0) as is_on_timeRestrictionEating,
    ifnull(s2.currentManualSyntax,'') as currentManualSyntax,
	ifnull(greenFood,0) as greenFood, ifnull(redfood_adjusted,0) as redFood, ifnull(pstarFood,0) as pstarFood, ifnull(B_eggFood,0) as B_eggFood, ifnull(B_totalFood,0) as B_totalFood
	from 
	(
		select clientid, eventdateitz, 
		max(case when categoryType = 'BREAKFAST' then pS1food else null end) as B_pS1food, 
		max(case when categoryType = 'LUNCH' then pS1food else null end) as L_pS1food, 
		max(case when categoryType = 'DINNER' then pS1food else null end) as D_pS1food, 
        sum(case when categoryType like '%SNACK%' then pS1food else null end) as S_pS1food,
        
		max(case when categoryType = 'BREAKFAST' then pS2food else null end) as B_pS2food, 
		max(case when categoryType = 'LUNCH' then pS2food else null end) as L_pS2food, 
		max(case when categoryType = 'DINNER' then pS2food else null end) as D_pS2food,
		sum(case when categoryType like '%SNACK%' then pS2food else null end) as S_pS2food,
       
        
		max(case when categoryType = 'BREAKFAST' then PCRfood else null end) as B_PCRfood, 
		max(case when categoryType = 'LUNCH' then PCRfood else null end) as L_PCRfood, 
		max(case when categoryType = 'DINNER' then PCRfood else null end) as D_PCRfood,
        sum(case when categoryType like '%SNACK%' then PCRfood else null end) as S_PCRfood,

        
		sum(redfood) as redFood, sum(greenfood) as greenFood, sum(pstar_food) as pstarFood, sum(redfood) - sum(pstar_food) as redfood_adjusted,
        sum(case when categoryType = 'BREAKFAST' then egg_related_food else 0 end) as B_eggFood,
        max(case when categoryType = 'BREAKFAST' then totalFood else null end) as B_totalFood
		from 
		(
			select a.clientid, eventdateitz, categoryType, 
			count(distinct case when foodGroups like '%P_STAR%' then b.foodid else null end) as pS1food, 
			count(distinct case when foodGroups like '%P_STAR%' then b.foodid else null end) as pS2food,
            count(distinct case when foodGroups like '%PCR%' then b.foodid else null end) as PCRfood,
			count(distinct case when b.recommendationRating <= 1 then b.foodid else null end) as redfood, 
			count(distinct case when b.recommendationRating > 2 then b.foodid else null end) as greenfood,
			count(distinct case when e.foodid is not null and b.recommendationRating <= 1 then a.itemid else null end) as pstar_food,
            count(distinct case when d.name like '%egg%' then b.foodid else null end) as egg_related_food,
            count(distinct b.foodid) as totalFood
			from bi_food_supplement_log_detail a inner join foods b on a.itemid = b.foodid
												 inner join foodingredientmappings c on b.foodid = c.foodid
                                                 inner join ingredients d on c.ingredientid = d.ingredientId
												 left join personalizedfoods e on a.itemid = e.foodid and a.clientid = e.clientid and e.recommendationrating is not null 
			where eventdateitz = date_sub(date(itz(now())), interval 2 day) 
			and a.category = 'foodlog'
			group by clientid, eventdateitz, categoryType
		) s 
		group by clientid, eventdateitz
	) s1 left join nutritionprofiles s2 on s1.clientid = s2.clientid 
) s 
) s2 on s1.clientid = s2.clientid 
;
