-- set collation_connection = 'latin1_swedish_ci'; 
alter view v_bi_active_patient_lab_due_alert as 
select
    a.date as date,
    a.clientid as clientid,
    a.patientname as patientname,
    a.city as city,
    a.labels as labels,
    a.firstbloodworkDateITZ as firstbloodworkDateITZ,
    a.LabDue as Labdue,
    a.isDisenrollOpen as isDisenrollOpen,
    a.bloodwork_Scheduled_date as bloodwork_Scheduled_date,
    a.bloodwork_Scheduled_Status as bloodwork_Scheduled_Status,
    if((b.clientid is not null),
    'Yes',
    '') as isBloodReport_present,
    if((b.clientid is not null),
    itz(b.bloodworktime),
    '') as bloodWorkTime,
    if((inv.clientid is not null),
    inv.invoices,
    '') as dueInvoices,
    if((inv.clientid is not null),
    'Yes',
    '') as isDueInvoices,
    zp.lastAvailablePaymentDate as lastAvailablePaymentDate,
    a.planCode as planCode,
    a.enrollType as enrollType,
    a.startDate as startDate,
    a.programStartDate_analytics as programStartDate_analytics,
    a.conversionPayment_Date as conversionPayment_Date,
    a.age,
    a.gender,
    a.email,
    a.address,
    a.mobileNumber,
    re.is_reactivation
from
    (((((
    select
        s.date as date,
        s.clientid as clientid,
        s.patientname as patientname,
        s.city as city,
        s.labels as labels,
        s.firstbloodworkDateITZ as firstbloodworkDateITZ,
        s.LabDue as LabDue,
        s.isDisenrollOpen as isDisenrollOpen,
        s.planCode as planCode,
        s.enrollType as enrollType,
        s.startDate as startDate,
        s.programStartDate_analytics as programStartDate_analytics,
        s.conversionPayment_Date as conversionPayment_Date,
        (case
            when (s.LabDue in ('D84', 'D90')) then s1.D90_BloodWork_Scheduled_Date
            when (s.LabDue in ('D174', 'D180')) then s1.D180_BloodWork_Scheduled_Date
            when (s.LabDue in ('D264', 'D270')) then s1.D270_BloodWork_Scheduled_Date
            when (s.LabDue in ('D354', 'D360')) then s1.D360_BloodWork_Scheduled_Date
            when (s.LabDue in ('D534', 'D540')) then s1.D540_BloodWork_Scheduled_Date
            when (s.LabDue in ('D714', 'D720')) then s1.D720_BloodWork_Scheduled_Date
            when (s.LabDue in ('D894', 'D900')) then s1.D900_BloodWork_Scheduled_Date
            when (s.LabDue in ('D1074', 'D1080')) then s1.D1080_BloodWork_Scheduled_Date
            else null
        end) as bloodwork_Scheduled_date,
        (case
            when (s.LabDue in ('D84', 'D90')) then s1.D90_bloodwork_status
            when (s.LabDue in ('D174', 'D180')) then s1.D180_bloodwork_status
            when (s.LabDue in ('D264', 'D270')) then s1.D270_bloodwork_status
            when (s.LabDue in ('D354', 'D360')) then s1.D360_bloodwork_status
            when (s.LabDue in ('D534', 'D540')) then s1.D540_bloodwork_status
            when (s.LabDue in ('D714', 'D720')) then s1.D720_bloodwork_status
            when (s.LabDue in ('D894', 'D900')) then s1.D900_bloodwork_status
            when (s.LabDue in ('D1074', 'D1080')) then s1.D1080_bloodwork_status
            else null
        end) as bloodwork_Scheduled_Status,
        age,gender,email,address,mobileNumber
    from
        ((
        select
            a.date as date,
            s.clientid as clientid,
            s.patientname as patientname,
            s.city as city,
            s.labels as labels,
            s.firstbloodworkDateITZ as firstbloodworkDateITZ,
            s.planCode as planCode,
            s.enrollType as enrollType,
            s.startDate as startDate,
            s.programStartDate_analytics as programStartDate_analytics,
            s.conversionPayment_Date as conversionPayment_Date,
            (case
                when (a.date = s.Day84_labdate) then 'D84'
                when (a.date = s.Day174_labdate) then 'D174'
                when (a.date = s.Day264_labdate) then 'D264'
                when (a.date = s.Day354_labdate) then 'D354'
                when (a.date = s.Day534_labdate) then 'D534'
                when (a.date = s.Day714_labdate) then 'D714'
                when (a.date = s.Day894_labdate) then 'D894'
                when (a.date = s.Day1074_labdate) then 'D1074'
            end) as LabDue,
            s.isDisenrollOpen as isDisenrollOpen,
            age,gender,email,address,mobileNumber
        from
            (v_date a
        left join (
            select
                s.clientid as clientid,
                s.patientname as patientname,
                s.labels as labels,
                s.firstbloodworkDateITZ as firstbloodworkDateITZ,
                s.city as city,
                s.firstBloodWorkDate as firstBloodWorkDate,
                s.planCode as planCode,
                s.enrollType as enrollType,
                s.startDate as startDate,
                s.programStartDate_analytics as programStartDate_analytics,
                s.conversionPayment_Date as conversionPayment_Date,
                (s.startDate + interval 84 day) as Day84_labdate,
                (s.startDate + interval 174 day) as Day174_labdate,
                (s.startDate + interval 264 day) as Day264_labdate,
                (s.startDate + interval 354 day) as Day354_labdate,
                (s.startDate + interval 534 day) as Day534_labdate,
                (s.startDate + interval 714 day) as Day714_labdate,
                (s.startDate + interval 894 day) as Day894_labdate,
                (s.startDate + interval 1074 day) as Day1074_labdate,
                s.isDisenrollOpen as isDisenrollOpen,
                age,gender,email,address,mobileNumber
            from
                (
                select
                    s.clientid as clientid,
                    s.patientname as patientname,
                    s.labels as labels,
                    s.firstbloodworkDateITZ as firstbloodworkDateITZ,
                    s.city as city,
                    s.firstBloodWorkDate as firstBloodWorkDate,
                    s.planCode as planCode,
                    s.enrollType as enrollType,
                    s.programStartDate_analytics as programStartDate_analytics,
                    s.conversionPayment_Date as conversionPayment_Date,
                    s.programStartDate_analytics as startDate,
                    s.isDisenrollOpen as isDisenrollOpen,
                    age,gender,email,address,mobileNumber
                from
                    (
                    select
                        a.clientId as clientid,
                        a.patientName as patientname,
                        a.labels as labels,
                        b.firstBloodWorkDateITZ as firstbloodworkDateITZ,
                        a.city as city,
                        cast(b.firstBloodWorkDateITZ as date) as firstBloodWorkDate,
                        a.planCode as planCode,
                        b.programStartDate_analytics as programStartDate_analytics,
                        (case
                            when (b.isM1 = 'Yes') then 'M1'
                            when (b.isM2Converted = 'Yes') then 'M1-M2'
                            else 'M2'
                        end) as enrollType,
                        (case
                            when ((ttp.clientid is not null)
                            and (ttp.is_patient_converted = 'Yes')) then ttp.conversion_payment_date
                            else null
                        end) as conversionPayment_Date,
                        if((c.clientid is not null),
                        'Yes',
                        'No') as isDisenrollOpen,
                        age,gender,email,address,mobileNumber
                    from
                        (((v_allClient_list a
                    join dim_client b on
                        ((a.clientId = b.clientid)))
                    left join (
                        select
                            bi_incident_mgmt.clientid as clientid,
                            max(cast(itz(bi_incident_mgmt.report_time) as date)) as openDate,
                            max(cast(ifnull(itz(bi_incident_mgmt.resolved_time), itz(now())) as date)) as resolvedDate
                        from
                            bi_incident_mgmt
                        where
                            (bi_incident_mgmt.category = 'Disenrollment')
                        group by
                            bi_incident_mgmt.clientid) c on
                        (((a.clientId = c.clientid)
                            and (cast(itz(now()) as date) between c.openDate and ifnull(c.resolvedDate, cast(itz(now()) as date))))))
                    left join bi_ttp_enrollments ttp on
                        ((a.clientId = ttp.clientid)))
                    where
                        ((b.is_row_current = 'y')
                            and (not((a.labels like '%PRIME%')))
                                and (not((a.labels like '%CONTROL%')))
                                    and (not((a.labels like '%TWIN_HTN%'))) and (a.status = 'Active'))
                                    ) s) s) s on
            (((a.date = s.Day84_labdate)
                or (a.date = s.Day174_labdate)
                    or (a.date = s.Day264_labdate)
                        or (a.date = s.Day354_labdate)
                            or (a.date = s.Day534_labdate)
                                or (a.date = s.Day714_labdate)
                                    or (a.date = s.Day894_labdate)
                                        or (a.date = s.Day1074_labdate))))
        where
            ((a.date >= (cast(itz(now()) as date) - interval 1 month))
                and (a.date <= (cast(itz(now()) as date) + interval 7 day)))) s
    left join (
        select
            a.clientId as clientid,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D84', 'D90'))) then itz(a.eventTime) else null end)) as D90_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D84', 'D90'))) then a.status else null end)) as D90_bloodwork_status,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D174', 'D180'))) then itz(a.eventTime) else null end)) as D180_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D174', 'D180'))) then a.status else null end)) as D180_bloodwork_status,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D264', 'D270'))) then itz(a.eventTime) else null end)) as D270_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D264', 'D270'))) then a.status else null end)) as D270_bloodwork_status,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D354', 'D360'))) then itz(a.eventTime) else null end)) as D360_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D354', 'D360'))) then a.status else null end)) as D360_bloodwork_status,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D534', 'D540'))) then itz(a.eventTime) else null end)) as D540_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D534', 'D540'))) then a.status else null end)) as D540_bloodwork_status,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D714', 'D720'))) then itz(a.eventTime) else null end)) as D720_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D714', 'D720'))) then a.status else null end)) as D720_bloodwork_status,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D894', 'D900'))) then itz(a.eventTime) else null end)) as D900_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D894', 'D900'))) then a.status else null end)) as D900_bloodwork_status,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D1074', 'D1080'))) then itz(a.eventTime) else null end)) as D1080_BloodWork_Scheduled_Date,
            max((case when ((a.type = 'BLOOD_WORK') and (c.bloodWorkDay in ('D1074', 'D1080'))) then a.status else null end)) as D1080_bloodwork_status
        from
            (clientappointments a
        left join bloodworkschedules c on
            (((a.clientId = c.clientId)
                and (a.clientAppointmentId = c.clientAppointmentId))))
        where
            (a.type = 'BLOOD_WORK')
        group by
            a.clientId) s1 on
        ((s.clientid = s1.clientid)))) a
left join (
    select
        clinictestresults.clientid as clientid,
        clinictestresults.bloodWorkTime as bloodworktime
    from
        clinictestresults
    where
        (clinictestresults.deleted = 0)) b on
    (((a.clientid = b.clientid)
        and (cast(itz(b.bloodworktime) as date) between (cast(a.bloodwork_Scheduled_date as date) + interval -(1) day) and (cast(a.bloodwork_Scheduled_date as date) + interval 10 day)))))
left join (
    select
        v_bi_patient_overdue_invoices_zoho.clientid as clientid,
        count(distinct v_bi_patient_overdue_invoices_zoho.inv_number) as total_invoices,
        group_concat(concat(v_bi_patient_overdue_invoices_zoho.inv_number, ' : ', v_bi_patient_overdue_invoices_zoho.invoice_date) separator '\n') as invoices
    from
        v_bi_patient_overdue_invoices_zoho
    group by
        v_bi_patient_overdue_invoices_zoho.clientid) inv on
    ((a.clientid = inv.clientid)))
left join (
    select
        v_bi_patient_daily_payment_zoho.clientid as clientid,
        max(v_bi_patient_daily_payment_zoho.payment_date) as lastAvailablePaymentDate
    from
        v_bi_patient_daily_payment_zoho
    group by
        v_bi_patient_daily_payment_zoho.clientid) zp on
    ((a.clientid = zp.clientid)))
    -- Below join added on 14-Dec-22 to get contact details of the member.
 left join (select clientid ,case when count(status) > 1 then 'Yes' else 'No' end as is_reactivation from bi_patient_Status where upper(status) ='ACTIVE' group by 1) re on a.clientid = re.clientid)
 ;


/*
select a.date,a.clientid, a.patientname, city, a.labels, a.firstbloodworkDateITZ, a.Labdue, a.isDisenrollOpen, 
a.bloodwork_Scheduled_date, a.bloodwork_Scheduled_Status, 
if(b.clientid is not null, 'Yes', '') as isBloodReport_present,
if(b.clientid is not null, itz(bloodworkTime), '') as bloodWorkTime, 
if(inv.clientid is not null, invoices,'') as dueInvoices,
if(inv.clientid is not null, 'Yes','') as isDueInvoices,
zp.lastAvailablePaymentDate,
planCode,
enrollType, startDate, programStartDate_analytics, conversionPayment_Date,
age, gender, email, address, postalCode, mobileNumber, is_reactivation
from
(
    select s.date, s.clientid, s.patientname, city, s.labels, s.firstbloodworkDateITZ, s.LabDue, s.isDisenrollOpen, planCode, enrollType, startDate, programStartDate_analytics, conversionPayment_Date,age, gender, email, address, postalCode, mobileNumber,
	case when s.labDue = 'D90' then D90_BloodWork_Scheduled_Date
		 when s.labDue = 'D180' then D180_BloodWork_Scheduled_Date
		 when s.labDue = 'D270' then D270_BloodWork_Scheduled_Date
		 when s.labDue = 'D360' then D360_BloodWork_Scheduled_Date else null end as bloodwork_Scheduled_date,
	case when s.labDue = 'D90' then D90_bloodwork_status
		 when s.labDue = 'D180' then D180_bloodwork_status
		 when s.labDue = 'D270' then D270_bloodwork_status
		 when s.labDue = 'D360' then D360_bloodwork_status else null end as bloodwork_Scheduled_Status
	from 
	(
		select date, clientid, patientname, city, labels, firstbloodworkDateITZ,planCode, enrollType, startDate, programStartDate_analytics, conversionPayment_Date, age, gender, email, address, postalCode, mobileNumber,
		case when a.date = Day89_labdate then 'D90' 
			 when a.date = Day179_labdate then 'D180'
			 when a.date = Day269_labdate then 'D270'
			 when a.date = Day359_labdate then 'D360'
			 when a.date = Day539_labdate then 'D540'
			 when a.date = Day719_labdate then 'D720' end as LabDue, isDisenrollOpen
		from v_date a left join 
		(
				select clientid, patientname, labels, firstbloodworkDateITZ, city, firstBloodWorkDate, planCode, enrollType, startDate, programStartDate_analytics, conversionPayment_Date, age, gender, email, address, postalCode, mobileNumber,
				date_add(startDate, interval 89 day) as Day89_labdate,
				date_add(startDate, interval 90 day) as Day90_labdate,
				date_add(startDate, interval 179 day) as Day179_labdate,
				date_add(startDate, interval 180 day) as Day180_labdate,
				date_add(startDate, interval 269 day) as Day269_labdate,
				date_add(startDate, interval 270 day) as Day270_labdate,
				date_add(startDate, interval 359 day) as Day359_labdate,
				date_add(startDate, interval 360 day) as Day360_labdate,
				date_add(startDate, interval 539 day) as Day539_labdate,
				date_add(startDate, interval 540 day) as Day540_labdate,
				date_add(startDate, interval 719 day) as Day719_labdate,
				date_add(startDate, interval 720 day) as Day720_labdate,
				isDisenrollOpen
				from 
				(
					select clientid, patientname, labels, firstbloodworkDateITZ, city, firstBloodWorkDate, planCode, enrollType, programStartDate_analytics, conversionPayment_Date,
					case when enrollType = 'M2' then programStartDate_analytics
						 when enrollType = 'M1-M2' then conversionPayment_Date else null end as startDate, isDisenrollOpen,
					age, gender, email, address, postalCode, mobileNumber
					from 
					(
							select a.clientid, patientname, labels, firstbloodworkDateITZ, city, 
							date(firstbloodworkDateITZ) as firstBloodWorkDate, a.planCode, programStartDate_analytics, 
							case when b.isM1 = 'Yes' then 'M1'
								 when b.isM2Converted = 'Yes' then 'M1-M2'
								 else 'M2' end as enrollType, 
							case when ttp.clientid is not null and is_patient_converted = 'Yes' then Conversion_payment_date else null end as conversionPayment_Date,
							if(c.clientid is not null, 'Yes', 'No') as isDisenrollOpen,
							a.age, a.gender, a.email, a.address, a.postalCode, a.mobileNumber
							from v_allclient_list a inner join dim_Client b on a.clientid = b.clientid 
													left join 
													(
															select distinct clientid, max(date(itz(report_time))) as openDate, max(date(ifnull(itz(resolved_time), itz(now())))) as resolvedDate 
															from bi_incident_mgmt where category = 'Disenrollment' 
															group by clientid
													) c on a.clientid = c.clientid and date(itz(now())) between opendate and ifnull(resolvedDate,date(itz(now())))
													left join bi_ttp_enrollments ttp on a.clientid= ttp.clientid 
							where b.is_row_current = 'y'
							and (labels not like '%PRIME%' and labels not like '%CONTROL%')
							and a.status = 'Active'
					) s 
				) s 
		) s on a.date = Day89_labdate or a.date = Day179_labdate or a.date = Day269_labdate or a.date = Day359_labdate or a.date = Day539_labdate or a.date = Day719_labdate
		where date >= date_sub(date(itz(now())), interval 1 month) and date <= date_add(date(itz(now())), interval 7 day)
	) s left join 
	(
				select a.clientid, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D90' then itz(a.eventTime) else null end) as D90_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D90' then a.status else null end) as D90_bloodwork_status,                        

				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D180' then itz(a.eventTime) else null end) as D180_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D180' then a.status else null end) as D180_bloodwork_status,
							
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D270' then itz(a.eventTime) else null end) as D270_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D270' then a.status else null end) as D270_bloodwork_status,
							
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D360' then itz(a.eventTime) else null end) as D360_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'D360' then a.status else null end) as D360_bloodwork_status
						   
				from clientappointments a left join bloodworkschedules c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
				where a.type = 'BLOOD_WORK'
				group by a.clientid 
	) s1 on s.clientid = s1.clientid 
) a
left join 
(			
	select clientid, bloodworktime from clinictestresults where deleted = 0 
) b on a.clientid = b.clientid and date(itz(b.bloodWorkTime)) between date_add(date(a.bloodwork_Scheduled_date), interval -1 day) and date_add(date(a.bloodwork_Scheduled_date), interval 10 day)

left join (
			select clientid, count(distinct inv_number) as total_invoices, group_concat(concat(inv_number,' : ', invoice_date) separator '\n') as invoices
			from v_bi_patient_overdue_invoices_zoho 
            group by clientid 
		   ) inv on a.clientid = inv.clientid
left join (select clientid, max(payment_date) as lastAvailablePaymentDate from v_bi_patient_daily_payment_Zoho group by clientid) zp on a.clientid = zp.clientid 
left join (select clientid ,case when count(status) > 1 then 'Yes' else 'No' end as is_reactivation from bi_patient_Status where upper(status) ='ACTIVE' group by 1) re on a.clientid = re.clientid
;

*/

/* Existing code
select a.date,a.clientid, a.patientname, city, a.labels, a.firstbloodworkDateITZ, a.Labdue, a.isDisenrollOpen, 
a.bloodwork_Scheduled_date, a.bloodwork_Scheduled_Status, 
if(b.clientid is not null, 'Yes', '') as isBloodReport_present,
if(b.clientid is not null, itz(bloodworkTime), '') as bloodWorkTime, 
if(inv.clientid is not null, invoices,'') as dueInvoices,
if(inv.clientid is not null, 'Yes','') as isDueInvoices,
zp.lastAvailablePaymentDate,
planCode
from
(
    select s.date, s.clientid, s.patientname, city, s.labels, s.firstbloodworkDateITZ, s.LabDue, s.isDisenrollOpen,planCode,
	case when s.labDue = 'D90' then D90_BloodWork_Scheduled_Date
		 when s.labDue = 'D180' then D180_BloodWork_Scheduled_Date
		 when s.labDue = 'D270' then D270_BloodWork_Scheduled_Date
		 when s.labDue = 'D360' then D360_BloodWork_Scheduled_Date else null end as bloodwork_Scheduled_date,
	case when s.labDue = 'D90' then D90_bloodwork_status
		 when s.labDue = 'D180' then D180_bloodwork_status
		 when s.labDue = 'D270' then D270_bloodwork_status
		 when s.labDue = 'D360' then D360_bloodwork_status else null end as bloodwork_Scheduled_Status
	from 
	(
		select date, clientid, patientname, city, labels, firstbloodworkDateITZ,planCode, 
		case when a.date = Day89_labdate then 'D90' 
			 when a.date = Day179_labdate then 'D180'
			 when a.date = Day269_labdate then 'D270'
			 when a.date = Day359_labdate then 'D360'
			 when a.date = Day539_labdate then 'D540'
			 when a.date = Day719_labdate then 'D720' end as LabDue, isDisenrollOpen
		from v_date a left join 
		(
			select a.clientid, patientname, labels, firstbloodworkDateITZ, city, 
			date(firstbloodworkDateITZ) as firstBloodWorkDate, a.planCode, programStartDate_analytics, 
            case when b.isM1 = 'Yes' then 'M1'
				 when b.isM1Converted = 'Yes' then 'M1-M2'
                 else 'M2' end as enrollType, 
			case when ttp.clientid is not null and is_patient_converted = 'Yes' then Conversion_payment_date else null end as conversionPayment_Date,
			date_add(date(firstbloodworkDateITZ), interval 89 day) as Day89_labdate,
			date_add(date(firstbloodworkDateITZ), interval 90 day) as Day90_labdate,
			date_add(date(firstbloodworkDateITZ), interval 179 day) as Day179_labdate,
			date_add(date(firstbloodworkDateITZ), interval 180 day) as Day180_labdate,
			date_add(date(firstbloodworkDateITZ), interval 269 day) as Day269_labdate,
			date_add(date(firstbloodworkDateITZ), interval 270 day) as Day270_labdate,
			date_add(date(firstbloodworkDateITZ), interval 359 day) as Day359_labdate,
			date_add(date(firstbloodworkDateITZ), interval 360 day) as Day360_labdate,
			date_add(date(firstbloodworkDateITZ), interval 539 day) as Day539_labdate,
			date_add(date(firstbloodworkDateITZ), interval 540 day) as Day540_labdate,
			date_add(date(firstbloodworkDateITZ), interval 719 day) as Day719_labdate,
			date_add(date(firstbloodworkDateITZ), interval 720 day) as Day720_labdate,
			if(c.clientid is not null, 'Yes', 'No') as isDisenrollOpen
			from v_Active_clients a inner join dim_Client b on a.clientid = b.clientid 
									left join 
									(
											select distinct clientid, max(date(itz(report_time))) as openDate, max(date(ifnull(itz(resolved_time), itz(now())))) as resolvedDate 
											from bi_incident_mgmt where category = 'Disenrollment' 
											group by clientid
									) c on a.clientid = c.clientid and date(itz(now())) between opendate and ifnull(resolvedDate,date(itz(now())))
                                    left join bi_ttp_enrollments ttp on a.clientid= ttp.clientid 
			where b.is_row_current = 'y'
			and (labels not like '%PRIME%' and labels not like '%CONTROL%')
		) s on a.date = Day89_labdate or a.date = Day179_labdate or a.date = Day269_labdate or a.date = Day359_labdate or a.date = Day539_labdate or a.date = Day719_labdate
		where date >= date_sub(date(itz(now())), interval 1 month) and date <= date_add(date(itz(now())), interval 7 day)
	) s left join 
	(
				select a.clientid, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 1 then itz(a.eventTime) else null end) as D90_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 1 then a.status else null end) as D90_bloodwork_status,

				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 2 then itz(a.eventTime) else null end) as D180_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 2 then a.status else null end) as D180_bloodwork_status,
				
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 3 then itz(a.eventTime) else null end) as D270_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 3 then a.status else null end) as D270_bloodwork_status,
				
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 4 then itz(a.eventTime) else null end) as D360_BloodWork_Scheduled_Date, 
				max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 4 then a.status else null end) as D360_bloodwork_status
						   
				from clientappointments a left join bloodworkschedules c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
				where a.type = 'BLOOD_WORK'
				group by a.clientid 
	) s1 on s.clientid = s1.clientid 
) a
left join 
(			
	select clientid, bloodworktime from clinictestresults where deleted = 0 
) b on a.clientid = b.clientid and date(itz(b.bloodWorkTime)) between date_add(date(a.bloodwork_Scheduled_date), interval -1 day) and date_add(date(a.bloodwork_Scheduled_date), interval 10 day)

left join (
			select clientid, count(distinct inv_number) as total_invoices, group_concat(concat(inv_number,' : ', invoice_date) separator '\n') as invoices
			from v_bi_patient_overdue_invoices_zoho 
            group by clientid 
		   ) inv on a.clientid = inv.clientid
left join (select clientid, max(payment_date) as lastAvailablePaymentDate from v_bi_patient_daily_payment_Zoho group by clientid) zp on a.clientid = zp.clientid 
*/

