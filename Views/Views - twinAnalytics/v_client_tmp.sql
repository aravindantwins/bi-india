set collation_connection = 'latin1_swedish_ci'; 

alter view v_client_tmp as 
    SELECT  
        `c`.`ID` AS `clientId`,
        CAST(ITZ(NOW()) AS DATE) AS `dateITZ`,
        UPPER(`cp`.`firstName`) as firstName, 
        UPPER(`cp`.`lastName`) as lastName, 
        UPPER(CONCAT(`cp`.`firstName`, ' ', `cp`.`lastName`)) AS `patientName`,
        UPPER(CONCAT(`cp`.`firstName`,
                        ' ',
                        LEFT(`cp`.`lastName`, 1))) AS `patientNameShort`,
        ((YEAR(CURDATE()) - YEAR(`cp`.`dateOfBirth`)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(`cp`.`dateOfBirth`, '%m%d'))) AS `age`,
        c.doctorId,
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`, ' ', `d`.`lastName`)))) AS `doctorName`,
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`,
                        ' ',
                        LEFT(`d`.`lastName`, 1))))) AS `doctorNameShort`,
		d.clinicName,
        c.coachId, 
        itz(c.dateadded) as coachAddedDate, 
        UPPER(CONCAT(`h`.`firstName`, ' ', `h`.`lastName`)) AS `coachName`,
        UPPER(CONCAT(`h`.`firstName`,
                        ' ',
                        LEFT(`h`.`lastName`, 1))) AS `coachNameShort`,
		upper(concat(up.firstName ,' ' ,left(up.lastName, 1))) as leadCoachNameShort,
        h.email as coachEmail,
        c.gender, 
        cp.email,
        ap.city, 
        trim(concat(ifnull(ap.line1,''),' ',ifnull(ap.line2,''),' ',ifnull(ap.city,''),'-',ifnull(ap.postalCode,0))) as address, 
		UPPER(ifnull(pc.divisionName,'')) as city_new,
        UPPER(ifnull(pc.circleName,'')) as state,
        ap.postalCode, 
        cp.mobileNumber, 
        `c`.`dateAdded` AS `dateAdded`,
        `c`.`dateModified` AS `dateModified`,
        `c`.`status` AS `status`,
        `t`.`name` AS `cohortGroup`,
        `c`.`enrollmentDate` AS `enrollmentDate`,
        ITZ(`c`.`enrollmentDate`) AS `enrollmentDateITZ`,
        ITZ(`c`.`dateModified`) AS `dateModifiedITZ`,
        ITZ(`c`.`lastStatusChange`) AS `lastStatusChangeITZ`,
        `c`.`renewalVisitScheduled` AS `renewalVisitScheduledDate`,
        `c`.`dischargeReason` AS `dischargeReason`,
         c.dischargeReasonL1 as dischargeReasonL1,
         c.dischargeReasonL2 as dischargeReasonL2,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (TO_DAYS(ITZ(NOW())) - TO_DAYS(`c`.`enrollmentDate`))
            WHEN (`c`.`status` <> 'Active') THEN (TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`))
        END) AS `daysEnrolled`,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (FLOOR(((TO_DAYS(`c`.`termEndDate`) - TO_DAYS(`c`.`enrollmentDate`)) / 84)) - 1)
            WHEN (`c`.`status` = 'INACTIVE') THEN FLOOR(((TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`)) / 84))
        END) AS `renewalCount`,
        `c`.`firstDiabetesInReversalTime` AS `firstDiabetesInReversalTime`,
        `c`.`termEndDate` AS `termEndDate`,
        `c`.`planCode` AS `planCode`,
        c.labels,
        c.sourceType as source,
        pc.mapped_city, pc.cx_payment_Plan, pc.city_tier, ifnull(pc.twin_fs_service,'No') as twin_fs_service, ifnull(pc.tsa_service, 'No') as tsa_service, ifnull(pc.lab_service, 'No') as lab_service
    FROM
        ((((`twins`.`clients` `c`
        JOIN `twinspii`.`clientspii` `cp` ON ((`c`.`ID` = `cp`.`ID`)))
		LEFT JOIN twinspii.addressespii ap on c.addressId = ap.ID
        left join (select distinct pincode, divisionName, circleName, mapped_city, cx_payment_Plan,city_tier,twin_fs_service,tsa_service, lab_service from bi_address_lookup_new where deliveryStatus = 'Delivery') pc on ltrim(rtrim(replace(ap.postalCode,' ',''))) = pc.pinCode
        JOIN `twins`.`doctors` `d` ON ((`c`.`doctorId` = `d`.`ID`)))
        LEFT JOIN `twins`.`coaches` `h` ON ((`c`.`coachId` = `h`.`ID`)))
        LEFT JOIN twinspii.userspii up on h.leadCoachID = up.id
        LEFT JOIN `twins`.`cohorts` `t` ON ((`c`.`cohortId` = `t`.`cohortId`)))
    WHERE
        ((`c`.`status` IN ('ACTIVE' , 'REGISTRATION', 'INACTIVE', 'DISCHARGED', 'PENDING_ACTIVE', 'ON_HOLD'))
            AND (`c`.`deleted` = 0)
            AND (`d`.`test` = FALSE)
            AND (`c`.`ID` NOT IN (15544 , 15558, 15593, 15594, 15596, 15680))
            AND (NOT ((`cp`.`firstName` LIKE '%TEST%')))
            AND (NOT ((`cp`.`lastName` LIKE '%TEST%'))))
    ORDER BY `c`.`enrollmentDate`
;
