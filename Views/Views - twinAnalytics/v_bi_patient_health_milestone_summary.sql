CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_patient_health_milestone_summary` AS
    SELECT 
        `v_bi_patient_bs_health_milestone`.`clientid` AS `clientId`,
        `v_bi_patient_bs_health_milestone`.`is_Insulin_on_day0` AS `is_Insulin_on_day0`,
        `v_bi_patient_bs_health_milestone`.`category` AS `metrics`,
        '% with Reduced BS since ED0' AS `Category`,
        `v_bi_patient_bs_health_milestone`.`BS_ReducedDays_within30` AS `thirthyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_ReducedDays_within60` AS `sixtyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_ReducedDays_within90` AS `ninetyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_ReducedDays_within120` AS `hundredTwentyDays`
    FROM
        `twins`.`v_bi_patient_bs_health_milestone` 
    UNION ALL SELECT 
        `v_bi_patient_bs_health_milestone`.`clientid` AS `clientId`,
        `v_bi_patient_bs_health_milestone`.`is_Insulin_on_day0` AS `is_Insulin_on_day0`,
        `v_bi_patient_bs_health_milestone`.`category` AS `metrics`,
        '% with Reduced Meds since ED0' AS `Category`,
        `v_bi_patient_bs_health_milestone`.`MedReducedDays_within30` AS `thirthyDays`,
        `v_bi_patient_bs_health_milestone`.`MedReducedDays_within60` AS `sixtyDays`,
        `v_bi_patient_bs_health_milestone`.`MedReducedDays_within90` AS `ninetyDays`,
        `v_bi_patient_bs_health_milestone`.`MedReducedDays_within120` AS `hundredTwentyDays`
    FROM
        `twins`.`v_bi_patient_bs_health_milestone` 
    UNION ALL SELECT 
        `v_bi_patient_bs_health_milestone`.`clientid` AS `clientId`,
        `v_bi_patient_bs_health_milestone`.`is_Insulin_on_day0` AS `is_Insulin_on_day0`,
        `v_bi_patient_bs_health_milestone`.`category` AS `metrics`,
        '% with BS<6.5% for 30 consecutive days with no meds or Metormin only' AS `Category`,
        `v_bi_patient_bs_health_milestone`.`BS_Consec30days_withNoMed_or_MetOnly_within30` AS `thirthyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_Consec30days_withNoMed_or_MetOnly_within60` AS `sixtyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_Consec30days_withNoMed_or_MetOnly_within90` AS `ninetyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_Consec30days_withNoMed_or_MetOnly_within120` AS `hundredTwentyDays`
    FROM
        `twins`.`v_bi_patient_bs_health_milestone` 
    UNION ALL SELECT 
        `v_bi_patient_bs_health_milestone`.`clientid` AS `clientId`,
        `v_bi_patient_bs_health_milestone`.`is_Insulin_on_day0` AS `is_Insulin_on_day0`,
        `v_bi_patient_bs_health_milestone`.`category` AS `metrics`,
        '% with BS<6.5% with no meds or Metormin only' AS `Category`,
        `v_bi_patient_bs_health_milestone`.`BS_Reduced_withNoMed_or_MetOnly_within30` AS `thirthyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_Reduced_withNoMed_or_MetOnly_within60` AS `sixtyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_Reduced_withNoMed_or_MetOnly_within90` AS `ninetyDays`,
        `v_bi_patient_bs_health_milestone`.`BS_Reduced_withNoMed_or_MetOnly_within120` AS `hundredTwentyDays`
    FROM
        `twins`.`v_bi_patient_bs_health_milestone`