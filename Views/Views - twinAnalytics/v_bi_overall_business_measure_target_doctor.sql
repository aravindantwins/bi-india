CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_overall_business_measure_target_doctor` AS
    SELECT 
        `a`.`report_date` AS `date`,
        `a`.`doctorname` AS `doctorname`,
        `a`.`category` AS `category`,
        `a`.`measure_name` AS `measure_name`,
        `a`.`measure_group` AS `measure_group`,
        `a`.`measure` AS `actual_measure`,
        `t`.`measure` AS `target_measure`
    FROM
        (`bi_overall_measure_x_doctor` `a`
        LEFT JOIN `bi_overall_sprint_target_daily` `t` ON (((`a`.`report_date` = `t`.`date`)
            AND (`a`.`category` = CONVERT( `t`.`category` USING UTF8))
            AND (`a`.`measure_name` = CONVERT( `t`.`measure_name` USING UTF8))
            AND (`a`.`measure_group` = CONVERT( `t`.`measure_group` USING UTF8)))))
    WHERE
        ((`a`.`report_date` >= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 2 MONTH))
            AND (`a`.`report_date` <= ITZ(NOW()))
            AND (`a`.`category` IN ('Active' , 'L1.2', 'L1.3', 'L1.4', 'L6')))