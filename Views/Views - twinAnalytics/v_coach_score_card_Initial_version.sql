CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `twinsAnalytics`@`%` 
    SQL SECURITY DEFINER
VIEW `v_coach_score_card` AS
    SELECT DISTINCT
        `cd`.`clientId` AS `clientId`,
        `cd`.`patientName` AS `patientName`,
        `cd`.`doctorName` AS `doctorName`,
        `cd`.`coachName` AS `coachName`,
        `dg`.`eventDateITZ` AS `eventDateITZ`,
        (TO_DAYS(`dg`.`eventDateITZ`) - TO_DAYS(`cd`.`enrollmentDateITZ`)) AS `daysEnrolled`,
        `dg`.`measureValue` AS `cgm1Dg`,
        `dg5`.`measureValue` AS `cgm5dg`,
        IFNULL(`m`.`baseLine`, `dg`.`measureValue`) AS `baseline`,
        `pc`.`peakCount` AS `peakCount`,
        `pc`.`peakCount` AS `spikes`,
        `s`.`score` AS `actionScore`,
        `e5`.`e5pct` AS `e5pct`,
        `sr`.`5DgValue` AS `5dgvalue`,
        `es`.`treatmentStatus` AS `treatmentStatus`
    FROM
        ((((((((`twins`.`v_active_clients` `cd`
        JOIN `twins`.`glucose_ketone_cgm_1dg` `dg` ON (((`dg`.`clientId` = `cd`.`clientId`)
            AND (`dg`.`measureType` = 'CGM'))))
        LEFT JOIN `twins`.`glucose_ketone_cgm_5dg` `dg5` ON (((`dg`.`clientId` = `dg5`.`clientId`)
            AND (`dg`.`eventDateITZ` = `dg5`.`eventDateITZ`)
            AND (`dg`.`measureType` = `dg5`.`measureType`))))
        LEFT JOIN `twins`.`cgmminmax` `m` ON (((`m`.`clientId` = `dg`.`clientId`)
            AND (`m`.`eventDateITZ` = `dg`.`eventDateITZ`))))
        LEFT JOIN (SELECT 
            `twins`.`cgmminmax`.`clientId` AS `clientId`,
                `twins`.`cgmminmax`.`eventDateITZ` AS `eventDateITZ`,
                COUNT(`twins`.`cgmminmax`.`peakValue`) AS `peakCount`
        FROM
            `twins`.`cgmminmax`
        WHERE
            ((`twins`.`cgmminmax`.`delta` > 50)
                AND (`twins`.`cgmminmax`.`eventDateITZ` > (CURDATE() - INTERVAL 2 WEEK)))
        GROUP BY `twins`.`cgmminmax`.`clientId` , `twins`.`cgmminmax`.`eventDateITZ`) `pc` ON (((`dg`.`clientId` = `pc`.`clientId`)
            AND (`dg`.`eventDateITZ` = `pc`.`eventDateITZ`))))
        LEFT JOIN `twins`.`scores` `s` ON (((`dg`.`clientId` = `s`.`clientId`)
            AND (`dg`.`eventDateITZ` = CAST(ITZ(`s`.`eventTime`) AS DATE))
            AND (`s`.`scoreType` = 'DAILY_SCORE')
            AND (`s`.`period` = 'DAILY'))))
        LEFT JOIN (SELECT 
            `v_food_recorded_orig`.`clientid` AS `clientId`,
                `v_food_recorded_orig`.`mealDateITZ` AS `eventDateITZ`,
                (SUM(`v_food_recorded_orig`.`foodpoint`) / COUNT(`v_food_recorded_orig`.`foodpoint`)) AS `e5pct`
        FROM
            `twins`.`v_food_recorded_orig`
        WHERE
            (`v_food_recorded_orig`.`mealDateITZ` > (CURDATE() - INTERVAL 2 WEEK))
        GROUP BY `v_food_recorded_orig`.`clientid` , `v_food_recorded_orig`.`mealDateITZ`) `e5` ON (((`dg`.`clientId` = `e5`.`clientId`)
            AND (`dg`.`eventDateITZ` = `e5`.`eventDateITZ`))))
        LEFT JOIN `twins`.`v_self_report_5dg` `sr` ON (((`dg`.`clientId` = `sr`.`clientId`)
            AND (`dg`.`eventDateITZ` = `sr`.`eventDateITZ`)
            AND (`sr`.`feedback` = 'coach'))))
        JOIN `twins`.`patientescalation` `es` ON (((`dg`.`clientId` = `es`.`clientId`)
            AND (`dg`.`eventDateITZ` = `es`.`eventDateITZ`))))
    WHERE
        (`dg`.`eventDateITZ` > (CURDATE() - INTERVAL 2 WEEK))
    ORDER BY `dg`.`clientId` , `dg`.`eventDateITZ`