alter
VIEW `v_bi_overall_business_measure_target_coach` AS
    SELECT 
        `a`.`report_date` AS `date`,
        `a`.`coach` AS `coach`,
        c.leadCoachNameShort as leadCoach,
        `c`.`startDate` AS `coachStartDate`,
        `a`.`category` AS `category`,
        `a`.`measure_name` AS `measure_name`,
        `a`.`measure_group` AS `measure_group`,
        `a`.`measure` AS `measure`,
        `b`.`measure` AS `target`
    FROM
        ((`bi_overall_measure_x_coach` `a`
        JOIN `v_coach` `c` ON ((`a`.`coach` = CONVERT( `c`.`coachNameShort` USING UTF8))))
        LEFT JOIN `bi_overall_sprint_target_daily` `b` ON (((`a`.`category` = CONVERT( `b`.`category` USING UTF8))
            AND (`a`.`measure_name` = CONVERT( `b`.`measure_name` USING UTF8))
            AND (`a`.`measure_group` = CONVERT( `b`.`measure_group` USING UTF8))
            AND (`a`.`report_date` = `b`.`date`))))
    WHERE
        ((`a`.`report_date` >= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 45 DAY))
            AND (`a`.`report_date` <= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 1 DAY)))