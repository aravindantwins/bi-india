create VIEW v_self_report_archive AS
   SELECT
       cd.clientId AS clientId,
       cd.patientName AS patientName,
       cd.patientNameShort as patientNameShort,
       cd.coach,
       cd.enrollmentDateITZ,
       m.eventDateTimeITZ AS eventTimeITZ,
       cd.Date AS eventDateITZ,
       m.comment1 AS ID,
       m.measureType AS Feedback,
       m.measureValue AS value,
       m.comment2 AS textValue,
       m.comment3 AS Question
   FROM
   (
	   select date, c.clientid, d.patientName, d.patientNameShort, d.coachNameShort as coach, c.enrollmentDateITZ
	   from v_date_to_date a left join bi_patient_status c on a.date between c.status_start_date and c.status_end_date and c.status = 'active'
							 inner join v_client_tmp d on c.clientid = d.clientid
	   where c.visitdateitz is not null 
       and date > date(c.visitdateitz)
       and date > date_sub(date(itz(now())), interval 3 month) 
	) cd 	
   left join
    (
        select max(eventDateTimeITZ) as eventDateTimeITZ,eventDateITZ, clientId, measureSource, measureType, max(measureValue) as measureValue, comment1, max(comment2) as comment2, comment3 
        from selfreportdaily
        where eventdateitz >= date_sub(date(itz(now())), interval 3 month) and eventdateitz <= date(itz(now()))
        group by eventDateITZ, clientId, measureSource, measureType, comment1,  comment3
    ) m
    --   selfreportdaily m
ON m.clientId = cd.clientId and m.eventDateITZ = cd.date
;
