CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_overall_ops_business_measure_target` AS
    SELECT 
        `m`.`report_date` AS `date`,
        `m`.`category` AS `category`,
        `m`.`measure_name` AS `measure_name`,
        `m`.`measure_group` AS `measure_group`,
        `m`.`measure` AS `actual_measure`,
        `t`.`measure` AS `target_measure`
    FROM
        (`bi_overall_measure` `m`
        LEFT JOIN `bi_overall_sprint_target_daily` `t` ON (((`m`.`report_date` = `t`.`date`)
            AND (`m`.`category` = CONVERT( `t`.`category` USING UTF8))
            AND (`m`.`measure_name` = CONVERT( `t`.`measure_name` USING UTF8))
            AND (`m`.`measure_group` = CONVERT( `t`.`measure_group` USING UTF8)))))
    WHERE
        ((`m`.`report_date` <= ITZ(NOW()))
            AND (((`m`.`report_date` >= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 2 MONTH))
            AND (`m`.`category` IN ('Active' , 'L1.2', 'L1.3', 'L1.4')))
            OR ((`m`.`report_date` >= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 12 MONTH))
            AND (`m`.`category` = 'L6'))))