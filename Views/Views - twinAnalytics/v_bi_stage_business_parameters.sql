CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_stage_business_parameters` AS
    SELECT 
        `bi_stage_business_parameters`.`category` AS `category`,
        (CASE
            WHEN
                (`bi_stage_business_parameters`.`category` IN ('GFR' , 'Mood', 'Energy'))
            THEN
                (CASE
                    WHEN
                        (`bi_stage_business_parameters`.`normal_end` >= 1000)
                    THEN
                        CONCAT('> ',
                                ROUND(`bi_stage_business_parameters`.`normal_start`,
                                        1))
                    ELSE CONCAT(ROUND(`bi_stage_business_parameters`.`normal_start`,
                                    1),
                            ' - ',
                            ROUND(`bi_stage_business_parameters`.`normal_end`,
                                    1))
                END)
            ELSE (CASE
                WHEN
                    (`bi_stage_business_parameters`.`normal_start` = 0)
                THEN
                    CONCAT('< ',
                            ROUND(`bi_stage_business_parameters`.`normal_end`,
                                    1))
                ELSE CONCAT(ROUND(`bi_stage_business_parameters`.`normal_start`,
                                1),
                        ' - ',
                        ROUND(`bi_stage_business_parameters`.`normal_end`,
                                1))
            END)
        END) AS `normal_range`,
        (CASE
            WHEN
                (`bi_stage_business_parameters`.`category` IN ('GFR' , 'Mood', 'Energy'))
            THEN
                (CASE
                    WHEN
                        (`bi_stage_business_parameters`.`severe_start` = 0)
                    THEN
                        CONCAT('< ',
                                ROUND(`bi_stage_business_parameters`.`severe_end`,
                                        1))
                    ELSE CONCAT(ROUND(`bi_stage_business_parameters`.`severe_start`,
                                    1),
                            ' - ',
                            ROUND(`bi_stage_business_parameters`.`severe_end`,
                                    1))
                END)
            ELSE (CASE
                WHEN
                    (`bi_stage_business_parameters`.`severe_end` >= 1000)
                THEN
                    CONCAT('> ',
                            ROUND(`bi_stage_business_parameters`.`severe_start`,
                                    1))
                ELSE CONCAT(ROUND(`bi_stage_business_parameters`.`severe_start`,
                                1),
                        ' - ',
                        ROUND(`bi_stage_business_parameters`.`severe_end`,
                                1))
            END)
        END) AS `severe_range`
    FROM
        `bi_stage_business_parameters`