alter view v_coach_score_card 
as 
select distinct cd.clientId,cd.patientName, cd.doctorName, cd.coachName, cd.leadCoach, dg.measure_event_date as eventDateITZ,  
datediff(dg.measure_event_date,cd.enrollmentDateITZ) as daysEnrolled, dg.average_measure as cgm1Dg, dg5.average_measure as cgm5dg, 
ifnull(m.baseline, dg.average_measure) as baseline, peakCount , peakCount as spikes, ifnull(s1.average_measure,0) as actionScore, e5.e5pct,  -- (s.score) as actionScore,
sr.average_measure as 5dgvalue, es.treatmentStatus
from
(
	select a.clientId, b.patientName, b.doctorName, b.leadCoachNameShort as leadCoach, a.enrollmentDateITZ, b.coachName, visitdateITZ, status_start_date, status_end_date
	from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid 
	where a.visitdateITZ is not null
	and a.status = 'active'
) cd

-- 1d CGM
join bi_measures dg on cd.clientid = dg.clientid and dg.measure_event_date between cd.status_start_date and cd.status_end_date and dg.measure_type = '1d' and dg.measure_name = 'cgm'
-- 5d CGM
join bi_measures dg5 on dg.clientid = dg5.clientid and dg5.measure_event_date = dg.measure_event_date and dg5.measure_type = '5d' and dg5.measure_name = 'cgm'
left join cgmminmax m on m.clientId = dg.clientId and m.eventDateITZ = dg.measure_event_date
left join -- get peak count
(
	select clientId, eventDateITZ, count(peakValue) as peakCount from cgmminmax
	where delta > 50    and eventDateITZ  > date_sub(curdate(), interval 4 week)
	group by clientId, eventDateITZ
) pc on dg.clientId = pc.clientId and dg.measure_event_date = pc.eventDateITZ
-- get action score
-- old logic 1d AS
-- left join scores s on dg.clientId = s.clientId and dg.measure_event_date = date(ITZ(s.eventTime)) and s.scoreType  = 'DAILY_SCORE' and s.period = 'DAILY'
-- Hanif asked to show the action score (5d) as in overall dashboard.
left join bi_measures s1 on dg.clientid = s1.clientid and dg.measure_Event_date = s1.measure_event_date and s1.measure_name = 'action score' and s1.measure_type = '5d'
 -- get e5%
left join
(
    select clientId, mealdateITZ as eventDateITZ, sum(foodpoint)/count(foodpoint) as e5pct  from v_food_recorded_orig
   where mealdateITZ > date_sub(curdate(), interval 4 week)
   group by clientId, mealdateITZ
) e5 on dg.clientId = e5.clientId and dg.measure_event_date = e5.eventDateITZ
-- 5d Star Rating
left join bi_measures sr on dg.clientid = sr.clientid and dg.measure_event_date = sr.measure_event_date and sr.measure_type = '5d' and sr.measure_name = 'Star Rating'
left join patientescalation es on dg.clientId = es.clientId and dg.measure_event_date = es.eventDateITZ
where dg.measure_event_date > date_sub(curdate(), interval 4 week) and dg.measure_event_date > cd.visitdateitz
order by dg.clientId, dg.measure_event_date
;
