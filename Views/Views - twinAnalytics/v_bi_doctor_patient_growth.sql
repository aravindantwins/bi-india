CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_doctor_patient_growth` AS
    SELECT 
        `a`.`date` AS `date`,
        `a`.`doctorname` AS `doctor`,
        `a`.`Enrolled_cnt` AS `cumulative_enrolled`,
        `a`.`Active_cnt` AS `active_cnt`,
        `a`.`Pending_Active_Cnt` AS `pending_active_cnt`,
        `b`.`enrolled` AS `enrolled`,
        `s`.`counselled` AS `cumulative_counselled`,
        `s2`.`counselled` AS `counselled`,
        ((IFNULL(`a`.`Active_cnt`, 0) + IFNULL(`a`.`Pending_Active_Cnt`, 0)) * `p`.`annual_fee`) AS `estimated_earnings`,
        `p`.`annual_fee` AS `annual_fee`
    FROM
        ((((`twins`.`v_bi_patient_count_x_status` `a`
        LEFT JOIN (SELECT 
            `s2`.`doctor` AS `doctor`,
                `s1`.`date` AS `date`,
                COUNT(DISTINCT `s2`.`clientid`) AS `enrolled`
        FROM
            (`twins`.`v_date_to_date` `s1`
        LEFT JOIN (SELECT 
            'enrolled' AS `category`,
                `twins`.`bi_patient_status`.`doctor_name` AS `doctor`,
                CAST(`twins`.`bi_patient_status`.`enrollmentdateITZ`
                    AS DATE) AS `date`,
                `twins`.`bi_patient_status`.`clientid` AS `clientid`
        FROM
            `twins`.`bi_patient_status`
        WHERE
            (`twins`.`bi_patient_status`.`status` = 'active')) `s2` ON ((`s1`.`date` = `s2`.`date`)))
        GROUP BY `s2`.`doctor` , `s1`.`date`) `b` ON (((`a`.`doctorname` = `b`.`doctor`)
            AND (`a`.`date` = `b`.`date`))))
        LEFT JOIN (SELECT 
            `b`.`doctor` AS `doctor`,
                COUNT(DISTINCT `b`.`clientId`) AS `counselled`,
                `a`.`date` AS `date`
        FROM
            (`twins`.`v_date_to_date` `a`
        LEFT JOIN (SELECT 
            `c`.`clientid` AS `clientId`,
                `c`.`doctorName` AS `doctor`,
                CAST(`p`.`eventdate` AS DATE) AS `eventdateITZ`,
                `c`.`enrollmentDateITZ` AS `enrollmentDateITZ`
        FROM
            (`twins`.`bi_doctor_counsel_data` `p`
        JOIN `twins`.`dim_client` `c` ON ((`c`.`clientid` = `p`.`clientid`)))
        WHERE
            ((`p`.`counselled` = 'YES')
                AND (`p`.`record_flag` = 'FC')
                AND (`c`.`is_row_current` = 'Y'))) `b` ON ((`a`.`date` >= `b`.`eventdateITZ`)))
        WHERE
            (`b`.`doctor` IS NOT NULL)
        GROUP BY `b`.`doctor` , `a`.`date`) `s` ON (((`a`.`doctorname` = `s`.`doctor`)
            AND (`a`.`date` = `s`.`date`))))
        LEFT JOIN (SELECT 
            `c`.`doctorName` AS `doctor`,
                CAST(`p`.`eventdate` AS DATE) AS `eventdateITZ`,
                COUNT(DISTINCT `c`.`clientid`) AS `counselled`
        FROM
            (`twins`.`bi_doctor_counsel_data` `p`
        JOIN `twins`.`dim_client` `c` ON ((`c`.`clientid` = `p`.`clientid`)))
        WHERE
            ((`p`.`counselled` = 'YES')
                AND (`p`.`record_flag` = 'FC')
                AND (`c`.`is_row_current` = 'Y'))
        GROUP BY `c`.`doctorName` , CAST(`p`.`eventdate` AS DATE)) `s2` ON (((`a`.`doctorname` = `s2`.`doctor`)
            AND (`a`.`date` = `s2`.`eventdateITZ`))))
        LEFT JOIN `twins`.`doctorfees` `p` ON ((`a`.`doctorname` = `p`.`doctorName`)))