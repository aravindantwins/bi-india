CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_e5_measure_detail` AS
    SELECT 
        `c`.`clientId` AS `clientid`,
        `c`.`patient` AS `patient`,
        `c`.`Doctor` AS `Doctor`,
        `c`.`Coach` AS `Coach`,
        `c`.`eventDateITZ` AS `eventDateITZ`,
        (CASE
            WHEN (`x`.`measure` = 'Yes') THEN 1
            WHEN (`x`.`measure` = 'No') THEN 0
            ELSE 'UK'
        END) AS `nut_com`,
        `s`.`breakfast_qty` AS `breakfast_qty`,
        `s`.`breakfast_qty_Red` AS `breakfast_qty_Red`,
        `s`.`b_fasting` AS `b_fasting`,
        `s`.`lunch_qty` AS `lunch_qty`,
        `s`.`lunch_qty_Red` AS `lunch_qty_Red`,
        `s`.`l_Fasting` AS `l_Fasting`,
        `s`.`snack_qty` AS `snack_qty`,
        `s`.`snack_qty_Red` AS `snack_qty_Red`,
        `s`.`s_Fasting` AS `s_Fasting`,
        `s`.`dinner_qty` AS `dinner_qty`,
        `s`.`dinner_qty_Red` AS `dinner_qty_Red`,
        `s`.`d_fasting` AS `d_fasting`,
        `s`.`B_ind` AS `B_ind`,
        `s`.`L_ind` AS `L_ind`,
        `s`.`S_ind` AS `S_ind`,
        `s`.`D_ind` AS `D_ind`
    FROM
        ((((SELECT 
            `ac`.`clientId` AS `clientId`,
                `ac`.`patientNameShort` AS `patient`,
                `ac`.`doctorNameShort` AS `Doctor`,
                `ac`.`coachNameShort` AS `Coach`,
                `d`.`date` AS `eventDateITZ`
        FROM
            (`twins`.`v_date_to_date` `d`
        JOIN `twins`.`v_client_tmp` `ac`)
        WHERE
            ((`d`.`date` <= (CASE
                WHEN (`ac`.`status` = 'active') THEN CAST(ITZ(NOW()) AS DATE)
                ELSE CAST(`ac`.`lastStatusChangeITZ` AS DATE)
            END))
                AND (`d`.`date` >= `ac`.`enrollmentDateITZ`)
                AND (`ac`.`status` IN ('Active' , 'Inactive', 'Discharged'))
                AND (`d`.`date` >= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 2 MONTH))))) `c`
        LEFT JOIN (SELECT 
            `s1`.`clientid` AS `clientId`,
                `s1`.`mealdateITZ` AS `eventDateITZ`,
                `s1`.`breakfast_qty` AS `breakfast_qty`,
                `s1`.`breakfast_qty_Red` AS `breakfast_qty_Red`,
                IFNULL(`s2`.`b_fasting`, 0) AS `b_fasting`,
                `s1`.`lunch_qty` AS `lunch_qty`,
                `s1`.`lunch_qty_Red` AS `lunch_qty_Red`,
                IFNULL(`s2`.`l_fasting`, 0) AS `l_Fasting`,
                `s1`.`snack_qty` AS `snack_qty`,
                `s1`.`snack_qty_Red` AS `snack_qty_Red`,
                IFNULL(`s2`.`s_fasting`, 0) AS `s_Fasting`,
                `s1`.`dinner_qty` AS `dinner_qty`,
                `s1`.`dinner_qty_Red` AS `dinner_qty_Red`,
                IFNULL(`s2`.`d_fasting`, 0) AS `d_fasting`,
                (CASE
                    WHEN
                        ((`s1`.`breakfast_qty_Red` = 0)
                            AND ((`s2`.`b_fasting` = 1)
                            OR ((`s1`.`breakfast_qty` > 0)
                            AND (`s2`.`b_fasting` = 0))))
                    THEN
                        1
                    ELSE 0
                END) AS `B_ind`,
                (CASE
                    WHEN
                        ((`s1`.`lunch_qty_Red` = 0)
                            AND ((`s2`.`l_fasting` = 1)
                            OR ((`s1`.`lunch_qty` > 0)
                            AND (`s2`.`l_fasting` = 0))))
                    THEN
                        1
                    ELSE 0
                END) AS `L_ind`,
                (CASE
                    WHEN (`s1`.`snack_qty_Red` > 0) THEN 0
                    ELSE 1
                END) AS `S_ind`,
                (CASE
                    WHEN
                        ((`s1`.`dinner_qty_Red` = 0)
                            AND ((`s2`.`d_fasting` = 1)
                            OR ((`s1`.`dinner_qty` > 0)
                            AND (`s2`.`d_fasting` = 0))))
                    THEN
                        1
                    ELSE 0
                END) AS `D_ind`
        FROM
            (((SELECT 
            `x`.`clientid` AS `clientid`,
                `x`.`mealdateITZ` AS `mealdateITZ`,
                MAX(IFNULL(`x`.`breakfast_qty`, 0)) AS `breakfast_qty`,
                MAX(IFNULL(`x`.`breakfast_qty_Red`, 0)) AS `breakfast_qty_Red`,
                MAX(IFNULL(`x`.`lunch_qty`, 0)) AS `lunch_qty`,
                MAX(IFNULL(`x`.`lunch_qty_Red`, 0)) AS `lunch_qty_Red`,
                MAX(IFNULL(`x`.`snack_qty`, 0)) AS `snack_qty`,
                MAX(IFNULL(`x`.`snack_qty_Red`, 0)) AS `snack_qty_Red`,
                MAX(IFNULL(`x`.`dinner_qty`, 0)) AS `dinner_qty`,
                MAX(IFNULL(`x`.`dinner_qty_Red`, 0)) AS `dinner_qty_Red`
        FROM
            (SELECT 
            `a`.`clientId` AS `clientid`,
                `a`.`mealDateITZ` AS `mealdateITZ`,
                (CASE
                    WHEN (`a`.`mealType` = 'BREAKFAST') THEN `a`.`totalsum`
                END) AS `breakfast_qty`,
                (CASE
                    WHEN (`a`.`mealType` = 'BREAKFAST') THEN `a`.`totalRed`
                END) AS `breakfast_qty_Red`,
                (CASE
                    WHEN (`a`.`mealType` = 'LUNCH') THEN `a`.`totalsum`
                END) AS `lunch_qty`,
                (CASE
                    WHEN (`a`.`mealType` = 'LUNCH') THEN `a`.`totalRed`
                END) AS `lunch_qty_Red`,
                (CASE
                    WHEN (`a`.`mealType` = 'SNACK') THEN `a`.`totalsum`
                END) AS `snack_qty`,
                (CASE
                    WHEN (`a`.`mealType` = 'SNACK') THEN `a`.`totalRed`
                END) AS `snack_qty_Red`,
                (CASE
                    WHEN (`a`.`mealType` = 'DINNER') THEN `a`.`totalsum`
                END) AS `dinner_qty`,
                (CASE
                    WHEN (`a`.`mealType` = 'DINNER') THEN `a`.`totalRed`
                END) AS `dinner_qty_Red`
        FROM
            (SELECT 
            `f`.`clientid` AS `clientId`,
                `f`.`mealDateITZ` AS `mealDateITZ`,
                IF(((`f`.`mealType` = 'MORNING_SNACK')
                    OR (`f`.`mealType` = 'AFTERNOON_SNACK')), 'SNACK', `f`.`mealType`) AS `mealType`,
                SUM(`f`.`quantity`) AS `totalsum`,
                (SUM(IF((`f`.`E5foodgrading` = 'Red'), 1, 0)) - COUNT(DISTINCT `p`.`foodId`)) AS `totalRed`
        FROM
            (`twins`.`v_food_recorded` `f`
        LEFT JOIN `twins`.`personalizedfoods` `p` ON (((`f`.`clientid` = `p`.`clientId`)
            AND (`f`.`foodid` = `p`.`foodId`)
            AND (`p`.`recommendationRating` IS NOT NULL))))
        WHERE
            ((`f`.`rankGrade` = 'Overall')
                AND (`f`.`mealDateITZ` >= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 70 DAY)))
        GROUP BY `f`.`clientid` , `f`.`mealDateITZ` , IF(((`f`.`mealType` = 'MORNING_SNACK')
            OR (`f`.`mealType` = 'AFTERNOON_SNACK')), 'SNACK', `f`.`mealType`)) `a`) `x`
        GROUP BY `x`.`clientid` , `x`.`mealdateITZ`)) `s1`
        LEFT JOIN (SELECT 
            `f`.`clientid` AS `clientid`,
                `f`.`mealdate` AS `mealdate`,
                MAX(IFNULL(`f`.`b_fasting`, 0)) AS `b_fasting`,
                MAX(IFNULL(`f`.`l_fasting`, 0)) AS `l_fasting`,
                MAX(IFNULL(`f`.`s_fasting`, 0)) AS `s_fasting`,
                MAX(IFNULL(`f`.`d_fasting`, 0)) AS `d_fasting`
        FROM
            (SELECT 
            `twins`.`mealjourneys`.`clientId` AS `clientid`,
                `twins`.`mealjourneys`.`mealdate` AS `mealdate`,
                (CASE
                    WHEN (`twins`.`mealjourneys`.`mealType` = 'breakfast') THEN `twins`.`mealjourneys`.`fasting`
                END) AS `b_fasting`,
                (CASE
                    WHEN (`twins`.`mealjourneys`.`mealType` = 'lunch') THEN `twins`.`mealjourneys`.`fasting`
                END) AS `l_fasting`,
                (CASE
                    WHEN (`twins`.`mealjourneys`.`mealType` = 'snack') THEN `twins`.`mealjourneys`.`fasting`
                END) AS `s_fasting`,
                (CASE
                    WHEN (`twins`.`mealjourneys`.`mealType` = 'dinner') THEN `twins`.`mealjourneys`.`fasting`
                END) AS `d_fasting`
        FROM
            `twins`.`mealjourneys`
        WHERE
            (`twins`.`mealjourneys`.`mealdate` >= (CAST(ITZ(NOW()) AS DATE) - INTERVAL 70 DAY))) `f`
        GROUP BY `f`.`clientid` , `f`.`mealdate`) `s2` ON (((`s1`.`clientid` = `s2`.`clientid`)
            AND (`s1`.`mealdateITZ` = `s2`.`mealdate`))))) `s` ON (((`c`.`clientId` = `s`.`clientId`)
            AND (`c`.`eventDateITZ` = `s`.`eventDateITZ`))))
        LEFT JOIN `twins`.`bi_patient_monitor_measures` `x` ON (((`c`.`clientId` = `x`.`clientid`)
            AND (`c`.`eventDateITZ` = `x`.`measure_event_date`)
            AND (`x`.`measure_name` = 'NUT COM'))))