alter view v_active_clients as 
SELECT 
        c.ID AS clientId,
        UPPER(CONCAT(cp.firstName, ' ', cp.lastName)) AS patientName,
        UPPER(CONCAT(cp.firstName, ' ', LEFT(cp.lastName, 1))) AS patientNameShort,
		ap.city AS city,
		UPPER(ifnull(pc.divisionName,'')) as city_new,
        UPPER(ifnull(pc.circleName,'')) as state,
        UPPER(CONCAT(d.firstName, ' ', d.lastName)) AS doctorName,
        UPPER(CONCAT(d.firstName, ' ', LEFT(d.lastName, 1))) AS doctorNameShort,
        UPPER(CONCAT(h.firstName, ' ', h.lastName)) AS coachName,
        UPPER(CONCAT(h.firstName, ' ', LEFT(h.lastName, 1))) AS coachNameShort,
        c.gender,
        cp.userName AS userName,
        ((YEAR(CURDATE()) - YEAR(cp.dateOfBirth)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(cp.dateOfBirth, '%m%d'))) AS age,
        c.enrollmentDate AS enrollmentDate,
        c.paidType AS paidType,
        t.name AS cohortGroup,
		ITZ(c.enrollmentDate) AS enrollmentDateITZ,
        (vs.VisitDate) AS visitDateITZ,
        ITZ(v.homeVisitDate) as homeVisitDateITZ,
        ITZ(v.dateAdded) AS visitDateAddedITZ,
        c.renewalVisitScheduled AS renewalVisitScheduledDate,
        c.dischargeReason AS dischargeReason,
		CASE
            WHEN (c.status = 'Active') THEN (TO_DAYS(ITZ(NOW())) - TO_DAYS(c.enrollmentDate))
            WHEN (c.status <> 'Active') THEN (TO_DAYS(c.lastStatusChange) - TO_DAYS(c.enrollmentDate))
        END AS enrollmentDates, 
		-- (FLOOR(((TO_DAYS(c.termEndDate) - TO_DAYS(c.enrollmentDate)) / 84)) - 1) AS renewalCount,
        null as renewalCount,
        c.firstDiabetesInReversalTime, 
        c.termEndDate AS termEndDate,
        /*
        CASE
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 23) THEN 'D2-22'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 43) THEN 'D23-42'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 57) THEN 'D43-56'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 85) THEN 'D57-84'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) >= 85) THEN 'D85PLUS'
        END AS escalationStage,
        */
        null as escalationStage,
        c.planCode AS planCode,
        c.labels,
        c.vegetarian,
        
        -- if(ttp.clientid is not null and ttp.is_patient_currently_TTP = 'Yes', 'Yes', '') as isPatientCurrentTTP
        ifnull(dc.isM1,'') as isPatientCurrentTTP
    FROM
       twins.clients c
       inner join twinspii.clientspii cp on c.id = cp.id
       LEFT JOIN twinspii.addressespii ap on c.addressId = ap.ID
        left join (select distinct pincode, divisionName, circleName from bi_address_lookup_new where deliveryStatus = 'Delivery') pc on ltrim(rtrim(replace(ap.postalCode,' ',''))) = pc.pinCode
        JOIN twins.doctors d ON c.doctorId = d.ID
        INNER JOIN twins.coaches h ON c.coachId = h.ID
        LEFT JOIN (
				select a.clientid, a.date_of_visit, a.homevisitdate, a.dateadded from 
                (
						SELECT 	clientId,
								DATE(visitDate) as date_of_visit,
								MAX(visitDate) AS homeVisitDate,
								MAX(DateAdded) AS dateAdded
						FROM homevisits
						GROUP BY clientId, date(visitdate) -- there are multiple entries for the same home schedule date so pick the one needed (most recent)
                ) a
                inner join (select clientid, MAX(DATE(visitdate)) as date_of_visit from homevisits group by clientid) b on a.clientid = b.clientid and a.date_of_visit = b.date_of_visit -- (pick the most recent schedule date)
        ) v ON (c.ID = v.clientId)
        LEFT JOIN cohorts t ON (c.cohortId = t.cohortId)
        LEFT JOIN  (
				SELECT 
					clientId AS CLIENTID,
					TIMESTAMP(MAX(startDate)) AS VisitDate
				FROM
					twins.momentumweekdetails
				GROUP BY clientId
        ) vs on c.id = vs.clientid 
        LEFT JOIN dim_client dc on c.id = dc.clientid and dc.is_row_current = 'y'
        -- LEFT JOIN bi_ttp_enrollments ttp on c.id = ttp.clientid
    WHERE
        ((c.status IN ('ACTIVE'))
            AND (c.deleted = 0)
            AND (d.test = FALSE))