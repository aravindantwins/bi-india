alter view v_bi_patient_TSA_CBG_alert_tracker as 
select date, case when  a.date = b.startdate then 'CycleStart' 
			      when  a.date = b.enddate then 'CycleEnd' end as cbgCycleStatus, 
c.clientid, c.patientname, c.city, c.tsa, c.coachNameShort as coach, c.status  as current_status
from v_date_to_date a left join predictedcgmcycles b on (a.date = b.startdate or a.date = b.enddate)
					   inner join v_allClient_list c on b.clientid = c.clientid 
where date >= date_sub(date(itz(now())), interval 14 day)
and c.status in ('Active')
;
 