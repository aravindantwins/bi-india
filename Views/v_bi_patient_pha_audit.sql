alter view v_bi_patient_pha_audit as -- created by Saantha
select a.id, v.patientname, v.enrollmentdateitz, a.counselorUserID, UPPER(CONCAT(up1.firstName, ' ', up1.lastName)) AS pha, a.modifiedBy, UPPER(CONCAT(up2.firstName, ' ', up2.lastName)) AS Modified_person, min(a.dateModified) pha_assigned_date
from clientauxfields_aud a
join v_bi_ttp_patient_detail v on (a.id=v.clientid)
left join twinspii.userspii up1 ON (a.counselorUserId = up1.ID)
left join twinspii.userspii up2 on (a.modifiedby=up2.ID)
where a.counselorUserID is not null
group by a.id, a.counselorUserID