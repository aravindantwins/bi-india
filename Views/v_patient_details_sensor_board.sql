create view v_patient_details_sensor_board
as
/*this section is to show each sensor measures by day by patient*/
select 
clientid, patientName, doctorName, coachName, enrollmentDate, enrollmentDateITZ, measure_event_date, 
measure_name, final_measure, targetvalue from v_patient_details_sensor_board_measures

union all

/*this section is to show the percentage of all 7 sensors data collection*/
select 
clientid, patientName, doctorName, coachName, enrollmentDate, enrollmentDateITZ, measure_event_date, 
measure_name, final_measure, targetvalue from v_patient_details_sensor_board_sensor_percentage

/* old definition
select distinct b.clientid, c.patientName, c.doctorName, c.coachName, enrollmentDate, enrollmentDateITZ, measure_event_date, 
a.measure_name, 
case when b.average_measure is null then 'No'
     when a.measure_name in ('Steps', 'Heartrate','Glucose','Action Score', 'CGM') then floor(average_measure)
     when a.measure_name in ('Sleep') then floor(average_measure)/60
     else b.average_measure end as final_measure,
ifnull(floor(d.targetvalue),'') as targetvalue
from
 (
	select distinct measure_name from bi_measures where measure_type='1d' 
    and measure_name in ('Sleep','Heartrate','Glucose','Ketone','CGM', 'Weight','Action Score', 'Steps')
 ) a inner join bi_measures b on a.measure_name = b.measure_name and b.measure_type='1d' 
inner join v_active_clients c on b.clientid = c.clientid
left join clienttodoschedules d on b.clientid = d.clientid and d.type='steps' and a.measure_name = 'steps' and b.measure_event_date between d.startdate and d.enddate


Union all

select b.clientid, c.patientName, c.doctorName, c.coachName, enrollmentDate, enrollmentDateITZ, measure_event_date, 
'Blood Pressure' as measureName, ifnull(bp_average_measure,'No') as final_measure, '' as targetvalue
from 
(
select a.clientid, a.measure_event_Date, group_concat(b.systolic_measure,'/',a.diastolic_measure) as bp_average_measure
from 
	(select clientid, measure_event_date, floor(average_measure) as diastolic_measure 
    from bi_measures where measure_type = '1d' and measure_name = 'Diastolic') a
	inner join 
	(select clientid, measure_event_date, floor(average_measure) as systolic_measure 
    from bi_measures where measure_type = '1d' and measure_name = 'Systolic') b
	on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date
group by a.clientid, a.measure_event_Date
) b, v_active_clients c
where b.clientid = c.clientid
*/
