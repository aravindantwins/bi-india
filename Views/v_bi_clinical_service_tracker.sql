set collation_connection = 'latin1_swedish_ci';

alter view v_bi_clinical_service_tracker as 
select a.date, 
count(distinct incidentID) as total_incidents, 

count(distinct case when category = 'symptoms_handling' then incidentId else null end) as total_Symp_incidents_reported,
count(distinct case when category = 'symptoms_handling' and b.status = 'OPEN' then incidentId else null end) as total_Symp_incidents_backlog,
count(distinct case when category = 'symptoms_handling' and b.status = 'OPEN' and datediff(date(resolved_time), report_date) > 3 then incidentId else null end) as total_Symp_incidents_more_than_3days_backlog,
count(distinct case when category = 'symptoms_handling' and b.status = 'RESOLVED' then incidentId else null end) as total_Symp_incidents_resolved, 

count(distinct case when category = 'patient_treatment' then incidentId else null end) as total_PT_incidents_reported,
count(distinct case when category = 'patient_treatment' and b.status = 'OPEN' then incidentId else null end) as total_PT_incidents_backlog,
count(distinct case when category = 'patient_treatment' and b.status = 'OPEN' and datediff(date(resolved_time), report_date) > 1 then incidentId else null end) as total_PT_incidents_more_than_1days_backlog,
count(distinct case when category = 'patient_treatment' and b.status = 'RESOLVED' then incidentId else null end) as total_PT_incidents_resolved, 

count(distinct case when category = 'symptoms_handling' and symptom_severity = 'HIGH' then incidentId else null end) as total_Symp_CRTICIAL_incidents,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'MEDIUM' then incidentId else null end) as total_Symp_MODERATE_incidents,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'LOW' then incidentId else null end) as total_Symp_MILD_incidents,

count(distinct case when category = 'symptoms_handling' and symptom_severity = 'HIGH' and address_time_hours is null then incidentId else null end) as total_Symp_CRTICIAL_no_FRT,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'HIGH' and address_time_hours > 1 then incidentId else null end) as total_Symp_CRTICIAL_FRT_more_than_SLA,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'HIGH' and b.status = 'RESOLVED' and close_duration_hours > 72 then incidentId else null end) as total_Symp_CRTICIAL_close_after_SLA,

count(distinct case when category = 'symptoms_handling' and symptom_severity = 'MEDIUM' and address_time_hours is null then incidentId else null end) as total_Symp_MODERATE_no_FRT,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'MEDIUM' and address_time_hours > 2 then incidentId else null end) as total_Symp_MODERATE_FRT_more_than_SLA,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'MEDIUM' and b.status = 'RESOLVED' and close_duration_hours > 48 then incidentId else null end) as total_Symp_MODERATE_close_after_SLA,

count(distinct case when category = 'symptoms_handling' and symptom_severity = 'LOW' and address_time_hours is null then incidentId else null end) as total_Symp_MILD_no_FRT,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'LOW' and address_time_hours > 24 then incidentId else null end) as total_Symp_MILD_FRT_more_than_SLA,
count(distinct case when category = 'symptoms_handling' and symptom_severity = 'LOW' and b.status = 'RESOLVED' and close_duration_hours > 24 then incidentId else null end) as total_Symp_MILD_close_after_SLA,

count(distinct case when category = 'PATIENT_TREATMENT' and subtype_l1 in ('PATIENT_TREATMENT_BLOOD_PRESSURE', 'PATIENT_TREATMENT_OTHER_ISSUES', 'PATIENT_TREATMENT_BETA_HYDROXYBUTYRATE', 'PATIENT_TREATMENT_HYPERGLYCEMIA', 'PATIENT_TREATMENT_HYPOGLYCEMIA') then incidentId else null end) as total_PT_CRTICIAL_incidents,
count(distinct case when category = 'PATIENT_TREATMENT' and subtype_l1 in ('PATIENT_TREATMENT_BLOOD_PRESSURE', 'PATIENT_TREATMENT_OTHER_ISSUES', 'PATIENT_TREATMENT_BETA_HYDROXYBUTYRATE', 'PATIENT_TREATMENT_HYPERGLYCEMIA', 'PATIENT_TREATMENT_HYPOGLYCEMIA') and address_time_hours > 1 then incidentId else null end) as total_PT_CRTICIAL_FRT_more_than_SLA,
count(distinct case when category = 'PATIENT_TREATMENT' and subtype_l1 in ('PATIENT_TREATMENT_BLOOD_PRESSURE', 'PATIENT_TREATMENT_OTHER_ISSUES', 'PATIENT_TREATMENT_BETA_HYDROXYBUTYRATE', 'PATIENT_TREATMENT_HYPERGLYCEMIA', 'PATIENT_TREATMENT_HYPOGLYCEMIA') and close_duration_hours > 72 then incidentId else null end) as total_PT_CRTICIAL_close_after_SLA,

count(distinct (case when (b.category = 'SYMPTOMS_HANDLING' or b.category = 'PATIENT_TREATMENT') and b.status = 'RESOLVED' and suspended is false then b.incidentId else null end)) as total_Symp_PT_incidents_resolved_exclude_suspended,
count(distinct (case when (b.category = 'SYMPTOMS_HANDLING' or b.category = 'PATIENT_TREATMENT') and b.status <> 'OBSERVATION' and suspended is false then b.incidentId else null end))  as total_Symp_PT_incidents_exclude_observation_suspended, 

count(distinct case when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'HIGH') or (category = 'PATIENT_TREATMENT' and PT_severity = 'HIGH')) and b.status='RESOLVED' and suspended is false and close_duration_hours < 1 then b.incidentId else null end) as total_Symp_PT_severe_resolved_within_SLA_exclude_suspended, 
count(distinct case when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'HIGH') or (category = 'PATIENT_TREATMENT' and PT_severity = 'HIGH')) and b.status='RESOLVED' and suspended is false and close_duration_hours >= 1 then b.incidentId else null end) as total_Symp_PT_severe_resolved_SLA_Breached_exclude_suspended,

count(distinct case when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'MEDIUM') or (category = 'PATIENT_TREATMENT' and PT_severity = 'MEDIUM')) and b.status='RESOLVED' and suspended is false and close_duration_hours < 4 then b.incidentId else null end) as total_Symp_PT_moderate_resolved_within_SLA_exclude_suspended,
count(distinct case when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'MEDIUM') or (category = 'PATIENT_TREATMENT' and PT_severity = 'MEDIUM')) and b.status='RESOLVED' and suspended is false and close_duration_hours >= 4 then b.incidentId else null end) as total_Symp_PT_moderate_resolved_SLA_Breached_exclude_suspended, 

count(distinct case when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'LOW') or (category = 'PATIENT_TREATMENT' and PT_severity = 'LOW')) and b.status='RESOLVED' and suspended is false and close_duration_hours < 24 then b.incidentId else null end) as total_Symp_PT_mild_resolved_within_SLA_exclude_suspended,
count(distinct case when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'LOW') or (category = 'PATIENT_TREATMENT' and PT_severity = 'LOW')) and b.status='RESOLVED' and suspended is false and close_duration_hours >= 24 then b.incidentId else null end) as total_Symp_PT_mild_resolved_SLA_Breached_exclude_suspended,


count(distinct case when c.publishStatus = 'New' then c.clientid else null end) as firstMedPublish, 
count(distinct case when c.publishStatus = 'Repeated' then c.clientid else null end) as repeatMedPublish,

count(distinct case when c.publishStatus = 'New' and ifnull(timestampdiff(hour, d0_dc_appnt_time, actionedDate), c.actionTime) > 4 then c.clientid else null end) as firstMedPublish_more_than_SLA, 
count(distinct case when c.publishStatus = 'Repeated' and c.actionTime > 24 then c.clientid else null end) as repeatMedPublish_more_than_SLA,

count(distinct case when c.status in ('AUTOMATED') then c.clientid else null end) as medsAutomated, 
count(distinct case when c.status in ('OVERRIDDEN', 'SKIPPED_OVERRIDDEN') then c.clientid else null end) as medsOverridden, 
count(distinct case when c.status in ('SKIPPED') then c.clientid else null end) as medsSkipped,
count(distinct case when c.status in ('MANUAL') then c.clientid else null end) as medsManual,

count(distinct case when changeType_Psuedo = 'Yes' then c.clientid else null end) as pSuedo_medpublished,

total_active + total_PA as total_Active_PA,

total_Manual_cohort

from (select date from v_date_to_date where date >= curdate() - interval 30 day) a left join 
(
	select distinct incidentId, clientid, category, symptom_severity, status, subtype_L1, incident_classification, 
	case when category = 'SYMPTOMS_HANDLING' then null 
		 when category = 'patient_treatment' and incident_classification like '%NON_CRITICAL%' then 'LOW' 
		 else 'HIGH' end as PT_severity,
	itz(report_time) as report_time,
    date(itz(report_time)) as report_date,
    if(status = 'OPEN', itz(now()), itz(resolved_time)) as resolved_time, 
    itz(owner_entry_date) as owner_entry_date, 
	itz(first_response_time) as first_response_time, 
	address_time_hours, 
	close_duration_hours,
	c.suspended
	from bi_incident_mgmt a left join clientauxfields c on a.clientid = c.id 
	where category in ('symptoms_handling','patient_treatment') 
	and date(report_time) >= curdate() - interval 1 month
) b on a.date = b.report_date

left join 
(
			select f1.clientid, recommendationDate, status, actionedDate, 
			case when f1.recommendationdate <> f2.newpublishingdate then 'Repeated' 
				 when f1.recommendationdate = f2.newpublishingdate then 'New' else null end as publishStatus, actionTime,
            case when f1.status in ('MANUAL','OVERRIDDEN') and f1.actualmedication ='No medicine' and f1.recommendedmedication='No medicine' then 'Yes' else null end as changeType_Psuedo,
            itz(c.eventtime) as d0_dc_appnt_time,
            timestampdiff(hour, itz(c.eventtime), actionedDate)
            
			from
			(
					select distinct a.clientid, 
					date(recommendationdate) as recommendationdate,
					status, actionTime, actionedDate, 
                    a.actualmedication,
					a.recommendedmedication
					from
					(
						select a.clientid, a.medicationTierId, eventtime as recommendationdate, status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime,
						b.actualMedication as actualmedication,b.recommendedMedication as recommendedmedication
						from
						(
								select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId, type
								from medicationrecommendations a inner join clients b on a.clientid = b.id 
																 left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'
								where type = 'DIABETES' and date(eventtime) >= curdate() - interval 35 day 
								and (labels not like '%RESEARCH%' and labels not like '%unqualified%')
								and ifnull(c.is_type1like, 'No') <> 'Yes'
						) a inner join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
					) a
			) f1 left join (select clientid, min(date(itz(eventtime))) as newpublishingdate from medicationrecommendations where type = 'DIABETES' group by clientid) f2 on f1.clientid = f2.clientid  -- where medicationTierId is not null 
				 left join (
								select c.clientid, c.eventtime, d.dateadded 
								from clientappointments c inner join doctorappointments d on (c.clientid = d.clientid and c.clientAppointmentID = d.clientAppointmentId)
								where c.type='DOCTOR_APPOINTMENT' 
								and d.quarter = '0' and d.day = '0'
								and c.status not in ('CANCELLED')
								and date(c.eventtime) >= curdate() - interval 35 day 
							) c on f1.clientid = c.clientid 
) c on a.date = c.recommendationDate
left join 
(
		select a.date, 
		count(distinct case when b.status = 'Active' then b.clientid else null end) as total_active, 
		count(distinct case when b.status = 'pending_active' then b.clientid else null end) as total_PA, -- this will show as 0 from Aug 30, 2021 because clinical ops doesn't want to include Pending Active into the base anymore. 
        count(distinct case when pt.cohortname = 'M' and (c.labels not like '%RESEARCH%') and (c.labels not like '%unqualified%') and ifnull(dc.is_type1like, 'No') <> 'Yes' then pt.clientid else null end) as total_Manual_cohort
        
		from v_date_to_date a inner join bi_patient_Status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
							  left join v_Client_tmp c on b.clientid = c.clientid
                              left join bi_patient_reversal_state_gmi_x_date pt on a.date = pt.date and b.clientid = pt.clientid 
                              left join dim_client dc on c.clientid = dc.clientid and dc.is_row_Current = 'y'
		where a.date >= date_sub(date(now()), interval 30 day) and a.date <= date(now())
		and c.patientname not like '%obsolete%'
		group by date
) d on a.date = d.date 
-- where a.date >= date_sub(date(itz(now())), interval 30 day)
group by date 
;  
