SET collation_connection = 'latin1_swedish_ci'; 

alter view v_bi_member_order_detail as 
select a.clientid, c.patientname, c.mobileNumber, c.address, c.email, date(itz(scheduledTime)) as scheduledDate, 
itz(scheduledTime) as scheduledTime, memberOrderType, memberOrderStatus, deliveryPartner, itz(deliveredTime) as deviveredTime,
 b.ordertype as medCondition, name as medicineName, quantity as medQuantity, dosage as medDosage, c.city, c.postalcode
 from memberorders a inner join memberorderdetails b on a.memberOrderId = b.memberOrderId
					 inner join v_client_tmp c on a.clientid = c.clientid 
where date(itz(scheduledTime)) >= curdate() - interval 2 month 					 
;					 
