set collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_reversal_management_detail as 
select clientid, patient, coach, leadCoach, date, treatmentDays, daysFromConversionDate, 
enrollType, medCount, medicine_drugs, medicine, 
as_5d, nut_adh_5d, cohortname, isPatientIncluded as old_isPatientIncluded, isPatientIncludedCommercial as isPatientIncluded,
cgm_5d,
is_InReversal,
is_MetOnly_by_OPS,
is_Escalation,
is_type1like,
is_medical_exclusion,
suspended,
isMedExclusion_to_include,
isUnqualified,
isDiabeticMet,
daysFromLast5d,
isExcluded,
is_app_suspended,
isNABrittle,
nonAdh_brittle_category,
totalEscalationDays,
isInReversal_previous_day,
is_Excluded_timetracked
from 
(
	select clientid, patient, coach, leadCoach, date, treatmentDays,isDiabeticMet, daysFromLast5d, daysFromConversionDate, 
	case when isM1_Converted = 'Yes' then 'M1-M2'
		 when isTTP = 'Yes' then 'M1'
         else 'M2' end as enrollType, medCount, ifnull(medicine_drugs,'') as medicine_drugs, ifnull(medicine,'') as medicine,suspended,
	as_5d,
	nut_adh_syntax_5d as nut_adh_5d,
    cgm_5d,
	cohortname,
   
	case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No'  then 'Yes' 
		 when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No' then 'Yes' else 'No' end as isPatientIncluded,
    case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then 'Yes'
		 when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then 'Yes' else 'No' end as isPatientIncludedCommercial,
    ifnull(is_InReversal, 'No') as is_InReversal,
	is_MetOnly_by_OPS,
	if(is_InReversal is null and is_MetOnly_by_OPS = 'No', 'Yes', 'No') as is_Escalation,
    is_type1like,
    is_medical_exclusion,
    isMedExclusion_to_include,
    isUnqualified,
    is_app_suspended,
    isNABrittle,
    nonAdh_brittle_category,
    isExcluded,
    totalEscalationDays
    ,isInReversal_previous_day
    ,is_Excluded_timetracked
	from 
	(
		select distinct a.clientid, ct.patientnameShort as patient, ct.doctornameShort as doctor, coach, ct.leadCoachNameShort as leadCoach, a.date, datediff(a.date, b.last_available_cgm_5d_date) as daysFromLast5d, a.treatmentDays, datediff(a.date, ttp.conversion_payment_date) as daysFromConversionDate, 
		b.cohortname,
		b.is_InReversal, 
		b.is_MetOnly_by_OPS,
		b.isNoCGM_14days,
		b.cgm_5d,
		b.actual_medCount as medCount, 
		b.medicine, 
		b.medicine_drugs,
		b.as_5d,
		b.nut_adh_syntax_5d, 
		if(ifnull(b.start_medCount,0) > 0 or b.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
		if((ttp.is_patient_currently_TTP = 'yes' or (ttp.is_patient_currently_TTP is null and ttp.is_patient_converted is null)) and ttp.clientid is not null, 'Yes', 'No') as isTTP,
		if(ttp.is_patient_converted = 'yes' and ttp.clientid is not null, 'Yes', 'No') as isM1_Converted,    
		cx.suspended,
		if(ct.labels like '%unqualified%', 'Yes', 'No') as isUnqualified,
		if(ct.labels like '%exclude%', 'Yes', 'No') as isExcluded,
        b.nonAdh_brittle_category, 
		if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
		ifnull(dc.is_type1like, 'No') as is_type1like,
		dc.is_medical_exclusion,
		case when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal = 'Yes' or b.is_MetOnly_by_OPS = 'Yes') then 'Yes' 
			when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal is null and b.is_MetOnly_by_OPS = 'No') then 'No' 
			else null end as isMedExclusion_to_include,
		
        case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' 
         else 'No' end as isMedExclusion_AST_ALT_BMI,
         
		case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('Age') then 'Yes' 
         else 'No' end as isMedExclusion_Age,
		if(at.clientid is not null, 'Yes', 'No') as is_app_suspended,
        totalEscalationDays
		,if(b1.is_InReversal = 'Yes' or b1.is_MetOnly_by_OPS = 'Yes', 'Yes', 'No') as isInReversal_previous_day
		,ifnull(b.isExcludeLabelOn, 'No') as is_Excluded_timetracked
		from 
		(
			select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
			from twins.v_date_to_date a
			inner join
			(
				select clientid, coach, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
				from
				(
					select clientid, coachname_short as coach, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
					from bi_patient_status
					where status = 'active'
					group by clientid, date(visitdateitz)
				) s
			) b on a.date between b.startDate and b.endDate
			where a.date >= date_sub(date(itz(now())), interval 7 day)   -- >  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))
		) a left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.date = b.date 
			left join bi_ttp_enrollments ttp on a.clientid = ttp.clientid
			left join clientauxfields cx on a.clientid = cx.id
			left join v_client_tmp ct on a.clientid = ct.clientid 
			left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
			left join bi_patient_suspension_tracker at on a.clientid = at.clientid and a.date between at.startdate and at.endDate and at.status = 1
            left join (
						select clientid, count(distinct case when is_InReversal is null and is_MetOnly_by_OPS = 'No' then date else null end) totalEscalationDays, 
                        count(distinct case when is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' then date else null end) totalReversalDays
						from bi_patient_reversal_state_gmi_x_date
						group by clientid 
					  )rd on a.clientid = rd.clientid 
			left join bi_patient_reversal_state_gmi_x_date b1 on a.clientid = b1.clientid and a.date = date_sub(b1.date, interval 1 day)
            where ct.patientname not like '%obsolete%'

	) s 
) s 
where isPatientIncludedCommercial = 'Yes' 
;

