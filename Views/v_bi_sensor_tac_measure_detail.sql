alter view v_bi_sensor_tac_measure_detail
as

select 
a.clientid,
patientname,
doctornameshort as doctor,
coachnameshort as coach,
measure_date,
eventtime,
measure,
intake,
sensor,
time as T,
accuracy as A,
complete as C 
from bi_sensor_tac_measure a inner join v_client_tmp b on a.clientid = b.clientid 
where a.measure_date >= date_sub(date(now()), interval 1 month)