create view v_bi_overall_business_target_measure_x_doctor
as 
select 
	date, 
    doctorname, 
	category, 
	measure_name,
	measure_group, 
	actual_measure, 
	target_measure
from bi_overall_business_target_measure_x_doctor
;