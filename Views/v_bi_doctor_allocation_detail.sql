alter view v_bi_doctor_allocation_detail as 
select doctorID, doctorName, city, plan, slotTime_inPerson, slotTime_online, totalSlots, actualAvailableSlots, spoc, 
total_TTP_enrolled, 
total_TTP_enrolled_last_7_days,
total_Regular_enrolled_last_7_days,
total_TTP_enrolled_last_7_days + total_Regular_enrolled_last_7_days as total_enrolled_last_7_days,
total_TTP_enrolled_last_7_days_by_doctor,
total_Regular_enrolled_last_7_days_by_doctor,
total_automated_recommendations_last_7_days,
time_taken_to_action_recommendations_last_7_days,
platform_recommendations_compliance_last_7_days,
doctor_support_rating,
closure_time_to_escalations_last_7_days,
daily_pending_active_cnt,
pending_active_case_percentage,
med_adoption_rating,
med_compliance_rating,
closure_escalation_rating,
PA_cases_rating,
doctor_driven_enroll_rating,
patient_rating,
BD_score,
total,
case when Total between 3.5 and 5 then 'HIGH'
	 when Total between 2 and 3.4 then 'MEDIUM'
     when Total < 2 then 'LOW' end as priority
from 
(
		select a.doctorId, c.doctorname, a.city, a.plan, a.slotTime_inPerson, a.slotTime_online, a.totalSlots, a.actualAvailableSlots, a.spoc, 
		total_TTP_enrolled, 
		total_TTP_enrolled_last_7_days,
		total_Regular_enrolled_last_7_days,
		total_TTP_enrolled_last_7_days + total_Regular_enrolled_last_7_days as total_enrolled_last_7_days,
		total_TTP_enrolled_last_7_days_by_doctor,
		total_Regular_enrolled_last_7_days_by_doctor,
		total_automated_recommendations_last_7_days,
		time_taken_to_action_recommendations_last_7_days,
		platform_recommendations_compliance_last_7_days,
		doctor_support_rating,
		closure_time_to_escalations_last_7_days,
		daily_pending_active_cnt,
		pending_active_case_percentage,
		med_adoption_rating,
		med_compliance_rating,
		closure_escalation_rating,
		PA_cases_rating,
		doctor_driven_enroll_rating,
		patient_rating,
		a.BD_score,
		cast((ifnull(med_adoption_rating,0) + ifnull(med_compliance_rating,0) + ifnull(closure_escalation_rating,0) + ifnull(doctor_driven_enroll_rating,0) + ifnull(PA_cases_rating,0) + ifnull(patient_rating,0) + ifnull(BD_score,0))
		/(if(med_adoption_rating is not null, 1, 0) + 
		 if(med_compliance_rating is not null, 1, 0) + 
		 if(closure_escalation_rating is not null, 1, 0) + 
		 if(doctor_driven_enroll_rating is not null, 1, 0) + 
		 if(PA_cases_rating is not null, 1, 0) + 
		 if(patient_rating is not null, 1, 0) + 
		 if(BD_score is not null, 1, 0)) as decimal(5,1)) as Total
		
		from bi_doctor_allocation_helper a left join bi_doctor_allocation_detail b on a.doctorId = b.doctorId
										   left join (select distinct id as doctorId, upper(concat(firstname,' ',lastname)) as doctorName from doctors where test = false) c on a.doctorId = c.doctorId
		where a.showInReport = 'Y'

) s 
;

