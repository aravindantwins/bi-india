    create view v_bi_patient_detail_x_doctor_crm as 
    select s.clientid, s.patientname, s.status_start_date, s.doctorName, s.status, s.is_drc from 
    (
    select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'pending_active') as status, if(c.source = 'doctor','Yes','No') as is_drc from 
	bi_patient_status b left join v_client_tmp c on b.clientid = c.clientid 
	where b.status = 'pending_active'
    and b.clientid not in (select clientid from v_Client_tmp where status = 'registration') -- patients are going back to registration status from pending_active but not getting captured in audit trail 
    
    union all 
    
	select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status, if(c.source = 'doctor','Yes','No') as is_drc  from 
	bi_patient_status b left join v_client_tmp c on b.clientid = c.clientid 
	where b.status = 'discharged'
    and b.clientid not in (select clientid from bi_patient_Status where status = 'active' and status_end_date = date(itz(now())))


    union all 
    
	select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status, if(c.source = 'doctor','Yes','No') as is_drc  from 
	bi_patient_status b left join v_client_tmp c on b.clientid = c.clientid 
	where b.status = 'inactive'
    and b.clientid not in (select distinct clientid from bi_patient_status where status = 'discharged')
    
    union all 
    
	select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'active') as status, if(c.source = 'doctor','Yes','No') as is_drc  from 
	bi_patient_status b left join v_client_tmp c on b.clientid = c.clientid 
	where b.status = 'active'
    ) s inner join bi_doctor_fee_ref s1 on s.doctorname = s1.doctorname and s1.isContractSigned = 'Y' and s1.year_no = 'Y1'
    where status_start_date >= '2020-07-01'
    ;