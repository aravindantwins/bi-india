create view v_bi_patient_treatment_detail as 
select 
a.clientid, 
a.patientname, 
a.doctorname, 
b.durationYears_diabetes, 
b.treatmentDays, 																						
c.start_medCount,												
c.actual_medCount as current_medCount,
c.is_MedReduced,
d.first_available_5dg, 										
d.last_available_5dg, 	
if(d.last_available_5dg < d.first_available_5dg, 'Yes', 'No') as is_glucose_reduced, 		
b.start_weight, 
b.last_available_weight, 
if(b.last_available_weight < b.start_weight, 'Yes', 'No') as is_weight_reduced, 
case when a.cohortGroup = 'In Reversal' then 'In Reversal'												
	 when a.cohortGroup = 'Default' then 'Default'											
	 else 'Escalation' end as cohortgroup, 		
total_nut_adh_days,
-- b.journey as nutrition_journey, 												
-- ns.allowed_syntax,												
-- ns.nut_syntax as current_nut_syntax, 
-- ns.nut_syntax_color, 									
if(f.clientid is not null, 'Yes', 'No') as is_disenroll_risk		
from v_active_clients a left join dim_client b on a.clientid = b.clientid and b.is_row_current = 'y'												
						left join bi_patient_reversal_state_gmi_x_date c on a.clientid = c.clientid and c.date = date_sub(date(now()), interval 1 day)						
						left join (						
                                            select clientid, max(first_available_5dg) as first_available_5dg, max(last_available_5dg) as last_available_5dg
                                            from 
                                            (
													select s.clientid, 
                                                    case when s.first_available_date = s1.date then s1.cgm_5d else null end as first_available_5dg, 
													case when s.last_available_date = s1.date then s1.cgm_5d else null end as last_available_5dg
													from 
													(
														select clientid, min(date) as first_available_date, max(date) as last_available_date
														from bi_patient_reversal_state_gmi_x_date
														where cgm_5d is not null
														group by clientid
													) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and (s.last_available_date = s1.date or s.first_available_date = s1.date)
                                            ) s 
                                            group by clientid
                                   ) d on a.clientid = d.clientid 				                 
						 left join (
										select a.clientid, count(distinct case when measure = 'yes' then measure_event_date else null end) as total_nut_adh_days 
										from bi_patient_monitor_measures a inner join v_active_clients b on a.clientid = b.clientid
										where measure_name = 'nut adh'
										group by a.clientid 
                                    ) e on a.clientid = e.clientid 
-- 						 left join v_bi_patient_nut_adh_syntax ns on a.clientid = ns.clientid and ns.date = date_sub(date(now()), interval 1 day)												
						 left join bi_incident_mgmt f on a.clientid = f.clientid and f.category = 'dienrollment' and f.status = 'OPEN'												
  ;
  

      
      
