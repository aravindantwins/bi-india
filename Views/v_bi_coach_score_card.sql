alter view v_bi_coach_score_card as 
select report_date, a.coach, b.leadCoachNameShort, b.leadCoachStartDate, b.email, measure_name, measure_group, measure, b.startdate as coachStartDate
from bi_coach_measure_x_scorecard a left join v_coach b on a.coach = b.coachnameShort
where report_date >= date_sub(date(itz(now())), interval 60 day)
;