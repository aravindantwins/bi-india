select clientid, date(itz(eventtime)) as eventtimeitz, 
sum(count) as total_steps from steps where clientid = 15669
group by clientid, date(itz(eventtime));
;
 
 SELECT 
        c.clientId AS clientId,
        eventDateITZ,
        total_steps AS steps
 FROM
        v_active_clients c
        LEFT JOIN (
				select clientid, date(itz(eventtime)) as eventDateITZ, 
				sum(count) as total_steps 
                from steps 
                group by clientid, date(itz(eventtime))
                ) a  ON c.clientId = a.clientid
ORDER BY c.clientId, eventDateITZ
;
