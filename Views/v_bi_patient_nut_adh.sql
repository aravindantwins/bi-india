create view v_bi_patient_nut_adh as
SELECT distinct
        cd.clientId AS clientid,
        cd.patientname AS patientname,
        cd.coachname as coachname,
		cd.status,
        cd.daysenrolled, 
        cd.date AS date,
        a.average_measure AS 1d_netcarb,
        b.redspike_curate AS redspike_curate,
        -- a1.redfood_count AS redfood_count,
        (a1.redfood_count - a1.pstar_food_count) as redfood_count,
        a1.pstar_food_count as pstar_food_count,
        a1.tac_score AS tac_score,
        IF((ISNULL(a.average_measure)
                OR ISNULL(a1.redfood_count)
                OR ISNULL(a1.tac_score)),
            'UK',
            IF(((a.average_measure < case when ifnull(pstar_food_count,0) < 1 then a.average_measure+1
										  when ifnull(pstar_food_count,0) > 0 and daysenrolled < 84 then 65
                                          when ifnull(pstar_food_count,0) > 0 and daysenrolled > 84 then 75 end)
                    AND ((a1.redfood_count - a1.pstar_food_count) < 1)
                    AND (a1.tac_score >= 2)),
                1,
                0)) AS nut_adh,
        case 	when b.nut_adh_curate = 'Yes' then 1
				when b.nut_adh_curate = 'No' then 0
				else 'UK' end as nut_adh_cur,
		case 	when b.nut_com_curate = 'Yes' then 1
				when b.nut_com_curate = 'No' then 0
				else 'UK' end as nut_com
FROM
        (
			SELECT 
				d.date AS date,
				c.clientId AS clientId,
				a.patientName AS patientname,
				a.coachname as coachname,
                c.status,
				c.enrollmentDateITZ AS enrollmentDateITZ,
                a.daysenrolled
			FROM
			twins.ddate d
			JOIN twins.bi_patient_status c on (d.date between c.status_start_date and c.status_end_date and c.status = 'active')
			LEFT JOIN v_client_tmp a on c.clientid = a.clientid
        ) cd
LEFT JOIN twins.bi_measures a ON (cd.clientId = a.clientid AND cd.date = a.measure_event_date AND a.measure_type = '1d' AND a.measure_name = 'netcarb')
LEFT JOIN twins.bi_tac_measure a1 ON (cd.clientId = a1.clientid AND cd.date = a1.measure_date)
LEFT JOIN (SELECT 
				s.clientid AS clientid,
                s.eventdateitz AS eventdateitz,
                MAX(s.redspike_curate) AS redspike_curate,
                MAX(s.nut_adh_curate) as nut_adh_curate,
                MAX(s.nut_com_curate) as nut_com_curate
			FROM
            (
					SELECT 
						a.clientId AS clientid,
						CAST(ITZ(a.eventTime) AS DATE) AS eventdateitz,
						CASE WHEN s.callLogFieldId = 11 THEN s.value ELSE NULL END AS redspike_curate,
                        CASE WHEN s.callLogFieldId = 10 THEN s.value ELSE NULL END AS nut_adh_curate,
                        CASE WHEN s.callLogFieldId = 9 THEN s.value ELSE NULL END AS nut_com_curate
					FROM
					twins.calllogs a
					JOIN twins.calllogdetails s ON a.callLogId = s.callLogId
					WHERE CAST(a.dateAdded AS DATE) >= '2019-06-10'
            ) s
			GROUP BY s.clientid , s.eventdateitz
            ) b ON (cd.clientId = b.clientid AND cd.date = b.eventdateitz)


/*
        IF((ISNULL(a.average_measure)
                OR ISNULL(b.redspike_curate)
                OR ISNULL(a1.redfood_count)
                OR ISNULL(a1.tac_score)),
            'UK',
            IF(((a.average_measure < case when ifnull(pstar_food_count,0) = 0 then 46
									      when ifnull(pstar_food_count,0) > 0 and daysenrolled < 84 then 60
                                          when ifnull(pstar_food_count,0) > 0 and daysenrolled > 84 then 75 end)
                    AND (b.redspike_curate = 0)
                    AND (a1.redfood_count >= 0)
                    AND (a1.tac_score >= 2)),
                1,
                0)) AS nut_adh_new,
*/