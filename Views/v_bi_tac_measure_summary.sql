alter view v_bi_tac_measure_summary as
select 
measure_date, 
-- 'ALL' as sec_com,
count(distinct a.clientid) as active_patients,
cast(avg(tac_score) as decimal(3,2)) as TAC_Avg,
cast(sum(case when tac_score = 3 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_TAC_3,
cast(sum(case when tac_score >= 2 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_TAC_2,
cast(sum(case when curate_items_mealtype_indicator = 1 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_MealComplete,
cast(sum(case when curate_logtime_apart_indicator = 1 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_MealTime,
cast(sum(case when curate_spike_redfood_indicator = 1 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_redfood
-- sum(vlogs_created_by_client+vlogs_added_by_coach+vlogs_corrected_by_coach) as Total_VLs,
-- sum(num_voicelogs_reviewed_by_coach)  as VL_Reviewed,
-- cast(avg(VL_Review_time) as decimal(5,2)) as Avg_VL_Time_review,
-- sum(mlogs_created_by_client+mlogs_entered_by_coach+mlogs_corrected_by_coach) as Total_MLs
from v_bi_tac_measure_details a 
group by measure_date

union all

select 
measure_date, 
-- sec_com,
count(distinct a.clientid) as active_patients,
cast(avg(tac_score) as decimal(3,2)) as TAC_Avg,
cast(sum(case when tac_score = 3 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_TAC_3,
cast(sum(case when tac_score >= 2 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_TAC_2,
cast(sum(case when curate_items_mealtype_indicator = 1 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_MealComplete,
cast(sum(case when curate_logtime_apart_indicator = 1 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_MealTime,
cast(sum(case when curate_spike_redfood_indicator = 1 then 1 else 0 end)/count(a.clientid) as decimal(5,2)) as Patients_redfood
-- sum(vlogs_created_by_client+vlogs_added_by_coach+vlogs_corrected_by_coach) as Total_VLs,
-- sum(num_voicelogs_reviewed_by_coach)  as VL_Reviewed,
-- cast(avg(VL_Review_time) as decimal(5,2)) as Avg_VL_Time_review,
-- sum(mlogs_created_by_client+mlogs_entered_by_coach+mlogs_corrected_by_coach) as Total_MLs
from v_bi_tac_measure_details a 
group by measure_date -- , sec_com


/* ***Logic below is moved to bi_tac_measure table voicelog_review_time column so just reading it from the table 
******instead of calculating here
left join 
(
select clientid, date(itz(submittedTime)) as submitdate, cast(sum(total_review_time) as decimal(5,2)) as VL_Time_review
from
(select a.voicefoodlogid, a.clientid, a.submittedTime, b.reviewedTime, timestampdiff(minute, submittedtime, reviewedTime)/60 as total_review_time
from
(select voiceFoodLogId, clientId, eventTime as submittedTime from voicefoodlogs) a inner join
(select voiceFoodLogId, clientId, dateModified as reviewedTime from voicefoodlogs where humanOutput is not null) b
on a.clientid = b.clientid and a.voicefoodlogid = b.voicefoodlogid
where date(a.submittedtime) >= '2019-05-20'
) x
group by clientid, date(itz(submittedTime)) 
) b on a.clientid = b.clientid and a.measure_date = b.submitdate
*/ 
