create view v_bi_GFY_actual_x_prediction_detail as 
select category,
clientid,
mealid,
mealdate,
mealtype,
measuredAUC, 
predictionID,
predictedAUC,
absAUCError 
from bi_GFY_actual_x_prediction_auc_detail
;