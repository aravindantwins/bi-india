create view v_bi_patient_interview_response_x_category as 
select report_date, enrolledday, patientid, 'HealthImprovements' as category, max(case when HealthImprovements = 5 then 1 else 0 end) as star_5, 
max(case when HealthImprovements = '4' then 1 else 0 end) as star_4, 
max(case when HealthImprovements = '3' then 1 else 0 end) as star_3, 
max(case when HealthImprovements = '2' then 1 else 0 end) as star_2, 
max(case when HealthImprovements = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid

union all 

select report_date, enrolledday, patientid, 'CoachingSupport' as category, max(case when CoachingSupport = 5 then 1 else 0 end) as star_5, 
max(case when CoachingSupport = '4' then 1 else 0 end) as star_4, 
max(case when CoachingSupport = '3' then 1 else 0 end) as star_3, 
max(case when CoachingSupport = '2' then 1 else 0 end) as star_2, 
max(case when CoachingSupport = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid

union all 

select report_date, enrolledday, patientid, 'NutritionSatisfaction' as category, max(case when NutritionSatisfaction = 5 then 1 else 0 end) as star_5, 
max(case when NutritionSatisfaction = '4' then 1 else 0 end) as star_4, 
max(case when NutritionSatisfaction = '3' then 1 else 0 end) as star_3, 
max(case when NutritionSatisfaction = '2' then 1 else 0 end) as star_2, 
max(case when NutritionSatisfaction = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid

union all 

select report_date, enrolledday, patientid, 'DoctorSupport' as category, max(case when DoctorSupport = 5 then 1 else 0 end) as star_5, 
max(case when DoctorSupport = '4' then 1 else 0 end) as star_4, 
max(case when DoctorSupport = '3' then 1 else 0 end) as star_3, 
max(case when DoctorSupport = '2' then 1 else 0 end) as star_2, 
max(case when DoctorSupport = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid

union all 

select report_date, enrolledday, patientid, 'DailyRoutine' as category, max(case when DailyRoutine = 5 then 1 else 0 end) as star_5, 
max(case when DailyRoutine = '4' then 1 else 0 end) as star_4, 
max(case when DailyRoutine = '3' then 1 else 0 end) as star_3, 
max(case when DailyRoutine = '2' then 1 else 0 end) as star_2, 
max(case when DailyRoutine = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid

union all 

select report_date, enrolledday, patientid, 'LogisticSupport' as category, max(case when LogisticSupport = 5 then 1 else 0 end) as star_5, 
max(case when LogisticSupport = '4' then 1 else 0 end) as star_4, 
max(case when LogisticSupport = '3' then 1 else 0 end) as star_3, 
max(case when LogisticSupport = '2' then 1 else 0 end) as star_2, 
max(case when LogisticSupport = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid

union all 

select report_date, enrolledday, patientid, 'BloodDrawService' as category, max(case when BloodDrawService = 5 then 1 else 0 end) as star_5, 
max(case when BloodDrawService = '4' then 1 else 0 end) as star_4, 
max(case when BloodDrawService = '3' then 1 else 0 end) as star_3, 
max(case when BloodDrawService = '2' then 1 else 0 end) as star_2, 
max(case when BloodDrawService = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid

union all 

select report_date, enrolledday, patientid, 'AppFeedback' as category, max(case when AppFeedback = 5 then 1 else 0 end) as star_5, 
max(case when AppFeedback = '4' then 1 else 0 end) as star_4, 
max(case when AppFeedback = '3' then 1 else 0 end) as star_3, 
max(case when AppFeedback = '2' then 1 else 0 end) as star_2, 
max(case when AppFeedback = '1' then 1 else 0 end) as star_1
from v_bi_patient_interview_response
group by report_date, enrolledday, patientid