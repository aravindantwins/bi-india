create view v_bi_patient_clinic_results_x_disease
as 

select a.clientid, a.patientname, a.eventDateTimeITZ as eventtime, testValue as actual_measure, b.diseaseID, b.diseaseName, b.markerId, 
markerExpandedName, 
concat(markerNormalStart,'-',markerNormalEnd) as MarkerNormalRange, markerNormalStart, markerNormalEnd,
detectionRule
from 
(
select a.clientid, c.patientname, a.eventDateTimeITZ, b.markerID, a.testValue
from testmeasure a inner join testlookup b on a.testItemid = b.itemID
					inner join v_client_tmp c on a.clientid = c.clientid 
) a inner join 
(
select diseaseId, diseaseName, markerId, markerExpandedName, probability,
concat(markerNormalStart,'-',markerNormalEnd) as MarkerNormalRange, markerNormalStart, markerNormalEnd,
detectionRule
from ref_disease a inner join ref_testResult b on 
(
a.value1 = b.markerId or 
a.value2 = b.markerId or 
a.value3 = b.markerId or 
a.value4 = b.markerId or 
a.value5 = b.markerId or 
a.value6 = b.markerId or 
a.value7 = b.markerId or 
a.value8 = b.markerId or 
a.value9 = b.markerId or 
a.value10 = b.markerId 
) and a.category = 'markerId' 
where detectionRule <> ''
order by diseaseId, markerId
) b on a.markerID = b.markerId and testValue not between markerNormalStart and markerNormalEnd
;


