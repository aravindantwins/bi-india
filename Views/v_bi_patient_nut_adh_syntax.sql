set collation_connection  = 'latin1_swedish_ci'; 

alter view v_bi_patient_nut_adh_syntax as 
select s1.date, s1.clientid, s6.patientname, s6.coachnameShort as coach, s6.leadCoachnameShort as leadCoach, s6.doctornameShort as doctor, 
case when s4.cohortname = 'In Reversal' then 'In Reversal'
	 when s4.cohortname = 'Default' then 'Default' 
     when s4.cohortname is null then ''
     else 'Escalation' end as escalationCohort, s4.treatmentdays, 
s2.Target_NC, s2.Target_Calories, s2.Target_Fiber, s2.Target_Protein, 
cast(s5.net_carb as decimal(10,2)) as Actual_NC, 
cast(s5.calories as decimal(10,2)) as Actual_Calories, 
cast(s5.fibre as decimal(10,2)) as Actual_Fiber, 
cast(s5.protein as decimal(10,2)) as Actual_Protein, 
round(s21.macro) as macro, 
round(s21.micro) as micro, 
round(s21.biota) as biota, 
s3.tac_score, 
s7.nut_syntax as allowed_syntax, s4.nut_syntax, s4.nut_syntax_color, s4.nut_adh_syntax, s4.nut_adh_syntax_5d,
s4.is_InReversal,
s4.is_MetOnly_by_OPS,
if(s6.labels like '%TEMP_DISCHARGE%', 'Yes', '') as isTempDischarge, 
if(imt.clientid is not null, 'Yes', '') as isDisenrollOpen,
if((pst.clientid IS NOT NULL),'Yes','') AS isAppSuspended
from 	
(
	select a.date, b.clientId -- , startdate, (datediff(a.date, startDate) + 1) as treatmentDays
	from twins.v_date_to_date a
	inner join
	(
		select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
		from
		(
			select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
			from bi_patient_status
			where status = 'active'
			group by clientid, date(visitdateitz)
		) s
	) b on a.date between b.startDate and b.endDate
    where a.date >= date_sub(date(itz(now())), interval 3 month) and a.date <= date_sub(date(itz(now())), interval 1 day)
) s1 left join bi_twins_health_score_summary s2 on s1.clientid = s2.clientid and s1.date = s2.mealdate 
	 left join nutritionscores s21 on s1.clientid = s21.clientid and s1.date = s21.date
	 left join bi_tac_measure s3 on s1.clientid = s3.clientid and s1.date = s3.measure_date
     left join bi_patient_reversal_state_gmi_x_date s4 on s1.clientid = s4.clientid and s1.date = s4.date
     left join bi_foodlog_nutrition_target_summary s5 on s1.clientid = s5.clientid and s1.date = s5.mealdate and s5.category = '1d-Nutrient'
     left join v_client_tmp s6 on s1.clientid = s6.clientid
     left join dim_client s7 on s1.clientid = s7.clientid and s7.is_row_current = 'y'
     left join (select clientid, count(distinct incidentId) as totalIncidents from bi_incident_mgmt where category = 'disenrollment' and status = 'OPEN' group by clientid) as imt on s1.clientid = imt.clientid
     left JOIN bi_patient_suspension_tracker pst on ((s1.date between pst.startDate and endDate) and (pst.clientid=s1.clientid) and (pst.status=1))
; 
