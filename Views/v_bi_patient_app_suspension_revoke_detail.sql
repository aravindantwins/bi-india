alter view v_bi_patient_app_suspension_revoke_detail as 
select a.date, b.clientid, v.patientNameShort, v.email, v.Mobilenumber, v.city_new, b.datemodified, b.status,  c.outstandingBalance as Amount_Overdue, d.invoice_id, d.number, d.invoice_date
from v_date_to_date a
left join bi_patient_suspension_tracker b on a.date=startDate
left join clientauxfields c on (b.clientid=c.id)
left join (select customer_cf_clientid_unformatted as clientid, invoice_id, number, invoice_date from importedbilling.zoho_invoices where status = 'overdue') d on  (b.clientid=d.clientid)
inner join v_client_tmp v on (b.clientid= v.clientid)
where a.date  >=date_sub(date(itz(now())), interval 30 day)
and v.status = 'Active'
;