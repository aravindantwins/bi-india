set collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_d20_detail_internal as 
select clientid, patientname, coach, leadCoach, enrollmentType, day20, cohort, is_MetOnly, is_InReversal, cgm_5d, eRating_5d as coach_rating_5d, nut_adh_5d, nut_adh_1d, null as e5_5d, tac_5d, as_5d, success_ind, medicine_drugs, isDisenrolmentIMT_Open, isGPP, chosen_coachrating_d20, total_redfoods_last5days
from 
(
	select clientid, patientname, coach, leadCoach, enrollmentType, day20, cgm_5d, tac_5d, AS_5d,  eRating_5d,medicine_drugs, isDisenrolmentIMT_Open, isGPP,chosen_coachrating_d20,	total_redfoods_last5days,																					
	cohort, nut_adh_5d, nut_adh_1d, is_MetOnly,	is_InReversal, 																							
			if((nut_adh_5d = 'YES'														
					-- and ifnull(tac_5d,0) >= 2.5												
					and ifnull(AS_5d,0) >= 85 -- (if(AS_5d >= 85, 'Yes', if(AS_5d < 85 and total_incidents >= 1, 'Yes','No')) = 'Yes')																				
					and ifnull(chosen_coachrating_d20,4) >= 4												
				) , 'Yes','No') as success_ind																				
	from																							
	(																						
			select a.clientid, ct.patientname, CAST(ct.coachNameShort AS char(500)) as coach, CAST(ct.leadCoachNameShort AS char(500)) as leadCoach, a.visitdateitz, a.treatmentsetupdateITZ, day20,																						
			-- b4.average_measure as E5_5d, 
			re.coach_rating_5d as eRating_5d, 																					
			re.cgm_5d,																						
			re.as_5d,																						
			re.tac_5d,																						
			re.cohortname as cohort,	
            re.medicine_drugs,
			case when re.nut_adh_syntax_5d >= 0.8 then 'YES' else 'NO' end as nut_adh_5d, re.is_MetOnly_by_OPS as is_MetOnly, re.is_InReversal, re.nut_adh_syntax as nut_adh_1d,																	
			case when dc.isM1 = 'Yes' then 'M1'																						
				when dc.isM2Converted = 'Yes' then 'M1-M2'																					
			else 'M2' end as enrollmentType,
            case when im.clientid is not null then 'Yes' else 'No' end as isDisenrolmentIMT_Open,
            case when ct.planCode like 'GoldPro%' then 'Yes' else 'No' end as isGPP,
            cRat.chosen_coachrating_d20,
			-- ,count(distinct imt.incidentid) as total_incidents	
			sum(t.redfood_count) as total_redfoods_last5days																					
			from																						
				(																					
						select distinct b.clientid, doctorname, coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,																			
						date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 20 day)) as day20																		
						from																			
						(																			
							select clientid, doctor_name as doctorname, coachname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status,																		
							max(status_start_date) as status_start_date, max(status_end_date) as status_end_date																		
							from bi_patient_status b																		
							group by clientid, status																		
						) b																			
						where b.status = 'active'																			
						and datediff(status_end_date, if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ))) >= 20																			
						and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 20 day)) >= date_sub(date(itz(now())), interval 4 month)
						and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 20 day)) <= date(itz(now()))																			
				) a																					
				-- left join bi_measures b4 on a.clientid = b4.clientid and b4.measure_event_date = a.day20 and b4.measure_type = '5d' and b4.measure_name in ('E5%')																					
				-- left join (select distinct incidentid, clientid, status, date(report_time) as report_date from bi_incident_mgmt where category = 'sensors') imt on a.clientid = imt.clientid and report_date <= a.day20
				left join (select clientid, incidentid from incident where incidentType = 'DISENROLLMENT' and incidentstatus = 'OPEN') im on a.clientid = im.clientid 
				left join bi_patient_reversal_state_gmi_x_date re on a.clientid = re.clientid and a.day20 = re.date
				left join (
							select a.clientid, 
							max(case when a.recent_d20_rating_available_date = b.date then coach_rating else null end) as chosen_coachrating_d20
							from 
							(
								select clientid, 
								max(case when treatmentdays > 16 and treatmentdays <= 21 and coach_rating is not null then date else null end) as recent_d20_rating_available_date
								from bi_patient_reversal_state_gmi_x_date bprsgxd
								group by clientid 
							) a left join bi_patient_reversal_state_gmi_x_date b on (a.clientid = b.clientid and a.recent_d20_rating_available_date = b.date)
							group by a.clientid 
						  ) cRat on a.clientid = cRat.clientid				
				left join dim_client dc on a.clientid = dc.clientid and dc.is_row_Current = 'Y'																					
				left join v_client_tmp ct on a.clientid = ct.clientid
				left join bi_tac_measure t on a.clientid = t.clientid and t.measure_date between date_sub(a.day20, interval 4 day) and a.day20
				group by a.clientid
		) s																						
) s 
;