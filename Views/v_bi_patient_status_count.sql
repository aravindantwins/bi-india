create view v_bi_patient_status_count
as
select date, doctor_name as doctorName, status, count(distinct clientid) as total_clients from 
v_date_to_date a left join bi_patient_status b 
on a.date between b.status_start_date and b.status_end_date 
where b.status not in ('REGISTRATION','INACTIVE','PROSPECT')
group by date, doctor_name, b.status

union all 

select date, doctor_name as doctorName, if(doctor_name is null, null, 'Enrolled') as status, if(doctor_name is null, null, count(distinct clientid)) as total_clients from 
v_date_to_date a left join bi_patient_status b 
on a.date = date(b.enrollmentdateitz) and status = 'active'
group by date, doctor_name, b.status