-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_cgm_memberorders_detail as 
select date, clientid, patientname, city, tsa, memberOrderType, itz(eventtime) as eventtime, memberOrderStatus, cgmOrderPatchCategory, faultyReason
from v_date_to_date a left join 
(
	select a.clientid, c.patientname, c.city, a.memberOrderType, eventtime, tsa, memberOrderStatus, b.cgmOrderPatchCategory, b.faultyReason
	from memberorders a inner join cgmorderdetails b on a.memberOrderID = b.memberOrderID 
						left join v_allClient_list c on a.clientid = c.clientid
	where a.memberOrderType = 'CGM_ORDERS'
) b on a.date = date(itz(eventtime))
where a.date >= date_sub(date(now()), interval 1 month)
and clientid is not null
;


