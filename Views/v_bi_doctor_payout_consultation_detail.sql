-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_doctor_payout_consultation_detail as 
select a.clientid, ct.patientName, ct.doctorname, date(itz(a.eventtime)) as DC_scheduled_date, b.doctorId		   
from clientappointments a inner join doctorappointments b on a.clientid = b.clientid and a.clientAppointmentId = b.clientAppointmentId
						  inner join v_client_tmp ct on a.clientid = ct.clientid 
where type = 'DOCTOR_APPOINTMENT' and a.status = 'COMPLETED'
and date(itz(eventtime)) >= date_sub(date(itz(now())), interval 2 month)
;