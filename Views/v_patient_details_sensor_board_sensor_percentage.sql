alter view v_patient_details_sensor_board_sensor_percentage
as

select e.clientid, b.patientName, b.doctorName, b.coachName, b.enrollmentDate, b.enrollmentDateITZ, measure_event_date,'sensor_percentage' as measure_name, 
average_measure as final_measure, null as targetvalue
from bi_patient_status e inner join bi_measures a on (a.clientid = e.clientid and a.measure_event_date between e.status_start_date and e.status_end_date)
						left join v_client_tmp b on e.clientid = b.clientid
where measure_type='1d' 
and e.status = 'active'
and measure_name in ('sensor-AS')
and a.measure_event_date >= date_sub(date(itz(now())), interval 45 day)


-- OLD definition, use this in case the above one is creating problems.
-- this section is for the sensor_percentage calculation
/*
select clientid, patientName, doctorName, coachName, enrollmentDate, enrollmentDateITZ, measure_event_date, 'sensor_percentage' as measure_name, 
cast(((ketone_ind + sleep_ind + hr_ind + cgm_ind + weight_ind + bp_ind + steps_ind) / 7 ) * 100 as decimal(5,2)) as final_measure,
 '' as targetvalue
from
(
select clientid, patientName, doctorName, coachName, enrollmentDate, enrollmentDateITZ, measure_Event_Date, 
max(ketone_ind) as ketone_ind, 
max(sleep_ind) as sleep_ind,
max(hr_ind) as hr_ind,
max(cgm_ind) as cgm_ind,
max(weight_ind) as weight_ind,
max(bp_ind) as bp_ind,
max(steps_ind) as steps_ind
from 
(
select clientid, patientName, doctorName, coachName, enrollmentDate, enrollmentDateITZ, measure_event_date, 
case when measure_name = 'Ketone' and final_measure <> 'No' then 1 else 0 end as Ketone_ind, 
case when measure_name = 'Sleep' and final_measure <> 'No'  then 1 else 0 end as sleep_ind, 
case when measure_name = 'Heartrate' and final_measure <> 'No' then 1 else 0 end as hr_ind, 
case when measure_name = 'CGM' and final_measure <> 'No' then 1 else 0 end as cgm_ind, 
case when measure_name = 'Weight' and final_measure <> 'No' then 1 else 0 end as weight_ind, 
case when measure_name = 'Blood Pressure' and final_measure <> 'No' then 1 else 0 end as bp_ind, 
case when measure_name = 'steps' and final_measure <> 'No' and final_measure >= cast(targetvalue as decimal(8,2)) then 1 else 0 end as steps_ind 
from 
v_patient_details_sensor_board_measures
) a
group by clientid, measure_event_date
) s
;
*/
