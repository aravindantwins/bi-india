CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_overall_topmeal_actual_x_prediction_percentile` AS
    SELECT 
        `bi_overall_topmeal_actual_x_prediction`.`category` AS `category`,
        `bi_overall_topmeal_actual_x_prediction`.`mealID` AS `mealID`,
        `bi_overall_topmeal_actual_x_prediction`.`measuredAUC_25p` AS `measuredAUC_25p`,
        `bi_overall_topmeal_actual_x_prediction`.`measuredAUC_50p` AS `measuredAUC_50p`,
        `bi_overall_topmeal_actual_x_prediction`.`measuredAUC_75p` AS `measuredAUC_75p`,
        `bi_overall_topmeal_actual_x_prediction`.`measuredAUC_90p` AS `measuredAUC_90p`,
        `bi_overall_topmeal_actual_x_prediction`.`predictedAUC_25p` AS `predictedAUC_25p`,
        `bi_overall_topmeal_actual_x_prediction`.`predictedAUC_50p` AS `predictedAUC_50p`,
        `bi_overall_topmeal_actual_x_prediction`.`predictedAUC_75p` AS `predictedAUC_75p`,
        `bi_overall_topmeal_actual_x_prediction`.`predictedAUC_90p` AS `predictedAUC_90p`
    FROM
        `bi_overall_topmeal_actual_x_prediction`
    WHERE
        (`bi_overall_topmeal_actual_x_prediction`.`total_logged` > 1)