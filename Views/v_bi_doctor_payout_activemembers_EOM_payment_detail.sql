-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_doctor_payout_activemembers_EOM_payment_detail as 
select a.date, a.clientid, a.patientname, a.doctorname, a.source, a.planCode, a.active_status_start_date, b.invoice_date, b.invoice_number, b.invoice_status, b.payment_date, b.payment_mode, b.payment_amount
from 
(
			select distinct b.date, b.clientid, b.patientname, b.doctorname, b.status, source, planCode, c.active_status_start_date 
			from
			(select distinct date_sub(date, interval dayofmonth(date) day) as lastDayOfMonth from v_date_to_date) a	inner join 
			(
					select date, b.clientid, b.patientname, b.doctorname, b.status, source, plancode, status_start_date
					from v_date_to_date a inner join 
					(
						select distinct b.clientid, c.patientName, c.doctorName, source, plancode, b.status, status_start_date, status_end_date 
						from 
						bi_patient_status b inner join v_client_tmp c on b.clientid = c.clientid 
						where b.status = 'active' 
						and not exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'pending_active' and a.status_start_date >= b.status_end_date) 
						and c.patientname not like '%obsolete%' 
					) b on a.date between b.status_start_date and b.status_end_date 
					where date >= date_sub(itz(now()), interval 2 month)
			) b on a.lastDayOfMonth = b.date 
			left join ( 
						select clientid, min(status_start_date) as active_status_start_date 
						from bi_patient_status  
						where status = 'active' 
						group by clientid 
						) c on b.clientid = c.clientid
) a left join (
					select clientid, invoice_date, invoice_number, invoice_status, payment_date, payment_mode, invoice_amount, payment_amount, cf_service_purchased
					from 
					(
						select distinct a.cf_clientid_unformatted as clientid, c.invoice_date, c.number as invoice_number, c.status as invoice_status, d.date as payment_date, d.payment_mode, c.total as invoice_amount, d.amount as payment_amount, c.cf_service_purchased
						from importedbilling.zoho_customers a inner join clients b on a.cf_clientid_unformatted = b.id
															  left join importedbilling.zoho_invoices c on a.customer_id = c.customer_id
															  left join importedbilling.zoho_payments d on c.invoice_id = d.invoice_id
					) s 
					where payment_date >= date_sub(now(), interval 3 month) and invoice_status = 'PAID' and cf_service_purchased is null -- cf_service_purchased is null because this field is being used for twin store purchases
				) b on a.clientid= b.clientid and month(b.payment_date) = month(a.date) and year(b.payment_date) = year(a.date)
	-- inner join v_client_tmp c on a.clientid = c.clientid
;