alter view v_bi_CBG_patients_eligible_D21_predictor as 
select clientid, patientname, coach, email, mobilenumber, city, treatmentdays, leadCoach, plancode
from (	        
			select a.clientid, b.patientname, coachnameShort as coach, leadCoachNameShort as leadCoach, plancode, a.treatmentdays
            email, mobilenumber, city, s.date, s.treatmentdays, as_1d, as_5d, isCBGEligible, isCBG, is_MetOnly_by_OPS, is_MetOnly, 
            if(b.labels like '%prime%' or b.labels like '%control%', 'Yes', 'No') as isRCT
			from 
			(
					select a.date, b.clientId, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
					from twins.v_date_to_date a
					inner join
					(
						select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
						from
						(
							select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
							from bi_patient_status
							where status = 'active'
							group by clientid, date(visitdateitz)
						) s
					) b on a.date between b.startDate and b.endDate
					where a.date =  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now()))) 
            ) s left join bi_patient_reversal_state_gmi_x_date a on s.clientid = a.clientid and s.date = a.date 
				left join v_Client_tmp b on s.clientid = b.clientid 
			where s.treatmentdays >= 21 and s.treatmentdays < 35 and isCBGEligible = 'Y'            
) s 
;