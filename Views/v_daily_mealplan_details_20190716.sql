alter view v_daily_mealplan_details as
select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity,concat('Day ',a.day) as day, 
convert(sum(a.netcarb_measurevalue), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L1' as measureGroup,'Netcarb' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
c.measurementType as food_measure, c.measurementAmount as quantity, b.mealtype, carb - fibre as netcarb_measureValue, 
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.calories)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Calories' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
c.measurementType as food_measure, c.measurementAmount as quantity, b.mealtype, calories,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.ngc_measureValue)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'NGC' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
c.measurementType as food_measure, c.measurementAmount as quantity, b.mealtype, (IFNULL((((carb - fibre) * glycemicIndex) / 55), 0)) as ngc_measureValue,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.fibre)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fibre' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
c.measurementType as food_measure, c.measurementAmount as quantity, b.mealtype, fibre,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a,
     dailyplantemplatemappings b,
     foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.fat)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Fat' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
c.measurementType as food_measure, c.measurementAmount as quantity, b.mealtype, fat,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.protein)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Protein' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
c.measurementType as food_measure, c.measurementAmount as quantity, b.mealtype, protein,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
group by a.planname, a.day, a.mealtype, foodlabel

union all

select a.planname, a.mealtype, a.measuregroup, a.measurecategory, foodlabel, E5foodGrading, food_measure, quantity, concat('Day ',a.day) as day, 
convert((sum(a.carb)), decimal(7,2)) as measurevalue
from
(
select a.planName, 'L2' as measureGroup,'Total carb' as measureCategory, b.day, a.vegetarian, c.foodid, c.foodLabel, 
c.measurementType as food_measure, c.measurementAmount as quantity, b.mealtype, carb,
CASE WHEN ISNULL(c.recommendationRating) THEN 'Purple'
      WHEN (c.recommendationRating <= 1) THEN 'Red'
      WHEN ((c.recommendationRating > 1) AND (c.recommendationRating <= 2)) THEN 'Orange'
      WHEN ((c.recommendationRating > 2) AND (c.recommendationRating <= 3)) THEN 'Green'
      WHEN (c.recommendationRating > 3) THEN 'Green*' END as E5foodGrading
from dailyplantemplates a,
    dailyplantemplatemappings b,
    foods c 
where a.dailyplantemplateid = b.dailyplantemplateid
and b.foodid = c.foodid
) a
group by a.planname, a.day, a.mealtype, foodlabel
