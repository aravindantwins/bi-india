create view v_bi_food_supplement_log_archive 
as
select category, a.clientid as clientId, b.patientname as patientName, doctorname as DoctorName, b.coachName, 
eventdateITZ as eventDateITZ, eventTimeITZ as mealTimeITZ, itemName as foodName, 
measurementType as measure, quantity, categorytype as mealtype, 
net_carb, calories, fat, fibre, protein, total_Carb, netGICarb, 
E5grading as E5foodgrading, foodrating 
from bi_food_supplement_log_detail a 
left join v_client_tmp b on a.clientid = b.clientid
where a.eventdateitz >= date_sub(date(itz(now())), interval 3 month) and a.eventdateitz <= date(itz(now())) -- this condition is to limit the rows and improve the performane in Patient view dashboard,
and b.daysenrolled > (case when b.status = 'active' then 0 else 90 end) 

union all 

select null as category, 1 as clientid, null as patientName, null as DcotorName, null as coachName, 
date(date_sub(itz(now()),interval 1 day)) as eventDateITZ, date_sub(itz(now()),interval 1 day) as mealTimeITZ, null as foodName, 
null as measure, null as quantity, null as mealtype, 
null as net_carb, null as calories, null as fat, null as fibre, null as protein, null as total_Carb, null as netGICarb, 
null as E5foodgrading, null as foodrating 