alter view v_bi_Member1_patient_summary_tracker as 
select s1.date, s1.coach, 
s2.measure as Enrolled_cnt, 

s4.D0_count as Active_D0_cnt,
s4.D9_13_count as Active_D9_13_cnt,
s4.D9_13_NoSchedule as Active_more_than_D9_noReview, 
s4.D9_13_Scheduled as D9_13_Scheduled, 

s4.D9_13_Declined_Schedule as D9_13_Declined_Scheduled,
s4.D9_13_Pending_Schedule as D9_13_Pending_Schedule,
s4.D9_13_Pending_Schedule_NotConverted as D9_13_Pending_Schedule_NotConverted,
s4.D9_13_NoSchedule as D9_13_NoSchedule, 

s4.D10_count as Active_D10_cnt,
s4.D10_RC + s4.D10_RC_NoSchedule as Total_review_completed_d10_reached,

s5.scheduled_measure as Scheduled_cnt, 
s5.Cancelled_measure as Cancelled_cnt,
s11.schedule_created_measure as Schedule_created_cnt,

s7.converted_measure as Converted_cnt, 
s7.coachConverted_measure as CoachConverted_cnt, 
s7.Converted_within_14days_measure as Converted_with14days_cnt,
s7.CoachConverted_within_14days_measure as CoachConverted_within_14days_cnt,
s7.CoachConverted_within_10days_measure as CoachConverted_within_10days_cnt,
s7.CoachConverted_after_17days_measure as Converted_after_17days,

s8.measure as Discharged_cnt, 
s9.review_completed_measure as Review_cnt,
s9.review_completed_with_LTC_measure as Review_with_LTC_cnt,

s10.total_d10 as D10_reached_cnt,
s10.d10_success_measure as D10_success_cnt,
s10.d10_outcome_achieved_measure as d10_outcome_achieved_cnt,
s10.d10_schedule_not_created_yet as D10_schedule_not_created_yet
-- s12.total_active_D11_13_cumulative as D11_13_total_cumulative_active,
-- s13.review_completed_cumulative_measure as Review_completed_cumulative
from 
(
	select date, b.coach
	from v_date a cross join (select distinct coachnameShort as coach from v_client_tmp) b 
	where date >= '2021-03-01' and date <= date_add(date(now()), interval 3 day)
    -- and b.coach is not null
) s1 left join (
					select date, coach, status, count(distinct clientid) as measure
					from (select date from v_date_to_date where date >= date_sub(date(now()), interval 30 day)) a left join 
					(
						select s.clientid, s.enrollmentdateitz, s.conversion_payment_date, s.coach, s.status
						from 
						(
								select distinct b.clientid, a.enrollmentdateitz, conversion_payment_date, coachname_short as coach, if(coachname_short is null, null, 'ENROLLED') as status from 
								bi_patient_status b inner join bi_ttp_enrollments a on b.clientid = a.clientid 
								where b.status = 'active'
								and b.clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
								
								union all 
								
								-- Consider any patient who is in the system with PENDING_ACTIVE status but they have completed their enrollment. 
								select distinct b.clientid, a.enrollmentdateitz, conversion_payment_date, coachname_short as coach, if(coachname_short is null, null, 'ENROLLED') as status from 
								bi_patient_status b inner join bi_ttp_enrollments a on b.clientid = a.clientid 
								where b.status = 'PENDING_ACTIVE'
								and b.clientid not in (select distinct clientid from bi_patient_status where status = 'active')
								and b.clientid not in (select distinct clientid from v_client_Tmp where status = 'registration') -- patients are going back to registration status from pending_active but not getting captured in audit trail 
								and b.clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
						) s inner join clients s1 on s.clientid = s1.id 
						where s1.deleted = 0
					) b on date(enrollmentdateITZ) = a.date
					group by date, coach, b.status
			) s2 on s1.date = s2.date and s1.coach = s2.coach
	left join 
            (
						select date, coach, 
                        count(distinct case when treatmentday in ('D0') then clientid else null end) as D0_count,
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') then clientid else null end) as D9_13_count, 
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D9_13_RC,
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is null and is_d10_review_completed = 'Yes' then clientid else null end) as D9_13_RC_NoSchedule,
						
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is not null then clientid else null end) as D9_13_Scheduled,
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is null and is_d10_review_completed is null then clientid else null end) as D9_13_NoSchedule,
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is null and is_d10_review_completed is null and isDisenrollOpen = 'Yes' then clientid else null end) as D9_13_Declined_Schedule,
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is null and is_d10_review_completed is null and isDisenrollOpen = 'No' then clientid else null end) as D9_13_Pending_Schedule,
						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is null and is_d10_review_completed is null and isDisenrollOpen = 'No' and category = 'Not Converted' then clientid else null end) as D9_13_Pending_Schedule_NotConverted,

						count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is not null and is_d10_review_completed is null then clientid else null end) as D9_13_ScheduleAvailable_yet_to_complete,
                        
                        count(distinct case when treatmentday in ('D10') then clientid else null end) as D10_count, 
						count(distinct case when treatmentday in ('D10') and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D10_RC,
						count(distinct case when treatmentday in ('D10') and scheduled_review_date is null and is_d10_review_completed = 'Yes' then clientid else null end) as D10_RC_NoSchedule,

						count(distinct case when treatmentday in ('D10') and scheduled_review_date is null and is_d10_review_completed is null then clientid else null end) as D10_NoSchedule, 
						count(distinct case when treatmentday in ('D10') and scheduled_review_date is not null and is_d10_review_completed is null then clientid else null end) as D10_ScheduleAvailable_yet_to_complete

						from 
						(
								select a.clientid, 'Not Converted' as category, concat('D',(treatmentdays - 1)) as treatmentday, coachnameShort as coach, a.date, c.d10_review_date as scheduled_review_date, c.is_d10_review_completed, c.is_member_likely_to_convert, c.d10_review_schedule_status, if(inc.clientid is not null, 'Yes', 'No') as isDisenrollOpen
								from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
																			left join v_client_tmp d on a.clientid = d.clientid
                                                                            left join (select distinct clientid, max(date(itz(report_time))) as openDate, max(date(ifnull(itz(resolved_time), itz(now())))) as resolvedDate from bi_incident_mgmt where category = 'Disenrollment' group by clientid) inc on a.clientid = inc.clientid and a.date between opendate and ifnull(resolvedDate,date(now()))
								where treatmentdays in (1,10,11,12,13,14)
								and c.is_patient_converted is null
								and d.patientname not like '%obsolete%'
								and d.status not in ('registration')
								and (d.labels not like '%TWIN_PRIME%' and d.labels not like '%CONTROL%')
								
								union all
								
								select a.clientid, 'Converted' as category, concat('D',(treatmentdays - 1)) as treatmentday, coachnameShort as coach, a.date, c.d10_review_date as scheduled_review_date, c.is_d10_review_completed, is_member_likely_to_convert, c.d10_review_schedule_status, if(inc.clientid is not null, 'Yes', 'No') as isDisenrollOpen
								from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid
																			left join v_client_tmp d on a.clientid = d.clientid
                                                                            left join (select distinct clientid, max(date(itz(report_time))) as openDate, max(date(ifnull(itz(resolved_time), itz(now())))) as resolvedDate from bi_incident_mgmt where category = 'Disenrollment' group by clientid) inc on a.clientid = inc.clientid and a.date between opendate and ifnull(resolvedDate,date(now()))
								where treatmentdays in (1,10,11,12,13,14)
								and c.is_patient_converted = 'Yes'
								and d.patientname not like  '%obsolete%'
								and d.status not in ('registration')
								and (d.labels not like '%TWIN_PRIME%' and d.labels not like '%CONTROL%')
						) s1
						where date >= '2021-03-01'
						group by date, coach
            ) s4 on  s1.date = s4.date and s1.coach = s4.coach
	left join 
            (
					select date, coach, 
                    count(distinct case when d10_review_schedule_status in ('SCHEDULED', 'COMPLETED', 'CANCELLED') then clientid else null end) as scheduled_measure, 
                    count(distinct case when d10_review_schedule_status = 'CANCELLED' then clientid else null end) as cancelled_measure
					from v_date a left join -- Using v_date here because we want future 3 days
					(
						select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, d10_review_schedule_status from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
					) b on a.date = d10_review_schedule_date
					where date >= date_sub(date(now()), interval 30 day) and date <= date_add(date(now()), interval 3 day)
					group by date, coach
            ) s5 on  s1.date = s5.date and s1.coach = s5.coach
    	left join 
            (
				select date, coach, 'CONVERTED' as status, count(distinct b.clientid) as converted_measure, 
                count(distinct case when isCoachConverted = 'Yes' then clientid else null end) as coachConverted_measure, 
                count(distinct case when daysToConvert < 14 then clientid else null end) as Converted_within_14days_measure,
				count(distinct case when daysToConvert between 11 and 14 and isCoachConverted = 'Yes' then clientid else null end) as CoachConverted_within_14days_measure,
                count(distinct case when daysToConvert <= 10 and isCoachConverted = 'Yes' then clientid else null end) as CoachConverted_within_10days_measure,
				count(distinct case when daysToConvert >= 17 and isCoachConverted = 'Yes' then clientid else null end) as CoachConverted_after_17days_measure 
            

				from v_date_to_date a inner join 
				(
					select a.clientid, b.coachnameShort as coach, conversion_payment_date, isCoachConverted, datediff(conversion_payment_date, if(visitDate>active_status_start_date, visitdate, active_status_start_date)) as daysToConvert from 
					bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
										 inner join (select distinct clientid, date(visitdateitz) as visitDate from bi_patient_status where status = 'active') c on a.clientid = c.clientid 
					where is_patient_converted = 'Yes'
					and b.patientname not like '%obsolete%'
					and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
				) b on a.date = conversion_payment_date
				where date >= date_sub(date(now()), interval 30 day) 
				group by date, coach
            ) s7 on  s1.date = s7.date and s1.coach = s7.coach        
    	left join 
            (
					select date, coach, 'DISCHARGED' as status, count(distinct b.clientid) as measure
					from v_date_to_date a inner join 
					(
						select clientid, coachname_short as coach, status_start_date, status_end_date from 
						bi_patient_status b 
						where status = 'discharged' 
						and clientid not in (select clientid from bi_patient_status where status = 'inactive')
						and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
						and clientid in (select clientid from bi_ttp_enrollments where status in ('discharged', 'inactive') and is_patient_converted is null)   
						and not exists (select 1 from bi_patient_status c where b.clientid = c.clientid group by c.clientid having count(case when c.status in ('active', 'pending_active') then c.clientid else null end) = 0 ) -- to ignore patients who are getting discharged directly before reaching Pending active at least
					) b on a.date = b.status_start_date 
					where date >= date_sub(date(now()), interval 10 day) 
					and coach is not null
					group by date, coach
            ) s8 on  s1.date = s8.date and s1.coach = s8.coach
	left join 
            (
					select date, coach, 'REVIEW' as status, count(distinct case when is_d10_review_completed = 'Yes' then clientid else null end) as review_completed_measure, 
                    count(distinct case when is_d10_review_completed = 'Yes' and is_member_likely_to_convert in ('Yes', 'MAYBE') then clientid else null end) as review_completed_with_LTC_measure
					from v_date_to_date a inner join 
					(
						select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, is_d10_review_completed, is_member_likely_to_convert
                        from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
					) b on a.date = d10_review_schedule_date
					where date >= date_sub(date(now()), interval 30 day) 
					group by date, coach
            ) s9 on s1.date = s9.date and s1.coach = s9.coach
	left join 
            (
					select date, coach, 'D10 SUCCESS/OUTCOME' as status, 
					count(distinct clientid) as total_d10,
                    count(distinct case when is_d10_success = 'Yes' then clientid else null end) as d10_success_measure, 
                    count(distinct case when isD10_outcome_achieved = 'Yes' then clientid else null end) as d10_outcome_achieved_measure,
                    count(distinct case when d10_review_schedule_created_date is null then clientid else null end) as d10_schedule_not_created_yet
					from v_date_to_date a inner join 
					(
						select a.clientid, b.coachnameShort as coach, d10_date, is_d10_success, isD10_outcome_achieved, d10_review_schedule_created_date
                        from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
					) b on a.date = d10_date
					where date >= date_sub(date(now()), interval 30 day) 
					group by date, coach
            ) s10 on s1.date = s10.date and s1.coach = s10.coach
	left join 
            (
					select date, coach, 'SCHEDULE CREATED' as status, count(distinct clientid) as schedule_created_measure
					from v_date_to_date a inner join 
					(
						select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, d10_review_schedule_created_date, d10_date
                        from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
					) b on a.date = date(d10_review_schedule_created_date)
					where date >= date_sub(date(now()), interval 30 day) 
					group by date, coach
            ) s11 on  s1.date = s11.date and s1.coach = s11.coach
/*
	left join 
			(
            select a.date, coach, count(distinct clientid) as total_active_D11_13_cumulative
			from v_date_to_date a left join 
			(
						select clientid, coach, min(date) as last_date from 
						(
							select date, clientid, coach, status, is_d10_review_completed, d10_review_date, (datediff(a.date, b.status_start_date)) as treatmentDays
							from v_date_to_date a inner join 
							(
								select a.clientid, status_start_date, is_d10_review_completed, d10_review_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
								from 
								(
										select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
										from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
										where a.status = 'active'
										and b.patientname not like '%obsolete%'
										and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
										and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
										group by clientid
								) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
							) b on a.date between b.status_start_date and b.status_end_date
							 where date(d10_review_date) >= '2021-03-01' 
						) s 
						where treatmentdays in (11,12,13)
						group by clientid, coach
				) s on s.last_date <= a.date
				where a.date >= '2021-03-01'
				group by a.date, coach
			) s12 on s1.date = s12.date and s1.coach = s12.coach

	left join 
            (
					select date, coach, 'REVIEW_Cumulative' as status, 
                    count(distinct case when is_d10_review_completed = 'Yes' then clientid else null end) as review_completed_cumulative_measure 
					from v_date_to_date a inner join 
					(
						select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, is_d10_review_completed, is_member_likely_to_convert, conversion_payment_date
                        from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
                        where date(d10_review_date) >= '2021-03-01'
					) b on d10_review_schedule_date <= a.date 
					where date < conversion_payment_date and date >= date_sub(date(now()), interval 30 day) 
					group by date, coach
            ) s13 on s1.date = s13.date and s1.coach = s13.coach
;

	left join 
            (
					select date, coach, 'ACTIVE D0' as status, count(distinct case when treatmentdays in (0) then clientid else null end) as measure
					from 
					(
							select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
							from v_date_to_date a inner join 
							(
								select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
								from 
								(
										select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
										from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
										where a.status = 'active'
										and b.patientname not like '%obsolete%'
										and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
										and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
										group by clientid
								) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
							) b on a.date between b.status_start_date and b.status_end_date
							where date >= date_sub(date(now()), interval 30 day) 
					) s 
					group by date, coach
		) s3 on  s1.date = s3.date and s1.coach = s3.coach
        
	left join 
            (
					select date, coach, 'ACTIVE D11-12' as status, count(distinct case when treatmentdays in (11,12) then clientid else null end) as active_11_12_cnt,
                    count(distinct case when treatmentdays in (11,12,13,14) then clientid else null end) as active_11_14_count,
					count(distinct case when treatmentdays in (9,10,11,12,13,14) then clientid else null end) as active_9_14_count

					from 
					(
							select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
							from v_date_to_date a inner join 
							(
								select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
								from 
								(
										select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
										from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
										where a.status = 'active'
										and b.patientname not like '%obsolete%'
										and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
										and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
										group by clientid
								) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
							) b on a.date between b.status_start_date and b.status_end_date
							where date >= date_sub(date(now()), interval 30 day) 
					) s 
					group by date, coach
            ) s6 on  s1.date = s6.date and s1.coach = s6.coach
*/
/* OLD Definition 
select s1.date, s1.coach, 
max(case when s1.status = 'ACTIVE' then measure end) as Active_cnt, 
max(case when s1.status = 'ACTIVE D9-10' then measure end) as Active_D9_10_cnt, 
max(case when s1.status = 'ACTIVE D11-12' then measure end) as Active_D11_12_cnt, 
max(case when s1.status = 'ACTIVE D13' then measure end) as Active_D13_cnt,
max(case when s1.status = 'ACTIVE D16' then measure end) as Active_D16_cnt,
max(case when s1.status = 'DISCHARGED D16' then measure end) as Discharged_D16_cnt,
max(case when s1.status = 'CONVERTED' then measure end) as Converted_cnt,
max(case when s1.status = 'DISCHARGED BACKLOG' then measure end) as Discharged_backlog_cnt
from 
(
	select date, b.coach, c.status
	from v_date_to_date a cross join (select distinct coachnameShort as coach from v_client_tmp) b 
						  cross join (
										  select 'ACTIVE' as status 
										  union all select 'ACTIVE D9-10' as status 
										  union all select 'ACTIVE D11-12' as status 
										  union all select 'ACTIVE D13' as status 
										  union all select 'ACTIVE D16' as status
										  union all select 'DISCHARGED D16' as status
										  union all select 'CONVERTED' as status
										  union all select 'DISCHARGED BACKLOG' as status
                                      ) c
	where date >= date_sub(date(now()), interval 10 day) 
) s1 left join 
(   
	select date, coach, status, count(distinct clientid) as measure
	from 
    (
			select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
			from v_date_to_date a inner join 
			(
				select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
				from 
				(
						select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
						from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
						where a.status = 'active'
                        and b.patientname not like '%obsolete%'
						and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
						and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
                        group by clientid
				) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
			) b on a.date between b.status_start_date and b.status_end_date
			where date >= date_sub(date(now()), interval 10 day) 
    ) s 
    group by date, coach, status

	union all 
    
	select date, coach, 'ACTIVE D9-10' as status, count(distinct case when treatmentdays in (9,10) then clientid else null end) as measure
	from 
    (
			select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
			from v_date_to_date a inner join 
			(
				select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
				from 
				(
						select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
						from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
						where a.status = 'active'
                        and b.patientname not like '%obsolete%'
						and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
						and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
                        group by clientid
				) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
			) b on a.date between b.status_start_date and b.status_end_date
			where date >= date_sub(date(now()), interval 10 day) 
    ) s 
    group by date, coach
	
    union all 
    
	select date, coach, 'ACTIVE D11-12' as status, count(distinct case when treatmentdays in (11,12) then clientid else null end) as measure
	from 
    (
			select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
			from v_date_to_date a inner join 
			(
				select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
				from 
				(
						select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
						from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
						where a.status = 'active'
                        and b.patientname not like '%obsolete%'
						and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
						and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
                        group by clientid
				) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
			) b on a.date between b.status_start_date and b.status_end_date
			where date >= date_sub(date(now()), interval 10 day) 
    ) s 
    group by date, coach

    union all 

	select date, coach, 'ACTIVE D13' as status, count(distinct case when treatmentdays in (13) then clientid else null end) as measure
	from 
    (
			select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
			from v_date_to_date a inner join 
			(
				select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
				from 
				(
						select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
						from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
						where a.status = 'active'
                        and b.patientname not like '%obsolete%'
						and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
						and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
                        group by clientid
				) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
			) b on a.date between b.status_start_date and b.status_end_date
			where date >= date_sub(date(now()), interval 10 day) 
    ) s 
    group by date, coach

	union all 
    
	select date, coach, 'ACTIVE D16' as status, count(distinct case when treatmentdays in (16) then clientid else null end) as measure
	from 
    (
			select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
			from v_date_to_date a inner join 
			(
				select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
				from 
				(
						select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
						from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
						where a.status = 'active'
                        and b.patientname not like '%obsolete%'
						and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
						and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
                        group by clientid
				) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
			) b on a.date between b.status_start_date and b.status_end_date
			where date >= date_sub(date(now()), interval 10 day) 
    ) s 
    group by date, coach

    union all 
    
	select date, s.coach, 'DISCHARGED D16' as status, count(distinct case when treatmentdays in (16) and s1.clientid is not null then s.clientid else null end) as measure
	from 
    (
			select date, clientid, coach, status, (datediff(a.date, b.status_start_date)) as treatmentDays
			from v_date_to_date a inner join 
			(
				select a.clientid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, coach, a.status 
				from 
				(
						select distinct a.clientid, min(if(date(visitDateITZ) > status_start_date or status_start_date is null, date(visitDateITZ), status_start_date)) as status_start_date, max(status_end_date) as status_end_date, coachName_short as coach, if(coachName_short is null, null, 'ACTIVE') as status 
						from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid
						where a.status = 'active'
                        and b.patientname not like '%obsolete%'
						and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
						and a.clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
                        group by clientid
				) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
			) b on a.date between b.status_start_date and b.status_end_date
			where date >= date_sub(date(now()), interval 10 day) 
    ) s left join 
         (
				select clientid, status_start_date, status_end_date from 
				bi_patient_status b 
				where status = 'discharged' 
				and clientid not in (select clientid from bi_patient_status where status = 'inactive')
				and not exists (select 1 from bi_patient_status c where b.clientid = c.clientid group by c.clientid having count(case when c.status in ('active', 'pending_active') then c.clientid else null end) = 0 ) -- to ignore patients who are getting discharged directly before reaching Pending active at least
				and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
				and clientid in (select clientid from bi_ttp_enrollments where status in ('discharged', 'inactive') and is_patient_converted is null)
         ) s1 on s.clientid = s1.clientid and s.date = s1.status_start_date
    group by date, coach

    union all 

    select date, coach, 'CONVERTED' as status, count(distinct b.clientid) as measure
	from v_date_to_date a inner join 
	(
		select a.clientid, b.coachnameShort as coach, conversion_payment_date from 
		bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
        where is_patient_converted = 'Yes'
        and b.patientname not like '%obsolete%'
		and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
    ) b on a.date = conversion_payment_date
	where date >= date_sub(date(now()), interval 10 day) 
	group by date, coach

	union all 
    
	select date, coach, status, count(distinct clientid) as measure
	from v_date_to_date a inner join 
	(
		select clientid, status_start_date, status_end_date, coachName_short as coach, if(coachName_short is null, null, 'DISCHARGED BACKLOG') as status from 
		bi_patient_status b 
		where status = 'discharged' 
		and clientid not in (select clientid from bi_patient_status where status = 'inactive')
        and not exists (select 1 from bi_patient_status c where b.clientid = c.clientid group by c.clientid having count(case when c.status in ('active', 'pending_active') then c.clientid else null end) = 0 ) -- to ignore patients who are getting discharged directly before reaching Pending active at least
		and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
        and clientid in (select clientid from bi_ttp_enrollments where status in ('discharged', 'inactive') and is_patient_converted is null)
    ) b on a.date between status_start_date and status_end_date
	where date >= date_sub(date(now()), interval 10 day) 
    group by date, coach, b.status
) s2 on s1.date = s2.date and s1.coach = s2.coach and s1.status = s2.status
where s1.coach is not null
group by s1.date, s1.coach
;
*/