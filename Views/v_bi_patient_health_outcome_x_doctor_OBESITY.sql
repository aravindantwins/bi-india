create view v_bi_patient_health_outcome_x_doctor_OBESITY as 
select s1.clientid, doctorId, s1.doctorname, s1.treatmentdays,
s1.start_BMI, s1.last_available_BMI, s1.start_weight, s1.last_available_weight, 
s2.first_available_visceralFat, s2.last_available_visceralFat
from 
(
	select a.clientid, a.doctorname, b.doctorId, treatmentdays,
	a.start_BMI, a.start_weight, a.last_available_weight, cast(a.last_available_weight/(a.height*a.height) as decimal(6,2)) as last_available_BMI
	from dim_client a left join v_AllClient_list b on a.clientid = b.clientid 
	where is_row_current = 'y' and a.status = 'active' 
	-- and treatmentdays >= 30
    and a.isRCT is null
) s1 left join 
-- VisceralFat finder
(
	select s.clientid, 
    cast(avg(case when s.first_available_vFat_date = s1.eventtime then s1.weight else null end) as decimal(6,2)) as first_available_visceralFat,
    cast(avg(case when s.last_available_vFat_date = s1.eventtime then s1.weight else null end) as decimal(6,2)) as last_available_visceralFat
    from 
    (
		select clientid, min(case when visceralFat is not null then eventtime else null end) as first_available_vFat_date, 
        max(case when visceralFat is not null then eventtime else null end) as last_available_vFat_date 
		from weights 
		group by clientid 
    ) s inner join weights s1 on s.clientid = s1.clientid and (s.first_available_vFat_date = s1.eventtime or s.last_available_vFat_date = s1.eventtime)
    group by s.clientid
) s2 on s1.clientid = s2.clientid 
; 


