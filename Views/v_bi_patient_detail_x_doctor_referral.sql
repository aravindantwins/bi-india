    create view v_bi_patient_detail_x_doctor_referral as 
    select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'pending_active') as status from 
	bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
	where b.status = 'pending_active'
    and c.source = 'doctor'
    
    union all 
    
	select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status from 
	bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
	where b.status = 'discharged'
	and c.source = 'doctor'
    and b.clientid not in (select clientid from bi_patient_Status where status = 'active' and status_end_date = date(itz(now())))
    and b.clientid not in (select clientid from bi_patient_Status where status = 'inactive')

    union all 
    
	select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status from 
	bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
	where b.status = 'inactive'
    and c.source = 'doctor'
    
    union all 
    
	select distinct b.clientid, c.patientname, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'active') as status from 
	bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
	where b.status = 'active'
    and c.source = 'doctor'