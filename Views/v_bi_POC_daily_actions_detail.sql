alter view v_bi_POC_daily_actions_detail as 
select poc, 
group_concat(case when is_BD_Pending = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as BD_Pending,
	count(distinct case when is_BD_Pending = 'Yes' then clientid else null end) as total_BD_Pending,
group_concat(case when is_BD_Report_Pending = 'Yes' then concat(clientid,'- ', patient, ' - ', BD_Report_Pending_Days,'- ',Pending_active_days) else null end order by clientid separator '\n') as BD_Report_Pending,
	count(distinct case when is_BD_Report_Pending = 'Yes' then clientid else null end) as total_BD_Report_Pending,
group_concat(case when is_VC_not_completed = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as VC_NotCompleted,
	count(distinct case when is_VC_not_completed = 'Yes' then clientid else null end) as total_VC_NotCompleted,
group_concat(case when is_DC_Pending = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as DC_Pending,
	count(distinct case when is_DC_Pending = 'Yes' then clientid else null end) as total_DC_Pending,
group_concat(case when is_SA_not_scheduled = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as SA_NotScheduled,
	count(distinct case when is_SA_not_scheduled = 'Yes' then clientid else null end) as total_SA_not_scheduled,
group_concat(case when is_SA_scheduled = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as SA_Scheduled,
	count(distinct case when is_SA_scheduled = 'Yes' then clientid else null end) as total_SA_scheduled,
group_concat(case when is_Fitbit_Sync_Available = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as FirstSync_Available,
	count(distinct case when is_Fitbit_Sync_Available = 'Yes' then clientid else null end) as total_FirstSync_Available,
group_concat(case when is_Coach_not_assigned = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as CoachNotAssigned,
	count(distinct case when is_Coach_not_assigned = 'Yes' then clientid else null end) as total_CoachNotAssigned,
group_concat(case when is_Deffered = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as Deferred,
	count(distinct case when is_Deffered = 'Yes' then clientid else null end) as total_Deffered,
group_concat(case when is_Medical_Exclusion = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as MedicalExclusion,
	count(distinct case when is_Medical_Exclusion = 'Yes' then clientid else null end) as total_MedicalExclusion,
group_concat(case when is_DR = 'Yes' then concat(clientid,'- ', patient,'- ',Pending_active_days) else null end order by clientid separator '\n') as DisenrollmentOpen,
	count(distinct case when is_DR = 'Yes' then clientid else null end) as total_DisenrollmentOpen
from 
(
	select distinct 
	b.clientid, poc, DATEDIFF(b.status_end_date, b.status_start_date)+1 AS Pending_active_days, patientnameShort as patient,
	case when (d.bloodWork_scheduled_status is null or d.bloodWork_scheduled_status not in ('SCHEDULED','COMPLETED')) and d.first_Investigation_upload_date is null and def.clientid is null then 'Yes' else null end as is_BD_Pending, 
	case when d.bloodWork_scheduled_status in ('SCHEDULED','COMPLETED') and d.first_Investigation_upload_date is null and def.clientid is null then 'Yes' else null end as is_BD_Report_Pending,
	case when d.bloodWork_scheduled_status in ('SCHEDULED','COMPLETED') and d.first_Investigation_upload_date is null and def.clientid is null then datediff(date(itz(now())), date(bloodWork_scheduled_date)) else null end as BD_Report_Pending_Days,
	case when d.vitals_conds_allergies_meds_addInfor_captured is null and def.clientid is null then 'Yes' else null end as is_VC_not_completed,
	case when d.first_Investigation_upload_date is not null and d.first_medicine_published_date is null and def.clientid is null then 'Yes' else null end as is_DC_Pending,
    case when d.first_medicine_published_date is not null and d.sensorActivation_schedule_date is null and def.clientid is null then 'Yes' else null end as is_SA_not_scheduled,
	case when d.sensorActivation_schedule_date is not null and sensorActivation_schedule_status in ('SCHEDULED', 'COMPLETED')  and first_fitbit_sync_Date is null and def.clientid is null then 'Yes' else null end as is_SA_scheduled,
	case when first_fitbit_sync_Date is null and def.clientid is null then 'Yes'  else null end as is_Fitbit_Sync_Available,
	case when d.homeVisit_scheduled_date is not null and homeVisit_scheduled_status in ('SCHEDULED', 'COMPLETED') and (c.coachname is null or c.coachname = 'MS COACH') and def.clientid is null then 'Yes' else null end as is_Coach_not_assigned,
    case when def.clientid is not null then 'Yes' else null end as is_Deffered,
    case when dc.clientid is not null and def.clientid is null then 'Yes' else null end as is_Medical_Exclusion,
    case when dr.clientid is not null and def.clientid is null then 'Yes' else null end as is_DR
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
						  inner join (select clientid, coachname, patientnameShort, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
						  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                          left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                     ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
						  left join dim_client dc on c.clientid = dc.clientid and dc.is_row_current = 'Y' and dc.is_medical_exclusion = 'Yes'
						  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Disenrollment'
                                     ) dr on b.clientid = dr.clientid and a.date between dr.openDate and dr.resolvedDate
	where date = if(hour(date(itz(now()))) < 4, date_sub(date(itz(now())), interval 1 day), date(itz(now())))
    -- where date = '2021-03-09'
) s
group by poc
;

 