-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_doctor_payout_medPublish_detail as 
select f1.clientid, ct.patientname, f1.doctorId, ct.doctorname, recommendationDate, recommendationdatetime, f1.status, actualmedication, recommendedmedication,
case when f1.recommendationdate <> f2.newpublishingdate then 'Repeated' 
	 when f1.recommendationdate = f2.newpublishingdate then 'New' else null end as publishStatus, actionTime, actionedDate, appointmentTime, newActionTime,
	 case when f1.status in ('MANUAL','OVERRIDDEN') and f1.actualmedication ='No medicine' and f1.recommendedmedication='No medicine' then 'Yes' else null end as changeType_Psuedo
from
(
		select distinct a.clientid, a.doctorId, recommendationdate as recommendationdatetime, actionedDate, 
		date(recommendationdate) as recommendationdate,
		a.status, actionTime, appointmentTime, newActionTime, actualmedication, recommendedmedication  from 
		(
			select a.clientid, a.doctorId, a.medicationTierId, a.eventtime as recommendationdate, a.status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime, itz(da.eventtime) as appointmentTime,
            timestampdiff(HOUR, itz(da.eventtime), itz(b.dateAdded)) as newActionTime, b.actualMedication as actualmedication,b.recommendedMedication as recommendedmedication
			from
			(
					select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId, type, b.doctorId
					from medicationrecommendations a inner join v_client_tmp b on a.clientid = b.clientid 
													 left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'
					where type = 'DIABETES'
			) a inner join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
				left join clientappointments da on a.clientid = da.clientid and da.type = 'DOCTOR_APPOINTMENT' and da.status in ('completed') and date(itz(b.dateAdded)) = date(itz(da.eventtime))
		) a 
		where date(recommendationdate) >= date_sub(date(itz(now())), interval 2 month)
) f1 left join (select clientid, min(date(itz(eventtime))) as newpublishingdate from medicationrecommendations where type = 'DIABETES' group by clientid) f2 on f1.clientid = f2.clientid  -- where medicationTierId is not null 
	 inner join v_client_tmp ct on f1.clientid = ct.clientid
;
