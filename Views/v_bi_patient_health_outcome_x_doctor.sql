alter view v_bi_patient_health_outcome_x_doctor as 
select s1.clientid, doctorId, s1.doctorname, s1.treatmentdays,
s1.start_medCount, length(s3.medicine_drugs) - length(replace(s3.medicine_drugs,',','')) + 1 as current_medCount,
s1.start_symCount, length(s3.MH_Symptoms) - length(replace(s3.MH_Symptoms,',','')) + 1 as current_symCount, 
s1.start_insulin_units, s1.current_insulin_units, s1.start_glucose, s2.last_available_cgm_5d,
s4.last_available_energy_1d,
s3.is_InReversal,
s3.is_MetOnly_by_OPS
from 
(
	select a.clientid, a.doctorname, b.doctorId,treatmentdays,
	length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medCount, 
	length(start_symptoms) - length(replace(start_symptoms,',','')) + 1 as start_symCount,
	start_insulin_units,
	start_labA1c,
    current_insulin_units, 
    current_medicine_drugs,
 	length(current_medicine_drugs) - length(replace(current_medicine_drugs,',','')) + 1 as current_medCount, 
	cast((start_labA1C * 28.1) - 46.7 as decimal(10,2)) start_glucose
	from dim_client a left join v_AllClient_list b on a.clientid = b.clientid 
	where is_row_current = 'y' and a.status = 'active' 
	-- and treatmentdays >= 30
    and a.isRCT is null
) s1 left join 
( -- last available cgm_5d finder
		select a.clientid, b.date, b.cgm_5d as last_available_cgm_5d
		from 
		(
			select clientid, 
			max(case when cgm_5d is not null then date else null end) as last_cgm_5d_date 
			from bi_patient_reversal_state_gmi_x_date 
			where cgm_5d is not null 
			group by clientid 
		) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_cgm_5d_date = b.date 
) s2 on s1.clientid = s2.clientid 
left join bi_patient_reversal_state_gmi_x_date s3 on s1.clientid = s3.clientid and s3.date = if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))
left join ( -- last available energy_1d finder
			select a.clientid, b.date, b.energy_1d as last_available_energy_1d
			from 
			(
				select clientid, 
				max(case when energy_1d is not null then date else null end) as last_energy_1d_date 
				from bi_patient_reversal_state_gmi_x_date 
				where energy_1d is not null 
				group by clientid 
			) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_energy_1d_date = b.date 
) s4 on s1.clientid = s4.clientid 
;

