set sql_mode = 'NO_ENGINE_SUBSTITUTION'; 
set collation_connection = 'latin1_swedish_ci';

create view v_foodlog_x_food_detail as

select fl.clientid, concat(d.firstName,' ',d.lastName) as patientname, eventdateITZ as mealdate, c.tac_score, 
case when categorytype = 'Breakfast' then 'B'
	 when categoryType = 'Lunch' then 'L'
     when categoryType like '%Snack%' then 'S'
     when categoryType = 'Dinner' then 'D' 
     else categoryType end as mealtype,
time(eventtimeITZ) as mealTime,
eventTimeITZ as mealtimeITZ,
b.foodid,
b.foodlabel,
b.E5foodgrading,
fl.measurementType as measure,
fl.measurementType as measurementType,
fl.quantity as measurementAmount,
(b.net_carb  * fl.measurementAmount) as net_carb,
(b.calories * fl.measurementAmount) as calories,
(b.netGlycemicCarb * fl.measurementAmount) as netGlycemicCarb,
(b.fibre * fl.measurementAmount) as fibre,
(b.fat * fl.measurementAmount) as fat,
(b.protein * fl.measurementAmount) as protein,
(b.Total_Carb * fl.measurementAmount) as Total_Carb,
(sodium * fl.measurementAmount) as sodium,
(potassium * fl.measurementAmount) as potassium,
(magnesium * fl.measurementAmount) as magnesium,
(calcium * fl.measurementAmount) as calcium,
(chromium * fl.measurementAmount) as chromium,
(omega3 * fl.measurementAmount) as omega3,
(omega6 * fl.measurementAmount) as omega6,
(alphaLipoicAcid * fl.measurementAmount) as alphaLipoicAcid,
(q10 * fl.measurementAmount) as q10,
(biotin * fl.measurementAmount) as biotin,
(flavonoids * fl.measurementAmount) as flavonoids,
(improveInsulinSensitivity * fl.measurementAmount) as improveInsulinSensitivity,
(inhibitGluconeogenis * fl.measurementAmount) as inhibitGluconeogenis,
(inhibitCarbAbsorption * fl.measurementAmount) as inhibitCarbAbsorption,
(improveInsulinSecretion * fl.measurementAmount) as improveInsulinSecretion,
(improveBetaCellRegeneration * fl.measurementAmount) as improveBetaCellRegeneration,
(inhibitHunger * fl.measurementAmount) as inhibitHunger,
(inhibitGlucoseKidneyReabsorption * fl.measurementAmount) as inhibitGlucoseKidneyReabsorption,
(lactococcus * fl.measurementAmount) as lactococcus,
(lactobacillus * fl.measurementAmount) as lactobacillus,
(leuconostoc * fl.measurementAmount) as leuconostoc,
(streptococcus * fl.measurementAmount) as streptococcus,
(bifidobacterium * fl.measurementAmount) as bifidobacterium,
(saccharomyces * fl.measurementAmount) as saccharomyces,
(bacillus * fl.measurementAmount) as bacillus,
(fructose * fl.measurementAmount) as fructose,
(lactose * fl.measurementAmount) as lactose,
(glycemicIndex * fl.measurementAmount) as glycemicIndex,
(saturatedFat * fl.measurementAmount) as saturatedFat,
(monounsaturatedFat * fl.measurementAmount) as monounsaturatedFat,
(polyunsaturatedFat * fl.measurementAmount) as polyunsaturatedFat,
(transFat * fl.measurementAmount) as transFat,
(cholesterol * fl.measurementAmount) as cholesterol,
(histidine * fl.measurementAmount) as histidine,
(isolecuine * fl.measurementAmount) as isolecuine,
(lysine * fl.measurementAmount) as lysine,
(methionineAndCysteine * fl.measurementAmount) as methionineAndCysteine,
(phenylananineAndTyrosine * fl.measurementAmount) as phenylananineAndTyrosine,
(tryptophan * fl.measurementAmount) as tryptophan,
(threonine * fl.measurementAmount) as threonine,
(valine * fl.measurementAmount) as valine,
(vitaminA * fl.measurementAmount) as vitaminA,
(vitaminC * fl.measurementAmount) as vitaminC,
(vitaminD * fl.measurementAmount) as vitaminD,
(vitaminE * fl.measurementAmount) as vitaminE,
(vitaminK * fl.measurementAmount) as vitaminK,
(vitaminB1 * fl.measurementAmount) as vitaminB1,
(vitaminB12 * fl.measurementAmount) as vitaminB12,
(vitaminB2 * fl.measurementAmount) as vitaminB2,
(vitaminB3 * fl.measurementAmount) as vitaminB3,
(vitaminB5 * fl.measurementAmount) as vitaminB5,
(vitaminB6 * fl.measurementAmount) as vitaminB6,
(folate * fl.measurementAmount) as folate,
(copper * fl.measurementAmount) as copper,
(iron * fl.measurementAmount) as iron,
(zinc * fl.measurementAmount) as zinc,
(manganese * fl.measurementAmount) as manganese,
(phosphorus * fl.measurementAmount) as phosphorus,
(selenium * fl.measurementAmount) as selenium,
(omega6_3_ratio * fl.measurementAmount) as omega6_3_ratio,
(zinc_copper_ratio * fl.measurementAmount) as zinc_copper_ratio,
(pot_sod_ratio * fl.measurementAmount) as pot_sod_ratio,
(cal_mag_ratio * fl.measurementAmount) as cal_mag_ratio,
(pralAlkalinity * fl.measurementAmount) as pralAlkalinity,
(improveBloodPressure * fl.measurementAmount) as improveBloodPressure,
(improveCholesterol * fl.measurementAmount) as improveCholesterol,
(reduceWeight * fl.measurementAmount) as reduceWeight,
(improveRenalFunction * fl.measurementAmount) as improveRenalFunction,
(improveLiverFunction * fl.measurementAmount) as improveLiverFunction,
(improveThyroidFunction * fl.measurementAmount) as improveThyroidFunction,
(improveArthritis * fl.measurementAmount) as improveArthritis,
(reduceUricAcid * fl.measurementAmount) as reduceUricAcid,
(veg * fl.measurementAmount) as veg,
(nonVeg * fl.measurementAmount) as nonVeg,
(fruit * fl.measurementAmount) as fruit,
(oil * fl.measurementAmount) as oil,
(spice * fl.measurementAmount) as spice,
(grain * fl.measurementAmount) as grain,
(legume * fl.measurementAmount) as legume,
(nuts * fl.measurementAmount) as nuts,
(seeds * fl.measurementAmount) as seeds,
(inflammatoryIndex * fl.measurementAmount) as inflammatoryIndex,
(oxidativeStressIndex * fl.measurementAmount) as oxidativeStressIndex,
(gluten * fl.measurementAmount) as gluten,
-- (lactose) as lactose,
(allergicIndex * fl.measurementAmount) as allergicIndex,
(water * fl.measurementAmount) as water
from bi_food_supplement_log_detail fl  left join (
									select
									foodid, foodlabel,
									CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
										 WHEN (recommendationRating <= 1) THEN 'Red'
										 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
										 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
										 WHEN (recommendationRating > 3) THEN 'Green*'
									END AS E5foodgrading,
									measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb -- level1
									,calories, IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre -- level 2
									,sodium, potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids -- level 3
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  -- level 4 glycemic controllers
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus -- level 5 biota
									,fructose, lactose, glycemicIndex  -- level 6 carb
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol -- ,  medium-chain  -- level 6 fat
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine -- level 6 protein
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  -- level 6 vitamins
									-- , taurine, vanadium -- level 6 nutraceuticals  -- carnitine, inositol,
									,  copper, iron, zinc, manganese, phosphorus, selenium  -- level 6 minerals-- iodine,

									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  -- level 7 nutrient balance

									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  -- level 8 metabolic improvers

									, vegetarian as veg, case when vegetarian = 0 then 1 end as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  -- level 9 food type -- nonVeg,

									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  -- level 10 cellular stressers

									, water -- level 11 hydration

									from foods a
						) b on fl.itemId = b.foodid
                        left join bi_tac_measure c on fl.clientid = c.clientid and fl.eventdateitz = c.measure_date
                        left join twinspii.clientspii d on fl.clientid = d.id 
where eventdateitz between date_sub(date(itz(now())), interval 7 day) and date(itz(now()))
and category = 'foodlog'

union all 

-- Make the 1 unit calculation for supplements by dividing the measures (like netcarb, protein, etc) by measureamount and then multiply by quantity
select  fl.clientid, concat(d.firstName,' ',d.lastName) as patientname, eventdateITZ as mealdate, null as tac_score, 
'M' as mealtype,
time(eventtimeITZ) as mealTime,
eventTimeITZ as mealtimeITZ,
b.supplementId,
b.supplementName,
b.E5foodgrading,
b.measure,
b.measurementType,
b.measurementAmount,
((b.net_carb/b.measurementAmount) * fl.quantity) as net_carb,
((b.calories/b.measurementAmount) * fl.quantity) as calories,
((b.netGlycemicCarb/b.measurementAmount) * fl.quantity) as netGlycemicCarb,
((b.fibre/b.measurementAmount) * fl.quantity) as fibre,
((b.fat/b.measurementAmount) * fl.quantity) as fat,
((b.protein/b.measurementAmount) * fl.quantity) as protein,
((b.Total_Carb/b.measurementAmount) * fl.quantity) as Total_Carb,
((sodium/b.measurementAmount) * fl.quantity) as sodium,
((potassium/b.measurementAmount) * fl.quantity) as potassium,
((magnesium/b.measurementAmount) * fl.quantity) as magnesium,
((calcium/b.measurementAmount) * fl.quantity) as calcium,
((chromium/b.measurementAmount) * fl.quantity) as chromium,
((omega3/b.measurementAmount) * fl.quantity) as omega3,
((omega6/b.measurementAmount) * fl.quantity) as omega6,
((alphaLipoicAcid/b.measurementAmount) * fl.quantity) as alphaLipoicAcid,
((q10/b.measurementAmount) * fl.quantity) as q10,
((biotin/b.measurementAmount) * fl.quantity) as biotin,
((flavonoids/b.measurementAmount) * fl.quantity) as flavonoids,
((improveInsulinSensitivity/b.measurementAmount) * fl.quantity) as improveInsulinSensitivity,
((inhibitGluconeogenis/b.measurementAmount) * fl.quantity) as inhibitGluconeogenis,
((inhibitCarbAbsorption/b.measurementAmount) * fl.quantity) as inhibitCarbAbsorption,
((improveInsulinSecretion/b.measurementAmount) * fl.quantity) as improveInsulinSecretion,
((improveBetaCellRegeneration/b.measurementAmount) * fl.quantity) as improveBetaCellRegeneration,
((inhibitHunger/b.measurementAmount) * fl.quantity) as inhibitHunger,
((inhibitGlucoseKidneyReabsorption/b.measurementAmount) * fl.quantity) as inhibitGlucoseKidneyReabsorption,
((lactococcus/b.measurementAmount) * fl.quantity) as lactococcus,
((lactobacillus/b.measurementAmount) * fl.quantity) as lactobacillus,
((leuconostoc/b.measurementAmount) * fl.quantity) as leuconostoc,
((streptococcus/b.measurementAmount) * fl.quantity) as streptococcus,
((bifidobacterium/b.measurementAmount) * fl.quantity) as bifidobacterium,
((saccharomyces/b.measurementAmount) * fl.quantity) as saccharomyces,
((bacillus/b.measurementAmount) * fl.quantity) as bacillus,
((fructose/b.measurementAmount) * fl.quantity) as fructose,
((lactose/b.measurementAmount) * fl.quantity) as lactose,
((glycemicIndex/b.measurementAmount) * fl.quantity) as glycemicIndex,
((saturatedFat/b.measurementAmount) * fl.quantity) as saturatedFat,
((monounsaturatedFat/b.measurementAmount) * fl.quantity) as monounsaturatedFat,
((polyunsaturatedFat/b.measurementAmount) * fl.quantity) as polyunsaturatedFat,
((transFat/b.measurementAmount) * fl.quantity) as transFat,
((cholesterol/b.measurementAmount) * fl.quantity) as cholesterol,
((histidine/b.measurementAmount) * fl.quantity) as histidine,
((isolecuine/b.measurementAmount) * fl.quantity) as isolecuine,
((lysine/b.measurementAmount) * fl.quantity) as lysine,
((methionineAndCysteine/b.measurementAmount) * fl.quantity) as methionineAndCysteine,
((phenylananineAndTyrosine/b.measurementAmount) * fl.quantity) as phenylananineAndTyrosine,
((tryptophan/b.measurementAmount) * fl.quantity) as tryptophan,
((threonine/b.measurementAmount) * fl.quantity) as threonine,
((valine/b.measurementAmount) * fl.quantity) as valine,
((vitaminA/b.measurementAmount) * fl.quantity) as vitaminA,
((vitaminC/b.measurementAmount) * fl.quantity) as vitaminC,
((vitaminD/b.measurementAmount) * fl.quantity) as vitaminD,
((vitaminE/b.measurementAmount) * fl.quantity) as vitaminE,
((vitaminK/b.measurementAmount) * fl.quantity) as vitaminK,
((vitaminB1/b.measurementAmount) * fl.quantity) as vitaminB1,
((vitaminB12/b.measurementAmount) * fl.quantity) as vitaminB12,
((vitaminB2/b.measurementAmount) * fl.quantity) as vitaminB2,
((vitaminB3/b.measurementAmount) * fl.quantity) as vitaminB3,
((vitaminB5/b.measurementAmount) * fl.quantity) as vitaminB5,
((vitaminB6/b.measurementAmount) * fl.quantity) as vitaminB6,
((folate/b.measurementAmount) * fl.quantity) as folate,
((copper/b.measurementAmount) * fl.quantity) as copper,
((iron/b.measurementAmount) * fl.quantity) as iron,
((zinc/b.measurementAmount) * fl.quantity) as zinc,
((manganese/b.measurementAmount) * fl.quantity) as manganese,
((phosphorus/b.measurementAmount) * fl.quantity) as phosphorus,
((selenium/b.measurementAmount) * fl.quantity) as selenium,
(((omega6/b.measurementAmount) / (omega3/b.measurementAmount)) * fl.quantity) as omega6_3_ratio,
(((zinc/b.measurementAmount) / (copper/b.measurementAmount)) * fl.quantity) as zinc_copper_ratio,
-- (zinc_copper_ratio * fl.quantity) as zinc_copper_ratio,
(((potassium/b.measurementAmount) / (sodium/b.measurementAmount)) * fl.quantity) as pot_sod_ratio,
-- (pot_sod_ratio * fl.quantity) as pot_sod_ratio,
(((calcium/b.measurementAmount) / (magnesium/b.measurementAmount)) * fl.quantity) as cal_mag_ratio,
-- (cal_mag_ratio * fl.quantity) as cal_mag_ratio,
((pralAlkalinity/b.measurementAmount) * fl.quantity) as pralAlkalinity,
((improveBloodPressure/b.measurementAmount) * fl.quantity) as improveBloodPressure,
((improveCholesterol/b.measurementAmount) * fl.quantity) as improveCholesterol,
((reduceWeight/b.measurementAmount) * fl.quantity) as reduceWeight,
((improveRenalFunction/b.measurementAmount) * fl.quantity) as improveRenalFunction,
((improveLiverFunction/b.measurementAmount) * fl.quantity) as improveLiverFunction,
((improveThyroidFunction/b.measurementAmount) * fl.quantity) as improveThyroidFunction,
((improveArthritis/b.measurementAmount) * fl.quantity) as improveArthritis,
((reduceUricAcid/b.measurementAmount) * fl.quantity) as reduceUricAcid,
((veg/b.measurementAmount) * fl.quantity) as veg,
((nonVeg/b.measurementAmount) * fl.quantity) as nonVeg,
((fruit/b.measurementAmount) * fl.quantity) as fruit,
((oil/b.measurementAmount) * fl.quantity) as oil,
((spice/b.measurementAmount) * fl.quantity) as spice,
((grain/b.measurementAmount) * fl.quantity) as grain,
((legume/b.measurementAmount) * fl.quantity) as legume,
((nuts/b.measurementAmount) * fl.quantity) as nuts,
((seeds/b.measurementAmount) * fl.quantity) as seeds,
((inflammatoryIndex/b.measurementAmount) * fl.quantity) as inflammatoryIndex,
((oxidativeStressIndex/b.measurementAmount) * fl.quantity) as oxidativeStressIndex,
((gluten/b.measurementAmount) * fl.quantity) as gluten,
-- (lactose) as lactose,
((allergicIndex/b.measurementAmount) * fl.quantity) as allergicIndex,
((water/b.measurementAmount) * fl.quantity) as water
from bi_food_supplement_log_detail fl  left join (
									select
									supplementId, supplementName,
									null AS E5foodgrading,
									null as measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb -- level1
									,calories, IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre -- level 2
									,sodium, potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids -- level 3
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  -- level 4 glycemic controllers
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus -- level 5 biota
									,fructose, lactose, glycemicIndex  -- level 6 carb
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol -- ,  medium-chain  -- level 6 fat
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine -- level 6 protein
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  -- level 6 vitamins
									-- , taurine, vanadium -- level 6 nutraceuticals  -- carnitine, inositol,
									,  copper, iron, zinc, manganese, phosphorus, selenium  -- level 6 minerals-- iodine,

									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  -- level 7 nutrient balance

									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  -- level 8 metabolic improvers

									, '' as veg, '' as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  -- level 9 food type -- nonVeg,

									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  -- level 10 cellular stressers

									, water -- level 11 hydration

									from supplements a
						) b on fl.itemId = b.supplementid
                        left join twinspii.clientspii d on fl.clientid = d.id 
where eventdateitz between date_sub(date(itz(now())), interval 7 day) and date(itz(now()))
and category = 'supplementlog'
;

-- Old Definition - Was including only the foodlogs. New definition includes the Supplements as well.
/*
-- This view doesn't calculate the L1, L2 measures like other existing views because we are still not sure how to calculate those after the new changes in 
-- foods table ( after measurementType, measurementAmount introduction)



select fl.clientid, a.patientname, mealdate, 
case when mealtype = 'Breakfast' then 'B'
	 when mealtype = 'Lunch' then 'L'
     when mealtype like '%Snack%' then 'S'
     when mealtype = 'Dinner' then 'D' end as mealtype,
time(itz(mealtime)) as mealTime,
itz(mealtime) as mealtimeITZ,
b.*,
(select concat(greenstart,'-',greenend) from bi_nutrition_target where foodsNutrient = 'net_carb') as nc_range,
(select concat(greenstart,'-',greenend) from bi_nutrition_target where foodsNutrient = 'calories') as cal_range,
(select concat(greenstart,'-',greenend) from bi_nutrition_target where foodsNutrient = 'netGlycemicCarb') as nGC_range,
(select concat(greenstart,'-',greenend) from bi_nutrition_target where foodsNutrient = 'fibre') as fibre_range

from foodlogs_view fl inner join bi_patient_status a on (fl.clientid = a.clientid and a.status='active' and fl.mealdate between a.status_start_date and a.status_end_date)
				 left join (
									select
									foodid, foodlabel,
									CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
										 WHEN (recommendationRating <= 1) THEN 'Red'
										 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
										 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
										 WHEN (recommendationRating > 3) THEN 'Green*'
									END AS E5foodgrading,
									measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb -- level1
									,calories, IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre -- level 2
									,sodium, potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids -- level 3
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  -- level 4 glycemic controllers
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus -- level 5 biota
									,fructose, lactose, glycemicIndex  -- level 6 carb
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol -- ,  medium-chain  -- level 6 fat
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine -- level 6 protein
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  -- level 6 vitamins
									-- , taurine, vanadium -- level 6 nutraceuticals  -- carnitine, inositol,
									,  copper, iron, zinc, manganese, phosphorus, selenium  -- level 6 minerals-- iodine,

									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  -- level 7 nutrient balance

									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  -- level 8 metabolic improvers

									, vegetarian as veg, case when vegetarian = 0 then 1 end as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  -- level 9 food type -- nonVeg,

									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  -- level 10 cellular stressers

									, water -- level 11 hydration

									from foods a
                                    -- where draftstatus = 'published'
						) b on fl.foodid = b.foodid
where mealdate between date_sub(date(itz(now())), interval 3 month) and date(itz(now()))
;
*/