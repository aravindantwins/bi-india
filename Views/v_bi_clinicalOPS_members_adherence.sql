CREATE VIEW v_bi_clinicalOPS_members_adherence as 
select s.clientid, s3.doctorNameShort as primaryDoctor, s3.morningInternalDoctorName as twinDoctor, 
last_cgm_5d_date, last_cgm_1d_date, last_as_5d_date, last_nut_adh_5d_date, last_available_cgm_5d, last_available_cgm_1d, last_available_as_5d, last_available_nutAdh_5d
from 
(
	select clientid, date 
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
	where date = date(itz(now())) - interval 1 day
) s left join 
(
	select a.clientid, a.last_cgm_5d_date, a.last_cgm_1d_date, last_as_5d_date, last_nut_adh_5d_date, 
	max(case when a.last_cgm_5d_date = b.date then cgm_5d else null end) as last_available_cgm_5d, 
	max(case when a.last_cgm_1d_date = b.date then cgm_1d else null end) as last_available_cgm_1d,
	max(case when a.last_as_5d_date = b.date then as_5d else null end) as last_available_as_5d, 
	max(case when a.last_nut_adh_5d_date = b.date and b.nut_adh_syntax_5d is null then null 
			 when b.nut_adh_syntax_5d >= 0.8 then 'Yes' else 'No' end) as last_available_nutAdh_5d
	
	from 
	(
		select clientid,
		max(case when cgm_5d is not null then date else null end) as last_cgm_5d_date, 
		max(case when cgm_1d is not null then date else null end) as last_cgm_1d_date,
		max(case when as_5d is not null then date else null end) as last_as_5d_date,
		max(case when nut_adh_syntax_5d is not null then date else null end) as last_nut_adh_5d_date
		
		from bi_patient_reversal_state_gmi_x_date bprsgxd 
		group by clientid 
	) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and (a.last_cgm_5d_date = b.date or a.last_cgm_1d_date = b.date or a.last_as_5d_date = b.date or a.last_nut_adh_5d_date = b.date)
	group by a.clientid, a.last_cgm_5d_date, a.last_cgm_1d_date, last_as_5d_date, last_nut_adh_5d_date

) s1 on s.clientid = s1.clientid 
left join bi_patient_initial_setup_x_dates s2 on s.clientid = s2.clientid 
left join v_allclient_list s3 on s.clientid = s3.clientid 
