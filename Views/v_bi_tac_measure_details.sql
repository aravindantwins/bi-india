alter view v_bi_tac_measure_details as 
select 
a.clientid, 
a.patientname, 
a.coachname,
a.leadCoach,
measure_date, 
/*
case when x.measure = 'Yes' then 1
	 when x.measure = 'No' then 0
	 else 'UK' end as sec_com, 
*/
null as treatmentstatus,
is_cgm_available,
spikes_count, 
-- redfood_count, 
(redfood_count - pstar_food_count) as redfood_count,
pstar_food_count,
spike_redfood_indicator, 
curate_spike_redfood_indicator,
redspike_curate,
logtime_by_mealtype, 
logtime_apart_indicator, 
curate_logtime_apart_indicator,
mealtime_curate,
logged_item_quantity, 
items_mealtype_indicator, 
curate_items_mealtype_indicator,
mealcomplete_curate,
actual_predict_variance_under_reported,
actual_predict_indicator,
total_voicelogs, -- voicefoodlogs table
num_of_voicelogs, -- foodlogs table
num_of_manuallogs, -- 1 not required
num_logs_corrected_by_coach, -- 2 not required
num_voicelogs_reviewed_by_coach,
tac_score,  
/*
d.mlogs_created_client as mlogs_created_by_client,
d.mlogs_corrected_coach as mlogs_corrected_by_coach,
d.mlogs_entered_coach as mlogs_entered_by_coach,
d.vlogs_created_client as vlogs_created_by_client,
d.vlogs_added_coach as vlogs_added_by_coach,
d.vlogs_corrected_coach as vlogs_corrected_by_coach,
b.voicelog_review_time as VL_Review_time,
*/
b.is_mealplan_published,
if(b.purplefood_count > 0, 'Y', 'N') as isPurpleFoodPresent,
if(imt.clientid is not null, 'Yes', '') as isDisenrollOpen,
isTempDischarge,
if((pst.clientid IS NOT NULL),'Yes','') AS isAppSuspended

from
(
	SELECT 
					d.date AS date,
					c.clientId AS clientId,
					a.patientName AS patientname,
					a.coachname as coachname,
                    a.leadCoachNameShort as leadCoach,
					if(a.labels like '%TEMP_DISCHARGE%', 'Yes', '') as isTempDischarge
				FROM
				twins.v_date_to_date d
				JOIN twins.bi_patient_status c on (d.date between ifnull(date_add(date(c.visitdateitz), interval 1 day), c.status_start_date) and c.status_end_date and c.status = 'active')
				LEFT JOIN v_client_tmp a on c.clientid = a.clientid
                -- left join v_coach e on a.coachnameShort = e.coachnameShort
	where date >= date_sub(date(itz(now())), interval 1 month)  
) a inner join bi_tac_measure b on a.clientid = b.clientid and b.measure_date = a.date
		 left join (select clientid, count(distinct incidentId) as totalIncidents from bi_incident_mgmt where category = 'disenrollment' and status = 'OPEN' group by clientid) as imt on a.clientid = imt.clientid
         left JOIN bi_patient_suspension_tracker pst on ((a.date between pst.startDate and endDate) and (pst.clientid=a.clientid) and (pst.status=1))
	-- left join bi_foodlogs_x_voicelogs_summary d on a.clientid = d.clientid and a.date = d.mealdate
	 -- left join bi_patient_monitor_measures x on a.clientid = x.clientid and a.date = x.measure_event_date and x.measure_name = 'NUT COM'
where b.measure_date >= date_sub(date(itz(now())), interval 1 month)                       
;

/*Old reference to Google Sheet
left join ( -- this join is to bring in the SEC_COM,  column from a spreadsheet source
									select x.clientid, sec_com, nut_flw
									from v_bi_patient_commitment_detail x
									inner join
									(select clientid, max(report_date) as report_date from v_bi_patient_commitment_detail
									group by clientid
									) a on x.clientid = a.clientid and x.report_date = a.report_date
							  ) c  on b.clientid = c.clientid    
*/

/*
-- This NUT COM is maintained in BI_PATIENT_MONITOR_MEASURES now so using that table directly 
					(		
								SELECT 
									s.clientid AS clientid, s.eventdateitz AS eventdateitz, 
                                    MAX(s.nut_com_curate) as nut_com_curate
								FROM
								(
										SELECT 
											a.clientId AS clientid,
											CAST(ITZ(a.eventTime) AS DATE) AS eventdateitz,
											CASE WHEN s.callLogFieldId = 9 THEN s.value ELSE NULL END AS nut_com_curate
										FROM
										twins.calllogs a
										JOIN twins.calllogdetails s ON a.callLogId = s.callLogId
										WHERE CAST(a.dateAdded AS DATE) >= date_sub(date(itz(now())), interval 1 month)  -- '2019-06-10' (use this date for archive version)
								) s
								GROUP BY s.clientid , s.eventdateitz
					) x on a.clientid = x.clientid and a.date = x.eventdateitz
*/

/*
-- THIS section is maintained by bi_tac_measure load procedure and in table bi_foodlogs_x_voicelogs_summary for speed up of queries. 
                    left join 
                    (
						select a.clientid, date(itz(mealdate)) as mealdate, count(*) as total_logged, 
								-- sum(case when voicefoodlogid is null and b.createdby is null then 1 else 0 end) as mlogs_with_NULL_creator,
								sum(case when voicefoodlogid is null and a.username = coalesce(b.createdby, a.username) and a.username = coalesce(b.modifiedby, a.username) then 1 else 0 end) as mlogs_created_by_client,
								sum(case when voicefoodlogid is null and a.username = b.createdby and modifiedby <> createdby then 1 else 0 end) as mlogs_corrected_by_coach,
								sum(case when voicefoodlogid is null and a.username <> b.createdby then 1 else 0 end) as mlogs_entered_by_coach,
								sum(case when voicefoodlogid is not null and a.username = b.createdby then 1 else 0 end) as vlogs_created_by_client,
								sum(case when voicefoodlogid is not null and a.username <> b.createdby then 1 else 0 end) as vlogs_added_by_coach,
                                sum(case when voicefoodlogid is not null and a.username = b.createdby and modifiedby <> createdby then 1 else 0 end) as vlogs_corrected_by_coach
						from v_Active_clients a inner join foodlogs b on a.clientid = b.clientid
                        where date(itz(mealdate)) >= date_sub(date(itz(now())), interval 35 day)  
						group by a.clientid, date(itz(mealdate))
					) d on a.clientid = d.clientid and a.date = d.mealdate
*/