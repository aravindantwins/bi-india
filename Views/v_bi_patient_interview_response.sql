create view v_bi_patient_interview_response as 
select date(Timestamp) as report_date, EnrolledDay, patientID, b.patientname, cast(HealthImprovements as unsigned) as HealthImprovements,
if(HealthImprovementsComments is null,'',HealthImprovementsComments) as HealthImprovementsComments, 
cast(CoachingSupport as unsigned) as CoachingSupport, 
if(CoachingSupportComments is null,'',CoachingSupportComments) as CoachingSupportComments,
cast(Nutritionsatisfaction as unsigned) as Nutritionsatisfaction,
if(NutritionSatisfactionComments is null,'',NutritionSatisfactionComments) as NutritionSatisfactionComments,
cast(doctorSupport as unsigned) as doctorSupport,
if(DoctorSupportComments is null,'',DoctorSupportComments) as DoctorSupportComments,
cast(dailyRoutine as unsigned) as dailyRoutine,
if(DailyRoutineComments is null,'',DailyRoutineComments) as DailyRoutineComments,
cast(LogisticSupport as unsigned) as LogisticSupport,
if(LogisticSupportComments is null,'',LogisticSupportComments) as LogisticSupportComments,
cast(BloodDrawService as unsigned) as BloodDrawService,
if(BloodDrawServiceComments is null,'',BloodDrawServiceComments) as BloodDrawServiceComments,
cast(AppFeedback as unsigned) as AppFeedback, 
if(AppFeedbackComments is null,'',AppFeedbackComments) as AppFeedbackComments,
if(NPS='',null, cast(NPS as unsigned)) as NPS,
if(NPSComments is null,'',NPSComments) as NPSComments,
patientFriction,
if(PatientFrictionComments is null,'',PatientFrictionComments) as PatientFrictionComments
from bi_patient_interview_response a inner join v_client_tmp b on a.patientid = b.clientid 
order by report_date