create view v_bi_ops_control_center
as
select 	a.clientid, 
		patientnameShort as patientname, 
        doctornameShort as doctorname, 
        coachnameShort as coachname, 
		c.total_days_active as daysenrolled, 
        current_status, 
        category, 
        quarter, 
        alertdate, 
        actualMeasure, 
        case when category like '%interview%' then null else cohortname end as cohortname, 
        currentSetting, 
        alertStatus, 
        recommendedSetting
from bi_ops_control_notification a inner join v_client_tmp b on a.clientid = b.clientid
	left join  (
					select clientid, count(distinct date) as total_days_active 
					from 
					v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_Date and b.status = 'active'
                    where a.date >= if(visitdateitz is null, b.status_start_date, date(visitdateitz))
					group by clientid 
				) c on a.clientid = c.clientid 
;

/* logic moved to a stored procedure
select clientid, patientname, doctorname, coachname, daysenrolled, current_status, 'Ketone' as category, 
quarter,alertdate, cohortname, ketoneCurrentSetting as currentSetting, alertStatus, ketoneRecommendedSetting as recommendedSetting
from 
(
	select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.daysenrolled, s1.current_status, 
	s1.quarter, s1.status_date as alertDate, s1.cohortname, s2.frequency as ketoneCurrentSetting,
	case when current_status <> 'active' then null 
		 when cohortname is null or frequency is null then 'NotEnoughData' 
		 when quarter = 'Q1' and frequency <> 'daily' then 'Yes'
		 when quarter in ('Q2', 'Q3', 'Q4') and (cohortname <> 'In reversal' or cohortname  <> 'Default') and frequency <> 'Daily' then 'Yes'
		 when quarter in ('Q2', 'Q3', 'Q4') and (cohortname = 'In reversal') and frequency <> 'Weekly' then 'Yes' 
		 else 'No' end as alertStatus,
	case when current_status <> 'active' then null 
		 when cohortname is null or frequency is null then 'NotEnoughData' 
		 when quarter = 'Q1' and frequency <> 'Daily' then 'DAILY'
		 when quarter in ('Q2', 'Q3', 'Q4') and (cohortname <> 'In reversal' or cohortname  <> 'Default') and frequency <> 'Daily' then 'DAILY'
		 when quarter in ('Q2', 'Q3', 'Q4') and (cohortname = 'In reversal') and frequency <> 'Weekly' then 'WEEKLY' 
		 else frequency end as ketoneRecommendedSetting
	from 
	(
		select a.clientid, a.patientname, a.doctorname, a.coachname, a.daysenrolled, current_status, a.quarter, status_date, cohortname
		from 
		(
			select distinct a.clientid, a.patientName_short as patientname, 
			a.doctorname_short as doctorname, a.coachname_short as coachname, 
			b.daysenrolled, b.status as current_status, concat('Q',floor(b.daysenrolled/90) + 1) as quarter
			from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid 
			where a.status = 'active'
		) a left join v_all_patient_cohort c on (a.clientid = c.clientid and c.status_date = date(now()))
	) s1 left join 
	(
		select a.clientid, a.date, b.frequency
		from  (	
					select clientid, date 
					from v_date_to_date a left join bi_patient_status b 
					on a.date between status_start_date and status_end_date and b.status = 'active'
			  ) a inner join clienttodoschedules b on a.clientid = b.clientid and b.category = 'test' and type = 'ketone' and a.date between b.startdate and b.enddate
		where a.date = date(now())
	) s2 on s1.clientid = s2.clientid and s1.status_date = s2.date
) s
where alertStatus is not null
;

*/
