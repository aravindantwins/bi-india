create view v_bi_patient_renewal_rate
as
select clientid, current_term_no, doctorname, status_group from v_bi_patient_renewal_by_date where status_group = 'R' and clientid is not null
union all 
select clientid, current_term_no, doctorname, status_group from v_bi_patient_renewal_by_date where status_group = 'D'  and clientid is not null
