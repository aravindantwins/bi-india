create view v_bi_member_selfreport_nutritionSatisfaction_detail as 
select a.clientid, ct.patientName, a.date, c.treatmentdays, ct.coachnameshort as coach, ct.leadCoachnameShort as leadCoach, ct.city, ct.status as member_current_status, 
case when ct.labels like '%twin_prime%' or ct.labels like '%twin_control%' or ct.labels like '%twin_htn%' then 'RCT'
	 when dc.isM1 = 'Yes' then 'M1'																						
	 when dc.isM2Converted = 'Yes' then 'M1-M2'																					
	 else 'M2' end as enrollmentType,
ns.syntaxName as current_syntax, sr_energy, sr_mood, sr_coach, sr_nutrition_satisfaction, 
if((sr_energy is not null or sr_mood is not null or sr_coach is not null) and sr_nutrition_satisfaction is null, 0, sr_nutrition_satisfaction) as sr_nutrition_satisfaction_Derived, 
nut_sat_6d_total_count, nut_sat_6d_poor_rating_count
from 
(
	select clientid, date
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
	where date >= date_sub(date(itz(now())), interval 14 day)
) a left join bi_member_selfreport_detail b on a.clientid = b.clientid and a.date = b.date -- and sr_nutrition_satisfaction <= 3
	left join bi_patient_reversal_state_gmi_x_date c on a.clientid = c.clientid and a.date = c.date
	left join v_client_tmp ct on a.clientid = ct.clientid 
    left join nutritionprofiles np on a.clientid = np.clientid 
    left join nutritionsyntax ns on np.nutritionSyntaxId = ns.nutritionSyntaxId
    left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
; 
