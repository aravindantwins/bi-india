alter view v_bi_Member1_patient_detail_tracker_PILOT_D7_11 as 
select s.clientid, 
patient, 
s.category,
s.coach, 
s4.leadCoachNameShort as Leadcoach,
plancode,
city,
mobilenumber,
active_status_start_date,
s.scheduled_review_date,
s.is_d10_review_completed, 
s.isD10_outcome_achieved,
s1.treatmentday, 
s1.treatmentdayInNumber,
s1.date, 
s2.date as d10_review_date, 
start_glucose, 
s2.cgm_5d as d10_cgm_5d, 

start_medicine_count,
ifnull(s2.medCount,0) as d10_med_count, 

start_symptoms_count,
ifnull(length(s2.MH_symptoms) - length(replace(s2.MH_symptoms,',','')) + 1, 0) as d10_symptoms_count,

start_weight, 
s2.weight_1d as d10_weight,

s1.cgm_5d as current_cgm_5d, 
s1.cgm_1d as current_cgm_1d,
ifnull(s1.medCount,0) as current_med_count,

ifnull(length(s1.MH_symptoms) - length(replace(s1.MH_symptoms,',','')) + 1, 0) as current_symptoms_count,

s1.weight_1d as current_weight,

s1.AS_5d as current_AS_5d,
s1.IMT_outstanding as current_IMT_outstanding, 
s1.nut_adh_syntax_5d as current_nut_adh_5d,

s2.AS_5d as d10_AS_5d,
s2.IMT_outstanding as d10_IMT_outstanding, 
s2.nut_adh_syntax_5d as d10_nut_adh_5d,

if(s3.clientid is not null, 'Yes', 'No') as isDisnerollmentOpen,

concat('5dg: ',s1.cgm_5d, ', Glucose Difference: ', start_glucose - s1.cgm_5d, ', MedCount Difference: ', ifnull(start_medicine_count,0) - ifnull(s1.medCount,0), ', AS_5d: ', s1.AS_5d, ', 5d Nut Adh: ',s1.nut_adh_syntax_5d, ', IMTs Outstanding: ',ifnull(s1.IMT_outstanding,0)) as side1, 
concat('5dg: ',s2.cgm_5d, ', Glucose Difference: ', start_glucose - s2.cgm_5d, ', MedCount Difference: ', ifnull(start_medicine_count,0) - ifnull(s2.medCount,0), ', AS_5d: ', s2.AS_5d, ', 5d Nut Adh: ',s2.nut_adh_syntax_5d, ', IMTs Outstanding: ',ifnull(s2.IMT_outstanding,0)) as side2, 

case when s1.treatmentdays <= 10 and (s1.cgm_5d < 140 or (start_glucose - s1.cgm_5d >= 50)) and (ifnull(if(s1.med_adh = 'NO',0,s1.medCount),0) = 0 or (ifnull(s1.medCount,0) < ifnull(start_medicine_count,0))) and s1.AS_5d >= 90 and s1.nut_adh_syntax_5d >= 0.8 and ifnull(s1.IMT_outstanding,0) = 0 then 'IMMEDIATE'
	 when s1.treatmentdays <= 10 and (s1.cgm_5d < 140 or (start_glucose - s1.cgm_5d >= 50)) and (ifnull(if(s1.med_adh = 'NO',0,s1.medCount),0) = 0 or (ifnull(s1.medCount,0) < ifnull(start_medicine_count,0))) and s1.AS_5d >= 70 and ifnull(s1.IMT_outstanding,0) = 0 then 'HIGH'
     when s1.treatmentdays <= 10 and (s1.cgm_5d < start_glucose) and (ifnull(if(s1.med_adh = 'NO',0,s1.medCount),0) = 0 or (ifnull(s1.medCount,0) <= ifnull(start_medicine_count,0))) and (s1.AS_5d >= 60) and ifnull(s1.IMT_outstanding,0) >= 0 then 'MED'

	 when s1.treatmentdays > 10 and (s2.cgm_5d < 140 or (start_glucose - s2.cgm_5d >= 50)) and (ifnull(if(s2.med_adh = 'NO',0,s2.medCount),0) = 0 or (ifnull(s2.medCount,0) < ifnull(start_medicine_count,0))) and s2.AS_5d >= 90 and s2.nut_adh_syntax_5d >= 0.8 and ifnull(s2.IMT_outstanding,0) = 0 then 'IMMEDIATE'
     when s1.treatmentdays > 10 and (s2.cgm_5d < 140 or (start_glucose - s2.cgm_5d >= 50)) and (ifnull(if(s2.med_adh = 'NO',0,s2.medCount),0) = 0 or (ifnull(s2.medCount,0) < ifnull(start_medicine_count,0))) and s2.AS_5d >= 70 and ifnull(s2.IMT_outstanding,0) = 0 then 'HIGH'
	 when s1.treatmentdays > 10 and (s2.cgm_5d < start_glucose) and (ifnull(if(s2.med_adh = 'NO',0,s2.medCount),0) = 0 or (ifnull(s2.medCount,0) <= ifnull(start_medicine_count,0))) and (s2.AS_5d >= 60) and ifnull(s2.IMT_outstanding,0) >= 0 then 'MED' -- and ifnull(s2.IMT_outstanding,0) > 0

	 when (ifnull(s1.cgm_5d, s2.cgm_5d) is null or ifnull(s1.nut_adh_syntax_5d, s2.nut_adh_syntax_5d) is null) then 'ND' 
     else 'LOW' 
     END as conversionProbability,
     
case when (s1.cgm_5d < 140 or (start_glucose - s1.cgm_5d >= 50)) 
            and (ifnull(if(s1.med_adh = 'NO', 0, s1.medCount),0) = 0 or (ifnull(s1.medCount,0) <= ifnull(start_medicine_count,0)))
            -- and ifnull((IMT_opened - IMT_resolved),0) = 0, 
            then 'Yes' end as current_day_Trial_outcome_achieved
from 
(
	select a.clientid, 'Not Converted' as category, b.patientname as patient, b.coachnameShort as coach, b.city, b.mobilenumber, a.treatmentdays, 
    start_weight, cast((start_labA1C * 28.7) - 46.7 as decimal(10,5)) start_glucose, start_labA1C, 
    replace(replace(start_medicine_diabetes_drugs,'(DIABETES)',''), '(INSULIN)','') as start_diabetes_medicines_drugs, 
    length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count, 
    start_symptoms, b.plancode, 
    length(start_symptoms) - length(replace(start_symptoms,',','')) + 1 as start_symptoms_count,
    case when c.d10_review_schedule_status = 'CANCELLED' then NULL else c.d10_review_date end as scheduled_review_date,
    c.is_d10_review_completed,
	c.isD10_outcome_achieved,
    c.active_status_start_date
	from dim_client a inner join v_allClient_list b on a.clientid = b.clientid 
					  inner join bi_ttp_enrollments c on a.clientid = c.clientid 
	where a.is_row_current = 'y'
	and b.patientname not like '%obsolete%'
    and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
	and c.is_patient_converted is null
    and b.status = 'active'
    
    
	union all 
       
   	select a.clientid, 'Converted' as category, b.patientname as patient, b.coachnameShort as coach, b.city, b.mobilenumber, a.treatmentdays, 
    start_weight, cast((start_labA1C * 28.7) - 46.7 as decimal(10,5)) start_glucose, start_labA1C, 
    replace(replace(start_medicine_diabetes_drugs,'(DIABETES)',''), '(INSULIN)','') as start_diabetes_medicines_drugs, 
    length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count, 
    start_symptoms, b.plancode, 
    length(start_symptoms) - length(replace(start_symptoms,',','')) + 1 as start_symptoms_count,
    case when c.d10_review_schedule_status = 'CANCELLED' then NULL else c.d10_review_date end as scheduled_review_date,
    c.is_d10_review_completed,
	c.isD10_outcome_achieved,
    c.active_status_start_date
	from dim_client a inner join v_allClient_list b on a.clientid = b.clientid 
					  inner join bi_ttp_enrollments c on a.clientid = c.clientid 
	where a.is_row_current = 'y'
	and b.patientname not like '%obsolete%'
    and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
	and c.is_patient_converted = 'yes' and c.conversion_payment_date >= date_sub(date(itz(now())), interval 7 day)
    and b.status = 'active' 
    -- Include patients who are converted in the last 5 days
) s 
inner join 
	(
			select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, (treatmentdays - 1) as treatmentdayInNumber, treatmentdays, a.date, medicine_drugs, medicine, actual_medCount as medCount,
			cgm_1d, cgm_5d, energy_1d, weight_1d, nut_adh_syntax, nut_adh_syntax_5d, AS_1d, AS_5d, BMI, coach_rating, IMT_opened, IMT_resolved, IMT_opened - IMT_resolved as IMT_outstanding,
            isMedRecommendationAvailable, isMedRecommendationAdopted, isMedRecommendationAdopted24hrs, total_delight_moments, MH_symptoms, med_adh
			from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
            where a.date =  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now()))) -- date_sub(date(itz(now())), interval 1 day)
            -- and c.is_patient_converted is null
	) s1 on s.clientid = s1.clientid 
    left join 
	(
			select a.clientid, a.date, concat('D',(treatmentdays - 1)) as treatmentday, (treatmentdays - 1) as treatmentdayInNumber, actual_medCount as medCount,
			cgm_5d, weight_1d, MH_symptoms, nut_adh_syntax_5d, AS_5d, IMT_opened - IMT_resolved as IMT_outstanding, med_adh
			from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
            where treatmentdays = 11
			-- and c.is_patient_converted is null
	) s2 on s.clientid = s2.clientid 
    left join 
    (
			select s.clientid, date(itz(s1.report_time)) as openDate, date(itz(s1.resolved_time)) as resolvedDate 
            from
            (
				select distinct clientid, max(report_time) as report_time 
				from bi_incident_mgmt where category = 'Disenrollment' 
				group by clientid
            ) s inner join bi_incident_mgmt s1 on s.clientid = s1.clientid and s.report_time = s1.report_time
    ) s3 on s.clientid = s3.clientid and s1.date between opendate and ifnull(resolvedDate,date(itz(now())))
    left join  v_coach s4 on s.coach = s4.coachNameShort
;