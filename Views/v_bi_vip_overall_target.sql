set collation_connection = 'latin1_swedish_ci'; 

alter view v_bi_vip_overall_target as 
select s.date, 
count(distinct s.clientid) as totalActive, 
count(distinct case when s.treatmentDays > 35 and s.isReversal = 'Y' then s.clientid else null end) as total_Reversal, 
count(distinct case when s.coach_rating >= 4 then s.clientid else null end) as total_coachRating_4above, 
count(distinct case when s.coach_rating is not null then s.clientid else null end) as total_coachRating_participated, 
count(distinct case when cgm_5d is null and isCBG = 'N' and daysFromLastCgm_5d >= 7 then s.clientid else null end) as total_noData, 
count(distinct s2.incidentID) as totalIncidentOpened,
count(distinct case when resolvedHrs > 24 then s2.incidentID else null end) as totalIncidentOpened_above_24Hrs,
count(distinct case when s2.resolved_date is not null and status = 'RESOLVED' then s2.incidentID else null end) as totalIncidentOpened_resolved
from 
(
	select s1.clientid, s1.date, s1.coach, s1.treatmentdays, s2.coach_rating, s2.cgm_5d, s2.isCBG, if(s2.is_InReversal = 'yes' or s2.is_MetOnly_by_OPS = 'yes', 'Y', 'N') as isReversal, s2.last_available_cgm_5d_date, datediff(s1.date, s2.last_available_cgm_5d_date) as daysFromLastCgm_5d
	from 
	(
		select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays 
		from twins.v_date_to_date a
		inner join
		(
			select clientid, coach, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
			from
			(
				select a.clientid, a.coachname_short as coach, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
				from bi_patient_status a inner join v_client_vip_list vip on a.clientid = vip.clientid
				where a.status = 'active'
				group by a.clientid, date(visitdateitz)
			) s
		) b on a.date between b.startDate and b.endDate
	where a.date >= curdate() - interval 1 month
	) s1 left join bi_patient_reversal_state_gmi_x_date s2 on s1.clientid = s2.clientid and s1.date = s2.date
) s left join (
				select distinct clientid, incidentId, date(itz(resolved_time)) as resolved_date, a.status, date(itz(a.report_time)) as reportDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate, timestampdiff(hour, itz(a.report_time), ifnull(itz(a.resolved_time), itz(now()))) as resolvedHrs 
				from bi_incident_mgmt a inner join clients b on a.clientid = b.id and b.labels like '%VIP%' 
				group by clientid, incidentId
			   ) s2 on s.clientid = s2.clientid and s.date between s2.reportDate and s2.resolvedDate 
group by s.date		
;
