alter view v_bi_RCT_patient_measures_x_preactivation as 
select a.clientid, patientname, doctornameShort as doctor, coachNameShort as coach, enrollmentdate, daysEnrolled, isRCT_PRIME, isRCT_CONTROL, 
date, netcarb_1d, protein_1d, fat_1d, fiber_1d, calories_1d, macro, micro, biota, cgm_1d, cgm_5d, eA1C_1d, eA1C_5d, GMI, LBGI, HBGI, GV, TIR, TAR_1, TAR_2, TBR_1, TBR_2, AUC_1d, MAGE
from bi_rct_patient_measures_x_preactivation a inner join v_allClients_RCT b on a.clientid = b.clientid
;
