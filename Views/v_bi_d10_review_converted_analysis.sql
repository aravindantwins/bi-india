alter view v_bi_d10_review_converted_analysis as 
select date, 'Review_not_converted' as category, 'Actual' as measure_group, 
count(distinct clientid) as total_schedule_reviewed_measure, 
count(distinct case when is_Glucose_achieved = 'Yes' and is_Medicine_achieved = 'Yes' then clientid else null end) as total_D10_outcome_achieved,
count(distinct case when is_Glucose_achieved = 'Yes' then clientid else null end) as total_glucose_achieved,
count(distinct case when is_Medicine_achieved = 'Yes' then clientid else null end) as total_medicine_achieved,
count(distinct case when is_NutAdh_achieved = 'Yes' then clientid else null end) as total_nutAdh_achieved,
count(distinct case when is_ActionScore_achieved = 'Yes' then clientid else null end) as total_AS_achieved,
count(distinct case when is_Member_Declined = 'Yes' then clientid else null end) as total_Member_Declined,
count(distinct case when is_Member_NoData = 'Yes' then clientid else null end) as total_Member_NoData
from v_date_to_date a inner join 
(
	select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, d10_review_schedule_status, d10_date, is_d10_review_completed,
    if((cgm_5d < 140 or (start_glucose - cgm_5d >= 50)), 'Yes', 'No') is_Glucose_achieved,
    if(((ifnull(if(med_adh = 'NO', 0, actual_medCount),0) = 0) or (ifnull(actual_medCount,0) <= ifnull(start_medicine_count,0))), 'Yes', 'No') is_Medicine_achieved,
    if(nut_adh_syntax_5d >= 0.8, 'Yes', 'No') as is_NutAdh_achieved, 
	if(as_5d >= 80, 'Yes', 'No') as is_ActionScore_achieved, 
    if(is_member_likely_to_convert = 'No', 'Yes', 'No') as is_Member_Declined,
    if(cgm_5d is null or nut_adh_syntax_5d is null, 'Yes', 'No') as is_Member_NoData
    from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
							  left join bi_patient_reversal_state_gmi_x_date c on a.clientid = c.clientid and a.d10_date = c.date
							  left join (
												select clientid, cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose,
												length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count
												from dim_client 
												where is_row_current = 'y'
										 ) d on a.clientid = d.clientid
	where is_patient_converted is null
    and is_d10_review_completed = 'Yes'
    and date(d10_review_date) >= '2021-03-01'
) b on d10_review_schedule_date <= a.date
where date >= date_sub(date(now()), interval 7 day) 
group by date

union all 
				
select date, 'Reviewed_Converted' as category, 'Actual' as measure_group, 
count(distinct clientid) as total_schedule_reviewed_measure, 
count(distinct case when is_Glucose_achieved = 'Yes' and is_Medicine_achieved = 'Yes' then clientid else null end) as total_D10_outcome_achieved,
count(distinct case when is_Glucose_achieved = 'Yes' then clientid else null end) as total_glucose_achieved,
count(distinct case when is_Medicine_achieved = 'Yes' then clientid else null end) as total_medicine_achieved,
count(distinct case when is_NutAdh_achieved = 'Yes' then clientid else null end) as total_nutAdh_achieved,
count(distinct case when is_ActionScore_achieved = 'Yes' then clientid else null end) as total_AS_achieved,
count(distinct case when is_Member_Declined = 'Yes' then clientid else null end) as total_Member_Declined,
count(distinct case when is_Member_NoData = 'Yes' then clientid else null end) as total_Member_NoData
from v_date_to_date a inner join 
(
	select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, d10_review_schedule_status, d10_date, is_d10_review_completed,
    if((cgm_5d < 140 or (start_glucose - cgm_5d >= 50)), 'Yes', 'No') is_Glucose_achieved,
    if(((ifnull(if(med_adh = 'NO', 0, actual_medCount),0) = 0) or (ifnull(actual_medCount,0) <= ifnull(start_medicine_count,0))), 'Yes', 'No') is_Medicine_achieved,
    if(nut_adh_syntax_5d >= 0.8, 'Yes', 'No') as is_NutAdh_achieved, 
	if(as_5d >= 80, 'Yes', 'No') as is_ActionScore_achieved, 
    if(is_member_likely_to_convert = 'No', 'Yes', 'No') as is_Member_Declined,
    if(cgm_5d is null or nut_adh_syntax_5d is null, 'Yes', 'No') as is_Member_NoData
    from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
							  left join bi_patient_reversal_state_gmi_x_date c on a.clientid = c.clientid and a.d10_date = c.date
							  left join (
												select clientid, cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose,
												length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count
												from dim_client 
												where is_row_current = 'y'
										 ) d on a.clientid = d.clientid
	where is_patient_converted = 'Yes'
    and is_d10_review_completed = 'Yes'
	and date(d10_review_date) >= '2021-03-01'
) b on d10_review_schedule_date <= a.date
where date >= date_sub(date(now()), interval 7 day) 
group by date

union all 

select date, 'Review_not_converted' as category, 'Percentage' as measure_group, 
count(distinct clientid)/count(distinct clientid) as total_schedule_reviewed_measure, 
count(distinct case when is_Glucose_achieved = 'Yes' and is_Medicine_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_D10_outcome_achieved,
count(distinct case when is_Glucose_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_glucose_achieved,
count(distinct case when is_Medicine_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_medicine_achieved,
count(distinct case when is_NutAdh_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_nutAdh_achieved,
count(distinct case when is_ActionScore_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_AS_achieved,
count(distinct case when is_Member_Declined = 'Yes' then clientid else null end)/count(distinct clientid) as total_Member_Declined,
count(distinct case when is_Member_NoData = 'Yes' then clientid else null end)/count(distinct clientid) as total_Member_NoData
from v_date_to_date a inner join 
(
	select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, d10_review_schedule_status, d10_date, is_d10_review_completed,
    if((cgm_5d < 140 or (start_glucose - cgm_5d >= 50)), 'Yes', 'No') is_Glucose_achieved,
    if(((ifnull(if(med_adh = 'NO', 0, actual_medCount),0) = 0) or (ifnull(actual_medCount,0) <= ifnull(start_medicine_count,0))), 'Yes', 'No') is_Medicine_achieved,
    if(nut_adh_syntax_5d >= 0.8, 'Yes', 'No') as is_NutAdh_achieved, 
	if(as_5d >= 80, 'Yes', 'No') as is_ActionScore_achieved, 
    if(is_member_likely_to_convert = 'No', 'Yes', 'No') as is_Member_Declined,
    if(cgm_5d is null or nut_adh_syntax_5d is null, 'Yes', 'No') as is_Member_NoData
    from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
							  left join bi_patient_reversal_state_gmi_x_date c on a.clientid = c.clientid and a.d10_date = c.date
							  left join (
												select clientid, cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose,
												length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count
												from dim_client 
												where is_row_current = 'y'
										 ) d on a.clientid = d.clientid
	where is_patient_converted is null
    and is_d10_review_completed = 'Yes'
	and date(d10_review_date) >= '2021-03-01'
) b on d10_review_schedule_date <= a.date
where date >= date_sub(date(now()), interval 7 day) 
group by date

union all 
				
select date, 'Reviewed_Converted' as category, 'Percentage' as measure_group, 
count(distinct clientid)/count(distinct clientid) as total_schedule_reviewed_measure, 
count(distinct case when is_Glucose_achieved = 'Yes' and is_Medicine_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_D10_outcome_achieved,
count(distinct case when is_Glucose_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_glucose_achieved,
count(distinct case when is_Medicine_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_medicine_achieved,
count(distinct case when is_NutAdh_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_nutAdh_achieved,
count(distinct case when is_ActionScore_achieved = 'Yes' then clientid else null end)/count(distinct clientid) as total_AS_achieved,
count(distinct case when is_Member_Declined = 'Yes' then clientid else null end)/count(distinct clientid) as total_Member_Declined,
count(distinct case when is_Member_NoData = 'Yes' then clientid else null end)/count(distinct clientid) as total_Member_NoData
from v_date_to_date a inner join 
(
	select a.clientid, b.coachnameShort as coach, date(d10_review_date) as d10_review_schedule_date, d10_review_schedule_status, d10_date, is_d10_review_completed,
    if((cgm_5d < 140 or (start_glucose - cgm_5d >= 50)), 'Yes', 'No') is_Glucose_achieved,
    if(((ifnull(if(med_adh = 'NO', 0, actual_medCount),0) = 0) or (ifnull(actual_medCount,0) <= ifnull(start_medicine_count,0))), 'Yes', 'No') is_Medicine_achieved,
    if(nut_adh_syntax_5d >= 0.8, 'Yes', 'No') as is_NutAdh_achieved, 
	if(as_5d >= 80, 'Yes', 'No') as is_ActionScore_achieved, 
    if(is_member_likely_to_convert = 'No', 'Yes', 'No') as is_Member_Declined,
    if(cgm_5d is null or nut_adh_syntax_5d is null, 'Yes', 'No') as is_Member_NoData
    from bi_ttp_enrollments a inner join v_client_tmp b on a.clientid = b.clientid 
							  left join bi_patient_reversal_state_gmi_x_date c on a.clientid = c.clientid and a.d10_date = c.date
							  left join (
												select clientid, cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose,
												length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count
												from dim_client 
												where is_row_current = 'y'
										 ) d on a.clientid = d.clientid
	where is_patient_converted = 'Yes'
    and is_d10_review_completed = 'Yes'
	and date(d10_review_date) >= '2021-03-01'
) b on d10_review_schedule_date <= a.date
where date >= date_sub(date(now()), interval 7 day) 
group by date
;
                    


