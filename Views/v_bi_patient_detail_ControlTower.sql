-- set collation_connection = 'latin1_swedish_ci';

alter view v_bi_patient_detail_ControlTower as 
-- No cgm 1d 72 hrs
select Category, aa.clientid,patientname,mobileNumber,city,Assigned_TSA,Lead_TSA,Coach,Lead_Coach, bb.last_cgm_date as Last_value_available_date, bb.last_cgm as last_available_value
from
(
		select 'CGM' as category, a.clientId, a.patientName,a.mobileNumber,a.city,a.TSA as Assigned_TSA,a.LeadTSA as Lead_TSA,a.coachNameShort as Coach,a.leadCoachNameShort as Lead_Coach
		from v_allclient_list  a 
		left join (select id as clientid from clientauxfields cx where cx.suspended = 1) b on a.clientid = b.clientid
		  /*
		  (
			Select distinct clientid,subtypel1 from incident where incidenttype ='DISENROLLMENT' and subtypel1 not in ('DISENROLLMENT_FINANCIAL','DISENROLLMENT_NON_RENEWAL_QUARTERLY_PLAN','DISENROLLMENT_NON_RENEWAL_ANNUAL_PLAN')
		  )b on a.clientid=b.clientid
          */
		  left join dim_client c on a.clientid=c.clientid and ( c.is_row_current='y') and  ((c.plancode like '%goldpro%' and c.treatmentdays <39) or c.plancode not like '%goldpro%')
		  left join predictedcgmcycles d on a.clientid=d.clientid and date(itz(now())) between d.startdate and d.enddate 
		  left join 
		  ( select clientid, count(distinct case when cgm_1d is null then date else null end ) as cgm_null_days from  bi_patient_reversal_state_gmi_x_date 
									where date >= date_sub(date(itz(now())),interval 3 day) and date <= date_sub(date(itz(now())),interval 1 day) -- and cgm_1d is null 
									group by clientid  having count(distinct case when cgm_1d is null then date else null end ) = 3
			) bb on a.clientid=bb.clientid
		where a.status='ACTIVE' and d.clientid is null and  bb.clientid is not null and b.clientid is not null and c.clientid is not null
) aa 
left join
(
	select a.clientid,a.last_cgm_date,b.cgm_1d as last_cgm from 
	( 
		select clientid, max( case when cgm_1d is not null then date else null end) as last_cgm_date 
		from bi_patient_reversal_state_gmi_x_date where  date < date_sub(date(itz(now())),interval 3 day)
		group by clientid
	) a join bi_patient_reversal_state_gmi_x_date b where b.clientid=a.clientid and b.date=a.last_cgm_date
) bb on aa.clientid=bb.clientid

union all

-- No weight 1d 48 hrs
select Category, aa.clientid,patientname,mobileNumber,city,Assigned_TSA,Lead_TSA,Coach,Lead_Coach, bb.last_weight_date as Last_value_available_date, bb.last_weight as last_available_value
from
(
		select 'BCM' as category, a.clientId, a.patientName,a.mobileNumber,a.city,a.TSA as Assigned_TSA,a.LeadTSA as Lead_TSA,a.coachNameShort as Coach,a.leadCoachNameShort as Lead_Coach
		from v_allclient_list  a 
		left join (select id as clientid from clientauxfields cx where cx.suspended = 1) b on a.clientid = b.clientid
		  /*
		  (
			Select distinct clientid,subtypel1 from incident where incidenttype ='DISENROLLMENT' and subtypel1 not in ('DISENROLLMENT_FINANCIAL','DISENROLLMENT_NON_RENEWAL_QUARTERLY_PLAN','DISENROLLMENT_NON_RENEWAL_ANNUAL_PLAN')
		  )b on a.clientid=b.clientid
          */
		  left join dim_client c on a.clientid=c.clientid and ( c.is_row_current='y') and  ((c.plancode like '%goldpro%' and c.treatmentdays <39) or c.plancode not like '%goldpro%')
		  left join predictedcgmcycles d on a.clientid=d.clientid and date(itz(now())) between d.startdate and d.enddate 
		  left join 
		  ( 
				select clientid, count(distinct case when weight_1d is null then date else null end ) as weight_null_days from  bi_patient_reversal_state_gmi_x_date 
				where date >= date_sub(date(itz(now())),interval 2 day) and date <= date_sub(date(itz(now())),interval 1 day)-- and cgm_1d is null 
				group by clientid  having count(distinct case when weight_1d is null then date else null end ) = 2
		  ) bb on a.clientid=bb.clientid
		where a.status='ACTIVE' and d.clientid is null and  bb.clientid is not null and b.clientid is not null and c.clientid is not null
) aa left join
(select a.clientid,a.last_weight_date,b.weight_1d as last_weight from 
			( 
				select clientid, max( case when weight_1d is not null then date else null end) as last_weight_date 
                from bi_patient_reversal_state_gmi_x_date where  date < date_sub(date(itz(now())),interval 2 day)
				group by clientid
			) a join bi_patient_reversal_state_gmi_x_date b where b.clientid=a.clientid and b.date=a.last_weight_date
) bb on aa.clientid=bb.clientid

union all

-- No steps 48 hrs
select Category, aa.clientid,patientname,mobileNumber,city,Assigned_TSA,Lead_TSA,Coach,Lead_Coach,bb.last_steps_date as Last_value_available_date,bb.last_steps as last_available_value
from
(
			select 'FITBIT' as category, a.clientId, a.patientName,a.mobileNumber,a.city,a.TSA as Assigned_TSA,a.LeadTSA as Lead_TSA,a.coachNameShort as Coach,a.leadCoachNameShort as Lead_Coach
			from v_allclient_list  a 
			left join (select id as clientid from clientauxfields cx where cx.suspended = 1) b on a.clientid = b.clientid
			  /*
			  (
				Select distinct clientid,subtypel1 from incident where incidenttype ='DISENROLLMENT' and subtypel1 not in ('DISENROLLMENT_FINANCIAL','DISENROLLMENT_NON_RENEWAL_QUARTERLY_PLAN','DISENROLLMENT_NON_RENEWAL_ANNUAL_PLAN')
			  )b on a.clientid=b.clientid
			  */
			  left join dim_client c on a.clientid=c.clientid and ( c.is_row_current='y') and  ((c.plancode like '%goldpro%' and c.treatmentdays <39) or c.plancode not like '%goldpro%')
			  left join predictedcgmcycles d on a.clientid=d.clientid and date(itz(now())) between d.startdate and d.enddate 
			  left join 
			  ( 	
						select a.clientid, count(distinct case when total_steps is null or total_steps = 0 then date else null end ) as steps_null_days
						from 
						(
							select clientid, date from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
							where date >= date_sub(date(itz(now())),interval 2 day) and date <= date_sub(date(itz(now())),interval 1 day)
						) a left join bi_patient_daily_steps_summary b on a.clientid = b.clientid and a.date = b.eventdate
						group by a.clientid 
						having count(distinct case when total_steps is null or total_steps = 0 then date else null end ) = 2
			   ) bb on a.clientid=bb.clientid
			where a.status='ACTIVE' and d.clientid is null and  bb.clientid is not null and b.clientid is not null and c.clientid is not null
) aa LEFT JOIN 
(
	select a.clientid,a.last_steps_date,b.total_steps as last_steps from 
	( 
				select clientid, max(case when total_steps >0  then eventdate else null end) as last_steps_date 
                from bi_patient_daily_steps_summary where  eventdate < date_sub(date(itz(now())),interval 2 day)
				group by clientid
	) a join bi_patient_daily_steps_summary b where b.clientid=a.clientid and b.eventdate=a.last_steps_date
) bb on aa.clientid=bb.clientid
;

            

 