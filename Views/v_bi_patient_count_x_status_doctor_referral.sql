 create view v_bi_patient_count_x_status_doctor_referral as 
 SELECT 
        s1.date AS date,
        s1.doctorname AS doctorname,
        MAX(CASE WHEN (s1.status = 'Pending_active') THEN s2.measure END) AS DRC_Pending_Active_Cnt,
        MAX(CASE WHEN (s1.status = 'Cum_Pending_Active') THEN s2.measure END) AS DRC_Cumulative_Pending_Active_Cnt,
		MAX(CASE WHEN (s1.status = 'Discharged') THEN s2.measure END) AS DRC_Cumulative_Discharged_Cnt,
        MAX(CASE WHEN (s1.status = 'Inactive') THEN s2.measure END) AS DRC_Cumulative_Inactive_Cnt,
        MAX(case when (s1.status = 'Pending_active') THEN s2.total_rate end) as  DRC_Pending_Active_Rate

from 
(
select date, doctor_name as doctorname, c.status
from v_date_to_date a cross join (select distinct doctor_name from bi_patient_Status) b 
					  cross join (select 'Pending_active' as status union all select 'Cum_Pending_Active' as status union all select 'Discharged' as status union all select 'Inactive' as status) c
) s1
left join 
(
	select s1.date, s1.doctorname, s1.status, sum(measure) as measure, sum(s1.measure * s2.fee) as total_rate
    from 
    (
				select date, doctorname, status, year_no, count(distinct clientid) as measure 
                from v_date_to_date a left join 
				(
					select distinct b.clientid, status_start_date, status_end_date, doctor_name as doctorName, 'Y1' as year_no, if(doctor_name is null, null, 'Pending_active') as status from 
					bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
					where b.status = 'pending_active'
					and c.source = 'doctor'
				) b on a.date between b.status_start_date and b.status_end_date
				group by date, doctorName, b.status, year_no
	) s1 left join bi_doctor_fee_ref s2 on s1.doctorname = s2.doctorname and s1.year_no = s2.year_no
    group by s1.date, s1.doctorname, s1.status
    
	union all 
    
	select date, doctorName, status, count(distinct clientid) as measure, null as total_rate
	from v_date_to_date a left join 
	(
		select distinct b.clientid, status_start_date, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'Cum_Pending_Active') as status from 
		bi_patient_status b  left join v_allClient_list c on b.clientid = c.clientid 
		where b.status = 'pending_active'
		and c.source = 'doctor'
	) b on status_start_date <= a.date
	group by date, doctorName, b.status
    
    union all 
    
	select date, doctorName, status, count(distinct clientid) as measure, null as total_rate
	from v_date_to_date a left join 
	(
		select distinct b.clientid, status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status from 
		bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
		where b.status = 'discharged' 
		and c.source = 'doctor'
        and b.clientid not in (select clientid from bi_patient_Status where status = 'active' and status_end_date = date(itz(now())))
		and exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'active') -- this is needed because patients are getting discharged even from pending_active state). Proper discharge is when a patient goes out from Active term
    ) b on date(status_start_date) <= a.date
	group by date, doctorName, b.status
	
    union all 
    
	select date, doctorName, status, count(distinct clientid) as measure, null as total_rate
	from v_date_to_date a left join 
	(
		select distinct b.clientid, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'inactive') as status from 
		bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
		where b.status = 'active' 
		and c.source = 'doctor'
        and b.clientid in (select clientid from bi_patient_Status where status = 'inactive')
		and b.clientid not in (select clientid from bi_patient_Status where status = 'discharged')
	) b on date(status_end_date) <= a.date
	group by date, doctorName, b.status
) s2
on s1.date = s2.date and s1.doctorname = s2.doctorname and s1.status = s2.status 
where (s1.date = last_day(s1.date) or s1.date = date(itz(now())))
group by s1.date, s1.doctorname
; 

