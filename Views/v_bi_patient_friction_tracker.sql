create  view v_bi_patient_friction_tracker as 
select a.date, b.highsatisfactionaction, b.frictionfree, b.discomfort, b.complaint, b.actions
from 
v_date_to_date a 
left join bi_patient_friction_detail b on a.date between b.startdate and b.enddate
order by a.date
;