create view v_bi_delicious_food_rating_detail
as 
select clientid, 
	   patientname, 
       doctorname,
       concat(left(coachname,1), substring(coachName, locate(' ',coachName)+1, 1)) as coach,
       a.date, 
       foodid,
       foodlabel,
       isE5Dish,
       E5foodgrading,
       concat(ifnull(TCU,''),' ',TCM) as TS,
       foodrating,
       netcarb,
	   is_following_nutrition as nut_flw,
       is_following_medicine as med_flw,
       if(length(left(sustain_rating,1)) = 0,null,left(sustain_rating,1)) as sustain_rating,
       sustain_reason,
       sec_com,
       foodcomment,
       is_sustenance_food
from v_date_to_date a left join 
(
select 	a.clientid, 
		b.patientname, 
        b.doctorname, 
        b.coachname,
        itz(a.dateadded) as dateaddedITZ,
		a.foodid, 
        d.foodlabel, 
		-- CASE WHEN (ISNULL(d.recipeLink) OR (d.recipeLink = '')) THEN 'No' ELSE 'Yes' END AS isE5Dish,
        case when rs.foodid is not null then 'Yes' else 'No' end as isE5Dish,
		CASE WHEN isnull(d.recommendationRating) THEN 'Purple' 
			 WHEN (d.recommendationRating <= 1) THEN 'Red' 
             WHEN (d.recommendationRating > 1 AND d.recommendationRating <= 2) THEN 'Orange' 
			 WHEN (d.recommendationRating > 2 AND d.recommendationRating <= 3) THEN 'Green' 
			 WHEN (d.recommendationRating > 3) THEN 'Green*' 
		END AS E5foodgrading,
		if(e.quantity < 1, round(e.quantity,1), floor(e.quantity)) as TCU, 
        d.measurementType as TCM,
        a.foodrating,
        (d.carb - d.fibre) as netcarb,
		pm1.measure as is_following_nutrition,
        pm2.measure as is_following_medicine,
        left(sustain_rating,1) as sustain_rating,
        sustain_reason,
        pm.measure as sec_com,
        a.foodcomment,
        case when sf.foodid is not null then 'Yes' else 'No' end as is_sustenance_food
from personalizedfoods a inner join v_client_tmp b on a.clientid = b.clientid
						 left join foods d on a.foodid = d.foodid
                         left join (select distinct foodid from recipeinstructions) rs on d.foodid = rs.foodid
                         left join (select distinct foodid, quantity from defaultfoodplans where vegetarian = 1 and quantity > 0) e on a.foodid = e.foodid
                         left join bi_patient_monitor_measures pm on a.clientid = pm.clientid and date(itz(a.dateadded)) = pm.measure_event_date and pm.measure_name = 'NUT COM'
                         left join bi_patient_monitor_measures pm1 on a.clientid = pm1.clientid and date(itz(a.dateadded)) = pm1.measure_event_date and pm1.measure_name = 'NUT ADH'
                         left join bi_patient_monitor_measures pm2 on a.clientid = pm2.clientid and date(itz(a.dateadded)) = pm2.measure_event_date and pm2.measure_name = 'MED ADH'
					     left join bi_patient_commitment_detail f on a.clientid = f.clientid and date(itz(a.dateadded)) between record_startdate and record_enddate
                         left join bi_sustenance_foods_list sf on a.foodid = sf.foodid 
) b on a.date = date(b.dateaddedITZ)
where clientid is not null
;
