-- set collation_connection = 'latin1_swedish_ci'; 
-- set sql_mode = 'NO_ENGINE_SUBSTITUTION'

alter view v_bi_patient_service_activation_tracker as 
select a.clientid, concat(a.clientid,' | ',ifnull(a.plancode,''),
							', BP Meter - ',if(b2.clientid is not null, 'Yes', 'No'),
                            ', BHB Meter - ',if(b1.clientid is not null, 'Yes', 'No'), 
                            ifnull(concat(', ',pr.latest_Medicine_Published),'')) as Task_description,
poc,
coach,
email, 
patientname, 
address,
city,
postalCode,
mobileNumber,
pd.homeVisit_scheduled_date as HV_scheduled_date, 
date_add(pd.homeVisit_scheduled_date, interval 2 hour) as HV_scheduled_date_plus_2_hrs,
sa.sensorActivation_scheduled_date as SA_scheduled_date,
fb.first_fitbit_sync_Date,
a.plancode, 
workEmailId,  workPhoneNumber, homePhoneNumber,
First_Medicine_Published,
First_Medicine_Published_Date,
sa.status as SA_Status,
IF((b2.clientId IS NOT NULL), 'Yes', 'No') BP_Meter,
IF((b1.clientId IS NOT NULL), 'Yes', 'No') BHB_Meter,                    
dc.is_medical_exclusion,
dc.medical_exclusion_cohort
from 
(
	select c.clientid, c.patientname, c.coachnameShort AS coach, c.poc, address, city, postalCode, mobileNumber, email, c.plancode, workEmailId,  workPhoneNumber, homePhoneNumber
	from v_allclient_list c                                                                 
	where c.status = 'PENDING_ACTIVE'
    and c.patientname not like '%obsolete%'
) a left join clienttodoschedules b1 on a.clientid = b1.clientid and b1.category = 'TEST' and b1.type in ('GLUCOSE_KETONE', 'KETONE','GLUCOSE') and b1.enddate > date(itz(now()))
	left join clienttodoschedules b2 on a.clientid = b2.clientid and b2.category = 'TEST' and b2.type = 'BLOOD_PRESSURE' and b2.enddate > date(itz(now()))
	-- left join bi_patient_initial_setup_x_dates pd on a.clientid = pd.clientid
    LEFT JOIN 
	(
			select clientid, 
            max(itz(eventTime)) as homeVisit_scheduled_date
			from clientappointments 
			where type = 'HOME_VISIT'
            group by clientid 
    ) pd on a.clientid = pd.clientid
	LEFT JOIN 
	(
			select clientid, 
            max(itz(eventTime)) as sensorActivation_scheduled_date, max(status) as status
			from clientappointments  
			where type = 'SENSOR_ACTIVATION'
            group by clientid
    ) sa on a.clientid = sa.clientid
    LEFT JOIN
    (
					select a.clientid, a.latest_medicine_Published_Date, ifnull(b.actualmedication,'') as latest_Medicine_Published
					from
					(
						Select clientid, max(itz(dateadded)) as latest_medicine_Published_Date from medicationhistories where type = 'DIABETES' group by clientid
					) a inner join medicationhistories b on a.clientid = b.clientid and a.latest_medicine_Published_Date = itz(dateadded) and type = 'DIABETES'
	) pr on a.clientid = pr.clientid
    LEFT JOIN 
    (
					select a.clientid, a.First_Medicine_Published_Date, b.actualmedication as First_Medicine_Published
					from
					(
						Select clientid, min(itz(dateadded)) as First_Medicine_Published_Date from medicationhistories group by clientid
					) a inner join medicationhistories b on a.clientid = b.clientid and a.First_Medicine_Published_Date = itz(dateadded)
    ) fm on a.clientid = fm.clientid 
    left join 
    (
				select a.clientid, min(itz(a.dateadded)) as First_Fitbit_Sync_Date 
                from sensors a 
				where a.sensorModel in ('FITBIT')
				group by a.clientid
	) fb on a.clientid = fb.clientid
	LEFT JOIN dim_client dc on (a.clientid=dc.clientid)
    ; 


