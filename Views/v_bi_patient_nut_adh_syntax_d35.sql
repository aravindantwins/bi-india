create view v_bi_patient_nut_adh_syntax_d35 as 
select s1.date, count(distinct s1.clientid) as total_d35_completed, 
count(distinct case when s2.nut_syntax_color = 'Green' then s1.clientid else null end) as total_green_patients,
count(distinct case when s2.nut_syntax_color = 'Green' then s1.clientid else null end)/count(distinct s1.clientid) as green_P,

count(distinct case when s2.nut_syntax_color = 'Red' then s1.clientid else null end) as total_red_patients,
count(distinct case when s2.nut_syntax_color = 'Red' then s1.clientid else null end)/count(distinct s1.clientid) as red_P,

count(distinct case when s2.nut_syntax_color = 'Orange' then s1.clientid else null end) as total_orange_patients,
count(distinct case when s2.nut_syntax_color = 'Orange' then s1.clientid else null end)/count(distinct s1.clientid) as orange_P,

count(distinct case when s2.nut_syntax = 'UK' then s1.clientid else null end) as total_redPlus_patients,
count(distinct case when s2.nut_syntax = 'UK' then s1.clientid else null end)/count(distinct s1.clientid) as redPlus_P,

count(distinct case when s2.nut_syntax = 'NA' then s1.clientid else null end) as total_NoData_patients,
count(distinct case when s2.nut_syntax = 'NA' then s1.clientid else null end)/count(distinct s1.clientid) as NoData_P,

count(distinct case when s2.nut_adh_syntax = 'Yes' then s1.clientid else null end) as total_1d_NutAdh_patients,
count(distinct case when s2.nut_adh_syntax = 'Yes' then s1.clientid else null end)/ (count(distinct s1.clientid) - count(distinct case when s2.nut_syntax in ('NA') then s1.clientid else null end))  as NutAdh_1d_P,

count(distinct case when s2.nut_adh_syntax_5d >= 0.8 then s1.clientid else null end) as total_5d_NutAdh_patients,
count(distinct case when s2.nut_adh_syntax_5d >= 0.8 then s1.clientid else null end)/ (count(distinct s1.clientid) - count(distinct case when s2.nut_syntax in ('NA') then s1.clientid else null end))  as NutAdh_5d_P,

count(distinct case when s2.nut_adh_syntax_5d < 0.8 then s1.clientid else null end) as total_Non_NutAdh_patients

from 
(
	select date, c.clientid 
	from v_date_to_date a 
	left join 
			(
			select distinct b.clientid, doctorname, b.enrollmentdateitz, 
            date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) as day35
			from	(
							select clientid, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
							max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
							from bi_patient_status b
							group by clientid, status
					) b
			where b.status = 'active' 
			and datediff(status_end_date, if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ))) >= 35
            and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) >= '2019-06-19'
            and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 35 day)) <= date(itz(now()))
			) c on c.day35 >= date_sub(a.date, interval 13 day) and c.day35 <= a.date
	where a.date >= date_sub(date(itz(now())), interval 1 month)
) s1 left join bi_patient_reversal_state_gmi_x_date s2 on (s1.clientid = s2.clientid and s1.date = s2.date)
group by s1.date 
;

