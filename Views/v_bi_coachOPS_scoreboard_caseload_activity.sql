set collation_connection = 'latin1_swedish_ci';

	
alter view v_bi_coachOPS_scoreboard_caseload_activity as 
select date, b.coachId, b.coach, b.leadCoach, 
	count(distinct b.clientid) as total_active, 
	count(distinct case when a.date = b.status_start_date then b.clientid else null end) AS total_treatment_activated,
	count(distinct c.clientid) as total_discharged, 
	count(distinct d.clientid) as total_suspended
	from (select date from v_date_to_date where date >= curdate() - interval 1 month) a left join 
	(
				select distinct a.clientid, status_start_date, status_end_date, b.coachId, b.coachNameShort as coach, b.leadCoachNameShort as leadCoach
				from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid 
				where a.status = 'active'
				and patientname not like '%obsolete%'
	) b on a.date between b.status_start_date and b.status_end_date
	
	left join 
	(
			select distinct a.clientid, coachID, max(status_start_date) as status_start_date
			from bi_patient_status a inner join v_client_tmp b on a.clientid = b.clientid 
			where a.status = 'discharged' 
			and status_start_date >= curdate() - interval 40 day
			and patientname not like '%obsolete%'
			group by a.clientid 
	) c on a.date = c.status_start_date and b.coachId = c.coachID
	
	left join (select a.clientid, b.coachId, startDate, endDate from bi_patient_suspension_tracker a inner join v_client_tmp b on a.clientid = b.clientid and a.status = 1 and startDate >= curdate() - interval 40 day) d on b.clientid = d.clientid and a.date = d.startDate and b.coachId = d.coachId
	group by date, b.coachId, b.coach, leadCoach
	;
	


