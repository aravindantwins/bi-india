create view v_bi_patient_nut_adh_system as 

select s.clientid, b.patientnameShort as patientname, b.doctornameShort as doctor, b.coachnameshort as coach, 
s.eventdate as date, tac_score, total_redfood_count, NC_WO_Pstar, micro_score, biota_score, pstar_food_count, netcarb_1d, cohort, Nut_adh
from bi_patient_nut_adh s left join v_Client_tmp b on s.clientid = b.clientid
where s.eventdate >= date_sub(date(now()), interval 45 day)
;
 
-- This is moved to a stored procedure to maintain the logic
/* 
select s.clientid, b.patientnameShort as patientname, b.doctornameShort as doctor, b.coachnameshort as coach, s.date, tac_score, total_redfood_count, NC_WO_Pstar, micro_score, biota_score,
case when (tac_score = 3 and NC_WO_Pstar < 46 and total_redfood_count = 0 and micro_score > 70 and biota_score > 70) then 'Yes' else 'No' end as Nut_adh 
from 
(
	select a.clientid, a.date, b.tac_score, (b.redfood_count - pstar_food_count) as total_redfood_count, cast(c.measure as decimal(10,2)) as NC_WO_Pstar, 
	cast((d.Micro_final_score/21.21) * 100 as decimal(10,2)) as Micro_score
    ,cast((d.biota_final_score/6.06) * 100 as decimal(10,2)) as biota_score
	from 
	(
		select clientid, date 
		from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
		where date > date(visitdateitz)
		and a.date >= '2020-03-01'-- date_sub(date(itz(now())), interval 2 month) -- date is hardcoded because this new logic to be applied only from this date. 
	) a left join bi_tac_measure b on a.clientid = b.clientid and a.date = b.measure_date 
		left join bi_patient_monitor_measures c on a.clientid = c.clientid and a.date = c.measure_event_date and c.measure_name = 'NC_Excl_PStar'
		left join (
			select clientid, mealdate, sum(case when score_Category = 'Macro' then ifnull(actualScore, 0) end) as macro_final_score,
			sum(case when score_Category = 'Micro' then ifnull(actualScore, 0) end) as Micro_final_score,
			sum(case when score_Category = 'Biota' then ifnull(actualScore, 0) end) as Biota_final_score
			from bi_twins_health_score 
			where score_Category in ('Macro','Micro','biota')
			group by clientid, mealdate
			) d on a.clientid = d.clientid and a.date = d.mealdate 
) s 
	left join v_Client_tmp b on s.clientid = b.clientid
 ;   
 */
 
