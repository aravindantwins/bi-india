use twins;
create view v_bi_doctor_billing_detail as 
select distinct a.cf_clientid_unformatted as clientid, c.patientname, c.doctorname, c.status as current_status, ifnull(c.clinicName,'') as clinicName, a.customer_id, a.email,  					
b.number as invoice_number, b.invoice_id, b.invoice_date, b.status as invoice_status, b.total as invoice_total, b.balance as invoice_balance, ifnull(b.transaction_type,'') as invoice_txn_type, 					
ifnull(d.id,'') as razor_payment_id, ifnull(d.method,'') as razor_payment_method, ifnull(cast(d.amount/100 as decimal(10,2)),'') as razor_payment_amount, ifnull(d.created_at,'') as razor_payment_initiated, 					
ifnull(e.id,'') as razor_trasnfer_id, ifnull(e.processed_at,'') as razor_transfer_processed, ifnull(cast(e.amount/100 as decimal(10,2)),'') as razor_transfer_amount, 
ifnull(e.recipient_settlement_id,'') as razor_transfer_recipient_settle_id,
ifnull(p.payment_id,'') as zoho_payment_id, ifnull(p.invoice_payment_id,'') as zoho_invoice_payment_id, ifnull(p.date,'') as zoho_payment_date, ifnull(p.payment_mode,'') as zoho_payment_mode, ifnull(p.amount,'') as zoho_payment_amount,
ifnull(s.plan_code,'') as plan_code, case when c.source = 'doctor' then 'Yes' else 'No' end as is_DCR
from importedbilling.zoho_customers a inner join importedbilling.zoho_invoices b on a.customer_id = b.customer_id					
					  inner join twins.v_client_tmp c on a.cf_clientid_unformatted = c.clientid 
					  left join importedbilling.zoho_payments p on b.invoice_id = p.invoice_id
                      -- left join importedbilling.razor_payments d on d.notes like concat('%',b.invoice_id,'%') and d.status = 'Captured'			
					  left join importedbilling.razor_payments d on trim(p.gateway_transaction_id) = d.id and d.status = 'Captured'				
                      left join importedbilling.razor_transfers e on e.source = d.id			
					  left join importedbilling.zoho_subscriptions s on a.customer_id = s.customer_id and s.status = 'live'
;	

-- create index idx_zoho_invoice_x_customers on importedbilling.zoho_invoices (invoice_id asc, customer_cf_clientid_unformatted asc, customer_id asc, invoice_date asc, number, status, transaction_type);
-- create index idx_zoho_payments on importedbilling.zoho_payments (invoice_id asc, date asc, gateway_transaction_id, payment_mode);
-- create index idx_razor_payments on importedbilling.razor_payments (id asc, customer_id, status);
-- create index idx_zoho_subscriptions on importedbilling.zoho_subscriptions (customer_id asc, status, plan_code);
-- create index idx_razor_transfers on importedbilling.razor_transfers (id asc, source, recipient_settlement_id, payment_id);

