alter VIEW v_patient_discharge_status AS
SELECT 
        s.clientid AS clientid,
        v.patientName AS PATIENTNAME,
        s.start AS discharged_date,
        v.dischargeReason AS DISCHARGEREASON,
        s.doctor_name AS doctor_name,
        v.age AS AGE,
        v.gender AS GENDER,
        v.address AS ADDRESS,
        v.city AS CITY,
        v.postalCode AS POSTALCODE,
        v.mobileNumber AS MOBILENUMBER,
        i.subtype_l1 AS DISENROLL_CATEGORY,
        fit.first_fitbit_sync_Date AS First_Fitbit_Sync_Date,
        s.status AS status,
        if(sensors.total_weight_available_days > 0, 'Y', 'N') as is_weight_synced_ever, 
        if(sensors.total_BP_available_days > 0, 'Y', 'N') as is_BP_synced_ever, 
        if(sensors.total_ketone_available_days > 0, 'Y', 'N') as is_Ketone_synced_ever
        FROM
        (
				SELECT 
					twins.bi_patient_status.clientid AS clientid,
						MAX(twins.bi_patient_status.status_start_date) AS start,
						MAX(twins.bi_patient_status.status_end_date) AS end,
						twins.bi_patient_status.doctor_name AS doctor_name,
						twins.bi_patient_status.status AS status
				FROM
					twins.bi_patient_status
				WHERE
					twins.bi_patient_status.status = 'discharged'
                    and clientid not in (select clientid from bi_patient_status where status = 'inactive')
				GROUP BY twins.bi_patient_status.clientid
        ) s JOIN twins.v_client_tmp v ON s.clientid = v.clientId
			LEFT JOIN 
					(
							SELECT 
									b.incidentid AS incidentid,
									b.clientid AS clientid,
									b.subtype_L1 AS subtype_l1
							FROM
							twins.bi_incident_mgmt b
							JOIN (
									SELECT 
										twins.bi_incident_mgmt.clientid AS clientid,
										MAX(twins.bi_incident_mgmt.report_time) AS RECENT_report_date
									FROM
									twins.bi_incident_mgmt
									WHERE
									twins.bi_incident_mgmt.category = 'Disenrollment'
									GROUP BY twins.bi_incident_mgmt.clientid
								) a ON b.clientid = a.clientid AND b.report_time = a.RECENT_report_date
							WHERE (b.category = 'Disenrollment')
							GROUP BY b.incidentid
					) i ON s.clientid = i.clientid
			LEFT JOIN bi_patient_initial_setup_x_dates fit ON s.clientid = fit.clientid
            LEFT JOIN (
							select clientid, 
								count(distinct case when measure_type= '1d' and measure_name = 'weight' and average_measure is not null then measure_event_date else null end) as total_weight_available_days,
								count(distinct case when measure_type= '1d' and measure_name = 'ketone' and average_measure is not null then measure_event_date else null end) as total_ketone_available_days,
                                count(distinct case when measure_type= '1d' and measure_name = 'systolic' and average_measure is not null then measure_event_date else null end) as total_BP_available_days
							from bi_measures 
                            where measure_name in ('weight','ketone','systolic')
                            and measure_type = '1d'
                            group by clientid
					   ) sensors on s.clientid = sensors.clientid 
WHERE
        v.status NOT IN ('active' , 'pending_active', 'registration', 'inactive')
		AND v.patientName not LIKE '%obsolete%'
        AND EXISTS
        ( 
			SELECT 1
            FROM bi_patient_status b
            WHERE s.clientid = b.clientid AND b.status IN ('active' , 'pending_active')
            GROUP BY b.clientid
            HAVING COUNT(DISTINCT b.status) >= 1
		)
ORDER BY s.start
;


