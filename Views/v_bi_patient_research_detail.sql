create view v_bi_patient_research_detail as 

select a.clientid, patientnameshort as patientname, doctornameshort as doctorname, coachnameshort as coachname, 
daysenrolled, status, day11, day35, firstbloodworkTimeITZ, report_date as today, eventdateitz, 
category, measureName,  measure
from bi_patient_research_detail a inner join v_client_tmp b on a.clientid = b.clientid
;

-- Logic below along with Lab results moved to a stored procedure
/*
select clientid, patientname, doctorname, coachname, day11, day35, firstbloodworkTimeITZ, today, eventdateitz, 
category, measureName,  measure
from 
( 
select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s2.eventdateitz, 
'Medicine' as category, 'Medicine' as measureName, s2.medication as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join patientescalation s2 on s1.clientid = s2.clientid and (s1.day11 = date(eventdateITZ) or s1.day35 = date(eventdateITZ) or s1.firstbloodworkTimeITZ = date(eventdateITZ) or s1.today = date(eventdateITZ))

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s3.measure_event_date as eventdateitz, 
'Signal' as category, '1dg' as measureName, s3.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s3 on s1.clientid = s3.clientid and (s1.day11 = s3.measure_event_date or s1.day35 = s3.measure_Event_Date or s1.firstbloodworkTimeITZ = s3.measure_event_date or s1.today = s3.measure_event_date) and s3.measure_name = 'cgm' and s3.measure_type = '1d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s4.measure_event_date as eventdateitz,  
'Signal' as category, '5dg' as measureName, s4.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s4 on s1.clientid = s4.clientid and (s1.day11 = s4.measure_event_date or s1.day35 = s4.measure_Event_Date or s1.firstbloodworkTimeITZ = s4.measure_event_date or s1.today = s4.measure_event_date) and s4.measure_name = 'cgm' and s4.measure_type = '5d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s5.measure_event_date as eventdateitz, 
'Signal' as category, '1dk' as measureName, s5.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ , date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s5 on s1.clientid = s5.clientid and (s1.day11 = s5.measure_event_date or s1.day35 = s5.measure_Event_Date or s1.firstbloodworkTimeITZ = s5.measure_event_date or s1.today = s5.measure_event_date) and s5.measure_name = 'ketone' and s5.measure_type = '1d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s6.measure_event_date as eventdateitz, 
'Signal' as category, '5dk' as measureName, s6.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today 
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s6 on s1.clientid = s6.clientid and (s1.day11 = s6.measure_event_date or s1.day35 = s6.measure_Event_Date or s1.firstbloodworkTimeITZ = s6.measure_event_date or s1.today = s6.measure_event_date) and s6.measure_name = 'ketone' and s6.measure_type = '5d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s7.measure_event_date as eventdateitz, 
'Signal' as category, 'Weight' as measureName, s7.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today  
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s7 on s1.clientid = s7.clientid and (s1.day11 = s7.measure_event_date or s1.day35 = s7.measure_Event_Date or s1.firstbloodworkTimeITZ = s7.measure_event_date or s1.today = s7.measure_event_date) and s7.measure_name = 'Weight' and s7.measure_type = '1d'

union all

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, hr.measure_event_date as eventdateitz, 
'Signal' as category, 'Rest-HR' as measureName, hr.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today  
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures hr on s1.clientid = hr.clientid and (s1.day11 = hr.measure_event_date or s1.day35 = hr.measure_Event_Date or s1.firstbloodworkTimeITZ = hr.measure_event_date or s1.today = hr.measure_event_date) and hr.measure_name = 'Rest-HR' and hr.measure_type = '1d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s8.measure_event_date as eventdateitz, 
'Nutrition' as category, '1d-TAC' as measureName, s8.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today  
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s8 on s1.clientid = s8.clientid and (s1.day11 = s8.measure_event_date or s1.day35 = s8.measure_Event_Date or s1.firstbloodworkTimeITZ = s8.measure_event_date or s1.today = s8.measure_event_date) and s8.measure_name = 'TAC' and s8.measure_type = '1d'

union all

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s9.measure_event_date as eventdateitz, 
'Nutrition' as category, '5d-TAC' as measureName, s9.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today   
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s9 on s1.clientid = s9.clientid and (s1.day11 = s9.measure_event_date or s1.day35 = s9.measure_Event_Date or s1.firstbloodworkTimeITZ = s9.measure_event_date or s1.today = s9.measure_event_date) and s9.measure_name = 'TAC' and s9.measure_type = '5d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s10.measure_event_date as eventdateitz, 
'Nutrition' as category, '1d-E5' as measureName, s10.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today  
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s10 on s1.clientid = s10.clientid and (s1.day11 = s10.measure_event_date or s1.day35 = s10.measure_Event_Date or s1.firstbloodworkTimeITZ = s10.measure_event_date or s1.today = s10.measure_event_date) and s10.measure_name = 'E5%' and s10.measure_type = '1d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s11.measure_event_date as eventdateitz, 
'Nutrition' as category, '5d-E5' as measureName, s11.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today  
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s11 on s1.clientid = s11.clientid and (s1.day11 = s11.measure_event_date or s1.day35 = s11.measure_Event_Date or s1.firstbloodworkTimeITZ = s11.measure_event_date or s1.today = s11.measure_Event_Date) and s11.measure_name = 'E5%' and s11.measure_type = '5d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s12.measure_event_date as eventdateitz, 
'Nutrition' as category, '1d-NC' as measureName, s12.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today   
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s12 on s1.clientid = s12.clientid and (s1.day11 = s12.measure_event_date or s1.day35 = s12.measure_Event_Date or s1.firstbloodworkTimeITZ = s12.measure_event_date or s1.today = s12.measure_Event_Date) and s12.measure_name = 'Netcarb' and s12.measure_type = '1d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s13.measure_event_date as eventdateitz, 
'Nutrition' as category, '5d-NC' as measureName, s13.average_measure as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ , date(itz(now())) as today  
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join bi_measures s13 on s1.clientid = s13.clientid and (s1.day11 = s13.measure_event_date or s1.day35 = s13.measure_Event_Date or s1.firstbloodworkTimeITZ = s13.measure_event_date or s1.today = s13.measure_event_date) and s13.measure_name = 'Netcarb' and s13.measure_type = '5d'

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, s14.eventdateitz, 
'Nutrition' as category, 'Nut Adh' as measureName, s14.nut_adh as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today   
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join  
	(
		SELECT 
			a.clientId AS clientid, CAST(ITZ(a.eventTime) AS DATE) AS eventdateitz, s.value as nut_adh
		FROM
		twins.calllogs a JOIN twins.calllogdetails s ON a.callLogId = s.callLogId
		WHERE CAST(a.dateAdded AS DATE) >= '2019-06-10'
		and s.callLogFieldId = 10
	) s14 on s1.clientid = s14.clientid and (s1.day11 = s14.eventdateitz or s1.day35 = s14.eventdateitz or s1.firstbloodworkTimeITZ = s14.eventdateitz or s1.today = s14.eventdateitz)

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, sl.eventdateitz, 
'Signal' as category, 'Sleep-Duraion' as measureName, sum(durationMinutes) as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join 
(
 SELECT DISTINCT
        s.clientid AS clientId,
        date(itz(eventtime)) AS eventdateitz,
        (ITZ(s.eventtime) + INTERVAL s.durationSeconds SECOND) AS endtime,
        (s.durationSeconds / 86400) AS duration,
        (s.durationSeconds / 60) AS durationMinutes,
        s.level AS level
    FROM
        sleepdetails s
        where level in ('deep','light','rem','wake')
) sl on s1.clientid = sl.clientid and (s1.day11 = sl.eventdateitz or s1.day35 = sl.eventdateitz or s1.firstbloodworkTimeITZ = sl.eventdateitz or s1.today = sl.eventdateitz)
group by s1.clientid, sl.eventdateitz

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, sl.eventdateitz, 
'Signal' as category, 'Deep' as measureName, sum(durationMinutes) as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join 
(
 SELECT DISTINCT
        s.clientid AS clientId,
        date(itz(eventtime)) AS eventdateitz,
        (ITZ(s.eventtime) + INTERVAL s.durationSeconds SECOND) AS endtime,
        (s.durationSeconds / 86400) AS duration,
        (s.durationSeconds / 60) AS durationMinutes,
        s.level AS level
    FROM
        sleepdetails s
        where level in ('deep')
) sl on s1.clientid = sl.clientid and (s1.day11 = sl.eventdateitz or s1.day35 = sl.eventdateitz or s1.firstbloodworkTimeITZ = sl.eventdateitz or s1.today = sl.eventdateitz)
group by s1.clientid, sl.eventdateitz

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, sl.eventdateitz, 
'Signal' as category, 'Light' as measureName, sum(durationMinutes) as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join 
(
 SELECT DISTINCT
        s.clientid AS clientId,
        date(itz(eventtime)) AS eventdateitz,
        (ITZ(s.eventtime) + INTERVAL s.durationSeconds SECOND) AS endtime,
        (s.durationSeconds / 86400) AS duration,
        (s.durationSeconds / 60) AS durationMinutes,
        s.level AS level
    FROM
        sleepdetails s
        where level in ('light')
) sl on s1.clientid = sl.clientid and (s1.day11 = sl.eventdateitz or s1.day35 = sl.eventdateitz or s1.firstbloodworkTimeITZ = sl.eventdateitz or s1.today = sl.eventdateitz)
group by s1.clientid, sl.eventdateitz

union all

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, sl.eventdateitz, 
'Signal' as category, 'rem' as measureName, sum(durationMinutes) as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join 
(
 SELECT DISTINCT
        s.clientid AS clientId,
        date(itz(eventtime)) AS eventdateitz,
        (ITZ(s.eventtime) + INTERVAL s.durationSeconds SECOND) AS endtime,
        (s.durationSeconds / 86400) AS duration,
        (s.durationSeconds / 60) AS durationMinutes,
        s.level AS level
    FROM
        sleepdetails s
        where level in ('rem')
) sl on s1.clientid = sl.clientid and (s1.day11 = sl.eventdateitz or s1.day35 = sl.eventdateitz or s1.firstbloodworkTimeITZ = sl.eventdateitz or s1.today = sl.eventdateitz)
group by s1.clientid, sl.eventdateitz

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, s1.day35, s1.firstbloodworkTimeITZ, s1.today, sl.eventdateitz, 
'Signal' as category, 'Wake' as measureName, sum(durationMinutes) as measure
from 
(
	select distinct b.clientid, b.doctorname, b.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11,
	date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 35 day)) as day35,
	firstbloodworkTimeITZ, date(itz(now())) as today
	from
		(
			select clientid, patientname, doctor_name as doctorname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
			max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
			from bi_patient_status b
			group by clientid, status
		) b left join v_Client_tmp c on b.clientid = c.clientid
		left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults group by clientid) d on b.clientid = d.clientid 
	where b.status = 'active' 
	and datediff(status_end_date, status_Start_date) >= 10
) s1 left join 
(
 SELECT DISTINCT
        s.clientid AS clientId,
        date(itz(eventtime)) AS eventdateitz,
        (ITZ(s.eventtime) + INTERVAL s.durationSeconds SECOND) AS endtime,
        (s.durationSeconds / 86400) AS duration,
        (s.durationSeconds / 60) AS durationMinutes,
        s.level AS level
    FROM
        sleepdetails s
        where level in ('wake')
) sl on s1.clientid = sl.clientid and (s1.day11 = sl.eventdateitz or s1.day35 = sl.eventdateitz or s1.firstbloodworkTimeITZ = sl.eventdateitz or s1.today = sl.eventdateitz)
group by s1.clientid, sl.eventdateitz

) s 
where eventdateITZ <= date(itz(now()))
;
*/