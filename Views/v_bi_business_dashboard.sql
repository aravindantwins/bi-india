alter view v_bi_business_dashboard as 
select  a.clientid, d.patientname, d.doctorname as doctor, d.enrollmentdateitz, a.date, a.date_category, a.category, a.measure
, b.severe_start, b.severe_end, b.normal_start, b.normal_end
, case when d.labels like '%exclude%' then 'yes' else 'No' end as exclude_flag -- Type 1 Like
, case when e.is_type1 = 'Yes' then 'Yes' else 'No' end as is_type1 -- unqualified
, ifnull(f.is_exclude_insulin,'Yes') as is_exclude_insulin
, case when d.labels like '%unqualified%' then 'Yes' else 'No' end as is_Unqualified 
, case when e.is_type1Like = 'Yes' then 'Yes' else 'No' end as is_type1Like
, d.status as current_status,
d.city
from bi_business_dashboard a left join bi_stage_business_parameters b on a.category = b.category
 							 -- left join bi_patient_monitor_measures c on a.clientid = c.clientid and a.date = c.measure_event_date and c.measure_name = 'NUT ADH'
                             left join v_client_tmp d on a.clientid = d.clientid 
                             left join dim_client e on a.clientid = e.clientid and e.is_row_current = 'y'
                             left join (select distinct clientid, date_category, case when category = 'Insulin-CurrentCount' and measure > 0 then 'No' else 'Yes' end as is_exclude_insulin from bi_business_dashboard where category = 'Insulin-CurrentCount') f on a.clientid = f.clientid and a.date_category = f.date_category 
;

-- , c.measure as nut_adh
-- , case when e.is_currently_on_insulin = 'Yes' then 'Yes' else 'No' end as is_on_insulin_currently
-- , case when a.category = 'Insulin-CurrentCount' and a.measure > 0 then 'Yes' else 'No' end as is_on_insulin_currently
/*case when a.measure between b.severe_start and b.severe_end then 'S'
	 when a.measure between b.normal_start and b.normal_end then 'N'
          else 'UN' end as normal_status
*/

