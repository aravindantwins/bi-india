alter view v_bi_patient_d10_journey as 
select s.clientid, patient, doctor, s.coach, enrollmentdateITZ as enrollmentDate, planCode, treatmentdays, treatmentActivationDate, start_weight, 
cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose, start_labA1C, start_BMI, 
start_diabetes_medicines_drugs, 
length(start_diabetes_medicines_drugs) - length(replace(start_diabetes_medicines_drugs,',','')) + 1 as start_medicine_count, 
start_symptoms, start_allergies, start_conditions, motivations, s.cohort, 
s1.treatmentday, s1.date, medicine_drugs, medicine, medCount, cgm_1d, cgm_5d, energy_1d, weight_1d, if(nut_adh_syntax = 'Yes',1,0) as nut_adh_1d, 
nut_adh_syntax_5d as nut_adh_5d, AS_1d, AS_5d, coach_rating, 
if(s5.nut_happy='Yes',1,0) as nut_happy_1d,
nut_happy_5d,
if(s2.clientid is not null, 1, 0) as is_CallLog_available, s2.content,
IMT_resolved,
IMT_outstanding,
if(isMedRecommendationAvailable is null, null, if(isMedRecommendationAvailable = 'Yes' and isMedRecommendationAdopted24hrs = 'Yes', 1, 0)) as MRT_SLA,
isMedRecommendationAvailable, isMedRecommendationAdopted, isMedRecommendationAdopted24hrs,
total_delight_moments,
MH_symptoms,
length(MH_symptoms) - length(replace(MH_symptoms,',','')) + 1 as MH_symptoms_count
from 
(
	select a.clientid, b.patientname as patient, b.enrollmentdateITZ, b.doctornameShort as doctor, b.coachnameShort as coach, a.treatmentdays, a.programStartDate_analytics as treatmentActivationDate, start_weight, start_glucose, start_labA1C, 
    replace(replace(start_medicine_diabetes_drugs,'(DIABETES)',''), '(INSULIN)','') as start_diabetes_medicines_drugs, start_symptoms, start_allergies, start_conditions, motivations, b.planCode, a.start_BMI, 
	case when  (start_medicine_diabetes_drugs is null 
			   or (start_medicine_diabetes_drugs = 'Biguanide(DIABETES)' or start_medicine_diabetes_drugs = 'METFORMIN(DIABETES)')
			   or ((length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1) > 2 
					and start_medicine_diabetes_drugs not like '%Sodium-Glucose Cotransporter 2 Inhibitor%' 
					and start_labA1c < 8))
			then 'C1'
		 
			when (start_medicine_diabetes_drugs like '%Insulin%' 
			or (start_BMI < 23 and (length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1) > 2)
			or (b.age > 50 and start_medicine_diabetes_drugs like '%Sodium-Glucose Cotransporter 2 Inhibitor%' and start_medicine_diabetes_drugs like '%Sulfonylurea%'))
			
			then 'C3'
		 else 'C2' end as cohort
	from dim_client a inner join v_active_clients b on a.clientid = b.clientid 
	where a.is_row_current = 'y'
	and b.patientname not like '%obsolete%'
    and b.labels not like '%TWIN_PRIME%'
    and date(b.enrollmentdateITZ) >= date_sub(now(), interval 3 month)
) s 
inner join 
	(
							select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, a.date, medicine_drugs, medicine, actual_medCount as medCount,
							cgm_1d, cgm_5d, energy_1d, weight_1d, nut_adh_syntax, nut_adh_syntax_5d, AS_1d, AS_5d, BMI, coach_rating, IMT_opened, IMT_resolved, IMT_opened - IMT_resolved as IMT_outstanding,
                            isMedRecommendationAvailable, isMedRecommendationAdopted, isMedRecommendationAdopted24hrs, total_delight_moments, MH_symptoms
							from bi_patient_reversal_state_gmi_x_date a
							where a.treatmentdays between 1 and 11 
	) s1 on s.clientid = s1.clientid 
left join calllogs s2 on s.clientid = s2.clientid and s1.date = date(s2.eventLocalTime)
left join bi_patient_nut_happiness_x_date s5 on s.clientid = s5.clientid and s1.date = s5.date 
;