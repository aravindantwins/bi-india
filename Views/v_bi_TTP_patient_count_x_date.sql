-- alter view v_bi_TTP_patient_count_x_date as 
select date, 
total_enrolled,
(total_discharged_NoStart_1 + total_discharged_NoStart_2) as total_discharged_NO_START, 
total_discharged_NoStart_1,
total_discharged_NoStart_2,
total_pending_active,
total_active_less_than_10_days,
total_converted,
total_active_greater_than_10_days,
total_active_greater_than_14_days,
total_discharged_properly_1,
total_discharged_properly_2,
(total_discharged_properly_1 + total_discharged_properly_2) as total_discharged_NOT_EXTENDED,

-- The next calculations are to split the total_discharged_NOT_EXTENDED into <10 and >= 10 days
(total_discharged_properly_before_10days_1 + total_discharged_properly_before_10days_2) as total_discharged_NOT_EXTENDED_within_10days, 
(total_discharged_properly_after_10days_1 + total_discharged_properly_after_10days_2) as total_discharged_NOT_EXTENDED_after_10days, 

(total_discharged_1 + total_discharged_2) as total_discharged_in_TOTAL,
total_active_converted,
total_active
from 
(
	select date, 
	count(distinct s.clientid) as total_enrolled, 
	count(distinct case when (is_patient_converted is null or (is_patient_converted = 'Yes' and date < conversion_payment_date)) and bs.clientid is not null then s.clientid else null end) as total_active, -- total_active_as_on_date_in_TTP
	count(distinct case when bs1.clientid is not null then s.clientid else null end) as total_pending_active, 
	count(distinct case when conversion_payment_date <= date then s.clientid else null end) as total_converted, 
	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs2.clientid is not null and is_patient_discharge_proper = 'Yes' and is_patient_discharged_no_start is null then s.clientid else null end) as total_discharged_properly_1, 
	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs3.clientid is not null and is_patient_discharge_proper = 'Yes' and is_patient_discharged_no_start is null then s.clientid else null end) as total_discharged_properly_2, 

	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs2.clientid is not null and is_patient_discharge_proper = 'Yes' and is_patient_discharged_no_start is null and ifnull(treatmentdays,0) < 10 then s.clientid else null end) as total_discharged_properly_before_10days_1, 
	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs3.clientid is not null and is_patient_discharge_proper = 'Yes' and is_patient_discharged_no_start is null and ifnull(treatmentdays,0) < 10 then s.clientid else null end) as total_discharged_properly_before_10days_2, 

	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs2.clientid is not null and is_patient_discharge_proper = 'Yes' and is_patient_discharged_no_start is null and treatmentdays >= 10 then s.clientid else null end) as total_discharged_properly_after_10days_1, 
	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs3.clientid is not null and is_patient_discharge_proper = 'Yes' and is_patient_discharged_no_start is null and treatmentdays >= 10 then s.clientid else null end) as total_discharged_properly_after_10days_2, 

	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs2.clientid is not null and is_patient_discharged_no_start = 'Yes' then s.clientid else null end) as total_discharged_NoStart_1, 
	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs3.clientid is not null and is_patient_discharged_no_start = 'Yes' then s.clientid else null end) as total_discharged_NoStart_2, 

	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs2.clientid is not null then s.clientid else null end) as total_discharged_1, 
	count(distinct case when current_status in ('discharged', 'inactive') and is_patient_converted is null and bs3.clientid is not null then s.clientid else null end) as total_discharged_2, 

	count(distinct case when (is_patient_converted = 'Yes' and date >= conversion_payment_date) and bs.clientid is not null then s.clientid else null end) as total_active_converted, -- total_active_as_on_date_in_TTP

	count(distinct case when (is_patient_converted is null or (is_patient_converted = 'Yes' and date < conversion_payment_date)) and bs2.clientid is null and bs3.clientid is null and date >= bs.status_start_date and datediff(date, bs.status_start_date) >= 10 then s.clientid else null end) as total_active_greater_than_10_days,
	count(distinct case when (is_patient_converted is null or (is_patient_converted = 'Yes' and date < conversion_payment_date)) and bs2.clientid is null and bs3.clientid is null and date >= bs.status_start_date and datediff(date, bs.status_start_date) < 10 then s.clientid else null end) as total_active_less_than_10_days,

	count(distinct case when (is_patient_converted is null or (is_patient_converted = 'Yes' and date < conversion_payment_date)) and bs2.clientid is null and bs3.clientid is null and date >= bs.status_start_date and datediff(date, bs.status_start_date) >= 14 then s.clientid else null end) as total_active_greater_than_14_days

	from v_date_to_date a left join 
	(
		select s.clientid, ct.patientname, ct.doctorname, ct.status as current_status, 
		ct.enrollmentdateITZ, ct.plancode as current_plan_code, 
		s.is_patient_converted, 
		s.conversion_payment_date, 
		s.is_patient_currently_TTP,
        d.treatmentDays,
		if(count(distinct bs.status) >= 1 and (ct.status = 'discharged' or ct.status = 'Inactive'), 'Yes', null) as is_patient_discharge_proper, 
        s.is_program_exclusion as is_patient_discharged_no_start
		from 
		bi_ttp_enrollments s
			inner join v_client_tmp ct on s.clientid = ct.clientid
			left join bi_patient_status bs on s.clientid = bs.clientid and bs.status in ('active', 'pending_active')
            left join dim_client d on s.clientid = d.clientid and d.is_row_current = 'y'
		 where ct.status not in ('Registration')
		 and ct.patientname not like '%OBSOLETE%'
		group by s.clientid 
	) s on date(enrollmentdateitz) <= a.date
		left join bi_patient_status bs on s.clientid = bs.clientid and bs.status = 'active' and a.date between bs.status_start_date and bs.status_end_date
		left join bi_patient_status bs1 on s.clientid = bs1.clientid and bs1.status = 'pending_active' and a.date between bs1.status_start_date and bs1.status_end_date
		left join bi_patient_status bs2 on s.clientid = bs2.clientid and bs2.status = 'discharged' and a.date between bs2.status_start_date and bs2.status_end_date
		left join bi_patient_status bs3 on s.clientid = bs3.clientid and bs3.status = 'inactive' and a.date between bs3.status_start_date and bs3.status_end_date
    group by date
) s 
where total_enrolled > 0
;

/* old method
select date, 
total_enrolled, 
total_active,
total_pending_active,
total_converted,
total_discharged_properly1 + total_discharged_properly2 as total_proper_discharged,
total_discharged_before_starting,
total_active_converted,
total_active_greater_than_10_days, 
total_active_less_than_10_days
from 
(
	select date, 
	count(distinct clientid) as total_enrolled, 
	count(distinct case when (is_patient_converted is null or (is_patient_converted = 'Yes' and date < conversion_payment_date)) and date between bs.active_status_start_date and bs.active_status_end_date then clientid else null end) as total_active, -- total_active_as_on_date_in_TTP
	count(distinct case when date between pending_active_status_start_date and pending_active_status_end_date then clientid else null end) as total_pending_active, 
	count(distinct case when conversion_payment_date <= date then clientid else null end) as total_converted, 
	count(distinct case when current_status = 'discharged' and (pending_active_status_start_date is not null or active_status_start_date is not null) and date between discharge_status_start_date and discharge_status_end_date then clientid else null end) as total_discharged_properly1, 
	count(distinct case when current_status = 'Inactive' and (pending_active_status_start_date is not null or active_status_start_date is not null) and date between Inactive_status_start_date and Inactive_status_end_date then clientid else null end) as total_discharged_properly2, 

	count(distinct case when current_status = 'discharged' and pending_active_status_start_date is not null and active_status_start_date is null and date between discharge_status_start_date and discharge_status_end_date then clientid else null end) as total_discharged_before_starting, 

	count(distinct case when (is_patient_converted = 'Yes' and date >= conversion_payment_date) and date between active_status_start_date and active_status_end_date then clientid else null end) as total_active_converted, -- total_active_as_on_date_in_TTP

	count(distinct case when (is_patient_converted is null or (is_patient_converted = 'Yes' and date < conversion_payment_date)) and ifnull(discharge_status_start_date,inactive_status_start_date) is null and date >= active_status_start_date and datediff(date, active_status_start_date) >= 10 then clientid else null end) as total_active_greater_than_10_days,
	count(distinct case when (is_patient_converted is null or (is_patient_converted = 'Yes' and date < conversion_payment_date)) and ifnull(discharge_status_start_date,inactive_status_start_date) is null and date >= active_status_start_date and datediff(date, active_status_start_date) < 10 then clientid else null end) as total_active_less_than_10_days 

	from v_date_to_date a left join 
	(
		select s.clientid, ct.patientname, ct.doctorname, ct.status as current_status, 
		ct.enrollmentdateITZ, ct.plancode as current_plan_code, 
		s.is_patient_converted, 
		s.conversion_payment_date, 
		s.is_patient_currently_TTP, 
		min(case when bs.status = 'active' then bs.status_start_date else null end) as active_status_start_date, 
		min(case when bs.status = 'active' then bs.status_end_date else null end) as active_status_end_date, 
		min(case when bs.status = 'pending_active' then bs.status_start_date else null end) as pending_active_status_start_date,
		min(case when bs.status = 'pending_active' then bs.status_end_date else null end) as pending_active_status_end_date,
		if(count(distinct bs.clientid) >= 1 and (ct.status = 'discharged' or ct.status = 'Inactive'), 'Yes', null) as is_patient_discharge_proper,
		min(case when bs.status = 'discharged' then bs.status_start_date else null end) as discharge_status_start_date,
		min(case when bs.status = 'discharged' then bs.status_end_date else null end) as discharge_status_end_date,
		min(case when bs.status = 'inactive' then bs.status_start_date else null end) as inactive_status_start_date,
		min(case when bs.status = 'inactive' then bs.status_end_date else null end) as inactive_status_end_date
		from 
		bi_ttp_enrollments s
			inner join v_client_tmp ct on s.clientid = ct.clientid
			left join bi_patient_status bs on s.clientid = bs.clientid and bs.status in ('active', 'pending_active', 'discharged', 'Inactive')
		 where ct.status not in ('Registration')
		 and ct.patientname not like '%OBSOLETE%'
		group by s.clientid 
	) s on date(enrollmentdateitz) <= a.date
		left join bi_patient_status bs on s.clientid = bs.clientid and bs.status = 'active' and a.date between bs.status_start_date and bs.status_end_date
		left join bi_patient_status bs1 on s.clientid = bs1.clientid and bs1.status = 'pending_active' and a.date between bs1.status_start_date and bs1.status_end_date
	group by date
) s 
where total_enrolled > 0
;
*/
