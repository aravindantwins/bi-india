set collation_connection = 'latin1_swedish_ci';

alter view v_bi_vip_member_insight as 
select
    `v`.`clientId` as `clientid`,
    `v`.`patientName` as `patientName`,
    `v`.`coachNameShort` as `coachNameShort`,
    `v`.`leadCoachNameShort` as `leadcoachNameShort`,
    `v`.`doctorNameShort` as `DoctorNameshort`,
    `v`.`status` as `status`,
    `b`.`date` as `date`,
    `d`.`treatmentDays` as `treatmentdays`,
    `b`.`coach_rating` as `coach_rating`,
    `b`.`is_InReversal` as `is_InReversal`,
    `i`.`IMT_open` as `IMT_open`,
    `b`.`AS_5d` as `AS_5d`,
    `b`.`nut_adh_syntax_5d` as `nut_adh_syntax_5d`,
    `b`.`eA1C_5dg` as `eA1C_5dg`,
    `b`.`cgm_5d` as `cgm_5d`,
    `b`.`medicine` as `medicine`,
    b.isCBG
from
    (((`v_client_vip_list` `v`
join `dim_client` `d` on
    ((`v`.`clientId` = `d`.`clientid`)))
left join `bi_patient_reversal_state_gmi_x_date` `b` on
    (((`v`.`clientId` = `b`.`clientid`)
        and (`b`.`date` = (cast(`itz`(now()) as date) - interval 1 day)))))
left join (
    select
        `v_incident_mgmt`.`clientid` as `clientid`,
        count(distinct (case when (`v_incident_mgmt`.`status` = 'OPEN') then `v_incident_mgmt`.`incidentid` end)) as `IMT_open`
    from
        `v_incident_mgmt`
    group by
        `v_incident_mgmt`.`clientid`) `i` on
    ((`i`.`clientid` = `v`.`clientId`)))
where
    (`d`.`is_row_current` = 'Y')
;