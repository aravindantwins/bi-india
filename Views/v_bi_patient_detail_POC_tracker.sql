SET collation_connection = 'latin1_swedish_ci'; 

alter view v_bi_patient_detail_POC_tracker as 
select a.date, a.clientid, a.patientname, a.age

,ifnull(poc,'') as poc, doctorName_short as doctor, coach, enrollmentdateITZ, city, visitdateItz, firstBloodWorkDateITZ, 
b.HomeVisit_scheduled_Date, b.homeVisit_status, b.BloodWork_Scheduled_Date, b.bloodwork_status, b.DoctorAppointment_Scheduled_Date, b.doctorAppointment_status, b.SA_Scheduled_Date, b.SA_status, b.twinDoctorAppointment_Scheduled_Date, b.twinDoctorAppointment_Scheduled_Status,

Vitals_Conditions_status, Medicine_Allergies_status, Nutrition_QA_status, Habit_QA_status, DoctorConsultation_status, Medicine_publish_status, Language_status

,if(imt.clientid is not null, 'Yes', 'No') as is_disenroll_risk
,a.is_medical_exclusion
,a.medical_exclusion_cohort
,a.days_in_PA , plancode, labels 
,a.m2PlanCode, preferredM2Plan
-- ,case when labels like '%SERVICE_PREFERENCE_2%' then 'Yes' else 'No' end as is_Deferred_PA_OLD
-- ,if(def.clientid is not null, 'Yes', 'No') as is_Deferred_PA
,if(def.clientid is not null or a.status = 'ON_HOLD' OR def1.clientid IS NOT null, 'Yes', 'No') as is_Deferred_PA
,f.First_Medicine_Published_Date, First_Medicine_Published
,BMI
,first_Investigation_upload_date
,recent_bloodReport_upload_date
,mob.First_Mobile_Setup_Date
,fb.First_Fitbit_Sync_Date
,source
,if(status= 'pending_active', null, a.reactivationDate) as reactivationDate,
m.cgm_order,
m.medicine_order,
m.sensor_order,
case when a.labels like '%VIP%' then 'Yes' else 'No' end as VIP,
bf.selectedSlots
from
(
    select date, b.clientid, c.patientname, b.status, cx.reactivationDate, c.age, b.poc, b.doctorName_short, b.coachname_short as coach, b.enrollmentdateITZ, city, b.visitdateItz, firstBloodWorkDateITZ, dc.is_medical_exclusion, dc.medical_exclusion_cohort,
		   if(b.status = 'pending_active', datediff(a.date, date_add(b.status_start_date, interval 1 day)) + 1, null) as days_in_PA, c.plancode, c.labels, c.source, c.m2PlanCode, c.preferredM2Plan
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and (b.status = 'pending_active' or b.status = 'on_hold')
						  inner join v_allclient_list c on b.clientid = c.clientid
                          left join dim_client dc on b.clientid = dc.clientid and dc.is_row_current = 'Y'
                          left join clientauxfields cx on b.clientid = cx.id
	where date >= date_sub(date(itz(now())), interval 15 day)
    and c.status not in ('registration')
    and c.patientname not like '%obsolete%'
) a 
LEFT JOIN -- We have all these dates in a table. But to show them live using the Product tables. 
	(
			select a.clientid, 
			max(case when a.type = 'HOME_VISIT' and date(itz(a.eventTime)) <= ifnull(day5_from_active_status_start, date(itz(a.eventTime)))  then itz(a.eventTime) else null end) as HomeVisit_scheduled_Date, -- HOME_VISIT type is going to be used for tracking the D10 review for a while (until product comes up with the specific type)
			max(case when a.type = 'HOME_VISIT' and date(itz(a.eventTime)) <= ifnull(day5_from_active_status_start, date(itz(a.eventTime))) then a.status else null end) as homeVisit_status, 
			max(case when a.type = 'BLOOD_WORK' and c.bloodWorkQuarter = 'Q1' and c.bloodWorkDay = 'ONBOARDING' then itz(a.eventTime) else null end) as BloodWork_Scheduled_Date, 
			max(case when a.type = 'BLOOD_WORK' and c.bloodWorkQuarter = 'Q1' and c.bloodWorkDay = 'ONBOARDING' then a.status else null end) as bloodwork_status, 
			max(case when a.type = 'BLOOD_WORK' and c.bloodWorkQuarter = 'Q1' and c.bloodWorkDay = 'ONBOARDING' then itz(a.dateadded) else null end) as Bloodwork_Schedule_created_Date, 
			max(case when a.type = 'SENSOR_ACTIVATION' then itz(a.eventTime) else null end) as SA_Scheduled_Date, 
			max(case when a.type = 'SENSOR_ACTIVATION' then a.status else null end) as SA_status, 
			max(case when a.type = 'DOCTOR_APPOINTMENT' and d.Quarter = '0' and d.Day = '0' then itz(a.eventTime) else null end) as DoctorAppointment_Scheduled_Date, 
			max(case when a.type = 'DOCTOR_APPOINTMENT' and d.Quarter = '0' and d.Day = '0' then a.status else null end) as doctorAppointment_status,
			max(case when a.type = 'DOCTOR_APPOINTMENT' and d.Quarter = '0' and d.Day = '0' then itz(a.dateadded) else null end) as DoctorAppointment_Schedule_created_Date,
			max(case when a.type = 'PROGRESS_REVIEW' and p.quarter = '0' and p.day = '0' and a.status not in ('cancelled') then itz(a.eventTime) else null end) as twinDoctorAppointment_Scheduled_Date,
			max(case when a.type = 'PROGRESS_REVIEW' and p.quarter = '0' and p.day = '0' and a.status not in ('cancelled') then a.status else null end) as twinDoctorAppointment_Scheduled_Status
			
								   
			from clientappointments a left join homevisits b on a.clientid = b.clientid and a.clientAppointmentId = b.clientAppointmentId
									  left join bloodworkschedules c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
									  left join doctorappointments d on a.clientid = d.clientid and a.clientAppointmentId = d.clientAppointmentId
									  left join progressreviews p on a.clientid = p.clientid and a.clientAppointmentId = p.clientAppointmentId 
									  left join (
													select clientid, date_add(max(status_start_date), interval 5 day) as day5_from_active_status_start, max(status_end_date) as status_end_date 
													from bi_patient_Status 
													where status = 'active'
													group by clientid
                                                ) e on a.clientid = e.clientid
			
            where date(eventtime) >=  curdate() - interval 20 day                                    
            group by a.clientid 
    ) b on a.clientid = b.clientid

left join (
				select clientid, 
				max(case when otd.todotype = 'VITALS_CONDITIONS' then otd.status else null end) as vitals_conditions_status, 
				max(case when otd.todotype = 'MEDICINES_ALLERGIES' then otd.status else null end) as medicine_allergies_status, 
				max(case when otd.todotype = 'NUTRITION_QUESTIONNAIRE' then otd.status else null end) as Nutrition_QA_status, 
				max(case when otd.todotype = 'HABIT_QUESTIONNAIRE' then otd.status else null end) as Habit_QA_status, 
				max(case when otd.todotype = 'DOCTOR_CONSULTATION' then otd.status else null end) as DoctorConsultation_status, 
				max(case when otd.todotype = 'MEDICINES' then otd.status else null end) as Medicine_publish_status, 
				max(case when otd.todotype = 'LANGUAGES' then otd.status else null end) as Language_status
				from OpsTodoItems otd
				group by clientid 
		  ) otd on a.clientid = otd.clientid


left join (select distinct clientid, date(report_time) as report_date, date(resolved_time) as resolved_date from bi_incident_mgmt where category = 'disenrollment') imt on a.clientid = imt.clientid and a.date between report_date and ifnull(resolved_date, date(now()))

left join (
				select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
				where category = 'Approval' and custom_subtype = 'SPL'
		  ) def on a.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
		  
left join (
				select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
				where category = 'ONBOARDING' and subtype_L1 = 'PATIENT_PREFERENCE_DELAY'
		  ) def1 on a.clientid = def1.clientid and a.date between def1.openDate and def1.resolvedDate
left join (	
			select a.clientid, a.First_Medicine_Published_Date, b.actualmedication as First_Medicine_Published
			from
			(
			Select clientid, min(itz(dateadded)) as First_Medicine_Published_Date from medicationhistories group by clientid
			) a inner join medicationhistories b on a.clientid = b.clientid and a.First_Medicine_Published_Date = itz(dateadded) 
            -- where b.type = 'DIABETES'
		  ) f on f.clientid = a.clientid


left join (

            select a.clientid, b.labreportId, 
			itz(c.dateadded) as first_Investigation_upload_date, 
			-- cast(d.weight/(d.height*d.height) as decimal(10,2)) as BMI 
            a.start_BMI as BMI
			from dim_client a left join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
							  left join labreports c on b.labreportId = c.labreportId
			where a.is_row_current = 'y'
							  /*
                              left join (
											select s.clientid, itz(VITALS_UPDATED_DATE) as dateToConsider, s1.height, s1.weight
											from 
											(
												SELECT CLIENTID, max(DATEMODIFIED)  AS VITALS_UPDATED_DATE
												FROM clinictestresults_aud B
												WHERE B.HEIGHT IS NOT NULL AND B.WEIGHT IS NOT NULL
												GROUP BY CLIENTID
											) s inner join clinictestresults_aud s1 on s.clientid = s1.clientid and s.VITALS_UPDATED_DATE = s1.datemodified
										) d on a.clientid = d.clientid
							  */
		   ) lab on a.clientid = lab.clientid 


left join (
			 select a.clientid, min(itz(a.dateAdded)) as First_Mobile_Setup_Date
			 from userdeviceinfos a 
			 group by a.clientid
          ) mob on a.clientid = mob.clientid  
left join (
				select a.clientid, min(itz(a.dateadded)) as First_Fitbit_Sync_Date 
                from sensors a 
				where a.sensorModel in ('FITBIT')
				group by a.clientid
		  ) fb on a.clientid = fb.clientid 
left join ( select clientid,date(itz(eventtime)) eventtime, max(case when memberOrderType='CGM_ORDERS' then memberOrderStatus end) cgm_order,
			max(case when memberOrderType='NEW_MEDICINE_DELIVERY' then memberOrderStatus end) medicine_order,
			max(case when memberOrderType='NEW_SENSOR_ORDER' then memberOrderStatus end) sensor_order
            from memberorders 
            WHERE memberOrderType in ('CGM_ORDERS', 'NEW_MEDICINE_DELIVERY','NEW_SENSOR_ORDER') and eventtime>= date_sub(date(itz(now())), interval 15 day)
            group by clientid,date(itz(eventtime))
           ) m on (a.clientid=m.clientid) and (a.date=m.eventtime)
left join bloodworkpreferences bf on a.clientid = bf.clientid and bf.bloodworkquarter = 'Q1' and bf.bloodworkday = 'D1'

LEFT JOIN (
			  select m.clientid, max(itz(m.dateAdded)) as recent_bloodReport_upload_Date from medicalrecords m
			  where m.category='BLOOD_REPORT' and m.uploadedByRole='INTERNAL_OPERATIONS'
			  group by m.clientid
          ) mr on a.clientid = mr.clientid 
group by date, clientid
;

/*NEW DEFINITION may be

select a.date, a.clientid, a.patientname

,ifnull(poc,'') as poc, doctorName_short as doctor, coach, enrollmentdateITZ, city, visitdateItz, a.firstBloodWorkDateITZ,
b.homeVisit_scheduled_date as HomeVisit_scheduled_Date, 
b.homeVisit_scheduled_status as homeVisit_status, 
b.bloodWork_scheduled_date as BloodWork_Scheduled_Date, 
b.bloodWork_scheduled_status as bloodwork_status, 
b.doctorAppointment_scheduled_date as DoctorAppointment_Scheduled_Date, 
b.doctorAppointment_status as doctorAppointment_status,

vitals_conditions_status as Vitals_Conditions_status, 
medicine_allergies_status as Medicine_Allergies_status, 
nutrition_QA_status as Nutrition_QA_status, 
habit_QA_status as Habit_QA_status, 
doctorConsultation_status as DoctorConsultation_status, 
medicine_publish_status as Medicine_publish_status, 
language_status as Language_status

,if(imt.clientid is not null, 'Yes', 'No') as is_disenroll_risk
,a.is_medical_exclusion
,a.medical_exclusion_cohort
,a.days_in_PA, a.plancode, a.labels 
,a.m2PlanCode, a.preferredM2Plan
,if(def.clientid is not null, 'Yes', 'No') as is_Deferred_PA
,b.first_medicine_published_date as First_Medicine_Published_Date
,b.first_medicine_published as First_Medicine_Published
,b.first_BMI as BMI
,b.first_investigation_upload_date as first_Investigation_upload_date
,b.first_mobile_setup_Date as First_Mobile_Setup_Date
,b.first_fitbit_sync_Date as First_Fitbit_Sync_Date
,source
from
(
	select distinct date, b.clientid, c.patientname, b.poc, b.doctorName_short, b.coachname_short as coach, b.enrollmentdateITZ, city, b.visitdateItz, firstBloodWorkDateITZ, dc.is_medical_exclusion, dc.medical_exclusion_cohort,
		   datediff(a.date, date_add(b.status_start_date, interval 1 day)) + 1 as days_in_PA, c.plancode, c.labels, c.source, c.m2PlanCode, c.preferredM2Plan
	from v_date_to_date a inner join (
											select clientid, poc, doctorName_short, coachname_short, enrollmentdateITZ, visitdateItz, 
                                            max(status_start_date) as status_start_date, max(status_end_date) as status_end_date 
											from bi_patient_status
											where status = 'pending_active'
											group by clientid, poc
                                      ) b on a.date between b.status_start_date and b.status_end_date
						  inner join v_allclient_list c on b.clientid = c.clientid
                          left join dim_client dc on b.clientid = dc.clientid and dc.is_row_current = 'Y'
	where date >= date_sub(date(itz(now())), interval 30 day)
    and c.status not in ('registration')
    and c.patientname not like '%obsolete%'
) a 

LEFT JOIN bi_patient_initial_setup_x_dates b on a.clientid = b.clientid 
LEFT JOIN (
					select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
					where category = 'Disenrollment'
           ) imt on a.clientid = imt.clientid and a.date between imt.openDate and imt.resolvedDate
LEFT JOIN (
					select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate 
                    from bi_incident_mgmt
					where category = 'Approval' and custom_subtype = 'SPL'
		  ) def on a.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
;

*/



