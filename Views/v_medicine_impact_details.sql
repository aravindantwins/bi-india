-- alter view v_medicine_impact_details as
create view v_medicine_impact_details as
select a.clientid,
ac.patientname,
ac.doctorname,
a.eventdateitz,
ifnull(c.escalationCategory,'') as escalationCategory,
ifnull(d.cohortname,'') as cohortname,
a.medication,
a.cgm5Dg,
a.cgm3Dg,
a.ketone5Dk,
a.spike5D,
a.netcarb5D,
cgm1Dg,
ketone1Dk,
spike1D,
netcarb1D,
medication_change_date_ind,
medicine_up_indicator
from bi_medicine_impact_detail a
inner join ( 
/*
this is required because the sourcetable to load bi_medicine_impact_detail is patientescalation and it is a truncate and load type. 
But bi_medicine_impact_detail is an Insert/update type so it will carry all the changes made to the record. Hence this join helps
to bring only the last active record. 
*/
select clientid, eventdateitz, max(imp_record_key) as key_to_match
from bi_medicine_impact_detail
group by clientid, eventdateitz
) b on a.clientid = b.clientid and a.eventdateitz = b.eventdateitz and a.imp_record_key = b.key_to_match
inner join v_active_clients ac on a.clientid = ac.clientid
left join patientescalation c on a.clientid = c.clientid and a.eventdateitz = c.eventdateitz 
left join v_active_patient_cohort d on a.clientid = d.clientid and a.eventdateitz = d.status_date
;
