alter view v_bi_Member1_d10_review_backlog as 
select distinct s1.clientid, category, patientname, current_status, s3.treatmentday as current_treatmentday, coach, LeadCoach, scheduled_review_date, is_d10_review_completed, isDisenrollOpen, d10_review_schedule_status
from 
(
		select a.clientid, 'Not Converted' as category, d.patientname, d.status as current_status, concat('D',(treatmentdays - 1)) as treatmentday, coachnameShort as coach, leadCoachNameShort as LeadCoach, a.date, c.d10_review_date as scheduled_review_date, c.is_d10_review_completed, c.is_member_likely_to_convert, c.d10_review_schedule_status, if(inc.clientid is not null, 'Yes', 'No') as isDisenrollOpen
		from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
													left join v_client_tmp d on a.clientid = d.clientid
													left join (			
																select s.clientid, date(itz(s1.report_time)) as openDate, if(s1.status = 'OPEN', date(itz(now())), date(itz(s1.resolved_time))) as resolvedDate 
																from
																(
																	select distinct clientid, max(report_time) as report_time 
																	from bi_incident_mgmt where category = 'Disenrollment' 
																	group by clientid
																) s inner join bi_incident_mgmt s1 on s.clientid = s1.clientid and s.report_time = s1.report_time
													    	   ) inc on a.clientid = inc.clientid and a.date <= ifnull(resolvedDate,date(itz(now())))
		where treatmentdays in (7,8,9,10,11,12)
		and c.is_patient_converted is null
		and d.patientname not like '%obsolete%'
		and d.status not in ('registration', 'pending_active')
		and (d.labels not like '%TWIN_PRIME%' and d.labels not like '%CONTROL%')
		
		union all
		
		select a.clientid, 'Converted' as category, d.patientname, d.status as current_status, concat('D',(treatmentdays - 1)) as treatmentday, coachnameShort as coach, leadCoachNameShort as LeadCoach, a.date, c.d10_review_date as scheduled_review_date, c.is_d10_review_completed, is_member_likely_to_convert, c.d10_review_schedule_status, if(inc.clientid is not null, 'Yes', 'No') as isDisenrollOpen
		from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid
													left join v_client_tmp d on a.clientid = d.clientid
													left join (			
																select s.clientid, date(itz(s1.report_time)) as openDate, if(s1.status = 'OPEN', date(itz(now())), date(itz(s1.resolved_time))) as resolvedDate 
																from
																(
																	select distinct clientid, max(report_time) as report_time 
																	from bi_incident_mgmt where category = 'Disenrollment' 
																	group by clientid
																) s inner join bi_incident_mgmt s1 on s.clientid = s1.clientid and s.report_time = s1.report_time
													    	   ) inc on a.clientid = inc.clientid and a.date <= ifnull(resolvedDate,date(itz(now())))
		where treatmentdays in (7,8,9,10,11,12)
		and c.is_patient_converted = 'Yes'
		and d.patientname not like  '%obsolete%'
		and d.status not in ('registration', 'pending_active')
		and (d.labels not like '%TWIN_PRIME%' and d.labels not like '%CONTROL%')
) s1 inner join 
(
		select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, (treatmentdays - 1) as treatmentdayInNumber, treatmentdays, a.date
		from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
        where a.date >= '2021-03-01' and (date between date_sub(date(itz(now())), interval 60 day) and date(itz(now()))) -- March 1st is hardcoded because D10 review scheduling process implemented from this day
) s2 on s1.clientid = s2.clientid and s1.date = s2.date
left join 
(
		select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, (treatmentdays - 1) as treatmentdayInNumber
		from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
        where a.date = if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))  -- date_sub(date(itz(now())), interval 1 day)
) s3 on s1.clientid = s3.clientid 
where s1.treatmentday in ('D6','D7','D8','D9','D10','D11') and (scheduled_review_date is null or (scheduled_review_date is not null and d10_review_schedule_status = 'CANCELLED')) and is_d10_review_completed is null and isDisenrollOpen = 'No'
;
    

