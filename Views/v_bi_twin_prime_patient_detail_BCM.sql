create view v_bi_twin_prime_patient_detail_BCM as 
select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.date, cast(avg(s2.weight) as decimal(10,3)) as weight, cast(avg(s2.bmi) as decimal(10,3)) as bmi, 
cast(avg(s2.metabolicage) as decimal(10,3)) as metabolicage, 
cast(avg(s2.bmr) as decimal(10,3)) as bmr, 
cast(avg(s2.bodytype) as decimal(10,3)) as bodytype, 
cast(avg(s2.bodywaterrate) as decimal(10,3)) as bodyWaterRate, 
cast(avg(s2.bonemass) as decimal(10,3)) as bonemass, 
cast(avg(s2.leanbodyweight) as decimal(10,3)) as leanbodyweight, 
cast(avg(s2.musclemass) as decimal(10,3)) as musclemass, 
cast(avg(s2.musclerate) as decimal(10,3)) as musclerate, 
cast(avg(s2.protein) as decimal(10,3)) as protein,
cast(avg(s2.fat) as decimal(10,3)) as bodyFat, 
cast(avg(s2.subcutaneousFat) as decimal(10,3)) as subcutaneousFat, 
cast(avg(s2.visceralFat) as decimal(10,3)) as visceralFat
from 
(
	select b.clientid, patientname, date, doctor_name as doctorname, b.coachname as coachname, c.labels
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_Date
						  inner join v_client_tmp c on b.clientid = c.clientid 
	where b.status = 'active'
	and date between date_sub(date(itz(now())), interval 1 month) and date(itz(now()))
    and labels like '%TWIN_PRIME%'
) s1 left join weights s2 on s1.clientid = s2.clientid and s1.date = date(itz(s2.eventtime)) 
group by s1.clientid, s1.date
;
