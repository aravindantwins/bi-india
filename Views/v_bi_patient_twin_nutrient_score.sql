create view v_bi_patient_twin_nutrient_score as

select c.clientid, e.patientname, c.category, c.nutrient, c.weight, c.inc_exc, d.date, c.date_category, d.measure, d.max_score, 
d.score_category, d.actualScore, case when e.labels like '%exclude%' then 'yes' else 'No' end as exclude_flag
from (
		select a.clientid, category, nutrient, weight, inc_exc, 'D0' date_category from 
        dim_client a cross join bi_stage_RDI_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
        union all 
        
        select a.clientid, category, nutrient, weight, inc_exc, 'D35' as date_category from 
        dim_client a cross join bi_stage_RDI_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
        union all 
        
        select a.clientid, category, nutrient, weight, inc_exc, 'D90' as date_category from 
        dim_client a cross join bi_stage_RDI_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
	 ) c left join 
(
	select a.clientid, date, category, measure, a.max_score, score_category, actualScore, date_category
	from bi_twins_health_input_score_x_date_category a inner join dim_client d on a.clientid = d.clientid and d.is_row_current = 'y' 
) d on c.clientid = d.clientid and c.nutrient = d.category and c.date_category = d.date_category
left join v_client_tmp e on c.clientid = e.clientid 
;

/* OLD Logic
select c.clientid, e.patientname, c.category, c.nutrient, c.weight, c.inc_exc, c.date, c.date_category, d.measure, d.max_score, 
d.score_category, d.actualScore, case when e.labels like '%exclude%' then 'yes' else 'No' end as exclude_flag
from (
		select a.clientid, category, nutrient, weight, inc_exc, date(a.firstBloodWorkDateITZ) as date, 'D0' date_category from 
        dim_client a cross join bi_stage_RDI_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
        union all 
        
        select a.clientid, category, nutrient, weight, inc_exc, a.d35_date_lab as date, 'D35' as date_category from 
        dim_client a cross join bi_stage_RDI_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
        union all 
        
        select a.clientid, category, nutrient, weight, inc_exc, a.d90_date_lab as date, 'D90' as date_category from 
        dim_client a cross join bi_stage_RDI_lookup b
        where a.status in ('active','discharged','inactive') and a.is_row_current= 'y'
        
	 ) c left join 
(
	select a.clientid, mealdate as eventdate, a.nutrient as category, measure, a.max_score, score_category, actualScore, 
	case when a.mealdate = date(firstBloodWorkDateITZ) then 'D0' 
		 when a.mealdate = d.d35_date_lab then 'D35' 
		 when a.mealdate = d.d90_date_lab then 'D90' end as date_category
	from bi_twins_health_score a inner join dim_client d on a.clientid = d.clientid and d.is_row_current = 'y' 
    and (a.mealdate = date(firstBloodWorkDateITZ) or a.mealdate = d.d35_date_lab or a.mealdate = d.d90_date_lab)
) d on c.clientid = d.clientid and c.nutrient = d.category and c.date = d.eventdate
left join v_client_tmp e on c.clientid = e.clientid 
*/


