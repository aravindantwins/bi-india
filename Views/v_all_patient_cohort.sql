create view v_all_patient_cohort
as
select cd.clientid, patientname, coachname, doctorname, enrollmentDateITZ, visitdateITZ, daysenrolled, cd.date as status_date, ifnull(a.measure,'Default') as cohortname
from 
(	
		select d.date, c.clientId, e.enrollmentDateITZ, c.visitdateITZ, e.patientname, e.coachname, e.doctorname, e.daysenrolled
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date >= c.status_Start_Date and d.date <= c.status_end_date and c.status = 'active'
        inner join v_client_tmp e on c.clientid = e.clientid
) cd left join bi_patient_monitor_measures a on cd.clientid = a.clientid and cd.date = a.measure_event_date and a.measure_name = 'Cohort'
;

/* This is being maintained in bi_patient_monitor_measures table now
select a.clientid, patientname, coachname, doctorname, enrollmentDateITZ, visitdateITZ, daysenrolled, a.date as status_date, ifnull(c.name,'Default') as cohortname
from 
(
    select cd.clientid, patientname, coachname, doctorname, enrollmentDateITZ, visitdateITZ, daysenrolled, cd.date, max(itz(starttime)) as change_timeITZ
	from 
    (	
		select d.date, c.clientId, e.enrollmentDateITZ, c.visitdateITZ, e.patientname, e.coachname, e.doctorname, e.daysenrolled
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date >= c.status_Start_Date and d.date <= c.status_end_date and c.status = 'active'
        inner join v_client_tmp e on c.clientid = e.clientid
	) cd 
	left join cohorthistories a on cd.clientid = a.clientid and date(itz(Starttime)) <= cd.date
	group by cd.clientid, cd.date
    ) a left join cohorthistories b on a.clientid = b.clientid and a.change_timeITZ = itz(b.starttime)
        left join cohorts c on b.cohortid = c.cohortid
;
*/


