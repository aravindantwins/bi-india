alter view v_patient_details_sensor_board_measures
as
select a.clientid, patientName, doctorName, coachName, enrollmentDate, enrollmentDateITZ, measure_event_date, measure_name, final_measure, 
floor(d.targetvalue) as targetvalue
from
 (
	select  e.clientid, c.patientName, c.doctorName, c.coachName, enrollmentDate, c.enrollmentDateITZ, measure_event_date, cast(measure_name as char(25)) as measure_name, 
		case when average_measure is null then null
		 when measure_name in ('Heartrate','Glucose','Action Score', 'CGM') then floor(average_measure)
		 when measure_name in ('Sleep') then floor(average_measure)/60
		 else average_measure end as final_measure 
	from bi_patient_status e inner join bi_measures a on (a.clientid = e.clientid and a.measure_event_date between e.status_start_date and e.status_end_date and e.status = 'active')
							 left join v_client_tmp c on (e.clientid = c.clientid)
    where measure_type='1d' 
    and measure_name in ('Sleep','Heartrate','Glucose','Ketone','CGM', 'Weight','Action Score', 'Steps')
    and a.measure_event_date >= date_sub(date(itz(now())), interval 45 day)
 ) a 
left join clienttodoschedules d on a.clientid = d.clientid and d.type='steps' and a.measure_name = 'steps' and a.measure_event_date between d.startdate and d.enddate

Union all

select b.clientid, c.patientName, c.doctorName, c.coachName, enrollmentDate, c.enrollmentDateITZ, measure_event_date, 
'Blood Pressure' as measureName, bp_average_measure as final_measure, null as targetvalue
from 
(
select a.clientid, a.measure_event_Date, group_concat(b.systolic_measure,'/',a.diastolic_measure) as bp_average_measure
from 
	(select clientid, measure_event_date, floor(average_measure) as diastolic_measure 
    from bi_measures where measure_type = '1d' and measure_name = 'Diastolic' and measure_event_date >= date_sub(date(itz(now())), interval 45 day)) a
	inner join 
	(select clientid, measure_event_date, floor(average_measure) as systolic_measure 
    from bi_measures where measure_type = '1d' and measure_name = 'Systolic' and measure_event_date >= date_sub(date(itz(now())), interval 45 day)) b
	on a.clientid = b.clientid and a.measure_event_date = b.measure_event_date
group by a.clientid, a.measure_event_Date
) b -- , v_active_clients c
-- where b.clientid = c.clientid
inner join bi_patient_status e on b.clientid = e.clientid and b.measure_event_date between e.status_start_date and e.status_end_date and e.status = 'active'
left join v_client_tmp c on b.clientid = c.clientid
;