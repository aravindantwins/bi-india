alter view v_bi_patient_d20_detail as 
select clientid, patientname, coach, leadCoach, enrollmentType, day20, cohort, is_MetOnly, cgm_5d, eRating_5d as coach_rating_5d, nut_adh_5d, e5_5d, tac_5d, as_5d, success_ind, medicine_drugs
from 
(
	select clientid, patientname, coach, leadCoach, enrollmentType, day20, cgm_5d, tac_5d, AS_5d, E5_5d, eRating_5d,medicine_drugs,																						
	total_incidents, cohort, nut_adh_5d, is_MetOnly,																						
	if ( (if(nut_adh_5d = 'YES', 'YES', if(E5_5d >= .90, 'YES','NO')) = 'YES'																						
			and ifnull(tac_5d,0) >= 2.5																				
			and (if(AS_5d >= 85, 'Yes', if(AS_5d < 85 and total_incidents >= 1, 'Yes','No')) = 'Yes')																				
			and ifnull(eRating_5d,4) >= 4																				
		) or cohort = 'In Reversal' or is_MetOnly = 'Yes', 'Yes','No') as success_ind																			
	from																							
	(																						
			select a.clientid, ct.patientname, ct.coachNameShort as coach, ct.leadCoachNameShort as leadCoach, a.visitdateitz, a.treatmentsetupdateITZ, day20,																						
			b4.average_measure as E5_5d, 
			re.coach_rating_5d as eRating_5d, 																					
			re.cgm_5d,																						
			re.as_5d,																						
			re.tac_5d,																						
			re.cohortname as cohort,	
            re.medicine_drugs,
			case when re.nut_adh_syntax_5d >= 0.8 then 'YES' else 'NO' end as nut_adh_5d, re.is_MetOnly_by_OPS as is_MetOnly,																						
			case when dc.isM1 = 'Yes' then 'M1'																						
				when dc.isM2Converted = 'Yes' then 'M1-M2'																					
			else 'M2' end as enrollmentType																						
			,count(distinct imt.incidentid) as total_incidents																						
			from																						
				(																					
						select distinct b.clientid, doctorname, coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,																			
						date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 20 day)) as day20																		
						from																			
						(																			
							select clientid, doctor_name as doctorname, coachname, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status,																		
							max(status_start_date) as status_start_date, max(status_end_date) as status_end_date																		
							from bi_patient_status b																		
							group by clientid, status																		
						) b																			
						where b.status = 'active'																			
						and datediff(status_end_date, if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ))) >= 20																			
						and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 20 day)) >= date_sub(date(itz(now())), interval 4 month)
						and date(date_add(if(status_start_date > date(visitdateITZ), status_start_date, date(visitdateITZ)), interval 20 day)) <= date(itz(now()))																			
				) a																					
				left join bi_measures b4 on a.clientid = b4.clientid and b4.measure_event_date = a.day20 and b4.measure_type = '5d' and b4.measure_name in ('E5%')																					
				left join (select distinct incidentid, clientid, status, date(report_time) as report_date from bi_incident_mgmt where category = 'sensors') imt on a.clientid = imt.clientid and report_date <= a.day20																					
				left join bi_patient_reversal_state_gmi_x_date re on a.clientid = re.clientid and a.day20 = re.date																					
				left join dim_client dc on a.clientid = dc.clientid and dc.is_row_Current = 'Y'																					
				left join v_client_tmp ct on a.clientid = ct.clientid																					
			group by a.clientid																							
		) s																						
) s 
;