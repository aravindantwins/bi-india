SET collation_connection = 'latin1_swedish_ci';


alter view v_bi_medicine_adoption_detail
as
select 
date(itz(h.dateAdded)) as medchangedate, 
p.doctornameshort as doctor, 
p.doctorId, 
p.clientid,
p.patientname as patient, 
c.cohortType, 
h.changeType, 
itz(h.dateAdded) as medChangeTimeITZ, 
case when h.changeReason is null and h.overrideReasons is null then null
	 when h.changeReason is null then h.overrideReasons
	 when h.overrideReasons is null then h.changeReason
     else concat(trim(h.changeReason), '; ', trim(h.overrideReasons)) end as doctorComments, 

r.rationale as rationale, 
h.recommendedMedication, 
h.actualMedication,
h.medicationtierid,
h.overrideReasons 
from medicationhistories h, 
medicationRecommendations r, 
v_client_tmp p, 
cohorts c
where h.clientId=p.clientid 
and h.medicationRecommendationId=r.medicationRecommendationId 
and date(itz(h.dateadded)) >= '2019-06-20'
and h.cohortId=c.cohortId
;
