create view v_bi_delicious_food_rating
as
select foodid, foodlabel, E5foodgrading, TS, netcarb, isE5Dish, is_sustenance_food, 
count(foodrating) as num_D_ratings, 
count(distinct clientid) as num_patients,
avg(foodrating) as avg_D_rating,
sum(case when foodrating < 4 then 1 else 0 end)/count(foodrating) as D_rating_4_percent
from
(
select 	a.clientid, 
		b.patientname, 
        itz(a.dateadded) as dateaddedITZ,
		a.foodid, 
        d.foodlabel, 
		-- CASE WHEN (ISNULL(d.recipeLink) OR (d.recipeLink = '')) THEN 'No' ELSE 'Yes' END AS isE5Dish,
        case when rs.foodid is not null then 'Yes' else 'No' end as isE5Dish,
		CASE WHEN isnull(d.recommendationRating) THEN 'Purple' 
			 WHEN (d.recommendationRating <= 1) THEN 'Red' 
             WHEN (d.recommendationRating > 1 AND d.recommendationRating <= 2) THEN 'Orange' 
			 WHEN (d.recommendationRating > 2 AND d.recommendationRating <= 3) THEN 'Green' 
			 WHEN (d.recommendationRating > 3) THEN 'Green*' 
		END AS E5foodgrading,
		e.quantity as TS,
        a.foodrating,
        (d.carb - d.fibre) as netcarb,
        case when sf.foodid is not null then 'Yes' else 'No' end as is_sustenance_food
from personalizedfoods a left join v_client_tmp b on a.clientid = b.clientid
						 left join foods d on a.foodid = d.foodid
                         left join (select distinct foodid from recipeinstructions) rs on d.foodid = rs.foodid
                         left join (select distinct foodid, quantity from defaultfoodplans where vegetarian = 1 and quantity > 0) e on a.foodid = e.foodid
						 left join bi_sustenance_foods_list sf on a.foodid = sf.foodid 
) X
group by foodid, foodlabel, E5foodgrading, TS, netcarb, isE5Dish
;
