SELECT 
				`fl`.`clientid` AS `clientid`,
				`fl`.`foodlogid` AS `foodlogid`,
				`fl`.`foodid` AS `foodid`,
				`fl`.`quantity` AS `quantity`,
				(`f`.`calories` * `fl`.`quantity`) AS `calories`,
				(`f`.`fiber` * `fl`.`quantity`) AS `fiber`,
				(`f`.`protein` * `fl`.`quantity`) AS `protein`,
				(`f`.`carb` * `fl`.`quantity`) AS `carb`,
				((`f`.`carb` - `f`.`fiber`) * `fl`.`quantity`) AS `netCarb`,
				(`f`.`fat` * `fl`.`quantity`) AS `fat`,
				`fl`.`mealType` AS `mealType`,
				`fl`.`customFoodLog` AS `customFoodLog`,
				COALESCE(`fl`.`customFoodLog`, `f`.`foodLabel`) AS `foodLogged`,
				(`fl`.`dateAdded` + INTERVAL 330 MINUTE) AS `dateAddedITZ`,
				(`fl`.`mealTime` + INTERVAL 330 MINUTE) AS `mealTimeITZ`,
				CAST((`fl`.`mealTime` + INTERVAL 330 MINUTE) AS DATE) AS `mealDateITZ`,
				`f`.`foodLabel` AS `foodLabel`,
				`f`.`foodName` AS `foodName`,
				`f`.`descriptor` AS `descriptor`,
				IFNULL((((`f`.`carb` - `f`.`fiber`) * `f`.`glycemicIndex`) / 55),
						0) AS `netGICarb`,
				`f`.`measure` AS `measure`,
				`f`.`recommendationRating` AS `recommendationRating`,
				IFNULL(`f`.`E5foodgrading`, 'Purple') AS `E5foodgrading`,
				IFNULL(`f`.`foodpoint`, 0) AS `foodpoint`,
				IFNULL(`f`.`E5Grade`, 0) AS `E5Grade`,
				IFNULL(`f`.`isE5Dish`, 'No') AS `isE5Dish`,
				`f`.`points` AS `points`,
				IFNULL(`f`.`hasReplacement`, 'No') AS `hasReplacement`,
				`k`.`foodgrade` AS `rankGrade`,
				`k`.`food_consumed` AS `food_consumed`,
				`k`.`patient_count` AS `patient_count`,
				`k`.`rank` AS `rank`,
                f.tcu
FROM
				`twins`.`foodlogs` `fl`
				LEFT JOIN (SELECT DISTINCT
									`d`.`foodid` AS `foodId`,
									`d`.`foodName` AS `foodName`,
									`d`.`foodLabel` AS `foodLabel`,
									`d`.`descriptor` AS `descriptor`,
									`d`.`calories` AS `calories`,
									`d`.`fibre` AS `fiber`,
									`d`.`glycemicIndex` AS `glycemicIndex`,
									`d`.`carb` AS `carb`,
									`d`.`fat` AS `fat`,
									`d`.`protein` AS `protein`,
									`d`.`recommendationRating` AS `recommendationRating`,
									`d`.`points` AS `points`,
									`d`.`measure` AS `measure`,
									CASE
												WHEN ISNULL(`d`.`recommendationRating`) THEN 'Purple'
												WHEN (`d`.`recommendationRating` <= 1) THEN 'Red'
												WHEN  (`d`.`recommendationRating` > 1) AND (`d`.`recommendationRating` <= 2) THEN	'Orange'
												WHEN (`d`.`recommendationRating` > 2) AND (`d`.`recommendationRating` <= 3)	THEN 'Green'
												WHEN (`d`.`recommendationRating` > 3) THEN 'Green*'
									END AS `E5foodgrading`,
									CASE
												WHEN (`d`.`recommendationRating` > 2) THEN 1
												ELSE 0
									END AS `foodpoint`,
									CASE
												WHEN (`d`.`recommendationRating` > 2) AND (`d`.`recommendationRating` <= 4)	THEN 100 ELSE 0
									END AS `E5Grade`,
									CASE
												WHEN (ISNULL(`d`.`recipeLink`) OR (`d`.`recipeLink` = '')) THEN 'No'
												ELSE 'Yes'
									END AS `isE5Dish`,
									CASE
												WHEN ISNULL(`m`.`substituteFoodId`) THEN 'No'
												ELSE 'Yes'
									END AS `hasReplacement`,
									t.quantity as TCU
							FROM
								 `twins`.`foods` `d`
								 LEFT JOIN `twins`.`foodsubstitutemappings` `m` ON `m`.`foodId` = `d`.`foodid`
								 left join (SELECT DISTINCT foodid, quantity
								 FROM  defaultfoodplans t where vegetarian = 1 and quantity > 0 ) t on d.foodid = t.foodid
							) `f` ON (`fl`.`foodid` = `f`.`foodId`)                 
				LEFT JOIN `twins`.`bi_foodgrade_rank` `k` ON (`fl`.`foodid` = `k`.`foodid`)
;
select now()