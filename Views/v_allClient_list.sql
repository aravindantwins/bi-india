set collation_connection = 'latin1_swedish_ci';
 
  alter view v_allClient_list as 
       SELECT 
        `c`.`ID` AS `clientId`,
        ca.userId, 
        CAST(ITZ(NOW()) AS DATE) AS `dateITZ`,
        UPPER(CONCAT(`cp`.`firstName`, ' ', `cp`.`lastName`)) AS `patientName`,
        UPPER(CONCAT(`cp`.`firstName`,
                        ' ',
                        LEFT(`cp`.`lastName`, 1))) AS `patientNameShort`,
        `cp`.`dateOfBirth`,                
        ((YEAR(CURDATE()) - YEAR(`cp`.`dateOfBirth`)) - (DATE_FORMAT(CURDATE(), '%m%d') < DATE_FORMAT(`cp`.`dateOfBirth`, '%m%d'))) AS `age`,
        d.id as doctorId, 
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`, ' ', `d`.`lastName`)))) AS `doctorName`,
        LTRIM(RTRIM(UPPER(CONCAT(`d`.`firstName`,
                        ' ',
                        LEFT(`d`.`lastName`, 1))))) AS `doctorNameShort`,
		d.clinicName,
        UPPER(CONCAT(`h`.`firstName`, ' ', `h`.`lastName`)) AS `coachName`,
        UPPER(CONCAT(`h`.`firstName`,
                        ' ',
                        LEFT(`h`.`lastName`, 1))) AS `coachNameShort`,
		upper(concat(up.firstName ,' ' ,left(up.lastName, 1))) as leadCoachNameShort,
        UPPER(CONCAT(mind.firstname, ' ', mind.lastname)) as morningInternalDoctorName, -- morning doctor
        UPPER(CONCAT(eved.firstname, ' ', eved.lastname)) as eveningInternalDoctorName, -- evening doctor
		UPPER(CONCAT(mind.firstname, ' ', LEFT(mind.lastname,1))) as morningInternalDoctorNameShort, 
		UPPER(CONCAT(eved.firstname, ' ', LEFT(eved.lastname,1))) as eveningInternalDoctorNameShort, 
        cp.email, 
        ca.city,
		ca.pha,
        ca.phaShort,
        ca.poc,
        ca.pocDateAdded,
        ca.RPA,
        ca.TSA,
        ca.LeadTSA,
        ca.healthCoordinator,
        c.gender,
        ca.address, 
        ca.postalCode, 
        ca.city_new,
        ca.state,
        ca.mapped_city, 
        ca.cx_payment_plan, 
        ca.city_tier, 
        ca.twin_fs_service, 
        ca.tsa_service, 
        ca.lab_service,
        cp.mobileNumber,
		axpii.workEmailId, 
        axpii.workPhoneNumber,
        axpii.homePhoneNumber,
        `c`.`dateAdded` AS `dateAdded`,
        `c`.`dateModified` AS `dateModified`,
        `c`.`status` AS `status`,
        `t`.`name` AS `cohortGroup`,
        `c`.`enrollmentDate` AS `enrollmentDate`,
        ITZ(`c`.`enrollmentDate`) AS `enrollmentDateITZ`,
        ITZ(`c`.`dateModified`) AS `dateModifiedITZ`,
        ITZ(`c`.`lastStatusChange`) AS `lastStatusChangeITZ`,
        `c`.`renewalVisitScheduled` AS `renewalVisitScheduledDate`,
        `c`.`dischargeReason` AS `dischargeReason`,
         c.dischargeReasonL1 as dischargeReasonL1,
         c.dischargeReasonL2 as dischargeReasonL2,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (TO_DAYS(ITZ(NOW())) - TO_DAYS(`c`.`enrollmentDate`))
            WHEN (`c`.`status` <> 'Active') THEN (TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`))
        END) AS `daysEnrolled`,
        (CASE
            WHEN (`c`.`status` = 'Active') THEN (FLOOR(((TO_DAYS(`c`.`termEndDate`) - TO_DAYS(`c`.`enrollmentDate`)) / 84)) - 1)
            WHEN (`c`.`status` = 'INACTIVE') THEN FLOOR(((TO_DAYS(`c`.`lastStatusChange`) - TO_DAYS(`c`.`enrollmentDate`)) / 84))
        END) AS `renewalCount`,
        `c`.`firstDiabetesInReversalTime` AS `firstDiabetesInReversalTime`,
        `c`.`termEndDate` AS `termEndDate`,
        `c`.`planCode` AS `planCode`,
        c.labels,
        c.sourceType as source,
        c.timezoneId,
        ca.m2planCode,
        ca.preferredM2Plan,
        case when cx.intervalUnit = 'YEARS' then 'Annual' 
        	 when cx.intervalUnit = 'MONTHS' and cx.paymentInterval = 3 then 'Quarterly'
        	 when cx.intervalUnit = 'MONTHS' and cx.paymentInterval <> 3 then 'Monthly'
        	 else cx.intervalUnit end as subscriptionType
        FROM
        ((((`twins`.`clients` `c`
        JOIN `twinspii`.`clientspii` `cp` ON ((`c`.`ID` = `cp`.`ID`)))
        JOIN `twins`.`doctors` `d` ON ((`c`.`doctorId` = `d`.`ID`)))
        LEFT JOIN `twins`.`coaches` `h` ON ((`c`.`coachId` = `h`.`ID`)))
        LEFT JOIN twinspii.userspii up on h.leadCoachID = up.id
		LEFT JOIN twinspii.userspii mind on d.morningInternalDoctorId=mind.id -- morning internal doctorid
		LEFT JOIN twinspii.userspii eved ON d.eveningInternalDoctorId = eved.id -- evening internal doctorid
        LEFT JOIN `twins`.`cohorts` `t` ON ((`c`.`cohortId` = `t`.`cohortId`))
		LEFT JOIN (
			    select a.id, LTRIM(RTRIM(UPPER(ap.city))) as city, up3.id as userId, 
                UPPER(CONCAT(up1.firstname, ' ', up1.lastname)) as pha, 
                UPPER(CONCAT(up1.firstname, ' ', LEFT(up1.lastname,1))) as phaShort,
                UPPER(CONCAT(up2.firstname, ' ', up2.lastname)) as poc,
                up2.dateadded as pocDateAdded,
                UPPER(CONCAT(up4.firstname, ' ', up4.lastname)) as RPA,
				UPPER(CONCAT(up5.firstname, ' ', up5.lastname)) as TSA,
				UPPER(CONCAT(up7.firstname, ' ', up7.lastname)) as LeadTSA,
				UPPER(CONCAT(up8.firstname, ' ', up8.lastname)) as healthCoordinator,
				

				trim(concat(ifnull(ap.line1,''),' ',ifnull(ap.line2,''),' ',ifnull(ap.city,''),ifnull(concat('-',ap.postalCode),''))) as address,
                ap.postalCode,
                UPPER(ifnull(pc.divisionName,'')) as city_new,
                UPPER(ifnull(pc.circleName,'')) as state,
                pc.mapped_city, pc.cx_payment_Plan, pc.city_tier, ifnull(pc.twin_fs_service,'No') as twin_fs_service, ifnull(pc.tsa_service, 'No') as tsa_service, ifnull(pc.lab_service, 'No') as lab_service,
                ca.m2planCode,
                ca.preferredM2Plan
			    from clients a 	left join twins.clientauxfields ca on a.id = ca.id
								left join twinspii.userspii up1 on ca.counselorUserId = up1.id
								left join twinspii.userspii up2 on ca.coordinatorUserId = up2.id
                                left join twinspii.userspii up3 on a.id = up3.id
                                left join twinspii.userspii up4 on ca.retainedCounselorUserId = up4.id
                                left join twinspii.userspii up5 on ca.twinServiceAssistantUserId = up5.id
                                left join twins.users up6 on ca.twinServiceAssistantUserId = up6.id
                                left join twinspii.userspii up7 on up6.leadTwinServiceAssistantUserId = up7.id
                                LEFT JOIN twinspii.userspii up8 ON ca.healthCoordinatorId = up8.id

								LEFT JOIN twinspii.addressespii ap on a.addressId = ap.ID
                                left join (select distinct pincode, divisionName, circleName, mapped_city,cx_payment_Plan,city_tier,twin_fs_service,tsa_service, lab_service from bi_address_lookup_new where deliveryStatus = 'Delivery') pc on ltrim(rtrim(replace(ap.postalCode,' ',''))) = pc.pinCode
                where a.deleted is false
                and test is false
        ) ca on c.id = ca.id
		LEFT JOIN twinspii.clientauxfieldspii axpii on c.id = axpii.id
		left join clientauxfields cx on c.id = cx.id
        )
    WHERE
        ((`c`.`status` IN ('ACTIVE' , 'REGISTRATION', 'INACTIVE', 'DISCHARGED', 'PENDING_ACTIVE', 'PROSPECT', 'ON_HOLD'))
            AND (`c`.`deleted` = 0)
            AND (`d`.`test` = FALSE)
            AND (`c`.`ID` NOT IN (15544 , 15558, 15593, 15594, 15596, 15680))
            AND (NOT ((`cp`.`firstName` LIKE '%TEST%')))
            AND (NOT ((`cp`.`lastName` LIKE '%TEST%'))))
   -- ORDER BY `c`.`enrollmentDate`
    ;
    
