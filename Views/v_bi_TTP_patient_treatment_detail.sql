alter view v_bi_TTP_patient_treatment_detail as 
select a.clientid, 
ct.patientnameShort as patientname, 
ct.status as current_status, 
ct.doctornameShort, 
ct.phaShort as pha,
ct.coachNameShort,
ct.city, 
ct.city_new,
b.durationYears_diabetes, 
ct.enrollmentdateitz,
ct.plancode, 
b.treatmentdays as daysActive,
a.active_status_start_date as treatment_start_date,
b.start_medicine_diabetes_drugs, 
length(b.start_medicine_diabetes_drugs) - length(replace(b.start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count,
b.start_medicine_diabetes,
b.start_weight,
b.start_glucose,
cast((b.start_labA1C * 28.1) - 46.7 as decimal(10,2)) as start_glucose_from_labA1C,
b.start_labA1C,
b.start_symptoms,
s.treatmentdays, 
s.medicine_drugs, 
length(s.medicine_drugs) - length(replace(s.medicine_drugs,',','')) + 1 as medicine_count,
s.medicine,
s.cgm_5d,
s.eA1C_5dg,
s.energy_1d, 
s.weight_1d,
s.nut_adh_syntax,
imt.total_incidents,
s1.energy_1d as last_available_energy, 
s2.weight_1d as last_available_weight,
MH_symptoms,
medicine_nameUntouched,
length(s.medicine_nameUntouched) - length(replace(s.medicine_nameUntouched,',','')) + 1 as medicine_count_from_name

from 
bi_ttp_enrollments a left join dim_client b on a.clientid = b.clientid and b.is_row_current = 'y'
					 inner join v_allClient_list ct on a.clientid = ct.clientid
					 inner join 
					(
						select a.clientid, treatmentdays, date, medicine_drugs, medicine, cgm_5d, eA1C_5dg, energy_1d, weight_1d, nut_adh_syntax, MH_symptoms, medicine_nameUntouched
						from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments b on a.clientid = b.clientid 
						where a.treatmentdays between 1 and 10 
						-- and b.is_patient_converted is null
					) s on a.clientid = s.clientid
                    left join (select clientid, count(distinct incidentId) as total_incidents from bi_incident_mgmt where status = 'OPEN' and category not in ('approval') group by clientid) imt on a.clientid = imt.clientid
					left join ( -- to get last available 1d energy in the 1st 10 treatmentdays
								select s.clientid, s.last_available_energy_date, s1.energy_1d
								from 
								(
									select a.clientid, max(case when energy_1d is not null then date end) as last_available_energy_date
									from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments b on a.clientid = b.clientid 
									where a.treatmentdays between 1 and 10 
									-- and b.is_patient_converted is null
									group by a.clientid 
								) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s1.date = s.last_available_energy_date
                        ) s1 on ct.clientid = s1.clientid 
					left join (-- to get last available 1d weight in the 1st 10 treatmentdays
								select s.clientid, s.last_available_weight_date, s1.weight_1d
								from 
								(
									select a.clientid, max(case when weight_1d is not null then date end) as last_available_weight_date
									from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments b on a.clientid = b.clientid 
									where a.treatmentdays between 1 and 10 
									-- and b.is_patient_converted is null
									group by a.clientid 
								) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s1.date = s.last_available_weight_date
                        ) s2 on ct.clientid = s2.clientid 
where -- a.is_patient_converted is null
ct.status in ('active','discharged','inactive')
and ct.patientname not like '%obsolete%'
and ct.labels not like '%TWIN_PRIME%' 
and a.active_status_start_date is not null
;

