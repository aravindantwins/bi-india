alter view v_bi_M1_M2_d10_journey_summary as 
select s.clientid, 
patient, 
s.patient_category as category,
s.coach, 
plancode,
city,
mobilenumber,
s1.treatmentday, 
s1.treatmentdayInNumber,
s1.date, 
s2.date as d10_review_date, 
start_glucose, 
s2.cgm_5d as d10_cgm_5d, 

start_medicine_count,
ifnull(s2.medCount,0) as d10_med_count, 

start_symptoms_count,
ifnull(length(s2.MH_symptoms) - length(replace(s2.MH_symptoms,',','')) + 1, 0) as d10_symptoms_count,

start_weight, 
s2.weight_1d as d10_weight,

s1.cgm_5d as current_cgm_5d, 
s1.cgm_1d as current_cgm_1d,
s1.cgm_2d as cgm_2d,
ifnull(s1.medCount,0) as current_med_count,

ifnull(length(s1.MH_symptoms) - length(replace(s1.MH_symptoms,',','')) + 1, 0) as current_symptoms_count,

s1.weight_1d as current_weight,

s1.AS_5d as current_AS_5d,
s1.AS_1d as current_AS_1d,
s1.IMT_outstanding as current_IMT_outstanding, 
s1.nut_adh_syntax_5d as current_nut_adh_5d,
s1.nut_adh_syntax as current_nut_adh_1d, 

s1.coach_rating as current_coach_rating_1d,
s2.AS_5d as d10_AS_5d,
s2.IMT_outstanding as d10_IMT_outstanding, 
s2.nut_adh_syntax_5d as d10_nut_adh_5d,

if(s3.clientid is not null, 'Yes', 'No') as isDisnerollmentOpen
from 
(
	select a.clientid, b.patientname as patient, 
    case when c.clientid is null then 'M2'
		 when c.clientid is not null and is_patient_converted = 'yes' then 'M1-M2'
		 when c.clientid is not null and is_patient_converted is null then 'M1'
     end as patient_category,
    b.coachnameShort as coach, b.city, b.mobilenumber, a.treatmentdays, 
    start_weight, cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose, start_labA1C, 
    replace(replace(start_medicine_diabetes_drugs,'(DIABETES)',''), '(INSULIN)','') as start_diabetes_medicines_drugs, 
    length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count, 
    start_symptoms, b.plancode, 
    length(start_symptoms) - length(replace(start_symptoms,',','')) + 1 as start_symptoms_count
	from dim_client a inner join v_allClient_list b on a.clientid = b.clientid 
					  left join bi_ttp_enrollments c on a.clientid = c.clientid 
	where a.is_row_current = 'y'
	and b.patientname not like '%obsolete%'
    and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
    and b.status = 'active'
) s 
inner join 
	(
			select a.clientid,
            concat('D',(a.treatmentdays - 1)) as treatmentday, (a.treatmentdays - 1) as treatmentdayInNumber, a.treatmentdays, a.date, a.medicine_drugs, a.medicine, a.actual_medCount as medCount,
			a.cgm_1d, a.cgm_5d, a.energy_1d, a.weight_1d, a.nut_adh_syntax, a.nut_adh_syntax_5d, a.AS_1d, a.AS_5d, a.BMI, a.coach_rating, a.IMT_opened, a.IMT_resolved, a.IMT_opened - a.IMT_resolved as IMT_outstanding,
            a.isMedRecommendationAvailable, a.isMedRecommendationAdopted, a.isMedRecommendationAdopted24hrs, a.total_delight_moments, a.MH_symptoms, a.med_adh,
            cast(avg(a1.cgm_1d) as decimal(10,2)) as cgm_2d -- joining the same table again to get cgm_2d.
            from bi_patient_reversal_state_gmi_x_date a 
														inner join bi_patient_reversal_state_gmi_x_date a1 on a.clientid = a1.clientid and a1.date >= date_sub(if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now()))), interval 1 day)
            where a.date =  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))  -- date_sub(date(itz(now())), interval 1 day)
            and a.treatmentdays <= 11
            group by a.clientid
	) s1 on s.clientid = s1.clientid 
    left join 
	(
			select a.clientid, a.date, concat('D',(treatmentdays - 1)) as treatmentday, (treatmentdays - 1) as treatmentdayInNumber, actual_medCount as medCount,
			cgm_5d, weight_1d, MH_symptoms, nut_adh_syntax_5d, AS_5d, IMT_opened - IMT_resolved as IMT_outstanding, med_adh
			from bi_patient_reversal_state_gmi_x_date a 
            where treatmentdays = 11
			-- and c.is_patient_converted is null
	) s2 on s.clientid = s2.clientid 
    left join 
    (
			select distinct clientid, max(date(itz(report_time))) as openDate, max(date(itz(resolved_time))) as resolvedDate 
            from bi_incident_mgmt where category = 'Disenrollment' 
			group by clientid
    ) s3 on s.clientid = s3.clientid and s1.date between opendate and ifnull(resolvedDate,date(now()))
;


