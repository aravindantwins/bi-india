set collation_connection = 'latin1_swedish_ci';

	create view v_bi_clinicalOPS_IMT_tracker as 
	select distinct incidentId, a.clientid, val.morningInternalDoctorName as twinDoctor, val.doctorNameShort as primaryDoctor, category, symptom_severity, a.status as incidentStatus, subtype_L1, incident_classification, 
	case when category = 'SYMPTOMS_HANDLING' then null 
		 when category = 'patient_treatment' and incident_classification like '%NON_CRITICAL%' then 'LOW' 
		 else 'HIGH' end as PT_severity,
	itz(report_time) as report_time,
    date(itz(report_time)) as report_date,
    if(a.status = 'OPEN', itz(now()), itz(resolved_time)) as resolved_time, 
    itz(owner_entry_date) as owner_entry_date, 
	itz(first_response_time) as first_response_time, 
	address_time_hours, 
	close_duration_hours,
	c.suspended,

	case when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'HIGH') or (category = 'PATIENT_TREATMENT' and incident_classification not like '%NON_CRITICAL%')) and a.status='RESOLVED' and close_duration_hours < 1 then 'Yes' 
  		 when (category = 'SYMPTOMS_HANDLING' and symptom_severity = 'MEDIUM') and a.status='RESOLVED' and close_duration_hours < 4 then 'Yes' 
		 when ((category = 'SYMPTOMS_HANDLING' and symptom_severity = 'LOW') or (category = 'PATIENT_TREATMENT' and incident_classification like '%NON_CRITICAL%')) and a.status='RESOLVED' and close_duration_hours < 24 then 'Yes' 
		 else null end as isIncidentResolvedWithinSLA

	
	
	from bi_incident_mgmt a left join clientauxfields c on a.clientid = c.id 
							left join v_allclient_list val on a.clientid = val.clientid 
	where category in ('symptoms_handling','patient_treatment') 
	and date(report_time) >= curdate() - interval 1 month
;





