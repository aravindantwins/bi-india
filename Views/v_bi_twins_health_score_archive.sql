create view v_bi_twins_health_score_archive
as 
select clientid, patientname, mealdate, macro_final_score, micro_final_score, biota_final_score, 
(ifnull(macro_final_score,0) + ifnull(micro_final_score,0) + ifnull(biota_final_score,0)) as nutrition_score
from 
(
	select s.clientid, c.patientname, mealdate, macro_final_score, 
	micro_final_score,	biota_final_score
	from 
    bi_twins_health_score_summary s inner join v_client_tmp c on s.clientid = c.clientid
	where mealdate >= date_sub(date(itz(now())), interval 3 month)
	group by clientid, mealdate
) s1
;
