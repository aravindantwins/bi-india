alter view v_bi_patient_delight_moments_d10 as 
select s.clientid, s1.patientname, s.category, s.date as date_achieved, concat('D',(s.treatmentdays - 1)) as treatmentday
from 
(
	select s.clientid, 'First 1dg dropped by 50 points' as category, s1.date, s1.treatmentdays
	from 
	(
		select clientid, min(date) as first_1d_dropped_by_50_points
		from bi_patient_reversal_state_gmi_x_date 
		where  cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) - cgm_1d >= 50
		group by clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_1d_dropped_by_50_points = s1.date

	union all 

	select s.clientid, 'First 1dg below 140' as category, s1.date, s1.treatmentdays
	from 
	(
		select clientid, min(date) as first_1d_below_140
		from bi_patient_reversal_state_gmi_x_date 
		where  cgm_1d < 140
		group by clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_1d_below_140 = s1.date

/*
	union all 

	select s.clientid, 'First 5dg below 140' as category, s1.date, s1.treatmentdays
	from 
	(
		select clientid, min(date) as first_5d_below_140
		from bi_patient_reversal_state_gmi_x_date 
		where  cgm_5d < 140
		group by clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_5d_below_140 = s1.date
*/
	union all 

	select s.clientid, 'First Medicine Reduction' as category, s1.date, s1.treatmentdays
	from 
	(
		select clientid, min(date) as first_medicine_reduced
		from bi_patient_reversal_state_gmi_x_date 
		where is_MedReduced = 'Yes'
		group by clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_medicine_reduced = s1.date

	union all 

	select s.clientid, 'Medicine Stopped' as category, s1.date, s1.treatmentdays
	from 
	(
		select clientid, min(date) as first_medicine_stopped
		from bi_patient_reversal_state_gmi_x_date 
		where medicine is null
		group by clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_medicine_stopped = s1.date

	union all

	select s.clientid, 'Achieved Reversal + Metformin' as category, s1.date, s1.treatmentdays
	from 
	(
		select clientid, min(date) as first_InReversal_W_Metformin
		from bi_patient_reversal_state_gmi_x_date 
		where is_MetOnly_by_OPS = 'Yes'
		group by clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_InReversal_W_Metformin = s1.date

	union all

	select s.clientid, 'Achieved Reversal + No Meds' as category, s1.date, s1.treatmentdays
	from 
	(
		select clientid, min(date) as first_InReversal
		from bi_patient_reversal_state_gmi_x_date 
		where is_InReversal = 'Yes'
		group by clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_InReversal = s1.date

	union all

	select s.clientid, '2Kg weight reduction' as category, s1.date, s1.treatmentdays
	from 
	(
		select a.clientid, min(date) as first_WeightReduced
		from bi_patient_reversal_state_gmi_x_date a inner join dim_client b on a.clientid = b.clientid 
		where is_row_Current = 'y'
		and b.start_weight - weight_1d >= 2
		group by a.clientid 
	) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_WeightReduced = s1.date
) s inner join v_active_clients s1 on s.clientid = s1.clientid 
where s.treatmentdays between 1 and 11
and s1.labels not like '%TWIN_PRIME%'
and s1.patientname not like '%OBSOLETE%'
and date(s1.enrollmentdateitz) >= date_sub(date(itz(now())), interval 3 month)
;