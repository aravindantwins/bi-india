alter view v_bi_CBG_patients_friction_detail as 
select distinct s.clientid, coach, leadCoach, inc.category, count(distinct incidentId) as totalIncidents, min(inc.reportDate) as firstIncidentOpenDate, min(inc.resolvedDate) as firstIncidentResolvedDate
from (	
			select a.clientid, b.patientname, coachnameShort as coach, leadCoachNameShort as leadCoach, plancode, a.treatmentdays
            email, mobilenumber, city, date, treatmentdays, as_1d, as_5d, isCBGEligible, isCBG, is_MetOnly_by_OPS, is_MetOnly, 
            if(b.labels like '%prime%' or b.labels like '%control%', 'Yes', 'No') as isRCT
			from bi_patient_reversal_state_gmi_x_date a left join v_Client_tmp b on a.clientid = b.clientid 
			where date =  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now()))) 
            and isCBG = 'Y'
) s inner join 
(
	select a.clientid, date(itz(report_time)) as reportDate, date(itz(resolved_time)) as resolvedDate, category, incidentID, status
	from bi_incident_mgmt a inner join predictedCGMCycles b on a.clientid = b.clientid and date(itz(report_time)) between b.startdate and b.enddate
	where category in ('NUTRITION', 'INVENTORY', 'SENSORS', 'TWINS_PRODUCT', 'PATIENT_TREATMENT') 
) inc on s.clientid = inc.clientid and s.date between reportDate and ifnull(resolvedDate,date(itz(now()))) 
group by s.clientid, inc.category
;

