CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `aravindan`@`%` 
    SQL SECURITY DEFINER
VIEW `v_bi_overall_ops_business_measure` AS
    SELECT 
        `bi_overall_measure`.`report_date` AS `date`,
        `bi_overall_measure`.`category` AS `category`,
        `bi_overall_measure`.`measure_name` AS `measure_name`,
        `bi_overall_measure`.`measure_group` AS `measure_group`,
        `bi_overall_measure`.`measure` AS `measure`
    FROM
        `bi_overall_measure`
    WHERE
        (`bi_overall_measure`.`measure_name` NOT IN ('SNS ADH' , 'SNS COM'))