create view v_bi_clinicalOPS_medpublish_tracker as 
select f1.clientid, recommendationDate, actionedDate, f1.status, case when f1.status in ('Manual', 'OVERRIDDEN') and actualMedication = 'No medicine' and recommendedMedication = 'No medicine' then 'Yes' else null end as changeType_Psuedo,dc.isRCT,
case when f1.recommendationdate <> f2.newpublishingdate then 'Repeated'
when f1.recommendationdate = f2.newpublishingdate then 'New' else null end as publishStatus, actionTime, 
case when f1.recommendationdate = f2.newpublishingdate then timestampdiff(HOUR, x.doctorAppointment_scheduled_date, x.first_medicine_published_date) else null end as action_Time_for_New,
medicationTierID, actualMedication, recommendedMedication, 
tc.labels, 
tc.cohortgroup,
case when f1.status = 'Automated' and actionTime <= 24 then 'Yes' else 'No' end as isAutomatedWithin24hrs,
case when f1.status in ('SKIPPED', 'OVERRIDDEN','SKIPPED_OVERRIDDEN') and actionTime <= 24 then 'Yes' else 'No' end as isNotAutomatedWithin24hrs,
case when f1.status = 'Automated' and actionTime > 48 then 'Yes' else 'No' end as isAutomatedAfter48hrs,
case when f1.status in ('SKIPPED', 'OVERRIDDEN','SKIPPED_OVERRIDDEN') and actionTime > 48 then 'Yes' else 'No' end as isNotAutomatedAfter48hrs,
changeReason, overrideReasons, if(tc.labels LIKE '%Exclude%', 'Y', 'N') as Exclude, if(tc.labels LIKE '%TEMP%', 'Y', 'N') as Temp_Discharge,
x.doctorAppointment_scheduled_date, x.doctorAppointment_status,
x.twinDoctorD0AppointmentTime, x.twinDoctorD0Appointmentstatus,
r.last_cgm_5d_date, r.last_available_cgm_5d, r.last_cgm_1d_date, r.last_available_cgm_1d
from 
(
	select distinct a.clientid,
	date(recommendationdate) as recommendationdate,actionedDate,
	status, actionTime,
	medicationTierID, actualMedication, recommendedMedication, changeReason, overrideReasons
	from
	(
		select a.clientid, a.medicationTierId, eventtime as recommendationdate, status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime, b.actualMedication, b.recommendedMedication, b.changeReason, b.overrideReasons 
		from
		(
			select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId, type
			from medicationrecommendations a
			where type = 'DIABETES'
		) a inner join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
	) a
	where date(recommendationdate) >= date_sub(date(itz(now())), interval 1 month)
) f1 left join (select clientid, min(date(itz(eventtime))) as newpublishingdate from medicationrecommendations where type = 'DIABETES' group by clientid) f2 on f1.clientid = f2.clientid
left join dim_client dc on f1.clientid = dc.clientid and dc.is_row_current = 'Y'
left join v_client_tmp tc on f1.clientid= tc.clientid
left join
(
		select c.clientid, min(itz(c.eventTime)) as First_DC_ScheduledTime from clientappointments c inner join doctorappointments d on c.clientAppointmentId=d.clientAppointmentId where c.type='Doctor_Appointment'
		and c.status in ('COMPLETED', 'SCHEDULED')
		group by c.clientid
)s on f1.clientid=s.clientid
left join bi_patient_initial_setup_x_dates x on f1.clientid= x.clientid
left join 
(
	select a.clientid, a.last_cgm_5d_date, a.last_cgm_1d_date, 
	max(case when a.last_cgm_5d_date = b.date then cgm_5d else null end) as last_available_cgm_5d, 
	max(case when a.last_cgm_1d_date = b.date then cgm_1d else null end) as last_available_cgm_1d
	from 
	(
		select clientid,
		max(case when cgm_5d is not null then date else null end) as last_cgm_5d_date, 
		max(case when cgm_1d is not null then date else null end) as last_cgm_1d_date
		
		from bi_patient_reversal_state_gmi_x_date bprsgxd 
		group by clientid 
	) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and (a.last_cgm_5d_date = b.date or a.last_cgm_1d_date = b.date)
	group by a.clientid, a.last_cgm_5d_date, a.last_cgm_1d_date
) r on f1.clientid = r.clientid 
;