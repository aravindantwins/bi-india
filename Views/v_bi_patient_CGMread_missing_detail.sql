alter view v_bi_patient_CGMread_missing_detail as 
select a.clientid, b.patientname, b.coachnameShort as coach, leadCoachNameShort as leadCoach, b.plancode, 
email, mobilenumber, city, datediff(a.date, last_cgm_1d_date) as lastCGMarrivedBefore, last_cgm_1d_date
from bi_patient_reversal_state_gmi_x_date a inner join v_client_tmp b on a.clientid = b.clientid 
											left join (
															select clientid, max(case when cgm_1d is not null then date else null end) as last_cgm_1d_date
															from bi_patient_reversal_state_gmi_x_date
															group by clientid 
													  ) cgm on a.clientid = cgm.clientid
where isCBG = 'N'
and date =  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))
and a.clientid not in (select clientid from bi_incident_mgmt where category = 'disenrollment' and status = 'open') 
and b.status = 'ACTIVE'
;

