alter view v_bi_CBG_patient_detail as 
select a.clientid, ct.patientname, e.treatmentDays, ct.coachnameShort as coach, ct.city, a.date, a.startdate as cycleStartDate, e.sleep_duration_minutes as totalSleepMinutes, c.total_steps, d.macro, d.micro, d.biota, e.predicted_1d_cgm, 
datediff(a.date, a.startdate) as daysFromCycleStart
from 
(
	select b.clientid, date, startdate
	from v_date_to_date a inner join predictedcgmcycles b on a.date between b.startdate and b.enddate
						  inner join bi_patient_status c on a.date between c.status_start_date and c.status_end_date and c.status = 'active' and b.clientid = c.clientid
	where date >= date_sub(date(itz(now())), interval 14 day)
) a -- left join bi_patient_monitor_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'TotalMinutes' -- TotalMinutes, DeepMinutes,awakeMinutes,  RemMinutes 
	left join bi_patient_daily_steps_summary c on a.clientid = c.clientid and a.date = eventdate
    left join nutritionScores d on a.clientid = d.clientid and a.date = d.date
    left join bi_patient_reversal_state_gmi_x_date e on a.clientid = e.clientid and a.date = e.date 
    left join v_client_tmp ct on a.clientid = ct.clientid
;
