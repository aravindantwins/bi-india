create view v_bi_netadds_x_deals
as
select 
deal_id,
contact_name,
person_id,
ifnull(patient_id,'') as patient_id,
if(date(enrollment_date) = '0000-00-00','',enrollment_date) as enrollment_date,
case when deal_stage = 1 then 'Contract secured'
	 when deal_stage = 2 then 'Interested'
	 when deal_stage = 5 then 'Enrolled'
	 when deal_stage = 6 then 'Scheduled'
	 when deal_stage = 8 then 'Visited Clinic'
	 when deal_stage = 10 then 'Diabetic'
	 when deal_stage = 11 then 'Renewal'
	else 'N/A' end as deal_stage,
orgnaization as organization,
owner,
deal_status, 
if(date(deal_created_on)='0000-00-00','',deal_created_on) as deal_created_on,
if(date(deal_closed_on)='0000-00-00','',deal_closed_on) as deal_closed_on,
if(date(expected_close_date)='0000-00-00','',expected_close_date) as expected_close_date,
if(date(first_contact_date)='0000-00-00','',first_contact_date) as first_contact_date,
if(date(last_activity_date)='0000-00-00','',last_activity_date) as last_activity_date,
if(date(update_time)='0000-00-00','',update_time) as update_time,
case when visit_type = 15 then 'Appointment'
	 when visit_type = 16 then 'Walk-IN'
	 when visit_type = 17 then 'PHONE CALL'
     else ''
     end as visit_type,
case when registered = 11 then 'Yes'
	 when registered = 12 then 'No'
	 when registered = 13 then 'NOT YET'
	 when registered = 14 then 'COMING ON NEXT WEEK'
	 else ''
     end as registered, 
if(date(clinic_visit_date)='0000-00-00','',clinic_visit_date) as clinic_visit_date,
if(date(declined_date)='0000-00-00','',declined_date) as declined_date,
case when patient_type = 27 then 'New'
	 when patient_type = 28 then 'Converted'
     else ''
     end as patient_type
from bi_netadds_deal
;
