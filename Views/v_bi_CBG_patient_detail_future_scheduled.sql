alter view v_bi_CBG_patient_detail_future_scheduled as 
select a.clientid, patientname, coach, leadCoach, date, startDate as scheduledStartDate
from (
            select s.clientid, patientname, coachnameShort as coach, leadCoachNameShort as leadCoach, s.date, s.treatmentdays, as_1d, as_5d, isCBGEligible, isCBG, is_MetOnly_by_OPS, is_MetOnly, if(b.labels like '%prime%' or b.labels like '%control%' or b.labels like '%TWIN_HTN%', 'Yes', 'No') as isRCT
			from 
			(
					select a.date, b.clientId, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
					from twins.v_date_to_date a
					inner join
					(
						select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
						from
						(
							select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
							from bi_patient_status
							where status = 'active'
							group by clientid, date(visitdateitz)
						) s
					) b on a.date between b.startDate and b.endDate
					where a.date >= date_sub(date(itz(now())), interval 7 day)
            ) s left join bi_patient_reversal_state_gmi_x_date a on s.clientid = a.clientid and s.date = a.date 
				left join v_Client_tmp b on s.clientid = b.clientid 
			where s.treatmentdays > 35
      ) a left join (select clientid, startDate from predictedcgmcycles where startdate > date(itz(now()))) fpc on a.clientid = fpc.clientid 
where treatmentdays > 35 and isCBGEligible = 'Y' and isCBG = 'N' and fpc.clientid is not null
;
