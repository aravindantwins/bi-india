create view v_bi_patient_overall_actual_x_prediction as 
select 
a.clientid, 
ct.patientname,
c.totalCount as total_logged_overall,
count(a.mealid) as total_logged, 
count(a.predictionID) as total_predicted, 
cast(avg(measuredAUC) as decimal(10,2)) as MeanActualAUC, 
cast(avg(predictedAUC) as decimal(10,2)) as MeanPredictedAUC,
count(case when measuredAUC <= 70 then 1 else null end) as count_measured_auc_less_than_threshold,
count(case when predictedAUC <= 70 then 1 else null end) as count_predicted_auc_less_than_threshold,
count(case when measuredAUC <= 100 and predictedAUC <= 70 then 1 else null end) as count_precision, 
count(case when measuredAUC <= 70 and predictedAUC <= 100 then 1 else null end) as count_recall
from 
(
			select category, clientid, mealid, mealdate, mealtype, measuredAUC, predictionID, predictedAUC           
            from bi_GFY_actual_x_prediction_auc_detail
) a	left join (
						select clientid, count(mealtype) as totalCount				
						from foodlogs 
						where mealid is not null 
						group by clientid
			   ) c on a.clientid = c.clientid
	inner join v_Client_Tmp ct on a.clientid = ct.clientid
group by a.clientid