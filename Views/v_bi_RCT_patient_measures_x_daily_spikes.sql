alter view v_bi_RCT_patient_measures_x_daily_spikes as 
select distinct a.clientid, a.mealdate as date, c.spikeId, valleyToEndAUC
from foodlogs a inner join foodlogspikes b on a.clientid = b.clientid and a.mealdate = b.mealdate and a.foodlogid = b.foodlogid and a.foodid = b.foodid
				inner join spikes c on a.clientid = c.clientid and b.spikeid = c.spikeid
				inner join foods d on a.foodid = d.foodid 
				inner join v_allClients_RCT rct on a.clientid = rct.clientid 
;

