set collation_connection = 'latin1_swedish_ci'; 

alter view v_bi_coachOPS_scoreboard_D1_D35_Performance as 
select s.date, s.coachId, s.coach, s.leadCoach,
count(distinct case when s.treatmentDays = 6 then s.clientid else null end) as total_treatmentDay5,
count(distinct case when s.treatmentDays = 6 and s1.nut_adh_syntax_5d >= 0.8 then s.clientid else null end ) as total_treatmentDay5_nut_adh_yes,
count(distinct case when s.treatmentDays = 6 and s1.as_5d > 80 then s.clientid else null end ) as total_treatmentDay5_as_5d_80above,

count(distinct case when s.treatmentDays = 11 then s.clientid else null end ) as total_treatmentDay10,
count(distinct case when s.treatmentDays = 11 and s1.nut_adh_syntax_5d >= 0.8 then s.clientid else null end ) as total_treatmentDay10_nut_adh_yes,
count(distinct case when s.treatmentDays = 11 and s1.as_5d > 80 then s.clientid else null end ) as total_treatmentDay10_as_5d_80above,


count(distinct case when s.treatmentDays = 26 then s.clientid else null end ) as total_treatmentDay25,
count(distinct case when s.treatmentDays = 26 and s1.nut_adh_syntax_5d >= 0.8 then s.clientid else null end) as total_treatmentDay25_nut_adh_yes,
count(distinct case when s.treatmentDays = 26 and s1.as_5d > 80 then s.clientid else null end ) as total_treatmentDay25_as_5d_80above,

count(distinct case when s.treatmentDays = 36 then s.clientid else null end ) as total_treatmentDay35,
count(distinct case when s.treatmentDays = 36 and s1.nut_adh_syntax_5d >= 0.8 then s.clientid else null end ) as total_treatmentDay35_nut_adh_yes,
count(distinct case when s.treatmentDays = 36 and s1.as_5d > 80 then s.clientid else null end ) as total_treatmentDay35_as_5d_80above,

count(distinct case when s1.coach_rating < 4 then s.clientid else null end) as total_members_with_coach_rating_4
from 
(
					select a.date, b.clientId, coachId, leadCoach, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays 
					from twins.v_date_to_date a
					inner join
					(
						select s.clientid, b.coachId, b.leadCoachNameShort as leadCoach, b.coachNameShort as coach, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
						from
						(
							select a.clientid,  date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate -- b.coachId, b.leadCoachNameShort as leadCoach, b.coachNameShort as coach,
							from bi_patient_status a 
							where a.status = 'active'
							group by a.clientid, date(visitdateitz)
						) s inner join v_client_tmp b on s.clientid = b.clientid 
						where b.patientname not like '%obsolete%'
					) b on a.date between b.startDate and b.endDate
					where a.date >= date_sub(date(itz(now())), interval 30 day) and a.date <= date(itz(now()))
) s left join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.date = s1.date 
group by s.date, s.coachId, s.coach, s.leadCoach
;

					
							