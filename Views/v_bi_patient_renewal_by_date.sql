create view v_bi_patient_renewal_by_date
as
select date, clientid, patientname, doctorname, current_term_no - 1 as current_term_no, enrolled_days, 'R' as status_group, dischargeReason
from v_date_to_date a left join v_bi_patient_renewal n on a.date = date(renewed_date)
/*just excluse these 3 patients manually as they has renewal done for just as follow up, this will not be done in future*/
where clientid not in ('15562', '15529', '15662')

union all

select date, clientid, patientname, doctorname, current_term_no, enrolled_days, 'D' as status_group, dischargeReason
from v_date_to_date a left join 
(
				select clientid, patientname, doctorname, current_term_no, enrolled_days, dischargeReason, laststatuschange, status
				from v_bi_patient_renewal
				/* Logic imposed to ignore disenrolled patients in the different terms*/
				where clientid not in 
				(
					select clientid from v_client_tmp where status in ('inactive', 'discharged') and daysenrolled < 84
					union 
					select clientid from v_client_tmp where lastStatusChangeITZ < termenddate and status in ('inactive', 'discharged') and daysenrolled > 84 and datediff(termenddate, laststatuschangeitz) > 7
				)
) b on a.date = date(b.laststatuschange) and (b.status = 'discharged' or b.status = 'inactive')

union all

/*This portion is to include some of the disenrolled patients in the past who were simply renewed for follow up purpose
So business wants to combine them into the proper Decline list (i.e. even after extending for follow up, the client refused to renew). 
This kind of renewals are not allowed in the future but the portion below will only include the patients we know till date (May 27, 2019)
*/
select date(laststatuschange) as date, clientid, patientname, doctorname, 1 as current_term_no, enrolled_days, 'D' as status_group, dischargeReason
from v_bi_patient_renewal
where clientid in ('15562', '15529', '15662') and (status = 'discharged' or status = 'inactive')
