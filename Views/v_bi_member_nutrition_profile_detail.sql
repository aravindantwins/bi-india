create view v_bi_member_nutrition_profile_detail as 
select a.clientid, a.date, s.syntaxname, s.greenfoods, s.pstarFoods, s.portionControlledRedFoods, s.breakfast, s.lunch, s.dinner, s.timerestrictedEating 
from 
(
	select clientid, date 
	from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and status='active'
	where date >= date_sub(date(now()), interval 21 day)
) a left join 
(
				select s1.clientid, s1.syntaxname, s1.greenfoods as greenfoods, s1.pstarFoods as pstarFoods, s1.portionControlledRedFoods as portionControlledRedFoods, 
				s1.breakfast as breakfast, s1.lunch as lunch, s1.dinner as dinner, s1.timerestrictedEating as timerestrictedEating,  s1.currentManualSyntax, 
				s1.date as start_date, 
				ifnull(date_sub(s2.date, interval 1 day), date(itz(now()))) as end_date 
                from 
                    (
						select row_number() over (order by s1.clientid, s1.date) as row_num,clientid, s1.syntaxname, s1.greenfoods, s1.pstarFoods, s1.portionControlledRedFoods, s1.breakfast, s1.lunch, s1.dinner, s1.timerestrictedEating, s1.currentManualSyntax, date
						
						from 
						(	
								select a.clientid, ns.syntaxname, b.greenfoods, b.pstarFoods, b.portionControlledRedFoods, b.breakfast, b.lunch, b.dinner, b.timerestrictedEating, b.currentManualSyntax, date(itz(a.recentModified)) as date from 
								(
									select clientid, date(datemodified) as date, max(datemodified) as recentModified from nutritionprofiles_aud group by clientid, date(datemodified) 
								) a inner join nutritionprofiles_aud b on a.clientid = b.clientid and a.recentModified = b.datemodified
									inner join nutritionsyntax ns on b.nutritionSyntaxId = ns.nutritionSyntaxId
									inner join v_client_tmp c on a.clientid = c.clientid 
								
								union 
								   
								select a.clientid, ns.syntaxname, a.greenfoods, a.pstarFoods, a.portionControlledRedFoods, a.breakfast, a.lunch, a.dinner, a.timerestrictedEating, a.currentManualSyntax, date(itz(a.datemodified)) as date from 
								nutritionprofiles a inner join nutritionsyntax ns on a.nutritionSyntaxId = ns.nutritionSyntaxId
													inner join v_client_tmp b on a.clientid = b.clientid 
						 ) s1 
                     ) s1 left join 
                     (
								select row_number() over (order by s11.clientid, s11.date) as row_num, clientid, s11.syntaxname, s11.greenfoods, s11.pstarFoods, s11.portionControlledRedFoods, s11.breakfast, s11.lunch, s11.dinner, s11.timerestrictedEating, s11.currentManualSyntax, date 
								
								from 
								(	
										select a.clientid, ns.syntaxname, b.greenfoods, b.pstarFoods, b.portionControlledRedFoods, b.breakfast, b.lunch, b.dinner, b.timerestrictedEating, b.currentManualSyntax, date(itz(a.recentModified)) as date from 
										(
											select clientid, date(datemodified) as date, max(datemodified) as recentModified from nutritionprofiles_aud group by clientid, date(datemodified) 
										) a inner join nutritionprofiles_aud b on a.clientid = b.clientid and a.recentModified = b.datemodified
											inner join nutritionsyntax ns on b.nutritionSyntaxId = ns.nutritionSyntaxId
											inner join v_client_tmp c on a.clientid = c.clientid 
										
										union 
										   
										select a.clientid, ns.syntaxname, a.greenfoods, a.pstarFoods, a.portionControlledRedFoods, a.breakfast, a.lunch, a.dinner, a.timerestrictedEating, a.currentManualSyntax, date(itz(a.datemodified)) as date from 
										nutritionprofiles a inner join nutritionsyntax ns on a.nutritionSyntaxId = ns.nutritionSyntaxId
															inner join v_client_tmp b on a.clientid = b.clientid 
								 ) s11 
                     ) s2 on s1.clientid = s2.clientid and s2.row_num = s1.row_num + 1                    
) s  on a.clientid = s.clientid and a.date between s.start_date and s.end_date                
;

