create view v_bi_Member1_nonpayment_post_D12_detail as 
select s.clientid, s.patient, s.city, s.mobilenumber, s.plancode, s.active_status_start_date as Activation_date, s1.treatmentday, s1.treatmentdayInNumber, s1.date as treatmentday12,
    s2.last_CGM_1d_available
    from
    (
    select a.clientid, b.patientname as patient, b.coachnameShort as coach, b.city, b.mobilenumber, a.treatmentdays,
    b.plancode, c.active_status_start_date
    from dim_client a inner join v_allClient_list b on a.clientid = b.clientid
					  inner join bi_ttp_enrollments c on a.clientid = c.clientid
	where a.is_row_current = 'y'
	and b.patientname not like '%obsolete%'
    and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
	and c.is_patient_converted is null
    and b.status = 'active'
    ) s left join
    (
			select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, (treatmentdays - 1) as treatmentdayInNumber, treatmentdays, a.date
			from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid
            where treatmentdays = 13
            and c.is_patient_converted is null
	) s1 on s.clientid = s1.clientid
    left join
    (
			select a.clientid, max(case when cgm_1d is not null then date else null end) as last_CGM_1d_available
			from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid
			where c.is_patient_converted is null
			group by a.clientid
    ) s2 on s.clientid = s2.clientid
    where treatmentdayInNumber >= 12
;