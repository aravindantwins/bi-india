alter view v_foodlog_x_foodsubstitute
as

/* This section is only for the Red food */
SELECT 	   x.clientid,
		   x.mealDateITZ,
           x.mealTimeITZ,
           x.mealtype,
           x.foodlogid,
           x.foodid,
           x.foodlabel,
           x.foodname,
           x.measure,
           x.quantity,
           x.calories,
           x.fiber,
           x.protein,
           x.carb,
           x.netcarb,
           x.fat,
           x.customfoodlog, 
           x.foodlogged,
           x.dateaddedITZ,
           x.netGICarb,
           x.recommendationRating,
           x.E5foodgrading,
           x.foodpoint,
           x.E5Grade, 
           x.isE5Dish,
           x.points,
           x.hasReplacement,
           x.tcu,
           y.origfoodid, 
           y.substituteFoodId,
           y.foodLabel as subfoodLabel,
           Y.foodname as subfoodName, 
           Y.measure as subfood_measure,
           y.calories as subfood_calories,
           y.fibre as subfood_fibre,
           y.protein as subfood_protein,
           y.carb as subfood_carb,
           y.netcarb as subfood_netcarb,
           y.fat as subfood_fat,
           y.netGICarb as subfood_netGICarb,
           y.recommendationRating as subfood_recommendationRating,
           v.foodrank as food_rank
FROM 
(	
	SELECT 
			fl.clientid AS clientid,
			fl.foodlogid AS foodlogid,
			fl.foodid AS foodid,
			fl.quantity AS quantity,
			(f.calories * fl.quantity) AS calories,
			(f.fiber * fl.quantity) AS fiber,
			(f.protein * fl.quantity) AS protein,
			(f.carb * fl.quantity) AS carb,
			((f.carb - f.fiber) * fl.quantity) AS netCarb,
			(f.fat * fl.quantity) AS fat,
			fl.mealType AS mealType,
			fl.customFoodLog AS customFoodLog,
			COALESCE(fl.customFoodLog, f.foodLabel) AS foodLogged,
			itz(fl.dateAdded) AS dateAddedITZ,
			itz(fl.mealTime) AS mealTimeITZ,
			CAST(itz(fl.mealTime) AS DATE) AS mealDateITZ,
			f.foodLabel AS foodLabel,
			f.foodName AS foodName,
			IFNULL((((f.carb - f.fiber) * f.glycemicIndex) / 55),0) AS netGICarb,
			f.measure AS measure,
			f.recommendationRating AS recommendationRating,
			IFNULL(f.E5foodgrading, 'Purple') AS E5foodgrading,
			IFNULL(f.foodpoint, 0) AS foodpoint,
			IFNULL(f.E5Grade, 0) AS E5Grade,
			IFNULL(f.isE5Dish, 'No') AS isE5Dish,
			f.points AS points,
			IFNULL(f.hasReplacement, 'No') AS hasReplacement,
            f.tcu
		FROM
        twins.foodlogs fl
        inner JOIN (
					SELECT DISTINCT
							d.foodid AS foodId,
							d.foodName AS foodName,
							d.foodLabel AS foodLabel,
							d.calories AS calories,
							d.fibre AS fiber,
							d.glycemicIndex AS glycemicIndex,
							d.carb AS carb,
							d.fat AS fat,
							d.protein AS protein,
							d.recommendationRating AS recommendationRating,
							d.points AS points,
							d.measure AS measure,
							CASE WHEN ISNULL(d.recommendationRating) THEN 'Purple'
								  WHEN (d.recommendationRating <= 1) THEN 'Red'
								  WHEN ((d.recommendationRating > 1) AND (d.recommendationRating <= 2)) THEN 'Orange'
								  WHEN ((d.recommendationRating > 2) AND (d.recommendationRating <= 3)) THEN 'Green'
								  WHEN (d.recommendationRating > 3) THEN 'Green*'
								  END AS E5foodgrading,
							CASE WHEN (d.recommendationRating > 2) THEN 1 ELSE 0 END AS foodpoint,
							CASE WHEN ((d.recommendationRating > 2) AND (d.recommendationRating <= 4)) THEN 100 ELSE 0 END AS E5Grade,
							CASE WHEN (ISNULL(d.recipeLink) OR (d.recipeLink = '')) THEN 'No' ELSE 'Yes' END AS isE5Dish,
							CASE WHEN ISNULL(m.substituteFoodId) THEN 'No' ELSE 'Yes' END AS hasReplacement,
                            t.quantity as tcu
					FROM
					twins.foods d
					LEFT JOIN twins.foodsubstitutemappings m ON m.foodId = d.foodid
					left join (SELECT DISTINCT foodid, quantity
					FROM  defaultfoodplans t where vegetarian = 1 and quantity > 0 ) t on d.foodid = t.foodid
				) f ON fl.foodid = f.foodId and f.e5foodgrading = 'Red'
) X inner JOIN  
(
	SELECT 
        m.foodId AS origFoodId,
        o.foodLabel AS origFoodLabel,
        m.substituteFoodId AS substituteFoodId,
        f.calories AS calories,
        f.fibre AS fibre,
        f.protein AS protein,
        f.carb AS carb,
        (f.carb - f.fibre) AS netCarb,
        f.fat AS fat,
        f.foodLabel AS foodLabel,
        f.foodName AS foodName,
        NULLIF((((f.carb - f.fibre) * f.glycemicIndex) / 55),0) AS netGICarb,
        f.measure AS measure,
        f.recommendationRating AS recommendationRating
    FROM
        foodsubstitutemappings m
        JOIN foods f ON m.substituteFoodId = f.foodid
        JOIN foods o ON m.foodId = o.foodid
) Y on X.foodid = Y.origFoodId
left join bi_foodgrade_rank v on x.foodid = v.foodid and v.foodgrade = 'red'

union all 

/* This section is only for the Orange food */
SELECT 	   x.clientid,
		   x.mealDateITZ,
           x.mealTimeITZ,
           x.mealtype,
           x.foodlogid,
           x.foodid,
           x.foodlabel,
           x.foodname,
           x.measure,
           x.quantity,
           x.calories,
           x.fiber,
           x.protein,
           x.carb,
           x.netcarb,
           x.fat,
           x.customfoodlog, 
           x.foodlogged,
           x.dateaddedITZ,
           x.netGICarb,
           x.recommendationRating,
           x.E5foodgrading,
           x.foodpoint,
           x.E5Grade, 
           x.isE5Dish,
           x.points,
           x.hasReplacement,
           x.tcu,
           y.origfoodid, 
           y.substituteFoodId,
           y.foodLabel as subfoodLabel,
           Y.foodname as subfoodName, 
           Y.measure as subfood_measure,
           y.calories as subfood_calories,
           y.fibre as subfood_fibre,
           y.protein as subfood_protein,
           y.carb as subfood_carb,
           y.netcarb as subfood_netcarb,
           y.fat as subfood_fat,
           y.netGICarb as subfood_netGICarb,
           y.recommendationRating as subfood_recommendationRating,
           v.foodrank as food_rank
FROM 
(	
	SELECT 
			fl.clientid AS clientid,
			fl.foodlogid AS foodlogid,
			fl.foodid AS foodid,
			fl.quantity AS quantity,
			(f.calories * fl.quantity) AS calories,
			(f.fiber * fl.quantity) AS fiber,
			(f.protein * fl.quantity) AS protein,
			(f.carb * fl.quantity) AS carb,
			((f.carb - f.fiber) * fl.quantity) AS netCarb,
			(f.fat * fl.quantity) AS fat,
			fl.mealType AS mealType,
			fl.customFoodLog AS customFoodLog,
			COALESCE(fl.customFoodLog, f.foodLabel) AS foodLogged,
			itz(fl.dateAdded) AS dateAddedITZ,
			itz(fl.mealTime) AS mealTimeITZ,
			CAST(itz(fl.mealTime) AS DATE) AS mealDateITZ,
			f.foodLabel AS foodLabel,
			f.foodName AS foodName,
			IFNULL((((f.carb - f.fiber) * f.glycemicIndex) / 55),0) AS netGICarb,
			f.measure AS measure,
			f.recommendationRating AS recommendationRating,
			IFNULL(f.E5foodgrading, 'Purple') AS E5foodgrading,
			IFNULL(f.foodpoint, 0) AS foodpoint,
			IFNULL(f.E5Grade, 0) AS E5Grade,
			IFNULL(f.isE5Dish, 'No') AS isE5Dish,
			f.points AS points,
			IFNULL(f.hasReplacement, 'No') AS hasReplacement,
            tcu
		FROM
        twins.foodlogs fl
        inner JOIN (
					SELECT DISTINCT
							d.foodid AS foodId,
							d.foodName AS foodName,
							d.foodLabel AS foodLabel,
							d.calories AS calories,
							d.fibre AS fiber,
							d.glycemicIndex AS glycemicIndex,
							d.carb AS carb,
							d.fat AS fat,
							d.protein AS protein,
							d.recommendationRating AS recommendationRating,
							d.points AS points,
							d.measure AS measure,
							CASE WHEN ISNULL(d.recommendationRating) THEN 'Purple'
								  WHEN (d.recommendationRating <= 1) THEN 'Red'
								  WHEN ((d.recommendationRating > 1) AND (d.recommendationRating <= 2)) THEN 'Orange'
								  WHEN ((d.recommendationRating > 2) AND (d.recommendationRating <= 3)) THEN 'Green'
								  WHEN (d.recommendationRating > 3) THEN 'Green*'
								  END AS E5foodgrading,
							CASE WHEN (d.recommendationRating > 2) THEN 1 ELSE 0 END AS foodpoint,
							CASE WHEN ((d.recommendationRating > 2) AND (d.recommendationRating <= 4)) THEN 100 ELSE 0 END AS E5Grade,
							CASE WHEN (ISNULL(d.recipeLink) OR (d.recipeLink = '')) THEN 'No' ELSE 'Yes' END AS isE5Dish,
							CASE WHEN ISNULL(m.substituteFoodId) THEN 'No' ELSE 'Yes' END AS hasReplacement,
                            t.quantity as tcu
					FROM
					twins.foods d
					LEFT JOIN twins.foodsubstitutemappings m ON m.foodId = d.foodid
					left join (SELECT DISTINCT foodid, quantity
					FROM  defaultfoodplans t where vegetarian = 1 and quantity > 0 ) t on d.foodid = t.foodid
				) f ON fl.foodid = f.foodId and f.e5foodgrading = 'Orange'
) X inner JOIN  
(
	SELECT 
        m.foodId AS origFoodId,
        o.foodLabel AS origFoodLabel,
        m.substituteFoodId AS substituteFoodId,
        f.calories AS calories,
        f.fibre AS fibre,
        f.protein AS protein,
        f.carb AS carb,
        (f.carb - f.fibre) AS netCarb,
        f.fat AS fat,
        f.foodLabel AS foodLabel,
        f.foodName AS foodName,
        NULLIF((((f.carb - f.fibre) * f.glycemicIndex) / 55),0) AS netGICarb,
        f.measure AS measure,
        f.recommendationRating AS recommendationRating
    FROM
        foodsubstitutemappings m
        JOIN foods f ON m.substituteFoodId = f.foodid
        JOIN foods o ON m.foodId = o.foodid
) Y on X.foodid = Y.origFoodId
left join bi_foodgrade_rank v on x.foodid = v.foodid and v.foodgrade = 'Orange'

