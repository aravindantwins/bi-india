alter view v_bi_patient_daily_payment_Zoho as 
select a.cf_clientid_unformatted as clientid, c.patientname, c.email, c.city, b.number as inv_number, b.invoice_date, 
gateway_transaction_id, p.amount as amount_paid, p.date as payment_date, p.amount_refunded, p.payment_mode from 
importedbilling.zoho_customers a inner join importedbilling.zoho_invoices b on a.customer_id = b.customer_id
								 inner join importedbilling.zoho_payments p on b.invoice_id = p.invoice_id
								 inner join v_client_tmp c on a.cf_clientid_unformatted = c.clientid
where b.status = 'PAID' 
-- and date(a.updated_time) >= date_sub(date(itz(now())), interval 1 month)
;