set collation_connection = 'latin1_swedish_ci'; 

alter view v_bi_patient_reversal_management_detail_all_patients as 
select clientid, patient, coach, doctor, eveningDoctor, morningDoctor, leadCoach, enrollmentDateITZ, date, treatmentDays, daysFromConversionDate, 
enrollType, medCount, medicine_drugs, medicine, 
as_5d, nut_adh_5d, cohortname, isPatientIncluded, isPatientIncludedCommercial,
isM1_Converted,
cgm_5d,
is_InReversal,
is_MetOnly_by_OPS,
is_Escalation,
is_Relapse,
is_type1like,
is_medical_exclusion,
medical_exclusion_cohort, 
suspended,
-- isMedExclusion_to_include,
isUnqualified,
isNABrittle,
nonAdh_brittle_category,
isDiabeticMet,
daysFromLast5d,
isExcluded,
is_app_suspended,
isTTP,
totalEscalationDays
,totalReversalDays
,isInReversal_previous_day
,last_available_cgm_5d_date 
,last_available_cgm_5d
,is_TempCharge
,is_Disenroll_Open
,Disenroll_OpenTime
,syntaxName
,isCBG
,is_GlucosePrick_On
,is_GoldPro_member
,recentLabDate
,recentLabA1C
,invoiceDate
,invoicestatus
,IF(invoiceStatus = 'Overdue', datediff(date, invoiceDate), NULL) AS totalDaysDue
,is_CBGtoCGM_to_include
,is_Excluded_timetracked
,start_insulin_units
,current_insulin_units
,nut_adh_1d
,AS_1d
,last_available_ea1c_60d
,last_available_ea1c_60d_date
,start_labA1C
,first_available_cgm_1d
,first_available_cgm_1d_date
,first_available_cgm_5d
,first_available_cgm_5d_date
,start_homa2b
,last_homa2b
,start_BMI_bcm
,last_available_BMI
,start_eGFR
,latest_eGFR 
from 
(
	select clientid, patient, doctor, eveningDoctor, morningDoctor, coach, leadCoach,enrollmentDateITZ, date, treatmentDays, isDiabeticMet,daysFromLast5d, daysFromConversionDate, last_available_cgm_5d_date, last_available_cgm_5d,
	case when isM1_Converted = 'Yes' then 'M1-M2'
		 when isTTP = 'Yes' then 'M1'
         else 'M2' end as enrollType, medCount, ifnull(medicine_drugs,'') as medicine_drugs, ifnull(medicine,'') as medicine,suspended,
	as_5d,
    isTTP,
	nut_adh_syntax_5d as nut_adh_5d,
    cgm_5d,
	cohortname,
    isM1_Converted,
	case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No'  then 'Yes' 
		 when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and isDiabeticMet = 'Yes' and daysFromLast5d <= 98 and is_Excluded_timetracked = 'No' then 'Yes' else 'No' end as isPatientIncluded,
    case when isM1_Converted = 'Yes' and treatmentdays > 35 and daysFromConversionDate > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then 'Yes'
		 when isM1_Converted = 'No' and treatmentdays > 35 and isNABrittle = 'No' and (is_medical_exclusion = 'No' or isMedExclusion_AST_ALT_BMI = 'Yes' or isMedExclusion_Age = 'Yes') and isTTP = 'No' and daysFromLast5d <= 150 and is_Excluded_timetracked = 'No' then 'Yes' else 'No' end as isPatientIncludedCommercial,
    ifnull(is_InReversal, 'No') as is_InReversal,
	is_MetOnly_by_OPS,
	if(is_InReversal is null and is_MetOnly_by_OPS = 'No', 'Yes', 'No') as is_Escalation,
	if((medicine_drugs is null or (medicine_drugs = 'METFORMIN' or medicine_drugs = 'METFORMIN (DIAB)' or medicine_drugs = 'Biguanide (DIABETES)')) and last_available_cgm_5d > 140, 'Yes', 'No') as is_Relapse,
    is_type1like,
    is_medical_exclusion,
    medical_exclusion_cohort,
    -- isMedExclusion_to_include,
    isUnqualified,
    isNABrittle,
    nonAdh_brittle_category,
    isExcluded,
    is_app_suspended
    ,totalEscalationDays
    ,totalReversalDays
    ,isInReversal_previous_day
    ,is_TempCharge
    ,is_Disenroll_Open
    ,Disenroll_OpenTime
    ,syntaxName
    ,isCBG
    ,is_GlucosePrick_On
    ,is_GoldPro_member
    ,recentLabDate
    ,recentLabA1C
    ,invoiceDate
    ,invoicestatus
    ,is_CBGtoCGM_to_include
    ,is_Excluded_timetracked
    ,start_insulin_units
    ,current_insulin_units
    ,ea1c_60d
    ,last_available_ea1c_60d
    ,last_available_ea1c_60d_date
    ,nut_adh_1d
    ,AS_1d
    ,start_labA1C
    ,first_available_cgm_1d
    ,first_available_cgm_1d_date
    ,first_available_cgm_5d
    ,first_available_cgm_5d_date
    ,start_homa2b
    ,last_homa2b
    ,start_BMI_bcm
    ,last_available_BMI
    ,start_eGFR
    ,latest_eGFR 
	from 
	(
		select distinct a.clientid, ct.patientnameShort as patient, ct.doctornameShort as doctor, morningInternalDoctorName as morningDoctor, eveningInternalDoctorName as eveningDoctor,  coach, ct.leadCoachNameShort as leadCoach, ct.enrollmentDateITZ, a.date, b.last_available_cgm_5d_date, b.last_available_cgm_5d, 
        datediff(a.date, b.last_available_cgm_5d_date) as daysFromLast5d, a.treatmentDays, datediff(a.date, ttp.conversion_payment_date) as daysFromConversionDate,
		b.cohortname,
		b.is_InReversal, 
		b.is_MetOnly_by_OPS,
		b.isNoCGM_14days,
		b.cgm_5d,
		b.actual_medCount as medCount, 
		b.medicine, 
		b.medicine_drugs,
		b.as_5d,
		b.nut_adh_syntax_5d, 
        if(ifnull(b.start_medCount,0) > 0 or b.start_labA1c >= 6.5, 'Yes', 'No') isDiabeticMet,
		if((ttp.is_patient_currently_TTP = 'yes' or (ttp.is_patient_currently_TTP is null and ttp.is_patient_converted is null)) and ttp.clientid is not null, 'Yes', 'No') as isTTP,
		if(ttp.is_patient_converted = 'yes' and ttp.clientid is not null, 'Yes', 'No') as isM1_Converted,    
		cx.suspended,
		if(ct.labels like '%unqualified%', 'Yes', 'No') as isUnqualified,
		if(ct.labels like '%exclude%', 'Yes', 'No') as isExcluded,
        b.nonAdh_brittle_category, 
		if(b.nonAdh_brittle_category is not null, 'Yes', 'No') as isNABrittle,
		ifnull(dc.is_type1like, 'No') as is_type1like,
		dc.is_medical_exclusion,
        dc.medical_exclusion_cohort, 
		case when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal = 'Yes' or b.is_MetOnly_by_OPS = 'Yes') then 'Yes' 
			when dc.is_medical_exclusion = 'Yes' and (b.is_InReversal is null and b.is_MetOnly_by_OPS = 'No') then 'No' 
			else null end as isMedExclusion_to_include, 
            
        case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('AST', 'ALT', 'BMI') then 'Yes' 
         else 'No' end as isMedExclusion_AST_ALT_BMI,
         
		case when dc.is_medical_exclusion = 'Yes' and medical_exclusion_cohort in ('Age') then 'Yes' 
         else 'No' end as isMedExclusion_Age,
         
		if(at.clientid is not null, 'Yes', 'No') as is_app_suspended,
        totalEscalationDays
        ,totalReversalDays
		,if(b1.is_InReversal = 'Yes' or b1.is_MetOnly_by_OPS = 'Yes', 'Yes', 'No') as isInReversal_previous_day
        ,if(ct.labels like '%temp_discharge%', 'Yes', '') as is_TempCharge
        ,if(imt.clientid is not null, 'Yes', '') as is_Disenroll_Open
        ,if(imt.clientid is not null, report_time, '') as Disenroll_OpenTime
        ,ns.syntaxName
        ,b.isCBG
        ,if(ctd.clientid is not null, 'Yes', '') as is_GlucosePrick_On
        ,if(ct.plancode like '%GoldPro%', 'Yes', '') as is_GoldPro_member
        ,b.recentLabDate
        ,b.recentLabA1C
        ,cx.invoiceDate
        ,cx.invoicestatus
       	,if(cb.clientid is not null, 'Yes', 'No') as is_CBGtoCGM_to_include
        ,ifnull(b.isExcludeLabelOn, 'No') as is_Excluded_timetracked
        ,dc.start_insulin_units
        ,dc.current_insulin_units,
        b.ea1c_60d,
        b.last_available_ea1c_60d,
        b.last_available_ea1c_60d_date,
        b.nut_adh_syntax as nut_adh_1d,
        b.as_1d,
        dc.start_labA1C,
        dc.first_available_cgm_1d,
        dc.first_available_cgm_1d_date,
        dc.first_available_cgm_5d,
        dc.first_available_cgm_5d_date,
        dc.start_homa2b,
        dc.last_homa2b,
        dc.start_BMI_bcm,
        dc.last_available_BMI,
        dc.start_eGFR,
        dc.latest_eGFR 
		from 
		(
			select a.date, b.clientId, coach, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays --  (datediff(case when a.date between f.startdate and f.enddate then date_sub(b.startdate, interval 1 day) else a.date end, b.startDate) + 1) as treatmentDays
			from twins.v_date_to_date a
			inner join
			(
				select clientid, coach, startDate, endDate -- if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
				from
				(
					select clientid, coachname_short as coach, max(status_start_date) as startDate, max(Status_end_date) as endDate
					from bi_patient_status
					where status = 'active'
					group by clientid,  coachname_short
				) s
			) b on a.date between b.startDate and b.endDate
			where a.date >= date_sub(date(itz(now())), interval 14 day)   -- >  if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))
		) a left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.date = b.date 
			left join bi_ttp_enrollments ttp on a.clientid = ttp.clientid
			left join clientauxfields cx on a.clientid = cx.id
			left join v_allclient_list ct on a.clientid = ct.clientid 
			left join dim_client dc on a.clientid = dc.clientid and dc.is_row_current = 'y'
 			left join bi_patient_suspension_tracker at on a.clientid = at.clientid and a.date between at.startdate and at.endDate and at.status = 1
			left join (
						select clientid, count(distinct case when is_InReversal is null and is_MetOnly_by_OPS = 'No' then date else null end) totalEscalationDays, 
                        count(distinct case when is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' then date else null end) totalReversalDays
						from bi_patient_reversal_state_gmi_x_date
						group by clientid 
					  )rd on a.clientid = rd.clientid 
			left join bi_patient_reversal_state_gmi_x_date b1 on a.clientid = b1.clientid and a.date = date_sub(b1.date, interval 1 day)
            left join (select clientid, max(itz(report_time)) as report_time from bi_incident_mgmt where category = 'disenrollment' and status = 'open' group by clientid) imt on a.clientid = imt.clientid
            left join nutritionprofiles np on a.clientid = np.clientid 
            left join nutritionsyntax ns on np.nutritionSyntaxId = ns.nutritionSyntaxId
            left join clienttodoschedules ctd on a.clientid = ctd.clientid and ctd.category = 'TEST' and ctd.type in ('glucose_ketone', 'glucose') and a.date between ctd.startdate and ctd.enddate
            left join (
					select s.clientid, s.cbgEndDate, date_add(s.cbgenddate, interval 1 day) as day1_after_cbg_is_over, date_add(s.cbgenddate, interval 7 day) as day7_after_cbg_is_over, 
                    s.dayFirst5dg_available, datediff(dayFirst5dg_available, cbgEndDate ) as dayFirst5dg_available_after_CBGisOver, 							
					s1.cgm_5d, -- s1.is_inReversal, s1.is_metonly_by_ops, 							
					case when is_InReversal is null and is_MetOnly_by_OPS = 'No' then 'Yes'							
						 when is_InReversal is null and is_MetOnly_by_OPS is null then 'NoCGM5d_yet'						
						 when is_InReversal = 'Yes' or is_MetOnly_by_OPS = 'Yes' then 'inReversal'
						 else 'No' end is_Escalation_finder						
					from 							
					(							
						select a.clientid, a.date as cbgEndDate, min(case when cgm_5d is not null then b.date else null end) as dayFirst5dg_available						
						from 						
						(						
							select distinct b.clientid,date 					
							from v_date_to_date a inner join predictedcgmcycles b on a.date = b.enddate					
												  -- inner join v_client_tmp c on b.clientid = c.clientid
							where a.date >= date_sub(date(itz(now())), interval 45 day)
                            and datediff(b.enddate, b.startdate) >= 50
						) a  						
						 left join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date > a.date						
						 group by a.clientid, a.date						
					 ) s left join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.dayFirst5dg_available = s1.date 			
					) cb on a.clientid = cb.clientid and a.date between cb.day1_after_cbg_is_over and cb.day7_after_cbg_is_over      
            where ct.patientname not like '%obsolete%'
	) s 
) s 
-- where isPatientIncluded = 'Yes' 
;