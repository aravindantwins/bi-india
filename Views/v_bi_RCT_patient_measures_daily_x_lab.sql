set collation_connection = 'latin1_swedish_ci';

alter view v_bi_RCT_patient_measures_daily_x_lab as 
select f1.clientid, f2.patientName, f2.labels, 
f3.treatmentDays, 
f2.gender, 
f2.age, 
f2.durationYears_diabetes, 
f2.status, 
f2.coachShortName, 
f2.coachname AS coach, 
f1. category as measure_category, 
SUM(f1.D0) as D0_measure, 
SUM(f1.D10) as D10_measure, 
SUM(f1.D30) as D30_measure, 
SUM(f1.D60) as D60_measure, 
SUM(f1.D90) as D90_measure, 
SUM(f1.D120) as D120_measure, 
SUM(f1.D150) as D150_measure, 
SUM(f1.D180) as D180_measure,
SUM(f1.D210) as D210_measure,
SUM(f1.D240) as D240_measure,
SUM(f1.D270) as D270_measure,
SUM(f1.D300) as D300_measure,
SUM(f1.D330) as D330_measure,
SUM(f1.D360) as D360_measure,

SUM(f1.D390) as D390_measure,
SUM(f1.D420) as D420_measure,
SUM(f1.D450) as D450_measure,
SUM(f1.D480) as D480_measure,
SUM(f1.D510) as D510_measure,
SUM(f1.D540) as D540_measure,
SUM(f1.D570) as D570_measure,
SUM(f1.D600) as D600_measure,
SUM(f1.D630) as D630_measure,
SUM(f1.D660) as D660_measure,
SUM(f1.D690) as D690_measure,
SUM(f1.D720) as D720_measure


from 
(
	(
		select f.date, f.clientid, f.category,
		SUM(case when f.date_category ='D0' then convert(measure, decimal(10,2)) end) as D0, 
		SUM(case when f.date_category ='D10' then convert(measure, decimal(10,2)) end) as D10,
		SUM(case when f.date_category ='D30' then convert(measure, decimal(10,2)) end) as D30,
		SUM(case when f.date_category ='D60' then convert(measure, decimal(10,2)) end) as D60,
		SUM(case when f.date_category ='D90' then convert(measure, decimal(10,2)) end) as D90,
		SUM(case when f.date_category ='D120' then convert(measure, decimal(10,2)) end) as D120,
		SUM(case when f.date_category ='D150' then convert(measure, decimal(10,2)) end) as D150,
		SUM(case when f.date_category ='D180' then convert(measure, decimal(10,2)) end) as D180,
		SUM(case when f.date_category ='D210' then convert(measure, decimal(10,2)) end) as D210,
		SUM(case when f.date_category ='D240' then convert(measure, decimal(10,2)) end) as D240,
		SUM(case when f.date_category ='D270' then convert(measure, decimal(10,2)) end) as D270,
		SUM(case when f.date_category ='D300' then convert(measure, decimal(10,2)) end) as D300,
		SUM(case when f.date_category ='D330' then convert(measure, decimal(10,2)) end) as D330,
		SUM(case when f.date_category ='D360' then convert(measure, decimal(10,2)) end) as D360,
		
		SUM(case when f.date_category ='D390' then convert(measure, decimal(10,2)) end) as D390,
		SUM(case when f.date_category ='D420' then convert(measure, decimal(10,2)) end) as D420,
		SUM(case when f.date_category ='D450' then convert(measure, decimal(10,2)) end) as D450,
		SUM(case when f.date_category ='D480' then convert(measure, decimal(10,2)) end) as D480,
		SUM(case when f.date_category ='D510' then convert(measure, decimal(10,2)) end) as D510,
		SUM(case when f.date_category ='D540' then convert(measure, decimal(10,2)) end) as D540,
		SUM(case when f.date_category ='D570' then convert(measure, decimal(10,2)) end) as D570,
		SUM(case when f.date_category ='D600' then convert(measure, decimal(10,2)) end) as D600,
		SUM(case when f.date_category ='D630' then convert(measure, decimal(10,2)) end) as D630,
		SUM(case when f.date_category ='D660' then convert(measure, decimal(10,2)) end) as D660,
		SUM(case when f.date_category ='D690' then convert(measure, decimal(10,2)) end) as D690,
		SUM(case when f.date_category ='D720' then convert(measure, decimal(10,2)) end) as D720
		
		
		
		from 
		(
			select s.clientid, s.date_category, s.category, s.date_chosen as date, s1.measure 
			from 
			(
				select clientid, date_category, category, max(date)  as date_chosen -- Doing this because users ARE playing around the investigations AND this CONDITION would help handling the poor DATA governance
				from bi_twins_rct_health_output_measures
			group by clientid, date_category, category
			) s inner join bi_twins_rct_health_output_measures s1 on s.clientid = s1.clientid and s.date_category = s1.date_category and s.category = s1.category and s.date_chosen = s1.date
		) f
		-- bi_twins_rct_health_output_measures f  
		group by f.clientid , f.date , f.category
	)f1 
	left join (select clientid, patientName, age, labels, gender, convert(durationYears_diabetes, decimal(10,2)) as durationYears_diabetes, status, coachnameShort as coachShortName, coachname from v_allclients_rct)f2 on f1.clientid=f2.clientId 
	left join (select clientid, treatmentDays from dim_client where  is_row_current = 'y')f3 on f1.clientid=f3.clientid
) 

group by f1.clientid, f1.category
;






