create view v_bi_patient_count_x_status_DRC as 
select date, doctorname, 
-- if(DRC_Active_pts_rate = 0, 0, DRC_Active_cnt) as DRC_Active_cnt, 
-- if(DRC_Active_pts_rate = 0, 0, DRC_Discharged_cnt) as DRC_Discharged_cnt, 
-- if(DRC_Active_pts_rate = 0, 0, DRC_Inactive_Cnt) as  DRC_Inactive_Cnt, 
-- if(DRC_Active_pts_rate = 0, 0, DRC_Cumulative_Active_Cnt) as DRC_Cumulative_Active_Cnt, 
ifnull(DRC_Active_cnt,0) as DRC_Active_cnt,
ifnull(DRC_Discharged_cnt,0) as DRC_Discharged_cnt,
ifnull(DRC_Inactive_Cnt,0) as DRC_Inactive_Cnt,
ifnull(DRC_Cumulative_Active_Cnt,0) as DRC_Cumulative_Active_Cnt,
DRC_Active_pts_rate
from 
(
			select s1.date, s1.doctorname, 
			ifnull(max(case when s1.status = 'Active' then measure end),0) as DRC_Active_cnt, 
			ifnull(max(case when s1.status = 'Discharged' then measure end),0) as DRC_Discharged_cnt, 
			ifnull(max(case when s1.status = 'Inactive' then measure end),0) as DRC_Inactive_Cnt,
			ifnull(max(case when s1.status = 'Cum_Active' then measure end),0) as DRC_Cumulative_Active_Cnt,
			ifnull(max(case when s1.status = 'Active' then total_rate end),0) as DRC_Active_pts_rate
			from 
			(
				select date, doctor_name as doctorname, c.status
				from v_date_to_date a cross join (select distinct doctor_name from bi_patient_Status a inner join bi_doctor_fee_ref b on a.doctor_name = b.doctorname and b.isContractSigned = 'Y') b 
									  cross join (select 'Active' as status union all select 'Inactive' as status union all select 'discharged' as status union all select 'Cum_Active' as status) c
			) s1
			left join 
			(
				-- Section to calculate the DRC rate. Get all active patients through doctor. 
				select s1.date, s1.doctorname, s1.status, if(s2.isNewDoctor = 'Y', sum(drc_measure), sum(all_measure)) as measure, if(s2.isNewDoctor = 'Y', sum(s1.drc_measure * s2.referralFee), sum(s1.all_measure * s2.referralFee)) as total_rate
				from 
				(
					select date, doctorname, status, year_no, count(distinct clientid) as all_measure, count(distinct case when source = 'Doctor' then clientid else null end) as drc_measure  
					from 
					(
						select date, clientid, doctorName, source, status, datediff(date, b.status_start_date) + 1 as treatmentdays, 
						case when (datediff(date, b.status_start_date) + 1) <= 365 then 'Y1' else 'Y2' end as year_no
						from v_date_to_date a left join 
						(
							select distinct b.clientid, status_start_date, status_end_date, c.source, 
							doctor_name as doctorName, if(doctor_name is null, null, 'Active') as status from 
							bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid
							where b.status = 'active'
							and not exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'pending_active' and a.status_start_date >= b.status_end_date)
						) b on a.date between b.status_start_date and b.status_end_date
					) s 
					group by date, doctorName, status, year_no
				) s1 left join bi_doctor_fee_ref s2 on s1.doctorname = s2.doctorname and s1.year_no = s2.year_no
				group by s1.date, s1.doctorname, s1.status

				union all 

				-- Get all the discharged patients who came into the program through doctor. 
			select s.date, s.doctorname, s.status, if(s2.isNewDoctor = 'Y', drc_measure, total_measure) as measure, null as total_rate
            from 
            (
            select date, doctorName, status, count(distinct case when source = 'Doctor' then clientid else null end) as drc_measure, count(distinct clientid) as total_measure
				from v_date_to_date a left join 
				(
					select distinct b.clientid, c.source, max(status_start_date) as status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status from 
					bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
					where b.status = 'discharged' 
					and b.clientid not in (select clientid from bi_patient_Status where status = 'active' and status_end_date = date(itz(now()))) -- ignore all active patients today
					and exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'active') -- this is needed because patients are getting discharged even from pending_active state). Proper discharge is when a patient goes out from Active term
					and b.clientid not in (
											select distinct a.clientid from 
											(select clientid, max(status_end_date) as status_end_date from bi_patient_status where status = 'active' group by clientid) a 
											inner join 
											bi_patient_status b on a.clientid = b.clientid and b.status = 'pending_active' 
											and b.status_start_date >= a.status_end_date 
										) -- This is to ignore patients who came back to pending_active from active. 
					group by b.clientid 
				) b on date(status_start_date) <= a.date
				group by date, doctorName, b.status
			) s left join bi_doctor_fee_ref s2 on s.doctorname = s2.doctorname and s2.year_no = 'Y1'
            where s.doctorname is not null
				
			union all 
				
			-- Get all the Inactive customers (very rare for new doctors because the platform doesn't support Inactivation without a discharge today. But this section is to handle old patients. 
			select s.date, s.doctorname, s.status, if(s2.isNewDoctor = 'Y', drc_measure, total_measure) as measure, null as total_rate
            from 
			(
				select date, doctorName, status, count(distinct case when source = 'Doctor' then clientid else null end) as drc_measure, count(distinct clientid) as total_measure
				from v_date_to_date a left join 
				(
						select distinct b.clientid, c.source, max(status_end_date) as status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'inactive') as status from 
						bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
						where b.status = 'active' 
						and b.clientid in (select clientid from bi_patient_status where status = 'inactive')
						and b.clientid not in (select clientid from bi_patient_Status where status = 'discharged')
						group by b.clientid 
				) b on date(status_end_date) <= a.date
				group by date, doctorName, b.status
            ) s left join bi_doctor_fee_ref s2 on s.doctorname = s2.doctorname and s2.year_no = 'Y1'
            where s.doctorname is not null
				
			union all 
				
			-- Cumulative Active patients count till date by doctor. 
            select s.date, s.doctorname, s.status, if(s2.isNewDoctor = 'Y', drc_measure, total_measure) as measure, null as total_rate
            from 
			(
				select date, doctorName, status, count(distinct case when source = 'Doctor' then clientid else null end) as drc_measure, count(distinct clientid) as total_measure
				from v_date_to_date a left join 
				(
						select distinct b.clientid, c.source, status_start_date, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'Cum_Active') as status from 
						bi_patient_status b left join v_allClient_list c on b.clientid = c.clientid 
						where b.status = 'active'
						and not exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'pending_active' and a.status_start_date >= b.status_end_date)
				) b on status_start_date <= a.date
				group by date, doctorName, b.status
            ) s left join bi_doctor_fee_ref s2 on s.doctorname = s2.doctorname and s2.year_no = 'Y1'
			where s.doctorname is not null
            
			) s2
			on s1.date = s2.date and s1.doctorname = s2.doctorname and s1.status = s2.status 
			where (s1.date = last_day(s1.date) or s1.date = date(itz(now()))) and s1.date >= '2020-06-01' -- Date when the new doctor terms came into effect. 
			group by s1.date, s1.doctorname
) s 
;

