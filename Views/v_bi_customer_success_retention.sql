set collation_connection = 'latin1_swedish_ci';

alter view v_bi_customer_success_retention as 
select 
        a.date as   Retention_Date,
		b.Retention_Days,
	
        b.ClientId,
        b.patientNameShort as Patient_Name,
        b.city,
        b.gender,
        b.age,
        b.Years_Diabetic,
        b.start_symptoms as Pre_Twin_Symptoms,
        b.startSymptomCount as Pre_Twin_Symptoms_Count, 
        b.startmedicine,
        b.startMedicineCount,
        b.first_available_cgm_5d as Pre_Twin_5dg, 
        b.coachNameShort as Coach,
        b.leadCoachNameShort as Lead_Coach,
        b.doctorNameShort as Doctor, 
		b.TreatmentDays,
        b.Is_Achieved_Reversal,
        b.Is_Current_Reversal, 
        b.Client_Status,
		b.Incident_Status,
        b.InvoiceStatus,
		b.PlanCode,
        b.EnrollType,
        b.IMT_Report_Date,
        b.IMT_Dis_Subtype_L1,
        b.Is_Suspended,
        b.Resolved_User_Name
       
from 
(select date from v_date_to_date where date between date_sub(date(ITZ(now())), interval 1 month) and date(ITZ(now())) )a
left join
(
	select 	
		-- 'Overdue' as Category,
         d.IMT_resolved_date as  Retention_Date,
         DATEDIFF(d.IMT_resolved_date,d.IMT_Report_Date ) as Retention_Days,
     	a.ClientId,
		a.patientNameShort,
        a.city,
        a.gender,
        a.age,
		c.durationYears_diabetes as Years_Diabetic,
        c.start_symptoms, 
       case when c.start_symptoms not like 'None' 
			then
				((LENGTH(c.start_symptoms) - LENGTH(REPLACE(c.start_symptoms,
                    ',',
                    ''))) + 1)
			else 0 end as startSymptomCount,
        c.start_medicine_diabetes AS startmedicine,
        ((LENGTH(c.start_medicine_diabetes_drugs) - LENGTH(REPLACE(c.start_medicine_diabetes_drugs,
                    ',',
                    ''))) + 1) AS startMedicineCount,
        c.first_available_cgm_5d ,             
		a.coachNameShort,
        a.leadCoachNameShort,
        a.doctorNameShort,
	
        c.TreatmentDays,
		b.Is_Achieved_Reversal,
        b.Is_Current_Reversal, 
        
		a.status as Client_Status,
		d.status as Incident_Status,
        b.InvoiceStatus,
		a.PlanCode,
        
		case when c.isM1 ='Yes' then 'M1'
			 when c.isM2Converted='Yes' then 'M1-M2'
             else 'M2' end  AS EnrollType,  --  TTP_or_Regular_Tag,
             
		d.IMT_Report_Date,
        d.subtype_L1 as IMT_Dis_Subtype_L1,
        case when b.suspended=1 then 'Yes' else 'No' end as Is_Suspended,
        cc.Resolved_User_Name
    
from 
	( 	
		select b.incidentId, a.clientid,a.Max_IMT_resolved_date as IMT_resolved_date,cast(ITZ(b.report_time) as date) as IMT_Report_Date, b.subtype_L1,b.status 
		from
           (
            	select a.clientid,a.Max_IMT_open_date_R,a.Max_IMT_resolved_date, b.Max_IMT_open_date_O -- This IS because sometimes OPS IS opening a ticket BY resolving another TYPE ON the category (to differentiate - like coach opened vs finance or someone opened)
				from
				(
					select clientid,max(cast(ITZ(report_time) as date)) as Max_IMT_open_date_R, max(cast(ITZ(resolved_time) as date)) as Max_IMT_resolved_date
					from bi_incident_mgmt where category ='disenrollment' and status='RESOLVED' and Date(ITZ(report_time)) >=  (cast(itz(now()) as date) - interval 40 day)  -- date_sub(date(itz(now())), interval 40 day)
					group by clientid 
				) a
				left join 
				( 
					select clientid,max(cast(ITZ(report_time)as date)) as Max_IMT_open_date_O
					from bi_incident_mgmt where category ='disenrollment' and status='open' 
					group by clientid 
				) b on a.clientid=b.clientid 
				where  a.Max_IMT_open_date_R > ifnull(b.Max_IMT_open_date_O, '1900-01-01') 
		  ) a inner join bi_incident_mgmt b on a.clientid=b.clientid and a.Max_IMT_resolved_date= cast((ITZ(b.resolved_time)) as date) and b.category ='disenrollment'
	) d
	join v_client_Tmp a on a.clientid=d.clientid and a.status='Active'
		
    join 
		(  
			select a.incidentId,a.IMT_resolved_time, concat(b.firstname,' ', b.lastname) as Resolved_User_Name
		    from 
			 ( 		   
			 		   select distinct a.incidentId,  min(a.datemodified) as IMT_resolved_time
					   from incidentcomment a inner join incident b on a.incidentID = b.incidentId 
					   where b.incidentType = 'disenrollment'
					   and a.incidentStatus = 'RESOLVED' 
					   group by a.incidentid
			 ) a join incidentcomment b on a.incidentId=b.incidentId and a.IMT_resolved_time=b.datemodified -- and b.incidentStatus = 'RESOLVED' 
			 where date(IMT_resolved_time) >= date_sub(date(itz(now())), interval 40 day)
		)cc on d.incidentId=cc.incidentId 
	join
		(	select a.id , invoicestatus, suspended,
				case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
					  else 'No' end as Is_Achieved_Reversal,
                 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
                      else 'No' end as Is_Current_Reversal
			from clientauxfields a inner join clients b on a.id=b.id 
			where a.suspended=0 and b.status='Active'
		)  b on d.clientid=b.id
    left join dim_client c on (d.clientid=c.clientid and c.is_row_current='Y')
    -- left join bi_patient_reversal_state_gmi_x_date bb on d.clientid=bb.clientid and d.IMT_resolved_date=bb.date

) b on a.date = b.Retention_Date
where b.clientid is not null 
;

/*
select 	
		-- 'Overdue' as Category,
         d.IMT_resolved_date as  Retention_Date,
         DATEDIFF(d.IMT_resolved_date,d.IMT_Report_Date ) as Retention_Days,
     	a.ClientId,
		a.patientNameShort,
        a.city,
        a.gender,
        a.age,
		c.durationYears_diabetes as Years_Diabetic,
        c.start_symptoms, 
       case when c.start_symptoms not like 'None' 
			then
				((LENGTH(c.start_symptoms) - LENGTH(REPLACE(c.start_symptoms,
                    ',',
                    ''))) + 1)
			else 0 end as startSymptomCount,
        c.start_medicine_diabetes AS startmedicine,
        ((LENGTH(c.start_medicine_diabetes_drugs) - LENGTH(REPLACE(c.start_medicine_diabetes_drugs,
                    ',',
                    ''))) + 1) AS startMedicineCount,
        c.first_available_cgm_5d ,             
		a.coachNameShort,
        a.leadCoachNameShort,
        a.doctorNameShort,
	
        bb.TreatmentDays,
		b.Is_Achieved_Reversal,
        b.Is_Current_Reversal, 
        
		a.status as Client_Status,
		d.status as Incident_Status,
        b.InvoiceStatus,
		a.PlanCode,
        
		case when c.isM1 ='Yes' then 'M1'
			 when c.isM2Converted='Yes' then 'M1-M2'
             else 'M2' end  AS EnrollType,  --  TTP_or_Regular_Tag,
             
		d.IMT_Report_Date,
        d.subtype_L1 as IMT_Dis_Subtype_L1,
        case when b.suspended=1 then 'Yes' else 'No' end as Is_Suspended,
        cc.Resolved_User_Name
    
from 
	( select a.incidentId,a.clientid,a.IMT_report_date,a.report_time, a.IMT_resolved_date,a.resolved_time,a.subtype_L1,a.status 
		from
        (   select incidentId,clientid,Date(ITZ(report_time))as IMT_report_date, report_time, Date(ITZ(resolved_time))as IMT_resolved_date,resolved_time,subtype_L1 ,status
			from bi_incident_mgmt where category ='disenrollment'  and status='RESOLVED'
		 ) a 
         inner join
       
         (
             select clientid,max(Date(ITZ(resolved_time))) as Max_IMT_resolved_date
			 from bi_incident_mgmt where category ='disenrollment' and status='RESOLVED' 
			 group by clientid 
		  ) b on a.clientid=b.clientid and b.Max_IMT_resolved_date=a.IMT_resolved_date
	) d
	join 
		( select clientId, patientNameShort,  city,gender,age,coachNameShort,leadCoachNameShort,doctorNameShort,date(termEndDate) as termEndDate ,planCode,labels,status 
		  from v_allclient_list where status='active' 
		) a  on a.clientid=d.clientid
    join 
		(  select a.incidentId,a.Min_resolved_time, concat(b.firstname,' ', b.lastname) as Resolved_User_Name
		   from 
			 ( select incidentId,  min(datemodified) as Min_resolved_time
					   from incidentcomment 
					   where incidentStatus = 'RESOLVED' 
					   group by incidentid
			 ) a join incidentcomment b on a.incidentId=b.incidentId and a.Min_resolved_time=b.datemodified
		)cc on d.incidentId=cc.incidentId -- and cc.resolved_time=d.resolved_time
	join
		(	select a.id , invoicestatus, suspended,
				case when (a.historicDiabetesMetforminReversal=1 or b.historicDiabetesInReversal=1 ) then 'Yes' 
					  else 'No' end as Is_Achieved_Reversal,
                 case when ( a.currentDiabetesMetforminReversal=1 or b.currentDiabetesInReversal=1 ) then 'Yes'
                      else 'No' end as Is_Current_Reversal
			from clientauxfields a inner join clients b on a.id=b.id 
			where a.suspended=0 
		)  b on d.clientid=b.id
    left join dim_client c on (d.clientid=c.clientid and c.is_row_current='Y')
    left join bi_patient_reversal_state_gmi_x_date bb on d.clientid=bb.clientid and d.IMT_resolved_date=bb.date
*/
