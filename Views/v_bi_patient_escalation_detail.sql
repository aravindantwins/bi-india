create view v_bi_patient_escalation_detail as 
select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, NC_5d, TAC_5d, Ketone_3d, CGM_3d, 
case when (ketone_3d is null or cgm_3d is null) then 'NO DATA'
	 when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and ketone_3d > 4 and CGM_3d < 125 then 'NFLW'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and ketone_3d < 0.4 and CGM_3d > 200 then 'NFLW'
	 when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d <= 0.7 and CGM_3d <= 200 then 'NM' 
	 when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 0.7 and 2) and CGM_3d <= 137 then 'NM'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 2 and 4) and CGM_3d < 125 then 'NM'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 0.7 and 2) and (CGM_3d between 137 and 200) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 2 and 3) and (CGM_3d between 125 and 160) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 3 and 4) and (CGM_3d between 125 and 145) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d > 4) and (CGM_3d between 125 and 137) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 0.4 and 0.7) and (CGM_3d > 200) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d >= 0.7 and CGM_3d > 200 then 'ILT'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d >= 2 and (CGM_3d between 160 and 200) then 'ILT'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d >= 3 and (CGM_3d between 145 and 160) then 'ILT'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d > 4 and (CGM_3d between 137 and 145) then 'ILT'
     else 'NOT ADHERING' end as escalation_category
from 
(
		select s.clientid, s.patientname, s.doctorname, s.coachname, 
		s.day11, s1.average_measure as NC_5d, s2.average_measure as TAC_5d, s3.average_measure as CGM_3d, s4.average_measure as Ketone_3d
		from 
		(
				select distinct b.clientid, c.doctorname, c.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
				date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11, 
				case when daysenrolled >= 10 and daysenrolled < 35 then '>D10' 
					 when daysenrolled >= 35 then '>D35' 
					 when daysenrolled < 10 then 'D10' end as PT_Category
				from
					(
						select clientid, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
						max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
						from bi_patient_status b
						group by clientid, status
					) b left join v_Client_tmp c on b.clientid = c.clientid
				where b.status = 'active' 
				and datediff(status_end_date, status_Start_date) >= 10
				and date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) <= date(itz(now())) 
		) s left join bi_measures s1 on s.clientid = s1.clientid and s.day11 = s1.measure_event_date and s1.measure_name = 'netcarb' and s1.measure_type = '5d' 
			left join bi_measures s2 on s.clientid = s2.clientid and s.day11 = s2.measure_event_date and s2.measure_name = 'TAC' and s2.measure_type = '5d'
			left join bi_measures s3 on s.clientid = s3.clientid and s.day11 = s3.measure_event_date and s3.measure_name = 'CGM' and s3.measure_type = '3d'
			left join bi_measures s4 on s.clientid = s4.clientid and s.day11 = s4.measure_event_date and s4.measure_name = 'Ketone' and s4.measure_type = '3d'
) s1 
where day11 > '2019-05-23' 

union all 

select s1.clientid, s1.patientname, s1.doctorname, s1.coachname, s1.day11, NC_5d, TAC_5d, Ketone_3d, CGM_3d, 
case when (ketone_3d is null or cgm_3d is null) then 'NO DATA'
	 when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and ketone_3d > 4 and CGM_3d < 125 then 'NFLW'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and ketone_3d < 0.4 and CGM_3d > 200 then 'NFLW'
	 when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d <= 0.7 and CGM_3d <= 200 then 'NM' 
	 when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 0.7 and 2) and CGM_3d <= 137 then 'NM'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 2 and 4) and CGM_3d < 125 then 'NM'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 0.7 and 2) and (CGM_3d between 137 and 200) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 2 and 3) and (CGM_3d between 125 and 160) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 3 and 4) and (CGM_3d between 125 and 145) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d > 4) and (CGM_3d between 125 and 137) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and (Ketone_3d between 0.4 and 0.7) and (CGM_3d > 200) then 'M'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d >= 0.7 and CGM_3d > 200 then 'ILT'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d >= 2 and (CGM_3d between 160 and 200) then 'ILT'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d >= 3 and (CGM_3d between 145 and 160) then 'ILT'
     when ((ketone_3d >= 0.8) or (NC_5d <=48 and TAC_5d > 2.5) or (CGM_3d < 80 and TAC_5d > 2.5)) and Ketone_3d > 4 and (CGM_3d between 137 and 145) then 'ILT'
     else 'NOT ADHERING' end as escalation_category
from 
(
		select s.clientid, s.patientname, s.doctorname, s.coachname, 
		s.day11, s1.average_measure as NC_5d, s2.average_measure as TAC_5d, s3.average_measure as CGM_3d, s4.average_measure as Ketone_3d
		from 
		(
				select distinct b.clientid, c.doctorname, c.patientname, c.coachname, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ,
				date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) as day11, 
				case when daysenrolled >= 10 and daysenrolled < 35 then '>D10' 
					 when daysenrolled >= 35 then '>D35' 
					 when daysenrolled < 10 then 'D10' end as PT_Category
				from
					(
						select clientid, enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, status, 
						max(status_start_date) as status_start_date, max(status_end_date) as status_end_date
						from bi_patient_status b
						group by clientid, status
					) b left join v_Client_tmp c on b.clientid = c.clientid
				where b.status = 'active' 
				and datediff(status_end_date, status_Start_date) >= 10
				and date(date_add(ifnull(if(status_start_date > if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ)), status_start_date, if(treatmentsetupdateITZ > visitdateITZ, date(treatmentsetupdateITZ), date(visitdateITZ))), b.enrollmentdateitz), interval 11 day)) <= date(itz(now())) 
		) s left join bi_measures s1 on s.clientid = s1.clientid and s.day11 = s1.measure_event_date and s1.measure_name = 'netcarb' and s1.measure_type = '5d' 
			left join bi_measures s2 on s.clientid = s2.clientid and s.day11 = s2.measure_event_date and s2.measure_name = 'TAC' and s2.measure_type = '5d'
			left join bi_measures s3 on s.clientid = s3.clientid and s.day11 = s3.measure_event_date and s3.measure_name = 'CGM' and s3.measure_type = '3d'
			left join bi_measures s4 on s.clientid = s4.clientid and s.day11 = s4.measure_event_date and s4.measure_name = 'Ketone' and s4.measure_type = '3d'
) s1 
where day11 <= '2019-05-23' 
;