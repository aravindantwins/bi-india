alter view v_bi_patient_x_disease as 
select a.clientid, b.patientname, b.doctorname, eventdate, diseaseCategory, diseaseId, diseasename, markersNormalRange, detectionRule, symptoms, ifnull(rule_result, 'No Data') as rule_result, 
probability, ifnull(actual,'No Data') as actual
from bi_disease_ref_detail a inner join v_Active_clients b on a.clientid = b.clientid 

union all 

select 1 as clientid, ' Choose Patient' as patientname, '' as doctorname, null as eventdate, '' as diseaseCategory, '' as diseaseId, '' as diseasename, '' as markersNormalRange, '' as detectionRule, '' as symptoms, 'No Data' as rule_result, 
null as probability, 'No Data' as actual
;

/*
select a.clientid, patientname, doctorname, eventdate, a.diseaseCategory, b.diseaseId, a.diseasename, markersNormalRange, detectionRule, symptoms, b.rule_result, 
case when b.rule_result = 1 then probability else null end as probability, b.actual
from
(select clientid, patientname, doctorname, diseaseCategory, diseaseId, diseaseName, detectionRule, markersNormalRange, symptoms,
	case when probability = 3 then 'Certain' 
		 when probability = 2 then 'High Risk'
         when probability = 1 then 'Medium Risk' end as probability
	from v_active_clients cross join ref_disease where category='markerID' order by clientid, diseaseid) a
left join 
(
        select clientid, date(itz(bloodWorkTime)) as eventDate, '2' as diseaseID, 
			case when isnull(ldlCholesterol) and isnull(triglycerides) then null 
				 when (ldlCholesterol > 160 or triglycerides > 150) then 1 else 0 end as rule_result, 
			case when isnull(ldlCholesterol) and isnull(triglycerides) then null 
				 else concat('LDL: ',ifnull(cast(ldlCholesterol as decimal(5,1)),''),', TriGly: ',ifnull(cast(triglycerides as decimal(5,1)),'')) end as actual 
		from clinictestResults 
        where bloodworktime is not null
		
        union all 
		
        select a.clientid, date(itz(bloodWorkTime)) as eventDate, '4' as diseaseID, 
			case when isnull(c.average_measure) and isnull(b.average_measure) then null 
				 when (c.average_measure > 140 or b.average_measure > 90) then 1 else 0 end as rule_result,
			case when isnull(c.average_measure) and isnull(b.average_measure) then null  
				 else concat('Systolic: ',floor(c.average_measure),', Diastolic: ',floor(b.average_measure)) end as actual 
		from clinictestResults a left join bi_measures b on a.clientid = b.clientid and date(itz(bloodworkTime)) = b.measure_event_date and b.measure_type = '1d' and b.measure_name = 'Diastolic'
								 left join bi_measures c on a.clientid = c.clientid and date(itz(bloodworkTime)) = c.measure_event_date and c.measure_type = '1d' and c.measure_name = 'Systolic'
		where bloodworktime is not null
		
        union all 
		
        select clientid, date(itz(bloodWorkTime)) as eventDate, '5' as diseaseID, 
			case when isnull(veinhba1c) then null
				 when veinhba1c > 6.4 then 1 else 0 end as rule_result,
			concat('Hba1c: ',cast(veinhba1c as decimal(5,1))) as actual  
        from clinictestResults 
        where bloodworktime is not null
		
        union all
		
        select clientid, date(itz(bloodWorkTime)) as eventDate, '7' as diseaseID, 
			case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null 
				 when (creatinine > 1.2 or glomerularFiltrationRate < 60) then 1 else 0 end as rule_result,
            case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null      
				 else concat('Creat: ',ifnull(cast(creatinine as decimal(5,1)),''),',','GFR: ',ifnull(cast(glomerularFiltrationRate as decimal(5,1)),'') ) end as actual 
		from clinictestResults 
		where bloodworktime is not null
		
        union all
		
        select clientid, date(itz(bloodWorkTime)) as eventDate, '9' as diseaseID, 
			case when isnull(alkalinePhosphatase) and isnull(alt) and isnull(ast) and isnull(bilirubin) then null 
				 when (alkalinePhosphatase > 300 or alt > 200 or ast > 200 or bilirubin > 1.2 ) then 1 else 0 end as rule_result,
            case when isnull(alkalinePhosphatase) and isnull(alt) and isnull(ast) and isnull(bilirubin) then null      
			     else concat('ASP: ',ifnull(cast(alkalinePhosphatase as decimal(5,1)),''),', ALT: ',ifnull(cast(alt as decimal(5,1)),''),', AST: ',ifnull(cast(ast as decimal(5,1)),''),', Bilirubin: ',ifnull(cast(bilirubin as decimal(5,1)),'')) end as actual 
        from clinictestResults 
        where bloodworktime is not null
        
        union all 
        
        select clientid, date(itz(bloodWorkTime)) as eventDate, '23' as diseaseID, 
			case when isnull(alkalinePhosphatase) and isnull(alt) and isnull(ast) and isnull(bilirubin) then null 
				 when (alkalinePhosphatase > 300 and alt > 200 and ast > 200 and bilirubin > 1.2) then 1 else 0 end as rule_result,
			case when isnull(alkalinePhosphatase) and isnull(alt) and isnull(ast) and isnull(bilirubin) then null 
			     else concat('ASP: ',ifnull(cast(alkalinePhosphatase as decimal(5,1)),''),', ALT: ',ifnull(cast(alt as decimal(5,1)),''),', AST: ',ifnull(cast(ast as decimal(5,1)),''),', Bilirubin: ',ifnull(cast(bilirubin as decimal(5,1)),'')) end as actual 
        from clinictestResults 
		where bloodworktime is not null
		
        union all 
        
        select clientid, date(itz(bloodWorkTime)) as eventDate, '24' as diseaseID, 
			case when isnull(alkalinePhosphatase) and isnull(ast) and isnull(albumin) and isnull(bilirubin) then null
				 when (alkalinePhosphatase > 300 and ast > 200 and albumin < 3.4 and bilirubin > 1.2)  then 1 else 0 end as rule_result,
			case when isnull(alkalinePhosphatase) and isnull(ast) and isnull(albumin) and isnull(bilirubin) then null
                 else concat('ASP: ',ifnull(cast(alkalinePhosphatase as decimal(5,1)),''),', AST: ',ifnull(cast(ast as decimal(5,1)),''),', Albumin: ',ifnull(cast(albumin as decimal(5,1)),''),', Bilirubin: ',ifnull(cast(bilirubin as decimal(5,1)),'')) end as actual 
        from clinictestResults 
        where bloodworktime is not null
		
        union all 
        
        select clientid, date(itz(bloodWorkTime)) as eventDate, '31' as diseaseID, 
			case when isnull(amylase) or isnull(lipase) or isnull(triglycerides) then null 
				 when amylase is not null and lipase is not null and triglycerides is not null and (amylase > 450 and lipase > 450 and triglycerides > 1000) then 1 else 0 end as rule_result,
            case when isnull(amylase) or isnull(lipase) or isnull(triglycerides) then null 
				 else concat('Amylase: ',ifnull(cast(amylase as decimal(5,1)),''),', Lipase: ',ifnull(cast(lipase as decimal(5,1)),''),', TriGly: ',ifnull(cast(triglycerides as decimal(5,1)),'')) 
				end as actual 
		from clinictestResults 
        where bloodworktime is not null
		
        union all 
        
        select clientid, date(itz(bloodWorkTime)) as eventDate, '50' as diseaseID, 
			case when isnull(veinhba1c) then null
				 when veinhba1c between 5.7 and 6.4 then 1 else 0 end as rule_result,
			concat('Hba1c: ',cast(veinhba1c as decimal(5,1)))  as actual 
        from clinictestResults 
        where bloodworktime is not null
		
        union all 
        
        select clientid, date(itz(bloodWorkTime)) as eventDate, '54' as diseaseID, 
			case when amylase is null or lipase is null then null 
				 when amylase is not null and lipase is not null and (amylase < 23 and lipase < 10) then 1 else 0 end as rule_result,
			case when amylase is null or lipase is null then null 
                 else concat('Amylase: ',ifnull(cast(amylase as decimal(5,1)),''),', Lipase: ',ifnull(cast(lipase as decimal(5,1)),'')) end as actual 
		from clinictestResults 
        where bloodworktime is not null    
		
        union all 
		
        select clientid, date(itz(bloodWorkTime)) as eventDate, '32' as diseaseID, 
			case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null 
				 when isnull(veinHba1c) then null 
				 when ((creatinine > 1.2 or glomerularFiltrationRate < 90) and (veinHba1c > 6.4)) then 1 else 0 end as rule_result,
            case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null 
				 when isnull(veinHba1c) then null 
				 else concat('Creat: ',ifnull(cast(creatinine as decimal(5,1)),''),',','GFR: ',ifnull(cast(glomerularFiltrationRate as decimal(5,1)),''),',','Hba1c: ',ifnull(cast(veinHba1c as decimal(5,1)),'') ) end as actual 
		from clinictestResults 
        where bloodworktime is not null
		
        union all 
		
        select a.clientid, date(itz(bloodWorkTime)) as eventDate, '33' as diseaseID, 
			case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null 
				 when isnull(b.average_measure) and isnull(c.average_measure) then null 
				 when ((creatinine > 1.2 or glomerularFiltrationRate < 90) and (c.average_measure > 140 or b.average_measure > 90)) then 1 else 0 end as rule_result,
			case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null 
				 when isnull(b.average_measure) and isnull(c.average_measure) then null 
				 else concat('Creat: ',ifnull(cast(creatinine as decimal(5,1)),''),',','GFR: ',ifnull(cast(glomerularFiltrationRate as decimal(5,1)),''),',','Systolic: ',ifnull(floor(c.average_measure),''),',','Diastolic: ',ifnull(floor(b.average_measure),'') ) end as actual
		from clinictestResults a left join bi_measures b on a.clientid = b.clientid and date(itz(bloodworkTime)) = b.measure_event_date and b.measure_type = '1d' and b.measure_name = 'Diastolic'
								 left join bi_measures c on a.clientid = c.clientid and date(itz(bloodworkTime)) = c.measure_event_date and c.measure_type = '1d' and c.measure_name = 'Systolic'
		where bloodworktime is not null
                                     
		union all 
        
        select clientid, date(itz(bloodWorkTime)) as eventDate, '39' as diseaseID, 
			case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null 
				 when (creatinine > 1.2 or glomerularFiltrationRate < 60)  then 1 else 0 end as rule_result,
            case when isnull(creatinine) and isnull(glomerularFiltrationRate) then null      
				 else concat('Creat: ',ifnull(cast(creatinine as decimal(5,1)),''),',','GFR: ',ifnull(cast(glomerularFiltrationRate as decimal(5,1)),'')) end as actual
		from clinictestResults 
        where bloodworktime is not null
   		
        union all 
        
		select clientid, date(itz(bloodWorkTime)) as eventDate, '45' as diseaseID, 
			case when isnull(creatinine) then null 
				 when isnull(albumin) or isnull(cholesterol) then null 
				 when ((creatinine > 1.2) and (albumin < 2.5 and cholesterol > 200)) then 1 else 0 end as rule_result,
			case when isnull(creatinine) then null 
				 when isnull(albumin) or isnull(cholesterol) then null 
				 else concat('Creat: ',ifnull(cast(creatinine as decimal(5,1)),''),',','Albumin: ',ifnull(cast(albumin as decimal(5,1)),''),',','Cholesterol: ',ifnull(cast(cholesterol as decimal(5,1)),'')) end as actual
		from clinictestResults 
		where bloodworktime is not null
		
        union all 
        
        select a.clientid, date(itz(bloodWorkTime)) as eventDate, '46' as diseaseID, 
			case when isnull(veinHba1c) or isnull(b.average_measure) or isnull(c.average_measure) or isnull(sodium) then null 
				 when  veinHba1c is not null and b.average_measure is not null and c.average_measure is not null and sodium is not null and 
                 (veinHba1c > 6.4 and  b.average_measure > 300 and c.average_measure > 4 and sodium < 135) then 1 else 0 end as rule_result,
            case when isnull(veinHba1c) or isnull(b.average_measure) or isnull(c.average_measure) or isnull(sodium) then null      
				 else concat('Hba1c: ',ifnull(cast(veinHba1c as decimal(5,1)),''),',CGM: ',ifnull(cast(b.average_measure as decimal(5,1)),''),',Ketone: ',ifnull(floor(c.average_measure),''),',Sodium: ',ifnull(floor(sodium),'') ) end as actual
		from clinictestResults a left join bi_measures b on a.clientid = b.clientid and date(itz(bloodworkTime)) = b.measure_event_date and b.measure_type = '1d' and b.measure_name = 'cgm'
								 left join bi_measures c on a.clientid = c.clientid and date(itz(bloodworkTime)) = c.measure_event_date and c.measure_type = '1d' and c.measure_name = 'ketone'
		where bloodworktime is not null
        
        union all 
		
        select a.clientid, date(itz(bloodWorkTime)) as eventDate, '48' as diseaseID, 
			case when isnull(b.average_measure) or isnull(cPeptide) or isnull(insulin) then null 
				 when b.average_measure is not null and cPeptide is not null and insulin is not null and 
                 (b.average_measure < 45 and cPeptide > 2 and insulin > 10) then 1 else 0 end as rule_result,
            case when isnull(b.average_measure) or isnull(cPeptide) or isnull(insulin) then null      
				 else concat('CGM: ',ifnull(cast(b.average_measure as decimal(5,1)),''),',cPeptide: ',ifnull(cast(cPeptide as decimal(5,1)),''),',Insulin: ',ifnull(cast(insulin as decimal(5,1)),'') ) end as actual
		from clinictestResults a left join bi_measures b on a.clientid = b.clientid and date(itz(bloodworkTime)) = b.measure_event_date and b.measure_type = '1d' and b.measure_name = 'cgm'
		where bloodworktime is not null                         
        
        union all 
		
        select a.clientid, date(itz(bloodWorkTime)) as eventDate, '59' as diseaseID, 
			case when isnull(b.average_measure) or isnull(cPeptide) or isnull(insulin) then null 
				 when b.average_measure is not null and cPeptide is not null and insulin is not null and 
                 (b.average_measure < 70 and cPeptide > 3.1 and insulin < 3) then 1 else 0 end as rule_result,
            case when isnull(b.average_measure) or isnull(cPeptide) or isnull(insulin) then null      
				 else concat('CGM: ',ifnull(cast(b.average_measure as decimal(5,1)),''),',cPeptide: ',ifnull(cast(cPeptide as decimal(5,1)),''),',Insulin: ',ifnull(cast(insulin as decimal(5,1)),'') ) end as actual
		from clinictestResults a left join bi_measures b on a.clientid = b.clientid and date(itz(bloodworkTime)) = b.measure_event_date and b.measure_type = '1d' and b.measure_name = 'cgm'
		where bloodworktime is not null                         

        union all 
		
        select a.clientid, date(itz(bloodWorkTime)) as eventDate, '60' as diseaseID, 
			case when isnull(b.average_measure) or isnull(cPeptide) or isnull(insulin) then null 
				 when b.average_measure is not null and cPeptide is not null and insulin is not null and 
                 (b.average_measure < 70 and cPeptide > 3.1 and insulin < 3) then 1 else 0 end as rule_result,
            case when isnull(b.average_measure) or isnull(cPeptide) or isnull(insulin) then null      
				 else concat('CGM: ',ifnull(cast(b.average_measure as decimal(5,1)),''),',cPeptide: ',ifnull(cast(cPeptide as decimal(5,1)),''),',Insulin: ',ifnull(cast(insulin as decimal(5,1)),'') ) end as actual
		from clinictestResults a left join bi_measures b on a.clientid = b.clientid and date(itz(bloodworkTime)) = b.measure_event_date and b.measure_type = '1d' and b.measure_name = 'cgm'
		where bloodworktime is not null                       
        
		union all 
		
        select a.clientid, date(itz(bloodWorkTime)) as eventDate, '99' as diseaseID, 
			case when isnull(veinHba1c) or isnull(b.average_measure) or isnull(sodium) or isnull(potassium) or isnull(calcium) then null 
				 when veinHba1c is not null and b.average_measure is not null and sodium is not null and potassium is not null and calcium is not null and 
                 (veinHba1c < 5.7 and b.average_measure < 100 and sodium > 145 and potassium < 3.5 and calcium > 10.5) then 1 else 0 end as rule_result,
            case when isnull(veinHba1c) or isnull(b.average_measure) or isnull(sodium) or isnull(potassium) or isnull(calcium) then null 
				 else concat('Hba1c: ',ifnull(cast(veinHba1c as decimal(5,1)),''),'CGM: ',ifnull(cast(b.average_measure as decimal(5,1)),''),',Sodium: ',ifnull(cast(sodium as decimal(5,1)),''),',Potassium: ',ifnull(cast(potassium as decimal(5,1)),''),',Calcium: ',ifnull(cast(calcium as decimal(5,1)),'') ) end as actual
		from clinictestResults a left join bi_measures b on a.clientid = b.clientid and date(itz(bloodworkTime)) = b.measure_event_date and b.measure_type = '1d' and b.measure_name = 'cgm'
		where bloodworktime is not null    
        
		union all 
		
        select clientid, date(itz(bloodWorkTime)) as eventDate, '98' as diseaseID, 
			case when isnull(veinHba1c) or isnull(insulin) then null 
				 when veinHba1c is not null and insulin is not null and 
                 (veinHba1c > 6.4 and insulin < 2.6) then 1 else 0 end as rule_result,
            case when isnull(veinHba1c) or isnull(insulin) then null 
				 else concat('Hba1c: ',ifnull(cast(veinHba1c as decimal(5,1)),''),',Insulin: ',ifnull(cast(insulin as decimal(5,1)),'') ) end as actual
		from clinictestResults 
		where bloodworktime is not null    
        
        union all 
		
        select clientid, date(itz(bloodWorkTime)) as eventDate, '148' as diseaseID, 
			case when isnull(hemoglobin) or isnull(creatinine) or isnull(calcium) or isnull(protein) or isnull(globulin)  then null 
				 when hemoglobin is not null and creatinine is not null and calcium is not null and protein is not null and globulin is not null and 
                 (hemoglobin < 12 and creatinine > 1.2 and calcium > 10.5 and protein > 8.3 and globulin > 3.5) then 1 else 0 end as rule_result,
            case when isnull(hemoglobin) or isnull(creatinine) or isnull(calcium) or isnull(protein) or isnull(globulin)  then null  
				 else concat('Haemo: ',ifnull(cast(hemoglobin as decimal(5,1)),''),',Creatinine: ',ifnull(cast(creatinine as decimal(5,1)),''),',Calcium: ',ifnull(cast(calcium as decimal(5,1)),''),',Protein: ',ifnull(cast(protein as decimal(5,1)),''),',Globulin: ',ifnull(cast(globulin as decimal(5,1)),'') ) end as actual
		from clinictestResults
		where bloodworktime is not null    

) b 
on (a.clientid = b.clientid and a.diseaseID = b.diseaseID)

union all 

select 1 as clientid, '' as patientname, '' as doctorname, null as eventdate, '' as diseaseCategory, '' as diseaseId, '' as diseasename, '' as markersNormalRange, '' as detectionRule, '' as symptoms, null as rule_result, 
null as probability, null as actual
;
*/