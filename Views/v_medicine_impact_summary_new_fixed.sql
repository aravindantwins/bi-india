select 	y.clientid, 
		y.patientname, 
        y.doctorname, 
		current_medication,
		escalationCategory,
		date_used_for_running,
		5Dg_running,
		5Dketone_running,
		5Dspike_running,
		5Dnetcarb_running,
        medication_change_date,
		5Dg_day_before_med_change,
		5Dketone_day_before_med_change,
		5Dspike_day_before_med_change,
		5Dnetcarb_day_before_med_change,
		V1.medication as previous_medication,
        z.no_of_medicine_increases
from 
(select x.clientid, 
x.patientname, 
x.doctorname,
x.escalationCategory,
x.current_medication,
x.lastdate as date_used_for_running,
x.cgm5dg as 5Dg_running, 
x.ketone5dk as 5Dketone_running, 
x.spike5d as 5Dspike_running,  
x.netcarb5d as 5Dnetcarb_running,
coalesce(v3.medication_startdate, v.medication_startdate) as medication_change_date, 
coalesce(v3.cgm5dg_before_med, v.cgm5dg_before_med) as 5Dg_day_before_med_change,
coalesce(v3.ketone5dk_before_med, v.ketone5dk_before_med) as 5Dketone_day_before_med_change,
coalesce(v3.spike5d_before_med, v.spike5d_before_med) as 5Dspike_day_before_med_change,
coalesce(v3.netCarb5d_before_med, v.netCarb5d_before_med) as 5Dnetcarb_day_before_med_change
from 
(select a.clientid, a.patientname, a.doctorname, a.escalationcategory, a.medication as current_medication, b.lastdate, cgm5dg, ketone5dk, spike5d, netcarb5d 
from v_medicine_impact_details a
inner join 
		(select clientid, date_sub(max(eventdateitz), interval 1 day) as lastdate from v_medicine_impact_details 
         group by clientid
		) b
		on a.clientid = b.clientid and a.eventdateitz = b.lastdate
) x
left join v_medicine_impact_detail_x_medication v on x.clientid = v.clientid and v.medication_enddate is null -- this join is to see simply get the row where medication end date is blank (usually the current medication and current date).
left join v_medicine_impact_detail_x_medication v3 on x.clientid = v3.clientid and v3.medication_enddate = x.lastdate -- this join is for sometimes the daterunning could be an end of a particular medication, so this ensures to catch any medication that ended on the Running date.
) Y left join v_medicine_impact_detail_x_medication v1 on y.clientid = v1.clientid and date_sub(y.medication_change_date, interval 1 day) = v1.medication_enddate
    left join (
				select clientid, sum(case when medicine_up_indicator then 1 else 0 end)  as no_of_medicine_increases
				from bi_medicine_impact_detail
				group by clientid
			  ) Z on y.clientid = z.clientid

