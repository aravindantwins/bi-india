alter view v_bi_patient_with_no_doctor_in_PA as 
select a.id as clientid, UPPER(TRIM(concat(b.firstname, ' ', b.lastname))) as patientname, c.city, a.plancode, a.labels, a.source, a.sourcetype, a.notes
from clients a  
left join twinspii.clientspii b on a.id = b.id
left join twinspii.addressespii c on a.addressId = c.Id
where a.doctorId is null
and a.status = 'pending_active'
and a.test is false
and (b.firstname not like '%TEST%' and b.firstname not like '%QA%')
and a.deleted is false
; 