alter view v_bi_Member1_coach_summary as 
select Leadcoach, coach,
count(distinct case when treatmentday = 'D9' then clientid else null end) as D9_count, 
count(distinct case when treatmentday = 'D9' and scheduled_review_date is not null then clientid else null end) as D9_RS, 
count(distinct case when treatmentday = 'D9' and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D9_RC, 

count(distinct case when treatmentday = 'D10' then clientid else null end) as D10_count, 
count(distinct case when treatmentday = 'D10' and scheduled_review_date is not null then clientid else null end) as D10_RS, 
count(distinct case when treatmentday = 'D10' and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D10_RC, 

count(distinct case when treatmentday in ('D11','D12') then clientid else null end) as D11_12_count, 
count(distinct case when treatmentday in ('D11','D12') and scheduled_review_date is not null then clientid else null end) as D11_12_RS, 
count(distinct case when treatmentday in ('D11','D12') and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D11_12_RC, 

count(distinct case when treatmentday in ('D13','D14')  then clientid else null end) as D13_14_count, 
count(distinct case when treatmentday in ('D13','D14')  and scheduled_review_date is not null then clientid else null end) as D13_14_RS, 
count(distinct case when treatmentday in ('D13','D14')  and scheduled_review_date is not null and is_d10_review_completed = 'Yes' then clientid else null end) as D13_14_RC,

count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') then clientid else null end) as D9_13_count, 
count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is not null then clientid else null end) as D9_13_Scheduled,
count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is null and is_d10_review_completed is null then clientid else null end) as D9_13_NoSchedule,
count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and scheduled_review_date is null and is_d10_review_completed is null and isDisenrollOpen = 'No' then clientid else null end) as D9_13_Pending_Schedule,
count(distinct case when treatmentday in ('D9','D10','D11','D12','D13') and is_d10_review_completed = 'Yes' then clientid else null end) as D9_13_Review_Completed


from 
(
		select s.clientid, s.patient, coach, Leadcoach, treatmentdays, s1.treatmentday, is_d10_review_completed, scheduled_review_date, isDisenrollOpen, isD10_outcome_achieved
		from 
		(
			select a.clientid, b.patientname as patient, b.coachnameShort as coach, b1.leadCoachNameShort as Leadcoach, a.treatmentdays, 
			start_weight, cast((start_labA1C * 28.1) - 46.7 as decimal(10,5)) start_glucose, start_labA1C, 
			replace(replace(start_medicine_diabetes_drugs,'(DIABETES)',''), '(INSULIN)','') as start_diabetes_medicines_drugs, 
			length(start_medicine_diabetes_drugs) - length(replace(start_medicine_diabetes_drugs,',','')) + 1 as start_medicine_count, 
			start_symptoms, b.plancode, 
			length(start_symptoms) - length(replace(start_symptoms,',','')) + 1 as start_symptoms_count,
			c.d10_review_date as scheduled_review_date,
			c.is_d10_review_completed,
			if(inc.clientid is not null, 'Yes', 'No') as isDisenrollOpen, c.d10_date, c.isD10_outcome_achieved, c.is_patient_converted
			from dim_client a inner join v_active_clients b on a.clientid = b.clientid 
							  inner join bi_ttp_enrollments c on a.clientid = c.clientid 
                              left join v_coach b1 on b.coachnameShort = b1.coachNameShort
							  left join (
										 select distinct clientid, max(date(itz(report_time))) as openDate, max(date(itz(resolved_time))) as resolvedDate 
										 from bi_incident_mgmt where category = 'Disenrollment' 
										 group by clientid
                                         ) inc on a.clientid = inc.clientid and date(now()) between opendate and ifnull(resolvedDate,date(now()))
			where a.is_row_current = 'y'
			and b.patientname not like '%obsolete%'
			and (b.labels not like '%TWIN_PRIME%' and b.labels not like '%CONTROL%')
			-- and c.is_patient_converted is null
		) s 
		inner join 
			(
					select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, a.date
					from bi_patient_reversal_state_gmi_x_date a inner join bi_ttp_enrollments c on a.clientid = c.clientid 
					where a.date = if(hour(itz(now())) < 8, date_sub(date(itz(now())), interval 1 day), date(itz(now())))  -- date_sub(date(itz(now())), interval 1 day) -- date(now()) -- 
					-- and c.is_patient_converted is null
			) s1 on s.clientid = s1.clientid 
) s 
group by Leadcoach, coach
order by coach
;