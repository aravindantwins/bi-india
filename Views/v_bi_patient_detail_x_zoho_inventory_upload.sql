create view v_bi_patient_detail_x_zoho_inventory_upload as 
		select d.date, c.clientId, c.status_start_date as pending_active_start_date,
        ct.firstName, ct.lastName, ct.patientname, ct.gender, ct.age, ct.email, ct.mobilenumber, ct.address, ct.postalCode, ct.city
		from twins.v_date_to_date d
		left join bi_patient_status c on d.date = c.status_start_date and c.status = 'pending_active'
		left join  v_client_tmp ct on c.clientid = ct.clientid     
        where d.date >= date_sub(date(itz(now())), interval 1 month) 
        and ct.status not in ('registration')
        ;