create view v_bi_patient_count_x_status_doctor_rate as 
select s1.date, s1.doctorname, 
ifnull(max(case when s1.status = 'Active' then measure end),0) as Active_cnt, 
ifnull(max(case when s1.status = 'Discharged' then measure end),0) as Discharged_cnt, 
ifnull(max(case when s1.status = 'Inactive' then measure end),0) as Inactive_Cnt,
ifnull(max(case when s1.status = 'Cum_Active' then measure end),0) as Cumulative_Active_Cnt,
ifnull(max(case when s1.status = 'Active' then total_rate end),0) as Active_pts_rate
from 
(select date, doctor_name as doctorname, c.status
from v_date_to_date a cross join (select distinct doctor_name from bi_patient_Status a inner join bi_doctor_fee_ref b on a.doctor_name = b.doctorname and b.isContractSigned = 'Y') b 
					  cross join (select 'Active' as status union all select 'Inactive' as status union all select 'discharged' as status union all select 'Cum_Active' as status) c
) s1
left join 
(
	select s1.date, s1.doctorname, s1.status, sum(measure) as measure, sum(s1.measure * s2.fee) as total_rate
    from 
    (
		select date, doctorname, status, year_no, count(distinct clientid) as measure -- , count(distinct case when year_no = 'Y1' then clientid else null end) as y1_measure, count(distinct case when year_no = 'Y2' then clientid else null end) as y2_measure 
		from 
		(
			select date, clientid, doctorName, status, datediff(date, b.status_start_date) + 1 as treatmentdays, case when (datediff(date, b.status_start_date) + 1) <= 365 then 'Y1' else 'Y2' end as year_no
			from v_date_to_date a left join 
			(
				select distinct clientid, status_start_date, status_end_date, 
				doctor_name as doctorName, if(doctor_name is null, null, 'Active') as status from 
				bi_patient_status b 
				where status = 'active'
                and not exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'pending_active' and a.status_start_date >= b.status_end_date)
			) b on a.date between b.status_start_date and b.status_end_date
		) s 
		group by date, doctorName, status, year_no
    ) s1 left join bi_doctor_fee_ref s2 on s1.doctorname = s2.doctorname and s1.year_no = s2.year_no
    group by s1.date, s1.doctorname, s1.status

	union all 

	select date, doctorName, status, count(distinct clientid) as measure, null as total_rate
	from v_date_to_date a left join 
	(
		select distinct clientid, max(status_start_date) as status_start_date, doctor_name as doctorName, if(doctor_name is null, null, 'discharged') as status from 
		bi_patient_status b 
		where status = 'discharged' 
        and clientid not in (select clientid from bi_patient_Status where status = 'active' and status_end_date = date(itz(now())))
		and exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'active') -- this is needed because patients are getting discharged even from pending_active state). Proper discharge is when a patient goes out from Active term
        and b.clientid not in (
								select distinct a.clientid from 
								(select clientid, max(status_end_date) as status_end_date from bi_patient_status where status = 'active' group by clientid) a 
								inner join 
								bi_patient_status b on a.clientid = b.clientid and b.status = 'pending_active' 
								and b.status_start_date >= a.status_end_date 
							) -- This is to ignore patients who came back to pending_active from active. 
        group by b.clientid         
	) b on date(status_start_date) <= a.date
	group by date, doctorName, b.status
	
    union all 
    
	select date, doctorName, status, count(distinct clientid) as measure, null as total_rate
	from v_date_to_date a left join 
	(
		select distinct clientid, max(status_end_date) as status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'inactive') as status from 
		bi_patient_status b 
		where status = 'active' 
        and clientid in (select clientid from bi_patient_status where status = 'inactive')
		and clientid not in (select clientid from bi_patient_Status where status = 'discharged')
        group by clientid 
	) b on date(status_end_date) <= a.date
	group by date, doctorName, b.status
    
    union all 
    
	select date, doctorName, status, count(distinct clientid) as measure, null as total_rate
	from v_date_to_date a left join 
	(
		select distinct clientid, status_start_date, status_end_date, doctor_name as doctorName, if(doctor_name is null, null, 'Cum_Active') as status from 
		bi_patient_status b 
		where status = 'active'
		and not exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'pending_active' and a.status_start_date >= b.status_end_date)
	) b on status_start_date <= a.date
	group by date, doctorName, b.status
) s2
on s1.date = s2.date and s1.doctorname = s2.doctorname and s1.status = s2.status 
where (s1.date = last_day(s1.date) or s1.date = date(itz(now()))) and s1.date >= '2020-06-01'
group by s1.date, s1.doctorname