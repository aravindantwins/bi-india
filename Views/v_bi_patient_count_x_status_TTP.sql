alter view v_bi_patient_count_x_status_TTP as 
-- TTP version
select s1.date, 'TTP' as category, s1.doctorid, s1.doctorname, 
max(case when s1.status = 'ENROLLED' then measure end) as Enrolled_cnt, 
max(case when s1.status = 'ACTIVE' then measure end) as Active_cnt, 
max(case when s1.status = 'DISCHARGED' then measure end) as Discharged_cnt, 
max(case when s1.status = 'INACTIVE' then measure end) as Inactive_Cnt,
max(case when s1.status = 'PENDING_ACTIVE' then measure end) as Pending_Active_Cnt,
max(case when s1.status = 'ON_HOLD' then measure end) as OnHold_cnt
from 
(
select date, doctorid, doctor_name as doctorname, c.status
from v_date_to_date a cross join (select distinct doctorid, doctor_name from bi_patient_Status where doctorid is not null) b 
					  cross join (select 'ACTIVE' as status union all select 'ENROLLED' as status union all select 'INACTIVE' as status union all select 'DISCHARGED' as status union all select 'PENDING_ACTIVE' as status union all select 'ON_HOLD' as status) c
where date >= date_sub(date(now()), interval 12 month) 
) s1 left join 
(
	select date, doctorid, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select s.clientid, s.doctorid, s.enrollmentdateitz, s.conversion_payment_date, s.doctorName, s.status, s1.status as current_status
        from 
        (
				select distinct b.clientid, a.enrollmentdateitz, conversion_payment_date, b.doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'ENROLLED') as status from 
				bi_patient_status b inner join bi_ttp_enrollments a on b.clientid = a.clientid 
				where b.status = 'active'
				and b.clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
				
				union all 
				
				-- Consider any patient who is in the system with PENDING_ACTIVE status but they have completed their enrollment. 
				select distinct b.clientid, a.enrollmentdateitz, conversion_payment_date, b.doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'ENROLLED') as status from 
				bi_patient_status b inner join bi_ttp_enrollments a on b.clientid = a.clientid 
				where b.status = 'PENDING_ACTIVE'
				and b.clientid not in (select distinct clientid from bi_patient_status where status = 'active')
				and b.clientid not in (select distinct clientid from v_client_Tmp where status = 'registration') -- patients are going back to registration status from pending_active but not getting captured in audit trail 
				and b.clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
        ) s inner join clients s1 on s.clientid = s1.id 
        where s1.deleted = 0
	) b on date(enrollmentdateITZ) <= a.date and a.date <= if(current_status = 'PENDING_ACTIVE', date, ifnull(date_sub(conversion_payment_date, interval 1 day), a.date)) -- Consider the patient as TTP until the day before he/she converted
    group by date, doctorid, doctorName, b.status
 
	union all 
    
	select date, doctorid, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select a.clientid, doctorid, status_start_date, if(b.is_patient_converted = 'Yes', date_sub(conversion_payment_date,interval 1 day), status_end_date) as status_end_date, doctorName, a.status 
        from 
		(
				select distinct clientid, status_start_date, status_end_date, doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'ACTIVE') as status 
				from bi_patient_status a
				where status = 'active'
				and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
				and clientid in (select clientid from bi_ttp_enrollments) -- Consider all TTP patients, if the patient is converted - consider until the 2nd payment date 
				and not exists (select 1 from bi_patient_status b where a.clientid = b.clientid and b.status = 'pending_active' and b.status_start_date = a.status_start_date and b.status_end_date > a.status_end_date)
        ) a left join bi_ttp_enrollments b on a.clientid = b.clientid         
	) b on a.date between b.status_start_date and b.status_end_date
	group by date, doctorid, doctorName, b.status

	union all 

	select date, doctorid, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
			select distinct clientid, enrollmentdateitz, status_start_date, status_end_date, doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'PENDING_ACTIVE') as status from 
			bi_patient_status a
			where status = 'pending_active'
			and clientid not in (select distinct clientid from v_client_tmp where status = 'registration')  -- patients are going back to registration status from pending_active but not getting captured in audit trail 
			and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
			and clientid in (select clientid from bi_ttp_enrollments) -- Lets consider only TTP patients who are in pending_active state
    ) b on a.date between b.status_start_date and b.status_end_date
	group by date, doctorid, doctorName, b.status
 
	union all 
    
	select date, doctorid, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
			select distinct clientid, enrollmentdateitz, status_start_date, status_end_date, doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'ON_HOLD') as status from 
			bi_patient_status 
			where status = 'on_hold'
			and clientid not in (select distinct clientid from v_client_tmp where status = 'registration')  -- patients are going back to registration status from pending_active but not getting captured in audit trail 
			and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
			and clientid in (select clientid from bi_ttp_enrollments) -- Lets consider only TTP patients who are in pending_active state
    ) b on a.date between b.status_start_date and b.status_end_date
	group by date, doctorid, doctorName, b.status
    
	union all 
    
	select date, doctorid, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select clientid, status_start_date, status_end_date, doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'DISCHARGED') as status from 
		bi_patient_status b 
		where status = 'discharged' 
		and clientid not in (select clientid from v_client_tmp where status = 'inactive')
        and not exists (select 1 from bi_patient_status c where b.clientid = c.clientid group by c.clientid having count(case when c.status in ('active', 'pending_active') then c.clientid else null end) = 0 ) -- to ignore patients who are getting discharged directly before reaching Pending active at least
		and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
        and clientid in (select clientid from bi_ttp_enrollments where status in ('discharged') and is_patient_converted is null)
        and not exists (select 1 from bi_patient_status d where d.clientid = b.clientid and d.status <> 'discharged' and d.status_end_date = b.status_end_date and d.status_start_date < b.status_start_date) -- Due to audit corruption (last status change is getting corrupted so during the same period, patients are having both active and discharged records, sample - 1168401). So in such case, see if there is another state started with date less than discharged state, if yes consider that
    ) b on a.date between status_start_date and status_end_date
	group by date, doctorid, doctorName, b.status
 
 	union all 
    
	select date, doctorid, doctorName, status, count(distinct clientid) as measure
	from v_date_to_date a left join 
	(
		select distinct clientid, date_add(max(status_end_date), interval 1 day) as status_start_date, date(now()) as status_end_date, doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'INACTIVE') as status from 
		bi_patient_status b 
		where status = 'Active' 
        and clientid in (select clientid from v_client_tmp where status = 'Inactive')
        and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
        and clientid in (select clientid from bi_ttp_enrollments where status = 'Inactive' and is_patient_converted is null)
        group by clientid 
        
        union all
        
        select distinct clientid, date_add(max(status_end_date), interval 1 day), date(now()) as status_end_date, doctorid, doctor_name as doctorName, if(doctor_name is null, null, 'INACTIVE') as status from -- Patients are getting discharged and inactivated directly from pending_active as well.
		bi_patient_status b 
		where status = 'pending_active' 
        and clientid in (select clientid from v_client_tmp where status = 'Inactive')
        and clientid in (select clientid from bi_patient_status group by clientid having count(case when status = 'active' then clientid else null end) = 0) -- So get those patients who had pending_active status and never reached active status.
		and clientid not in (select clientid from v_Client_tmp where patientname like '%obsolete%')
		and clientid in (select clientid from bi_ttp_enrollments where status = 'Inactive' and is_patient_converted is null)
        group by clientid 
    ) b on a.date between status_start_date and status_end_date
	group by date, doctorName, b.status
 ) s2  on s1.date = s2.date and s1.doctorid = s2.doctorid and s1.doctorname = s2.doctorname and s1.status = s2.status 
 where s1.doctorid is not null
group by s1.date, s1.doctorid
;