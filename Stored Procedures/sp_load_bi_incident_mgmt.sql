DELIMITER $$
CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `sp_load_bi_incident_mgmt`(  )
BEGIN




update dailyprocesslog 
set startdate = now()
where processname = 'sp_load_bi_incident_mgmt'
;

drop temporary table if exists tmp_bi_incident_mgmt;

create temporary table tmp_bi_incident_mgmt
(
incidentid int not null,
clientid int not null,
status varchar(50) null,
category varchar(50) null,
symptom varchar(50) null,
symptom_severity varchar(50) null,
symptom_classification varchar(100) null,
incident_classification varchar(100) null, 
subtype_L1 varchar(50) null,
subtype_L2 varchar(50) null,
custom_subtype varchar(50) null,
grievance char(1) null,
owner varchar(50) null,

owner_description varchar(5000) null,

report_time datetime not null,
datemodified datetime null,
owner_entry_date datetime null,
first_response_time datetime null,
address_time_hours int null,
close_duration_hours int null,
resolved_time datetime null
)
;

insert into tmp_bi_incident_mgmt (incidentid, clientid, status, category, symptom, symptom_severity, symptom_classification, incident_classification, 
subtype_L1, subtype_L2, custom_subtype, grievance, owner, owner_description, report_time, dateModified, owner_entry_date, 
first_response_time, address_time_hours, close_duration_hours, resolved_time)
select 	a.incidentId as incidentId,
		a.clientid,
		a.incidentstatus as status, 
		a.incidentType as category, 
        b.symptomType as title,
        case when b1.value = 3 then 'HIGH' 
			 when b1.value = 2 then 'MEDIUM'
             when b1.value = 1 then 'LOW' end as severity,
		b.symptomClassification, 
		a.incidentClassification as incident_classification,
        a.subTypeL1 as subtype_L1,
        a.subTypeL2 as subtype_L2,
        a.customSubType as custom_subtype,
        case when a.grievance = 1 then 'Y'
			 when a.grievance = 0 then 'N' else NULL end as grievance,
		concat(a.userfirstName,' ',a.userlastName) as owner,
        
        h.incidentcomment as incident_owner_description,
        
        a.eventtime as report_time,        
        a.dateModified,
		f.owner_entry_date,
        f.first_response_time,
        timestampdiff(hour, f.owner_entry_date, f.first_response_time) as addres_time_HOURS, 
        if(a.incidentstatus = 'Resolved', timestampdiff(hour, a.dateadded, ic.resolved_time), null) as close_duration_HOURS,
        ic.resolved_time
from incident a left join incidentsymptom b on a.incidentId = b.incidentId
				left join selfreportanswers b1 on b.selfReportAnswerId = b1.ID
			    left join (select distinct incidentid, userId from incidentrecipient) d on a.incidentId = d.incidentID 
                left join twinspii.userspii e on d.userId = e.Id                
               	
                left join (
                
							select a.incidentId, a.owner_entry_date, min(b.dateadded) as first_response_time 
							from 
							(select incidentId, min(dateadded) as owner_entry_date from incidentcomment group by incidentId) a left join incidentcomment b on a.incidentId = b.incidentId and b.dateadded > a.owner_entry_date
							group by a.incidentId, a.owner_entry_date
								
						  ) f on a.incidentid = f.incidentid
				left join incidentcomment h on f.incidentId = h.incidentID and h.dateadded = f.owner_entry_date
				left join (
								select incidentId, min(date(datemodified)) as resolved_date, min(datemodified) as resolved_time
								from incidentcomment 
								where incidentStatus = 'RESOLVED' 
								group by incidentid
                          ) ic on a.incidentid = ic.incidentid
				inner join v_client_tmp g on a.clientid = g.clientid
group by incidentid, clientid, a.incidentstatus, category
;



create index idx_tmp_bi_incident_mgmt on tmp_bi_incident_mgmt(incidentId asc, clientid, status, category); 

insert into bi_incident_mgmt (incidentid, clientid, status, category, symptom, symptom_severity, symptom_classification, incident_classification, subtype_L1, subtype_L2, custom_subtype, grievance, 
owner, owner_description, report_time, dateModified, owner_entry_date, first_response_time, address_time_hours, close_duration_hours, resolved_time)
select distinct incidentid, clientid, status, category, symptom, symptom_severity, symptom_classification, incident_classification, subtype_L1, subtype_L2, custom_subtype, grievance, 
owner, owner_description, report_time, dateModified, owner_entry_date, first_response_time, address_time_hours, close_duration_hours, resolved_time
from tmp_bi_incident_mgmt a
where not exists (select 1 from bi_incident_mgmt b where a.incidentId = b.incidentID)
;

update bi_incident_mgmt a inner join tmp_bi_incident_mgmt b on (a.incidentId = b.incidentID)
set a.clientid = b.clientid, 
	a.status = b.status, 
    a.category = a.category, 
    a.symptom = b.symptom, 
    a.symptom_severity = b.symptom_severity, 
    a.symptom_classification = b.symptom_classification, 
    a.incident_classification = b.incident_classification, 
    a.subtype_L1 = b.subtype_L1, 
    a.subtype_L2 = b.subtype_L2, 
    a.custom_subtype = b.custom_subtype, 
    a.grievance = b.grievance, 
	a.owner = b.owner, 
    a.owner_description = b.owner_description, 
    a.report_time = b.report_time, 
    a.dateModified = b.dateModified, 
    a.owner_entry_date = b.owner_entry_date, 
    a.first_response_time = b.first_response_time, 
    a.address_time_hours = b.address_time_hours, 
    a.close_duration_hours = b.close_duration_hours, 
    a.resolved_time = b.resolved_time
;     


drop temporary table tmp_bi_incident_mgmt
;



update dailyprocesslog 
set updatedate = now()
where processname = 'sp_load_bi_incident_mgmt'
;
	
END$$
DELIMITER ;
