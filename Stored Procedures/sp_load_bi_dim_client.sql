CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_dim_client`(  )
BEGIN



update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_dim_client'
;



drop temporary table if exists tmp_dim_client;

create temporary table tmp_dim_client
(
customerkey integer null, 
clientid integer not null, 
doctorName varchar(50) not null,
doctorNameShort varchar(50) not null,
coachName varchar(50) null,
coachNameShort varchar(50) null,






enrollmentDate datetime null,
enrollmentDateITZ datetime null,
status varchar(25) null,
paidType varchar(100) null,
cohortGroup varchar(100) null,
visitDateITZ datetime null,
visitDateAddedITZ datetime null,
visitScheduledDate date null,
renewalVisitScheduledDate datetime null,
dischargeReason varchar(2000) null,
enrolledDays integer null,
renewalCount integer null,
firstDiabetesInReversalTime datetime null, 
latestdiabetesinreversaltime datetime null,
treatmentSetupDateITZ datetime null,
currentTermEndDate datetime null, 
escalationStage varchar(25) null,
planCode varchar(1000) null,
predictionCgmON tinyInt(1) null,
dateAdded datetime null
); 

insert into tmp_dim_client 
( clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,enrollmentDateITZ, status, 
paidType, cohortGroup, visitDateITZ, visitDateAddedITZ, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
enrolledDays, renewalCount, firstDiabetesInReversalTime, latestdiabetesinreversaltime, treatmentSetupDateITZ, currentTermEndDate, escalationStage, planCode,
predictionCgmON, dateAdded)
SELECT 
        c.ID AS clientId,
        UPPER(CONCAT(d.firstName, ' ', d.lastName)) AS doctorName,
        UPPER(CONCAT(d.firstName, ' ', LEFT(d.lastName, 1))) AS doctorNameShort,
        UPPER(CONCAT(h.firstName, ' ', h.lastName)) AS coachName,
        UPPER(CONCAT(h.firstName, ' ', LEFT(h.lastName, 1))) AS coachNameShort,
        c.enrollmentDate AS enrollmentDate,
		ITZ(c.enrollmentDate) AS enrollmentDateITZ,
        c.status, 
        c.paidType AS paidType,
        t.name AS cohortGroup,
        ITZ(v.visitDate) AS visitDateITZ,
        ITZ(v.dateAdded) AS visitDateAddedITZ,
        vs.startDate as visitScheduledDate,
        c.renewalVisitScheduled AS renewalVisitScheduledDate,
        c.dischargeReason AS dischargeReason,
		CASE
            WHEN (c.status = 'Active') THEN (TO_DAYS(ITZ(NOW())) - TO_DAYS(c.enrollmentDate))
            WHEN (c.status <> 'Active') THEN (TO_DAYS(c.lastStatusChange) - TO_DAYS(c.enrollmentDate))
        END AS enrolledDays, 
		(FLOOR(((TO_DAYS(c.termEndDate) - TO_DAYS(c.enrollmentDate)) / 84)) - 1) AS renewalCount,
        c.firstDiabetesInReversalTime, 
        c.latestdiabetesinreversaltime,
        itz(c.treatmentSetupDate) as treatmentSetupDateITZ,
        c.termEndDate AS currentTermEndDate,
        CASE
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 23) THEN 'D2-22'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 43) THEN 'D23-42'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 57) THEN 'D43-56'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) < 85) THEN 'D57-84'
            WHEN ((TO_DAYS(CURDATE()) - TO_DAYS(c.enrollmentDate)) >= 85) THEN 'D85PLUS'
        END AS escalationStage,
        c.planCode AS planCode,
        c.predictedCGMActivated as predictionCgmON,
        itz(c.dateAdded) as dateAdded
    FROM
       twins.clients c
        JOIN twins.doctors d ON c.doctorId = d.ID
        LEFT JOIN twinspii.clientspii c1 on c.id = c1.id
        LEFT JOIN twins.coaches h ON c.coachId = h.ID
        LEFT JOIN (
				select a.clientid, a.date_of_visit, a.visitdate, a.dateadded from 
                (
						SELECT 	clientId,
								DATE(visitDate) as date_of_visit,
								MAX(visitDate) AS visitDate,
								MAX(DateAdded) AS dateAdded
						FROM homevisits
						GROUP BY clientId, date(visitdate) 
                ) a
                inner join (select clientid, max(date(visitdate)) as date_of_visit from homevisits group by clientid) b on a.clientid = b.clientid and a.date_of_visit = b.date_of_visit 
        ) v ON (c.ID = v.clientId)
        LEFT JOIN cohorts t ON (c.cohortId = t.cohortId)
        LEFT JOIN (select clientid, max(startDate) as startDate from momentumweekdetails group by clientid ) vs on c.id = vs.clientid 
        LEFT JOIN addresses ad on c.addressId = ad.id 
    WHERE
        ((c.status IN ('PROSPECT','PENDING_ACTIVE', 'ACTIVE', 'REGISTRATION', 'INACTIVE', 'DISCHARGED'))
            AND (c.deleted = 0)
            
			AND ((d.test = FALSE and c.labels not like '%PRIME%' and c.labels not like '%CONTROL%') 
					or (c.labels like '%PRIME%' or c.labels like '%CONTROL%'))
            AND (c.ID NOT IN (15544 , 15558, 15593, 15594, 15596, 15680))
            AND (NOT ((c1.firstName LIKE '%TEST%')))
            AND (NOT ((c1.lastName LIKE '%TEST%'))))
;



update tmp_dim_client a inner join (select clientid, max(customerkey) as customerkey from dim_client group by clientid) b on a.clientid = b.clientid 
set a.customerkey = b.customerkey; 


insert into dim_client
(clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,
 enrollmentDateITZ, status, paidType, cohortGroup, visitDateITZ, visitDateAddedITZ, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
 enrolledDays, renewalCount, firstDiabetesInReversalTime, latestdiabetesinreversaltime, treatmentSetupDateITZ, currentTermEndDate, escalationStage, planCode,
 predictionCgmON, dateAdded, row_start_date) 
select 
clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,
enrollmentDateITZ, status, paidType, cohortGroup, visitDateITZ, visitDateAddedITZ, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
enrolledDays, renewalCount, firstDiabetesInReversalTime, latestdiabetesinreversaltime, treatmentSetupDateITZ, currentTermEndDate, escalationStage, planCode, 
predictionCgmON, dateAdded, if(dateAdded is null, date(itz(now())), date(itz(dateAdded)))
from tmp_dim_client a 
where customerkey is null
; 



drop temporary table if exists tmp_records_modified; 

create temporary table tmp_records_modified
(
customerkey integer not null,
clientid integer not null, 
doctorName varchar(50) not null,
doctorNameShort varchar(50) not null,
coachName varchar(50) null,
coachNameShort varchar(50) null,
enrollmentDate datetime null,
enrollmentDateITZ datetime null,
status varchar(25) null,
paidType varchar(100) null,
cohortGroup varchar(100) null,
visitDateITZ datetime null,
visitDateAddedITZ datetime null,
visitScheduledDate date null,
renewalVisitScheduledDate datetime null,
dischargeReason varchar(2000) null,
enrolledDays integer null,
renewalCount integer null,
firstDiabetesInReversalTime datetime null, 
latestdiabetesinreversaltime datetime null, 
treatmentSetupDateITZ datetime null,
currentTermEndDate datetime null, 
escalationStage varchar(25) null,
planCode varchar(1000) null,
predictionCgmON tinyint(1) null,
dateAdded datetime null,
changed_columns varchar(5000) null
); 

insert into tmp_records_modified
select a.customerkey, a.clientid, a.doctorName, a.doctorNameShort, a.coachName, a.coachNameShort, 
a.enrollmentDate, a.enrollmentDateITZ, a.status, a.paidType, a.cohortGroup, a.visitDateITZ, a.visitDateAddedITZ, 
a.visitScheduledDate, a.renewalVisitScheduledDate, a.dischargeReason, 
a.enrolledDays, a.renewalCount, a.firstDiabetesInReversalTime, a.latestDiabetesInReversalTime, a.treatmentSetupDateITZ,
a.currentTermEndDate, a.escalationStage, a.planCode, a.predictionCgmON, a.dateAdded,
concat(
		 case when a.status <> b.status then concat('status: ''', a.status,''',') else '' end, 
		 case when a.visitScheduledDate <> b.visitScheduledDate then concat('visitScheduledDate: ''', a.visitScheduledDate,''',') else '' end,
		 case when a.predictionCgmON <> b.predictionCgmON then concat('predictionCgmON: ''', a.predictionCgmON,''',') else '' end, 
		 case when a.currentTermEndDate <> b.currentTermEndDate then concat('currentTermEndDate: ''', a.currentTermEndDate,'''') else '' end
     ) as changed_columns 
from tmp_dim_client a inner join dim_client b on (a.clientid = b.clientid and a.customerkey = b.customerkey)
where ( 
	ltrim(rtrim(a.status)) <> ltrim(rtrim(b.status))
	or (ifnull(a.visitScheduledDate,'1900-01-01') <> ifnull(b.visitScheduledDate,'1900-01-01'))
    or (ifnull(a.currentTermEndDate,'1900-01-01') <> ifnull(b.currentTermEndDate,'1900-01-01'))
    or ltrim(rtrim(a.predictionCgmON)) <> ltrim(rtrim(b.predictionCgmON)))
;



update dim_client a inner join tmp_dim_client b on a.clientid = b.clientid and a.customerkey = b.customerkey
set a.doctorName = b.doctorName,
    a.doctorNameShort = b.doctorNameShort,
    a.coachName = b.coachName,
    a.coachNameShort = b.coachNameShort,
    a.enrollmentDate = b.enrollmentDate,
    a.enrollmentDateITZ = b.enrollmentDateITZ,
    
    a.paidType = b.paidType,
    a.cohortGroup = b.cohortGroup,
    a.visitDateITZ = b.visitDateITZ,
    a.visitDateAddedITZ = b.visitDateAddedITZ,
    
    a.renewalVisitScheduledDate = b.renewalVisitScheduledDate,
    a.dischargeReason = b.dischargeReason,
    a.enrolledDays = b.enrolledDays,
    a.renewalCount = b.renewalCount,
    a.firstDiabetesInReversalTime = b.firstDiabetesInReversalTime,
    a.latestDiabetesInReversalTime = b.latestDiabetesInReversalTime,
    a.treatmentSetupDateITZ = b.treatmentSetupDateITZ,
    
    a.escalationStage = b.escalationStage,
    a.planCode = b.planCode
    
    where a.clientid not in (select clientid from tmp_records_modified)
;


update dim_client a inner join tmp_records_modified b on a.customerkey = b.customerkey and a.clientid = b.clientid  
set row_end_date = date_sub(date(itz(now())), interval 1 day),
	is_row_current = 'N' 
;


insert into dim_client 
(clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,
 enrollmentDateITZ, status, paidType, cohortGroup, visitDateITZ, visitDateAddedITZ, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
 enrolledDays, renewalCount, firstDiabetesInReversalTime, latestDiabetesInReversalTime, treatmentSetupDateITZ, currentTermEndDate, escalationStage, planCode, predictionCgmON, 
 dateAdded, row_start_date, changed_columns) 
select 
clientid, doctorName, doctorNameShort, coachName, coachNameShort, enrollmentDate,
enrollmentDateITZ, status, paidType, cohortGroup, visitDateITZ, visitDateAddedITZ, visitScheduledDate, renewalVisitScheduledDate, dischargeReason, 
enrolledDays, renewalCount, firstDiabetesInReversalTime, latestDiabetesInReversalTime, treatmentSetupDateITZ, currentTermEndDate, escalationStage, planCode, predictionCgmON, 
dateAdded, date(itz(now())) as row_start_date, changed_columns
from tmp_records_modified a 
; 


update dim_client a inner join clients b on a.clientid = b.id 
set a.isRCT = (case when b.labels like '%PRIME%' or b.labels like '%CONTROL%' or b.labels like '%NAFLD%' then 'Yes' end)
where a.is_row_current = 'y'
;

update dim_client a inner join clients b on a.clientid = b.id 
set a.isRCT_PRIME = (case when b.labels like '%PRIME%' then 'Yes' end)
where a.is_row_current = 'y'
;

update dim_client a inner join clients b on a.clientid = b.id 
set a.isRCT_CONTROL = (case when b.labels like '%CONTROL%' then 'Yes' end)
where a.is_row_current = 'y'
;


update dim_client a inner join 
(
		select s1.clientid, 
		firstBloodWorkDate as day0,
		s1.day0 as sensor_day0,
		s1.day10,
		if(s2.bloodworkdate = s1.day30, s1.day30, max(s2.bloodworkdate)) as lab_day30,
		if(s21.bloodworkdate = s1.day60, s1.day60, max(s21.bloodworkdate)) as lab_day60,
		if(s3.bloodworkdate = s1.day90, s1.day90, max(s3.bloodworkdate)) as lab_day90,
		if(s31.bloodworkdate = s1.day120, s1.day120, max(s31.bloodworkdate)) as lab_day120,
		if(s32.bloodworkdate = s1.day150, s1.day150, max(s32.bloodworkdate)) as lab_day150,
		if(s4.bloodworkdate = s1.day180, s1.day180, max(s4.bloodworkdate)) as lab_day180,
		if(s5.bloodworkdate = s1.day270, s1.day270, max(s5.bloodworkdate)) as lab_day270,
		if(s6.bloodworkdate = s1.day360, s1.day360, max(s6.bloodworkdate)) as lab_day360
		from 
		(
					select distinct clientid, status_start_date as day0,
					date_add(status_start_date, interval 9 day) as day10,
					
					date_add(status_start_date, interval 20 day) as day21,
					date_add(status_start_date, interval 29 day) as day30,
					date_add(status_start_date, interval 30 day) as day31,
					date_add(status_start_date, interval 39 day) as day40,
					
					date_add(status_start_date, interval 50 day) as day51,
					date_add(status_start_date, interval 59 day) as day60,
					date_add(status_start_date, interval 60 day) as day61,
					date_add(status_start_date, interval 69 day) as day70,
					
					
					date_add(status_start_date, interval 75 day) as day76,
					date_add(status_start_date, interval 80 day) as day81,
					date_add(status_start_date, interval 89 day) as day90,
					date_add(status_start_date, interval 90 day) as day91,
					date_add(status_start_date, interval 104 day) as day105,

					date_add(status_start_date, interval 110 day) as day111,
					date_add(status_start_date, interval 119 day) as day120,
					date_add(status_start_date, interval 120 day) as day121,
					date_add(status_start_date, interval 129 day) as day130,

					date_add(status_start_date, interval 140 day) as day141,
					date_add(status_start_date, interval 149 day) as day150,
					date_add(status_start_date, interval 150 day) as day151,
					date_add(status_start_date, interval 159 day) as day160,
					
					date_add(status_start_date, interval 165 day) as day166,
					date_add(status_start_date, interval 170 day) as day171,
					date_add(status_start_date, interval 179 day) as day180,
					date_add(status_start_date, interval 180 day) as day181,
					date_add(status_start_date, interval 194 day) as day195,
					
					date_add(status_start_date, interval 255 day) as day256,
					date_add(status_start_date, interval 260 day) as day261, 
					date_add(status_start_date, interval 269 day) as day270,
					date_add(status_start_date, interval 270 day) as day271,
					date_add(status_start_date, interval 284 day) as day285,
					
					date_add(status_start_date, interval 345 day) as day346,
					date_add(status_start_date, interval 350 day) as day351,
					date_add(status_start_date, interval 359 day) as day360,
					date_add(status_start_date, interval 360 day) as day361,
					date_add(status_start_date, interval 374 day) as day375,
					firstBloodWorkDate
					from
					(
						select clientid, date(enrollmentdateitz) as status_start_date, date(firstBloodWorkDateITZ) as firstBloodWorkDate
						from dim_client
						where is_row_current = 'Y'
						and isRCT_CONTROL = 'Yes'
					) s1
			) s1 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s2 on s1.clientid = s2.clientid and ((s1.day30 = s2.bloodworkdate) or (s2.bloodworkdate between s1.day21 and s1.day30) or (s2.bloodworkdate between s1.day31 and s1.day40))
			     left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s21 on s1.clientid = s21.clientid and ((s1.day30 = s21.bloodworkdate) or (s21.bloodworkdate between s1.day51 and s1.day60) or (s21.bloodworkdate between s1.day61 and s1.day70))

				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s3 on s1.clientid = s3.clientid and ((s1.day90 = s3.bloodworkdate) or (s3.bloodworkdate between s1.day76 and s1.day90) or (s3.bloodworkdate between s1.day91 and s1.day105))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s31 on s1.clientid = s31.clientid and ((s1.day120 = s31.bloodworkdate) or (s31.bloodworkdate between s1.day111 and s1.day120) or (s31.bloodworkdate between s1.day121 and s1.day130))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s32 on s1.clientid = s32.clientid and ((s1.day150 = s32.bloodworkdate) or (s32.bloodworkdate between s1.day141 and s1.day150) or (s32.bloodworkdate between s1.day151 and s1.day160))

                 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s4 on s1.clientid = s4.clientid and ((s1.day180 = s4.bloodworkdate) or (s4.bloodworkdate between s1.day166 and s1.day180) or (s4.bloodworkdate between s1.day181 and s1.day195))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s5 on s1.clientid = s5.clientid and ((s1.day270 = s5.bloodworkdate) or (s5.bloodworkdate between s1.day256 and s1.day270) or (s5.bloodworkdate between s1.day271 and s1.day285))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_CONTROL = 'Yes' and a.deleted = 0) s6 on s1.clientid = s6.clientid and ((s1.day360 = s6.bloodworkdate) or (s6.bloodworkdate between s1.day346 and s1.day360) or (s6.bloodworkdate between s1.day361 and s1.day375))

        group by s1.clientid
) b on a.clientid = b.clientid
set a.d30_date_lab = b.lab_day30,
	a.d60_date_lab = b.lab_day60,
    a.d90_date_lab = b.lab_day90,
    a.d120_date_lab = b.lab_day120,
    a.d150_date_lab = b.lab_day150,
    a.d180_date_lab = b.lab_day180,
    a.d270_date_lab = b.lab_day270,
    a.d360_date_lab = b.lab_day360,
    a.programStartDate_analytics = b.sensor_day0
where a.is_row_current = 'Y'
and a.isRCT_CONTROL = 'Yes'
;



update dim_client a inner join 
(
	select s.clientid, count(distinct case when s1.cgm_1d is not null then date else null end) as total_Cgm_1d_Availabe,
	min(case when s1.cgm_1d is not null then date else null end) as first_CGM_sync,
	datediff( if(date(currentTermEndDate) <= date(itz(now())),date(currentTermEndDate), date(itz(now()))), min(case when s1.cgm_1d is not null then date else null end)) as treatmentDays
	from 
	(
		select clientid, enrollmentdateitz, currentTermEndDate, status
		from dim_client 
		where is_row_current = 'y'
		and isRCT_Control = 'Yes'
	) s left join bi_rct_patient_measures_x_preactivation s1 on s.clientid = s1.clientid and cgm_1d is not null
group by s.clientid
) s on a.clientid = s.clientid
set a.treatmentDays = s.treatmentDays
where a.is_row_current = 'Y'
and a.isRCT_Control = 'Yes'
;


update dim_client a inner join 
(
		select s1.clientid, 
		firstbloodworkTimeITZ as day0,
		s1.day0 as sensor_day0,
		s1.day10,
		if(s2.bloodworkdate = s1.day30, s1.day30, max(s2.bloodworkdate)) as lab_day30,
		if(s21.bloodworkdate = s1.day60, s1.day60, max(s21.bloodworkdate)) as lab_day60,
		if(s3.bloodworkdate = s1.day90, s1.day90, max(s3.bloodworkdate)) as lab_day90,
		if(s31.bloodworkdate = s1.day120, s1.day120, max(s31.bloodworkdate)) as lab_day120,
		if(s32.bloodworkdate = s1.day150, s1.day150, max(s32.bloodworkdate)) as lab_day150,
		if(s4.bloodworkdate = s1.day180, s1.day180, max(s4.bloodworkdate)) as lab_day180,
		if(s5.bloodworkdate = s1.day270, s1.day270, max(s5.bloodworkdate)) as lab_day270,
		if(s6.bloodworkdate = s1.day360, s1.day360, max(s6.bloodworkdate)) as lab_day360
		from 
		(
					select distinct b.clientid, status_start_date as day0,
					date_add(status_start_date, interval 9 day) as day10,
					
					date_add(status_start_date, interval 20 day) as day21,
					date_add(status_start_date, interval 29 day) as day30,
					date_add(status_start_date, interval 30 day) as day31,
					date_add(status_start_date, interval 39 day) as day40,
					
					date_add(status_start_date, interval 50 day) as day51,
					date_add(status_start_date, interval 59 day) as day60,
					date_add(status_start_date, interval 60 day) as day61,
					date_add(status_start_date, interval 69 day) as day70,
					
					
					date_add(status_start_date, interval 75 day) as day76,
					date_add(status_start_date, interval 80 day) as day81,
					date_add(status_start_date, interval 89 day) as day90,
					date_add(status_start_date, interval 90 day) as day91,
					date_add(status_start_date, interval 104 day) as day105,

					date_add(status_start_date, interval 110 day) as day111,
					date_add(status_start_date, interval 119 day) as day120,
					date_add(status_start_date, interval 120 day) as day121,
					date_add(status_start_date, interval 129 day) as day130,

					date_add(status_start_date, interval 140 day) as day141,
					date_add(status_start_date, interval 149 day) as day150,
					date_add(status_start_date, interval 150 day) as day151,
					date_add(status_start_date, interval 159 day) as day160,
					
					date_add(status_start_date, interval 165 day) as day166,
					date_add(status_start_date, interval 170 day) as day171,
					date_add(status_start_date, interval 179 day) as day180,
					date_add(status_start_date, interval 180 day) as day181,
					date_add(status_start_date, interval 194 day) as day195,
					
					date_add(status_start_date, interval 255 day) as day256,
					date_add(status_start_date, interval 260 day) as day261, 
					date_add(status_start_date, interval 269 day) as day270,
					date_add(status_start_date, interval 270 day) as day271,
					date_add(status_start_date, interval 284 day) as day285,
					
					date_add(status_start_date, interval 345 day) as day346,
					date_add(status_start_date, interval 350 day) as day351,
					date_add(status_start_date, interval 359 day) as day360,
					date_add(status_start_date, interval 360 day) as day361,
					date_add(status_start_date, interval 374 day) as day375,
			firstbloodworkTimeITZ, status_end_date
			from
				(
					select clientid, if(date(visitdateitz) > status_start_date or status_start_date is null, date(visitdateitz), status_start_date) as status_start_date,
					status_end_date from 
					(
						select b.clientid, b.enrollmentdateitz, b.visitdateitz, b.status, 
						min(status_start_date) as status_start_date, min(status_end_date) as status_end_date
						from bi_patient_status b inner join dim_client c on b.clientid = c.clientid 
						where b.status = 'active'
                        and c.is_row_current = 'y'
                        and c.isRCT_PRIME = 'Yes'
						group by clientid, status
					) s 
                ) b 
				left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults where deleted = 0 group by clientid) d on b.clientid = d.clientid 
			) s1 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s2 on s1.clientid = s2.clientid and ((s1.day30 = s2.bloodworkdate) or (s2.bloodworkdate between s1.day21 and s1.day30) or (s2.bloodworkdate between s1.day31 and s1.day40))
			     left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s21 on s1.clientid = s21.clientid and ((s1.day30 = s21.bloodworkdate) or (s21.bloodworkdate between s1.day51 and s1.day60) or (s21.bloodworkdate between s1.day61 and s1.day70))

				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s3 on s1.clientid = s3.clientid and ((s1.day90 = s3.bloodworkdate) or (s3.bloodworkdate between s1.day76 and s1.day90) or (s3.bloodworkdate between s1.day91 and s1.day105))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s31 on s1.clientid = s31.clientid and ((s1.day120 = s31.bloodworkdate) or (s31.bloodworkdate between s1.day111 and s1.day120) or (s31.bloodworkdate between s1.day121 and s1.day130))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s32 on s1.clientid = s32.clientid and ((s1.day150 = s32.bloodworkdate) or (s32.bloodworkdate between s1.day141 and s1.day150) or (s32.bloodworkdate between s1.day151 and s1.day160))

                 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s4 on s1.clientid = s4.clientid and ((s1.day180 = s4.bloodworkdate) or (s4.bloodworkdate between s1.day166 and s1.day180) or (s4.bloodworkdate between s1.day181 and s1.day195))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s5 on s1.clientid = s5.clientid and ((s1.day270 = s5.bloodworkdate) or (s5.bloodworkdate between s1.day256 and s1.day270) or (s5.bloodworkdate between s1.day271 and s1.day285))
				 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join dim_client b on a.clientid = b.clientid where is_row_current = 'Y' and isRCT_PRIME = 'Yes' and a.deleted = 0 ) s6 on s1.clientid = s6.clientid and ((s1.day360 = s6.bloodworkdate) or (s6.bloodworkdate between s1.day346 and s1.day360) or (s6.bloodworkdate between s1.day361 and s1.day375))
        group by s1.clientid
) b on a.clientid = b.clientid
set a.d30_date_lab = b.lab_day30,
	a.d60_date_lab = b.lab_day60,
    a.d90_date_lab = b.lab_day90,
    a.d120_date_lab = b.lab_day120,
    a.d150_date_lab = b.lab_day150,
    a.d180_date_lab = b.lab_day180,
    a.d270_date_lab = b.lab_day270,
    a.d360_date_lab = b.lab_day360,
    a.programStartDate_analytics = b.sensor_day0
where a.is_row_current = 'Y'
and a.isRCT_PRIME = 'Yes'
;





update dim_client a inner join 
(
		select s1.clientid, 
		firstbloodworkTimeITZ as day0,
		s1.day0 as sensor_day0,
		datediff(status_end_date, day0) as total_treatment_days,
		s1.day10,
		s1.day26,
		s1.day35,
		s1.day81,
		s1.day90,
		s1.day171,
		s1.day180,
		s1.day261, 
		s1.day270,
		s1.day351, 
		s1.day360,
		if(s2.bloodworkdate = s1.day35, s1.day35, max(s2.bloodworkdate)) as lab_day35,
		if(s3.bloodworkdate = s1.day90, s1.day90, max(s3.bloodworkdate)) as lab_day90,
		if(s4.bloodworkdate = s1.day180, s1.day180, max(s4.bloodworkdate)) as lab_day180,
		if(s5.bloodworkdate = s1.day270, s1.day270, max(s5.bloodworkdate)) as lab_day270,
		if(s6.bloodworkdate = s1.day360, s1.day360, max(s6.bloodworkdate)) as lab_day360
		from 
		(
			select distinct b.clientid, status_start_date as day0,
			date_add(status_start_date, interval 10 day) as day10,
			date_add(status_start_date, interval 20 day) as day20,
			date_add(status_start_date, interval 26 day) as day26,
			date_add(status_start_date, interval 35 day) as day35,
			date_add(status_start_date, interval 36 day) as day36,
			date_add(status_start_date, interval 45 day) as day45,
			date_add(status_start_date, interval 76 day) as day76,
			date_add(status_start_date, interval 81 day) as day81,
			date_add(status_start_date, interval 90 day) as day90,
			date_add(status_start_date, interval 91 day) as day91,
			date_add(status_start_date, interval 105 day) as day105,
			
			date_add(status_start_date, interval 166 day) as day166,
			date_add(status_start_date, interval 171 day) as day171,
			date_add(status_start_date, interval 180 day) as day180,
			date_add(status_start_date, interval 181 day) as day181,
			date_add(status_start_date, interval 195 day) as day195,
			
			date_add(status_start_date, interval 256 day) as day256,
			date_add(status_start_date, interval 261 day) as day261, 
			date_add(status_start_date, interval 270 day) as day270,
			date_add(status_start_date, interval 271 day) as day271,
			date_add(status_start_date, interval 285 day) as day285,
			
			date_add(status_start_date, interval 346 day) as day346,
			date_add(status_start_date, interval 351 day) as day351,
			date_add(status_start_date, interval 360 day) as day360,
			date_add(status_start_date, interval 361 day) as day361,
			date_add(status_start_date, interval 375 day) as day375,
			firstbloodworkTimeITZ, status_end_date
			from
				(
                select clientid, if(date(visitdateitz) > status_start_date or status_start_date is null, date(visitdateitz), status_start_date) as status_start_date,
                status_end_date from 
                (
					select b.clientid, b.enrollmentdateitz, visitdateitz, treatmentsetupdateITZ, b.status, 
					min(status_start_date) as status_start_date, min(status_end_date) as status_end_date
					from bi_patient_status b
                    where status = 'active'
					group by clientid, status
				) s 
                ) b 
				left join (select clientid, min(date(itz(bloodworktime))) as firstbloodworkTimeITZ from clinictestresults where deleted = 0 group by clientid) d on b.clientid = d.clientid 
			
		) s1 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where a.deleted = 0) s2 on s1.clientid = s2.clientid and ((s1.day35 = s2.bloodworkdate) or (s2.bloodworkdate between s1.day26 and s1.day35) or (s2.bloodworkdate between s1.day36 and s1.day45))
			 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where a.deleted = 0) s3 on s1.clientid = s3.clientid and ((s1.day90 = s3.bloodworkdate) or (s3.bloodworkdate between s1.day76 and s1.day90) or (s3.bloodworkdate between s1.day91 and s1.day105))
			 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where a.deleted = 0) s4 on s1.clientid = s4.clientid and ((s1.day180 = s4.bloodworkdate) or (s4.bloodworkdate between s1.day166 and s1.day180) or (s4.bloodworkdate between s1.day181 and s1.day195))
			 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where a.deleted = 0) s5 on s1.clientid = s5.clientid and ((s1.day270 = s5.bloodworkdate) or (s5.bloodworkdate between s1.day256 and s1.day270) or (s5.bloodworkdate between s1.day271 and s1.day285))
			 left join (select distinct a.clientid, date(itz(bloodworktime)) as bloodworkdate from clinictestresults a inner join v_client_tmp b on a.clientid = b.clientid where a.deleted = 0) s6 on s1.clientid = s6.clientid and ((s1.day360 = s6.bloodworkdate) or (s6.bloodworkdate between s1.day346 and s1.day360) or (s6.bloodworkdate between s1.day361 and s1.day375))
		group by s1.clientid
) b on a.clientid = b.clientid
set a.d10_date = b.day10,
	a.d35_date = b.day35,
    a.d90_date = b.day90,
    a.d180_date = b.day180,
    a.d270_date = b.day270,
    a.d360_date = b.day360,
	a.d35_date_lab = b.lab_day35,
    a.d90_date_lab = b.lab_day90,
    a.d180_date_lab = b.lab_day180,
    a.d270_date_lab = b.lab_day270,
    a.d360_date_lab = b.lab_day360,
    a.programStartDate_analytics = b.sensor_day0
where  a.is_row_current = 'Y'
and a.isRCT is null
; 


update dim_client a inner join 
(	select clientid, min(itz(bloodworktime)) as bloodworkTimeITZ, 
	max(itz(bloodworktime)) as latest_bloodworktimeITZ
    from clinictestresults 
    where bloodworktime is not null 
    and deleted = 0
    group by clientid
) c on a.clientid = c.clientid 
set  firstBloodWorkDateITZ = bloodworkTimeITZ,
	 latestBloodWorkDateITZ = if(latest_bloodworktimeITZ = bloodworkTimeITZ, null, latest_bloodworktimeITZ)
where a.is_row_current = 'Y'
;


update dim_client a inner join (
										select s.clientid, itz(VITALS_UPDATED_DATE) as dateToConsider, if(s1.height > 100, s1.height/100, s1.height) as height, s1.weight
										from 
										(
											SELECT CLIENTID, max(DATEMODIFIED)  AS VITALS_UPDATED_DATE
											FROM clinictestresults_aud B
											WHERE B.HEIGHT IS NOT NULL AND B.WEIGHT IS NOT NULL
											GROUP BY CLIENTID
										) s inner join clinictestresults_aud s1 on s.clientid = s1.clientid and s.VITALS_UPDATED_DATE = s1.datemodified
								  ) b on a.clientid = b.clientid 
set a.start_weight = b.weight,
	a.height = b.height, 
	a.start_BMI = cast(b.weight/(b.height*b.height) as decimal(10,2))
where is_row_current = 'y'
; 



update dim_client a inner join 
(
	select clientid, if(height > 100, height/100, height) as height
	from clinictestresults where height is not null
	and deleted = 0
) b on a.clientid = b.clientid 
set a.height = b.height
where a.is_row_current = 'Y'
and a.height is null
;

commit
;



		set time_zone = 'Asia/Calcutta'; 

		drop temporary table if exists tmp_first_last_BMI_from_BCM; 

		create temporary table tmp_first_last_BMI_from_BCM as 
		  select s.clientid, 
		  max(case when s.first_entry = s1.eventtime then s1.BMI else null end) as firstBMI,     
		  max(case when s.first_entry <> s.last_entry and s.last_entry = s1.eventtime then s1.BMI else null end) as lastBMI
		  from 
		  (
			select clientid, min(eventtime) as first_entry, max(eventtime) as last_entry from weights where BMI is not null group by clientid
		  ) s inner join weights s1 on s.clientid = s1.clientid and (s.last_entry = s1.eventtime or s.first_entry = s1.eventtime)
		  group by s.clientid
		;

		create index idx_tmp_first_last_BMI_from_BCM on tmp_first_last_BMI_from_BCM(clientid, firstBMI, lastBMI)
		; 

		update dim_client a inner join tmp_first_last_BMI_from_BCM b on a.clientid = b.clientid
		set a.start_BMI_bcm = b.firstBMI
		where a.is_row_current = 'Y'
		
		;

		update dim_client a inner join tmp_first_last_BMI_from_BCM b on a.clientid = b.clientid
		set a.last_available_BMI = b.lastBMI
		where a.is_row_current = 'Y'
		;

		drop temporary table if exists tmp_first_last_BMI_from_BCM
		; 
        
        
        
        
		drop temporary table if exists tmp_first_last_weight_from_BCM; 

		create temporary table tmp_first_last_weight_from_BCM as 
		  select s.clientid, 
		  max(case when s.first_entry = s1.eventtime then s1.weight else null end) as first_weight,     
		  max(case when s.first_entry <> s.last_entry and s.last_entry = s1.eventtime then s1.weight else null end) as last_weight
		  from 
		  (
			select clientid, min(eventtime) as first_entry, max(eventtime) as last_entry from weights where weight is not null group by clientid
		  ) s inner join weights s1 on s.clientid = s1.clientid and (s.last_entry = s1.eventtime or s.first_entry = s1.eventtime)
		  group by s.clientid
		;

		create index idx_tmp_first_last_weight_from_BCM on tmp_first_last_weight_from_BCM (clientid, first_weight, last_weight)
		; 

		update dim_client a inner join tmp_first_last_weight_from_BCM b on a.clientid = b.clientid
		set a.start_weight = b.first_weight
		where a.is_row_current = 'Y'
		and a.start_weight is null
		;

		update dim_client a inner join tmp_first_last_weight_from_BCM b on a.clientid = b.clientid
		set a.last_available_weight = b.last_weight
		where a.is_row_current = 'Y'
		;

		drop temporary table if exists tmp_first_last_weight_from_BCM
		; 
        
		set time_zone = 'UTC'
		;



update dim_client a inner join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
set start_labA1C = b.veinHba1c,
    start_glucose = b.glucose,
    motivations = b.hotButtonIssues
where is_row_current = 'y'
;



update dim_client a inner join clinictestresults b on a.clientid = b.clientid and a.latestBloodWorkDateITZ = itz(b.bloodworktime)
set latest_labA1C = b.veinHba1c
where is_row_current = 'y'
;


update dim_client a inner join 
(
		select clientid, count(distinct date) as treatmentDays 
		from 
		v_date_to_date a inner join bi_patient_status b on 
        (a.date > if(date(visitDateitz) > b.status_start_Date or b.status_start_Date is null, date(visitDateITZ), b.status_start_Date) 
        and date <= b.status_end_Date) and b.status = 'active'
        group by clientid
) b on a.clientid = b.clientid
set a.treatmentDays = b.treatmentDays
where a.is_row_current = 'Y'
;

	
			drop temporary table if exists tmp_bp_5d_from_sensor; 
			
			create temporary table tmp_bp_5d_from_sensor as 
			select s.clientid, 
			max(case when s.first_available_date = s1.date then s.first_available_date else null end) as firstBPavailableDate, 
			max(case when s.first_available_date = s1.date then round(sysBP_5d) else null end) as first_systolicBP,
			max(case when s.first_available_date = s1.date then round(diasBP_5d) else null end) as first_diastolicBP,
			
			max(case when s.first_available_date <> s.last_available_bp_date and s.last_available_bp_date = s1.date then s.last_available_bp_date else null end) as lastBPavailableDate, 
			max(case when s.first_available_date <> s.last_available_bp_date and s.last_available_bp_date = s1.date then round(sysBP_5d) else null end) as last_systolicBP,
			max(case when s.first_available_date <> s.last_available_bp_date and s.last_available_bp_date = s1.date then round(diasBP_5d) else null end) as last_diastolicBP
			from 
			(
				select clientid, min(date) as first_available_date, max(date) as last_available_bp_date 
				from bi_patient_reversal_state_gmi_x_date 
				where sysBP_5d is not null 
				group by clientid
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and (s.first_available_date = s1.date or s.last_available_bp_date = s1.date) 
			group by s.clientid
			;
			
			create index idx_tmp_bp_5d_from_sensor on tmp_bp_5d_from_sensor(clientid asc, first_systolicBP, first_diastolicBP, last_systolicBP, last_diastolicBP)
			;
			
			update dim_client a inner join tmp_bp_5d_from_sensor b on (a.clientid = b.clientid)
			set a.start_systolicBP = b.first_systolicBP,
				a.start_diastolicBP = b.first_diastolicBP,
				a.last_systolicBP = b.last_systolicBP,
				a.last_diastolicBP = b.last_diastolicBP
			where a.is_row_current = 'Y'
			;
			
			drop temporary table if exists tmp_bp_5d_from_sensor
			;
			
			commit; 
			
			
			drop temporary table if exists tmp_bp_maintenace; 

			create temporary table tmp_bp_maintenace as 
			select clientid,
			max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
			max(start_sys_BP) as start_sys_BP,
			max(start_Dias_BP) as start_dias_BP,
			max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
			max(last_sys_bp) as last_sys_bp,
			max(last_dias_bp) as last_dias_bp
			from
			(
				select s.clientid,
				case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
				case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then systolicBP else null end as start_sys_BP,
				case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then diastolicBP else null end as start_dias_BP,
				case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate,
				case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then systolicBP else null end as last_sys_BP,
				case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then diastolicBP else null end as last_dias_BP
				from
				(
						select clientid, min(ifnull(bloodworktime, eventtime))as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate 
						from clinictestresults
						where systolicBP is not null and diastolicBP is not null
						and deleted = 0
						group by clientid
				) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
			) s   
			group by clientid
			;

			create index idx_tmp_bp_maintenace on tmp_bp_maintenace(clientid, start_sys_BP, start_Dias_BP, last_sys_BP, last_dias_BP); 

			update dim_client a inner join tmp_bp_maintenace b on (a.clientid = b.clientid)
			set a.start_systolicBP = b.start_sys_BP,
				a.start_diastolicBP = b.start_Dias_BP,
				a.last_systolicBP = b.last_sys_BP,
				a.last_diastolicBP = b.last_dias_BP
			where a.is_row_current = 'Y'
			and a.start_systolicBP is null and a.start_diastolicBP is null
			;


			drop temporary table if exists tmp_bp_maintenace
			;
            
            commit
            ;


	drop temporary table if exists tmp_eGFR_cretinine_start_latest_measures;

	create temporary table tmp_eGFR_cretinine_start_latest_measures as 
			select s.clientid, s.start_creatinine, s.latest_creatinine,
			cast((141 * 
				power(if((start_creatinine/eGFR_Constant_K) < 1, (start_creatinine/eGFR_Constant_K), 1), eGFR_Constant_Alpha) * 
				power(if((start_creatinine/eGFR_Constant_K) > 1, (start_creatinine/eGFR_Constant_K), 1), -1.209) * 
				power(0.993, age) * 
				eGFR_Constant_Gender) as decimal(7,2)) as start_eGFR,
			if(latest_creatinine is null, null, cast((141 * 
				power(if((latest_creatinine/eGFR_Constant_K) < 1, (latest_creatinine/eGFR_Constant_K), 1), eGFR_Constant_Alpha) * 
				power(if((latest_creatinine/eGFR_Constant_K) > 1, (latest_creatinine/eGFR_Constant_K), 1), -1.209) * 
				power(0.993, age) * 
				eGFR_Constant_Gender) as decimal(7,2))) as latest_eGFR
			from 
			(
					select s1.clientid, s3.gender, s3.age, 
					if(s3.gender = 'FEMALE', 0.7, 0.9) as eGFR_Constant_K, 
					if(s3.gender = 'FEMALE', -0.329, -0.411) as eGFR_Constant_Alpha, 
					if(s3.gender = 'FEMALE', 1.018, 1) as eGFR_Constant_Gender, 
					max(case when s1.firstBloodWorkDateITZ = itz(s2.bloodworktime) then s2.creatinine else null end) as start_creatinine, 
					max(case when s1.latestBloodWorkDateITZ = itz(s2.bloodworktime) then s2.creatinine else null end) as latest_creatinine
					from 
					dim_client s1 inner join clinictestresults s2 on s1.clientid = s2.clientid and (s1.firstBloodWorkDateITZ = itz(s2.bloodworktime) or s1.latestBloodWorkDateITZ = itz(s2.bloodworktime))
								  inner join v_client_tmp s3 on s1.clientid = s3.clientid
					where s1.is_row_current = 'Y'
					group by s1.clientid
			) s 
	  ;      
	create index idx_tmp_eGFR_cretinine_start_latest_measures on tmp_eGFR_cretinine_start_latest_measures(clientid asc, start_creatinine, latest_creatinine, start_eGFR, latest_eGFR); 

	update dim_client a inner join 	tmp_eGFR_cretinine_start_latest_measures b on a.clientid = b.clientid
	set a.start_creatinine = b.start_creatinine,
		a.latest_creatinine = b.latest_creatinine,
		a.start_eGFR = b.start_eGFR,
		a.latest_eGFR = b.latest_eGFR
	where is_row_current = 'Y'
	;

	drop temporary table if exists tmp_eGFR_cretinine_start_latest_measures
	;
    

drop temporary table if exists tmp_tg_hdl_ratio_maintenace; 

create temporary table tmp_tg_hdl_ratio_maintenace as 
select clientid,
max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
max(start_tg_hdl_ratio) as start_tg_hdl_ratio,

max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
max(last_tg_hdl_ratio) as last_tg_hdl_ratio
from
(
	select s.clientid,
	case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
    case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cast(triglycerides/hdlCholesterol as decimal(10,2)) else null end as start_tg_hdl_ratio,

	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate,
	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cast(triglycerides/hdlCholesterol as decimal(10,2))  else null end as last_tg_hdl_ratio
	from
	(
			select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
			where triglycerides is not null and hdlCholesterol is not null
			and deleted = 0
			group by clientid
	) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
) s
group by clientid
;

create index idx_tmp_tg_hdl_ratio_maintenace on tmp_tg_hdl_ratio_maintenace(clientid, start_tg_hdl_ratio, last_tg_hdl_ratio); 

update dim_client a inner join tmp_tg_hdl_ratio_maintenace b on (a.clientid = b.clientid)
set a.start_tg_hdl_ratio = b.start_tg_hdl_ratio,
	a.last_tg_hdl_ratio = b.last_tg_hdl_ratio
where a.is_row_current = 'Y'
;


		drop temporary table if exists tmp_liverHealth_maintenace; 

		create temporary table tmp_liverHealth_maintenace as 
		select clientid,
		max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
		max(start_tg) as start_tg,
		max(start_hdl) as start_hdl,
		max(start_fasting_glucose) as start_fasting_glucose, 
		max(start_tg_hdl_ratio) as start_tg_hdl_ratio,
		max(start_ast) as start_ast,
		max(start_alt) as start_alt, 
		max(start_insulin) as start_insulin,
		max(start_ast_alt_ratio) as start_ast_alt_ratio,

		max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
		max(last_tg) as last_tg,
		max(last_hdl) as last_hdl,
		max(last_fasting_glucose) as last_fasting_glucose,
		max(last_tg_hdl_ratio) as last_tg_hdl_ratio,
		max(last_ast) as last_ast,
		max(last_alt) as last_alt,
		max(last_insulin) as last_insulin,
		max(last_ast_alt_ratio) as last_ast_alt_ratio
		from
		(
			select s.clientid,
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then triglycerides else null end as start_tg,
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then hdlCholesterol else null end as start_hdl, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then glucose else null end as start_fasting_glucose, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then ast else null end as start_ast, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then alt else null end as start_alt, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cast(ast/alt as decimal(10,2)) else null end as start_ast_alt_ratio, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then insulin else null end as start_insulin, 
			case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cast(triglycerides/hdlCholesterol as decimal(10,2)) else null end as start_tg_hdl_ratio,

			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then triglycerides else null end as last_tg,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then hdlCholesterol else null end as last_hdl,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then glucose else null end as last_fasting_glucose,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cast(triglycerides/hdlCholesterol as decimal(10,2))  else null end as last_tg_hdl_ratio,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then ast else null end as last_ast,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then alt else null end as last_alt,
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then insulin else null end as last_insulin, 
			case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cast(ast/alt as decimal(10,2))  else null end as last_ast_alt_ratio
			from
			(
					select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
					group by clientid
			) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
		) s
		group by clientid
		;

		create index idx_tmp_liverHealth_maintenace on tmp_liverHealth_maintenace (clientid asc, first_value_available_bloodworkDate, last_value_available_bloodworkDate); 

		drop temporary table if exists tmp_nafld_lfs; 

		create temporary table tmp_nafld_lfs as 
		select s.*, if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2) as last_diabetes_flag,

		if(start_BMI_bcm is null, null, -2.89 + (1.18 * (if(is_StartMetabolicSyndrome = 'Y', 1, 0))) + (0.45 * 2) + (0.15 * start_insulin) + (0.04 * start_ast) - (0.94 * start_ast_alt_ratio)) as start_NAFLD_LFS,
		if(last_available_BMI is null, null, -2.89 + (1.18 * (if(is_LastMetabolicSyndrome = 'Y', 1, 0))) + (0.45 * (if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2))) + (0.15 * last_insulin) + (0.04 * last_ast) - (0.94 * last_ast_alt_ratio)) as last_NAFLD_LFS

		from 
		(
			select s.*, 
			case when s.start_BMI_bcm >= 25 or s.start_tg >= 150 or s.start_hdl < if(gender = 'MALE', 40, 50) or s.start_fasting_glucose >= 100 or (ifnull(s.start_systolicBP,0) >= 130 or ifnull(s.start_diastolicBP,0) >= 85) then 'Y' else 'N' end as is_StartMetabolicSyndromeOLD,
			case when s.last_available_BMI >= 25 or s.last_tg >= 150 or s.last_hdl < if(gender = 'MALE', 40, 50)  or s.last_fasting_glucose >= 100 or (ifnull(s.last_systolicBP,0) >= 130 or ifnull(s.last_diastolicBP,0) >= 85) then 'Y' else 'N' end as is_LastMetabolicSyndromeOLD,
			
			case when (BMI_flag + TG_flag + HDL_flag + fastingGlucose_flag + BP_flag) >= 3 then 'Y' else 'N' end as is_StartMetabolicSyndrome,
			case when (last_BMI_flag + last_TG_flag + last_HDL_flag + last_fastingGlucose_flag + last_BP_flag) >= 3 then 'Y' else 'N' end as is_LastMetabolicSyndrome
		   
			
			from 
			(
				select a.clientid, a.start_systolicBP, a.start_diastolicBP, a.start_BMI_bcm, b.start_tg, b.start_hdl, b.start_fasting_glucose, start_ast, start_alt, start_ast_alt_ratio, start_insulin, 
				a.last_systolicBP, a.last_diastolicBP, a.last_available_BMI, last_ast, last_alt, last_ast_alt_ratio, last_insulin, last_hdl, last_fasting_glucose, last_tg, c.gender, c.status,
				if(a.start_BMI_bcm is not null and a.start_BMI_bcm >= 25, 1, 0) as BMI_flag, 
				if(b.start_tg is not null and b.start_tg >= 150, 1, 0) as TG_flag, 
				if(b.start_hdl is not null and b.start_hdl < if(gender = 'MALE', 40, 50), 1, 0) as HDL_flag, 
				if(b.start_fasting_glucose is not null and b.start_fasting_glucose >= 100 , 1, 0) as fastingGlucose_flag,
				if((a.start_systolicBP is not null or a.start_diastolicBP is not null) and (ifnull(a.start_systolicBP,0) >= 130 or ifnull(a.start_diastolicBP,0) >= 85), 1, 0) as BP_flag, 
				
				if(a.last_available_BMI is not null and a.last_available_BMI >= 25, 1, 0) as last_BMI_flag, 
				if(b.last_tg is not null and b.last_tg >= 150, 1, 0) as last_TG_flag, 
				if(b.last_hdl is not null and b.last_hdl < if(gender = 'MALE', 40, 50), 1, 0) as last_HDL_flag, 
				if(b.last_fasting_glucose is not null and b.last_fasting_glucose >= 100 , 1, 0) as last_fastingGlucose_flag,
				if((a.last_systolicBP is not null or a.last_diastolicBP is not null) and (ifnull(a.last_systolicBP,0) >= 130 or ifnull(a.last_diastolicBP,0) >= 85), 1, 0) as last_BP_flag
					  
				
				from dim_client a left join tmp_liverHealth_maintenace b on a.clientid = b.clientid
								  left join v_allClient_list c on a.clientid = c.clientid 
				where a.is_row_current = 'Y'
			) s 
			
		) s left join (	
							select s.clientid, is_InReversal, is_metOnly_by_ops
							from 
							(
								select a.clientid, case when status = 'Active' then date_sub(date(itz(now())), interval 1 day) else b.date end as dateChosen
								from v_client_tmp a inner join 
								(
									select clientid, max(date) as date 
									from bi_patient_reversal_state_gmi_x_date
									group by clientid 
								) b on a.clientid = b.clientid 
							) s left join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s1.date = dateChosen
						) s1 on s.clientid = s1.clientid
		;

		create index idx_tmp_nafld_lfs on tmp_nafld_lfs (clientid asc, start_NAFLD_LFS, last_NAFLD_LFS); 

		update dim_client a inner join tmp_nafld_lfs b on a.clientid = b.clientid 
		set a.start_NAFLD_LFS = b.start_NAFLD_LFS,
			a.last_NAFLD_LFS = b.last_NAFLD_LFS
		where a.is_row_current = 'Y'
		; 

		drop temporary table if exists tmp_nafld_lfs;

		drop temporary table if exists tmp_liverHealth_maintenace; 


drop temporary table if exists tmp_heartHealth_maintenace; 

create temporary table tmp_heartHealth_maintenace as 
select clientid,
max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
max(start_hdl) as start_hdl,
max(start_TotalCholesterol) as start_TotalCholesterol, 


max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
max(last_hdl) as last_hdl,
max(last_TotalCholesterol) as last_TotalCholesterol,

max(useSmoking) as useSmoking
from
(
	select s.clientid,
	case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
    case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then hdlCholesterol else null end as start_hdl, 
    case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then cholesterol else null end start_TotalCholesterol, 

	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate,
    case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then hdlCholesterol else null end as last_hdl,
	case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then cholesterol else null end as last_TotalCholesterol, 

	case when s2.clientid is not null then 'Y' else 'N' end as useSmoking
	from
	(
			select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
			where Cholesterol is not null and hdlCholesterol is not null
			and deleted = 0
			group by clientid
	) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
		left join 			
            (
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork from clinictestresults
				where useSmoking = 1
				and deleted = 0
				group by clientid
            ) s2 on s.clientid = s2.clientid 
) s
group by clientid
;

create index idx_tmp_heartHealth_maintenace on tmp_heartHealth_maintenace(clientid asc, first_value_available_bloodworkDate, last_value_available_bloodworkDate); 



set @M_age := 3.06117, @M_TC := 1.12370, @M_HDL := -0.93263, @M_BP_TRT := 1.99881, @M_BP_NonTRT := 1.93303, @M_SMOKE := 0.65451, @M_DIAB := 0.57367; 
set @F_age := 2.32888, @F_TC := 1.20904, @F_HDL := -0.70833, @F_BP_TRT := 2.82263, @F_BP_NonTRT := 2.76157, @F_SMOKE := 0.52873, @F_DIAB := 0.69154; 


drop temporary table if exists tmp_FRS_score;
create temporary table tmp_FRS_score as 
select clientid, total_start_coeff, total_last_coeff, 
cast(case when gender = 'MALE' then 1 - power(0.88936, exp(total_start_coeff - 23.9802)) 
		  when gender = 'FEMALE' then 1 - power(0.95012, exp(total_start_coeff - 26.1931)) else null end * 100  as decimal(6,2)) as start_FRS,
     
cast(case when gender = 'MALE' then 1 - power(0.88936, exp(total_last_coeff - 23.9802))
		  when gender = 'FEMALE' then 1 - power(0.95012, exp(total_last_coeff - 26.1931)) else null end * 100 as decimal(6,2)) as last_FRS     
from 
(
	select clientid, gender, age, 
	cast((age_coeff + sysBP_coeff + totalCholesterol_coeff + totalHDL_coeff + smoke_coeff + diab_coeff) as decimal(10,4)) as total_start_coeff, 
	cast((age_coeff + last_sysBP_coeff + last_totalCholesterol_coeff + last_totalHDL_coeff + smoke_coeff + last_diab_coeff) as decimal(10,4)) as total_last_coeff
	from 
	( 
			select s.clientid, gender, age,
			log_age * if(gender = 'MALE', @M_age, @F_age) as age_coeff, 
			log_start_sysBP * case when gender = 'MALE' and isHTNTreated = 'Y' then @M_BP_TRT
								   when gender = 'MALE' and isHTNTreated = 'N' then @M_BP_NonTRT
								   when gender = 'FEMALE' and isHTNTreated = 'Y' then @F_BP_TRT
								   when gender = 'FEMALE' and isHTNTreated = 'N' then @F_BP_NonTRT else null end as sysBP_coeff, 
			log_startTotalCholesterol * if(gender = 'MALE', @M_TC, @F_TC) as totalCholesterol_coeff, 
			log_start_hdl * if(gender = 'MALE', @M_HDL, @F_HDL) as totalHDL_coeff, 
			if(useSmoking = 'Y', 1, 0) * if(gender = 'MALE', @M_SMOKE, @F_SMOKE)  as smoke_coeff, 
			1 * if(gender = 'MALE', @M_DIAB, @F_DIAB) as diab_coeff,


			log_last_sysBP * case when gender = 'MALE' and isHTNTreated = 'Y' then @M_BP_TRT
								   when gender = 'MALE' and isHTNTreated = 'N' then @M_BP_NonTRT
								   when gender = 'FEMALE' and isHTNTreated = 'Y' then @F_BP_TRT
								   when gender = 'FEMALE' and isHTNTreated = 'N' then @F_BP_NonTRT else null end as last_sysBP_coeff, 
			log_lastTotalCholesterol * if(gender = 'MALE', @M_TC, @F_TC) as last_totalCholesterol_coeff, 
			log_last_hdl * if(gender = 'MALE', @M_HDL, @F_HDL) as last_totalHDL_coeff, 
			if(is_InReversal = 'Yes' or is_metOnly_by_ops= 'Yes', 0, 1) * if(gender = 'MALE', @M_DIAB, @F_DIAB) as last_diab_coeff
			from 
			(
				select a.clientid, b.start_TotalCholesterol, b.last_TotalCholesterol, b.start_hdl, b.last_hdl, 
				b.useSmoking, c.age, a.start_systolicBP, a.last_systolicBP, if(a.start_htn_medicine_drugs is not null, 'Y', 'N') as isHTNTreated, c.gender,
				if(b.start_TotalCholesterol <= 0, null, ln(b.start_TotalCholesterol)) as log_startTotalCholesterol, 
				if(b.last_TotalCholesterol <= 0, null, ln(b.last_TotalCholesterol)) as log_lastTotalCholesterol, 
				if(b.start_hdl <= 0, null, ln(b.start_hdl)) as log_start_hdl,
				if(b.last_hdl <= 0, null, ln(b.last_hdl)) as log_last_hdl,
				if(c.age <= 0, null, ln(age)) as log_age,
				if(a.start_systolicBP <= 0, null, ln(start_systolicBP)) as log_start_sysBP,
				if(a.last_systolicBP <= 0, null, ln(last_systolicBP)) as log_last_sysBP
				from 
				dim_client a left join tmp_heartHealth_maintenace b on a.clientid = b.clientid
							 left join v_client_tmp c on a.clientid = c.clientid
				where a.is_row_current = 'Y' 
			) s left join (	
								select s.clientid, is_InReversal, is_metOnly_by_ops
								from 
								(
									select a.clientid, case when status = 'Active' then date_sub(date(itz(now())), interval 1 day) else b.date end as dateChosen
									from v_client_tmp a inner join 
									(
										select clientid, max(date) as date 
										from bi_patient_reversal_state_gmi_x_date
										group by clientid 
									) b on a.clientid = b.clientid 
								) s left join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s1.date = dateChosen
							) s1 on s.clientid = s1.clientid
	) s 
) s 
; 

create index idx_tmp_FRS_score on tmp_FRS_score (clientid asc, start_FRS, last_FRS); 

update dim_client a inner join tmp_FRS_score b on a.clientid = b.clientid 
set a.start_FRS = b.start_FRS,
	a.last_FRS = b.last_FRS
where a.is_row_current = 'Y'
; 

drop temporary table if exists tmp_FRS_score;

drop temporary table if exists tmp_heartHealth_maintenace; 



	drop temporary table if exists tmp_hsCRP_maintenace; 

	create temporary table tmp_hsCRP_maintenace as 
	select clientid,
	max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
	max(start_hsCRP) as start_hsCRP,
	max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
	max(last_hsCRP) as last_hsCRP
	from
	(
		select s.clientid,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then hsCReactive else null end as start_hsCRP,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then hsCReactive else null end as last_hsCRP
		from
		(
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
				where hsCReactive is not null
				and deleted = 0
				group by clientid
		) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
	) s
	group by clientid
	;

	create index idx_tmp_hsCRP_maintenace on tmp_hsCRP_maintenace(clientid, start_hsCRP, last_hsCRP); 

	update dim_client a inner join tmp_hsCRP_maintenace b on (a.clientid = b.clientid)
	set a.start_hsCRP = b.start_hsCRP,
		a.last_hsCRP = b.last_hsCRP
	where a.is_row_current = 'Y'
	;

	drop temporary table if exists tmp_hsCRP_maintenace
	;


	drop temporary table if exists tmp_homa2IR_maintenace; 

	create temporary table tmp_homa2IR_maintenace as 
	select clientid,
	max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
	max(start_homa2IR) as start_homa2IR,
	max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
	max(last_homa2IR) as last_homa2IR
	from
	(
		select s.clientid,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then homa2IR else null end as start_homa2IR,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then homa2IR else null end as last_homa2IR
		from
		(
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
				where homa2IR is not null
				and deleted = 0
				group by clientid
		) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
	) s
	group by clientid
	;

	create index idx_tmp_homa2IR_maintenace on tmp_homa2IR_maintenace(clientid, start_homa2IR, last_homa2IR); 

	update dim_client a inner join tmp_homa2IR_maintenace b on (a.clientid = b.clientid)
	set a.start_homa2IR = b.start_homa2IR,
		a.last_homa2IR = b.last_homa2IR
	where a.is_row_current = 'Y'
	;

	drop temporary table if exists tmp_homa2IR_maintenace
	;


	drop temporary table if exists tmp_homa2b_maintenace; 

	create temporary table tmp_homa2b_maintenace as 
	select clientid,
	max(first_value_available_bloodworkDate) as first_value_available_bloodworkDate,
	max(start_homa2b) as start_homa2b,
	max(last_value_available_bloodworkDate) as last_value_available_bloodworkDate,
	max(last_homa2b) as last_homa2b
	from
	(
		select s.clientid,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then itz(s.firstbloodwork) else null end as first_value_available_bloodworkDate,
		case when s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime) then homa2b else null end as start_homa2b,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then itz(s.last_value_available_bloodworkDate) else null end as last_value_available_bloodworkDate,
		case when s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) and s.firstbloodwork <> s.last_value_available_bloodworkDate then homa2b else null end as last_homa2b
		from
		(
				select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork, max(ifnull(bloodworktime, eventtime)) as last_value_available_bloodworkDate  from clinictestresults
				where homa2b is not null
				and deleted = 0
				group by clientid
		) s inner join clinictestresults s1 on s.clientid = s1.clientid and (s.last_value_available_bloodworkDate = ifnull(s1.bloodworktime, s1.eventtime) or s.firstbloodwork = ifnull(s1.bloodworktime, s1.eventtime))
	) s
	group by clientid
	;

	create index idx_tmp_homa2b_maintenace on tmp_homa2b_maintenace(clientid, start_homa2b, last_homa2b); 

	update dim_client a inner join tmp_homa2b_maintenace b on (a.clientid = b.clientid)
	set a.start_homa2b = b.start_homa2b,
		a.last_homa2b = b.last_homa2b
	where a.is_row_current = 'Y'
	;

	drop temporary table if exists tmp_homa2b_maintenace
	;
    

drop temporary table if exists tmp_d10_clients; 

create temporary table tmp_d10_clients as 
select clientid, day10, success_ind from v_bi_patient_d10_detail_internal
where day10 >= date_sub(date(itz(now())), interval 2 month) -- using this condition to avoid restatement of older patients for whom the D10 success logic was different.
;


update dim_client a inner join 
tmp_d10_clients b on a.clientid = b.clientid 
set a.d10_success = b.success_ind,
	a.d10_date_internal = b.day10
where a.is_row_current = 'Y'
;

drop temporary table if exists tmp_d10_clients; 


drop temporary table if exists tmp_d20_clients; 

create temporary table tmp_d20_clients as 
select clientid, day20, success_ind from v_bi_patient_d20_detail_internal
where day20 >= date_sub(date(itz(now())), interval 2 month) -- using this condition to avoid restatement of older patients for whom the D10 success logic was different.
;


update dim_client a inner join 
tmp_d20_clients b on a.clientid = b.clientid 
set a.d20_success = b.success_ind,
	a.d20_date_internal = b.day20
where a.is_row_current = 'Y'
;

drop temporary table if exists tmp_d20_clients; 


drop temporary table if exists tmp_d35_clients; 

create temporary table tmp_d35_clients as 
select clientid, day35, success_ind from v_bi_patient_d35_detail_internal
where day35 >= date_sub(date(itz(now())), interval 2 month) -- using this condition to avoid restatement of older patients for whom the D10 success logic was different.
;


update dim_client a inner join 
tmp_d35_clients b on a.clientid = b.clientid 
set a.d35_success = b.success_ind,
	a.d35_date_internal = b.day35
where a.is_row_current = 'Y'
;

drop temporary table if exists tmp_d35_clients; 




update dim_client a inner join 
(
	select clientid, group_concat(name) as startSymps 
	from clients a left join clinicreportconditions b on a.id = b.clientid 
	where type = 'symptom' and hascondition = 1
	
	group by clientid
) b on a.clientid = b.clientid
set a.start_symptoms  = b.startSymps
where  a.is_row_current = 'Y'
;


update dim_client a inner join 
(
    select clientid, group_concat(distinct ifnull(description,name)) as start_allergies
	from clients a left join clinicreportconditions b on a.id = b.clientid
	where type = 'ALLERGY' and hascondition = 1
	
	group by clientid
) b on a.clientid = b.clientid
set a.start_allergies  = b.start_allergies
where  a.is_row_current = 'Y'
;


update dim_client a inner join 
(
    select clientid, group_concat(distinct name) as start_conditions
	from clients a left join clinicreportconditions b on a.id = b.clientid
	where type = 'DISORDER' and hascondition = 1
	
	group by clientid
) b on a.clientid = b.clientid
set a.start_conditions  = b.start_conditions
where  a.is_row_current = 'Y'
;


update dim_client a inner join 
(
		select a.clientid, group_concat(distinct c.medicineName) as medicinename , group_concat(distinct pc.epc, '(',pc.category,')') as drugs
		from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
		left  join medicines c on a.medicationId = c.medicineID
		left join medicinedrugs d on c.medicineid = d.medicineid 
		left join drugs e on d.drugid = e.drugid
		left join drugclasses f on e.drugclassid = f.drugclassid 
        left join medicinepharmclasses mp on c.medicineid = mp.medicineid
        left join pharmclasses pc on mp.pharmClassId = pc.pharmClassId
		where hasMedicine = 1 
		group by clientid
) b on a.clientid = b.clientid and a.is_row_current = 'Y'
set a.start_medicine  = b.medicinename
;




update dim_client a inner join 
(
		select a.clientid, group_concat(distinct c.medicineName) as medicinename , group_concat(distinct pc.epc, '(',pc.category,')') as drugs
		from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
		left  join medicines c on a.medicationId = c.medicineID
		left join medicinedrugs d on c.medicineid = d.medicineid 
		left join drugs e on d.drugid = e.drugid
		left join drugclasses f on e.drugclassid = f.drugclassid 
        left join medicinepharmclasses mp on c.medicineid = mp.medicineid
        left join pharmclasses pc on mp.pharmClassId = pc.pharmClassId
		where hasMedicine = 1 
		and pc.category in ('Insulin','DIABETES')
		group by clientid
) b on a.clientid = b.clientid 
set a.start_medicine_diabetes = b.medicinename,
	a.start_medicine_diabetes_drugs = b.drugs
where a.is_row_current = 'y'
;



update dim_client a inner join 
(
		select a.clientid, group_concat(distinct case when f.subcategory = 'CHOLESTEROL' then 'CHOLES' 
													  when f.subcategory = 'HYPERTENSION' then 'HTN'
													  when f.subcategory = 'HYPOTHYROIDISM' then 'HYPO-THYROID'
                                                      when f.subcategory = 'HEARTDISEASE' then 'HEART-DIS'
                                                      else 'OTH' end ) as start_nondiabetic_conditions
		from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
		left  join medicines c on a.medicationId = c.medicineID
		left join medicinedrugs d on c.medicineid = d.medicineid 
		left join drugs e on d.drugid = e.drugid
		left join drugclasses f on e.drugclassid = f.drugclassid 
		where hasMedicine = 1 
		and f.category not in ('Insulin','DIABETES')
		group by clientid
) b on a.clientid = b.clientid 
set a.start_nondiabetic_conditions = b.start_nondiabetic_conditions
where a.is_row_current = 'y'
;


drop temporary table if exists tmp_allActive_patients_lastDay_med;

create temporary table tmp_allActive_patients_lastDay_med 
(
	clientid int not null,
	eventdate date not null,
	all_med_drug varchar(1000) null,
	total_meds int null,
	insulin_units int null
); 

insert into tmp_allActive_patients_lastDay_med(clientid, eventdate, all_med_drug, total_meds, insulin_units)
select clientid, eventdate, group_concat(ltrim(rtrim(med_drug))) as all_med_drug, sum(total_meds) as total_meds, sum(insulin_units) as insulin_units
from 
(
	
	select clientId, eventdate, group_concat(concat(' ', TYPE) order by Type) as med_drug, sum(case when type like '%insulin%' then unit else null end) as insulin_units,
	count(distinct type) as total_meds
	from 
	(
			select a.clientId, date(scheduledLocalTime) as eventDate, Type,
			case when type = 'GLICLAZIDE' then 'Gc'
			  when type = 'GLIMEPIRIDE' then 'Gm'
			  when type = 'JANUVIA' then 'Jv'
			  when type like 'JANUMET%' then 'Jm'
			else left(type,1) end as med, if(targetValue < 1, left(targetValue,3),cast(floor(targetValue) as char(5)))  as unit,   count(type)  as amt  
			from clienttodoitems a 
			where category = 'medicine' and type not in ('medicine')
			and a.status in ('FINISHED','UNFINISHED')
			and date(a.scheduledLocalTime) = date_sub(date(itz(now())), interval 1 day)
			group by clientId, date(scheduledLocalTime), type , targetValue
		) s  
	group by clientId, eventDate
	
	union all 

	
	select clientid, eventdate, group_concat(distinct med_drug) as med_drug, sum(case when med_drug like '%Insu%' then qty else null end) as insulin_units,
	sum(total_drugs) as total_meds    
	from 
	(
		select a.clientid, eventdate, case when b.medicinename = 'GLICLAZIDE' then 'Gc'
				  when b.medicinename = 'GLIMEPIRIDE' then 'Gm'
				  when b.medicinename = 'JANUVIA' then 'Jv'
				  when b.medicinename like 'JANUMET%' then 'Jm'
				  when b.medicinename = 'METFORMIN' then 'M'
				  else b.medicineName end as medicine, qty, b.unit, group_concat(distinct pc.epc,' ','(',left(pc.category,4) ,')') as med_drug, count(distinct medicineName) as amt, count(pc.epc) as total_drugs
				  
		from (
					select a.clientid, date(scheduledLocalTime) as eventdate, medicineid, sum(floor(targetvalue)) as qty 
					from clienttodoitems a 
					where category = 'medicine' and type='medicine' and a.status in ('FINISHED','UNFINISHED')
					and date(a.scheduledLocalTime) = date_sub(date(itz(now())), interval 1 day)
					group by clientid, date(scheduledLocalTime), medicineid
			  ) a left join medicines b on a.medicineid = b.medicineid 
							   left join medicinepharmclasses mp on b.medicineid = mp.medicineid
							   left join pharmclasses pc on mp.pharmClassId = pc.pharmClassId
			group by clientid, eventdate, medicinename
	) s group by clientid, eventdate
) s 
group by clientid, eventdate
;

create index idx_tmp_allActive_patients_lastDay_med on tmp_allActive_patients_lastDay_med(clientid asc); 

update dim_client a inner join 
tmp_allActive_patients_lastDay_med b on a.clientid = b.clientid 
set a.is_currently_metformin_only = (case when ltrim(rtrim(b.all_med_drug)) = 'METFORMIN' or ltrim(rtrim(b.all_med_drug)) = 'Biguanide (DIAB)' then 'Yes' else null end),
	a.is_currently_on_insulin = (case when ltrim(rtrim(b.all_med_drug)) like '%insu%' then 'Yes' else null end),
    a.current_insulin_units = b.insulin_units,
    a.current_medicine_drugs = ltrim(rtrim(all_med_drug))
where a.is_row_current = 'Y'
;

drop temporary table if exists tmp_allActive_patients_lastDay_med
;


update dim_client a inner join 
(
		select clientid, eventdate, sum(case when category = 'Insulin' then total_units end) as total
		from                                  
		(
				select clientid, eventdate, category, sum(total_units) as total_units
				from 
				(
					select a.clientid, date(itz(eventtime)) as eventdate, pc.category, c.medicineName, group_concat( distinct pc.epc) as drugs, 
                    breakfastDosage+lunchDosage+dinnerDosage as total_units
					from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
					inner join medicines c on a.medicationId = c.medicineID
					left join medicinepharmclasses mp on c.medicineid = mp.medicineid
					left join pharmclasses pc on mp.pharmClassId = pc.pharmClassId
					where hasMedicine = 1 
					and pc.category in ('Insulin')
					group by clientid, date(itz(eventtime)), pc.category, c.medicineName
				) s 
				group by clientid, eventdate, category
			) s1
            group by clientid, eventdate
) b on a.clientid = b.clientid 
set a.start_insulin_units = b.total
where a.is_row_current = 'y'
;


update dim_client a inner join 
clinicreportconditions b on (a.clientid = b.clientid and b.name like '%diab%' and b.hascondition = 1)
set a.durationYears_diabetes = b.durationYears
where a.is_row_current = 'y'
;


update dim_client a inner join 
(
		select distinct clientid from v_client_tmp a 
		where exists 
		(select 1 from clinictestresults b where a.clientid = b.clientid and b.gada > 17)
)  b on a.clientid = b.clientid 
set is_type1 = 'Yes' 
where is_row_current = 'Y'
;


drop temporary table if exists tmp_ttp_enrollments
; 

create temporary table tmp_ttp_enrollments as 
select clientid, case when b.is_patient_currently_TTP = 'Yes' then 'Yes' 
					  when b.is_patient_converted is null and b.is_patient_currently_TTP is null then 'Yes' else null end as isM1,  
case when b.is_patient_converted = 'Yes' then 'Yes' else null end as isM2Converted
from bi_ttp_enrollments b
;

create index idx_tmp_ttp_enrollments on tmp_ttp_enrollments(clientid asc, isM1, isM2Converted); 

update dim_client a inner join tmp_ttp_enrollments b on a.clientid = b.clientid 
set a.isM1 = b.isM1,
	a.isM2Converted = b.isM2Converted
where a.is_row_current = 'Y'
;

drop temporary table if exists tmp_ttp_enrollments
; 



drop temporary table if exists tmp_patient_in_reversal_data;
drop temporary table if exists tmp_patient_in_reversal_data1;

create temporary table tmp_patient_in_reversal_data as 
select clientid, date, is_InReversal as Included_or_not_InREVERSAL 
from bi_patient_reversal_state_gmi_x_date
where date between date_sub(date(itz(now())), interval 51 day) and date_sub(date(itz(now())), interval 1 day)
;

create temporary table tmp_patient_in_reversal_data1 as select * from tmp_patient_in_reversal_data;

drop temporary table if exists tmp_journey_decider;
create temporary table tmp_journey_decider
(
clientid int not null,
recent_consecutive_reversaldays int null,
journey varchar(10) null,
nut_syntax varchar(10) null
); 

insert into tmp_journey_decider (clientid, recent_consecutive_reversaldays, journey, nut_syntax)
select s.clientid, recent_total_consecutivedays_InReversal, 
case when recent_total_consecutivedays_InReversal <= 14 then 'J1'
	 when recent_total_consecutivedays_InReversal between 15 and 21 then 'J2'
	 when recent_total_consecutivedays_InReversal between 22 and 45 then 'J3'
	 when recent_total_consecutivedays_InReversal > 45 then 'J4' end as journey, 
case when recent_total_consecutivedays_InReversal <= 14 then '1-4'
	 when recent_total_consecutivedays_InReversal between 15 and 21 then '1-7'
	 when recent_total_consecutivedays_InReversal between 22 and 45 then '1-10'
	 when recent_total_consecutivedays_InReversal > 45 then '1-14' end as nut_syntax
from 
(
	select curr_record as clientid, count as recent_total_consecutivedays_InReversal
	from 
	(
	select 	@prev_record := @clientid as prev_record, 
			@prev_date := @date as prev_date, 
			@prev_record_reversal := @Included_or_not_InREVERSAL as prev_record_reversal,
			@clientid := clientid as curr_record, @date := date as curr_date, @Included_or_not_InREVERSAL := Included_or_not_InREVERSAL as curr_record_reveral,
			@count := if(@prev_record = @clientid && @prev_record_reversal = @Included_or_not_InREVERSAL, @count + 1, 0) as count
	from tmp_patient_in_reversal_data a cross join (select @count := 0, @clientid := 0, @date := NULL, @Included_or_not_InREVERSAL = NULL, @prev_record = NULL, @prev_date = NULL, @prev_record_reversal = NULL) b
    order by clientid, date
	) s inner join (select clientid, max(date) as recent_date from tmp_patient_in_reversal_data1 group by clientid) s1 on s.curr_record = s1.clientid and s.curr_date = s1.recent_date
) s 
;


create index idx_tmp_journey_decider on tmp_journey_decider(clientid asc); 

update dim_client a inner join tmp_journey_decider b on a.clientid = b.clientid 
set a.recent_consecutive_reversaldays = b.recent_consecutive_reversaldays,
	a.journey = b.journey,
    a.nut_syntax = b.nut_syntax
where is_row_current = 'Y'
and status = 'active'
; 

drop temporary table if exists tmp_journey_decider;
drop temporary table if exists tmp_patient_in_reversal_data;
drop temporary table if exists tmp_patient_in_reversal_data1;




drop temporary table if exists tmp_patient_health_condition;

create temporary table tmp_patient_health_condition as 
select clientid, if(count(case when is_BP_patient = 'Yes' then date else null end) > 0 , 'Yes', 'No')  as is_HTN_patient, 
if(count(case when is_Cholesterol_patient = 'Yes' then date else null end) > 0 , 'Yes', 'No')  as is_cholesterol_patient, 
if(count(case when is_HD_patient = 'Yes' then date else null end) > 0 , 'Yes', 'No')  as is_heartdisease_patient
from 
(
select distinct clientid, date, isBP_Condition, is_BP_Med, isBP_identified, 
case when (is_BP_Med = 'Yes' or isBP_identified = 'Yes' or isBP_Condition = 'Yes' ) then 'Yes'
	 else 'No' end as is_BP_patient, 
isCholesterol_Condition, is_Cholesterol_Med, is_cholesterol_identified, 
case when (is_Cholesterol_Med = 'Yes' or is_cholesterol_identified = 'Yes' or isCholesterol_Condition = 'Yes' or is_Cholesterol_identified_first_lab = 'Yes') then 'Yes'
     else 'No' end as is_Cholesterol_patient,
is_HD_Med, is_HD_condition, 
case when is_HD_Med ='Yes' or is_HD_condition = 'Yes' then 'Yes' else 'No' end as is_HD_patient
from 
(
		select s.clientid, s.date, s1.isBP_Condition, s2.is_BP_Med, 
		case when (s3.sysBP_5d is null or s3.diasBP_5d is null) then null 
			 when (s3.sysBP_5d > 130 or s3.diasBP_5d > 80) then 'Yes' else 'No' end as isBP_identified, 
		s11.isCholesterol_Condition, s2.is_Cholesterol_Med, if(s5.clientid is not null, 'Yes', 'No') as is_cholesterol_identified, if(s6.clientid is not null, 'Yes', 'No') as is_Cholesterol_identified_first_lab,
        s2.is_HD_Med
        ,case when dc.start_nondiabetic_conditions like '%HEART-DIS%' then 'Yes' else 'No' end as is_HD_condition
		from 
		(
				select distinct clientid, date
				from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active' 
				
		) s
		left join 
		(
				select distinct clientid, case when medicalConditionId = 6 and hascondition = 1 then 'Yes' else 'No' end as isBP_Condition
				from clinicreportconditions 
				where medicalConditionID in (6)  
				and hascondition = 1
		) s1 on s.clientid = s1.clientid 
		left join 
		(
				select distinct clientid, case when (medicalConditionId = 57 or medicalConditionId = 38) and hascondition = 1 then 'Yes' else 'No' end as isCholesterol_Condition
				from clinicreportconditions 
				where medicalConditionID in (57, 38)  
				and hascondition = 1
		) s11 on s.clientid = s11.clientid 
		left join ( 
				select distinct a.clientid, da.date, category, if(count(distinct case when subcategory = 'HEARTDISEASE' then a.medicineid end) >0 ,'Yes','No') as is_HD_Med,
                if(count(distinct case when subcategory = 'HYPERTENSION' then a.medicineid end) >0 ,'Yes','No') as is_BP_Med,
                if(count(distinct case when subcategory = 'CHOLESTEROL' then a.medicineid end) >0 ,'Yes','No') as is_Cholesterol_Med
				from v_date_to_date da inner join 
									(
										select a.clientid, date(pm.startdate) as startdate, date(pm.enddate) as enddate, pm.medicineId from prescriptions a 
										inner join prescriptionmedicines pm on a.prescriptionid = pm.prescriptionid	
									)  a on da.date between a.startdate and a.enddate 
									inner join v_client_tmp b on a.clientid = b.clientid 
									inner join medicines c on a.medicineId = c.medicineID
									left join medicinedrugs d on c.medicineid = d.medicineid 
									left join drugs e on d.drugid = e.drugid
									left join drugclasses f on e.drugclassid = f.drugclassid 
                group by a.clientid, da.date
		) s2 on s.clientid = s2.clientid and s.date = s2.date	
        left join bi_patient_reversal_state_gmi_x_date s3 on s.clientid = s3.clientid and s.date = s3.date 
		
		
		left join (	
					 select a.clientid, date(b.bloodworktime) as bloodworkdate, b.ldlCholesterol, b.triglycerides
                     from 
						(select clientid, max(bloodworktime) as latest_test from clinictestresults group by clientid ) a inner join clinictestresults b 
                        on a.clientid = b.clientid and a.latest_test = b.bloodworktime
					 where (b.ldlCholesterol > 100 or b.triglycerides > 150)
				   ) s5 on s.clientid = s5.clientid 
		left join (	
					 select a.clientid, date(b.bloodworktime) as bloodworkdate, b.ldlCholesterol, b.triglycerides
                     from 
						(select clientid, min(bloodworktime) as first_test from clinictestresults group by clientid ) a inner join clinictestresults b 
                        on a.clientid = b.clientid and a.first_test = b.bloodworktime
					 where (b.ldlCholesterol > 100 or b.triglycerides > 150)
				   ) s6 on s.clientid = s6.clientid 
		 left join dim_client dc on s.clientid = dc.clientid and dc.is_row_current = 'y'
 ) s   
 ) s 
 group by clientid 
 ;
 
create index idx_tmp_patient_health_condition on tmp_patient_health_condition(clientid asc);
 
update dim_client a inner join tmp_patient_health_condition b on a.clientid = b.clientid 
set a.is_HTN_patient = b.is_HTN_patient,
	a.is_cholesterol_patient = b.is_cholesterol_patient,
	a.is_heartdisease_patient = b.is_heartdisease_patient
where a.is_row_current= 'y'
;

drop temporary table if exists tmp_patient_health_condition
;



drop temporary table if exists tmp_start_medicine_NONDIABETIC;

/* 20221124 - Aravindan - Commenting out this portion because this old med DB match is no longer working as the medicines are not maintained here old way
select clientid, eventdate, 
count(distinct case when subcategory = 'HYPERTENSION' then drugname else null end) as HT_med_drugs,
count(distinct case when subcategory = 'CHOLESTEROL' then drugname else null end) as CHOL_med_drugs,
count(distinct case when subcategory = 'HEARTDISEASE' then drugname else null end) as HD_med_drugs,
group_concat(distinct case when subcategory = 'HYPERTENSION' then drugname else null end) as HT_drugs,
group_concat(distinct case when subcategory = 'CHOLESTEROL' then drugname else null end) as CHOL_drugs,
group_concat(distinct case when subcategory = 'HEARTDISEASE' then drugname else null end) as HD_drugs
from 
(
	select a.clientid, date(itz(eventtime)) as eventdate, category, subcategory, c.medicineName, e.drugname
	from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
	inner join medicines c on a.medicationId = c.medicineID
	left join medicinedrugs d on c.medicineid = d.medicineid 
	left join drugs e on d.drugid = e.drugid
	left join drugclasses f on e.drugclassid = f.drugclassid 
	where hasMedicine = 1 
	and f.subcategory in ('HYPERTENSION','CHOLESTEROL','HEARTDISEASE')
) s 
group by clientid, eventdate
;
*/

-- New logic to use the medical condition to identify start meds - HTN, Cholesterol, Heart Disease
create temporary table tmp_start_medicine_NONDIABETIC as
select clientid, eventdate, 
group_concat(distinct case when medicalCondition = 'Hypertension (HTN)' then medicinename else null end) as HT_drugs,
group_concat(distinct case when medicalCondition = 'Dyslipidemia' then medicinename else null end) as CHOL_drugs,
group_concat(distinct case when medicalCondition = 'Coronary Artery Disease (CAD)' then medicinename else null end) as HD_drugs
from 
(
	select a.clientid, date(itz(eventtime)) as eventdate, c.medicinename, a.medicalCondition
	from clinicreportmedications a inner join v_client_tmp b on a.clientid = b.clientid 
									inner join medicines c on a.medicationId = c.medicineID
									inner join (select clientid, min(id) as firstID from clinictestresults where deleted = false group by clientid) crt on a.clinictestresultID = crt.firstID
										
	where hasMedicine = 1 
	and draft = false
) s 
group by clientid, eventdate
;

create index idx_tmp_start_medicine_NONDIABETIC on tmp_start_medicine_NONDIABETIC (clientid asc);

update dim_client a inner join tmp_start_medicine_NONDIABETIC b on a.clientid = b.clientid 
set a.start_HTN_medicine_drugs = b.HT_drugs, 
	a.start_CHOL_medicine_drugs = b.CHOL_drugs,
    a.start_HeartDisease_medicine_drugs = b.HD_drugs
where a.is_row_current = 'y'
; 

drop temporary table if exists tmp_start_medicine_NONDIABETIC;



drop temporary table if exists tmp_current_medicine_NONDIABETIC;


/* 20221124 - Aravindan - Commenting out this portion because this old med DB match is no longer working as the medicines are not maintained here old way
select clientid, current_status, HT_total_med_drugs, CHOL_total_med_drugs, HD_total_med_drugs, HT_drugs,CHOL_drugs,HD_drugs
from 
(
select s.clientid, c.status as current_status, startdate, enddate, c1.start_date as active_start_date, c1.end_date as active_end_date, 
count(distinct case when subcategory = 'HYPERTENSION' then drugname else null end) as HT_total_med_drugs,
count(distinct case when subcategory = 'CHOLESTEROL' then drugname else null end) as CHOL_total_med_drugs,
count(distinct case when subcategory = 'HEARTDISEASE' then drugname else null end) as HD_total_med_drugs,
group_concat(distinct case when subcategory = 'HYPERTENSION' then drugname else null end) as HT_drugs,
group_concat(distinct case when subcategory = 'CHOLESTEROL' then drugname else null end) as CHOL_drugs,
group_concat(distinct case when subcategory = 'HEARTDISEASE' then drugname else null end) as HD_drugs
from 
(
    select distinct clientid, subcategory, drugname, min(startdate) as startdate, max(enddate) as enddate 
    from 
    (
    select distinct P.clientId, pm.medicineid, subcategory, c.medicineName, e.drugname,
	(date(itz(pm.startLocaldate))) as startdate, (date(itz(pm.endLocaldate))) as enddate 
	from prescriptions P join prescriptionmedicines PM on P.prescriptionid = PM.prescriptionid
						left join medicines c on pm.medicineid = c.medicineid
						left join medicinedrugs d on pm.medicineid = d.medicineid 
						left join drugs e on d.drugid = e.drugid
						left join drugclasses f on e.drugclassid = f.drugclassid 
						inner join v_client_tmp ac on p.clientid = ac.clientid
	where f.subcategory in ('HYPERTENSION','CHOLESTEROL','HEARTDISEASE')
    and (pm.startLocaldate is not null and pm.endLocalDate is not null)
    ) s 
	group by clientid, subcategory, drugname
) s left join v_client_tmp c on s.clientid = c.clientid 
	left join (select clientid, max(status_Start_Date) as start_date, max(status_end_date) as end_date from bi_patient_status where status = 'active' group by clientid) c1 on s.clientid = c1.clientid
group by clientid, startdate, enddate
) s
where (case when current_status = 'active' then date_sub(date(itz(now())), interval 1 day) else active_end_date end) between startdate and enddate
;
*/

-- New logic to use the medical condition to identify start meds - HTN, Cholesterol, Heart Disease

create temporary table tmp_current_medicine_NONDIABETIC as 
select clientid, current_status, HT_drugs,CHOL_drugs,HD_drugs
from 
(
select s.clientid, c.status as current_status, startdate, enddate, c1.start_date as active_start_date, c1.end_date as active_end_date, 
group_concat(distinct case when medicalCondition = 'Hypertension (HTN)' then medicineName else null end) as HT_drugs,
group_concat(distinct case when medicalCondition = 'Dyslipidemia' then medicineName else null end) as CHOL_drugs,
group_concat(distinct case when medicalCondition = 'Coronary Artery Disease (CAD)' then medicineName else null end) as HD_drugs
from 
(
    select distinct clientid, medicineID, medicineName, medicalCondition, min(startdate) as startdate, max(enddate) as enddate 
    from 
    (
    select distinct P.clientId, pm.medicineid,  c.medicineName,c.medicalCondition,
	(date(itz(pm.startLocaldate))) as startdate, (date(itz(pm.endLocaldate))) as enddate 
	from prescriptions P join prescriptionmedicines PM on P.prescriptionid = PM.prescriptionid
							left join medicines c on pm.medicineid = c.medicineid
							inner join v_client_tmp ac on p.clientid = ac.clientid
	where c.medicalCondition in ('Hypertension (HTN)','Dyslipidemia','Coronary Artery Disease (CAD)')
    and (pm.startLocaldate is not null and pm.endLocalDate is not null)
    ) s 
	group by clientid, medicineID, medicineName, medicalCondition
) s inner join v_client_tmp c on s.clientid = c.clientid 
	left join (select clientid, max(status_Start_Date) as start_date, max(status_end_date) as end_date from bi_patient_status where status = 'active' group by clientid) c1 on s.clientid = c1.clientid
group by clientid, startdate, enddate
) s
where (case when current_status = 'active' then date_sub(date(itz(now())), interval 1 day) else active_end_date end) between startdate and enddate
;

create index idx_tmp_current_medicine_NONDIABETIC on tmp_current_medicine_NONDIABETIC (clientid asc);

update dim_client a inner join tmp_current_medicine_NONDIABETIC b on a.clientid = b.clientid 
set a.current_HTN_medicine_drugs = b.HT_drugs, 
	a.current_CHOL_medicine_drugs = b.CHOL_drugs,
    a.current_HeartDisease_medicine_drugs = b.HD_drugs
where a.is_row_current = 'y'
; 

drop temporary table if exists tmp_current_medicine_NONDIABETIC;




	drop temporary table if exists tmp_type1Like_maintenance; 

	create temporary table tmp_type1Like_maintenance as 
	select clientid, status, is_type1Like, type1Like_category, cPeptide, homa2b, start_insulin_units, durationyears_diabetes, cgm_5d, ketone_5d
	from 
	(
		select a.clientid, a.status,f.cPeptide, f.homa2b, c.start_insulin_units, c.durationyears_diabetes, b1.cgm_5d, b1.ketone_5d, 
		if ((f.cPeptide <= 0.5 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0) or (f.cPeptide < 1.1 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0 and c.durationYears_diabetes >= 25) or (b1.cgm_5d > 175 and b1.ketone_5d > 0.4), 'Yes','No') as is_Type1Like,
        case when (f.cPeptide <= 0.5 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0) then 'C1'
			 when (f.cPeptide < 1.1 and ifnull(f.homa2b,0) <= 51 and c.start_insulin_units > 0 and c.durationYears_diabetes >= 25) then 'C2'
             when (b1.cgm_5d > 175 and b1.ketone_5d > 0.4) then 'C3' else null end as type1Like_category
		from 																
		v_client_tmp a left join ( 
									select s.clientid, s.td8, s1.cgm_5d, s1.ketone_5d
									from 
									(
										select clientid, max(date) as td8
										from bi_patient_reversal_state_gmi_x_date 
										where treatmentdays = 9
										group by clientid 
									) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.td8 = s1.date
								) b1 on a.clientid = b1.clientid	
						   left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'													
						   left join (																
										select distinct a.clientid, LabA1c, BMI, cPeptide, albumin, ast, alt, homa2B, creatinine, gfr 						
										from 								
										(								
											select s.clientid, s.durationYears_diabetes, cast(avg(veinhba1c) as decimal(10,2)) as LabA1c, avg(glomerularFiltrationRate) as gfr, avg(albumin) as albumin, 							
											avg(ast) as ast, avg(alt) as alt, avg(Creatinine) as creatinine, 							
											avg(GADA) as GADA, avg(cPeptide) as cPeptide, avg(homa2B) as homa2B,							
											avg(Weight)/(avg(c.Height)*avg(c.Height)) as BMI							
											from dim_client s inner join clinictestresults c on s.clientid = c.clientid and s.firstBloodWorkDateITZ = itz(c.bloodworktime)							
											where s.is_row_current = 'y'
											group by clientid							
										) a 
									) f on a.clientid = f.clientid 		
	) s 
	;

	create index idx_tmp_type1Like_maintenance on tmp_type1Like_maintenance (clientid asc, is_type1Like, type1Like_category)
	;

	update dim_client a inner join tmp_type1Like_maintenance b on a.clientid = b.clientid 
	set a.is_type1like = b.is_type1like,
		a.type1Like_category = b.type1Like_category
	where is_row_current = 'y'
	;

	drop temporary table if exists tmp_type1Like_maintenance
	;
	
    commit
    ;
    
	drop temporary table if exists tmp_members_more_than_200_cgm_7days; 
    
    create temporary table tmp_members_more_than_200_cgm_7days as 
	select a.clientid, a.total_cgm_days, b.is_type1like, b.type1Like_category
    from 
    (
		select clientid, total_treat_days, total_cgm_days from 
		(
			select a.clientid, count(date) as total_treat_days, sum(case when cgm_5d > 200 then 1 else null end) as total_cgm_days
			from
			(select clientid from v_client_tmp where date(enrollmentDateITZ) >= '2021-08-01'  and status='active') a -- hardcoded aug 1st here because this new condition needs to be applied only for members enrolled after this date. 
			join bi_patient_reversal_State_gmi_x_Date b on a.clientid=b.clientid and b.treatmentdays between 7 and 13
            group by a.clientid
		) a 
		where total_treat_days >= 7
    ) a left join dim_client b on a.clientid = b.clientid and b.is_row_current = 'y'
	where b.type1Like_category in ('C1', 'C2') and a.total_cgm_days is not null-- only the blood work related conditions need to be validated against this consecutive 5dg condition. Because such members will be there in the program until we double confirm the type1like using the CGM 5d condition as well and then only they will be decided for dischage. 
	;

	create index idx_tmp_members_more_than_200_cgm_7days on tmp_members_more_than_200_cgm_7days(clientid);
      
 	update dim_client a inner join tmp_members_more_than_200_cgm_7days b on a.clientid = b.clientid 
	set a.is_type1Like = (case when b.total_cgm_days = 7 then 'Yes' else 'No' end), 
		a.type1Like_category = (case when b.total_cgm_days = 7 then 'C4' else null end), -- member type1like because between treatmentdays 6 and 12, all days are cgm_5d > 200
		a.type1Like_changeNotes = (case when b.total_cgm_days < 7 then 'Member not meeting CGM 5d condition' else null end)
	where is_row_current = 'y'
	;

	drop temporary table tmp_members_more_than_200_cgm_7days
    ;    
    
	drop temporary table if exists tmp_patient_exclusion; 

    create temporary table tmp_patient_exclusion as 
    select clientid, is_exclusion, exclusion_cohort from v_bi_patient_exclusion_detail_all_patients 
    ;
    
    create index idx_tmp_patient_exclusion on tmp_patient_exclusion(clientid asc, is_exclusion, exclusion_cohort); 
    
    update dim_client a left join tmp_patient_exclusion b on a.clientid = b.clientid
    set a.is_medical_exclusion = b.is_exclusion,
		a.medical_exclusion_cohort = b.exclusion_cohort
	where a.is_row_current = 'y'
    ; 
    
	drop temporary table if exists tmp_patient_exclusion; 


-- Maintain first cgm_5d 
	drop temporary table if exists tmp_first_available_cgm_5d;

	create temporary table tmp_first_available_cgm_5d as 
	select a.clientid,b.date as first_available_cgm_5d_date,b.cgm_5d as first_available_cgm_5d
			from 
			(
			   		select clientid, min(case when cgm_5d is not null then date else null end) as first_available_cgm_5d_date
					from bi_patient_reversal_state_gmi_x_date 
					group by clientid 
			) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid=b.clientid and a.first_available_cgm_5d_date=b.date
   ;         
	
  	create index idx_tmp_first_available_cgm_5d on tmp_first_available_cgm_5d (clientid asc, first_available_cgm_5d_date asc, first_available_cgm_5d);

    update dim_client a inner join tmp_first_available_cgm_5d b on a.clientid=b.clientid
    set
         a.first_available_cgm_5d_date=b.first_available_cgm_5d_date,
         a.first_available_cgm_5d=b.first_available_cgm_5d
    where is_Row_current = 'y'
    ;

    drop temporary table if exists tmp_first_available_cgm_5d;
   
-- Maintain first cgm_1d
	drop temporary table if exists tmp_first_available_cgm_1d;

	create temporary table tmp_first_available_cgm_1d as 
	select a.clientid,b.date as first_available_cgm_1d_date,b.cgm_1d as first_available_cgm_1d
			from 
			(
			   select clientid, min(case when cgm_1d is not null then date else null end) as first_available_cgm_1d_date
				from bi_patient_reversal_state_gmi_x_date 
				group by clientid 
			) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid=b.clientid and a.first_available_cgm_1d_date=b.date
   ;         
	
  	create index idx_tmp_first_available_cgm_1d on tmp_first_available_cgm_1d (clientid asc, first_available_cgm_1d_date asc, first_available_cgm_1d);
    

    
    update dim_client a inner join tmp_first_available_cgm_1d b on a.clientid=b.clientid 
       set
         a.first_available_cgm_1d_date=b.first_available_cgm_1d_date,
         a.first_available_cgm_1d=b.first_available_cgm_1d
    where a.is_row_current='Y'
     ;
    
    drop temporary table if exists tmp_first_available_cgm_1d;

			insert into bi_patient_initial_setup_x_dates (clientid)
			select distinct clientid
			from dim_client a
			where status in ('Active','Pending_active','Inactive','Discharged')
			and is_row_current = 'y'
			and not exists (select 1 from bi_patient_initial_setup_x_dates b where a.clientid = b.clientid)
			; 

			
			update bi_patient_initial_setup_x_dates a inner join 
			(select clientid, min(itz(bloodworktime)) as firstBloodWorkTime from clinictestresults where bloodworktime is not null group by clientid) b on a.clientid = b.clientid
			set a.firstBloodWorkDateITZ = firstBloodWorkTime
			;


			
			update bi_patient_initial_setup_x_dates a inner join 
			(
						select a.clientid, b.labreportId, 
						itz(c.dateadded) as first_Investigation_upload_date, 
						First_Vital_Updated_Date, 
						cast(d.weight/(d.height*d.height) as decimal(10,2)) as BMI 
						from dim_client a left join clinictestresults b on a.clientid = b.clientid and a.firstBloodWorkDateITZ = itz(b.bloodworktime)
										  left join labreports c on b.labreportId = c.labreportId
										  left join (
														SELECT CLIENTID, if(height > 100, height/100, height) as height, WEIGHT, MIN(ITZ(DATEMODIFIED))  AS First_Vital_Updated_Date
														FROM clinictestresults_aud B
														WHERE B.HEIGHT IS NOT NULL AND B.WEIGHT IS NOT NULL
														GROUP BY CLIENTID
													) d on a.clientid = d.clientid
						where a.is_row_current = 'y'
			) lab on a.clientid = lab.clientid 
			set a.First_Investigation_upload_date = lab.first_Investigation_upload_date,
				a.First_Vital_Updated_Date = lab.First_Vital_Updated_Date, 
				a.First_BMI = lab.BMI
			;

-- Medical Record upload date (start, latest)
			update bi_patient_initial_setup_x_dates a inner join 
			(
					  select m.clientid, min(itz(m.dateAdded)) as first_medicalrecord_upload_date, max(itz(m.dateAdded)) as latest_medicalrecord_upload_date from medicalrecords m
					  where m.category='BLOOD_REPORT' and m.uploadedByRole='INTERNAL_OPERATIONS'
					  group by m.clientid
			 ) b on a.clientid = b.clientid
			 set a.first_medicalrecord_upload_date = b.first_medicalrecord_upload_date,
			 	 a.latest_medicalrecord_upload_date = b.latest_medicalrecord_upload_date
			 ; 
			
			update bi_patient_initial_setup_x_dates a inner join 
			(
						select a.clientid, a.First_Medicine_Published_Date, b.actualmedication as First_Medicine_Published
						from
						(
						Select clientid, min(itz(dateadded)) as First_Medicine_Published_Date from medicationhistories group by clientid
						) a inner join medicationhistories b on a.clientid = b.clientid and a.First_Medicine_Published_Date = itz(dateadded)
			) f on f.clientid = a.clientid
			set a.First_Medicine_Published_Date = f.First_Medicine_Published_Date,
				a.First_Medicine_Published = f.First_Medicine_Published
			;

			
            update bi_patient_initial_setup_x_dates a inner join 
			(
					select a.clientid, a.latest_medicine_Published_Date, ifnull(b.actualmedication,'') as latest_Medicine_Published
					from
					(
						Select clientid, max(itz(dateadded)) as latest_medicine_Published_Date from medicationhistories where type = 'DIABETES' group by clientid
					) a inner join medicationhistories b on a.clientid = b.clientid and a.latest_medicine_Published_Date = itz(dateadded) and type = 'DIABETES'
 			) f on f.clientid = a.clientid
			set a.latest_medicine_published_date = f.latest_medicine_Published_Date,
				a.latest_medicine_published = f.latest_Medicine_Published
			;           
            
			
			update bi_patient_initial_setup_x_dates a inner join 
			(
							select a.clientid, min(itz(a.dateadded)) as First_Fitbit_Sync_Date 
							from sensors a 
							where a.sensorModel in ('FITBIT')
							group by a.clientid
			) f on f.clientid = a.clientid
			set a.First_Fitbit_Sync_Date = f.First_Fitbit_Sync_Date
			;

			update bi_patient_initial_setup_x_dates a inner join 
			(
							select a.clientid, min(itz(a.dateadded)) as First_Garmin_Sync_Date 
							from sensors a 
							where a.sensorModel in ('Garmin')
							and syncEnabled=1
							group by a.clientid
			) f on f.clientid = a.clientid
			set a.First_Garmin_Sync_Date = f.First_Garmin_Sync_Date
			;
		
			update bi_patient_initial_setup_x_dates a inner join 
			(
						 select a.clientid, min(itz(a.dateAdded)) as First_Mobile_Setup_Date
						 from userdeviceinfos a 
						 group by a.clientid
			) f on f.clientid = a.clientid
			set a.First_Mobile_Setup_Date = f.First_Mobile_Setup_Date
			;

			

			drop temporary table if exists tmp_schedule_status; 
			
			create temporary table tmp_schedule_status
			(
			clientid int not null,
			HomeVisit_scheduled_Date datetime null,
			homeVisit_status varchar(50) null,
			BloodWork_Scheduled_Date datetime null,
			bloodwork_status varchar(50) null,
			Bloodwork_Schedule_created_Date datetime null,
			SA_Scheduled_Date datetime null,
			SA_status varchar(50) null,
			DoctorAppointment_Scheduled_Date datetime null,
			doctorAppointment_status varchar(50) null,
			DoctorAppointment_Schedule_created_Date datetime null
			);
			

			insert into tmp_schedule_status
			select a.clientid, 
			max(case when a.type = 'HOME_VISIT' and date(itz(a.eventTime)) <= ifnull(day5_from_active_status_start, date(itz(a.eventTime))) then itz(a.eventTime) else null end) as HomeVisit_scheduled_Date, 
			max(case when a.type = 'HOME_VISIT' and date(itz(a.eventTime)) <= ifnull(day5_from_active_status_start, date(itz(a.eventTime))) then a.status else null end)  as homeVisit_status, 
			max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'ONBOARDING' then itz(a.eventTime) else null end) as BloodWork_Scheduled_Date, 
			max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'ONBOARDING' then a.status else null end)  as bloodwork_status, 
			max(case when a.type = 'BLOOD_WORK' and c.bloodWorkDay = 'ONBOARDING' then itz(a.dateadded) else null end) as Bloodwork_Schedule_created_Date, 
			max(case when a.type = 'SENSOR_ACTIVATION' then itz(a.eventTime) else null end) as SA_Scheduled_Date, 
			max(case when a.type = 'SENSOR_ACTIVATION' then a.status else null end) as SA_status, 
			max(case when a.type = 'DOCTOR_APPOINTMENT' and d.Quarter = '0' and d.Day = '0' and meetingType = 'ONBOARDING_CONSULTATATION' then itz(a.eventTime) else null end) as DoctorAppointment_Scheduled_Date, 
			max(case when a.type = 'DOCTOR_APPOINTMENT' and d.Quarter = '0' and d.Day = '0' and meetingType = 'ONBOARDING_CONSULTATATION' then a.status else null end) as doctorAppointment_status,
			max(case when a.type = 'DOCTOR_APPOINTMENT' and d.Quarter = '0' and d.Day = '0' and meetingType = 'ONBOARDING_CONSULTATATION' then itz(a.dateadded) else null end) as DoctorAppointment_Schedule_created_Date
						
											   
			from clientappointments a left join homevisits b on a.clientid = b.clientid and a.clientAppointmentId = b.clientAppointmentId
									  left join bloodworkschedules c on a.clientid = c.clientid and a.clientAppointmentId = c.clientAppointmentId
									  left join doctorappointments d on a.clientid = d.clientid and a.clientAppointmentId = d.clientAppointmentId
                                      left join (
														select clientid, date_add(max(status_start_date), interval 5 day) as day5_from_active_status_start, max(status_end_date) as status_end_date 
														from bi_patient_Status 
														where status = 'active'
														group by clientid
                                                 ) e on a.clientid = e.clientid
			group by a.clientid 
			;

			create index idx_tmp_schedule_status on tmp_schedule_status(clientid asc);
		
			update bi_patient_initial_setup_x_dates a inner join tmp_schedule_status f on a.clientid = f.clientid 
			set a.HomeVisit_scheduled_Date = f.HomeVisit_scheduled_Date,
				a.HomeVisit_scheduled_Status = f.homeVisit_status,
				a.BloodWork_Scheduled_Date = f.BloodWork_Scheduled_Date,
				a.BloodWork_Scheduled_Status = f.bloodwork_status,
				a.Bloodwork_Schedule_created_Date = f.Bloodwork_Schedule_created_Date,
                a.sensorActivation_schedule_date = f.SA_Scheduled_Date,
                a.sensorActivation_schedule_status = f.SA_status,
				a.DoctorAppointment_Scheduled_Date = f.DoctorAppointment_Scheduled_Date,
				a.DoctorAppointment_status = f.doctorAppointment_status,
				a.DoctorAppointment_Schedule_created_Date = f.DoctorAppointment_Schedule_created_Date
			;

			drop temporary table if exists tmp_schedule_status; 

		
-- maintain Twin doctor D0 appointment status and time. 
		
			update bi_patient_initial_setup_x_dates a inner join
			(		
						select s.clientid, itz(c.eventTime) as firstTwinsDoctorAppntTime, c.status  as firstTwinsDoctorAppntStatus
						from 
						(
							select a.clientid, min(a.clientAppointmentId) as firstTwinDoctorAppntID
							from progressreviews a inner join clientappointments c2 on a.clientid = c2.clientid and a.clientappointmentID = c2.clientAppointmentId 
							where a.quarter = '0' and a.day = '0' 
							and c2.status not in ('CANCELLED')
							group by a.clientid 
						) s inner join clientappointments c on s.clientid = c.clientid and s.firstTwinDoctorAppntID = c.clientAppointmentId
			) b on a.clientid = b.clientid 
			set a.twinDoctorD0AppointmentTime = b.firstTwinsDoctorAppntTime,
				a.twinDoctorD0Appointmentstatus = b.firstTwinsDoctorAppntStatus
			;
		
		
			drop temporary table if exists tmp_opsToDo_status; 
			
			create temporary table tmp_opsToDo_status
			(
				clientid int not null,
				vitals_conditions_status varchar(50) null,
				medicine_allergies_status varchar(50) null,
				Nutrition_QA_status varchar(50) null,
				Habit_QA_status varchar(50) null,
				DoctorConsultation_status varchar(50) null,
				Medicine_publish_status varchar(50) null,
				Language_status varchar(50) null,
				phase_one_report_upload_status varchar(50) null
			);
			
			
			insert into tmp_opsToDo_status
			select clientid, 
			max(case when otd.todotype = 'VITALS_CONDITIONS' then otd.status else null end) as vitals_conditions_status, 
			max(case when otd.todotype = 'MEDICINES_ALLERGIES' then otd.status else null end) as medicine_allergies_status, 
			max(case when otd.todotype = 'NUTRITION_QUESTIONNAIRE' then otd.status else null end) as Nutrition_QA_status, 
			max(case when otd.todotype = 'HABIT_QUESTIONNAIRE' then otd.status else null end) as Habit_QA_status, 
			max(case when otd.todotype = 'DOCTOR_CONSULTATION' then otd.status else null end) as DoctorConsultation_status, 
			max(case when otd.todotype = 'MEDICINES' then otd.status else null end) as Medicine_publish_status, 
			max(case when otd.todotype = 'LANGUAGES' then otd.status else null end) as Language_status,
			max(case when otd.todotype = 'PHASE_ONE_REPORT_UPLOAD' then otd.status else null end) as phase_one_report_upload_status
			from OpsTodoItems otd
			group by clientid 
			;
			
			create index idx_tmp_opsToDo_status on tmp_opsToDo_status(clientid asc);
			
							
			update bi_patient_initial_setup_x_dates a inner join tmp_opsToDo_status f on a.clientid = f.clientid
			set a.vitals_conditions_status = f.vitals_conditions_status,
				a.medicine_allergies_status = f.medicine_allergies_status,
				a.nutrition_QA_status = f.Nutrition_QA_status,
				a.habit_QA_status = f.Habit_QA_status,
				a.doctorConsultation_status = f.DoctorConsultation_status,
				a.medicine_publish_status = f.Medicine_publish_status,
				a.language_status = f.Language_status,
				a.phase_one_report_upload_status = f.phase_one_report_upload_status
			;
			
			drop temporary table if exists tmp_opsToDo_status; 
			

			
			drop temporary table if exists tmp_opsToDo_dates; 
			
			create temporary table tmp_opsToDo_dates
			(
			clientid int not null,
			vitals_conditions_finished_date datetime null,
			medicine_allergies_finished_date datetime null,
			nutrition_QA_finished_date datetime null,
			habit_QA_finished_date datetime null,
			doctorConsultation_finished_date datetime null,
			medicine_publish_finished_date datetime null,
			language_finished_date datetime null,
			phase_one_report_upload_finished_date datetime null
			);
			
			
			insert into tmp_opsToDo_dates
			select clientid, 
			max(case when otd.todotype = 'VITALS_CONDITIONS' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as vitals_conditions_finished_date, 
			max(case when otd.todotype = 'MEDICINES_ALLERGIES' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as medicine_allergies_finished_date, 
			max(case when otd.todotype = 'NUTRITION_QUESTIONNAIRE' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as nutrition_QA_finished_date, 
			max(case when otd.todotype = 'HABIT_QUESTIONNAIRE' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as habit_QA_finished_date, 
			max(case when otd.todotype = 'DOCTOR_CONSULTATION' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as doctorConsultation_finished_date, 
			max(case when otd.todotype = 'MEDICINES' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as medicine_publish_finished_date, 
			max(case when otd.todotype = 'LANGUAGES' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as language_finished_date,
			max(case when otd.todotype = 'PHASE_ONE_REPORT_UPLOAD' and otd.status = 'FINISHED' then itz(timeStamp) else null end) as phase_one_report_upload_finished_date
			from OpsTodoItems otd inner join opstodotimelines otdt on otd.opsTodoItemId = otdt.opsTodoItemId
			group by clientid 
			;
			
			create index idx_tmp_opsToDo_dates on tmp_opsToDo_dates(clientid asc);
			
							
			update bi_patient_initial_setup_x_dates a inner join tmp_opsToDo_dates f on (a.clientid = f.clientid)
			set a.vitals_conditions_finished_date = f.vitals_conditions_finished_date,
				a.medicine_allergies_finished_date = f.medicine_allergies_finished_date,
				a.nutrition_QA_finished_date = f.nutrition_QA_finished_date,
				a.habit_QA_finished_date = f.habit_QA_finished_date,
				a.doctorConsultation_finished_date = f.doctorConsultation_finished_date,
				a.medicine_publish_finished_date = f.medicine_publish_finished_date,
				a.language_finished_date = f.language_finished_date,
				a.phase_one_report_upload_finished_date = f.phase_one_report_upload_finished_date
			;
						
					
			drop temporary table if exists tmp_opsToDo_dates; 

			
			update bi_patient_initial_setup_x_dates a inner join 
			(
				select s1.clientid, min(itz(s1.datemodified)) as first_all_5_filled_date
				from 
                clinictestresults_aud s1 
				where vitalsCaptured = 1 and conditionsCaptured = 1 and additionalInformationCaptured = 1 and medicinesAllergiesCaptured = 1 and currentSymptomsCaptured = 1
				group by s1.clientid 
			) b on a.clientid = b.clientid
			set a.vitals_conds_allergies_meds_addInfor_captured = b.first_all_5_filled_date
			;
            
            
			update bi_patient_initial_setup_x_dates a inner join
			(
					select clientid, min(itz(eventtime)) AS BCM_DATE
					from  weights
					where source in ('direct sync')
					group by clientid
			) w on w.clientid = a.clientid
			set a.first_BCM_sync_date = w.BCM_date
			;
            
			
			update bi_patient_initial_setup_x_dates a inner join
			(
						select clientid, min(itz(eventtime)) as bp_sync_date
						from bloodpressurevalues
						group by clientid
			) bp on bp.clientid = a.clientid
			set a.first_BP_sync_date = bp.bp_sync_date
			;
            
			
			update bi_patient_initial_setup_x_dates a inner join
			(
						select clientid, min(itz(eventtime)) as BHB_sync_date
						from  glucoseketones
						group by clientid
			) bhb on bhb.clientid = a.clientid
			set a.first_BHB_sync_date = bhb.BHB_sync_date
			;
            

		
		set time_zone = 'Asia/Calcutta'; 

		drop temporary table if exists tmp_patient_suspended_status;

		create temporary table tmp_patient_suspended_status as 
		select clientid, date(changeDay) as dateChanged, changeDay as dateModified, s2_suspended as newStatusFlag
		from 
		(
				select s1.clientid, s1.suspended as s1_suspended, s1.datemodified as s1_datemodified, s2.suspended as s2_suspended, s2.datemodified as s2_datemodified,
				case when s1.suspended is null and s2.suspended = 1 then s2.dateModified
					 when s1.suspended <> s2.suspended then s2.datemodified
					 else null end as changeDay
				 from  
				 (   
						select @row_num := case when @id = id then @row_num + 1 else 1 end as rownumber,  
						@id := id as clientid, datemodified, suspended
						from   
						(   
								select distinct s.id, s1.datemodified, s1.suspended
								from 
								(
									select id, date(datemodified) as date, max(datemodified) as lastUpdate from clientauxfields_aud 
									group by id, date(datemodified)
								) s inner join clientauxfields_aud s1 on s.id = s1.id and s.lastUpdate = s1.datemodified
						) s1   cross join (select @row_num := 0, @id := 0 ) s2  
						order by id, datemodified
				 ) s1 
				left join  
				 (   
						select @s_num := case when @id = id then @s_num + 1 else 1 end as rownumber,  
						@id := id as clientid, datemodified, suspended
						from   
						(   
								select distinct s.id, s1.datemodified, s1.suspended
								from 
								(
									select id, date(datemodified) as date, max(datemodified) as lastUpdate from clientauxfields_aud 
									group by id, date(datemodified)
								) s inner join clientauxfields_aud s1 on s.id = s1.id and s.lastUpdate = s1.datemodified
						) s1   cross join (select @s_num := 0, @id := 0 ) s2  
						order by id, datemodified
				 ) s2  on s1.clientid = s2.clientid and s2.rownumber = s1.rownumber + 1 
		 ) s 
		 where changeDay is not null
		 ;

		 
		create index idx_tmp_patient_suspended_status on tmp_patient_suspended_status (clientid asc, dateChanged asc, dateModified); 

		drop temporary table if exists tmp_patient_suspended_status_1; 

		create temporary table tmp_patient_suspended_status_1 as select clientid, dateChanged, dateModified, newStatusFlag from tmp_patient_suspended_status; 

		create index idx_tmp_patient_suspended_status1 on tmp_patient_suspended_status_1 (clientid asc, dateChanged asc, dateModified); 

		drop temporary table if exists tmp_bi_patient_suspension_tracker;

		create temporary table tmp_bi_patient_suspension_tracker as 
		select s1.clientid, s1.dateChanged as startDate, s1.dateModified, s1.newStatusFlag, ifnull(date_sub(s2.dateChanged, interval 1 day), date(now())) as endDate
		from 
		(
						select @rnum := case when @id = clientid then @rnum + 1 else 1 end as rownum,  
						@id := clientid as clientid, dateChanged, dateModified, newStatusFlag
						from   
						(   
								select clientid, dateChanged, dateModified, newStatusFlag from tmp_patient_suspended_status 
						) s1   cross join (select @snum := 0, @id := 0 ) s2  
						order by clientid, dateChanged
		) s1 left join 
		(
						select @snum := case when @id = clientid then @snum + 1 else 1 end as rownum,  
						@id := clientid as clientid, dateChanged, dateModified, newStatusFlag
						from   
						(   
								select clientid, dateChanged, dateModified, newStatusFlag from tmp_patient_suspended_status_1
						) s1   cross join (select @snum := 0, @id := 0 ) s2  
						order by clientid, dateChanged
		) s2 on s1.clientid = s2.clientid and s2.rownum = s1.rownum + 1 
		;

		create index idx_tmp_bi_patient_suspension_tracker on tmp_bi_patient_suspension_tracker(clientid asc, startDate asc, newStatusFlag); 

		insert into bi_patient_suspension_tracker (clientid, dateModified, status, startDate, endDate) 
		select clientid, dateModified, newStatusFlag, startDate, endDate from tmp_bi_patient_suspension_tracker  a 
		where not exists 
		(select 1 from bi_patient_suspension_tracker b where a.clientid = b.clientid and a.startDate = b.startDate)
		;

		update bi_patient_suspension_tracker a inner join tmp_bi_patient_suspension_tracker b on (a.clientid = b.clientid and a.startDate = b.startDate)
		set a.status = newStatusFlag,	
			a.dateModified = b.dateModified,
			a.endDate = b.endDate
		; 

		drop temporary table if exists tmp_patient_suspended_status;
		drop temporary table if exists tmp_patient_suspended_status_1;
		drop temporary table if exists tmp_bi_patient_suspension_tracker;

		set time_zone = 'UTC'; 



update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_dim_client'
;
END