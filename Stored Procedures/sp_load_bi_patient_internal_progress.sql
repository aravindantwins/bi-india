DELIMITER $$
CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `sp_load_bi_patient_internal_progress`(  )
BEGIN




update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_patient_internal_progress'
;


drop temporary table if exists tmp_patient_internal_progress; 

create temporary table tmp_patient_internal_progress
as 
select
c.clientid, c.doctorNameShort as doctor,  c.coachNameShort as coach,    
c.treatmentDays, p.cohortGroup, ifnull(c.d10_success, '') as d10_success, ifnull(c.d35_success, '') as d35_success, t.cPeptide as D0_cPeptide, n.NPS, n.Nut_Sat
, h.nut_happy as NUT_HAP, h.food_rating, h.total_disenrollment_incidents, h.total_nutrition_incidents, h.interview_score
,m.event_date, m.measure_group,  m.measure_name, m.measure as measure
 from dim_client c
 join v_client_tmp p
 on c.clientId = p.clientId
 left join
 (
	select distinct clientId, itz(bloodworktime) as eventDate, cast(avg(cPeptide) as decimal(10,2)) as cPeptide
    from clinictestresults
	where  cPeptide is not null and deleted = 0
    group by clientId, itz(bloodworktime)
 ) t
 on c.clientId = t.clientId and date(c.firstbloodworkdateITZ) = eventDate
 left join
 (
	select a.patientId as clientId,   NPS, NutritionSatisfaction as Nut_Sat
	 from  bi_patient_interview_response  a
	 join
	 ( select patientId, max(timestamp) as timestamp from bi_patient_interview_response group by patientId) b
	 on a.patientId = b.patientId and a.timestamp = b.timestamp
 ) n
 on c.clientId = n.clientId
 left join v_bi_patient_nut_happiness h
 on c.clientId = h.clientId
join
(
 select clientId, measure_event_date as event_date, 'Default' as measure_group, case when measure_name = 'TAC' then 'NUT TAC' when measure_name = 'Ketone' then '5Dk' else measure_name end as measure_name, average_measure as measure
 from bi_measures m
 where (measure_type  = '1d' and measure_name in( 'Action Score', 'TAC' )or measure_type  = '5d' and measure_name = 'Ketone')  and m.measure_event_date between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

 union all

 select clientId,  date as event_date, 'Default' as measure_group, 'NUT ADH' as measure_name, case when nut_adh_syntax = 'YES' then 1 when nut_adh_syntax = 'NO' then 0 else NULL end as measure
 from v_bi_patient_nut_adh_syntax
 where     date between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

union all

select clientId,  date as event_date, 'Default' as measure_group, 'NUT SYNTAX' as measure_name, nut_syntax as measure
 from v_bi_patient_nut_adh_syntax
 where     date between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

union all
select clientId,  date as event_date, 'Default' as measure_group, 'Macro' as measure_name, round(macro) as measure
 from nutritionscores
 where     date between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

union all

select clientId,  date as event_date,  'Default' as measure_group,'Biota' as measure_name, round(Biota) as measure
 from nutritionscores
 where     date between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

union all

select clientId,  date as event_date, 'Default' as measure_group, 'Micro' as measure_name, round(micro) as measure
 from nutritionscores
 where     date between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

union all

select clientId, date as event_date, 'Output' as measure_group, category as measure_name, measure
from bi_twins_health_output_score where date between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

union all

select clientId, mealdate as event_date, 'Input' as measure_group, nutrient as measure_name, measure
from bi_twins_health_score where mealdate between (ITZ(NOW()) - INTERVAL 1 MONTH) and ITZ(now())

) m
on c.clientId = m.clientId
where c.is_row_current = 'Y' and c.status in ('active','discharged','inactive') 
;


truncate table bi_patient_internal_progress
; 

insert into bi_patient_internal_progress
(
clientid, doctor, coach,    
treatmentDays, cohortGroup, d10_success, d35_success, D0_cPeptide, NPS, Nut_Sat
,NUT_HAP, food_rating, total_disenrollment_incidents, total_nutrition_incidents, interview_score
,event_date, measure_group, measure_name, measure
) 
select clientid, doctor, coach,    
treatmentDays, cohortGroup, d10_success, d35_success, D0_cPeptide, NPS, Nut_Sat
,NUT_HAP, food_rating, total_disenrollment_incidents, total_nutrition_incidents, interview_score
,event_date, measure_group, measure_name, measure
from tmp_patient_internal_progress
;

drop temporary table tmp_patient_internal_progress
;


update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_patient_internal_progress'
;

END$$
DELIMITER ;
