CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_doctor_payout_detail`()
begin
	
update dailyprocesslog 
set startDate = now()
where processName = 'sp_load_bi_doctor_payout_detail'
;



-- Get all active patients at the end of each month along with their 
drop temporary table if exists tmp_last_3_months_active_patients_on_last_day
;

create temporary table tmp_last_3_months_active_patients_on_last_day as 
select distinct b.date, b.clientid, b.doctorId, b.status, source, planCode, c.active_status_start_date 
from
(select distinct date_sub(date, interval dayofmonth(date) day) as lastDayOfMonth from v_date_to_date) a	inner join 
(
		select date, b.clientid, b.doctorId, b.status, source, plancode, status_start_date
		from v_date_to_date a inner join 
		(
			select distinct b.clientid,  source, plancode, b.status, status_start_date, status_end_date, c.doctorId 
			from 
			bi_patient_status b inner join v_client_tmp c on b.clientid = c.clientid 
			where b.status = 'active' 
			and not exists (select 1 from bi_patient_status a where b.clientid = a.clientid and a.status = 'pending_active' and a.status_start_date >= b.status_end_date) 
			and c.patientname not like '%obsolete%' 
			) b on a.date between b.status_start_date and b.status_end_date 
		where date >= date_sub(itz(now()), interval 3 month)
) b on a.lastDayOfMonth = b.date 
left join ( 
			select clientid, min(status_start_date) as active_status_start_date 
			from bi_patient_status  
			where status = 'active' 
			group by clientid 
			) c on b.clientid = c.clientid
;

create index idx_tmp_last_3_months_active_patients_on_last_day on tmp_last_3_months_active_patients_on_last_day(clientid asc, date asc, source, planCode); 


	-- Get all invoices paid in a month so we can calculate the doctor referral and doctor retention fees
	drop temporary table if exists tmp_invoices_paid_in_last_4_months;
	
	create temporary table tmp_invoices_paid_in_last_4_months as 
	select clientid, invoice_date, invoice_number, invoice_status, payment_date, payment_mode, invoice_amount, payment_amount, cf_service_purchased
	from 
	(
		select distinct a.cf_clientid_unformatted as clientid, c.invoice_date, c.number as invoice_number, c.status as invoice_status, d.date as payment_date, d.payment_mode, c.total as invoice_amount, d.amount as payment_amount, c.cf_service_purchased
		from importedbilling.zoho_customers a inner join clients b on a.cf_clientid_unformatted = b.id
											  left join importedbilling.zoho_invoices c on a.customer_id = c.customer_id
											  left join importedbilling.zoho_payments d on c.invoice_id = d.invoice_id
	) s 
	where payment_date >= date_sub(now(), interval 4 month) and invoice_status = 'PAID' and cf_service_purchased is null -- cf_service_purchased is null because this field is being used for twin store purchases
	;
	
	create index idx_tmp_invoices_paid_in_last_4_months on tmp_invoices_paid_in_last_4_months (clientid asc, invoice_date, invoice_number, payment_date); 


-- All medicine recommendations and the publish status
drop temporary table if exists tmp_med_recommendations; 

create temporary table tmp_med_recommendations as 
select f1.clientid, f1.doctorId, recommendationDate, recommendationdatetime, status, actualmedication, recommendedmedication,
case when f1.recommendationdate <> f2.newpublishingdate then 'Repeated' 
	 when f1.recommendationdate = f2.newpublishingdate then 'New' else null end as publishStatus, actionTime, actionedDate, appointmentTime, newActionTime,
	 case when f1.status in ('MANUAL','OVERRIDDEN') and f1.actualmedication ='No medicine' and f1.recommendedmedication='No medicine' then 'Yes' else null end as changeType_Psuedo
from
(
		select distinct a.clientid, a.doctorId, recommendationdate as recommendationdatetime, actionedDate, 
		date(recommendationdate) as recommendationdate,
		a.status, actionTime, appointmentTime, newActionTime, actualmedication, recommendedmedication  from 
		(
			select a.clientid, a.doctorId, a.medicationTierId, a.eventtime as recommendationdate, a.status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime, itz(da.eventtime) as appointmentTime,
            timestampdiff(HOUR, itz(da.eventtime), itz(b.dateAdded)) as newActionTime, b.actualMedication as actualmedication,b.recommendedMedication as recommendedmedication
			from
			(
					select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId, type, b.doctorId
					from medicationrecommendations a inner join v_client_tmp b on a.clientid = b.clientid 
													 left join dim_client c on a.clientid = c.clientid and c.is_row_current = 'y'
					where type = 'DIABETES'
			) a inner join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
				left join clientappointments da on a.clientid = da.clientid and da.type = 'DOCTOR_APPOINTMENT' and da.status in ('completed') and date(itz(b.dateAdded)) = date(itz(da.eventtime))
		) a 
		where date(recommendationdate) >= date_sub(date(itz(now())), interval 3 month)
) f1 left join (select clientid, min(date(itz(eventtime))) as newpublishingdate from medicationrecommendations where type = 'DIABETES' group by clientid) f2 on f1.clientid = f2.clientid  -- where medicationTierId is not null 
;

create index idx_tmp_med_recommendations on tmp_med_recommendations(clientid, doctorId, recommendationDate, actionedDate); 


-- all completed doctor appointments
drop temporary table if exists tmp_doctor_appointments; 

create temporary table tmp_doctor_appointments as 	
select a.clientid, date(itz(a.eventtime)) as DC_scheduled_date, b.doctorId		   
from clientappointments a inner join doctorappointments b on a.clientid = b.clientid and a.clientAppointmentId = b.clientAppointmentId
where type = 'DOCTOR_APPOINTMENT' and status = 'COMPLETED'
and date(itz(eventtime)) >= date_sub(date(itz(now())), interval 3 month)
;

create index idx_tmp_doctor_appointments on tmp_doctor_appointments(clientid asc, DC_scheduled_date, doctorId)
;



drop temporary table if exists tmp_doctor_payout_detail; 

create temporary table tmp_doctor_payout_detail as 
select a.doctorId, a.date as eventDate, 
a.totalActiveEOM, 
a.totalActiveEOM_nonDoctorChannel, 
a.totalActiveEOM_DoctorChannel, 
a.totalActiveEOM_nonDoctorChannel_GoldPlatinum, 
a.totalActiveEOM_DoctorChannel_Gold, 
a.totalActiveEOM_DoctorChannel_Platinum, 

a.total_DoctorChannel_Gold_Qtr_Acquisition,
a.total_DoctorChannel_Gold_Annual_Acquisition,

a.total_DoctorChannel_Platinum_Qtr_Acquisition,
a.total_DoctorChannel_Platinum_Annual_Acquisition,

count(distinct case when b1.clientid is not null and b1.changeType_Psuedo is null then b1.actionedDate else null end) as total_medpublish_actioned,
count(distinct case when b2.clientid is not null then b2.DC_scheduled_date else null end) as total_consult_appointment_completed
from 
(
	select date, a.doctorId, 
	count(distinct a.clientid) as totalActiveEOM,
	count(distinct case when a.source <> 'DOCTOR' then a.clientid else null end) as totalActiveEOM_nonDoctorChannel,
	count(distinct case when a.source = 'DOCTOR' then a.clientid else null end) as totalActiveEOM_DoctorChannel,
	count(distinct case when a.source <> 'DOCTOR' and (a.planCode like '%GoldPro%' or a.planCode like '%platinum%') then a.clientid else null end) as totalActiveEOM_nonDoctorChannel_GoldPlatinum,
	count(distinct case when a.source = 'DOCTOR' and a.planCode like '%GoldPro%' then a.clientid else null end) as totalActiveEOM_DoctorChannel_Gold,
	count(distinct case when a.source = 'DOCTOR' and a.planCode like '%Platinum%' then a.clientid else null end) as totalActiveEOM_DoctorChannel_Platinum,

/*
	-- Retention - Only quarterly patients
	count(distinct case when a.source <> 'DOCTOR' and ((a.planCode like '%GoldPro%' and a.planCode like '%Quarter%') or (a.planCode like '%platinum%' and a.planCode like '%QP%')) and datediff(date, active_status_start_date) > 60 and b.clientid is not null then a.clientid else null end) as total_non_doctor_channel_GOLD_PLAT_QTR_paid_currentMonth_RET,
	count(distinct case when a.source = 'DOCTOR' and (a.planCode like '%GoldPro%' and a.planCode like '%Quarter%') and datediff(date, active_status_start_date) > 60 and b.clientid is not null then a.clientid else null end) as total_doctor_channel_GOLD_QTR_paid_currentMonth_RET,
	count(distinct case when a.source = 'DOCTOR' and (a.planCode like '%platinum%' and a.planCode like '%QP%') and datediff(date, active_status_start_date) > 60 and b.clientid is not null then a.clientid else null end) as total_doctor_channel_PLATINUM_OTR_paid_currentMonth_RET,
	-- Retention - Only quarterly patients
*/

	-- Acquisition
	count(distinct case when a.source = 'DOCTOR' and a.planCode like '%GoldPro%' and a.planCode like '%Quarter%' and b.clientid is not null and month(active_status_start_date) = month(date) and year(active_status_start_date) = year(date) then a.clientid else null end) as total_DoctorChannel_Gold_Qtr_Acquisition,
	count(distinct case when a.source = 'DOCTOR' and a.planCode like '%GoldPro%' and a.planCode like '%Annual%' and b.clientid is not null and month(active_status_start_date) = month(date) and year(active_status_start_date) = year(date) then a.clientid else null end) as total_DoctorChannel_Gold_Annual_Acquisition,

	count(distinct case when a.source = 'DOCTOR' and a.planCode like '%Platinum%' and a.planCode like '%QP%' and b.clientid is not null and month(active_status_start_date) = month(date) and year(active_status_start_date) = year(date) then a.clientid else null end) as total_DoctorChannel_Platinum_Qtr_Acquisition,
	count(distinct case when a.source = 'DOCTOR' and a.planCode like '%Platinum%' and a.planCode like '%AP%' and b.clientid is not null and month(active_status_start_date) = month(date) and year(active_status_start_date) = year(date) then a.clientid else null end) as total_DoctorChannel_Platinum_Annual_Acquisition
	-- 


	from tmp_last_3_months_active_patients_on_last_day a left join tmp_invoices_paid_in_last_4_months b on a.clientid= b.clientid and month(b.payment_date) = month(a.date) and year(b.payment_date) = year(a.date)						 
	group by date, a.doctorId
) a left join tmp_med_recommendations b1 on a.doctorId = b1.doctorId and month(b1.actionedDate) = month(a.date) and year(b1.actionedDate) = year(a.date)
    left join tmp_doctor_appointments b2 on a.doctorId = b2.doctorId and month(b2.DC_scheduled_date) = month(a.date) and year(b2.DC_scheduled_date) = year(a.date)
group by date, doctorId
;


create index idx_tmp_doctor_payout_detail on tmp_doctor_payout_detail(doctorId asc, eventDate asc);

insert into bi_doctor_payout_detail 
(
	doctorId, 
	eventDate, 
	totalActiveEOM, 
	totalActiveEOM_nonDoctorChannel, 
	totalActiveEOM_DoctorChannel, 
	totalActiveEOM_nonDoctorChannel_GoldPlatinum, 
	totalActiveEOM_DoctorChannel_Gold,
	totalActiveEOM_DoctorChannel_Platinum, 
	total_DoctorChannel_Gold_Qtr_Acquisition,
	total_DoctorChannel_Gold_Annual_Acquisition, 
	total_DoctorChannel_Platinum_Qtr_Acquisition, 
	total_DoctorChannel_Platinum_Annual_Acquisition, 
	total_medpublish_actioned, 
	total_consult_appointment_completed
)
select a.doctorId, a.eventDate, 
a.totalActiveEOM, 
a.totalActiveEOM_nonDoctorChannel, 
a.totalActiveEOM_DoctorChannel, 
a.totalActiveEOM_nonDoctorChannel_GoldPlatinum, 
a.totalActiveEOM_DoctorChannel_Gold, 
a.totalActiveEOM_DoctorChannel_Platinum, 

a.total_DoctorChannel_Gold_Qtr_Acquisition,
a.total_DoctorChannel_Gold_Annual_Acquisition,

a.total_DoctorChannel_Platinum_Qtr_Acquisition,
a.total_DoctorChannel_Platinum_Annual_Acquisition,

a.total_medpublish_actioned,
a.total_consult_appointment_completed

from tmp_doctor_payout_detail a 
where not exists (select 1 from bi_doctor_payout_detail b where a.doctorId = b.doctorID and a.eventDate = b.eventDate)
;



update bi_doctor_payout_detail a inner join tmp_doctor_payout_detail b on (a.doctorId = b.doctorID and a.eventDate = b.eventDate)
set a.totalActiveEOM = b.totalActiveEOM,
	a.totalActiveEOM_nonDoctorChannel = b.totalActiveEOM_nonDoctorChannel, 
	a.totalActiveEOM_DoctorChannel = b.totalActiveEOM_DoctorChannel, 
	a.totalActiveEOM_nonDoctorChannel_GoldPlatinum = b.totalActiveEOM_nonDoctorChannel_GoldPlatinum, 
	a.totalActiveEOM_DoctorChannel_Gold = b.totalActiveEOM_DoctorChannel_Gold, 
	a.totalActiveEOM_DoctorChannel_Platinum = b.totalActiveEOM_DoctorChannel_Platinum, 
	a.total_DoctorChannel_Gold_Qtr_Acquisition = b.total_DoctorChannel_Gold_Qtr_Acquisition,
	a.total_DoctorChannel_Gold_Annual_Acquisition = b.total_DoctorChannel_Gold_Annual_Acquisition,
	a.total_DoctorChannel_Platinum_Qtr_Acquisition = b.total_DoctorChannel_Platinum_Qtr_Acquisition,
	a.total_DoctorChannel_Platinum_Annual_Acquisition = b.total_DoctorChannel_Platinum_Annual_Acquisition,
	a.total_medpublish_actioned = b.total_medpublish_actioned,
	a.total_consult_appointment_completed = b.total_consult_appointment_completed
where a.eventdate >= date_sub(date(itz(now())), interval 1 month)
; 
-- DC payout




drop temporary table if exists tmp_last_3_months_active_patients_on_last_day;	
drop temporary table if exists tmp_med_recommendations; 
drop temporary table if exists tmp_doctor_appointments; 
drop temporary table if exists tmp_doctor_payout_detail; 
	
	
update dailyprocesslog 
set updateDate = now()
where processName = 'sp_load_bi_doctor_payout_detail'
;

END