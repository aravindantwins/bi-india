CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_food_supplement_log_detail`()
begin
	
/*
2019-09-23 Aravindan - Added deletion part to handle the foodlogs that are getting removed by OPS at the later stages. 
2020-01-13 Aravindan - Since the supplements and foodlogs dont work the same way, in order to find the actual nutrient values for supplements, first get the
					   1 unit nutrient value by dividing the nutrient/measurementAmount (TCU) and then multiply it with the TargetValue (quantity).
                       As per email from Haoyu on Jan 8, 2020
2020-04-07 Aravindan  - Included supplmentlogs deletion logic in the code. To remove any old data loaded into the main detail table but not available anymore in platform
2020-05-18 Aravindan  - Made some changes in the foodlog section (especially the deletion part) as it was long running. 
2021-03-30 Aravindan - Choose the E5grading from personalizedfoods if the food is personalized for a patient. If it is not available, then use the regular food grading.
*/

/*log the starttime*/
update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_food_supplement_log_detail'
;


drop temporary table if exists tmp_foodlog_section; 
create temporary table tmp_foodlog_section
(
		category varchar(20), 
		clientid int not null, 
        eventdate date, 
		eventTime datetime,
        categorytype varchar(30),
		itemId int, 
		itemName varchar(200),
		quantity decimal(10,5),
		E5grading varchar(25),
		measurementType varchar(100),
		measurementAmount decimal(10,5),
		net_carb decimal(12,5),
		calories decimal(12,5), 
		fat decimal(12,5), 
		fibre decimal(12,5),
		protein decimal(12,5),
		total_Carb decimal(12,5),
		netGICarb decimal(12,5),
        foodRating decimal(10,2),
        creator varchar(50) null,
		chosenRecommendationRating decimal(8,5) null
);

insert into tmp_foodlog_section 
select distinct 
 'foodlog' as category, 
		fl.clientid, 
        fl.mealdate as eventdate, 
		-- convert_tz(fl.mealtime, @@time_zone, ct.timezoneId)  as eventTime, -- Convert the mealtime as per patient's timezone
        concat(fl.mealdate,' ',fl1.mealDateTimeOnly) as eventTime,
		fl.mealtype as categorytype,
		if(cf.customfoodid is not null, cf.customfoodid, b.foodid) as itemId, 
		if(cf.foodName is not null, cf.foodName, b.foodlabel) as itemName,
		fl.quantity as quantity,
		CASE WHEN ISNULL(ifnull(p.neRecommendationRating, b.recommendationRating)) THEN 'Purple' -- take the recommendationRating from personalizedfoods table if the food is personalized for a patient.
		 WHEN (ifnull(p.neRecommendationRating, b.recommendationRating) <= 1) THEN 'Red'
		 WHEN ((ifnull(p.neRecommendationRating, b.recommendationRating) > 1) AND (ifnull(p.neRecommendationRating, b.recommendationRating)  <= 2)) THEN 'Orange'
		 WHEN ((ifnull(p.neRecommendationRating, b.recommendationRating) > 2) AND (ifnull(p.neRecommendationRating, b.recommendationRating)  <= 3)) THEN 'Green'
		 WHEN (ifnull(p.neRecommendationRating, b.recommendationRating) > 3) THEN 'Green*'
		END AS E5Grading,
		-- fl.measure as measurementType,
		left(fl.measure, 100) as measurementType, -- to handle some invalid data in the clinic app. 
		fl.basequantity as measurementAmount,
		cast((b.net_carb * fl.baseQuantity) as decimal(12,5)) as net_carb,
		cast((b.calories * fl.baseQuantity) as decimal(12,5)) as calories, 
		cast((b.fat * fl.baseQuantity) as decimal(12,5)) as fat, 
		cast((b.fibre * fl.baseQuantity) as decimal(12,5)) as fibre,
		cast((b.protein * fl.baseQuantity) as decimal(12,5)) as protein,
		cast((b.total_Carb *fl.baseQuantity) as decimal(12,5)) as total_carb,
		b.netGICarb,
        foodRating as foodRating,
		concat(fl1.creatorRole,' - ', fl1.creatorType) as creator,
		ifnull(p.neRecommendationRating, b.recommendationRating) as chosenRecommendationRating
from foodlogs_view fl inner join bi_patient_status a on (fl.clientid = a.clientid and a.status='active' and fl.mealdate between a.status_start_date and a.status_end_date)
				 inner join v_client_tmp ct on a.clientid = ct.clientid 
				 left join (
									select
									foodid, foodlabel,
                                    recommendationRating,
									CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
										 WHEN (recommendationRating <= 1) THEN 'Red'
										 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
										 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
										 WHEN (recommendationRating > 3) THEN 'Green*'
									END AS E5grading,
									measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb -- level1
									,calories, carb as total_Carb, protein, fat, fibre, -- level 2,
                                    cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as decimal(10,5)) AS netGICarb
									from foods
						) b on fl.foodid = b.foodid
				left join personalizedfoods p on fl.foodid = p.foodid and fl.clientid = p.clientid          
                left join customfoods cf on fl.foodlogid = cf.foodlogid and fl.clientid = cf.clientid -- and deleted = 0
				left join foodlogs fl1 on fl.foodlogid = fl1.foodlogid
-- where fl.mealdate >= date_sub(date(now()), interval 6 month) and fl.mealdate <= date(now())
-- group by category, fl.clientid, mealdate, mealtype, fl.foodid
;


insert into bi_food_supplement_log_detail
(category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, measurementType, measurementAmount,
net_carb, calories, fat, fibre, protein, total_Carb, netGICarb, foodRating, creator)

select category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, -- Choose the E5grading from personalizedfoods if the food is personalized for a patient. If it is not available, then use the regular food grading.
measurementType, measurementAmount,net_carb,calories, fat, fibre, protein, total_Carb, netGICarb, foodRating, creator
from tmp_foodlog_section a where not exists 
(
select 1 from bi_food_supplement_log_detail b where a.category = b.category and a.clientid = b.clientid and a.eventdate = b.eventdate and a.eventtime = b.eventtime and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0)
);

create index tmp_idx_foodlog on tmp_foodlog_section (category asc, clientid asc, eventdate asc, categorytype asc, itemid asc);

update bi_food_supplement_log_detail a 
inner join tmp_foodlog_section b 
on (a.category = b.category and a.clientid = b.clientid and a.eventdate = b.eventdate and a.eventtime = b.eventtime and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0))
set a.eventtime = b.eventtime, 
	a.itemName = b.itemName,
    a.quantity = b.quantity,
    a.E5grading = b.E5grading,
    a.measurementType = b.measurementType,
    a.measurementAmount = b.measurementAmount,
    a.net_carb = b.net_carb,
    a.calories = b.calories,
    a.fat = b.fat,
    a.fibre = b.fibre,
    a.protein = b.protein,
    a.total_carb = b.total_carb,
    a.netGICarb = b.netGICarb,
    a.foodRating = b.foodRating,
    a.creator = b.creator
;

-- it is noted that the foodlogs added by coaches or patients are getting removed at the later time
-- so the section below is to delete such removals from this history tracking table. Strange but that is what
-- needed otherwise in the report, it will never match what platform shows

-- Bring in only the data to check for deletion - To be safer, lets check last 3 months of data. 
drop temporary table if exists tmp_data_to_check;
create temporary table tmp_data_to_check 
as 
select a.category, a.clientid, a.eventdate, a.eventtime, a.categorytype, a.itemId
from bi_food_supplement_log_detail  a 
where -- a.eventdate >= date_sub(date(now()), interval 3 month) and a.eventdate <= date(now()) and
 a.category = 'foodlog'
; 

create index idx_tmp_data_to_check on tmp_data_to_check (category asc, clientid asc, eventdate asc, eventtime asc, categorytype asc, itemid asc);

create temporary table tmp_foodlogs_removed_by_ops as 
select a.category, a.clientid, a.eventdate, a.eventtime, a.categorytype, a.itemId
from tmp_data_to_check a
where not exists 
(
	select 1 from tmp_foodlog_section b where a.category = b.category and a.clientid = b.clientid and a.eventdate = b.eventdate and a.eventtime = b.eventTime and a.categoryType = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemId,0)
);

/* OLD definition - long running
create temporary table tmp_foodlogs_removed_by_ops as 
select a.category, a.clientid, a.eventdateITZ, a.eventtimeITZ, a.categorytype, a.itemId
from bi_food_supplement_log_detail  a 
where a.eventdateitz >= date_sub(date(itz(now())), interval 3 month) and a.eventdateitz <= date(itz(now()))
and a.category = 'foodlog'
and not exists 
-- (select 1 from foodlogs_view b where a.clientid = b.clientid and a.eventdateitz = b.mealdate and a.eventtimeITZ = itz(b.mealtime) and a.categoryType = b.mealtype and a.itemid = b.foodid)
(select 1 from tmp_foodlog_section b where a.clientid = b.clientid and a.eventdateitz = b.eventdateITZ and a.eventtimeITZ = b.eventTimeITZ and a.categoryType = b.categorytype and a.itemid = b.itemId)
;
*/

delete bi_food_supplement_log_detail
from bi_food_supplement_log_detail, tmp_foodlogs_removed_by_ops
where bi_food_supplement_log_detail.clientid = tmp_foodlogs_removed_by_ops.clientid
	  and bi_food_supplement_log_detail.eventdate = tmp_foodlogs_removed_by_ops.eventdate
      and bi_food_supplement_log_detail.eventtime = tmp_foodlogs_removed_by_ops.eventtime
      and bi_food_supplement_log_detail.categoryType = tmp_foodlogs_removed_by_ops.categoryType
      and ifnull(bi_food_supplement_log_detail.itemid,0) = ifnull(tmp_foodlogs_removed_by_ops.itemid,0)
      and bi_food_supplement_log_detail.category = 'foodlog'
;

-- Clean up the used temporary tables from the processed steps 
drop temporary table if exists tmp_foodlogs_removed_by_ops;
drop temporary table if exists tmp_foodlog_section;
drop temporary table if exists tmp_data_to_check;

drop temporary table if exists tmp_supplementlog_section;

create temporary table tmp_supplementlog_section
(
		category varchar(20), 
		clientid int not null, 
        eventdate date, 
		eventTime datetime,
        categorytype varchar(30),
		itemId int, 
		itemName varchar(200),
		quantity decimal(10,5),
		E5grading varchar(25),
		measurementType varchar(100),
		measurementAmount decimal(10,5),
		net_carb decimal(10,5),
		calories decimal(10,5), 
		fat decimal(10,5), 
		fibre decimal(10,5),
		protein decimal(10,5),
		total_Carb decimal(10,5),
		netGICarb decimal(10,5)
);
                        
insert into tmp_supplementlog_section                       
SELECT 'supplementlog' as category, 
        s.clientId AS clientId,
        s.eventDate,
        s.eventTime AS eventTime,
        timeslot AS categorytype,
        s.supplementId AS itemId,
        t.supplementName AS itemName,
        s.quantity AS quantity,
        '' as E5grading,
        t.measurementType,
        t.measurementAmount,
        (((t.carb - t.fibre)/t.measurementAmount) * s.quantity) AS netCarb,
        ((t.calories/t.measurementAmount) * s.quantity) AS calories,
        ((t.fat/t.measurementAmount) * s.quantity) AS fat,
        ((t.fibre/t.measurementAmount) * s.quantity) AS fiber,
        ((t.protein/t.measurementAmount) * s.quantity) AS protein,
        ((t.carb/t.measurementAmount) * s.quantity) AS totalCarb,
        cast(IFNULL((((t.carb - t.fibre) * t.glycemicIndex) / 55),0) as decimal(10,5)) AS netGICarb
FROM
        (
			SELECT DISTINCT 
					t.clientId AS clientId,
                    date(t.scheduledLocalTime) AS eventDate,
					max(t.scheduledLocalTime) AS eventTime, -- Convert the intaketime as per patient's timezone
					ifnull(s1.supplementId, s2.supplementId) AS supplementId,
					t.type AS supplement,
                    t.timeslot AS timeslot,
					sum(t.targetvalue) AS quantity
			FROM
					twins.clienttodoitems t
			JOIN 	twins.bi_patient_status p ON (t.clientId = p.clientid AND p.status = 'active' AND CAST(t.scheduledLocalTime AS DATE) BETWEEN p.status_start_date AND p.status_end_date)
			inner join v_client_tmp ct on p.clientid = ct.clientid 
            left join supplements s1 on t.supplementId = s1.supplementId
			left join (
					-- Handle the Supplements in 2 sections if the SupplementId is not captured in ClientToDoItems
					-- Because there are certain supplements have the same patientActionType but we have to choose the 1 to bring the nutrition values.
					-- 1. If there is a supplementId in the todo item, use the specific supplement
					-- 2. If there is no supplement, find all supplements mapped by the patientActionType attribute
					-- 3. If the the mapped supplements only have 1, then use the one
					-- 4. if there are multiple, use the one with “recommended” attribute set as true
					-- 5. If there are multiple with recommended set as True, use the 1st one from the results. So using min(supplementId)
								select s1.patientActionType, min(s2.supplementId) as supplementId
								from 
								(
									select patientActionType, count(distinct supplementId) as total 
									from supplements 
									where patientActionType is not null and draftstatus = 'published'
									group by patientActionType 
									having count(distinct supplementId) > 1
								) s1 inner join supplements s2 on s1.patientActionType = s2.patientActionType and s2.recommended = 1
								group by s1.patientActionType

								union all 

								select s1.patientActionType, s2.supplementId
								from 
								(
									select patientActionType, count(distinct supplementId) as total 
									from supplements 
									where patientActionType is not null and draftstatus = 'published'
									group by patientActionType 
									having count(distinct supplementId) = 1
								) s1 inner join supplements s2 on s1.patientActionType = s2.patientActionType and s2.draftStatus = 'published'
					  ) s2 on t.type = s2.patientActionType
			WHERE t.category = 'SUPPLEMENT' AND t.status = 'FINISHED'
            group by clientid, date(t.scheduledLocalTime), ifnull(s1.supplementId, s2.supplementId), t.type, timeslot
        ) s
LEFT JOIN twins.supplements t ON (s.supplementId = t.SupplementId)
-- where s.eventDateITZ >= date_sub(date(itz(now())), interval 2 month) and s.eventDateITZ <= date(itz(now()))
;

create index tmp_idx_supplementlog on tmp_supplementlog_section (category asc, clientid asc, eventdate asc, eventtime asc, categorytype asc, itemid asc, itemname asc);

insert into bi_food_supplement_log_detail
(category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, measurementType, measurementAmount,
net_carb,calories, fat, fibre, protein, total_Carb, netGICarb)

select category, clientid, eventdate, eventTime, categorytype, itemId, itemName, quantity, E5grading, 
measurementType, measurementAmount,net_carb,calories, fat, fibre, protein, total_Carb, netGICarb
from tmp_supplementlog_section a where not exists 
(
select 1 from bi_food_supplement_log_detail b where a.category = b.category and a.clientid = b.clientid and a.eventdate = b.eventdate and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0) and a.itemName = b.itemName
);

update bi_food_supplement_log_detail a 
inner join tmp_supplementlog_section b on (a.category = b.category and a.clientid = b.clientid and a.eventdate = b.eventdate and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0) and a.itemName = b.itemName)
set	a.eventtime = b.eventtime, 
	a.itemName = b.itemName,
    a.quantity = b.quantity,
    a.E5grading = b.E5grading,
    a.measurementType = b.measurementType,
    a.measurementAmount = b.measurementAmount,
    a.net_carb = b.net_carb,
    a.calories = b.calories,
    a.fat = b.fat,
    a.fibre = b.fibre,
    a.protein = b.protein,
    a.total_carb = b.total_carb,
    a.netGICarb = b.netGICarb
;

-- Drop any rows that were already loaded into the bi_food_supplement_log_detail table but not available now
-- This is happening because users are changing the supplement details in the supplements master page (example Multivitamin and Multivitamin Tablet). 
-- platform recognizes these 2 as 1 type called MULTI_VITAMIN but both have different nutrient values. 
drop temporary table if exists tmp_supplements_to_be_removed
;

create temporary table tmp_supplements_to_be_removed as 
select category, clientid, eventdate, eventtime, categorytype, itemId
from bi_food_supplement_log_detail  a 
where a.eventdate <= date(now())
and category = 'supplementlog'
and not exists 
(select 1 from tmp_supplementlog_section b where a.category = b.category and a.clientid = b.clientid and a.eventdate = b.eventdate and a.eventtime = b.eventTime and a.categoryType = b.categoryType and ifnull(a.itemid,0) = ifnull(b.itemid,0))
;

delete bi_food_supplement_log_detail
from bi_food_supplement_log_detail, tmp_supplements_to_be_removed
where bi_food_supplement_log_detail.clientid = tmp_supplements_to_be_removed.clientid
	  and bi_food_supplement_log_detail.eventdate = tmp_supplements_to_be_removed.eventdate
      and bi_food_supplement_log_detail.eventtime = tmp_supplements_to_be_removed.eventtime
      and bi_food_supplement_log_detail.categoryType = tmp_supplements_to_be_removed.categoryType
      and ifnull(bi_food_supplement_log_detail.itemid,0) = ifnull(tmp_supplements_to_be_removed.itemid,0)
      and bi_food_supplement_log_detail.category = 'supplementlog'
;

drop temporary table tmp_supplements_to_be_removed
;

drop temporary table tmp_supplementlog_section;

/*log the endtime*/
update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_food_supplement_log_detail'
;

END