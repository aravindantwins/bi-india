DELIMITER $$
CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `sp_load_bi_incident_summary`(  )
BEGIN

update dailyprocesslog 
set startDate = now()
where processname = 'sp_load_bi_incident_summary'
;

drop temporary table if exists tmp_bi_incident_summary;

CREATE temporary TABLE tmp_bi_incident_summary (
  eventdate date NOT NULL,
  state varchar(50) DEFAULT NULL,
  city varchar(50) DEFAULT NULL,
  incidentType varchar(50) DEFAULT NULL,
  L2_category varchar(50) DEFAULT NULL,
  system_status varchar(50) DEFAULT NULL,
  category varchar(50) DEFAULT NULL,
  grievance varchar(50) DEFAULT NULL,
  cnt int
); 


insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
select  d.date, state, city, incidentType, L2_category,  system_status,  'Open Backlog' as category, grievance, count(distinct incidentId) as cnt
from 
v_date_to_date d
left join
(
select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
       
       c.state,
       c.city_new as city,
        m.status, 
        t.status as system_status,
        date(itz(report_time)) as incident_report_date, 
        report_time as report_time, 
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date,
        m.grievance as grievance,
        close_duration_hours as closed_duration_hours 
from bi_incident_mgmt m 
join v_client_tmp c 
on m.clientId = c.clientId
join 
 bi_patient_status t 
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time))  >= '2020-12-01' and  t.status in ( 'ACTIVE', 'PENDING_ACTIVE') 
)s1
on d.date between incident_report_date and ifnull(date_sub(resolved_date, interval 1 day), ITZ(now()))
where d.date > '2020-12-01'
group by date, state, city, incidentType, L2_category,  system_status, grievance
 ;

 

insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
select  d.date, state, city, incidentType, L2_category,  system_status,  'Open Backlog > 3 days' as category, grievance,
count(distinct case when datediff(d.date, incident_report_date) >3 THEN incidentId ELSE NULL END) as cnt
from 
v_date_to_date d
left join

(

select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
       
        
        c.state,
        c.city_new as city,
        t.status as system_status,
        date(itz(report_time)) as incident_report_date,
       
       m.grievance as grievance,
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date
       
from bi_incident_mgmt m 
join v_client_tmp c 
on m.clientId = c.clientId
join 
 bi_patient_status t 
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time))  >= '2020-12-01'  and t.status in ( 'ACTIVE', 'PENDING_ACTIVE')
)s1
on d.date between incident_report_date and ifnull(date_sub(resolved_date, interval 1 day), ITZ(now()))
where d.date > '2020-12-01'
group by date, state, city, incidentType, L2_category,  system_status, grievance
 ;
 
 
 
 insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
select d.date, state, city, incidentType, L2_category,  system_status,  'First Response > 24 hrs' as category, grievance,
count(distinct case when first_response_hr > 24 THEN incidentId ELSE NULL END) as cnt
from 
v_date_to_date d
left join

(

select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
		c.state,
        c.city_new as city,
        t.status as system_status,
        date(itz(report_time)) as incident_report_date, 
  
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date,
        m.grievance as grievance,
        close_duration_hours as closed_duration_hours,
        timestampdiff(hour, report_time, first_response_time) as first_response_hr
from bi_incident_mgmt m 
join v_client_tmp c 
on m.clientId = c.clientId
join 
 bi_patient_status t 
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time))  >= '2020-12-01'  and t.status in ( 'ACTIVE', 'PENDING_ACTIVE') 
)s1
on d.date between incident_report_date and ifnull(date_sub(resolved_date, interval 1 day), ITZ(now()))
where d.date > '2020-12-01'
group by date, state, city, incidentType, L2_category,  system_status, grievance

;


 
 insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
select d.date, state, city, incidentType, L2_category,  system_status,  'No Response > 1 day' as category, grievance,
count(distinct case when first_response_hr is null and datediff(d.date, date(itz(report_time)) )>1 THEN incidentId ELSE NULL END) as cnt
from 
v_date_to_date d
left join

(

select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
       
        m.status, 
        c.state,
        c.city_new as city,
        t.status as system_status,
        date(itz(report_time)) as incident_report_date, 
        report_time as report_time, 
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date,
        close_duration_hours as closed_duration_hours,
        m.grievance as grievance,
        timestampdiff(hour, report_time, first_response_time) as first_response_hr
from bi_incident_mgmt m 

join v_client_tmp c 
on m.clientId = c.clientId
join bi_patient_status t 
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time))  >= '2020-12-01' and t.status in ( 'ACTIVE', 'PENDING_ACTIVE')  
)s1
on d.date between incident_report_date and ifnull(date_sub(resolved_date, interval 1 day), date_sub(ITZ(now()), interval 1 day))
where d.date > '2020-12-01'
group by date, state, city, incidentType, L2_category,  system_status, grievance
;



 
 insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
select d.date, state, city, incidentType, L2_category,  system_status,  'Incident Reported' as category, grievance, count(distinct incidentId) as cnt
from 
v_date_to_date d
left join
(
select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
       
       c.state,
        c.city_new as city,
        t.status as system_status,
        date(itz(report_time)) as incident_report_date, 
        report_time as report_time, 
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date,
        m.grievance as grievance,
        close_duration_hours as closed_duration_hours 
from bi_incident_mgmt m 
join v_client_tmp c 
on m.clientId = c.clientId
join  bi_patient_status t 
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time)) >= '2020-12-01' and t.status in ( 'ACTIVE', 'PENDING_ACTIVE')
)s1
on d.date = incident_report_date  
where d.date >= '2020-12-01'  
group by date, state, city, incidentType, L2_category,  system_status, grievance
 ;
 
 
 
 
  insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
 select d.date, state, city, incidentType, L2_category,  system_status,  'Incident Closed' as category, grievance,
 count(distinct incidentId) as total_cloase
from 
v_date_to_date d
left join
(
select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
        incident_Classification as incident_class,
        m.status, 
        c.state,
        c.city_new as city,
        t.status as system_status,
        date(itz(report_time)) as incident_report_date, 
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date,
        m.grievance as grievance,
        close_duration_hours   
from bi_incident_mgmt m 
join v_client_tmp c 
on m.clientId = c.clientId
join  bi_patient_status t 
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time))  >= '2020-12-01' and t.status in ( 'ACTIVE', 'PENDING_ACTIVE')  
)s1
on d.date = resolved_date  
where d.date >= '2020-12-01'  
group by date, state, city, incidentType, L2_category,  system_status, grievance
;


 
 
   insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
 select d.date, state, city, incidentType, L2_category,  system_status,  'Closed <24hrs' as category,grievance, 

 count(distinct if(close_duration_hours < 24, incidentId, null)) as close_lt_24hr
from 
v_date_to_date d
left join
(
select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
        incident_Classification as incident_class,
        m.status, 
        c.state,
        c.city_new as city,
        t.status as system_status,	
        date(itz(report_time)) as incident_report_date, 
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date,
        m.grievance as grievance,
        close_duration_hours   
from bi_incident_mgmt m
join v_client_tmp c 
on m.clientId = c.clientId 
join bi_patient_status t    
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time))  >= '2020-12-01' and t.status in ( 'ACTIVE', 'PENDING_ACTIVE') 
)s1
on d.date = resolved_date  
where d.date >= '2020-12-01'  
group by date, state, city, incidentType, L2_category,  system_status, grievance
;


 
insert into tmp_bi_incident_summary 
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
 select d.date, state, city, incidentType, L2_category,  system_status,  'Closed 24-72 hrs' as category, grievance,
 count(distinct if(close_duration_hours >= 24 and close_duration_hours < 72, incidentId, null)) as close_24_72hr
from 
v_date_to_date d
left join
(
select distinct incidentId, 
		category as incidentType, 
        subtype_L1 as L2_category, 
        incident_Classification as incident_class,
        m.status, 
        c.state,
        c.city_new as city,
        t.status as system_status,
        date(itz(report_time)) as incident_report_date, 
        if(m.status = 'OPEN',null, date(itz(resolved_time))) as resolved_date,
        m.grievance as grievance,
        close_duration_hours   
from bi_incident_mgmt m
join v_client_tmp c 
on m.clientId = c.clientId 
join bi_patient_status t    
on m.clientID = t.clientID and date(itz(report_time))  between t.status_start_date and t.status_end_date
 where date(itz(report_time))  >= '2020-12-01' and t.status in ( 'ACTIVE', 'PENDING_ACTIVE') 
)s1
on d.date = resolved_date  
where d.date >= '2020-12-01'  
group by date, state, city, incidentType, L2_category,  system_status, grievance
;





truncate table bi_incident_summary;

insert into bi_incident_summary
(eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt)
select eventdate, state, city, incidentType, L2_category, system_status, category, grievance, cnt
from tmp_bi_incident_summary
;

drop temporary table tmp_bi_incident_summary
;


update dailyprocesslog 
set updatedate = now()
where processname = 'sp_load_bi_incident_summary'
;

END$$
DELIMITER ;
