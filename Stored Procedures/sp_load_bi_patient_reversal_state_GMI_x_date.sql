CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_patient_reversal_state_GMI_x_date`(  )
BEGIN

/*
	2021-08-29 Aravindan - Modified the NA Brittle category 3 by removing Nut adherence condition
	2021-11-02 Aravindan - Added last available value, date per member as of a calendar date for BMI, sysBP_5d, diasBP_5d
*/




		update dailyProcessLog
		set startDate = now()
		where processName = 'sp_load_bi_patient_reversal_state_GMI_x_date'
		;
        







		drop temporary table if exists tmp_med_recommends;
		create temporary table tmp_med_recommends as 
							select clientid, date(itz(eventtime)) as notificationDate 
							from medicationrecommendations where medicationtierid in (
								select tiers.medicationtierid 
								from medicationtiers tiers join medicationtables ts on tiers.medicationtableid=ts.medicationtableid where tiers.tiernumber=0 
							)
		;

		create index idx_tmp_med_recommends on tmp_med_recommends (clientid asc, notificationdate asc);


		
		drop temporary table if exists tmp_last_available_cgm_5d;

		create temporary table tmp_last_available_cgm_5d as 
		select a.clientid, a.date, b.date as last_available_cgm_5d_date, b.cgm_5d as last_available_cgm_5d
		from 
		(
			select a.clientid, a.date, max(case when b.cgm_5d is not null then b.date else null end) as last_cgm_5d_date 
			from (select clientid, date from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 20 day)) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date <= a.date
			group by clientid, a.date
		) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_cgm_5d_date = b.date 
		 where a.date >= date_sub(date(itz(now())), interval 15 day)
		;

		create index idx_tmp_last_available_cgm_5d on tmp_last_available_cgm_5d (clientid asc, date asc, last_available_cgm_5d_date, last_available_cgm_5d); 


		
		
		drop temporary table if exists tmp_last_14days_CGM_avail;

		create temporary table tmp_last_14days_CGM_avail
		select a.clientid, a.date, 
		count(case when (b.cgm_1d is null and b.isOnCGMPrediction = 'No') then b.date else null end) as total_noCGM_14_days            
		from 
		bi_patient_reversal_state_gmi_x_date a inner join 
		 (
			select a.clientid, a.date, a.cgm_1d, if(pg.clientid is not null, 'Yes', 'No') as isOnCGMPrediction
			from bi_patient_reversal_state_gmi_x_date a left join predictedcgmcycles pg on a.clientid = pg.clientid and a.date between pg.startdate and pg.enddate
			where a.date >= date_sub(date(itz(now())), interval 30 day)
		) b on a.clientid = b.clientid and b.date >= date_sub(a.date, interval 13 day) and b.date <= a.date
		where a.date >= date_sub(date(itz(now())), interval 15 day)
		group by clientid, a.date
		;

		create index idx_tmp_last_14days_CGM_avail on tmp_last_14days_CGM_avail (clientid asc, date asc, total_noCGM_14_days); 







		drop temporary table if exists tmp_cgm_1d_1; 
		create temporary table tmp_cgm_1d_1 as 
		select clientid, measure_event_date, average_measure from bi_measures where measure_type = '1d' and measure_name = 'cgm'
		and measure_event_date >= date_sub(date(now()), interval 34 day) and measure_event_date <= date(now())
		;

		create index idx_tmp_cgm_1d_1 on tmp_cgm_1d_1 (clientid asc, measure_Event_Date asc, average_measure);






		drop temporary table if exists tmp_cgm_1d_2; 
		create temporary table tmp_cgm_1d_2 as select * from tmp_cgm_1d_1;

		create index idx_tmp_cgm_1d_2 on tmp_cgm_1d_2 (clientid asc, measure_Event_Date asc, average_measure);







		drop temporary table if exists tmp_cgm_14d; 
		create temporary table tmp_cgm_14d as 
		select a.clientid, a.measure_event_date, cast(avg(b.average_measure) as decimal(10,2)) as cgm_14d, 
		cast(3.31 + (0.02392*cast(avg(b.average_measure) as decimal(10,2))) as decimal(10,1)) as GMI
		from tmp_cgm_1d_1 a inner join tmp_cgm_1d_2 b on a.clientid = b.clientid and b.measure_event_date between date_sub(a.measure_event_date, interval 13 day) and a.measure_event_date
		where a.measure_event_date >= date_sub(date(now()), interval 20 day) and a.measure_event_date <= date(now())
		group by a.clientid, a.measure_event_date
		;

		create index idx_tmp_cgm_14d on tmp_cgm_14d (clientid asc, measure_Event_date asc, cgm_14d, GMI); 









		drop temporary table if exists tmp_pts_detail_reversal_medicine;

		create temporary table tmp_pts_detail_reversal_medicine as 
		select distinct a.clientid, treatmentDays, a.date, e.cohortname, h.measure as med_adh, i.average_measure as CGM_5d, s.notificationdate, m.measure as medicine_drugs, m2.measure as medicine, j.average_measure as weight_1d, k.average_measure as energy_1d,
		s4.last_available_cgm_5d, s4.last_available_cgm_5d_date, if(s1.total_noCGM_14_days = 14, 'Yes', 'No') isNoCGM_14days,
		case when m.measure is null and (s4.last_available_cgm_5d < 140 or pgc.clientid is not null) then 'Yes'
			else null end as is_InReversal,

		
			 
		case when ((s4.last_available_cgm_5d < 140 or pgc.clientid is not null) and (m.measure = 'METFORMIN' or m.measure = 'METFORMIN (DIAB)' or m.measure = 'Biguanide (DIABETES)')) then 'Yes'
			 else 'No' end as is_MetOnly_by_OPS,
		case when (m.measure = 'METFORMIN' or m.measure = 'METFORMIN (DIAB)' or m.measure = 'Biguanide (DIABETES)') then 'Yes' else null end as is_MetOnly,

		case when m.measure is not null and (m.measure <> 'METFORMIN' and m.measure <> 'METFORMIN (DIAB)' and m.measure <> 'Biguanide (DIABETES)') then 'Yes' else null end as is_MoreThanMet,
		n.cgm_14d, n.GMI
		from 
		(
			select a.date, b.clientId, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays
			from twins.v_date_to_date a
			inner join
			(
				select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
				from
				(
					select clientid, date(visitdateitz) as visitDate, min(status_start_date) as startDate, max(Status_end_date) as endDate 
					from bi_patient_status
					where status = 'active'
                    group by clientid
				) s
			) b on a.date between b.startDate and b.endDate
			where a.date >= date_sub(date(now()), interval 14 day) and a.date <= date(now())
		) a left join v_all_patient_cohort e on a.clientid = e.clientid and a.date = e.status_date
			left join bi_patient_monitor_measures h on a.clientid = h.clientid and a.date = h.measure_event_date and h.measure_name = 'MED ADH'
			left join bi_measures i on a.clientid = i.clientid and a.date = i.measure_event_date and i.measure_name = 'cgm' and i.measure_type = '5d'
			left join bi_measures j on a.clientid = j.clientid and a.date = j.measure_event_date and j.measure_name = 'weight' and j.measure_type = '1d'
			left join bi_measures k on a.clientid = k.clientid and a.date = k.measure_event_date and k.measure_name = 'energy' and k.measure_type = '1d'
			left join tmp_med_recommends s on a.clientid = s.clientid and a.date = s.notificationdate
			left join bi_patient_monitor_measures m on a.clientid = m.clientid and a.date = m.measure_event_date and m.measure_name = 'medicine_drug'
			left join bi_patient_monitor_measures m1 on a.clientid = m1.clientid and a.date = m1.measure_event_date and m1.measure_name = 'MED_ADH_NO_5d' 
			left join bi_patient_monitor_measures m2 on a.clientid = m2.clientid and a.date = m2.measure_event_date and m2.measure_name = 'medicine'
			left join tmp_cgm_14d n on a.clientid = n.clientid and a.date = n.measure_event_date 
			left join predictedcgmcycles pgc on a.clientid = pgc.clientid and a.date between pgc.startdate and pgc.enddate
			left join tmp_last_available_cgm_5d s4 on a.clientid = s4.clientid and a.date = s4.date
			left join tmp_last_14days_CGM_avail s1 on a.clientid = s1.clientid and a.date = s1.date
    ;

		create index idx_tmp_pt_reversal on tmp_pts_detail_reversal_medicine (clientid asc, date asc, is_InReversal, is_MetOnly, is_MoreThanMet, GMI, cgm_5d, weight_1d); 









		drop temporary table if exists tmp_med_count;

		create temporary table tmp_med_count as 
			select s.clientid, measure_Event_date, med_count, start_med_count, if(med_count < start_med_count, 'Yes', 'No') as is_reduced,
			start_LabA1c
			from 
			(
				select a.clientid, measure_event_date, 
				ifnull(length(measure) - length(replace(measure,',','')) + 1, 0) as med_count, 
				ifnull(length(b.start_medicine_diabetes_drugs) - length(replace(b.start_medicine_diabetes_drugs,',','')) + 1, 0) as start_med_count
				from bi_patient_monitor_measures a left join dim_client b on a.clientid = b.clientid and b.is_row_current = 'y'
				where measure_name = 'medicine_drug'
				and measure_event_date >= date_sub(date(now()), interval 15 day) and measure_event_date <= date(now())
			) s left join ( 
							select s.clientid, cast(avg(veinhba1c) as decimal(10,2)) as start_LabA1c 
							from dim_client s inner join clinictestresults c on s.clientid = c.clientid and s.firstBloodWorkDateITZ = itz(c.bloodworktime)
							group by clientid
						  ) c on s.clientid = c.clientid 
		;

		create index idx_tmp_med_count on tmp_med_count (clientid asc, measure_event_date asc, start_med_count, med_count, is_reduced, start_labA1C); 








		drop temporary table if exists tmp_recent_LabA1C_value; 
		create temporary table tmp_recent_LabA1C_value as 
		select s.clientid, s.treatmentdays, s.date, s.recentLabDate, s1.labA1C as recentLabA1C
		from 
		(
				select s1.clientid, s1.treatmentdays, s1.date, max(s2.bloodworkdate) as recentLabDate
				from bi_patient_reversal_state_GMI_x_date s1 left join 
				(
					select distinct clientid, date(itz(bloodworktime)) as bloodworkdate
					from clinictestresults 
					where bloodworktime is not null
				) s2 on s1.clientid = s2.clientid and s2.bloodworkdate <= s1.date
				group by s1.clientid, s1.date
		) s left join 
		(
					select distinct clientid, date(itz(bloodworktime)) as bloodworkdate, cast(avg(veinhba1c) as decimal(10,2)) as LabA1c 
					from clinictestresults 
					where bloodworktime is not null
					group by clientid, date(itz(bloodworktime))
		) s1 on s.clientid = s1.clientid and s.recentLabDate = s1.bloodworkdate
		where s.date >= date_sub(date(now()), interval 15 day) and s.date <= date(now())
        ;

		create index idx_tmp_recent_LabA1C_value on tmp_recent_LabA1C_value(clientid asc, date asc, recentLabDate, recentLabA1C);









		insert into bi_patient_reversal_state_GMI_x_date (clientid, treatmentdays, date, cohortname, med_adh, cgm_5d, eA1C_5dg, no_med_notificationdate, medicine_drugs, medicine,
		is_InReversal, is_MetOnly, is_MetOnly_by_OPS, is_MoreThanMet, cgm_14d, eA1C_14dg, GMI, weight_1d, energy_1d, last_available_cgm_5d, last_available_cgm_5d_date, isNoCGM_14days)
		select clientid, treatmentdays, date, cohortname, med_adh, cgm_5d, cast((cgm_5d + 46.7)/28.7 as decimal(10,2)) eA1C_5dg, 
		notificationdate, medicine_drugs, medicine, is_InReversal, is_MetOnly, is_MetOnly_by_OPS, is_moreThanMet, 
		cgm_14d, cast((cgm_14d + 46.7)/28.7 as decimal(10,2)) eA1C_14dg, GMI, weight_1d, energy_1d, last_available_cgm_5d, last_available_cgm_5d_date, isNoCGM_14days
		from tmp_pts_detail_reversal_medicine a 
		where not exists 
		(select 1 from bi_patient_reversal_state_GMI_x_date b where a.clientid = b.clientid and a.date = b.date)
		;

		update bi_patient_reversal_state_GMI_x_date a inner join tmp_pts_detail_reversal_medicine b on (a.clientid = b.clientid and a.date = b.date)
		set a.treatmentDays = b.treatmentDays,
			a.cohortname = b.cohortname, 
			a.med_adh = b.med_adh,
			a.cgm_5d = b.cgm_5d,
			a.eA1C_5dg = cast((b.cgm_5d + 46.7)/28.7 as decimal(10,2)),
			a.no_med_notificationdate = b.notificationdate,
			a.medicine_drugs = b.medicine_drugs,
            a.medicine = b.medicine,
			a.is_InReversal = b.is_InReversal,
			a.is_MetOnly = b.is_MetOnly,
			a.is_MetOnly_by_OPS = if(b.is_InReversal = 'Yes', 'No', b.is_MetOnly_by_OPS),
			a.is_moreThanMet = b.is_moreThanMet,
			a.cgm_14d = b.cgm_14d,
			a.eA1C_14dg = cast((b.cgm_14d + 46.7)/28.7 as decimal(10,2)),
			a.GMI = b.GMI,
            a.weight_1d = b.weight_1d,
            a.energy_1d = b.energy_1d,
            a.last_available_cgm_5d = b.last_available_cgm_5d,
            a.last_available_cgm_5d_date = b.last_available_cgm_5d_date,
            a.isNoCGM_14days = b.isNoCGM_14days
		;
        
		
        update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'cgm' and b.measure_type = '1d'
        set a.cgm_1d = b.average_measure
        where a.date >= date_sub(date(itz(now())), interval 10 day)
        ; 
        
        
        update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Action Score' and b.measure_type = '1d'
        set a.AS_1d = b.average_measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
        ; 
        
        
        update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Action Score' and b.measure_type = '5d'
        set a.AS_5d = b.average_measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
        ; 
        
        
        update bi_patient_reversal_state_gmi_x_date a inner join bi_patient_monitor_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'BMI'
        set a.BMI = b.measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
        ; 

        
        update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Star Rating' and b.measure_type = '1d'
        set a.coach_rating = b.average_measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
        ; 
        
        
        update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Star Rating' and b.measure_type = '5d'
        set a.coach_rating_5d = b.average_measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
        ; 
        
        
        update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'TAC' and b.measure_type = '5d'
        set a.tac_5d = b.average_measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
        ; 
        
		
		update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Ketone' and b.measure_type = '1d'
		set a.ketone_1d = b.average_measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
		;

		
		update bi_patient_reversal_state_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Ketone' and b.measure_type = '5d'
		set a.ketone_5d = b.average_measure
		where a.date >= date_sub(date(itz(now())), interval 10 day)
		;
        
        
		update bi_patient_Reversal_State_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Rest-HR' and b.measure_type = '1d'
		set a.restHR_1d = b.average_measure
		where date >= date_sub(date(itz(now())), interval 7 day)
		;

		
		update bi_patient_Reversal_State_gmi_x_date a inner join bi_patient_monitor_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'TotalMinutes' 
		set a.sleep_duration_minutes = b.measure
		where date >= date_sub(date(itz(now())), interval 7 day)
		;
 
		
		update bi_patient_Reversal_State_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Systolic' and b.measure_type = '1d'
		set a.sysBP_1d = round(b.average_measure)
		where date >= date_sub(date(itz(now())), interval 7 day)
		;
        
		
		update bi_patient_Reversal_State_gmi_x_date a inner join bi_measures b on a.clientid = b.clientid and a.date = b.measure_event_date and b.measure_name = 'Diastolic' and b.measure_type = '1d'
		set a.diasBP_1d = round(b.average_measure)
		where date >= date_sub(date(itz(now())), interval 7 day)
		;

-- maintain 60d cgm
		drop temporary table if exists tmp_60d_cgm;

		create temporary table tmp_60d_cgm
		select a.clientid, a.date, 
        if(count(distinct case when b.cgm_1d is not null then b.date else null end) >= 10, cast(avg(b.cgm_1d) as decimal(6,2)), null) as cgm_60d
		from bi_patient_reversal_state_gmi_x_date a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date between date_sub(a.date, interval 59 day) and a.date
		where a.date >= date_sub(date(itz(now())), interval 15 day)
		group by a.clientid, a.date
		;
   
		create index idx_tmp_60d_cgm on tmp_60d_cgm(clientid asc, date asc, cgm_60d);

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_60d_cgm b on 
		(a.clientid = b.clientid and a.date = b.date)
		set a.cgm_60d = b.cgm_60d
        where a.date >= date_sub(date(itz(now())), interval 10 day)
		;

		drop temporary table if exists tmp_60d_cgm
        ;
        
-- last available weight 1d        
				drop temporary table if exists tmp_last_available_weight_1d;

				create temporary table tmp_last_available_weight_1d as 
				select a.clientid, a.date, b.date as last_available_weight_1d_date, b.weight_1d as last_available_weight_1d
				from 
				(
					select a.clientid, a.date, max(case when b.weight_1d is not null then b.date else null end) as last_weight_1d_date 
					from (select clientid, date from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 15 day)) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date <= a.date
					group by clientid, a.date
				) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_weight_1d_date = b.date 
				where a.date >= date_sub(date(itz(now())), interval 10 day)
				;

				create index idx_tmp_last_available_weight_1d on tmp_last_available_weight_1d (clientid asc, date asc, last_available_weight_1d_date, last_available_weight_1d); 
				
				update bi_patient_reversal_state_gmi_x_date a inner join tmp_last_available_weight_1d b on a.clientid = b.clientid and a.date = b.date 
				set a.last_available_1d_weight_date = b.last_available_weight_1d_date
				;
				
				drop temporary table if exists tmp_last_available_weight_1d
				;
        
-- maintain last available cgm_60d
				drop temporary table if exists last_available_cgm_60d; 
						
				create temporary table last_available_cgm_60d as 
				select a.clientid, a.date, b.date as last_available_cgm_60d_date, b.cgm_60d as last_available_cgm_60d
				from 
				(
					select a.clientid, a.date, max(case when b.cgm_60d is not null then b.date else null end) as last_cgm_60d_date 
					from (select clientid, date from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 20 day)) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date <= a.date
					group by clientid, a.date
				) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_cgm_60d_date = b.date 
				;
				
				create index idx_last_available_cgm_60d on last_available_cgm_60d(clientid asc, date asc, last_available_cgm_60d_date, last_available_cgm_60d); 
				
				update bi_patient_reversal_state_gmi_x_date a inner join last_available_cgm_60d b on a.clientid = b.clientid and a.date = b.date 
				set a.last_available_cgm_60d = b.last_available_cgm_60d,
					a.last_available_cgm_60d_date = b.last_available_cgm_60d_date
				where a.date >= date_sub(date(itz(now())), interval 15 day)
				; 
				
				drop temporary table if exists last_available_cgm_60d; 



-- maintain eA1C_60d and last available eA1C_60d 

update bi_patient_reversal_state_gmi_x_date
set eA1C_60d=cast((cgm_60d + 46.7)/28.7 as decimal(10,2))
where date >= date_sub(date(itz(now())), interval 15 day)
;
 
drop temporary table if exists tmp_last_available_eA1C_60d; 
		
create temporary table tmp_last_available_eA1C_60d as 
select a.clientid, a.date, b.date as last_available_eA1C_60d_date, b.eA1C_60d as last_available_eA1C_60d
from 
(
	select a.clientid, a.date, max(case when b.eA1C_60d is not null then b.date else null end) as last_eA1C_60d_date 
	from 
    (select clientid, date from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 20 day)) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date <= a.date
	group by clientid, a.date
) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_eA1C_60d_date  = b.date 
;

create index idx_tmp_last_available_eA1C_60d on tmp_last_available_eA1C_60d(clientid asc, date asc, last_available_eA1C_60d_date, last_available_eA1C_60d); 

update bi_patient_reversal_state_gmi_x_date a inner join tmp_last_available_eA1C_60d b on a.clientid = b.clientid and a.date = b.date 
set a.last_available_eA1C_60d = b.last_available_eA1C_60d,
	a.last_available_eA1C_60d_date = b.last_available_eA1C_60d_date
where a.date >= date_sub(date(itz(now())), interval 15 day)
;


drop temporary table if exists tmp_last_available_eA1C_60d; 


-- maitain the last_available_BMI to a calendar date    
		drop temporary table if exists tmp_last_available_BMI;

		create temporary table tmp_last_available_BMI as 
		select a.clientid, a.date, b.date as last_available_BMI_date, b.BMI as last_available_BMI
		from 
		(
			select a.clientid, a.date, max(case when b.BMI is not null then b.date else null end) as last_BMI_date 
			from (select clientid, date from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 20 day)) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date <= a.date
			group by clientid, a.date
		) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_BMI_date = b.date 
		;
	

		create index idx_tmp_last_available_BMI on tmp_last_available_BMI (clientid asc, date asc, last_available_BMI_date, last_available_BMI); 
        
        update bi_patient_reversal_state_gmi_x_date a inner join tmp_last_available_BMI b on a.clientid=b.clientid and a.date=b.date
        set 
        a.last_available_BMI=b.last_available_BMI,
        a.last_available_BMI_date=b.last_available_BMI_date
        where a.date >= date_sub(date(itz(now())), interval 15 day)
        ;
       
		drop temporary table if exists tmp_last_available_BMI;
        
         
-- maitain the last_available_sysBP_5d to a calendar date
        drop temporary table if exists tmp_last_available_sysBP_5d;

		create temporary table tmp_last_available_sysBP_5d as 
		select a.clientid, a.date, b.date as last_available_sysBP_5d_date, b.sysBP_5d as last_available_sysBP_5d
		from 
		(
			select a.clientid, a.date, max(case when b.sysBP_5d is not null then b.date else null end) as last_sysBP_5d_date 
			from (select clientid, date from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 20 day)) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date <= a.date
			group by clientid, a.date
		) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_sysBP_5d_date = b.date 
		;

		create index idx_tmp_last_available_sysBP_5d on tmp_last_available_sysBP_5d (clientid asc, date asc, last_available_sysBP_5d_date, last_available_sysBP_5d); 
        
	
		update bi_patient_reversal_state_gmi_x_date a inner join tmp_last_available_sysBP_5d b on a.clientid=b.clientid and a.date = b.date
        set 
        a.last_available_sysBP_5d=b.last_available_sysBP_5d,
        a.last_available_sysBP_5d_date=b.last_available_sysBP_5d_date
       	where a.date >= date_sub(date(itz(now())), interval 15 day)
        ;
       
		drop temporary table if exists tmp_last_available_sysBP_5d;

    -- maitain the last_available_diasBP_5d to a calendar date
    	drop temporary table if exists tmp_last_available_diasBP_5d;

		create temporary table tmp_last_available_diasBP_5d as 
		select a.clientid, a.date, b.date as last_available_diasBP_5d_date, b.diasBP_5d as last_available_diasBP_5d
		from 
		(
			select a.clientid, a.date, max(case when b.diasBP_5d is not null then b.date else null end) as last_diasBP_5d_date 
			from (select clientid, date from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 20 day)) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date <= a.date
			group by clientid, a.date
		) a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and a.last_diasBP_5d_date = b.date 
		-- where 
		;

		create index idx_tmp_last_available_diasBP_5d on tmp_last_available_diasBP_5d (clientid asc, date asc, last_available_diasBP_5d_date, last_available_diasBP_5d); 
        
        update bi_patient_reversal_state_gmi_x_date a inner join tmp_last_available_diasBP_5d b on a.clientid=b.clientid and a.date=b.date
        set 
        a.last_available_diasBP_5d=b.last_available_diasBP_5d,
		a.last_available_diasBP_5d_date=b.last_available_diasBP_5d_date
        where a.date >= date_sub(date(itz(now())), interval 15 day)
        ;
       
		drop temporary table if exists tmp_last_available_diasBP_5d;
 
-- Store the recent lab on a calandar date with triglycerides and hdlCholesterol values present but the lab shouldn't be the first lab. So for a date, in the GMI table if column last_available_LabTg_Hdl_date is blank that means the member never had lab or only had 1st lab.
		drop temporary table if exists tmp_recent_lab_with_tg_hdl_value; 
	
		create temporary table tmp_recent_lab_with_tg_hdl_value as 
		select s.clientid, s.date, if(s.recentLabDate = s2.firstBloodWork, null, s.recentLabDate) as recentLabDate -- , s1.LabTg as recentLabTg,s1.LabHdl as recentLabHdl
		from 
		(
				select s1.clientid, s1.date, max(s2.bloodworkdate) as recentLabDate
				from bi_patient_reversal_state_GMI_x_date s1 left join 
				(
					select distinct clientid, date(itz(ifnull(bloodworktime, eventtime))) as bloodworkdate
					from clinictestresults 
					where triglycerides is not null and hdlCholesterol is not null
					AND deleted = 0
				) s2 on s1.clientid = s2.clientid and s2.bloodworkdate <= s1.date 
				group by s1.clientid, s1.date
		) s left join 
		(
					select distinct clientid, date(itz(ifnull(bloodworktime, eventtime))) as bloodworkdate
					from clinictestresults 
					where bloodworktime is not null and triglycerides is not null and hdlCholesterol is not NULL
					AND deleted = 0
		) s1 on s.clientid = s1.clientid and s.recentLabDate = s1.bloodworkdate
		left join 
		(
					select distinct a.clientid, min(date(itz(ifnull(bloodworktime, eventtime)))) as firstBloodWork
					from clinictestresults a 
					where bloodworktime is not null and triglycerides is not null and hdlCholesterol is not NULL
					AND deleted = 0
					group by a.clientid
		) s2 on s.clientid = s2.clientid
		where s.date >= date_sub(date(now()), interval 15 day) and s.date <= date(now())		
		;

		create index idx_tmp_recent_lab_with_tg_hdl_value on tmp_recent_lab_with_tg_hdl_value(clientid asc, date asc, recentLabDate);


        update bi_patient_reversal_state_gmi_x_date a inner join tmp_recent_lab_with_tg_hdl_value b on a.clientid=b.clientid and a.date=b.date  
        set a.last_available_LabTg_Hdl_date=b.recentLabDate
	    ;
     
       
        drop temporary table if exists tmp_recent_lab_with_tg_hdl_value; 
       
        
 -- Store the recent lab on a calandar date with alt and ast values present but the lab shouldn't be the first lab. So for a date, in the GMI table if column last_available_LabAst_Alt_date is blank that means the member never had lab or only had 1st lab.
        drop temporary table if exists tmp_recent_lab_with_alt_ast_value; 
       
		create temporary table tmp_recent_lab_with_alt_ast_value as 
		select s.clientid, s.date, if(s.recentLabDate = s2.firstBloodWork, null, s.recentLabDate) as recentLabDate
		from 
		(
				select s1.clientid, s1.date, max(s2.bloodworkdate) as recentLabDate
				from bi_patient_reversal_state_GMI_x_date s1 left join 
				(
					select distinct clientid, date(itz(ifnull(bloodworktime, eventtime))) as bloodworkdate
					from clinictestresults 
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
				) s2 on s1.clientid = s2.clientid and s2.bloodworkdate <= s1.date 
				group by s1.clientid, s1.date
		) s left join 
		(
					select distinct clientid, date(itz(ifnull(bloodworktime, eventtime))) as bloodworkdate
					from clinictestresults 
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
		) s1 on s.clientid = s1.clientid and s.recentLabDate = s1.bloodworkdate
		left join 
		(
					select distinct a.clientid, min(date(itz(ifnull(bloodworktime, eventtime)))) as firstBloodWork
					from clinictestresults a 
					where alt is not null and ast is not null and insulin is not null and glucose is not null
					and deleted = 0
					group by a.clientid
		) s2 on s.clientid = s2.clientid
		where s.date >= date_sub(date(now()), interval 15 day) and s.date <= date(now())
        ;

		create index idx_tmp_recent_lab_with_alt_ast_value on tmp_recent_lab_with_alt_ast_value(clientid asc, date asc, recentLabDate);

	    update bi_patient_reversal_state_gmi_x_date a inner join tmp_recent_lab_with_alt_ast_value b on a.clientid=b.clientid and a.date=b.date  
	    set a.last_available_LabAst_Alt_date=b.recentLabDate
		;

		drop temporary table if exists tmp_recent_lab_with_alt_ast_value; 
	
		drop temporary table if exists tmp_all_MH_symptoms; 

		create temporary table tmp_all_MH_symptoms as 
			select a.clientid, date(ifnull(a.eventLocalTime, itz(a.eventTime))) as eventDate, b.type
			from selfreportanswers a inner join selfreportquestions b on a.selfreportquestionId = b.id
			where b.answerType = 'HML'
			and a.value > 1 
			and date(ifnull(a.eventLocalTime, itz(a.eventTime))) >= date_sub(date(now()), interval 12 day)
		;

		create index idx_tmp_all_MH_symptoms on tmp_all_MH_symptoms(clientid asc, eventdate asc, type); 

		drop temporary table if exists tmp_all_MH_symptoms_to_LOAD; 

		create temporary table tmp_all_MH_symptoms_to_LOAD as 
		select cd.clientid, cd.date as measure_event_date, cast(group_concat(distinct type) as char(500)) as measure 
		from 
			(
				select d.date, c.clientId 
				from twins.v_date_to_date d
				inner join bi_patient_status c on d.date between c.status_start_date and c.status_end_date and c.status= 'active'
				where d.date >= date_sub(date(now()), interval 10 day)
			) cd left join tmp_all_MH_symptoms b on cd.clientid = b.clientid and cd.date = b.eventdate 
		group by cd.clientid, cd.date
		;

		create index idx_tmp_all_MH_symptoms_to_LOAD on tmp_all_MH_symptoms_to_LOAD(clientid asc, measure_event_date asc, measure); 

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_all_MH_symptoms_to_LOAD b on a.clientid = b.clientid and a.date = b.measure_event_date
		set a.MH_Symptoms = b.measure
		;

		drop temporary table if exists tmp_all_MH_symptoms; 
		drop temporary table if exists tmp_all_MH_symptoms_to_LOAD; 
        
        
        
		drop temporary table if exists tmp_medicine_fullform; 

		create temporary table tmp_medicine_fullform
		(
		clientid int not null,
		measure_event_date date not null,
		measure varchar(1000) null
		); 

		insert into tmp_medicine_fullform (clientid, measure_event_date, measure) 
		select cd.clientid, cd.date as measure_event_date, measurevalue as measure
		from 
		(
				select d.date, c.clientId
				from twins.v_date_to_date d
				inner join bi_patient_status c 
				where d.date between c.status_start_date and c.status_end_date and c.status = 'active'
				and date >= date_sub(date(now()), interval 10 day)
		) cd left join
		(    
			select clientid, eventdate, group_concat(medicine) as measurevalue
			from 
			(
				
				select clientid, eventdate, group_concat(distinct medicine order by medicine) as medicine
				from 
				(
						select distinct a.clientid, date(ifnull(scheduledLocalTime, itz(scheduledtime))) as eventdate, b.medicinename as medicine
						from clienttodoitems a left join medicines b on a.medicineid = b.medicineid 
						where a.category = 'medicine' and type='medicine' and a.status in ('FINISHED','UNFINISHED')
						and scheduledTime >= date_sub(now(), interval 15 day) and scheduledTime <= now()
				) s 
				group by clientid, eventdate

				union all 

				
				select clientId, eventdate, group_concat(distinct medicine order by medicine) as medicine
				from 
				(
						select distinct a.clientId, date(ifnull(scheduledLocalTime, itz(scheduledtime))) as eventdate, ifnull(b.medicinename, type) as medicine
						from clienttodoitems a left join medicines b on a.medicineid = b.medicineid 
						where a.category = 'medicine' and type not in ('medicine') and a.status in ('FINISHED','UNFINISHED')
						and scheduledTime >= date_sub(now(), interval 15 day) and scheduledTime <= now()
				) s  
				group by clientId, eventdate
			) s 
			group by clientid, eventdate
		) s on cd.clientid = s.clientid and cd.date = s.eventdate
		;

        create index idx_tmp_medicine_fullform on tmp_medicine_fullform (clientid asc, measure_event_date asc, measure); 

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_medicine_fullform b on a.clientid = b.clientid and a.date = b.measure_event_date 
		set a.medicine_nameUntouched = b.measure
		;

		drop temporary table if exists tmp_medicine_fullform; 
     
       
        
        
		drop temporary table if exists tmp_5d_values_calculator;

		create temporary table tmp_5d_values_calculator
		select a.clientid, a.date, a.restHR_1d, a.weight_1d, a.sleep_duration_minutes, 
        cast(avg(b.restHR_1d) as decimal(6,2)) as restHR_5d, cast(avg(b.weight_1d) as decimal(6,2)) as weight_5d, cast(avg(b.sleep_duration_minutes) as decimal(6,2)) as sleep_duration_minutes_5d,
        cast(avg(b.sysBP_1d) as decimal(6,0)) as sysBP_5d, cast(avg(b.diasBP_1d) as decimal(6,0)) as diasBP_5d
		from bi_patient_reversal_state_gmi_x_date a inner join bi_patient_reversal_state_gmi_x_date b on a.clientid = b.clientid and b.date between date_sub(a.date, interval 4 day) and a.date
		where a.date >= date_sub(date(now()), interval 15 day)
		group by a.clientid, a.date
		;

		create index idx_tmp_5d_values_calculator on tmp_5d_values_calculator(clientid asc, date asc, weight_5d, restHR_5d, sleep_duration_minutes_5d, sysBP_5d, diasBP_5d);


		update bi_patient_reversal_state_gmi_x_date a inner join tmp_5d_values_calculator b on 
		(a.clientid = b.clientid and a.date = b.date)
		set a.restHR_5d = b.restHR_5d,
			a.weight_5d = b.weight_5d,
            a.sleep_duration_minutes_5d = b.sleep_duration_minutes_5d,
            a.sysBP_5d = b.sysBP_5d,
            a.diasBP_5d = b.diasBP_5d
        where a.date >= date_sub(date(now()), interval 10 day)
		;

		drop temporary table if exists tmp_5d_values_calculator
        ;
        

		drop temporary table if exists tmp_total_mealtypes_per_day; 

		create temporary table tmp_total_mealtypes_per_day as 
		select clientid, eventdateitz as mealdate, count(distinct case when categoryType in ('breakfast','lunch','dinner') then categoryType else null end) total_mealtypes 
		from bi_food_supplement_log_detail  
		where category = 'foodlog' and eventdateitz >= date_sub(date(itz(now())), interval 30 day)
		group by clientid, eventdateitz
		;

		create index idx_tmp_total_mealtypes_per_day on tmp_total_mealtypes_per_day(clientid asc, mealdate asc, total_mealtypes); 

		drop temporary table if exists tmp_all_treatdays_cgm_cohort;

		create temporary table tmp_all_treatdays_cgm_cohort as 
		select date, clientid, treatmentdays, cohortname, cgm_5d, medicine_drugs, is_InReversal, is_MetOnly_by_OPS
		from bi_patient_reversal_state_gmi_x_date 
		where date >= date_sub(date(itz(now())), interval 30 day) 
		;

		create index idx_tmp_all_treatdays_cgm_cohort on tmp_all_treatdays_cgm_cohort(clientid asc, date asc, treatmentdays, cohortname, cgm_5d); 

		drop temporary table if exists tmp_CBG_maintenance; 

		create temporary table tmp_CBG_maintenance as 
		select a.clientid, a.date, a.treatmentdays, b.total_good_days, total_foodlog_available_days, b.isCBG from bi_patient_reversal_state_gmi_x_date a inner join 
		(	
			select distinct s.clientid, s.date, s.total_good_days, s.total_foodlog_available_days, if(s1.clientid is not null, 'Y', 'N') as isCBG
			from 
			(
				select b.clientid, a.date
				,count(distinct case when is_InReversal = 'Yes' or (is_MetOnly_by_ops = 'Yes' and is_InReversal is null) then b.date else null end) as total_good_days
                ,count(distinct case when c.total_mealtypes > 0 then c.mealdate else null end) as total_foodlog_available_days
				from v_date_to_date a left join tmp_all_treatdays_cgm_cohort b on b.date between date_sub(a.date, interval 7 day) and date_sub(a.date, interval 1 day)
									  left join tmp_total_mealtypes_per_day c on b.clientid = c.clientid and c.mealdate between date_sub(a.date, interval 13 day) and a.date
				where a.date >= date_sub(date(itz(now())), interval 14 day)
				and b.treatmentdays > 11
				group by b.clientid, a.date
			) s left join predictedCGMCycles s1 on s.clientid = s1.clientid and s.date between s1.startdate and s1.enddate
		) b on a.clientid = b.clientid and a.date = b.date 
		where a.treatmentdays > 11 
		;

		create index idx_tmp_CBG_maintenance on tmp_CBG_maintenance(clientid asc, date asc, isCBG, total_good_days, total_foodlog_available_days); 

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_CBG_maintenance b on (a.clientid = b.clientid and a.date = b.date)
		set a.isCBG = b.isCBG,
			a.isCBGEligible = case when b.total_good_days = 7 and b.total_foodlog_available_days >= 10 then 'Y' else 'N' end,
			a.total_available_foodlog_days = b.total_foodlog_available_days,
            a.total_good_reversal_days = b.total_good_days
		; 

		drop temporary table if exists tmp_CBG_maintenance; 
        
        
		
		drop temporary table if exists tmp_1d_predicted_cgm;

		create temporary table tmp_1d_predicted_cgm	as 
		select e.clientid, cast(score as decimal(7,2)) as predicted_1d_cgm, date(itz(eventtime)) as eventdate from 
		scores e inner join predictedcgmcycles pc on e.clientid = pc.clientid and date(itz(eventtime)) between pc.startdate and pc.enddate
		where e.scoreType = 'CGM' and e.period = 'Daily' 
		and e.eventtime >= date_sub(itz(now()), interval 14 day)
		;

		create index idx_tmp_1d_predicted_cgm on tmp_1d_predicted_cgm(clientid asc, eventdate asc, predicted_1d_cgm)
		;

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_1d_predicted_cgm b on a.clientid = b.clientid and a.date = b.eventdate
		set a.predicted_1d_cgm = b.predicted_1d_cgm
		where date >= date_sub(itz(now()), interval 10 day)
        ;

		drop temporary table if exists tmp_1d_predicted_cgm;





		update bi_patient_reversal_state_GMI_x_date a inner join tmp_med_count b on (a.clientid = b.clientid and a.date = b.measure_event_date)
		set a.actual_medCount = b.med_count,
			a.start_medcount = b.start_med_count, 
			a.is_MedReduced = b.is_reduced,
			a.start_LabA1c = b.start_LabA1c
		; 





		update bi_patient_reversal_state_GMI_x_date a inner join tmp_recent_LabA1C_value b on (a.clientid = b.clientid and a.date = b.date)
		set a.recentLabDate = b.recentLabDate,
			a.recentLabA1c = b.recentLabA1C
		; 








			drop temporary table if exists tmp_nondiabetic_drugs; 

			create temporary table tmp_nondiabetic_drugs  
			(
			clientid int not null,
			date date not null, 
			HTN_drugs varchar(500) null, 
			CHOL_drugs varchar(500) null,
			HD_drugs varchar(500) null
			); 

			insert into tmp_nondiabetic_drugs (clientid, date, HTN_drugs, CHOL_drugs, HD_drugs) 
			select s1.clientid, s1.date, 
			/*
			group_concat(distinct case when conditionName like '%HYPERTENSION%' then epc else null end) as HTN_drugs,   
			group_concat(distinct case when (conditionName like '%CHOLESTEROL%' or conditionName like '%Dyslipidemia%')  then epc else null end) as CHOL_drugs,
			group_concat(distinct case when conditionName like '%HEARTDISEASE%' then epc else null end) as HD_drugs
			*/
			
			-- Old Drugs method
			group_concat(distinct case when subcategory = 'HYPERTENSION' then drugname else null end) as HTN_drugs,   
			group_concat(distinct case when subcategory = 'CHOLESTEROL' then drugname else null end) as CHOL_drugs,
			group_concat(distinct case when subcategory = 'HEARTDISEASE' then drugname else null end) as HD_drugs
			from 
			(
						select a.date, b.clientId, b.startdate, (datediff(a.date, b.startDate) + 1) as treatmentDays
						from twins.v_date_to_date a
						inner join
						(
							select clientid, if(visitDate > startDate or startDate is null, visitDate, startDate) as startDate, endDate
							from
							(
								select clientid, date(visitdateitz) as visitDate, max(status_start_date) as startDate, max(Status_end_date) as endDate
								from bi_patient_status
								where status = 'active'
								group by clientid, date(visitdateitz)
							) s
						) b on a.date between b.startDate and b.endDate
                        where a.date >= date_sub(date(now()), interval 50 day)
			) s1 left join 
			(
			    select distinct clientid, subcategory, drugname, min(startdate) as startdate, max(enddate) as enddate 
				-- select distinct clientid, conditionName, epc, min(startdate) as startdate, max(enddate) as enddate 
				from 
				(
				select distinct P.clientId, pm.medicineid, subcategory, c.medicineName, e.drugname,
				-- select distinct P.clientId, pm.medicineid, c.medicineName, pc.epc, pc.conditionName, 
				(date(itz(pm.startLocaldate))) as startdate, (date(itz(pm.endLocaldate))) as enddate 
				from prescriptions P join prescriptionmedicines PM on P.prescriptionid = PM.prescriptionid
									left join medicines c on pm.medicineid = c.medicineid
									left join medicinedrugs d on pm.medicineid = d.medicineid 
									left join drugs e on d.drugid = e.drugid
									left join drugclasses f on e.drugclassid = f.drugclassid 
									-- left join medicinepharmclasses mpc on c.medicineID = mpc.medicineId 
									-- left join pharmclasses pc on mpc.pharmClassId = pc.pharmClassId 
									inner join v_client_tmp ac on p.clientid = ac.clientid
				where 
				(pm.startLocaldate is not null and pm.endLocalDate is not null)
				) s 
				group by clientid, subcategory, drugname
				-- group by clientid, conditionName, epc
			) s2 on s1.clientid = s2.clientid and (s1.date between s2.startdate and s2.enddate)
			group by s1.clientid, s1.date;

			create index idx_tmp_nondiabetic_drugs on tmp_nondiabetic_drugs (clientid asc, date asc, HTN_drugs, CHOL_drugs, HD_drugs); 

			update bi_patient_reversal_state_GMI_x_date a inner join tmp_nondiabetic_drugs b on (a.clientid = b.clientid and a.date = b.date) 
			set 
			a.nonDiabetic_medicine_drugs_HTN = b.HTN_drugs, 
			a.nonDiabetic_medicine_drugs_CHOL = b.CHOL_drugs,
			a.nonDiabetic_medicine_drugs_HD = b.HD_drugs
			;

			drop temporary table if exists tmp_nondiabetic_drugs; 
            
            






		drop temporary table if exists tmp_consecutive_threshold_counts;
	
		create temporary table tmp_consecutive_threshold_counts
		(
			prev_record int null,
			prev_date date null,
			prev_record_GMI decimal(10,2) null,
			prev_inReversal varchar(5) null,
			prev_MetOnly varchar(5) null,
			prev_eA1C_5dg decimal(10,2) null,
			prev_eA1C_14dg decimal(10,2) null,
			prev_med_drug varchar(500) null,
			
			curr_record int null,
			curr_date date null,
			curr_record_GMI decimal(10,2) null,
			curr_record_eA1C_5dg decimal(10,2) null,
			curr_record_eA1C_14dg decimal(10,2) null,
			curr_InReversal varchar(5) null,
			curr_metOnly varchar(5) null,
			curr_med_drug varchar(500) null,
			
			count_med int null,
			count_GMI int null,
			count_eA1C_5dg int null,
			count_eA1C_14dg int null
			
		);

		insert into tmp_consecutive_threshold_counts 
		select 	@prev_record := @clientid as prev_record, 
				@prev_date := cast(@rdate as date) as prev_date, 
				@prev_record_GMI := cast(@GMI as decimal(10,2)) as prev_record_GMI,
				@prev_inReversal := @is_InReversal as prev_inReversal,
				@prev_metOnly := @is_MetOnly as prev_MetOnly,
				@prev_eA1C_5dg := cast(@eA1C_5dg as decimal(10,2)) as prev_eA1C_5dg, 
				@prev_eA1C_14dg := cast(@eA1C_14dg as decimal(10,2)) as prev_eA1C_14dg,   
				@prev_med_drug := @med_drugs as prev_med_drug,
				
				@clientid := a.clientid as curr_record, 
				@rdate := cast(a.date as date) as curr_date, 
				@GMI := cast(a.GMI as decimal(10,2)) as curr_record_GMI,
				@eA1C_5dg := cast(a.eA1C_5dg as decimal(10,2)) as curr_record_eA1C_5dg, 
				@eA1C_14dg := cast(a.eA1C_14dg as decimal(10,2)) as curr_record_eA1C_14dg, 
				@is_InReversal := a.is_InReversal as curr_InReversal,
				@is_MetOnly := a.is_MetOnly as curr_metOnly,
				@med_drugs := a.medicine_drugs as curr_med_drug,
				
				
				
				
				
				@count_med := if(@prev_record = @clientid && (@prev_med_drug is null || @prev_metOnly = 'Yes'), @count_med + 1, 0) as count_med,
				@count_GMI := if(@prev_record = @clientid && @prev_record_GMI < 6.5 && (@prev_med_drug is null || @prev_metOnly = 'Yes'), @count_GMI + 1, 0) as count_GMI,
				@count_eA1C_5dg := if(@prev_record = @clientid && @prev_eA1C_5dg < 6.5 && (@prev_med_drug is null || @prev_metOnly = 'Yes'), @count_eA1C_5dg + 1, 0) as count_eA1C_5dg,
				@count_eA1C_14dg := if(@prev_record = @clientid && @prev_eA1C_14dg < 6.5 && (@prev_med_drug is null || @prev_metOnly = 'Yes'), @count_eA1C_14dg + 1, 0) as count_eA1C_14dg
				
		from bi_patient_reversal_state_GMI_x_date a 
		cross join (
						select 	@count_GMI := 0, @count_eA1C_5dg := 0, @count_eA1C_14dg := 0, @count_med := 0, 
								@clientid := 0, @rdate := '1900-01-01', @GMI := 0.0, @is_InReversal := '', @is_MetOnly := '', @eA1C_5dg := 0.0, @eA1C_14dg := 0.0,  @med_drugs := '', 
								@prev_record := 0, @prev_date := '1900-01-01', @prev_record_GMI := 0.0, @prev_inReversal := '', @prev_metOnly := '', @prev_eA1C_5dg := 0.0, @prev_eA1C_14dg := 0.0, @prev_med_drug := '' 
				   ) c
		where a.date >= date_sub(date(now()), interval 20 day)
		order by a.clientid, a.date
		;
	
		create index idx_tmp_consecutive_threshold_count on tmp_consecutive_threshold_counts (curr_record asc, curr_date asc, count_med, count_GMI, count_eA1C_5dg, count_eA1C_14dg);






		update bi_patient_reversal_state_GMI_x_date a inner join tmp_consecutive_threshold_counts b on a.clientid = b.curr_record and a.date = b.curr_date
		set a.consecutive_Med_satisfied_days = b.count_med,
			a.consecutive_GMI_threshold_days = b.count_GMI,
			a.consecutive_eA1C_5dg_threshold_days = b.count_eA1C_5dg,
			a.consecutive_eA1C_14dg_threshold_days = b.count_eA1C_14dg
		;


		drop temporary table if exists tmp_consecutive_threshold_counts;





	drop temporary table if exists tmp_nutrition_profiles_aud; 

    create temporary table tmp_nutrition_profiles_aud as 
    select s1.clientid, s1.syntaxname, s1.greenfoods as greenfoods, s1.pstarFoods as pstarFoods, s1.portionControlledRedFoods as portionControlledRedFoods, 
				s1.breakfast as breakfast, s1.lunch as lunch, s1.dinner as dinner, s1.timerestrictedEating as timerestrictedEating,  s1.currentManualSyntax, 
				s1.date as start_date, 
				ifnull(date_sub(s2.date, interval 1 day), date(itz(now()))) as end_date 
				from 
				(
					select @rnum := case when s1.clientid = @clientid then @rnum + 1 else 1 end as rownum, 
					@clientid := s1.clientid as clientid, s1.syntaxname, s1.greenfoods, s1.pstarFoods, s1.portionControlledRedFoods, s1.breakfast, s1.lunch, s1.dinner, s1.timerestrictedEating, s1.currentManualSyntax, date
					from 
					(	
							select a.clientid,ns.syntaxname, b.greenfoods, b.pstarFoods, b.portionControlledRedFoods, b.breakfast, b.lunch, b.dinner, b.timerestrictedEating, b.currentManualSyntax, date(itz(a.recentModified)) as date from 
							(
								select clientid, date(datemodified) as date, max(datemodified) as recentModified from nutritionprofiles_aud group by clientid, date(datemodified) 
							) a inner join nutritionprofiles_aud b on a.clientid = b.clientid and a.recentModified = b.datemodified
								inner join nutritionsyntax ns on b.nutritionSyntaxId = ns.nutritionSyntaxId
								inner join v_client_tmp c on a.clientid = c.clientid 
							
							union 
							   
							select a.clientid, ns.syntaxname, a.greenfoods, a.pstarFoods, a.portionControlledRedFoods, a.breakfast, a.lunch, a.dinner, a.timerestrictedEating, a.currentManualSyntax, date(itz(a.datemodified)) as date from 
							nutritionprofiles a inner join nutritionsyntax ns on a.nutritionSyntaxId = ns.nutritionSyntaxId
												inner join v_client_tmp b on a.clientid = b.clientid 
					 ) s1 cross join (select @rnum := 0, @clientid := '') s2 
					 order by s1.clientid, s1.date
				) s1 left join 
				(
					select @snum := case when s1.clientid = @clientid then @snum + 1 else 1 end as rownum, 
					@clientid := s1.clientid as clientid, s1.syntaxname, s1.greenfoods, s1.pstarFoods, s1.portionControlledRedFoods, s1.breakfast, s1.lunch, s1.dinner, s1.timerestrictedEating, s1.currentManualSyntax, date
					from 
					(	
							select a.clientid, ns.syntaxname, b.greenfoods, b.pstarFoods, b.portionControlledRedFoods, b.breakfast, b.lunch, b.dinner, b.timerestrictedEating, b.currentManualSyntax, date(itz(a.recentModified)) as date from 
							(
								select clientid, date(datemodified) as date, max(datemodified) as recentModified from nutritionprofiles_aud group by clientid, date(datemodified) 
							) a inner join nutritionprofiles_aud b on a.clientid = b.clientid and a.recentModified = b.datemodified
								inner join nutritionsyntax ns on b.nutritionSyntaxId = ns.nutritionSyntaxId
								inner join v_client_tmp c on a.clientid = c.clientid 
							
							union 
							   
							select a.clientid, ns.syntaxname, a.greenfoods, a.pstarFoods, a.portionControlledRedFoods, a.breakfast, a.lunch, a.dinner, a.timerestrictedEating, a.currentManualSyntax, date(itz(a.datemodified)) as date from 
							nutritionprofiles a inner join nutritionsyntax ns on a.nutritionSyntaxId = ns.nutritionSyntaxId
												inner join v_client_tmp b on a.clientid = b.clientid 
					 ) s1 cross join (select @snum := 0, @clientid := '') s2 
					 order by s1.clientid, s1.date
				) s2 on s1.clientid = s2.clientid and s2.rownum = s1.rownum + 1 
		; 

		create index idx_tmp_nutrition_profiles_aud on tmp_nutrition_profiles_aud(clientid asc, start_date, end_date, timerestrictedEating);








		drop temporary table if exists tmp_nutrition_syntax_prediction;

		create temporary table tmp_nutrition_syntax_prediction as 
				select s.clientid, date  as mealdate, current_nut_syntax, 
				case when journey = 'J1' and current_nut_syntax in ('1','2','3','4') THEN 'Green'
					 when journey = 'J1' and current_nut_syntax in ('5','6','7') THEN 'Orange'
					 when journey = 'J1' and current_nut_syntax in ('8','9','10','11','12','13','14') THEN 'Red'
					 
					 when journey = 'J2' and current_nut_syntax in ('1','2','3','4','5','6','7') then 'Green'
					 when journey = 'J2' and current_nut_syntax in ('8','9','10') then 'Orange'
					 when journey = 'J2' and current_nut_syntax in ('11','12','13','14') THEN 'Red'
					 
					 when journey = 'J3' and current_nut_syntax in ('1','2','3','4','5','6','7','8','9','10') then 'Green'
					 when journey = 'J3' and current_nut_syntax in ('11','12') then 'Orange'
					 when journey = 'J3' and current_nut_syntax in ('13','14') THEN 'Red'
					 
					 when journey = 'J4' and current_nut_syntax in ('1','2','3','4','5','6','7','8','9','10','11','12','13','14') then 'Green' end as color
				from 
				(
							select s1.clientid, s1.date, 
							ifnull(s1.journey,'J1') as journey, 
							ifnull(s1.green_syntax,'1-4') as green_syntax, ifnull(s1.recent_consecutive_reversaldays,'') as recent_consecutive_reversaldays, 
							ifnull(s1.orange_syntax,'5-7') as orange_syntax, 
							ifnull(s1.red_syntax,'8-14') as red_syntax, 

							ifnull(s2.current_nut_syntax,'NA') as current_nut_syntax, 
							ifnull(currentManualSyntax,'') as currentManualSyntax 
							from 
							(
								select a.clientid, 	a.date, journey, nut_syntax as green_syntax, recent_consecutive_reversaldays,
								case when journey = 'J1' then '5-7' 
									 when journey = 'J2' then '8-10' 
									 when journey = 'J3' then '11-12' 
											else null end as orange_syntax,  
								case when journey = 'J1' then '8-14' 
									 when journey = 'J2' then '11-14' 
									 when journey = 'J3' then '13-14' 
											else null end as red_syntax
								from (	
											select date, b.clientid from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
											where date >= date_sub(date(itz(now())), interval 25 day) 
									 ) a left join dim_client b on a.clientid = b.clientid and b.is_row_current = 'Y'
							) s1 left join 
							( 
							select s.clientid, s.mealdate, greenfood, redFood, pstarFood, B_eggFood, B_totalFood, 
							B_pS1food, L_pS1food, D_pS1food, S_pS1food, B_pS2food, L_pS2food, D_pS2food, S_pS2food, B_PCRfood, L_PCRfood, D_PCRfood, S_PCRfood, 
							is_on_mealSkip, is_on_timeRestrictionEating, currentManualSyntax, 
							case when greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 0 then 1 
								 when greenfood > 0 and is_on_timeRestrictionEating = 1 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 then 2
								 when greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 and (B_eggFood = B_totalFood = 1) then 3
								 when greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 0 then 4
								 
								 when ((greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 1) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 5
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 and (B_eggFood = B_totalFood = 1)) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 6
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 ) and (B_pS1food+L_pS1food+D_pS1food+S_pS1food) = 1) then 7

								 when ((greenfood > 0 and is_on_mealskip = 1 and redFood = 0 and pstarFood = 1) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 8
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1 and (B_eggFood = B_totalFood = 1)) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 9
								 when ((greenfood > 0 and is_on_mealskip = 0 and redFood = 0 and pstarFood = 1) and (B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1) then 10
								 
								 when ((greenfood > 0 and is_on_mealskip = 1 and (redFood = 1 or pstarFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 11
								 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood = 1 or pstarFood = 1) and (B_eggFood = B_totalFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 12
								 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood = 1 or pstarFood = 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) = 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) = 1)) then 13
								 when ((greenfood > 0 and is_on_mealskip = 0 and (redFood > 1 or pstarFood > 1)) and ((B_pS2food+L_pS2food+D_pS2food+S_pS2food) >= 1 or (B_PCRfood+L_PCRfood+D_PCRfood+S_PCRfood) >= 1)) then 14

								 else 'UK'
								 end as current_nut_syntax
							from 
							(
								select s1.clientid, s1.eventdateitz as mealdate, 
								ifnull(B_pS1food,0) as B_pS1food, ifnull(L_pS1food,0) as L_pS1food, ifnull(D_pS1food,0) as D_pS1food, ifnull(S_pS1food,0) as S_pS1food, 
								ifnull(B_pS2food,0) as B_pS2food, ifnull(L_pS2food,0) as L_pS2food, ifnull(D_pS2food,0) as D_pS2food, ifnull(S_pS2food,0) as S_pS2food, 
								ifnull(B_PCRfood,0) as B_PCRfood, ifnull(L_PCRfood,0) as L_PCRfood, ifnull(D_PCRfood,0) as D_PCRfood, ifnull(S_PCRfood,0) as S_PCRfood,
								if(s2.breakfast = 0 or s2.dinner = 0, 1, 0) as is_on_mealSkip, 
								if(s2.timeRestrictedEating = 1, 1, 0) as is_on_timeRestrictionEating,
								ifnull(s2.currentManualSyntax,'') as currentManualSyntax,
								ifnull(greenFood,0) as greenFood, ifnull(redfood_adjusted,0) as redFood, ifnull(pstarFood,0) as pstarFood, ifnull(B_eggFood,0) as B_eggFood, ifnull(B_totalFood,0) as B_totalFood
								from 
								(
									select clientid, eventdateitz, 
									max(case when categoryType = 'BREAKFAST' then pS1food else null end) as B_pS1food, 
									max(case when categoryType = 'LUNCH' then pS1food else null end) as L_pS1food, 
									max(case when categoryType = 'DINNER' then pS1food else null end) as D_pS1food, 
									sum(case when categoryType like '%SNACK%' then pS1food else null end) as S_pS1food,
									
									max(case when categoryType = 'BREAKFAST' then pS2food else null end) as B_pS2food, 
									max(case when categoryType = 'LUNCH' then pS2food else null end) as L_pS2food, 
									max(case when categoryType = 'DINNER' then pS2food else null end) as D_pS2food,
									sum(case when categoryType like '%SNACK%' then pS2food else null end) as S_pS2food,
								   
									
									max(case when categoryType = 'BREAKFAST' then PCRfood else null end) as B_PCRfood, 
									max(case when categoryType = 'LUNCH' then PCRfood else null end) as L_PCRfood, 
									max(case when categoryType = 'DINNER' then PCRfood else null end) as D_PCRfood,
									sum(case when categoryType like '%SNACK%' then PCRfood else null end) as S_PCRfood,

									
									sum(redfood) as redFood, sum(greenfood) as greenFood, sum(pstar_food) as pstarFood, sum(redfood) - sum(pstar_food) as redfood_adjusted,
									sum(case when categoryType = 'BREAKFAST' then egg_related_food else 0 end) as B_eggFood,
									max(case when categoryType = 'BREAKFAST' then totalFood else null end) as B_totalFood
									from 
									(
										select a.clientid, eventdateitz, categoryType, 
										count(distinct case when foodGroups like '%P_STAR%' then b.foodid else null end) as pS1food, 
										count(distinct case when foodGroups like '%P_STAR%' then b.foodid else null end) as pS2food,
										count(distinct case when foodGroups like '%PCR%' then b.foodid else null end) as PCRfood,
										count(distinct case when b.recommendationRating <= 1 then b.foodid else null end) as redfood, 
										count(distinct case when b.recommendationRating > 2 then b.foodid else null end) as greenfood,
										count(distinct case when e.foodid is not null and b.recommendationRating <= 1 then a.itemid else null end) as pstar_food,
										count(distinct case when d.name like '%egg%' then b.foodid else null end) as egg_related_food,
										count(distinct b.foodid) as totalFood
										from bi_food_supplement_log_detail a inner join foods b on a.itemid = b.foodid
																			 inner join foodingredientmappings c on b.foodid = c.foodid
																			 inner join ingredients d on c.ingredientid = d.ingredientId
																			 left join personalizedfoods e on a.itemid = e.foodid and a.clientid = e.clientid and e.recommendationrating is not null 
										where eventdateitz >= date_sub(date(itz(now())), interval 30 day) 
										and a.category = 'foodlog'
										group by clientid, eventdateitz, categoryType
									) s 
									group by clientid, eventdateitz
								) s1 left join tmp_nutrition_profiles_aud s2 on s1.clientid = s2.clientid and s1.eventdateitz between s2.start_date and s2.end_date
							) s 
							) s2 on s1.clientid = s2.clientid and s1.date = s2.mealdate
				) s 
		;

		create index idx_tmp_nutrition_syntax_prediction on tmp_nutrition_syntax_prediction (clientid asc, mealdate asc, current_nut_syntax, color); 








		update bi_patient_reversal_state_gmi_x_date a inner join tmp_nutrition_syntax_prediction b on (a.clientid = b.clientid and a.date = b.mealdate)
		set a.nut_syntax = b.current_nut_syntax, 
			a.nut_syntax_color = b.color
		;
        
        
        update bi_patient_reversal_state_gmi_x_date a, 
		(
			select a.clientid, a.date, a.nut_syntax_color, b.tac_score, round(c.macro) as macro, round(c.micro) as micro, round(c.biota) as biota
			from 
			bi_patient_reversal_state_gmi_x_date a left join bi_tac_measure b on a.clientid = b.clientid and a.date = b.measure_date
												   left join nutritionscores c on a.clientid = c.clientid and a.date = c.date
			where a.date >= date_sub(date(itz(now())), interval 30 day)
		) b 
		set a.nut_adh_syntax = if(b.tac_score = 3 and macro >= 70 and micro >= 70 and biota >= 70 and b.nut_syntax_color = 'Green', 'Yes', 'No')
		where (a.clientid = b.clientid and a.date = b.date) 
		;
        
        
		update bi_patient_reversal_state_gmi_x_date a, 
		(
				select s1.clientid, s1.date, cast(avg(s2.nut_adh) as decimal(5,2)) as nut_adh_5d															
				from 															
									(															
										select clientid, date, case when nut_adh_syntax = 'yes' then 1 when nut_adh_syntax = 'no' then 0 end as nut_adh														
										from bi_patient_reversal_state_gmi_x_date
										where date >=  date_sub(date(itz(now())), interval 45 day)
									) s1 inner join 															
									(															
										select clientid, date, case when nut_adh_syntax = 'yes' then 1 when nut_adh_syntax = 'no' then 0 end as nut_adh														
										from bi_patient_reversal_state_gmi_x_date	
										 where date >= date_sub(date(itz(now())), interval 50 day)
									) s2 on s1.clientid = s2.clientid and s2.date between date_sub(s1.date, interval 4 day) and s1.date															
				group by s1.clientid, s1.date
		) b 
		set a.nut_adh_syntax_5d = b.nut_adh_5d
		where (a.clientid = b.clientid and a.date = b.date) 
		;


	drop temporary table if exists tmp_na_brittle_maintenance; 
	create temporary table tmp_na_brittle_maintenance as 
		select clientid, date, treatmentdays, last_available_cgm_5d_date, nut_adh_syntax_5d, medicine_drugs, total_200above_days, if(medicine_drugs like '%insu%', 'Yes', 'No') is_on_Insulin,
		case 	 
			 when treatmentdays >= 35
					and last_available_cgm_5d > 175
					and medicine_drugs like '%insu%'
					and nut_adh_syntax_5d < 0.8 then 'NA-Brittle1'

			 when treatmentdays >= 7
					and total_200above_days = 7 
					and nut_adh_syntax_5d < 0.8
					and if((medicine_drugs like '%alpha-Glucosidase Inhibitor%' or medicine_drugs like '%GLP-1 Receptor Agonist%' or
							medicine_drugs like '%Glinide%' or medicine_drugs like '%Sodium-Glucose Cotransporter 2 Inhibitor%' or medicine_drugs like '%Sulfonylurea%' or
							medicine_drugs like '%Thiazolidinedione%' or medicine_drugs like '%Diabetes Others%' or 
							(medicine_drugs like '%Biguanide%' and medicine_drugs like '%Dipeptidyl Peptidase 4 Inhibitor%')), 'Yes', 'No') = 'Yes' then 'NA-Brittle2'
						
			 when if(medicine_drugs like '%alpha-Glucosidase Inhibitor%' or medicine_drugs like '%GLP-1 Receptor Agonist%' or
					medicine_drugs like '%Glinide%' or medicine_drugs like '%Sodium-Glucose Cotransporter 2 Inhibitor%' or medicine_drugs like '%Sulfonylurea%' or
					medicine_drugs like '%Thiazolidinedione%' or  medicine_drugs like '%Diabetes Others%', 'Yes', 'No') = 'Yes'
					-- and nut_adh_syntax_5d < 0.8
					and treatmentdays > 90 then 'NA-Brittle3' else null end  as NABrittle_Category
				  
		from 
		(
			select s1.clientid, s1.date, s1.treatmentdays, s1.cgm_5d, s1.last_available_cgm_5d_date, s1.last_available_cgm_5d, s1.nut_adh_syntax_5d, 
			s1.medicine_drugs, count(distinct case when s2.cgm_5d > 200 then s2.date else null end) as total_200above_days
			from 
			(select clientid, date, treatmentdays, cgm_5d, cgm_1d, last_available_cgm_5d_date,last_available_cgm_5d, nut_adh_syntax_5d, medicine_drugs from bi_patient_reversal_state_gmi_x_date where date >= date_sub(date(itz(now())), interval 7 day)) s1
			inner join bi_patient_reversal_state_gmi_x_date s2 on s1.clientid = s2.clientid and s2.date between date_sub(s1.last_available_cgm_5d_date, interval 6 day) and s1.last_available_cgm_5d_date
			group by s1.clientid, s1.date
		) s
	;

	drop temporary table if exists tmp_na_brittle_maintenance_1; 

	create temporary table tmp_na_brittle_maintenance_1 as 
	select * from tmp_na_brittle_maintenance where NABrittle_Category is not null
	;

		create index idx_tmp_na_brittle_maintenance on tmp_na_brittle_maintenance_1 (clientid asc, date asc, NABrittle_Category);

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_na_brittle_maintenance_1 b on a.clientid = b.clientid and a.date = b.date 
		set a.nonAdh_brittle_category = b.NABrittle_Category
		;

		drop temporary table if exists tmp_na_brittle_maintenance
		; 
        
        drop temporary table if exists tmp_na_brittle_maintenance_1; 






drop temporary table if exists tmp_incidents_open_resolved; 

create temporary table tmp_incidents_open_resolved as 
select a.clientid, a.treatmentday, a.date, a.measure as opened, b.measure as resolved, a.measure - b.measure as outstanding
from 
(
	select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, date, 'Happy' as category, '# of IMT Escalaitons Opened' as measure_name, count(distinct c.incidentId) as measure 
	from bi_patient_reversal_state_gmi_x_date a inner join v_active_clients b on a.clientid = b.clientid 
												left join (	select distinct incidentid, clientid, date(itz(report_time)) as report_date, date(itz(resolved_time)) as resolved_date 
															from bi_incident_mgmt where category not in ('approval')
														   ) c on a.clientid = c.clientid and c.report_date <= a.date 
	where a.treatmentdays between 1 and 11 
	and b.labels not like '%TWIN_PRIME%'
	and b.patientname not like '%OBSOLETE%'
	group by a.clientid, date
) a left join 
(
	select a.clientid, concat('D',(treatmentdays - 1)) as treatmentday, date, 'Happy' as category, '# of IMT Escalaitons Resolved' as measure_name, count(distinct c.incidentId) as measure 
	from bi_patient_reversal_state_gmi_x_date a inner join v_active_clients b on a.clientid = b.clientid 
												left join (	select distinct incidentid, clientid, date(itz(report_time)) as report_date, date(itz(resolved_time)) as resolved_date 
															from bi_incident_mgmt where category not in ('approval')
														   ) c on a.clientid = c.clientid and c.resolved_date <= a.date 
	where a.treatmentdays between 1 and 11 
	and b.labels not like '%TWIN_PRIME%'
	and b.patientname not like '%OBSOLETE%'
	group by a.clientid, date
) b  on a.clientid = b.clientid and a.date = b.date
;

create index idx_tmp_incidents_open_resolved on tmp_incidents_open_resolved(clientid asc, date asc, opened, resolved, outstanding); 

update bi_patient_reversal_state_gmi_x_date a inner join tmp_incidents_open_resolved b on (a.clientid = b.clientid and a.date = b.date)
set a.IMT_opened = b.opened,
	a.IMT_resolved = b.resolved
; 

drop temporary table if exists tmp_incidents_open_resolved; 

		
		update bi_patient_reversal_state_gmi_x_date a inner join 
		(select clientid, date(eventLocalTime) as eventdate from calllogs where date(eventLocalTime) >= date_sub(now(), interval 3 month)) b on a.clientid = b.clientid and a.date = b.eventdate
		set a.isCallLogAvailable = if(b.clientid is not null, 'Yes', 'No') 
		where a.treatmentdays between 1 and 15
		;







drop temporary table if exists tmp_med_recommends_sla; 

create temporary table tmp_med_recommends_sla as 
select a.clientid, 
date(recommendationdate) as recommendationdate, 
count(distinct medicationRecommendationId) as total_recommendations, 
count(distinct case when status = 'Automated' then medicationRecommendationId else null end) as total_automated_by_doctor,
count(distinct case when status = 'Automated' and actionTime <= 24 then medicationRecommendationId else null end) as total_automated_by_doctor_within_24_hrs,
count(distinct case when status in ('SKIPPED', 'OVERRIDDEN') and actionTime <= 24 then medicationRecommendationId else null end) as total_not_automated_by_doctor_within_24_hrs

from 
(
	select a.clientid,a.medicationTierId,a.medicationRecommendationId, eventtime as recommendationdate, status, itz(b.dateAdded) as actionedDate, timestampdiff(HOUR, a.eventtime, itz(b.dateAdded)) as actionTime
	from 
	(
		select medicationRecommendationId, a.clientid, itz(eventtime) as eventtime, a.status, medicationTierId
		from medicationrecommendations a inner join v_client_tmp b on a.clientid = b.clientid 
		
        where a.status <> 'MANUAL'
        and a.type = 'DIABETES'
	) a inner join medicationhistories b on a.clientid = b.clientid and a.medicationRecommendationId = b.medicationRecommendationId
) a 
group by clientid, date(recommendationdate) 
;

create index idx_tmp_med_recommends_sla on tmp_med_recommends_sla(clientid asc, recommendationdate asc, total_recommendations, total_automated_by_doctor, total_automated_by_doctor_within_24_hrs); 


update bi_patient_reversal_state_gmi_x_date a inner join tmp_med_recommends_sla b on (a.clientid = b.clientid and a.date = b.recommendationdate)
set a.isMedRecommendationAvailable = if(total_recommendations > 0, 'Yes', 'No'),
	a.isMedRecommendationAdopted = if(total_recommendations = total_automated_by_doctor and total_automated_by_doctor > 0, 'Yes', 'No'),
    a.isMedRecommendationAdopted24hrs = if(total_recommendations = total_automated_by_doctor and total_automated_by_doctor > 0 and total_automated_by_doctor = total_automated_by_doctor_within_24_hrs, 'Yes', 'No')
where a.treatmentdays between 1 and 15
; 

drop temporary table if exists tmp_med_recommends_sla; 





		
		drop temporary table if exists tmp_delight_moments; 

		create temporary table tmp_delight_moments as 
		select a.clientid, a.date, a.treatmentdays, count(distinct category) as total_delight_moments
		from bi_patient_reversal_state_gmi_x_date a left join 
		(
			select s.clientid, 'First 1dg dropped by 50 points' as category, s1.date, s1.treatmentdays
			from 
			(
				select clientid, min(date) as first_1d_dropped_by_50_points
				from bi_patient_reversal_state_gmi_x_date 
				where  cast((start_labA1C * 28.7) - 46.7 as decimal(10,5)) - cgm_1d >= 50
				group by clientid 
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_1d_dropped_by_50_points = s1.date

			union all 

			select s.clientid, 'First 1dg below 140' as category, s1.date, s1.treatmentdays
			from 
			(
				select clientid, min(date) as first_1d_below_140
				from bi_patient_reversal_state_gmi_x_date 
				where  cgm_1d < 140
				group by clientid 
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_1d_below_140 = s1.date
			
            
			union all 

			select s.clientid, 'First Medicine Reduction' as category, s1.date, s1.treatmentdays
			from 
			(
				select clientid, min(date) as first_medicine_reduced
				from bi_patient_reversal_state_gmi_x_date 
				where is_MedReduced = 'Yes'
				group by clientid 
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_medicine_reduced = s1.date

			union all 

			select s.clientid, 'Medicine Stopped' as category, s1.date, s1.treatmentdays
			from 
			(
				select clientid, min(date) as first_medicine_stopped
				from bi_patient_reversal_state_gmi_x_date 
				where medicine is null
				group by clientid 
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_medicine_stopped = s1.date

			union all

			select s.clientid, 'Achieved Reversal + Metformin' as category, s1.date, s1.treatmentdays
			from 
			(
				select clientid, min(date) as first_InReversal_W_Metformin
				from bi_patient_reversal_state_gmi_x_date 
				where is_MetOnly_by_OPS = 'Yes'
				group by clientid 
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_InReversal_W_Metformin = s1.date

			union all

			select s.clientid, 'Achieved Reversal + No Meds' as category, s1.date, s1.treatmentdays
			from 
			(
				select clientid, min(date) as first_InReversal
				from bi_patient_reversal_state_gmi_x_date 
				where is_InReversal = 'Yes'
				group by clientid 
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_InReversal = s1.date

			union all

			select s.clientid, '2Kg weight reduction' as category, s1.date, s1.treatmentdays
			from 
			(
				select a.clientid, min(date) as first_WeightReduced
				from bi_patient_reversal_state_gmi_x_date a inner join dim_client b on a.clientid = b.clientid 
				where is_row_Current = 'y'
				and b.start_weight - weight_1d >= 2
				group by a.clientid 
			) s inner join bi_patient_reversal_state_gmi_x_date s1 on s.clientid = s1.clientid and s.first_WeightReduced = s1.date
		) b on a.clientid = b.clientid and b.date = a.date 
		where a.treatmentdays between 1 and 15
		group by a.clientid, a.date, a.treatmentDays
		;

		create index idx_tmp_delight_moments on tmp_delight_moments (clientid asc, date asc, treatmentdays asc, total_delight_moments);

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_delight_moments b on (a.clientid = b.clientid and a.date = b.date)
		set a.total_delight_moments = b.total_delight_moments
		;

		drop temporary table if exists tmp_delight_moments; 


-- maintain NAFLS_LiverFat Score
		drop temporary table if exists tmp_nafld_maintenace; 

		create temporary table tmp_nafld_maintenace as 
		select clientid, date, sysbp_5d, diasBP_5d, s.bmi, is_Inreversal, is_metOnly_by_ops, 
		max(latest_value_available_bloodworkDate) as latest_value_available_bloodworkDate,
		max(latest_tg) as latest_tg,
		max(latest_hdl) as latest_hdl,
		max(latest_fasting_glucose) as latest_fasting_glucose,
		max(latest_tg_hdl_ratio) as latest_tg_hdl_ratio,
		max(latest_ast) as latest_ast,
		max(latest_alt) as latest_alt,
		max(latest_insulin) as latest_insulin,
		max(latest_ast_alt_ratio) as latest_ast_alt_ratio
		from
		(
			select s.clientid, s.date, sysbp_5d, diasBP_5d, s.bmi, is_Inreversal, is_metOnly_by_ops, 
			itz(s1.bloodworkTime) as latest_value_available_bloodworkDate,
			triglycerides as latest_tg,
			hdlCholesterol as latest_hdl,
			glucose as latest_fasting_glucose,
			cast(triglycerides/hdlCholesterol as decimal(10,2))  as latest_tg_hdl_ratio,
			ast as latest_ast,
			alt as latest_alt,
			insulin as latest_insulin, 
			cast(ast/alt as decimal(10,2)) as latest_ast_alt_ratio
			from
			(
					select a.clientid, date, recentLabDate, last_available_LabAst_Alt_date, sysbp_5d, diasBP_5d, weight_1d, is_Inreversal, is_metOnly_by_ops, last_available_bmi as BMI 
                    from bi_patient_reversal_state_gmi_x_date a left join (select distinct clientid, height from dim_client where is_Row_current = 'y') b on a.clientid = b.clientid 
					where date >= date_sub(date(now()), interval 14 day)
			) s inner join clinictestresults s1 on s.clientid = s1.clientid and s.last_available_LabAst_Alt_date = date(itz(ifnull(s1.bloodworktime, s1.eventtime))) 
		) s
		group by clientid, date
		;

		create index idx_tmp_nafld_maintenace on tmp_nafld_maintenace (clientid asc, latest_value_available_bloodworkDate); 

		drop temporary table if exists tmp_nafld_liverfat_score; 

		create temporary table tmp_nafld_liverfat_score as 
		select s.*, if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2) as latest_diabetes_flag,
		if(BMI is null, null, -2.89 + (1.18 * (if(is_MetabolicSyndrome = 'Y', 1, 0))) + (0.45 * (if(is_Inreversal = 'Yes' or is_metOnly_by_ops = 'Yes', 0, 2))) + (0.15 * latest_insulin) + (0.04 * latest_ast) - (0.94 * latest_ast_alt_ratio)) as NAFLD_LFS
		from 
		(
			select s.*, 
			case when (latest_BMI_flag + latest_TG_flag + latest_HDL_flag + latest_fastingGlucose_flag + latest_BP_flag) >= 3 then 'Y' else 'N' end as is_MetabolicSyndrome
			from 
			(
				select b.clientid, b.date, is_InReversal, is_metOnly_by_ops, bmi, latest_insulin, latest_ast, latest_ast_alt_ratio, latest_value_available_bloodworkDate,
				if(b.bmi is not null and b.bmi >= 25, 1, 0) as latest_BMI_flag, 
				if(b.latest_tg is not null and b.latest_tg >= 150, 1, 0) as latest_TG_flag, 
				if(b.latest_hdl is not null and b.latest_hdl < if(gender = 'MALE', 40, 50), 1, 0) as latest_HDL_flag, 
				if(b.latest_fasting_glucose is not null and b.latest_fasting_glucose >= 100 , 1, 0) as latest_fastingGlucose_flag,
				if((b.sysBP_5d is not null or b.diasBP_5d is not null) and (ifnull(b.sysBP_5d,0) >= 130 or ifnull(b.diasBP_5d,0) >= 85), 1, 0) as latest_BP_flag
				from tmp_nafld_maintenace b  left join v_allClient_list c on b.clientid = c.clientid 
			) s 
		) s
		;

		create index idx_tmp_nafld_liverfat_score on tmp_nafld_liverfat_score (clientid asc, date asc, NAFLD_LFS); 


		update bi_patient_reversal_state_gmi_x_date a inner join tmp_nafld_liverfat_score b on (a.clientid = b.clientid and a.date = b.date)
		set a.NAFLD_LFS = b.NAFLD_LFS,
			a.is_MetabolicSyndrome = b.is_MetabolicSyndrome
		; 

		drop temporary table if exists tmp_nafld_liverfat_score;

		drop temporary table if exists tmp_nafld_maintenace; 

-- Hearthealth FRS score maintenance

		drop temporary table if exists tmp_heartHealth; 

		create temporary table tmp_heartHealth as 
		select clientid,date, is_Inreversal, is_metOnly_by_ops,sysbp_5d,
		max(latest_bloodworkDate) as latest_bloodworkDate,
		max(latest_hdl) as latest_hdl,
		max(latest_TotalCholesterol) as latest_TotalCholesterol,
		max(useSmoking) as useSmoking
		from
		(
			select s.clientid, s.date, is_Inreversal, is_metOnly_by_ops,sysbp_5d,
			itz(s1.bloodworktime) as latest_bloodworkDate,
			hdlCholesterol as latest_hdl,
			cholesterol as latest_TotalCholesterol, 
			case when s2.clientid is not null then 'Y' else 'N' end as useSmoking
			from
			(
				select a.clientid, date, recentLabDate, sysbp_5d, is_Inreversal, is_metOnly_by_ops
				from bi_patient_reversal_state_gmi_x_date a
				where date >= date_sub(date(itz(now())), interval 14 day)
			) s inner join clinictestresults s1 on s.clientid = s1.clientid and s.recentLabDate = date(itz(s1.bloodworktime)) 
				left join 			
					(
						select clientid, min(ifnull(bloodworktime, eventtime)) as firstbloodwork from clinictestresults
						where useSmoking = 1
						and deleted = 0
						group by clientid
					) s2 on s.clientid = s2.clientid 
		) s
		group by clientid, date
		;


		create index idx_tmp_heartHealth on tmp_heartHealth(clientid asc, latest_bloodworkDate); 


		set @M_age := 3.06117, @M_TC := 1.12370, @M_HDL := -0.93263, @M_BP_TRT := 1.99881, @M_BP_NonTRT := 1.93303, @M_SMOKE := 0.65451, @M_DIAB := 0.57367; 
		set @F_age := 2.32888, @F_TC := 1.20904, @F_HDL := -0.70833, @F_BP_TRT := 2.82263, @F_BP_NonTRT := 2.76157, @F_SMOKE := 0.52873, @F_DIAB := 0.69154; 

		drop temporary table if exists tmp_FRS;

		create temporary table tmp_FRS as 
		select clientid, date, total_lastest_coeff, 
		cast(case when gender = 'MALE' then 1 - power(0.88936, exp(total_lastest_coeff - 23.9802))
				  when gender = 'FEMALE' then 1 - power(0.95012, exp(total_lastest_coeff - 26.1931)) else null end * 100 as decimal(6,2)) as latest_FRS     
		from 
		(
			select clientid, date, gender, age, 
			age_coeff + latest_sysBP_coeff + latest_totalCholesterol_coeff + latest_totalHDL_coeff + smoke_coeff + latest_diab_coeff as total_lastest_coeff
			from 
			( 
					select s.clientid, date, gender, age,
					log_age * if(gender = 'MALE', @M_age, @F_age) as age_coeff, 
					if(useSmoking = 'Y', 1, 0) * if(gender = 'MALE', @M_SMOKE, @F_SMOKE)  as smoke_coeff, 

					log_latest_sysBP * case when gender = 'MALE' and isHTNTreated = 'Y' then @M_BP_TRT
										   when gender = 'MALE' and isHTNTreated = 'N' then @M_BP_NonTRT
										   when gender = 'FEMALE' and isHTNTreated = 'Y' then @F_BP_TRT
										   when gender = 'FEMALE' and isHTNTreated = 'N' then @F_BP_NonTRT else null end as latest_sysBP_coeff, 
					log_latestTotalCholesterol * if(gender = 'MALE', @M_TC, @F_TC) as latest_totalCholesterol_coeff, 
					log_latest_hdl * if(gender = 'MALE', @M_HDL, @F_HDL) as latest_totalHDL_coeff, 
					if(is_InReversal = 'Yes' or is_metOnly_by_ops= 'Yes', 0, 1) * if(gender = 'MALE', @M_DIAB, @F_DIAB) as latest_diab_coeff
					from 
					(
						select b.clientid, b.date, b.latest_TotalCholesterol, b.latest_hdl, is_InReversal, is_metOnly_by_ops, 
						b.useSmoking, c.age, b.sysBP_5d, if(dc.start_htn_medicine_drugs is not null, 'Y', 'N') as isHTNTreated, c.gender,
						if(b.latest_TotalCholesterol <=0, null, ln(b.latest_TotalCholesterol)) as log_latestTotalCholesterol, 
						if(b.latest_hdl <= 0, null, ln(b.latest_hdl)) as log_latest_hdl,
						if(c.age <= 0, null, ln(c.age)) as log_age,
						if(sysBP_5d <= 0, null, ln(sysBP_5d)) as log_latest_sysBP
						from 
						tmp_heartHealth b left join v_client_tmp c on b.clientid = c.clientid
										  left join dim_client dc on b.clientid = dc.clientid and dc.is_row_current = 'y'
					) s 
			) s 
		) s 
		; 

		create index idx_tmp_FRS on tmp_FRS (clientid asc, date asc, latest_FRS); 

		update bi_patient_reversal_state_gmi_x_date a inner join tmp_FRS b on (a.clientid = b.clientid and a.date = b.date)
		set a.FRS_HeartHealth = b.latest_FRS
		; 

		drop temporary table if exists tmp_FRS;

		drop temporary table if exists tmp_heartHealth; 

	

		drop temporary table if exists tmp_med_recommends;
		drop temporary table if exists tmp_last_available_cgm_5d;
		drop temporary table if exists tmp_last_14days_CGM_avail;
		drop temporary table if exists tmp_cgm_1d_1; 
		drop temporary table if exists tmp_cgm_1d_2; 
		drop temporary table if exists tmp_cgm_14d; 
		drop temporary table if exists tmp_pts_detail_reversal_medicine;
		drop temporary table if exists tmp_med_count;
		drop temporary table if exists tmp_recent_LabA1C_value; 
		drop temporary table if exists tmp_nutrition_profiles_aud;
		drop temporary table if exists tmp_nutrition_syntax_prediction;
	
	
-- 20211203 AV - Maintain the Exclude label daily because a member can be added or dropped from the reversal calculation using this label. 
-- It has become important to track now because if this label is not tracked the removal or addition of this flag would affect the historical numbers
set time_zone = 'asia/calcutta';

drop temporary table if exists tmp_excludelabel_tracker; 

create temporary table tmp_excludelabel_tracker as  						
select a.id, a.date, if(b.labels like '%exclude%', 'Yes', null) as isExcludeOn
from 
(
			select b.id, date, max(b.datemodified) as lastupdate
			from v_date_to_date a inner join clients_aud b on date(b.datemodified) <= date
								  inner join clients c on b.id = c.id
			where date >= date_sub(date(now()), interval 7 day)
			and c.isdemo is false
			and c.status in ('active','discharged','inactive')
			group by b.id, date
) a left join clients_aud b on a.id = b.id and a.lastupdate = b.datemodified
where b.labels like '%exclude%'
;

create index idx_tmp_excludelabel_tracker on tmp_excludelabel_tracker (id asc, date asc); 

update bi_patient_reversal_state_gmi_x_date a inner join tmp_excludelabel_tracker b on (a.clientid = b.id and a.date = b.date)
set a.isExcludeLabelOn = b.isExcludeOn
;

drop temporary table if exists tmp_excludelabel_tracker; 

set time_zone = 'utc';



		update dailyProcessLog
		set updateDate = now()
		where processName = 'sp_load_bi_patient_reversal_state_GMI_x_date'
		;

END