CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_food_supplement_log_detail_RCT`(  )
BEGIN




update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_food_supplement_log_detail_RCT'
;

drop temporary table if exists tmp_all_RCT_patients; 

create temporary table tmp_all_RCT_patients as 
select c.id as clientid, c.labels, c.status, date(itz(c.enrollmentdate)) as enrollmentdate from clients c 
			JOIN `twinspii`.`clientspii` `cp` ON ((`c`.`ID` = `cp`.`ID`))
			JOIN `twins`.`doctors` `d` ON ((`c`.`doctorId` = `d`.`ID`))
WHERE (
			(`c`.`deleted` = 0)
			AND (NOT ((`cp`.`firstName` LIKE '%TEST%')))
			AND (NOT ((`cp`.`lastName` LIKE '%TEST%')))
			AND (c.labels like '%PRIME%' or c.labels like '%CONTROL%' or c.labels like '%NAFLD%')
	   )
;
        
create index idx_tmp_all_RCT_patients on tmp_all_RCT_patients (clientid asc, enrollmentdate); 
 
drop temporary table if exists tmp_foodlog_section_RCT; 
create temporary table tmp_foodlog_section_RCT
(
		category varchar(20), 
		clientid int not null, 
        eventdateITZ date, 
		eventTimeITZ datetime,
        categorytype varchar(30),
		itemId int, 
		itemName varchar(200),
		quantity decimal(10,5),
		E5grading varchar(25),
		measurementType varchar(100),
		measurementAmount decimal(10,5),
		net_carb decimal(10,5),
		calories decimal(10,5), 
		fat decimal(10,5), 
		fibre decimal(10,5),
		protein decimal(10,5),
		total_Carb decimal(10,5),
		netGICarb decimal(10,5),
        foodRating decimal(10,2)
);

insert into tmp_foodlog_section_RCT 
select distinct 
 'foodlog' as category, 
		fl.clientid, 
        fl.mealdate as eventdateITZ, 
		
        concat(fl.mealdate,' ',fl1.mealDateTimeOnly) as eventTimeITZ,
        fl.mealtype as categorytype,
		b.foodid as itemId, 
		b.foodlabel as itemName,
		fl.quantity as quantity,
		b.E5grading,
		fl.measure as measurementType,
		fl.basequantity as measurementAmount,
		cast((b.net_carb * fl.baseQuantity) as decimal(10,5)) as net_carb,
		cast((b.calories * fl.baseQuantity) as decimal(10,5)) as calories, 
		cast((b.fat * fl.baseQuantity) as decimal(10,5)) as fat, 
		cast((b.fibre * fl.baseQuantity) as decimal(10,5)) as fibre,
		cast((b.protein * fl.baseQuantity) as decimal(10,5)) as protein,
		cast((b.total_Carb *fl.baseQuantity) as decimal(10,5)) as total_carb,
		b.netGICarb,
        foodRating as foodRating
from foodlogs_view fl inner join foodlogs fl1 on fl.foodlogid = fl1.foodlogid
					  inner join tmp_all_RCT_patients rct on fl.clientid = rct.clientid and fl.mealdate >= rct.enrollmentdate
				 left join (
									select
									foodid, foodlabel,
									CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
										 WHEN (recommendationRating <= 1) THEN 'Red'
										 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
										 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
										 WHEN (recommendationRating > 3) THEN 'Green*'
									END AS E5grading,
									measure,
									measurementType,
									measurementAmount,
									carb - fibre as net_carb 
									,calories, carb as total_Carb, protein, fat, fibre, 
                                    cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as decimal(10,5)) AS netGICarb
									from foods
						) b on fl.foodid = b.foodid
				left join personalizedfoods p on fl.foodid = p.foodid and fl.clientid = p.clientid                        
-- where fl.mealdate >= date_sub(date(itz(now())), interval 2 month) and fl.mealdate <= date(itz(now()))
;


-- truncate and load is needed because RCT coaches can modify any older data. 

truncate table bi_food_supplement_log_detail_RCT
;

insert into bi_food_supplement_log_detail_RCT
(category, clientid, eventdateITZ, eventTimeITZ, categorytype, itemId, itemName, quantity, E5grading, measurementType, measurementAmount,
net_carb, calories, fat, fibre, protein, total_Carb, netGICarb, foodRating)

select category, clientid, eventdateITZ, eventTimeITZ, categorytype, itemId, itemName, quantity, E5grading, 
measurementType, measurementAmount,net_carb,calories, fat, fibre, protein, total_Carb, netGICarb, foodRating
from tmp_foodlog_section_RCT a 
;
/*
where not exists 
(
	select 1 from bi_food_supplement_log_detail_RCT b where a.category = b.category and a.clientid = b.clientid and a.eventdateitz = b.eventdateitz and a.eventtimeITZ = b.eventtimeITZ and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0)
);

create index tmp_idx_foodlog_RCT on tmp_foodlog_section_RCT (category asc, clientid asc, eventdateitz asc, categorytype asc, itemid asc);

update bi_food_supplement_log_detail_RCT a 
inner join tmp_foodlog_section_RCT b 
on (a.category = b.category and a.clientid = b.clientid and a.eventdateitz = b.eventdateitz and a.eventtimeITZ = b.eventtimeITZ and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0))
set a.eventtimeITZ = b.eventtimeITZ, 
	a.itemName = b.itemName,
    a.quantity = b.quantity,
    a.E5grading = b.E5grading,
    a.measurementType = b.measurementType,
    a.measurementAmount = b.measurementAmount,
    a.net_carb = b.net_carb,
    a.calories = b.calories,
    a.fat = b.fat,
    a.fibre = b.fibre,
    a.protein = b.protein,
    a.total_carb = b.total_carb,
    a.netGICarb = b.netGICarb,
    a.foodRating = b.foodRating
;


drop temporary table if exists tmp_data_to_check_RCT;
create temporary table tmp_data_to_check_RCT
as 
select a.category, a.clientid, a.eventdateITZ, a.eventtimeITZ, a.categorytype, a.itemId
from bi_food_supplement_log_detail_RCT  a 
where a.eventdateitz >= date_sub(date(itz(now())), interval 50 day) and a.eventdateitz <= date(itz(now()))
and a.category = 'foodlog'
; 

create index idx_tmp_data_to_check_RCT on tmp_data_to_check_RCT (category asc, clientid asc, eventdateitz asc, eventtimeITZ asc, categorytype asc, itemid asc);

create temporary table tmp_foodlogs_removed_by_ops_RCT as 
select a.category, a.clientid, a.eventdateITZ, a.eventtimeITZ, a.categorytype, a.itemId
from tmp_data_to_check_RCT a
where not exists 
(
	select 1 from tmp_foodlog_section_RCT b where a.category = b.category and a.clientid = b.clientid and a.eventdateitz = b.eventdateITZ and a.eventtimeITZ = b.eventTimeITZ and a.categoryType = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemId,0)
);


delete bi_food_supplement_log_detail_RCT
from bi_food_supplement_log_detail_RCT, tmp_foodlogs_removed_by_ops_RCT
where bi_food_supplement_log_detail_RCT.clientid = tmp_foodlogs_removed_by_ops_RCT.clientid
	  and bi_food_supplement_log_detail_RCT.eventdateitz = tmp_foodlogs_removed_by_ops_RCT.eventdateitz
      and bi_food_supplement_log_detail_RCT.eventtimeITZ = tmp_foodlogs_removed_by_ops_RCT.eventtimeITZ
      and bi_food_supplement_log_detail_RCT.categoryType = tmp_foodlogs_removed_by_ops_RCT.categoryType
      and ifnull(bi_food_supplement_log_detail_RCT.itemid,0) = ifnull(tmp_foodlogs_removed_by_ops_RCT.itemid,0)
      and bi_food_supplement_log_detail_RCT.category = 'foodlog'
;


drop temporary table if exists tmp_foodlogs_removed_by_ops_RCT;
drop temporary table if exists tmp_foodlog_section_RCT;
drop temporary table if exists tmp_data_to_check_RCT;
*/

drop temporary table if exists tmp_supplementlog_section_RCT;

create temporary table tmp_supplementlog_section_RCT
(
		category varchar(20), 
		clientid int not null, 
        eventdateITZ date, 
		eventTimeITZ datetime,
        categorytype varchar(30),
		itemId int, 
		itemName varchar(200),
		quantity decimal(10,5),
		E5grading varchar(25),
		measurementType varchar(100),
		measurementAmount decimal(10,5),
		net_carb decimal(10,5),
		calories decimal(10,5), 
		fat decimal(10,5), 
		fibre decimal(10,5),
		protein decimal(10,5),
		total_Carb decimal(10,5),
		netGICarb decimal(10,5)
);
                        
insert into tmp_supplementlog_section_RCT                       
SELECT 'supplementlog' as category, 
        s.clientId AS clientId,
        s.eventDateITZ,
        s.eventTimeITZ AS eventTimeITZ,
        timeslot AS categorytype,
        s.supplementId AS itemId,
        
        case when s.supplement = 'MICRONUTRIENTS_MIX' then 'MB Mix' else s.supplement end as itemName,
        s.quantity AS quantity,
        '' as E5grading,
        t.measurementType,
        t.measurementAmount,
        (((t.carb - t.fibre)/t.measurementAmount) * s.quantity) AS netCarb,
        ((t.calories/t.measurementAmount) * s.quantity) AS calories,
        ((t.fat/t.measurementAmount) * s.quantity) AS fat,
        ((t.fibre/t.measurementAmount) * s.quantity) AS fiber,
        ((t.protein/t.measurementAmount) * s.quantity) AS protein,
        ((t.carb/t.measurementAmount) * s.quantity) AS totalCarb,
        cast(IFNULL((((t.carb - t.fibre) * t.glycemicIndex) / 55),0) as decimal(10,5)) AS netGICarb
FROM
        (
			SELECT DISTINCT 
					t.clientId AS clientId,
                    date(t.scheduledLocalTime) AS eventDateITZ,
					max(t.scheduledLocalTime) AS eventTimeITZ,
					t.status AS status,
					r.supplementId AS supplementId,
					t.type AS supplement,
                    t.timeslot AS timeslot,
					sum(t.targetvalue) AS quantity
			FROM
					twins.clienttodoitems t
			inner join tmp_all_RCT_patients rct on t.clientid = rct.clientid and date(t.scheduledLocalTime) >= rct.enrollmentdate
			LEFT JOIN twins.ref_supplement r ON t.type = r.supplement
			WHERE t.category = 'SUPPLEMENT' AND t.status = 'FINISHED'
			-- and t.scheduledLocalTime >= date(date_sub(now(), interval 2 month)) 
            group by clientid, date(scheduledLocalTime), status, r.supplementid, t.type, timeslot
        ) s
LEFT JOIN twins.supplements t ON (s.supplementId = t.SupplementId)
;

create index tmp_idx_supplementlog_RCT on tmp_supplementlog_section_RCT (category asc, clientid asc, eventdateitz asc, eventtimeITZ asc, categorytype asc, itemid asc, itemname asc);

insert into bi_food_supplement_log_detail_RCT
(category, clientid, eventdateITZ, eventTimeITZ, categorytype, itemId, itemName, quantity, E5grading, measurementType, measurementAmount,
net_carb,calories, fat, fibre, protein, total_Carb, netGICarb)

select category, clientid, eventdateITZ, eventTimeITZ, categorytype, itemId, itemName, quantity, E5grading, 
measurementType, measurementAmount,net_carb,calories, fat, fibre, protein, total_Carb, netGICarb
from tmp_supplementlog_section_RCT a 
;

/*
where not exists 
(
select 1 from bi_food_supplement_log_detail_RCT b where a.category = b.category and a.clientid = b.clientid and a.eventdateitz = b.eventdateitz and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0) and a.itemName = b.itemName
);

update bi_food_supplement_log_detail_RCT a 
inner join tmp_supplementlog_section_RCT b on (a.category = b.category and a.clientid = b.clientid and a.eventdateitz = b.eventdateitz and a.categorytype = b.categorytype and ifnull(a.itemid,0) = ifnull(b.itemid,0) and a.itemName = b.itemName)
set	a.eventtimeITZ = b.eventtimeITZ, 
	a.itemName = b.itemName,
    a.quantity = b.quantity,
    a.E5grading = b.E5grading,
    a.measurementType = b.measurementType,
    a.measurementAmount = b.measurementAmount,
    a.net_carb = b.net_carb,
    a.calories = b.calories,
    a.fat = b.fat,
    a.fibre = b.fibre,
    a.protein = b.protein,
    a.total_carb = b.total_carb,
    a.netGICarb = b.netGICarb
;




drop temporary table if exists tmp_supplements_to_be_removed_RCT
;

create temporary table tmp_supplements_to_be_removed_RCT as 
select category, clientid, eventdateITZ, eventtimeITZ, categorytype, itemId
from bi_food_supplement_log_detail_RCT  a 
where a.eventdateitz >= date_sub(date(itz(now())), interval 50 day) and a.eventdateitz <= date(itz(now()))
and category = 'supplementlog'
and not exists 
(select 1 from tmp_supplementlog_section_RCT b where a.category = b.category and a.clientid = b.clientid and a.eventdateitz = b.eventdateITZ and a.eventtimeITZ = b.eventTimeITZ and a.categoryType = b.categoryType and ifnull(a.itemid,0) = ifnull(b.itemid,0))
;

delete bi_food_supplement_log_detail_RCT
from bi_food_supplement_log_detail_RCT, tmp_supplements_to_be_removed_RCT
where bi_food_supplement_log_detail_RCT.clientid = tmp_supplements_to_be_removed_RCT.clientid
	  and bi_food_supplement_log_detail_RCT.eventdateitz = tmp_supplements_to_be_removed_RCT.eventdateitz
      and bi_food_supplement_log_detail_RCT.eventtimeITZ = tmp_supplements_to_be_removed_RCT.eventtimeITZ
      and bi_food_supplement_log_detail_RCT.categoryType = tmp_supplements_to_be_removed_RCT.categoryType
      and ifnull(bi_food_supplement_log_detail_RCT.itemid,0) = ifnull(tmp_supplements_to_be_removed_RCT.itemid,0)
      and bi_food_supplement_log_detail_RCT.category = 'supplementlog'
;

drop temporary table tmp_supplements_to_be_removed_RCT
;

drop temporary table tmp_supplementlog_section_RCT;
*/


		drop temporary table if exists tmp_1d_foodlog_nutrition_target_RCT; 

		    create temporary table tmp_1d_foodlog_nutrition_target_RCT
            (
				clientid	int	,
				mealdate	date,	
				category	varchar(11)	,
				net_carb	double precision	,
				calories	double precision	,
				netGlycemicCarb	double precision	,
				fibre	double precision	,
				fat	double precision	,
				protein	double precision	,
				Total_Carb	double precision	,
				sodium	double precision	,
				potassium	double precision	,
				magnesium	double precision	,
				calcium	double precision	,
				chromium	double precision	,
				omega3	double precision	,
				omega6	double precision	,
				alphaLipoicAcid	double precision	,
				q10	double precision	,
				biotin	double precision	,
				flavonoids	double precision	,
				improveInsulinSensitivity	double precision,	
				inhibitGluconeogenis	double precision	,
				inhibitCarbAbsorption	double precision	,
				improveInsulinSecretion	double precision	,
				improveBetaCellRegeneration	double precision	,
				inhibitHunger	double precision	,
				inhibitGlucoseKidneyReabsorption	double precision	,
				lactococcus	double precision	,
				lactobacillus	double precision	,
				leuconostoc	double precision	,
				streptococcus	double precision	,
				bifidobacterium	double precision	,
				saccharomyces	double precision	,
				bacillus	double precision	,
				fructose	double precision	,
				glycemicIndex	double precision	,
				saturatedFat	double precision	,
				monounsaturatedFat	double precision	,
				polyunsaturatedFat	double precision	,
				transFat	double precision	,
				cholesterol	double precision	,
				histidine	double precision	,
				isolecuine	double precision	,
				lysine	double precision	,
				methionineAndCysteine	double precision	,
				phenylananineAndTyrosine	double precision	,
				tryptophan	double precision	,
				threonine	double precision	,
				valine	double precision	,
				vitaminA	double precision	,
				vitaminC	double precision	,
				vitaminD	double precision	,
				vitaminE	double precision	,
				vitaminK	double precision	,
				vitaminB1	double precision	,
				vitaminB12	double precision	,
				vitaminB2	double precision	,
				vitaminB3	double precision	,
				vitaminB5	double precision	,
				vitaminB6	double precision	,
				folate	double precision	,
				copper	double precision	,
				iron	double precision	,
				zinc	double precision	,
				manganese	double precision	,
				phosphorus	double precision	,
				selenium	double precision	,
				omega6_3_ratio	double precision	,
				zinc_copper_ratio	double precision	,
				pot_sod_ratio	double precision	,
				cal_mag_ratio	double precision	,
				pralAlkalinity	double precision	,
				improveBloodPressure	double precision	,
				improveCholesterol	double precision	,
				reduceWeight	double precision	,
				improveRenalFunction	double precision	,
				improveLiverFunction	double precision	,
				improveThyroidFunction	double precision	,
				improveArthritis	double precision	,
				reduceUricAcid	double precision	,
				veg	double precision	,
				nonVeg	double precision	,
				fruit	double precision	,
				oil	double precision	,
				spice	double precision	,
				grain	double precision	,
				legume	double precision	,
				nuts	double precision	,
				seeds	double precision	,
				inflammatoryIndex	double precision	,
				oxidativeStressIndex	double precision	,
				gluten	double precision	,
				allergicIndex	double precision	,
				water	double precision	
            );
           
            Insert into tmp_1d_foodlog_nutrition_target_RCT
            (
				clientid,
				mealdate,
				category,
				net_carb,
				calories,
				netGlycemicCarb,
				fibre,
				fat,
				protein,
				Total_Carb,
				sodium,
				potassium,
				magnesium,
				calcium,
				chromium,
				omega3,
				omega6,
				alphaLipoicAcid,
				q10,
				biotin,
				flavonoids,
				improveInsulinSensitivity,
				inhibitGluconeogenis,
				inhibitCarbAbsorption,
				improveInsulinSecretion,
				improveBetaCellRegeneration,
				inhibitHunger,
				inhibitGlucoseKidneyReabsorption,
				lactococcus,
				lactobacillus,
				leuconostoc,
				streptococcus,
				bifidobacterium,
				saccharomyces,
				bacillus,
				fructose,
				glycemicIndex,
				saturatedFat,
				monounsaturatedFat,
				polyunsaturatedFat,
				transFat,
				cholesterol,
				histidine,
				isolecuine,
				lysine,
				methionineAndCysteine,
				phenylananineAndTyrosine,
				tryptophan,
				threonine,
				valine,
				vitaminA,
				vitaminC,
				vitaminD,
				vitaminE,
				vitaminK,
				vitaminB1,
				vitaminB12,
				vitaminB2,
				vitaminB3,
				vitaminB5,
				vitaminB6,
				folate,
				copper,
				iron,
				zinc,
				manganese,
				phosphorus,
				selenium,
				omega6_3_ratio,
				zinc_copper_ratio,
				pot_sod_ratio,
				cal_mag_ratio,
				pralAlkalinity,
				improveBloodPressure,
				improveCholesterol,
				reduceWeight,
				improveRenalFunction,
				improveLiverFunction,
				improveThyroidFunction,
				improveArthritis,
				reduceUricAcid,
				veg,
				nonVeg,
				fruit,
				oil,
				spice,
				grain,
				legume,
				nuts,
				seeds,
				inflammatoryIndex,
				oxidativeStressIndex,
				gluten,
				allergicIndex,
				water
            )
			select clientid, mealdate, '1d-Nutrient' as category, 
			cast(sum(net_carb) as double precision)as net_carb,
			cast(sum(calories) as double precision) as calories,
			cast(sum(netGlycemicCarb) as double precision) as netGlycemicCarb,
			cast(sum(fibre) as double precision) as fibre,
			cast(sum(fat) as double precision) as fat,
			cast(sum(protein) as double precision) as protein,
			cast(sum(Total_Carb) as double precision) as Total_Carb,
			cast(sum(sodium) as double precision) as sodium,
            
            cast(sum(potassium) as double precision) as potassium,
			cast(sum(magnesium) as double precision) as magnesium,
			cast(sum(calcium) as double precision) as calcium,
			cast(sum(chromium) as double precision) as chromium,
			cast(sum(omega3) as double precision) as omega3,
			cast(sum(omega6) as double precision) as omega6,
			cast(sum(alphaLipoicAcid) as double precision) as alphaLipoicAcid,
			cast(sum(q10) as double precision) as q10,
			cast(sum(biotin) as double precision) as biotin,
			cast(sum(flavonoids) as double precision) as flavonoids,
			cast(sum(improveInsulinSensitivity) as double precision) as improveInsulinSensitivity,
			cast(sum(inhibitGluconeogenis) as double precision) as inhibitGluconeogenis,
			cast(sum(inhibitCarbAbsorption) as double precision) as inhibitCarbAbsorption,
			cast(sum(improveInsulinSecretion) as double precision)  as improveInsulinSecretion,
			cast(sum(improveBetaCellRegeneration) as double precision) as improveBetaCellRegeneration,
			cast(sum(inhibitHunger) as double precision) as inhibitHunger,
			cast(sum(inhibitGlucoseKidneyReabsorption) as double precision) as inhibitGlucoseKidneyReabsorption,
			cast(sum(lactococcus) as double precision) as lactococcus,
			cast(sum(lactobacillus) as double precision) as lactobacillus,
			cast(sum(leuconostoc) as double precision) as leuconostoc,
			cast(sum(streptococcus) as double precision) as streptococcus,
			cast(sum(bifidobacterium) as double precision)  as bifidobacterium,
			cast(sum(saccharomyces) as double precision) as saccharomyces,
			cast(sum(bacillus) as double precision) as bacillus,
			cast(sum(fructose) as double precision) as fructose,

			cast(sum(glycemicIndex) as double precision) as glycemicIndex,
			cast(sum(saturatedFat) as double precision) as saturatedFat,
			cast(sum(monounsaturatedFat) as double precision) as monounsaturatedFat,
			cast(sum(polyunsaturatedFat) as double precision) as polyunsaturatedFat,
			cast(sum(transFat) as double precision) as transFat,
			cast(sum(cholesterol) as double precision) as cholesterol,
			cast(sum(histidine) as double precision) as histidine,
			cast(sum(isolecuine) as double precision) as isolecuine,
			cast(sum(lysine)as double precision) as lysine,
			cast(sum(methionineAndCysteine) as double precision) as methionineAndCysteine,
			cast(sum(phenylananineAndTyrosine)as double precision) as phenylananineAndTyrosine,
			cast(sum(tryptophan) as double precision) as tryptophan,
			cast(sum(threonine) as double precision) as threonine,
			cast(sum(valine) as double precision) as valine,
			cast(sum(vitaminA) as double precision) as vitaminA,
			cast(sum(vitaminC) as double precision) as vitaminC,
			cast(sum(vitaminD) as double precision) as vitaminD,
			cast(sum(vitaminE) as double precision) as vitaminE,
			cast(sum(vitaminK) as double precision) as vitaminK,
			cast(sum(vitaminB1) as double precision) as vitaminB1,
			cast(sum(vitaminB12) as double precision) as vitaminB12,
			cast(sum(vitaminB2) as double precision) as vitaminB2,
			cast(sum(vitaminB3) as double precision) as vitaminB3,
			cast(sum(vitaminB5) as double precision) as vitaminB5,
			cast(sum(vitaminB6) as double precision) as vitaminB6,
			cast(sum(folate) as double precision) as folate,
			cast(sum(copper) as double precision) as copper,
			cast(sum(iron) as double precision) as iron,
			cast(sum(zinc) as double precision)  as zinc,
			cast(sum(manganese) as double precision) as manganese,
			cast(sum(phosphorus) as double precision) as phosphorus,
			cast(sum(selenium) as double precision) as selenium,

			cast(sum(omega6)/sum(omega3) as double precision) as omega6_3_ratio,
			cast(sum(zinc)/sum(copper) as double precision) as zinc_copper_ratio,

			cast(sum(potassium)/sum(sodium) as decimal) as pot_sod_ratio,

			cast(sum(calcium)/sum(magnesium) as double precision) as cal_mag_ratio,


			cast(sum((0.49 * protein) + (0.037 * phosphorus) - (0.021 * potassium) - (0.026 * magnesium) - (0.013 * calcium)) as double precision) as pralAlkalinity,
			cast(sum(improveBloodPressure) as double precision) as improveBloodPressure,
			cast(sum(improveCholesterol) as double precision) as improveCholesterol,
			cast(sum(reduceWeight) as double precision) as reduceWeight,
			cast(sum(improveRenalFunction) as double precision) as improveRenalFunction,
			cast(sum(improveLiverFunction) as double precision) as improveLiverFunction,
			cast(sum(improveThyroidFunction) as double precision) as improveThyroidFunction,
			cast(sum(improveArthritis) as double precision) as improveArthritis,
			cast(sum(reduceUricAcid) as double precision) as reduceUricAcid,
			cast(sum(veg) as double precision) as veg,
			cast(sum(nonVeg) as double precision)as nonVeg,
			cast(sum(fruit) as double precision)as fruit,
			cast(sum(oil)as double precision) as oil,
			cast(sum(spice) as double precision)as spice,
			cast(sum(grain)as double precision) as grain,
			cast(sum(legume)as double precision) as legume,
			cast(sum(nuts) as double precision)as nuts,
			cast(sum(seeds) as double precision)as seeds,
			cast(sum(inflammatoryIndex) as double precision) as inflammatoryIndex,
			cast(sum(oxidativeStressIndex) as double precision) as oxidativeStressIndex,
			cast(sum(gluten)as double precision) as gluten,

			cast(sum(allergicIndex) as double precision) as allergicIndex,
			cast(sum(water) as double precision) as water
			from 
			(
					select distinct a.clientid, eventdateitz as mealdate, 
					sum(b.net_carb  * fl.measurementAmount) as net_carb,
					sum(b.calories * fl.measurementAmount) as calories,
					sum(b.netGlycemicCarb * fl.measurementAmount) as netGlycemicCarb,
					sum(b.fibre * fl.measurementAmount) as fibre,
					sum(b.fat * fl.measurementAmount) as fat,
					sum(b.protein * fl.measurementAmount) as protein,
					sum(b.Total_Carb * fl.measurementAmount) as Total_Carb,
					sum(sodium * fl.measurementAmount) as sodium,
                    
                    sum(potassium * fl.measurementAmount) as potassium,
					sum(magnesium * fl.measurementAmount) as magnesium,
					sum(calcium * fl.measurementAmount) as calcium,
					sum(chromium * fl.measurementAmount) as chromium,
					sum(omega3 * fl.measurementAmount) as omega3,
					sum(omega6 * fl.measurementAmount) as omega6,
					sum(alphaLipoicAcid * fl.measurementAmount) as alphaLipoicAcid,
					sum(q10 * fl.measurementAmount) as q10,
					sum(biotin * fl.measurementAmount) as biotin,
					sum(flavonoids * fl.measurementAmount) as flavonoids,
					sum(improveInsulinSensitivity * fl.measurementAmount) as improveInsulinSensitivity,
					sum(inhibitGluconeogenis * fl.measurementAmount) as inhibitGluconeogenis,
					sum(inhibitCarbAbsorption * fl.measurementAmount) as inhibitCarbAbsorption,
					sum(improveInsulinSecretion * fl.measurementAmount) as improveInsulinSecretion,
					sum(improveBetaCellRegeneration * fl.measurementAmount) as improveBetaCellRegeneration,
					sum(inhibitHunger * fl.measurementAmount) as inhibitHunger,
					sum(inhibitGlucoseKidneyReabsorption * fl.measurementAmount) as inhibitGlucoseKidneyReabsorption,
					sum(lactococcus * fl.measurementAmount) as lactococcus,
					sum(lactobacillus * fl.measurementAmount) as lactobacillus,
					sum(leuconostoc * fl.measurementAmount) as leuconostoc,
					sum(streptococcus * fl.measurementAmount) as streptococcus,
					sum(bifidobacterium * fl.measurementAmount) as bifidobacterium,
					sum(saccharomyces * fl.measurementAmount) as saccharomyces,
					sum(bacillus * fl.measurementAmount) as bacillus,
					sum(fructose * fl.measurementAmount) as fructose,

					sum(glycemicIndex * fl.measurementAmount) as glycemicIndex,
					sum(saturatedFat * fl.measurementAmount) as saturatedFat,
					sum(monounsaturatedFat * fl.measurementAmount) as monounsaturatedFat,
					sum(polyunsaturatedFat * fl.measurementAmount) as polyunsaturatedFat,
					sum(transFat * fl.measurementAmount) as transFat,
					sum(cholesterol * fl.measurementAmount) as cholesterol,
					sum(histidine * fl.measurementAmount) as histidine,
					sum(isolecuine * fl.measurementAmount) as isolecuine,
					sum(lysine * fl.measurementAmount) as lysine,
					sum(methionineAndCysteine * fl.measurementAmount) as methionineAndCysteine,
					sum(phenylananineAndTyrosine * fl.measurementAmount) as phenylananineAndTyrosine,
					sum(tryptophan * fl.measurementAmount) as tryptophan,
					sum(threonine * fl.measurementAmount) as threonine,
					sum(valine * fl.measurementAmount) as valine,
					sum(vitaminA * fl.measurementAmount) as vitaminA,
					sum(vitaminC * fl.measurementAmount) as vitaminC,
					sum(vitaminD * fl.measurementAmount) as vitaminD,
					sum(vitaminE * fl.measurementAmount) as vitaminE,
					sum(vitaminK * fl.measurementAmount) as vitaminK,
					sum(vitaminB1 * fl.measurementAmount) as vitaminB1,
					sum(vitaminB12 * fl.measurementAmount) as vitaminB12,
					sum(vitaminB2 * fl.measurementAmount) as vitaminB2,
					sum(vitaminB3 * fl.measurementAmount) as vitaminB3,
					sum(vitaminB5 * fl.measurementAmount) as vitaminB5,
					sum(vitaminB6 * fl.measurementAmount) as vitaminB6,
					sum(folate * fl.measurementAmount) as folate,
					sum(copper * fl.measurementAmount) as copper,
					sum(iron * fl.measurementAmount) as iron,
					sum(zinc * fl.measurementAmount) as zinc,
					sum(manganese * fl.measurementAmount) as manganese,
					sum(phosphorus * fl.measurementAmount) as phosphorus,
					sum(selenium * fl.measurementAmount) as selenium,
					sum(omega6_3_ratio * fl.measurementAmount) as omega6_3_ratio,
					sum(zinc_copper_ratio * fl.measurementAmount) as zinc_copper_ratio,
					sum(pot_sod_ratio * fl.measurementAmount) as pot_sod_ratio,
					sum(cal_mag_ratio * fl.measurementAmount) as cal_mag_ratio,
					sum(pralAlkalinity * fl.measurementAmount) as pralAlkalinity,
					sum(improveBloodPressure * fl.measurementAmount) as improveBloodPressure,
					sum(improveCholesterol * fl.measurementAmount) as improveCholesterol,
					sum(reduceWeight * fl.measurementAmount) as reduceWeight,
					sum(improveRenalFunction * fl.measurementAmount) as improveRenalFunction,
					sum(improveLiverFunction * fl.measurementAmount) as improveLiverFunction,
					sum(improveThyroidFunction * fl.measurementAmount) as improveThyroidFunction,
					sum(improveArthritis * fl.measurementAmount) as improveArthritis,
					sum(reduceUricAcid * fl.measurementAmount) as reduceUricAcid,
					sum(veg * fl.measurementAmount) as veg,
					sum(nonVeg * fl.measurementAmount) as nonVeg,
					sum(fruit * fl.measurementAmount) as fruit,
					sum(oil * fl.measurementAmount) as oil,
					sum(spice * fl.measurementAmount) as spice,
					sum(grain * fl.measurementAmount) as grain,
					sum(legume * fl.measurementAmount) as legume,
					sum(nuts * fl.measurementAmount) as nuts,
					sum(seeds * fl.measurementAmount) as seeds,
					sum(inflammatoryIndex * fl.measurementAmount) as inflammatoryIndex,
					sum(oxidativeStressIndex * fl.measurementAmount) as oxidativeStressIndex,
					sum(gluten * fl.measurementAmount) as gluten,

					sum(allergicIndex * fl.measurementAmount) as allergicIndex,
					sum(water * fl.measurementAmount) as water
					from bi_food_supplement_log_detail_RCT fl inner join dim_client a on (fl.clientid = a.clientid and fl.eventdateitz >= enrollmentdate and a.is_row_current = 'y' and isRCT = 'Yes')
									 left join (
														select
														foodid, foodlabel,
														CASE WHEN ISNULL(recommendationRating) THEN 'Purple'
															 WHEN (recommendationRating <= 1) THEN 'Red'
															 WHEN ((recommendationRating > 1) AND (recommendationRating <= 2)) THEN 'Orange'
															 WHEN ((recommendationRating > 2) AND (recommendationRating <= 3)) THEN 'Green'
															 WHEN (recommendationRating > 3) THEN 'Green*'
														END AS E5foodgrading,
														measure,
														measurementType,
														measurementAmount,
														carb - fibre as net_carb 
														,calories, cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as double precision) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre 
														,sodium
                                                        
                                                        ,potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids 
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus 
									,fructose, lactose, glycemicIndex  
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol 
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine 
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  
									,  copper, iron, zinc, manganese, phosphorus, selenium  
									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  
									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  , vegetarian as veg, case when vegetarian = 0 then 1 end as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  
									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  
									, water 
									from foods a
														
											) b on fl.itemid = b.foodid
					where -- eventdateitz between date_sub(date(itz(now())), interval 45 day) and date(itz(now())) and 
					category = 'foodlog'
					group by a.clientid, eventdateitz

					union all 

					select distinct a.clientid, eventdateitz as mealdate, 
					sum((b.net_carb/b.measurementAmount) * fl.quantity) as net_carb,
					sum((b.calories/b.measurementAmount) * fl.quantity) as calories,
					sum((b.netGlycemicCarb/b.measurementAmount) * fl.quantity) as netGlycemicCarb,
					sum((b.fibre/b.measurementAmount) * fl.quantity) as fibre,
					sum((b.fat/b.measurementAmount) * fl.quantity) as fat,
					sum((b.protein/b.measurementAmount) * fl.quantity) as protein,
					sum((b.Total_Carb/b.measurementAmount) * fl.quantity) as Total_Carb,
					sum((sodium/b.measurementAmount) * fl.quantity) as sodium,
                    
                    sum((potassium/b.measurementAmount) * fl.quantity) as potassium,
					sum((magnesium/b.measurementAmount) * fl.quantity) as magnesium,
					sum((calcium/b.measurementAmount) * fl.quantity) as calcium,
					sum((chromium/b.measurementAmount) * fl.quantity) as chromium,
					sum((omega3/b.measurementAmount) * fl.quantity) as omega3,
					sum((omega6/b.measurementAmount) * fl.quantity) as omega6,
					sum((alphaLipoicAcid/b.measurementAmount) * fl.quantity) as alphaLipoicAcid,
					sum((q10/b.measurementAmount) * fl.quantity) as q10,
					sum((biotin/b.measurementAmount) * fl.quantity) as biotin,
					sum((flavonoids/b.measurementAmount) * fl.quantity) as flavonoids,
					sum((improveInsulinSensitivity/b.measurementAmount) * fl.quantity) as improveInsulinSensitivity,
					sum((inhibitGluconeogenis/b.measurementAmount) * fl.quantity) as inhibitGluconeogenis,
					sum((inhibitCarbAbsorption/b.measurementAmount) * fl.quantity) as inhibitCarbAbsorption,
					sum((improveInsulinSecretion/b.measurementAmount) * fl.quantity) as improveInsulinSecretion,
					sum((improveBetaCellRegeneration/b.measurementAmount) * fl.quantity) as improveBetaCellRegeneration,
					sum((inhibitHunger/b.measurementAmount) * fl.quantity) as inhibitHunger,
					sum((inhibitGlucoseKidneyReabsorption/b.measurementAmount) * fl.quantity) as inhibitGlucoseKidneyReabsorption,
					sum((lactococcus/b.measurementAmount) * fl.quantity) as lactococcus,
					sum((lactobacillus/b.measurementAmount) * fl.quantity) as lactobacillus,
					sum((leuconostoc/b.measurementAmount) * fl.quantity) as leuconostoc,
					sum((streptococcus/b.measurementAmount) * fl.quantity) as streptococcus,
					sum((bifidobacterium/b.measurementAmount) * fl.quantity) as bifidobacterium,
					sum((saccharomyces/b.measurementAmount) * fl.quantity) as saccharomyces,
					sum((bacillus/b.measurementAmount) * fl.quantity) as bacillus,
					sum((fructose/b.measurementAmount) * fl.quantity) as fructose,

					sum((glycemicIndex/b.measurementAmount) * fl.quantity) as glycemicIndex,
					sum((saturatedFat/b.measurementAmount) * fl.quantity) as saturatedFat,
					sum((monounsaturatedFat/b.measurementAmount) * fl.quantity) as monounsaturatedFat,
					sum((polyunsaturatedFat/b.measurementAmount) * fl.quantity) as polyunsaturatedFat,
					sum((transFat/b.measurementAmount) * fl.quantity) as transFat,
					sum((cholesterol/b.measurementAmount) * fl.quantity) as cholesterol,
					sum((histidine/b.measurementAmount) * fl.quantity) as histidine,
					sum((isolecuine/b.measurementAmount) * fl.quantity) as isolecuine,
					sum((lysine/b.measurementAmount) * fl.quantity) as lysine,
					sum((methionineAndCysteine/b.measurementAmount) * fl.quantity) as methionineAndCysteine,
					sum((phenylananineAndTyrosine/b.measurementAmount) * fl.quantity) as phenylananineAndTyrosine,
					sum((tryptophan/b.measurementAmount) * fl.quantity) as tryptophan,
					sum((threonine/b.measurementAmount) * fl.quantity) as threonine,
					sum((valine/b.measurementAmount) * fl.quantity) as valine,
					sum((vitaminA/b.measurementAmount) * fl.quantity) as vitaminA,
					sum((vitaminC/b.measurementAmount) * fl.quantity) as vitaminC,
					sum((vitaminD/b.measurementAmount) * fl.quantity) as vitaminD,
					sum((vitaminE/b.measurementAmount) * fl.quantity) as vitaminE,
					sum((vitaminK/b.measurementAmount) * fl.quantity) as vitaminK,
					sum((vitaminB1/b.measurementAmount) * fl.quantity) as vitaminB1,
					sum((vitaminB12/b.measurementAmount) * fl.quantity) as vitaminB12,
					sum((vitaminB2/b.measurementAmount) * fl.quantity) as vitaminB2,
					sum((vitaminB3/b.measurementAmount) * fl.quantity) as vitaminB3,
					sum((vitaminB5/b.measurementAmount) * fl.quantity) as vitaminB5,
					sum((vitaminB6/b.measurementAmount) * fl.quantity) as vitaminB6,
					sum((folate/b.measurementAmount) * fl.quantity) as folate,
					sum((copper/b.measurementAmount) * fl.quantity) as copper,
					sum((iron/b.measurementAmount) * fl.quantity) as iron,
					sum((zinc/b.measurementAmount) * fl.quantity) as zinc,
					sum((manganese/b.measurementAmount) * fl.quantity) as manganese,
					sum((phosphorus/b.measurementAmount) * fl.quantity) as phosphorus,
					sum((selenium/b.measurementAmount) * fl.quantity) as selenium,
					sum(((omega6/b.measurementAmount) / (omega3/b.measurementAmount)) * fl.quantity) as omega6_3_ratio,
					sum(((zinc/b.measurementAmount) / (copper/b.measurementAmount)) * fl.quantity) as zinc_copper_ratio,
					sum(((potassium/b.measurementAmount) / (sodium/b.measurementAmount)) * fl.quantity) as pot_sod_ratio,
					sum(((calcium/b.measurementAmount) / (magnesium/b.measurementAmount)) * fl.quantity) as cal_mag_ratio,
					sum((pralAlkalinity/b.measurementAmount) * fl.quantity) as pralAlkalinity,
					sum((improveBloodPressure/b.measurementAmount) * fl.quantity) as improveBloodPressure,
					sum((improveCholesterol/b.measurementAmount) * fl.quantity) as improveCholesterol,
					sum((reduceWeight/b.measurementAmount) * fl.quantity) as reduceWeight,
					sum((improveRenalFunction/b.measurementAmount) * fl.quantity) as improveRenalFunction,
					sum((improveLiverFunction/b.measurementAmount) * fl.quantity) as improveLiverFunction,
					sum((improveThyroidFunction/b.measurementAmount) * fl.quantity) as improveThyroidFunction,
					sum((improveArthritis/b.measurementAmount) * fl.quantity) as improveArthritis,
					sum((reduceUricAcid/b.measurementAmount) * fl.quantity) as reduceUricAcid,
					sum((veg/b.measurementAmount) * fl.quantity) as veg,
					sum((nonVeg/b.measurementAmount) * fl.quantity) as nonVeg,
					sum((fruit/b.measurementAmount) * fl.quantity) as fruit,
					sum((oil/b.measurementAmount) * fl.quantity) as oil,
					sum((spice/b.measurementAmount) * fl.quantity) as spice,
					sum((grain/b.measurementAmount) * fl.quantity) as grain,
					sum((legume/b.measurementAmount) * fl.quantity) as legume,
					sum((nuts/b.measurementAmount) * fl.quantity) as nuts,
					sum((seeds/b.measurementAmount) * fl.quantity) as seeds,
					sum((inflammatoryIndex/b.measurementAmount) * fl.quantity) as inflammatoryIndex,
					sum((oxidativeStressIndex/b.measurementAmount) * fl.quantity) as oxidativeStressIndex,
					sum((gluten/b.measurementAmount) * fl.quantity) as gluten,

					sum((allergicIndex/b.measurementAmount) * fl.quantity) as allergicIndex,
					sum((water/b.measurementAmount) * fl.quantity) as water
					from bi_food_supplement_log_detail_RCT fl inner join dim_client a on (fl.clientid = a.clientid and fl.eventdateitz >= enrollmentdate and a.is_row_current = 'y' and isRCT = 'Yes')
									 left join (
														select
														supplementId, supplementName,
														'' as E5foodgrading,
														'' as measure,
														measurementType,
														measurementAmount,
														carb - fibre as net_carb 
														,calories, cast(IFNULL((((carb - fibre) * glycemicIndex) / 55),0) as double precision) as netGlycemicCarb, carb as Total_Carb, protein, fat, fibre 
														,sodium
                                                        ,potassium, magnesium, calcium, chromium, omega3, omega6, alphaLipoicAcid, q10, biotin, flavonoids 
									,improveInsulinSensitivity, improveInsulinSecretion, improveBetaCellRegeneration, inhibitGluconeogenis, inhibitCarbAbsorption, inhibitHunger, inhibitGlucoseKidneyReabsorption  
									,lactococcus, lactobacillus, leuconostoc, streptococcus, bifidobacterium, saccharomyces, bacillus 
									,fructose, lactose, glycemicIndex  
									,saturatedFat, monounsaturatedFat, polyunsaturatedFat, transFat, cholesterol 
									,histidine, isolecuine, lysine, methionineAndCysteine, phenylananineAndTyrosine, tryptophan, threonine, valine 
									,vitaminA, vitaminC, vitaminD, vitaminE, vitaminK, vitaminB1, vitaminB12, vitaminB2, vitaminB3, vitaminB5, vitaminB6, folate  
									,  copper, iron, zinc, manganese, phosphorus, selenium  
									, omega6/omega3 as omega6_3_ratio, zinc/copper as zinc_copper_ratio, potassium/sodium as pot_sod_ratio, calcium/magnesium as cal_mag_ratio, pralAlkalinity  
									,improveBloodPressure, improveCholesterol, improveRenalFunction, improveLiverFunction, improveThyroidFunction, improveArthritis, reduceUricAcid, reduceWeight  
									, '' as veg, '' as nonVeg,  fruit, oil, spice, grain, legume,nuts, seeds  
									, inflammatoryIndex, oxidativeStressIndex, gluten, allergicIndex  
									, water 
									from supplements a
														
											) b on fl.itemid = b.supplementId
					where  -- eventdateitz between date_sub(date(itz(now())), interval 45 day) and date(itz(now())) 
					category = 'supplementlog'
					group by a.clientid, eventdateitz
			) s
			group by clientid, mealdate
			;

			create index idx_tmp_1d_RCT on tmp_1d_foodlog_nutrition_target_RCT (clientid asc, mealdate asc, category);

		
			truncate table bi_foodlog_nutrition_target_summary_RCT; 
            
			insert into bi_foodlog_nutrition_target_summary_RCT
			(
			clientid, mealdate, category, net_carb,calories, netGlycemicCarb, fibre, fat, protein, Total_Carb, sodium,
            potassium,
			magnesium,
			calcium,
			chromium,
			omega3,
			omega6,
			alphaLipoicAcid,
			q10,
			biotin,
			flavonoids,
			improveInsulinSensitivity,
			inhibitGluconeogenis,
			inhibitCarbAbsorption,
			improveInsulinSecretion,
			improveBetaCellRegeneration,
			inhibitHunger,
			inhibitGlucoseKidneyReabsorption,
			lactococcus,
			lactobacillus,
			leuconostoc,
			streptococcus,
			bifidobacterium,
			saccharomyces,
			bacillus,
			fructose,
			glycemicIndex,
			saturatedFat,
			monounsaturatedFat,
			polyunsaturatedFat,
			transFat,
			cholesterol,
			histidine,
			isolecuine,
			lysine,
			methionineAndCysteine,
			phenylananineAndTyrosine,
			tryptophan,
			threonine,
			valine,
			vitaminA,
			vitaminC,
			vitaminD,
			vitaminE,
			vitaminK,
			vitaminB1,
			vitaminB12,
			vitaminB2,
			vitaminB3,
			vitaminB5,
			vitaminB6,
			folate,
			copper,
			iron,
			zinc,
			manganese,
			phosphorus,
			selenium,
			omega6_3_ratio,
			zinc_copper_ratio,
			pot_sod_ratio,
			cal_mag_ratio,
			pralAlkalinity,
			improveBloodPressure,
			improveCholesterol,
			reduceWeight,
			improveRenalFunction,
			improveLiverFunction,
			improveThyroidFunction,
			improveArthritis,
			reduceUricAcid,
			veg,
			nonVeg,
			fruit,
			oil,
			spice,
			grain,
			legume,
			nuts,
			seeds,
			inflammatoryIndex,
			oxidativeStressIndex,
			gluten,
			allergicIndex,
			water
			)
			select clientid, mealdate, category, net_carb, calories, netGlycemicCarb, fibre, fat, protein, Total_Carb, sodium,
            potassium,
			magnesium,
			calcium,
			chromium,
			omega3,
			omega6,
			alphaLipoicAcid,
			q10,
			biotin,
			flavonoids,
			improveInsulinSensitivity,
			inhibitGluconeogenis,
			inhibitCarbAbsorption,
			improveInsulinSecretion,
			improveBetaCellRegeneration,
			inhibitHunger,
			inhibitGlucoseKidneyReabsorption,
			lactococcus,
			lactobacillus,
			leuconostoc,
			streptococcus,
			bifidobacterium,
			saccharomyces,
			bacillus,
			fructose,
			glycemicIndex,
			saturatedFat,
			monounsaturatedFat,
			polyunsaturatedFat,
			transFat,
			cholesterol,
			histidine,
			isolecuine,
			lysine,
			methionineAndCysteine,
			phenylananineAndTyrosine,
			tryptophan,
			threonine,
			valine,
			vitaminA,
			vitaminC,
			vitaminD,
			vitaminE,
			vitaminK,
			vitaminB1,
			vitaminB12,
			vitaminB2,
			vitaminB3,
			vitaminB5,
			vitaminB6,
			folate,
			copper,
			iron,
			zinc,
			manganese,
			phosphorus,
			selenium,
			omega6_3_ratio,
			zinc_copper_ratio,
			pot_sod_ratio,
			cal_mag_ratio,
			pralAlkalinity,
			improveBloodPressure,
			improveCholesterol,
			reduceWeight,
			improveRenalFunction,
			improveLiverFunction,
			improveThyroidFunction,
			improveArthritis,
			reduceUricAcid,
			veg,
			nonVeg,
			fruit,
			oil,
			spice,
			grain,
			legume,
			nuts,
			seeds,
			inflammatoryIndex,
			oxidativeStressIndex,
			gluten,
			allergicIndex,
			water
			from tmp_1d_foodlog_nutrition_target_RCT a 
			;
		
		
		    drop temporary table if exists tmp_7d_foodlog_nutrition_target_RCT;

			create temporary table tmp_7d_foodlog_nutrition_target_RCT
			as 
			select a.clientid, a.mealdate, '7d-Nutrient' as category, 
			cast(avg(b.net_carb) as double precision) as net_carb,
			cast(avg(b.calories) as double precision) as calories,
			cast(avg(b.netGlycemicCarb) as double precision) as netGlycemicCarb,
			cast(avg(b.fibre) as double precision) as fibre,
			cast(avg(b.fat) as double precision) as fat,
			cast(avg(b.protein) as double precision) as protein,
			cast(avg(b.Total_Carb) as double precision) as Total_Carb,
			cast(avg(b.sodium) as double precision) as sodium,
            
            cast(avg(b.potassium) as double precision) as potassium,
			cast(avg(b.magnesium) as double precision) as magnesium,
			cast(avg(b.calcium) as double precision) as calcium,
			cast(avg(b.chromium) as double precision) as chromium,
			cast(avg(b.omega3) as double precision) as omega3,
			cast(avg(b.omega6) as double precision) as omega6,
			cast(avg(b.alphaLipoicAcid) as double precision) as alphaLipoicAcid,
			cast(avg(b.q10) as double precision) as q10,
			cast(avg(b.biotin) as double precision) as biotin,
			cast(avg(b.flavonoids) as double precision) as flavonoids,
			cast(avg(b.improveInsulinSensitivity) as double precision) as improveInsulinSensitivity,
			cast(avg(b.inhibitGluconeogenis) as double precision) as inhibitGluconeogenis,
			cast(avg(b.inhibitCarbAbsorption) as double precision) as inhibitCarbAbsorption,
			cast(avg(b.improveInsulinSecretion) as double precision) as improveInsulinSecretion,
			cast(avg(b.improveBetaCellRegeneration) as double precision) as improveBetaCellRegeneration,
			cast(avg(b.inhibitHunger) as double precision) as inhibitHunger,
			cast(avg(b.inhibitGlucoseKidneyReabsorption) as double precision) as inhibitGlucoseKidneyReabsorption,
			cast(avg(b.lactococcus) as double precision) as lactococcus,
			cast(avg(b.lactobacillus) as double precision) as lactobacillus,
			cast(avg(b.leuconostoc) as double precision) as leuconostoc,
			cast(avg(b.streptococcus) as double precision) as streptococcus,
			cast(avg(b.bifidobacterium) as double precision) as bifidobacterium,
			cast(avg(b.saccharomyces) as double precision) as saccharomyces,
			cast(avg(b.bacillus) as double precision) as bacillus,
			cast(avg(b.fructose) as double precision) as fructose,

			cast(avg(b.glycemicIndex) as double precision) as glycemicIndex,
			cast(avg(b.saturatedFat) as double precision) as saturatedFat,
			cast(avg(b.monounsaturatedFat) as double precision) as monounsaturatedFat,
			cast(avg(b.polyunsaturatedFat) as double precision) as polyunsaturatedFat,
			cast(avg(b.transFat) as double precision) as transFat,
			cast(avg(b.cholesterol) as double precision) as cholesterol,
			cast(avg(b.histidine) as double precision) as histidine,
			cast(avg(b.isolecuine) as double precision) as isolecuine,
			cast(avg(b.lysine) as double precision) as lysine,
			cast(avg(b.methionineAndCysteine) as double precision) as methionineAndCysteine,
			cast(avg(b.phenylananineAndTyrosine) as double precision) as phenylananineAndTyrosine,
			cast(avg(b.tryptophan) as double precision) as tryptophan,
			cast(avg(b.threonine) as double precision) as threonine,
			cast(avg(b.valine) as double precision) as valine,
			cast(avg(b.vitaminA) as double precision) as vitaminA,
			cast(avg(b.vitaminC) as double precision) as vitaminC,
			cast(avg(b.vitaminD) as double precision) as vitaminD,
			cast(avg(b.vitaminE) as double precision) as vitaminE,
			cast(avg(b.vitaminK) as double precision) as vitaminK,
			cast(avg(b.vitaminB1) as double precision) as vitaminB1,
			cast(avg(b.vitaminB12) as double precision) as vitaminB12,
			cast(avg(b.vitaminB2) as double precision) as vitaminB2,
			cast(avg(b.vitaminB3) as double precision) as vitaminB3,
			cast(avg(b.vitaminB5) as double precision) as vitaminB5,
			cast(avg(b.vitaminB6) as double precision) as vitaminB6,
			cast(avg(b.folate) as double precision) as folate,
			cast(avg(b.copper) as double precision) as copper,
			cast(avg(b.iron) as double precision) as iron,
			cast(avg(b.zinc) as double precision) as zinc,
			cast(avg(b.manganese) as double precision) as manganese,
			cast(avg(b.phosphorus) as double precision) as phosphorus,
			cast(avg(b.selenium) as double precision) as selenium,
			cast(avg(b.omega6_3_ratio) as double precision) as omega6_3_ratio,
			cast(avg(b.zinc_copper_ratio) as double precision) as zinc_copper_ratio,
			cast(avg(b.pot_sod_ratio) as double precision) as pot_sod_ratio,
			cast(avg(b.cal_mag_ratio) as double precision) as cal_mag_ratio,
			cast(avg(b.pralAlkalinity) as double precision) as pralAlkalinity,
			cast(avg(b.improveBloodPressure) as double precision) as improveBloodPressure,
			cast(avg(b.improveCholesterol) as double precision) as improveCholesterol,
			cast(avg(b.reduceWeight) as double precision) as reduceWeight,
			cast(avg(b.improveRenalFunction) as double precision) as improveRenalFunction,
			cast(avg(b.improveLiverFunction) as double precision) as improveLiverFunction,
			cast(avg(b.improveThyroidFunction) as double precision) as improveThyroidFunction,
			cast(avg(b.improveArthritis) as double precision) as improveArthritis,
			cast(avg(b.reduceUricAcid) as double precision) as reduceUricAcid,
			cast(avg(b.veg) as double precision) as veg,
			cast(avg(b.nonVeg) as double precision) as nonVeg,
			cast(avg(b.fruit) as double precision) as fruit,
			cast(avg(b.oil) as double precision) as oil,
			cast(avg(b.spice) as double precision) as spice,
			cast(avg(b.grain) as double precision) as grain,
			cast(avg(b.legume) as double precision) as legume,
			cast(avg(b.nuts) as double precision) as nuts,
			cast(avg(b.seeds) as double precision) as seeds,
			cast(avg(b.inflammatoryIndex) as double precision) as inflammatoryIndex,
			cast(avg(b.oxidativeStressIndex) as double precision) as oxidativeStressIndex,
			cast(avg(b.gluten) as double precision) as gluten,

			cast(avg(b.allergicIndex) as double precision) as allergicIndex,
			cast(avg(b.water) as double precision) as water
						
			from bi_foodlog_nutrition_target_summary_RCT a inner join tmp_1d_foodlog_nutrition_target_RCT b 
			on (a.clientid = b.clientid and a.category = b.category and a.category = '1d-nutrient' and b.mealdate between date_sub(a.mealdate, interval 6 day) and a.mealdate)
			-- where a.mealdate between date_sub(date(itz(now())), interval 39 day) and date(itz(now()))
			group by a.clientid, a.mealdate
			;

			insert into bi_foodlog_nutrition_target_summary_RCT
			select * from tmp_7d_foodlog_nutrition_target_RCT
			;

			drop temporary table tmp_1d_foodlog_nutrition_target_RCT
			;

			drop temporary table tmp_7d_foodlog_nutrition_target_RCT
			;


update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_food_supplement_log_detail_RCT'
;

END;
