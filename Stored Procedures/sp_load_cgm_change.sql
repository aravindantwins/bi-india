CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_cgm_change`(  )
BEGIN


update dailyProcessLog
set startDate = now()
where processName = 'sp_load_cgm_change'
;

set time_zone = 'Asia/calcutta';

DROP TEMPORARY TABLE if exists temp_stage;


set @dateToRun := (select cast(date_sub(date(now()), interval 21 day) as datetime));

CREATE TEMPORARY TABLE TEMP_STAGE AS 
SELECT  A.CLIENTID, 
		A.EVENTTIME AS LAST_EVENT_TIME,  
        B.EVENTTIME AS FOLLOWING_EVENT_TIME, 
        TIME(A.EVENTTIME) AS CHANGE_TIME,
		TIMESTAMPDIFF(SECOND, A.EVENTTIME, B.EVENTTIME)  AS GAP_IN_SECONDS 
FROM 
(
		SELECT 
		CLIENTID, EVENTTIME, ROW_NUMBER() OVER(PARTITION BY CLIENTID ORDER BY EVENTTIME ASC) AS ROWNUMBER
		FROM TWINS.STAGINGGLUCOSEVALUES
		WHERE EVENTTIME >= @DATETORUN AND EVENTTIME <= NOW()
) A, 
(
		SELECT 
		CLIENTID, EVENTTIME, ROW_NUMBER() OVER(PARTITION BY CLIENTID ORDER BY EVENTTIME ASC) AS ROWNUMBER
		FROM TWINS.STAGINGGLUCOSEVALUES
		WHERE EVENTTIME >= @DATETORUN AND EVENTTIME <= NOW()
) B
WHERE A.CLIENTID= B.CLIENTID
AND B.ROWNUMBER = (A.ROWNUMBER + 1)
AND TIMESTAMPDIFF(SECOND, A.EVENTTIME, B.EVENTTIME) > 1200
;


CREATE INDEX IDX_TEMP_STAGE ON TEMP_STAGE (CLIENTID ASC, LAST_EVENT_TIME); 

-- Remove the data loaded already for the last 2 weeks. this is because in India - abbott reader does delete the last 2 weeks of CGM files and reloads them back. 
-- If a corrupt file came in before, this delete/reload action would leave the correct data in stagingglucosevalues. 
-- but not in cgmchange as we are doing Insert new rows method. Dont want to do truncate and load either to avoid historical data loss. 

DELETE FROM CGMCHANGE WHERE CHANGEDATE >= DATE_SUB(DATE(NOW()), INTERVAL 15 DAY);

INSERT INTO CGMCHANGE(CLIENTID, CHANGEDATE, CHANGETIME)
SELECT S.CLIENTID,
       DATE(LAST_EVENT_TIME) AS CHANGEDATE,
       CHANGE_TIME
FROM TEMP_STAGE S, v_client_tmp A
WHERE S.CLIENTID = A.CLIENTID
AND NOT EXISTS 
(SELECT 1 FROM CGMCHANGE C 
WHERE S.CLIENTID = C.CLIENTID AND DATE(S.LAST_EVENT_TIME) = C.CHANGEDATE)
AND DATE(S.LAST_EVENT_TIME) >= DATE_SUB(DATE(NOW()), INTERVAL 15 DAY)
;


DROP TEMPORARY TABLE IF EXISTS TEMP_STAGE
;

set time_zone = 'UTC';


update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_cgm_change'
;

END