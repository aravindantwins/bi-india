CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_twins_rct_health_output_measures`(  )
BEGIN





update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_twins_rct_health_output_measures'
;



drop temporary table if exists tmp_rct_patient_lab_dates;

create temporary table tmp_rct_patient_lab_dates

as
select distinct d1.clientid ,  d1.cohort,
d1.day0,
d1.day10,
d1.day26,
d1.day55,
d1.day56,
d1.day85,
d1.day86,
d1.day115,
d1.day116,
d1.day145,
d1.day146,
d1.day175,
d1.day176,
d1.day205,
d1.day206,
d1.day235,
d1.day236,
d1.day265,
d1.day266,
d1.day295,
d1.day296,
d1.day325,
d1.day326,
d1.day355,
d1.day356,
d1.day385,
d1.day386,
d1.day415,
d1.day416,
d1.day445,
d1.day446,
d1.day475,
d1.day476,
d1.day505,
d1.day506,
d1.day535,
d1.day536,
d1.day565,
d1.day566,
d1.day595,
d1.day596,
d1.day625,
d1.day626,
d1.day655,
d1.day656,
d1.day685,
d1.day686,
d1.day715,
d1.day716,
d1.day745,

d1.firstBloodWorkDate as D0_labdate, d2.D30_labdate, d3.D60_labdate, d4.D90_labdate,
d5.D120_labdate, d6.D150_labdate, d7.D180_labdate, d8.D210_labdate, d9.D240_labdate,
d10.D270_labdate, d11.D300_labdate, d12.D330_labdate, d13.D360_labdate,d14.D390_labdate,
d15.D420_labdate,d16.D450_labdate,d17.D480_labdate,d18.D510_labdate,d19.D540_labdate,
d20.D570_labdate,d21.D600_labdate,d22.D630_labdate,d23.D660_labdate,d24.D690_labdate,
d25.D720_labdate,

d1.sensor_day1,
d1.sensor_day4,
d1.sensor_day5,
d1.sensor_day6,
d1.sensor_day7,
d1.sensor_day10,
d1.sensor_day24,
d1.sensor_day26,
d1.sensor_day30,
d1.sensor_day84,
d1.sensor_day86,
d1.sensor_day90,
d1.sensor_day114,
d1.sensor_day116,
d1.sensor_day120,
d1.sensor_day144,
d1.sensor_day146,
d1.sensor_day150,
d1.sensor_day174,
d1.sensor_day176,
d1.sensor_day180,
d1.sensor_day264,
d1.sensor_day266,
d1.sensor_day270,
d1.sensor_day354,
d1.sensor_day356,
d1.sensor_day360,

d1.sensor_day384,
d1.sensor_day386,
d1.sensor_day390,
d1.sensor_day414,
d1.sensor_day416,
d1.sensor_day420,
d1.sensor_day444,
d1.sensor_day446,
d1.sensor_day450,
d1.sensor_day474,
d1.sensor_day476,
d1.sensor_day480,
d1.sensor_day504,
d1.sensor_day506,
d1.sensor_day510,
d1.sensor_day534,
d1.sensor_day536,
d1.sensor_day540,
d1.sensor_day564,
d1.sensor_day566,
d1.sensor_day570,
d1.sensor_day594,
d1.sensor_day596,
d1.sensor_day600,
d1.sensor_day624,
d1.sensor_day626,
d1.sensor_day630,
d1.sensor_day654,
d1.sensor_day656,
d1.sensor_day660,
d1.sensor_day684,
d1.sensor_day686,
d1.sensor_day690,
d1.sensor_day714,
d1.sensor_day716,
d1.sensor_day720

  from
	(	
	select distinct b.clientid, a.cohort, firstBloodWorkDate,
    firstBloodWorkDate as day0,
	date_add(sensor_day1, interval 9 day) as day10,
    date_add(sensor_day1, interval 25 day) as day26,
	date_add(sensor_day1, interval 54 day) as day55,
    date_add(sensor_day1, interval 55 day) as day56,
    date_add(sensor_day1, interval 84 day) as day85,
    date_add(sensor_day1, interval 85 day) as day86,
	date_add(sensor_day1, interval 114 day) as day115,
    date_add(sensor_day1, interval 115 day) as day116,
	date_add(sensor_day1, interval 144 day) as day145,
    date_add(sensor_day1, interval 145 day) as day146,
	date_add(sensor_day1, interval 174 day) as day175,
    date_add(sensor_day1, interval 175 day) as day176,
	date_add(sensor_day1, interval 204 day) as day205,
    date_add(sensor_day1, interval 205 day) as day206,
	date_add(sensor_day1, interval 234 day) as day235,
    date_add(sensor_day1, interval 235 day) as day236,
	date_add(sensor_day1, interval 264 day) as day265,
	date_add(sensor_day1, interval 265 day) as day266,
	date_add(sensor_day1, interval 294 day) as day295,
    date_add(sensor_day1, interval 295 day) as day296,
	date_add(sensor_day1, interval 324 day) as day325,
    date_add(sensor_day1, interval 325 day) as day326,
	date_add(sensor_day1, interval 354 day) as day355,
    date_add(sensor_day1, interval 355 day) as day356,
	date_add(sensor_day1, interval 384 day) as day385,
    date_add(sensor_day1, interval 385 day) as day386,
date_add(sensor_day1, interval 414 day) as day415,
    date_add(sensor_day1, interval 415 day) as day416,
date_add(sensor_day1, interval 444 day) as day445,
    date_add(sensor_day1, interval 445 day) as day446,
date_add(sensor_day1, interval 474 day) as day475,
    date_add(sensor_day1, interval 475 day) as day476,
date_add(sensor_day1, interval 504 day) as day505,
    date_add(sensor_day1, interval 505 day) as day506,
date_add(sensor_day1, interval 534 day) as day535,
    date_add(sensor_day1, interval 535 day) as day536,
date_add(sensor_day1, interval 564 day) as day565,
    date_add(sensor_day1, interval 565 day) as day566,
date_add(sensor_day1, interval 594 day) as day595,
    date_add(sensor_day1, interval 595 day) as day596,
date_add(sensor_day1, interval 624 day) as day625,
    date_add(sensor_day1, interval 625 day) as day626,
date_add(sensor_day1, interval 654 day) as day655,
    date_add(sensor_day1, interval 655 day) as day656,
date_add(sensor_day1, interval 684 day) as day685,
    date_add(sensor_day1, interval 685 day) as day686,
date_add(sensor_day1, interval 714 day) as day715,
    date_add(sensor_day1, interval 715 day) as day716,
    date_add(sensor_day1, interval 744 day) as day745,
	a.sensor_day1,
	date_add(sensor_day1, interval 3 day) as sensor_day4,
    date_add(sensor_day1, interval 4 day) as sensor_day5,
	date_add(sensor_day1, interval 5 day) as sensor_day6,
    date_add(sensor_day1, interval 6 day) as sensor_day7,
	date_add(sensor_day1, interval 9 day) as sensor_day10,
	date_add(sensor_day1, interval 22 day) as sensor_day24,
	date_add(sensor_day1, interval 25 day) as sensor_day26,
	date_add(sensor_day1, interval 29 day) as sensor_day30,
	date_add(sensor_day1, interval 82 day) as sensor_day84,
	date_add(sensor_day1, interval 85 day) as sensor_day86,
	date_add(sensor_day1, interval 89 day) as sensor_day90,
	date_add(sensor_day1, interval 112 day) as sensor_day114,
	date_add(sensor_day1, interval 115 day) as sensor_day116,
	date_add(sensor_day1, interval 119 day) as sensor_day120,
	date_add(sensor_day1, interval 142 day) as sensor_day144,
	date_add(sensor_day1, interval 145 day) as sensor_day146,
	date_add(sensor_day1, interval 149 day) as sensor_day150,
	date_add(sensor_day1, interval 172 day) as sensor_day174,
	date_add(sensor_day1, interval 175 day) as sensor_day176,
	date_add(sensor_day1, interval 179 day) as sensor_day180,
	date_add(sensor_day1, interval 262 day) as sensor_day264,
	date_add(sensor_day1, interval 265 day) as sensor_day266,
	date_add(sensor_day1, interval 269 day) as sensor_day270,
	date_add(sensor_day1, interval 352 day) as sensor_day354,
	date_add(sensor_day1, interval 355 day) as sensor_day356,
	date_add(sensor_day1, interval 359 day) as sensor_day360,
    date_add(sensor_day1, interval 382 day) as sensor_day384,
date_add(sensor_day1, interval 385 day) as sensor_day386,
date_add(sensor_day1, interval 389 day) as sensor_day390,
date_add(sensor_day1, interval 412 day) as sensor_day414,
date_add(sensor_day1, interval 415 day) as sensor_day416,
date_add(sensor_day1, interval 419 day) as sensor_day420,
date_add(sensor_day1, interval 442 day) as sensor_day444,
date_add(sensor_day1, interval 445 day) as sensor_day446,
date_add(sensor_day1, interval 449 day) as sensor_day450,
date_add(sensor_day1, interval 472 day) as sensor_day474,
date_add(sensor_day1, interval 475 day) as sensor_day476,
date_add(sensor_day1, interval 479 day) as sensor_day480,
date_add(sensor_day1, interval 502 day) as sensor_day504,
date_add(sensor_day1, interval 505 day) as sensor_day506,
date_add(sensor_day1, interval 509 day) as sensor_day510,
date_add(sensor_day1, interval 532 day) as sensor_day534,
date_add(sensor_day1, interval 535 day) as sensor_day536,
date_add(sensor_day1, interval 539 day) as sensor_day540,
date_add(sensor_day1, interval 562 day) as sensor_day564,
date_add(sensor_day1, interval 565 day) as sensor_day566,
date_add(sensor_day1, interval 569 day) as sensor_day570,
date_add(sensor_day1, interval 592 day) as sensor_day594,
date_add(sensor_day1, interval 595 day) as sensor_day596,
date_add(sensor_day1, interval 599 day) as sensor_day600,
date_add(sensor_day1, interval 622 day) as sensor_day624,
date_add(sensor_day1, interval 625 day) as sensor_day626,
date_add(sensor_day1, interval 629 day) as sensor_day630,
date_add(sensor_day1, interval 652 day) as sensor_day654,
date_add(sensor_day1, interval 655 day) as sensor_day656,
date_add(sensor_day1, interval 659 day) as sensor_day660,
date_add(sensor_day1, interval 682 day) as sensor_day684,
date_add(sensor_day1, interval 685 day) as sensor_day686,
date_add(sensor_day1, interval 689 day) as sensor_day690,
date_add(sensor_day1, interval 712 day) as sensor_day714,
date_add(sensor_day1, interval 715 day) as sensor_day716,
date_add(sensor_day1, interval 719 day) as sensor_day720 	                                                         
	from

          (
		  select clientid, status, treatmentDays,  "TWIN_PRIME" as cohort,
			min(date(firstBloodWorkDateITZ)) as firstBloodWorkDate, min(programStartDate_analytics) as sensor_day1
			from dim_client
            where upper(status) in ('ACTIVE')
            and isRCT_Prime='Yes'
            and  treatmentDays >= 1
            and is_row_current = 'y'
            group by clientid having min(firstBloodWorkDateITZ) is not null
            union all
	
            select clientid, status, treatmentDays,  "TWIN_PRIME" as cohort,
			min(date(firstBloodWorkDateITZ)) as firstBloodWorkDate,  min(programStartDate_analytics) as sensor_day1
			from dim_client
            where upper(status) in ('DISCHARGED','INACTIVE')
             and isRCT_Prime='Yes'
            and  datediff(latestBloodWorkDateITZ, firstBloodWorkDateITZ) > 25
            and is_row_current = 'y'
            group by clientid having min(firstBloodWorkDateITZ) is not null
            union all
	
		  select a.*, b.sensor_day1 from
           (
           select clientid, status, treatmentDays,  "TWIN_CONTROL" as cohort,
           min(date(firstBloodWorkDateITZ)) as firstBloodWorkDate
			from dim_client
            where upper(status) in ('ACTIVE' , 'REGISTRATION' , 'PENDING_ACTIVE')
            and isRCT_Control='Yes'
            and  treatmentDays >= 1
            and is_row_current = 'y'
            group by clientid having min(firstBloodWorkDateITZ) is not null
		           
            union all
	
            select clientid, status, treatmentDays, "TWIN_CONTROL" as cohort ,
            min(date(firstBloodWorkDateITZ)) as firstBloodWorkDate
			from dim_client
            where upper(status) in ('DISCHARGED','INACTIVE')
             and isRCT_Control='Yes'
            and  datediff(latestBloodWorkDateITZ, firstBloodWorkDateITZ) > 25
            and is_row_current = 'y'
            group by clientid having min(firstBloodWorkDateITZ) is not null ) a
            inner join (select clientid, min(case when cgm_1d is not null then date else null end) as sensor_day1
            from bi_rct_patient_measures_x_preactivation group by clientid) b
            on a.clientid=b.clientid
			)a left join clinictestresults  b on a.clientid = b.clientid
             WHERE sensor_day1 IS NOT NULL) d1
	 left join (select distinct clientid, date(itz(bloodworktime)) as D30_labdate from clinictestresults where deleted = 0) d2 on d1.clientid = d2.clientid and (d2.D30_labdate between d1.day26 and d1.day55)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D60_labdate from clinictestresults where deleted = 0) d3 on d1.clientid = d3.clientid and (d3.D60_labdate between d1.day56 and d1.day85)
     left join (select distinct clientid, date(itz(bloodworktime)) as D90_labdate from clinictestresults where deleted = 0) d4 on d1.clientid = d4.clientid and (d4.D90_labdate between d1.day86 and d1.day115)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D120_labdate from clinictestresults where deleted = 0) d5 on d1.clientid = d5.clientid and (d5.D120_labdate between d1.day116 and d1.day145)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D150_labdate from clinictestresults where deleted = 0) d6 on d1.clientid = d6.clientid and (d6.D150_labdate between d1.day146 and d1.day175)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D180_labdate from clinictestresults where deleted = 0) d7 on d1.clientid = d7.clientid and (d7.D180_labdate between d1.day176 and d1.day205)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D210_labdate from clinictestresults where deleted = 0) d8 on d1.clientid = d8.clientid and (d8.D210_labdate between d1.day206 and d1.day235)
     left join (select distinct clientid, date(itz(bloodworktime)) as D240_labdate from clinictestresults where deleted = 0) d9 on d1.clientid = d9.clientid and (d9.D240_labdate between d1.day236 and d1.day265)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D270_labdate from clinictestresults where deleted = 0) d10 on d1.clientid = d10.clientid and (d10.D270_labdate between d1.day266 and d1.day295)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D300_labdate from clinictestresults where deleted = 0) d11 on d1.clientid = d11.clientid and (d11.D300_labdate between d1.day296 and d1.day325)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D330_labdate from clinictestresults where deleted = 0) d12 on d1.clientid = d12.clientid and (d12.D330_labdate between d1.day326 and d1.day355)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D360_labdate from clinictestresults where deleted = 0) d13 on d1.clientid = d13.clientid and (d13.D360_labdate between d1.day356 and d1.day385)
	 left join (select distinct clientid, date(itz(bloodworktime)) as D390_labdate from clinictestresults where deleted = 0) d14 on d1.clientid = d14.clientid and (d14.D390_labdate between d1.day386 and d1.day415)
     left join (select distinct clientid, date(itz(bloodworktime)) as D420_labdate from clinictestresults where deleted = 0) d15 on d1.clientid = d15.clientid and (d15.D420_labdate between d1.day416 and d1.day445)
     left join (select distinct clientid, date(itz(bloodworktime)) as D450_labdate from clinictestresults where deleted = 0) d16 on d1.clientid = d16.clientid and (d16.D450_labdate between d1.day446 and d1.day475)
     left join (select distinct clientid, date(itz(bloodworktime)) as D480_labdate from clinictestresults where deleted = 0) d17 on d1.clientid = d17.clientid and (d17.D480_labdate between d1.day476 and d1.day505)
     left join (select distinct clientid, date(itz(bloodworktime)) as D510_labdate from clinictestresults where deleted = 0) d18 on d1.clientid = d18.clientid and (d18.D510_labdate between d1.day506 and d1.day535)
     left join (select distinct clientid, date(itz(bloodworktime)) as D540_labdate from clinictestresults where deleted = 0) d19 on d1.clientid = d19.clientid and (d19.D540_labdate between d1.day536 and d1.day565)
     left join (select distinct clientid, date(itz(bloodworktime)) as D570_labdate from clinictestresults where deleted = 0) d20 on d1.clientid = d20.clientid and (d20.D570_labdate between d1.day566 and d1.day595)
     left join (select distinct clientid, date(itz(bloodworktime)) as D600_labdate from clinictestresults where deleted = 0) d21 on d1.clientid = d21.clientid and (d21.D600_labdate between d1.day596 and d1.day625)  
	 left join (select distinct clientid, date(itz(bloodworktime)) as D630_labdate from clinictestresults where deleted = 0) d22 on d1.clientid = d22.clientid and (d22.D630_labdate between d1.day626 and d1.day655)
     left join (select distinct clientid, date(itz(bloodworktime)) as D660_labdate from clinictestresults where deleted = 0) d23 on d1.clientid = d23.clientid and (d23.D660_labdate between d1.day656 and d1.day685)
     left join (select distinct clientid, date(itz(bloodworktime)) as D690_labdate from clinictestresults where deleted = 0) d24 on d1.clientid = d24.clientid and (d24.D690_labdate between d1.day686 and d1.day715)
     left join (select distinct clientid, date(itz(bloodworktime)) as D720_labdate from clinictestresults where deleted = 0) d25 on d1.clientid = d25.clientid and (d25.D720_labdate between d1.day716 and d1.day745)
;
     
create index idx_tmp_rct_patient_lab_dates on tmp_rct_patient_lab_dates(clientid asc, D0_labdate, day0); 

 
drop temporary table if exists tmp_rct_lab_test_output;
create temporary table tmp_rct_lab_test_output as 
select b.clientid, b.bloodworkdate, b.category, cast(b.measure as decimal(10,4)) as measure from 
(select clientid from tmp_rct_patient_lab_dates) a, 
(
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'HbA1c ' as category, avg(veinhba1c) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by a.clientid, date(itz(bloodworktime)) union all  
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'HDL' as category, avg(hdlCholesterol) as  measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all 
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'LDL' as category, avg(ldlCholesterol) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all 
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Triglycerides(TG)' as category, avg(triglycerides) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'TG/HDL' as category, cast(avg(triglycerides/hdlCholesterol) as decimal(10,2)) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all 
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ALT' as category, avg(alt) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'GGT' as category, avg(gammaGlutamylTransferase) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'TSH' as category, avg(tsh) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Testosterone' as category, avg(testosterone) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime))union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Homa2B' as category, avg(homa2B) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime))union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Homa2IR' as category, avg(homa2IR) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime))union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'hsCRP' as category, avg(hsCReactive) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime))union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Urine Microalbumin' as category, avg(urineMicroalbumin) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime))union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'eGFR' as category, avg(glomerularFiltrationRate) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'LDL/apoB' as category, cast(avg(ldlCholesterol/apoB) as decimal(10,2)) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime))  union all 
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'apoB' as category, avg(apoB) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'serumCreatine' as category, avg(serumCreatine) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'fasting_insulin' as category, avg(insulin) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'waist Circumference' as category, avg(waistCircumference) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'platelets' as category, avg(platelets) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'albumin' as category, avg(albumin) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'cPeptide' as category, avg(cPeptide) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'redCellDistributionWidth' as category, avg(redCellDistributionWidth) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ast' as category, avg(ast) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'cholesterol' as category, avg(cholesterol) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Sys BP' as category, avg(systolicBP) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT_Control = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Dia BP' as category, avg(diastolicBP) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT_Control = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Clinic_Sys BP' as category, avg(systolicBP) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Clinic_Dia BP' as category, avg(diastolicBP) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'BMI' as category, avg(a.weight/(a.height*a.height)) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT_Control = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all 
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'smoking' as category, avg(useSmoking) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ast/Alt' as category, avg(astAltRatio) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'lipoproteinA' as category, avg(lipoproteinA) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'glucose' as category, avg(glucose) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ketones' as category, avg(ketones) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'cortisol' as category, avg(cortisol) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'cystatinC' as category, avg(cystatinC) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'homa2S' as category, avg(homa2S) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ntProBnp' as category, avg(ntProBnp) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ferritin' as category, avg(ferritin) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'esr' as category, avg(esr) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'whiteBloodCellCount' as category, avg(whiteBloodCells) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'weight' as category, avg(weight) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT_Control = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all 
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'neutrophilsCount' as category, avg(neutrophils) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by a.clientid, date(itz(bloodworktime)) union all 

select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'sdLDL' as category, cast(avg(0.580*(cholesterol-hdlCholesterol)+0.407*ldlCholesterol-0.719*(cholesterol-hdlCholesterol-(triglycerides/5))-12.05) as decimal(10,2)) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'apoA1' as category, avg(apoA1) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'LpPla2' as category, avg(lpPla2) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'VLDL' as category, avg(vldlCholesterol) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Non-HDL_cholesterol' as category, cast(avg(cholesterol-hdlCholesterol) as decimal(10,2)) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Calculated_LDL' as category, cast(avg(cholesterol-hdlCholesterol-(triglycerides/5)) as decimal(10,2)) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all

select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'Alcohol' as category, avg(useAlcohol) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'glutathione' as category, avg(glutathione) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'malondialdehyde' as category, avg(malondialdehyde) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'nitricOxide' as category, avg(nitricOxide) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'endothelin' as category, avg(endothelin) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'vcam' as category, avg(vcam) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'sodium' as category, avg(sodium) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'potassium' as category, avg(potassium) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all

select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_systolic24hrAVG' as category, avg(systolic24hrAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_diastolic24hrAVG' as category, avg(diastolic24hrAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_systolicDayTimeAVG' as category, avg(systolicDayTimeAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_diastolicDayTimeAVG' as category, avg(diastolicDayTimeAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_systolicNightTimeAVG' as category, avg(systolicNightTimeAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_diastolicNightTimeAVG' as category, avg(diastolicNightTimeAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_systolicEarlyMorningAVG' as category, avg(systolicEarlyMorningAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select a.clientid, date(itz(bloodworktime)) as bloodworkdate, 'ABPM_diastolicEarlyMorningAVG' as category, avg(diastolicEarlyMorningAverage) as measure from clinictestresults a inner join dim_client b on a.clientid = b.clientid where b.isRCT = 'Yes' and a.deleted = 0 group by clientid, date(itz(bloodworktime)) union all
select b.clientid, date(itz(b.bloodworktime)) as bloodworkdate, 'TIMP' as category, avg(tissueInhibitorMetalloproteinase) as measure from clinicreportnafld a , clinictestresults b , dim_client c where   a.clinicReportNafldId = b.clinicReportNafldId and c.clientid = b.clientid and c.isRCT = 'Yes'  group by clientid, date(itz(bloodworktime)) union all
select b.clientid, date(itz(b.bloodworktime)) as bloodworkdate, 'P3NP' as category, avg(proCollagenPeptide) as measure from clinicreportnafld a , clinictestresults b , dim_client c where   a.clinicReportNafldId = b.clinicReportNafldId and c.clientid = b.clientid and c.isRCT = 'Yes'  group by clientid, date(itz(bloodworktime))  union all
select b.clientid, date(itz(b.bloodworktime)) as bloodworkdate, 'serumRenin' as category, avg(serumRenin) as measure from clinicreportnafld a , clinictestresults b , dim_client c where   a.clinicReportNafldId = b.clinicReportNafldId and c.clientid = b.clientid and c.isRCT = 'Yes'  group by clientid, date(itz(bloodworktime))  



) b where bloodworkdate is not null and a.clientid=b.clientid;

create index idx_tmp_rct_lab_test_output on tmp_rct_lab_test_output(clientid asc, bloodworkdate asc, category, measure); 



drop temporary table if exists tmp_rct_output_score_data;
create temporary table tmp_rct_output_score_data
(
	clientid int null,
	date date not null,
	date_category varchar(7) null,
	category varchar(100) null
);

insert into tmp_rct_output_score_data(clientid, date, date_category, category)
select a.clientid, case when a.bloodworkdate = b.D0_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D30_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D60_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D90_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D120_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D150_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D180_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D210_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D240_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D270_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D300_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D330_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D360_labdate then a.bloodworkdate 
					    when a.bloodworkdate = b.D390_labdate then a.bloodworkdate
                        when a.bloodworkdate = b.D420_labdate then a.bloodworkdate              
						when a.bloodworkdate = b.D450_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D480_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D510_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D540_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D570_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D600_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D630_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D660_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D690_labdate then a.bloodworkdate
						when a.bloodworkdate = b.D720_labdate then a.bloodworkdate	end as date,
case when a.bloodworkdate = b.D0_labdate then 'D0'
	when a.bloodworkdate = b.D30_labdate then 'D30'
    when a.bloodworkdate = b.D60_labdate then 'D60'
	when a.bloodworkdate = b.D90_labdate then 'D90'
    when a.bloodworkdate = b.D120_labdate then 'D120'
    when a.bloodworkdate = b.D150_labdate then 'D150'
	when a.bloodworkdate = b.D180_labdate then 'D180'
    when a.bloodworkdate = b.D210_labdate then 'D210'
    when a.bloodworkdate = b.D240_labdate then 'D240'
    when a.bloodworkdate = b.D270_labdate then 'D270'
    when a.bloodworkdate = b.D300_labdate then 'D300'
    when a.bloodworkdate = b.D330_labdate then 'D330'
    when a.bloodworkdate = b.D360_labdate then 'D360' 
	when a.bloodworkdate = b.D390_labdate then 'D390'
	when a.bloodworkdate = b.D420_labdate then 'D420'
	when a.bloodworkdate = b.D450_labdate then 'D450'
	when a.bloodworkdate = b.D480_labdate then 'D480'
	when a.bloodworkdate = b.D510_labdate then 'D510'
	when a.bloodworkdate = b.D540_labdate then 'D540'
	when a.bloodworkdate = b.D570_labdate then 'D570'
	when a.bloodworkdate = b.D600_labdate then 'D600'
	when a.bloodworkdate = b.D630_labdate then 'D630'
	when a.bloodworkdate = b.D660_labdate then 'D660'
	when a.bloodworkdate = b.D690_labdate then 'D690'
	when a.bloodworkdate = b.D720_labdate then 'D720'
	end as date_category,
a.category
from
tmp_rct_lab_test_output a inner join tmp_rct_patient_lab_dates b on (a.clientid = b.clientid and (a.bloodworkdate = b.D0_labdate or
																												 a.bloodworkdate = b.D30_labdate or
                                                                                                                 a.bloodworkdate = b.D60_labdate or
                                                                                                                 a.bloodworkdate = b.D90_labdate or
                                                                                                                 a.bloodworkdate = b.D120_labdate or
                                                                                                                 a.bloodworkdate = b.D150_labdate or
                                                                                                                 a.bloodworkdate = b.D180_labdate or
                                                                                                                 a.bloodworkdate = b.D210_labdate or
                                                                                                                 a.bloodworkdate = b.D240_labdate or
                                                                                                                 a.bloodworkdate = b.D270_labdate or
                                                                                                                 a.bloodworkdate = b.D300_labdate or
                                                                                                                 a.bloodworkdate = b.D330_labdate or
                                                                                                                 a.bloodworkdate = b.D360_labdate or
																												 a.bloodworkdate = b.D390_labdate or
																												 a.bloodworkdate = b.D420_labdate or
																												 a.bloodworkdate = b.D450_labdate or
																												 a.bloodworkdate = b.D480_labdate or
																												 a.bloodworkdate = b.D510_labdate or
																												 a.bloodworkdate = b.D540_labdate or
																												 a.bloodworkdate = b.D570_labdate or 
																												 a.bloodworkdate = b.D600_labdate or
																												 a.bloodworkdate = b.D630_labdate or
																												 a.bloodworkdate = b.D660_labdate or  
																												 a.bloodworkdate = b.D690_labdate or 
																												 a.bloodworkdate = b.D720_labdate 
																												 )
                                                                                                                 ) 		;								

drop temporary table if exists tmp_twin_rct_health_measures_final;

create temporary table tmp_twin_rct_health_measures_final
(
	clientid int not null,
	category varchar(100) not null,
	date date not null,
	date_category varchar(10) not null,
	measure decimal(10,2) null
); 

insert into tmp_twin_rct_health_measures_final (clientid, category, date, date_category, measure)
select a.clientid, a.category, a.date, a.date_category, cast(avg(b.measure) as decimal(10,2)) as measure
from tmp_rct_output_score_data a inner join tmp_rct_lab_test_output b on a.clientid = b.clientid and 
a.date = b.bloodworkdate and a.category = b.category 
group by a.clientid, a.category, a.date, a.date_category having avg(b.measure) is not null
;





insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"RHR" as category,convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D10" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D120" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D150" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
 select a.clientid,a.measure_event_date,"D390" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
 select a.clientid,a.measure_event_date,"D420" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
 select a.clientid,a.measure_event_date,"D450" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
 select a.clientid,a.measure_event_date,"D480" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
 select a.clientid,a.measure_event_date,"D510" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
 select a.clientid,a.measure_event_date,"D540" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"RHR" as category, convert(avg(b.average_measure), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.measure_event_date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_measures s2 on (s1.clientid = s2.clientid 
																		and (s2.measure_event_date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.measure_name = 'Rest-HR' 
                                                                        and s2.measure_type = '1d'                        
                                                                        and s2.average_measure is not null)
where measure_event_date is not null
group by s1.clientid) as a, bi_measures b
 where 
 a.clientid=b.clientid
 and b.measure_event_date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.measure_name = 'Rest-HR' and b.measure_type = '1d' and b.average_measure is not null
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null 
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.systolic_BP_1d is not null
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D10" as date_category,"Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D120" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D150" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D390" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D420" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D450" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D480" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D510" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D540" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D570" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D600" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D630" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D660" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D690" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D720" as date_category,"Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
and b.diastolic_BP_1d is not null
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D10" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D120" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D150" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D390" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D420" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D450" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D480" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D510" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D540" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D570" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D600" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D630" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D660" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
  
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D690" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
  
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D720" as date_category,"Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, min(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;
        
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"weight" as category, convert(avg(b.weight_1d), decimal(10,2)) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720)
                                                                        and s2.weight_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.weight_1d is not null
 group by clientid
 ;        
 

-- D0 BMI is the average of day1 to day5 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.bmi_1d is not null
 group by clientid
 ;


-- D10 BMI is the average of day6 to day10 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D10" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 
-- D30 BMI is the average of day26 to day30 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;

-- D90 BMI is the average of day86 to day90 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
-- D120 BMI is the average of day116 to day120 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D120" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;

-- D150 BMI is the average of day146 to day150 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D150" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;

-- D180 BMI is the average of day176 to day180 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
--  D270 BMI is the average of day266 to day270 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
--  D360 BMI is the average of day356 to day360 measures.
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D390" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D420" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D450" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D480" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D510" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D540" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D570" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D600" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D630" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D660" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D690" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D720" as date_category,"BMI" as category, convert(avg(b.bmi_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.bmi_1d is not null)
where s2.date is not null
and s1.cohort = 'TWIN_PRIME'  -- only for prime patients, the  BMI is coming from daily measures for CONTROL it is taken from clinictestresults (bloodwork).
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.bmi_1d is not null
 group by clientid
 ;
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
where a.clientid=b.clientid
and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
and b.visceralFat_1d is not null
group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D10" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D120" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D150" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D390" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D420" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D450" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D480" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D510" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D540" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D570" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D600" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D630" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D660" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D690" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D720" as date_category,"visceralFat" as category, convert(avg(b.visceralFat_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.visceralFat_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.visceralFat_1d is not null
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1
                                                                        and s2.eA1C_1d is not null)	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                       
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and b.eA1C_1d is not null
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.eA1C_1d is not null)	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                     
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.eA1C_1d is not null
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"eA1C" as category, convert(avg(b.eA1C_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.eA1C_1d is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.eA1C_1d is not null
 group by clientid
 ;




insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1 
                                                                        and s2.GV is not null)	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                        
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and b.GV is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.GV is not null)	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                     
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ; 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"GV" as category, convert(avg(b.GV), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.GV is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GV is not null
 group by clientid
 ;
 
 
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1 
                                                                        and s2.GMI is not null)	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                        
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and b.GMI is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.GMI is not null)	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                     
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.GMI is not null
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"GMI" as category, convert(avg(b.GMI), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.GMI is not null)
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date)
 and b.GMI is not null
 group by clientid
 ;
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                        
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                        
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"TIR" as category,
 (case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day4 and s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ; 
 


 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TIR" as category,
 (case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day114 and s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day144 and s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day384 and s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day414 and s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day444 and s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day474 and s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day504 and s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day534 and s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day564 and s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day594 and s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day624 and s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day654 and s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day684 and s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"TIR" as category, 
(case when SUM(b.TIR)/COUNT(b.date)>0 then convert(SUM(b.TIR)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day714 and s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TAR_1" as category,   
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TAR_1" as category, 
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day4 and s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day114 and s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day144 and s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day384 and s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day414 and s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day444 and s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day474 and s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day504 and s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day534 and s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day564 and s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day594 and s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day624 and s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day654 and s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day684 and s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"TAR_1" as category,
(case when SUM(b.TAR_1)/COUNT(b.date)>0 then convert(SUM(b.TAR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day714 and s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TAR_2" as category,
 (case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                      
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TAR_2" as category, 
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day4 and s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day114 and s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day144 and s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TAR_2" as category,(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day384 and s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day414 and s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day444 and s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day474 and s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day504 and s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day534 and s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day564 and s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day594 and s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day624 and s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day654 and s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day684 and s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"TAR_2" as category,
(case when SUM(b.TAR_2)/COUNT(b.date)>0 then convert(SUM(b.TAR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day714 and s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;





insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TBR_1" as category, 
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_PRIME'                                                                      
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TBR_1" as category, 
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;



 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day4 and s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day114 and s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day144 and s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day384 and s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day414 and s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day444 and s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day474 and s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day504 and s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day534 and s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day564 and s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day594 and s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day624 and s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day654 and s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day684 and s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"TBR_1" as category,
(case when SUM(b.TBR_1)/COUNT(b.date)>0 then convert(SUM(b.TBR_1)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day714 and s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 
 
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TBR_2" as category, 
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date < s1.sensor_day1 
                                                                        )	
where s2.date is not null 
and s1.cohort='TWIN_PRIME'                                                                      
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date between a.measure_event_date and date_sub(a.sensor_day1, INTERVAL 1 DAY))
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "TBR_2" as category, 
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day7) 
                                                                        )	
where s2.date is not null
and s1.cohort='TWIN_CONTROL'                                                                           
group by s1.clientid
) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 6 DAY)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day4 and s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 

 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day24 and s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day84 and s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day114 and s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day144 and s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day174 and s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day264 and s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

 
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day354 and s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day384 and s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day414 and s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day444 and s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day474 and s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day504 and s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day534 and s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day564 and s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day594 and s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day624 and s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day654 and s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day684 and s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category,"TBR_2" as category,
(case when SUM(b.TBR_2)/COUNT(b.date)>0 then convert(SUM(b.TBR_2)/COUNT(b.date), decimal(10,2)) else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day714 and s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date between date_sub(a.measure_event_date, INTERVAL 6 DAY) and a.measure_event_date)
 and not(b.TIR is null and b.TAR_1 is null and b.TAR_2 is null and b.TBR_1 is null and b.TBR_2 is null)
 group by clientid
 ;




insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.start_medicine_diabetes_drugs) - length(replace(b.start_medicine_diabetes_drugs,',','')) + 1) > 0,  max(length(b.start_medicine_diabetes_drugs) - length(replace(b.start_medicine_diabetes_drugs,',','')) + 1), 0) as measure 
from 
(select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid WHERE s1.sensor_day1 IS NOT NULL
group by s1.clientid
) as a, dim_client b
 where a.clientid=b.clientid 
 and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date, "D10" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
  insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category, "diabetic_drugs_count" as category, 
if(max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1)>0, max(length(b.medicine_drugs) - length(replace(b.medicine_drugs,',','')) + 1),0)as measure 
from (
		select s1.clientid, max(s2.date) as measure_event_date
			from tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where 
 a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 
 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.start_HTN_medicine_drugs) - length(replace(b.start_HTN_medicine_drugs,',','')) + 1)>0, max(length(b.start_HTN_medicine_drugs) - length(replace(b.start_HTN_medicine_drugs,',','')) + 1),0) as measure 
from 
(select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid	 WHERE s1.sensor_day1 IS NOT NULL
group by s1.clientid
) as a, dim_client b
 where a.clientid=b.clientid
and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category, "HTN_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_HTN) - length(replace(b.nonDiabetic_medicine_drugs_HTN,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
 
 
 
 
--  ++++++++++++++++++++++++++++++++++  CHOL drugs count  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--  D0 CHOL drugs count ---------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.start_CHOL_medicine_drugs) - length(replace(b.start_CHOL_medicine_drugs,',','')) + 1)>0, max(length(b.start_CHOL_medicine_drugs) - length(replace(b.start_CHOL_medicine_drugs,',','')) + 1),0) as measure 
from 
(select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid	 WHERE s1.sensor_day1 IS NOT NULL
group by s1.clientid
) as a, dim_client b
 where a.clientid=b.clientid
and b.is_row_current="Y"
 group by clientid
 ;
 
--  D10 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
 
-- D30 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
--  D90 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
--  D120 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
--  D150 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
--  D180 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 
 
--   D270 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ;
 
--   D360 CHOL drugs count--------------------
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category, "CHOL_drugs_count" as category, 
if(max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1)>0, max(length(b.nonDiabetic_medicine_drugs_CHOL) - length(replace(b.nonDiabetic_medicine_drugs_CHOL,',','')) + 1),0) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
 ; 





insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.start_medicine_diabetes_drugs)) = 'METFORMIN' or ltrim(rtrim(b.start_medicine_diabetes_drugs)) = 'Biguanide(DIABETES)' then 1 else 0 end) as measure
from (
select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid 		WHERE s1.sensor_day1 IS NOT NULL													
group by s1.clientid
) as a, dim_client b
 where  a.clientid=b.clientid
 and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category, "is_on_metformin_only" as category, 
(case when ltrim(rtrim(b.medicine_drugs)) = 'METFORMIN' or ltrim(rtrim(b.medicine_drugs)) = 'Biguanide (DIABETES)' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;





insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "is_on_insulin" as category, 
(case when ltrim(rtrim(b.start_medicine_diabetes_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, min(s1.sensor_day1 ) as measure_event_date 
from 
tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid 	 WHERE s1.sensor_day1 IS NOT NULL														
group by s1.clientid
) as a, dim_client b
 where a.clientid=b.clientid
 and b.is_row_current="Y"
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day10) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day30) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day90) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day120) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day150) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day180) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day270) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day360) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day390) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day420) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day450) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day480) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day510) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day540) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day570) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day600) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day630) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day660) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day690) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category, "is_on_insulin" as category, 
 (case when ltrim(rtrim(b.medicine_drugs)) like '%Insu%' then 1 else 0 end) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and s2.date = s1.sensor_day720) 
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and (b.date = a.measure_event_date)
 group by clientid
;





-- +++++++++++++++++++++++++++++++++++++   is_on_STATINS_only  ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
--  D0 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.start_CHOL_medicine_drugs)) = 'Rosuvastatin' or ltrim(rtrim(b.start_CHOL_medicine_drugs)) = 'Atorvastatin' or ltrim(rtrim(b.start_CHOL_medicine_drugs)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, min(s1.sensor_day1 ) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join dim_client s2 on s1.clientid = s2.clientid 		WHERE s1.sensor_day1 IS NOT NULL													
group by s1.clientid
) as a, dim_client b
 where  a.clientid=b.clientid  
 and b.is_row_current="Y"
 group by clientid
 ;
 
--  D10 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D10" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day10) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

-- D30 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D30" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day30) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
; 

-- D90 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D90" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day90) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

-- D120 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D120" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day120) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

-- D150 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D150" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day150) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

-- D180 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D180" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day180) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

-- D270 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D270" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day270) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;

-- D360 is on STATINS only
insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D360" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day360) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D390" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day390) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D420" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day420) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D450" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day450) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;




insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D480" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day480) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D510" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day510) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D540" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day540) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D570" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day570) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D600" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day600) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D630" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day630) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D660" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day660) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D690" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day690) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D720" as date_category, "is_on_STATINS_only" as category, 
(case when ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Rosuvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'Atorvastatin' or ltrim(rtrim(b.nonDiabetic_medicine_drugs_CHOL)) = 'STATINS' then 1 else 0 end) as measure
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date = s1.sensor_day720) 
                                                                        )
where s2.date is not null
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date = a.measure_event_date
 group by clientid
;




insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.systolic_BP_1d is not null)	
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')                                                                    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D10" as date_category,"Home_Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"Home_Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME') 
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"Home_Sys BP" as category,convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D120" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME') 
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D150" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D390" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D420" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D450" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D480" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D510" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D540" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D570" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME') 
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D600" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D630" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D660" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME') 
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D690" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D720" as date_category,"Home_Sys BP" as category, convert(avg(b.systolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.systolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.systolic_BP_1d is not null
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)
select a.clientid,a.measure_event_date,"D0" as date_category, "Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, min(s2.date) as measure_event_date,max(s1.sensor_day1) as sensor_day1
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day1 and s1.sensor_day5)
                                                                        and s2.diastolic_BP_1d is not null)	
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')                                                                    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between a.measure_event_date and date_add(a.measure_event_date, INTERVAL 4 DAY)
 and b.diastolic_BP_1d is not null
 group by clientid
 ;



insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D10" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day6 and s1.sensor_day10) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D30" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day26 and s1.sensor_day30) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D90" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day86 and s1.sensor_day90) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')   
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D120" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day116 and s1.sensor_day120) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')   
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D150" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day146 and s1.sensor_day150) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;


insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D180" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day176 and s1.sensor_day180) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D270" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day266 and s1.sensor_day270) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 

insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D360" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day356 and s1.sensor_day360) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D390" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day386 and s1.sensor_day390) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D420" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day416 and s1.sensor_day420) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')   
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D450" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day446 and s1.sensor_day450) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')   
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D480" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day476 and s1.sensor_day480) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')   
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D510" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day506 and s1.sensor_day510) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D540" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day536 and s1.sensor_day540) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D570" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day566 and s1.sensor_day570) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D600" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day596 and s1.sensor_day600) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D630" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day626 and s1.sensor_day630) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')   
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D660" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day656 and s1.sensor_day660) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
  
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D690" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day686 and s1.sensor_day690) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')    
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 
 
  
 insert into tmp_twin_rct_health_measures_final (clientid, date, date_category, category, measure)

select a.clientid,a.measure_event_date,"D720" as date_category,"Home_Dia BP" as category, convert(avg(b.diastolic_BP_1d), decimal(10,2)) as measure 
from (
select s1.clientid, max(s2.date) as measure_event_date
from 
tmp_rct_patient_lab_dates s1 left join bi_rct_patient_measures_x_preactivation s2 on (s1.clientid = s2.clientid 
																		and (s2.date between s1.sensor_day716 and s1.sensor_day720) 
                                                                        and s2.diastolic_BP_1d is not null)
where s2.date is not null
and s1.cohort in ('TWIN_CONTROL' ,'TWIN_PRIME')  
group by s1.clientid) as a, bi_rct_patient_measures_x_preactivation b
 where a.clientid=b.clientid
 and b.date between date_sub(a.measure_event_date, INTERVAL 4 DAY) and a.measure_event_date
 and b.diastolic_BP_1d is not null
 group by clientid
 ;
 





create index idx_tmp_twin_rct_health_measures_final on tmp_twin_rct_health_measures_final(clientid asc, category, date asc, date_category); 


insert into bi_twins_rct_health_output_measures
(clientid, category, date, date_category, measure)
select clientid, category, date, date_category, measure
from tmp_twin_rct_health_measures_final a 
where not exists (select 1 from bi_twins_rct_health_output_measures b where a.clientid = b.clientid and a.date = b.date and a.date_category = b.date_category and a.category = b.category)
;

update bi_twins_rct_health_output_measures a inner join tmp_twin_rct_health_measures_final b on (a.clientid = b.clientid and a.date = b.date and a.date_category = b.date_category and a.category = b.category)
set a.measure = b.measure
;


drop temporary table if exists tmp_twin_rct_health_measures_final
;

drop temporary table if exists tmp_rct_output_score_data;



update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_twins_rct_health_output_measures'
;

END