CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_patient_status`(  )
BEGIN
   
            
 
 update dailyProcessLog 
 set startDate = now() 
 where processName = 'sp_load_bi_patient_status' ;
 
 
 drop temporary table if exists temp_bi_patient_status;  
 
 create temporary table temp_bi_patient_status 
 ( 
 clientid int not null, 
 doctorid int null,
 doctor_name varchar(50) null, 
 doctorName_short varchar(30) null, 
 coachName varchar(50) null, 
 coachName_short varchar(30) null, 
 pha varchar(50) null,
 poc varchar(50) null,
 enrollmentdateitz datetime null, 
 visitdateITZ datetime null, 
 homeVisitDateITZ datetime null, 
 treatmentSetupDateITZ datetime null, 
 status varchar(15) null, 
 status_start_date date null, 
 status_end_date date null 
 )  
 ;  
 
 insert into temp_bi_patient_status(clientid, doctor_name, enrollmentdateitz, status, status_start_date, status_end_date)  
 
 select distinct s.clientid, doctorname, enrollmentdateITZ, s.status, status_start_date, status_end_date 
 from  
 ( 
 select s1.clientid, s1.status, date(itz(s1.laststatuschange)) as status_start_date,  
 date(	
		itz(if((ifnull(date_sub(s2.laststatuschange, interval 1 day), now()) < s1.laststatuschange) 
		, s1.laststatuschange, 
		ifnull(date_sub(s2.laststatuschange, interval 1 day), now())))
    ) as status_end_date 
 from  
 (   
		select @row_num := case when @id = id then @row_num + 1 else 1 end as rownumber,  
		@id := id as clientid, laststatuschange, status  
		from   
		(   
			select distinct id, laststatuschange, status 
			from    
			(    
				select distinct id, rev, laststatuschange as laststatuschange, status from clients_aud    
				union all     
				    
				select clientid as id, 1 as rev, enrollmentdateitz as laststatuschange, 'ACTIVE' as status from v_client_tmp where clientid in (15602, 15603)   
			) s    
			order by id, rev     
		) s1   cross join (select @row_num := 0, @id := 0 ) s2  
 ) s1 
left join  
 (   
	select @s_num := case when @id = id then @s_num + 1 else 1 end as rownumber,  
	@id := id as clientid, laststatuschange, status  
	from   
	(   
			select distinct id, laststatuschange, status 
			from    
			(    
				select distinct id, rev, laststatuschange as laststatuschange, status from clients_aud     
				union all     
				    
				select clientid as id, 1 as rev, enrollmentdateitz as laststatuschange, 'ACTIVE' as status from v_client_tmp where clientid in (15602, 15603)   
			) s   
			order by id, rev     
	) s1   cross join (select @s_num := 0, @id := 0 ) s2  
 ) s2  on s1.clientid = s2.clientid and s2.rownumber = s1.rownumber + 1 ) 
 s inner join v_allClient_list c on s.clientid = c.clientid 
 order by clientid, status_start_date 
;  

create index idx_tmp_patient_status on temp_bi_patient_status (clientid asc);   

 

update temp_bi_patient_status a inner join v_allClient_list b  on a.clientid = b.clientid 
set a.doctorid = b.doctorid,
	a.doctorName_short = b.doctornameShort,     
	a.coachName = b.coachName,     
    a.coachName_short = b.coachNameShort,
	a.pha = b.pha,
    a.poc = b.poc,
    a.enrollmentdateitz = b.enrollmentdateitz;  
    
 
update temp_bi_patient_status a inner join  
(select distinct clientid, max(itz(visitdate)) as homeVisitDateITZ from homevisits group by clientid) b  
on a.clientid = b.clientid  
set a.homeVisitDateITZ = b.homeVisitDateITZ ;  

 
update temp_bi_patient_status a inner join  
(select distinct id as clientid, max(itz(treatmentsetupdate)) as treatmentSetupDateITZ from clients group by id) b 
on a.clientid = b.clientid  
set a.treatmentSetupDateITZ = b.treatmentSetupDateITZ ;  

 
update temp_bi_patient_status a inner join  
(select distinct clientid, max(cast(startdate as datetime)) as visitdateITZ from MomentumWeekDetails group by clientid) b  
on a.clientid = b.clientid  
set a.visitdateITZ = b.visitdateITZ ;  

drop temporary table if exists tmp_multiple_status_records;  


create temporary table tmp_multiple_status_records 
select clientid, status, count(*) as cnt 
from temp_bi_patient_status where status in ('active', 'pending_active','prospect','registration' ,'discharged', 'inactive', 'on_hold') 
group by clientid, status 
having count(*) > 1;  

drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  

create temporary table tmp_recs_to_be_deleted_patient_status as  
select a.clientid, a.status, b.status_start_date, b.status_end_date 
from  tmp_multiple_status_records a inner join temp_bi_patient_status b 
on a.clientid = b.clientid and a.status = b.status and b.status_start_date = b.status_end_Date 
;  

delete temp_bi_patient_status from temp_bi_patient_status inner join tmp_recs_to_be_deleted_patient_status 
on  (temp_bi_patient_status.clientid = tmp_recs_to_be_deleted_patient_status.clientid  
		and temp_bi_patient_status.status = tmp_recs_to_be_deleted_patient_status.status  
        and temp_bi_patient_status.status_start_date = tmp_recs_to_be_deleted_patient_status.status_start_date  
        and temp_bi_patient_status.status_end_date = tmp_recs_to_be_deleted_patient_status.status_end_date) 
;  

drop temporary table if exists tmp_multiple_status_records; 
drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  


		
		create temporary table tmp_multiple_status_records
		select clientid, status, count(*) as cnt 
		from temp_bi_patient_status where status in ('active', 'pending_active','prospect','registration' ,'discharged', 'inactive', 'on_hold') 
		group by clientid, status 
		having count(*) > 1
		;  

		drop temporary table if exists tmp_recs_to_be_checked_patient_list;  

		create temporary table tmp_recs_to_be_checked_patient_list as  
		select a.clientid, a.status, b.status_start_date, b.status_end_date 
		from  tmp_multiple_status_records a inner join temp_bi_patient_status b 
		on a.clientid = b.clientid and a.status = b.status
		;

		drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  

		create temporary table tmp_recs_to_be_deleted_patient_status as 
		select a.clientid, a.status, a.status_start_date, a.status_end_date 
		from  tmp_recs_to_be_checked_patient_list a inner join temp_bi_patient_status b 
		on a.clientid = b.clientid and a.status = b.status and a.status_start_date = b.status_start_date and a.status_end_date < b.status_end_Date 
		;  

		delete temp_bi_patient_status from temp_bi_patient_status inner join tmp_recs_to_be_deleted_patient_status 
		on  (temp_bi_patient_status.clientid = tmp_recs_to_be_deleted_patient_status.clientid  
				and temp_bi_patient_status.status = tmp_recs_to_be_deleted_patient_status.status  
				and temp_bi_patient_status.status_start_date = tmp_recs_to_be_deleted_patient_status.status_start_date  
				and temp_bi_patient_status.status_end_date = tmp_recs_to_be_deleted_patient_status.status_end_date) 
		;  

		drop temporary table if exists tmp_multiple_status_records; 
		drop temporary table if exists tmp_recs_to_be_checked_patient;  
		drop temporary table if exists tmp_recs_to_be_deleted_patient_status;  




insert into bi_patient_status (clientid, doctorid, doctor_name, doctorName_short, coachName, coachName_short, pha, poc, enrollmentdateitz, visitdateITZ, 
homeVisitDateITZ, treatmentSetupDateITZ, status, status_start_date, status_end_date) 
select clientid, doctorid, doctor_name, doctorName_short, coachName, coachName_short, pha, poc, enrollmentdateitz, visitdateITZ, 
homeVisitDateITZ, treatmentSetupDateITZ, status, status_start_date, status_end_date 
from temp_bi_patient_status a 
where not exists (select 1 from bi_patient_status b where a.clientid = b.clientid and a.status = b.status and a.status_start_date = b.status_start_date)  
; 

update bi_patient_status a inner join temp_bi_patient_status b on (a.clientid = b.clientid and a.status = b.status and a.status_start_date = b.status_start_date) 
 set  a.status_end_date = b.status_end_date  
;


update bi_patient_status a inner join temp_bi_patient_status b on (a.clientid = b.clientid)
 set  a.visitdateitz = b.visitdateitz,      
      a.homeVisitDateITZ = b.homeVisitDateITZ,      
      a.treatmentSetupDateITZ = b.treatmentSetupDateITZ,      
      a.enrollmentdateITZ = b.enrollmentdateITZ,
      a.doctorid = b.doctorid,
      a.doctor_name = b.doctor_name,      
      a.doctorname_short = b.doctorname_short,      
      a.coachName = b.coachName,      
      a.coachName_short = b.coachName_short,
      a.pha = b.pha,
      a.poc = b.poc;  

drop temporary table if exists temp_bi_patient_status;  

 
update dailyProcessLog 
set updateDate = now() 
where processName = 'sp_load_bi_patient_status' 
;  

END;
