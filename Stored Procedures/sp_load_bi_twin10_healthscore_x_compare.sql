DELIMITER $$
CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `sp_load_bi_twin10_healthscore_x_compare`(  )
BEGIN 

update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_twin10_healthscore_x_compare'
;

drop temporary table if exists tmp_twin10_healthscore_x; 

create temporary table tmp_twin10_healthscore_x 
(
clientid int not null,
category varchar(512) not null,
date date not null,
score decimal(10,5),
timeline_flag varchar(50)
); 

insert into tmp_twin10_healthscore_x(clientid, category, date, score, timeline_flag)
select distinct d.clientid, d.category, d.first_date date, case when d.first_date<=act.treatmentStartDate then e.score end as score, 'initial' as timeline_flag 
from
(
	select a.clientid, h.category, min(date) as first_date
	from clienthealthscores a
	join healthscorelayers h on (a.layerid=h.healthscorelayerid)
	where a.versionid=501 and h.category not in ('CHRONIC_DISEASES')
	group by a.clientid, h.category) d 
inner join v_active_clients v on (d.clientid=v.clientid)
left join 
	(select clientid, h.category, date,score
     from clienthealthscores a
     join healthscorelayers h on (a.layerid=h.healthscorelayerid)
	 where a.versionid=501)e on (d.clientid=e.clientid) and (d.category=e.category) and (d.first_date=e.date)
left join (SELECT b.clientid AS clientid,MIN(b.status_start_date) AS treatmentStartDate
		   FROM bi_patient_status b
           WHERE b.status = 'active'
           GROUP BY b.clientid) act on (d.clientid=act.clientid);


insert into tmp_twin10_healthscore_x(clientid, category, date, score, timeline_flag) 
select distinct b.clientid, b.category, b.date, b.score, case when rnk=1 then 'latest' else 'latest-1' end as timeline_flag 
from
(select clientid, date, score, h.category, dense_rank() over (partition by clientid, h.category order by date desc) rnk
from clienthealthscores a
join healthscorelayers h on (a.layerid=h.healthscorelayerid)
where a.versionid=501 and h.category not in ('CHRONIC_DISEASES')
)b inner join v_active_clients v on (b.clientid=v.clientid)
where rnk<=2;

	
truncate table bi_twin10_healthscore_x_compare ;


insert into bi_twin10_healthscore_x_compare(clientid, category, date, score, timeline_flag)
select clientid, category, date, score, timeline_flag 
from tmp_twin10_healthscore_x;

drop temporary table if exists tmp_twin10_healthscore_x; 


update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_twin10_healthscore_x_compare'
;

END$$
DELIMITER ;
