CREATE DEFINER=`twinsAnalytics`@`%` PROCEDURE `twins`.`sp_load_bi_daily_onboarding_sla`(  )
BEGIN

/*
2021-08-26 Aravindan - New sections added under TSA OPS Review category to support TSA ops service dashboard
*/


update dailyProcessLog
set startDate = now()
where processName = 'sp_load_bi_daily_onboarding_sla'
;

drop temporary table if exists tmp_daily_onboarding_sla; 

create temporary table tmp_daily_onboarding_sla 
(
report_date date not null,
category varchar(50) not null,
measure_name varchar(100) not null,
measure decimal(10,1) null
); 

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Pending Active' as measure_name, 
count(distinct b.clientid) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and (b.status = 'pending_active' or b.status = 'on_hold')
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
where date >= date_sub(date(itz(now())), interval 14 day)
group by date ;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Pending Active E5-E8' as measure_name, 
count(distinct b.clientid) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
where date >= date_sub(date(itz(now())), interval 14 day)
and datediff(a.date, b.status_start_date) between 6 and 8	
group by date ;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Pending Active > E8' as measure_name, 
count(distinct b.clientid) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
where date >= date_sub(date(itz(now())), interval 14 day)
and datediff(a.date, b.status_start_date) > 8	
group by date;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Pending Active < E5' as measure_name, 
count(distinct b.clientid) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
where date >= date_sub(date(itz(now())), interval 14 day)
and datediff(a.date, b.status_start_date) < 5	
group by date;






insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'New Enrollments' as measure_name, 
count(distinct case when def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a left join bi_patient_status b on a.date = b.status_start_date and b.status = 'pending_active'
					  inner join v_allClient_list c on b.clientid = c.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where c.status not in ('registration')
and c.patientname not like '%obsolete%'
and date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;




insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Deffered' as measure_name, count(distinct case when def.clientid is not null then b.clientid else null end) as measure
from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason, labels from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 10 day)
group by date ;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Disenrolled' as measure_name, count(distinct case when d.clientid is not null and def.clientid is null and ex.clientid is null then b.clientid else null end) as measure
from v_date_to_date a left join bi_patient_status b on a.date = b.status_start_date and b.status = 'discharged'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
						left join 
						(		
								select distinct clientid from bi_patient_status a 
								where 
								status = 'pending_active' 
								and not exists (select 1 from bi_patient_status b where a.clientid = b.clientid and b.status = 'active')
								and exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'discharged' and c.status_start_date >= a.status_end_date)
						) d on b.clientid = d.clientid 
                        left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
                        left join dim_client ex on b.clientid = ex.clientid and ex.is_medical_exclusion = 'Yes'
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;



insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Activations' as measure_name, 
count(distinct case when d.clientid is not null and dis.clientid is null then b.clientid else null end) as measure
from v_date_to_date a left join (
										select clientid, max(status_start_date) as status_start_date from bi_patient_status 
										where status = 'active'
										group by clientid 
                                ) b on a.date = b.status_start_date 
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
					  left join (
								
								select a.clientid from
                                (
									select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status = 'pending_active'
									group by clientid
                                ) a
								where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
						) d on b.clientid = d.clientid
                        left join
                        (
									select a.clientid, a.status_end_date from
									(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status in ( 'discharged', 'inactive')
									group by clientid
                                    ) a
                                   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
                        ) dis on b.clientid = dis.clientid
where date >= date_sub(date(itz(now())), interval 20 day)
group by date
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Activations within SLA' as measure_name, 
count(distinct case when d.clientid is not null and dis.clientid is null then b.clientid else null end) as measure
from v_date_to_date a left join (
										select clientid, max(status_start_date) as status_start_date from bi_patient_status 
										where status = 'active'
										group by clientid 
                                ) b on a.date = b.status_start_date 
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join (
								
									select a.clientid from
									(
										select distinct clientid, max(status_end_date) as status_end_date, max(status_start_date) status_start_date from bi_patient_status a
										where
										status = 'pending_active'
										group by clientid
									) a
									where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
									and datediff(a.status_end_date, a.status_start_date) <= 9
						) d on b.clientid = d.clientid 
                        left join
                        (
									select a.clientid, a.status_end_date from
									(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status in ( 'discharged', 'inactive')
									group by clientid
                                    ) a
                                   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
                        ) dis on b.clientid = dis.clientid
where date >= date_sub(date(itz(now())), interval 20 day)
group by date; 

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Activations within SLA < E5' as measure_name, 
count(distinct case when d.clientid is not null and dis.clientid is null then b.clientid else null end) as measure
from v_date_to_date a left join (
										select clientid, max(status_start_date) as status_start_date from bi_patient_status 
										where status = 'active'
										group by clientid 
                                ) b on a.date = b.status_start_date 
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join (
								
									select a.clientid from
									(
										select distinct clientid, max(status_end_date) as status_end_date, max(status_start_date) status_start_date from bi_patient_status a
										where
										status = 'pending_active'
										group by clientid
									) a
									where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
									and datediff(a.status_end_date, a.status_start_date) <= 5
						) d on b.clientid = d.clientid 
                        left join
                        (
									select a.clientid, a.status_end_date from
									(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status in ( 'discharged', 'inactive')
									group by clientid
                                    ) a
                                   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
                        ) dis on b.clientid = dis.clientid
where date >= date_sub(date(itz(now())), interval 20 day)
group by date; 

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Activations - Deferred Cases' as measure_name, 
count(distinct case when d.clientid is not null and dis.clientid is null and (def.clientid is not null or oh.clientid is not null) then b.clientid else null end) as measure
from v_date_to_date a left join (
										select clientid, max(status_start_date) as status_start_date from bi_patient_status 
										where status = 'active'
										group by clientid 
                                ) b on a.date = b.status_start_date 
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
					  left join (
								
								select a.clientid from
                                (
									select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status = 'pending_active'
									group by clientid
                                ) a
								where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
						) d on b.clientid = d.clientid
                        left join
                        (
									select a.clientid, a.status_end_date from
									(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status in ( 'discharged', 'inactive')
									group by clientid
                                    ) a
                                   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
                        ) dis on b.clientid = dis.clientid
                        left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid
						left join (select distinct clientid from bi_patient_status where status = 'on_hold') oh on b.clientid = oh.clientid
where date >= date_sub(date(itz(now())), interval 20 day)
group by date
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Escalation Open' as measure_name,
count(distinct case when def.clientid is null then d.incidentId else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join (
									select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date 
									from bi_incident_mgmt where category not in ('Approval')
                                ) d on b.clientid = d.clientid and a.date >= report_date and a.date < ifnull(resolved_date,date(itz(now())))
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate          
where date >= date_sub(date(itz(now())), interval 14 day)
group by date;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Escalation Closed' as measure_name, count(distinct  case when def.clientid is null then d.incidentId else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join (
									select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date 
									from bi_incident_mgmt where category not in ('Approval')
                                ) d on b.clientid = d.clientid and a.date = d.resolved_date
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate          
where date >= date_sub(date(itz(now())), interval 14 day)
group by date  
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, 'Escalation closed AVG time (Hrs)' as measure_name, floor(avg(close_duration_hours)) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join (
									select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date, close_duration_hours
									from bi_incident_mgmt where category not in ('Approval') and status = 'RESOLVED'
                                ) d on b.clientid = d.clientid and a.date = d.resolved_date
					  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day) and def.clientid is NULL
group by date; 


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Overall' as category, '% Escalation Closed SLA' as measure_name, cast(count(distinct case when close_duration_hours <= 8 then d.incidentid else null end)/count(distinct d.clientid) as decimal(10,2)) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join (
									select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date, close_duration_hours
									from bi_incident_mgmt where category not in ('Approval') and status = 'RESOLVED'
                                ) d on b.clientid = d.clientid and a.date = d.resolved_date
					   left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day) and def.clientid is NULL
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw Schedule' as category, 'Schedule Backlog' as measure_name, 
count(distinct case when (d.bloodWork_scheduled_status is null or d.bloodWork_scheduled_status not in ('SCHEDULED','COMPLETED')) and d.first_Investigation_upload_date is null and def.clientid is null  then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day) 
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw Schedule' as category, 'Schedule Backlog >2 Days' as measure_name, 
count(distinct case when (d.bloodWork_scheduled_status is null or d.bloodWork_scheduled_status not in ('SCHEDULED','COMPLETED')) and d.first_Investigation_upload_date is null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where datediff(a.date, b.status_start_date) > 2
and date >= date_sub(date(itz(now())), interval 14 day)
group by date; 

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw Schedule' as category, 'Blood Draw Schedule Completed' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status = 'SCHEDULED' and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid and a.date = date(d.Bloodwork_Schedule_created_Date)
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw Schedule' as category, 'Blood Draw Schedule Completed in SLA' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status in ('PENDING','CANCELLED') and date(d.bloodwork_scheduled_date) = b.status_start_date and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw Schedule' as category, 'Blood Draw Schedule beyond E2 SLA' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status not in ('PENDING','CANCELLED') and date(d.bloodwork_scheduled_date) = date_add(b.status_start_date, interval 2 day) and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw Schedule' as category, 'Schedule IMT Open' as measure_name, null as measure
from v_date_to_date a 
where date >= date_sub(date(itz(now())), interval 14 day)
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw' as category, 'Scheduled & Blood Draw Pending' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status = 'SCHEDULED' and first_Investigation_upload_date is null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw' as category, 'Blood Draw Completed' as measure_name, 
count(distinct case when (d.bloodWork_scheduled_status = 'COMPLETED' or (d.bloodWork_scheduled_status = 'SCHEDULED' and first_Investigation_upload_date is not null)) and def.clientid is null  then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw' as category, 'Blood Draw Completed in SLA' as measure_name, null as measure
from v_date_to_date a 
where date >= date_sub(date(itz(now())), interval 14 day)
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw' as category, 'Patient Rescheduled Blood Draw' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status in ('PENDING','CANCELLED') and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Draw' as category, 'Blood Draw IMT Open' as measure_name, count(distinct case when def.clientid is null then d.incidentId else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join (select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date from bi_incident_mgmt where category in ('BLOOD_WORK')) d on b.clientid = d.clientid and a.date between report_date and ifnull(date_sub(resolved_date, interval 1 day), date(now()))
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Vitals & Condition' as category, 'V&C Backlog' as measure_name, 
count(distinct case when d.vitals_conds_allergies_meds_addInfor_captured is null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Vitals & Condition' as category, 'V&C Completed - Total' as measure_name, 
count(distinct case when d.vitals_conds_allergies_meds_addInfor_captured is not null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Vitals & Condition' as category, 'V&C Completed - Daily' as measure_name, 
count(distinct case when d.vitals_conds_allergies_meds_addInfor_captured is not null and date(d.vitals_conds_allergies_meds_addInfor_captured) = a.date and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date ;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Vitals & Condition' as category, 'V&C Completed in SLA' as measure_name, 
count(distinct case when date(d.vitals_conds_allergies_meds_addInfor_captured) <= date_add(b.status_start_date, interval 4 day) and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Vitals & Condition' as category, 'V&C Patient Rescheduled' as measure_name, 
count(distinct case when date(d.vitals_conds_allergies_meds_addInfor_captured) > date_add(b.status_start_date, interval 4 day) and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Report Upload' as category, 'Blood Report Backlog' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' and d.first_Investigation_upload_date is null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Report Upload' as category, 'Blood Report Completed' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' and d.first_Investigation_upload_date is not null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Report Upload' as category, 'Blood Report Completed in SLA' as measure_name, 
count(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' and d.first_Investigation_upload_date is not null and date(d.first_Investigation_upload_date) <= date_add(b.status_start_date, interval 4 day) and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Report Upload' as category, 'Blood Report Avg Time (Hrs)' as measure_name, 
floor(avg(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' 
				and d.first_Investigation_upload_date is not null 
                and d.firstBloodWorkDateITZ is not null and def.clientid is null
                then timestampdiff(hour, d.firstBloodWorkDateITZ, d.first_Investigation_upload_date) else null end)) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Blood Report Upload' as category, 'Blood Report IMT Open' as measure_name, null as measure
from v_date_to_date a 
where date >= date_sub(date(itz(now())), interval 14 day)
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Doctor Consult Backlog' as measure_name, 

count(distinct case when d.first_Investigation_upload_date is not null and first_bmi is not null and (First_Medicine_Published_Date is null) and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where  date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Doctors Active' as measure_name, count(distinct b.id) as measure
from v_date_to_date a inner join doctors b on b.test is false and date(b.dateadded) <= a.date
where  date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Doctors Consulted' as measure_name, 
count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED','SCHEDULED') and def.clientid is null then c.doctorname else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Consult Scheduled' as measure_name, 
count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('SCHEDULED', 'COMPLETED') and def.clientid is null then clientAppointmentId else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Consult Completed' as measure_name, 
count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED') and def.clientid is null then clientAppointmentId else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Consult in SLA (Consult + Med Publish)' as measure_name, 
count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED') and First_Medicine_Published_Date is not null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

			


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Pending Meds Published Backlog' as measure_name, 
count(distinct case when doctorAppointment_status = 'COMPLETED' and First_Medicine_Published_Date is null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
                      left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Consult completed with manual cohort' as measure_name, 
count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED') and First_Medicine_Published_Date_but_Manual is not null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
					  left join (
												select a.clientid, a.First_Medicine_Published_Date_but_Manual
												from
												(
												Select clientid, min(itz(dateadded)) as First_Medicine_Published_Date_but_Manual from medicationhistories group by clientid
												) a inner join medicationhistories b on a.clientid = b.clientid and a.First_Medicine_Published_Date_but_Manual = itz(dateadded) and changeType = 'MANUAL'
								) e on b.clientid = e.clientid 
					  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

					
                    
insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)                     
select date, 'Doctor Consult' as category, 'Doctor Rescheduled' as measure_name, 
count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('CANCELLED') and def.clientid is null  then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Member Rescheduled' as measure_name, null as measure
from v_date_to_date a 
where date >= date_sub(date(itz(now())), interval 14 day)
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Doctor Consult' as category, 'Doctor IMT Open' as measure_name, null as measure
from v_date_to_date a 
where date >= date_sub(date(itz(now())), interval 14 day)
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Sensor Activation' as category, 'Sensor Activation Backlog' as measure_name, 
count(distinct case when First_Medicine_Published_Date is not null and d.sensorActivation_schedule_date is null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day) 
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Sensor Activation' as category, 'Sensor Activation Scheduled' as measure_name, 
count(distinct case when First_Medicine_Published_Date is not null and sensorActivation_schedule_status in ('SCHEDULED','COMPLETED') and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Sensor Activation' as category, 'Sensor Activation Completed' as measure_name, 
count(distinct case when d.sensorActivation_schedule_status in ('SCHEDULED', 'COMPLETED') and First_Medicine_Published_Date is not null and date(First_Fitbit_Sync_Date) is not null and def.clientid is null then b.clientid else null end) as measure 
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and (b.status = 'pending_active' or b.status = 'active') 
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Sensor Activation' as category, 'Sensor Activation Completed in SLA' as measure_name, 
count(distinct case when d.sensorActivation_schedule_status in ('SCHEDULED', 'COMPLETED')
					
					and date(sensorActivation_schedule_date) <= date_add(b.status_start_date, interval 8 day) and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and (b.status = 'pending_active' or b.status = 'active')
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Sensor Activation' as category, 'Sensor Activation Rescheduled' as measure_name, 
count(distinct case when b1.type = 'HOME_VISIT' and b1.status in ('PENDING')  and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


			

insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Treatment Activation' as category, 'Treatment Activation Backlog' as measure_name, 
count(distinct case when First_Medicine_Published_Date is not null and First_Fitbit_Sync_Date is not null and date(First_Medicine_Published_Date) <= date and date(First_Fitbit_Sync_Date) <= date and c.status = 'pending_active' and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, doctorname, dischargeReason, status from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where  date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Treatment Activation' as category, 'Treatment Activation Completed' as measure_name,
count(distinct case when First_Medicine_Published_Date is not null and First_Fitbit_Sync_Date is not null and e.clientid is not null and dis.clientid is null and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date = b.status_start_date and b.status = 'active'
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
					  left join (
								
								select a.clientid from
                                (
									select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status = 'pending_active'
									group by clientid
                                ) a
								where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
                      ) e on b.clientid = e.clientid
                      left join
                        (
									select a.clientid, a.status_end_date from
									(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status in ( 'discharged', 'inactive')
									group by clientid
                                    ) a
                                   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date < a.status_end_date)
                        ) dis on b.clientid = dis.clientid
                        left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where  date >= date_sub(date(itz(now())), interval 14 day)
group by date
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Treatment Activation' as category, 'Treatment Activation Completed within SLA' as measure_name, 
count(distinct case when First_Medicine_Published_Date is not null and First_Fitbit_Sync_Date is not null and e.active_Status_start_Date <= date_add(b.status_start_date, interval 8 day) and def.clientid is null then b.clientid else null end) as measure
from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
					  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
					  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                      left join (select clientid, max(status_start_date) as active_status_start_date from bi_patient_status where status = 'active' group by clientid) e on b.clientid = e.clientid
                      left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
where date >= date_sub(date(itz(now())), interval 14 day)
group by date 
;


insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
select date, 'Treatment Activation' as category, 'Treatment Activation IMT Open' as measure_name, null as measure
from v_date_to_date a 
where date >= date_sub(date(itz(now())), interval 14 day)
;






		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
		select date, 'TSA OPS Review' as category, 'Sensor Activations' as measure_name, count(distinct case when d.first_fitbit_sync_Date is not null then b.clientid else null end) as measure
		from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
							  inner join v_allClient_list c on b.clientid = c.clientid
							  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
		where date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by date
		;

		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
		select a.date, 'TSA OPS Review' as category, 'Total Active PT && Non CBG && Insulin' as measure_name,
		count(distinct case when ps.clientid is null and d.medicine_drugs like '%INSU%' then b.clientid else null end) as measure
		from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
							  inner join v_allClient_list c on b.clientid = c.clientid
							  left join predictedcgmcycles ps on b.clientid = ps.clientid and a.date between ps.startdate and ps.enddate
							  left join bi_patient_reversal_state_gmi_x_date d on b.clientid = d.clientid and a.date = d.date
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
        and c.status not in ('registration')
		group by date
		;

		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
		select a.date, 'TSA OPS Review' as category, 'Total Active PT && Non CBG && Non-Insulin' as measure_name,
		count(distinct case when ps.clientid is null and (d.medicine_drugs not like '%INSU%' or d.medicine_drugs is null) then b.clientid else null end) as measure
		from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
							  inner join v_allClient_list c on b.clientid = c.clientid
							  left join predictedcgmcycles ps on b.clientid = ps.clientid and a.date between ps.startdate and ps.enddate
							  left join bi_patient_reversal_state_gmi_x_date d on b.clientid = d.clientid and a.date = d.date
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by date
		;

		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
		select a.date, 'TSA OPS Review' as category, 'Total Active PT w. CBG' as measure_name,
		count(distinct case when ps.clientid is not null then b.clientid else null end) as measure
		from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
							  inner join v_allClient_list c on b.clientid = c.clientid
							  left join predictedcgmcycles ps on b.clientid = ps.clientid and a.date between ps.startdate and ps.enddate                   
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by date
		;

		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
		select a.date, 'TSA OPS Review' as category, 'Pending Active Reading Uploaded' as measure_name, sum(case when a.date between b.status_start_date and b.status_end_date then 1 else 0 end) measure
		from v_date_to_date a
		left join
					(
						select distinct clientid, date(itz(dateadded)) dateadded
						from cgmfiles
					)  cgm on a.date = cgm.dateadded
					left join (select clientid, max(status_start_date) status_start_date,max(status_end_date) status_end_date from bi_patient_status where status = 'pending_active' group by clientid) b on cgm.clientid=b.clientid
					inner join v_allclient_list c on cgm.clientid =c.clientid
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by a.date;

		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
		select a.date, 'TSA OPS Review' as category, 'Total Reading Uploaded' as measure_name,
		count(distinct case when cgm.clientid is not null then cgm.clientid else null end) as measure
		from v_date_to_date a left join 
			(
				select clientid, date(itz(dateadded)) dateadded
				from cgmfiles
			)  cgm on a.date = cgm.dateadded
		inner join v_allclient_list c on cgm.clientid =c.clientid
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by a.date					  
		;

		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
		select a.date, 'TSA OPS Review' as category, 'New IMTs under Sensor_TSA ' as measure_name,
		count(distinct case when s.incidentid is not null then s.incidentid else null end) as measure
		from v_date_to_date a left join 
			( 	
				select distinct incidentid, clientid, date(itz(report_time)) report_time from bi_incident_mgmt
				where category ='sensors' and subtype_L1='sensor_tsa' 
			) s on ( a.date= s.report_time) 
							  inner join v_allClient_list c on s.clientid = c.clientid					
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by date;


		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
		select a.date, 'TSA OPS Review' as category, 'New IMTs under Sensor_TSA within 24 hours' as measure_name,
		count(distinct case when s.incidentid is not null and s.close_duration_hours <= 24 then s.incidentid else null end) as measure
		from v_date_to_date a left join 
			( 
				select distinct incidentid, clientid, date(itz(report_time)) report_time, close_duration_hours from bi_incident_mgmt
				where category ='sensors' and subtype_L1='sensor_tsa'  
			) s on ( a.date= s.report_time) 
							  inner join v_allClient_list c on s.clientid = c.clientid					
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by date;


-- build a table with a list of all twin store purchases
drop temporary table if exists tmp_all_services_purchased_twin_store;

create temporary table tmp_all_services_purchased_twin_store as 
select a.clientid, a.date, a.max_invoice_date, b.cf_service_purchased
from 
	(
		select clientid, date, max(invoice_date) as max_invoice_date -- we have to get the recent invoice available for each calendar date not just the max invoice available on the whole (otherwise members will be counted incorrectly). 
		from v_date_to_date a inner join  
		( SELECT d.clientid , a.customer_id, a.invoice_date
			FROM importedbilling.zoho_invoices a
			LEFT JOIN importedbilling.zoho_customers c ON a.customer_id = c.customer_id
			JOIN twins.v_allclient_list D ON D.clientId = c.cf_clientid_unformatted and d.plancode like '%goldpro%' 
			WHERE
			a.cf_service_purchased IS NOT NULL and a.status='paid' 
		) b on invoice_date <= date
		group by clientid, date
	 ) a
	join 
	(
		SELECT d.clientid , d.plancode AS PlanCode,a.cf_service_purchased ,a.invoice_date
			FROM importedbilling.zoho_invoices a
			LEFT JOIN importedbilling.zoho_customers c ON a.customer_id = c.customer_id
			JOIN twins.v_allclient_list D ON D.clientId = c.cf_clientid_unformatted and d.plancode like '%goldpro%' 
			WHERE
			a.cf_service_purchased IS NOT NULL and a.status='paid' 
	) b on a.clientid=b.clientid and a.max_invoice_date = b.invoice_date
;

create index idx_tmp_all_services_purchased_twin_store on tmp_all_services_purchased_twin_store(clientid asc, date asc, max_invoice_date, cf_service_purchased);
	
	-- GPP members who are in the program with more than 39 days of treatment    
		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
		select a.date, 'TSA OPS Review' as category, 'GPP Members >39 Days Not On CBG' as measure_name,
		-- count(distinct case when ps.clientid is null and c.plancode like '%goldpro%' and d.treatmentDays >39 and (d.medicine_drugs not like '%INSU%' or d.medicine_drugs is null) then b.clientid else null end) as measure,
		count(distinct case when ps.clientid is null and ps1.clientid is null and c.plancode like '%goldpro%' and d.treatmentDays >39 and (d.medicine_drugs not like '%INSU%' or d.medicine_drugs is null) then b.clientid else null end) as measure -- The member must not be in CBG and never was in CBG to that date as well
		from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'active'
							  inner join v_allClient_list c on b.clientid = c.clientid
							  left join predictedcgmcycles ps on b.clientid = ps.clientid and a.date between ps.startdate and ps.enddate
							  left join bi_patient_reversal_state_gmi_x_date d on b.clientid = d.clientid and a.date = d.date
							  left join predictedcgmcycles ps1 on  b.clientid = ps1.clientid and ps1.enddate < a.date
		where a.date >= date_sub(date(itz(now())), interval 14 day) and c.patientname not like '%obsolete%'
		and c.status not in ('registration')
		group by date
		;
		
	-- GPP ALL Purchases
		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
		SELECT  aa.date,'TSA OPS Review' as category,'GPP All Services' as Measure_Name, 
		count(distinct case when d.clientid is not null  then bb.clientid else null end)  as measure
		FROM
		twins.v_date_to_date aa 
		left join twins.bi_patient_status bb on aa.date between bb.status_start_date and bb.status_end_date and bb.status = 'active'
		inner join twins.v_allClient_list cc on bb.clientid = cc.clientid and  cc.plancode like '%goldpro%'
		left join tmp_all_services_purchased_twin_store d on bb.clientid = d.clientid and d.date = aa.date 
		where aa.date >= date_sub(date(itz(now())), interval 14 day) and cc.patientname not like '%obsolete%'
		and cc.status not in ('registration')
		group by aa.date
	;

	-- GPP Two Weeks purchases
		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
		SELECT  aa.date,'TSA OPS Review' as category,'GPP Two Weeks' as Measure_Name, 
		count(distinct case when d.clientid is not null and d.cf_service_purchased= 'TWO_WEEKS' then bb.clientid else null end)  as measure
		FROM
		twins.v_date_to_date aa 
		left join twins.bi_patient_status bb on aa.date between bb.status_start_date and bb.status_end_date and bb.status = 'active'
		inner join twins.v_allClient_list cc on bb.clientid = cc.clientid and  cc.plancode like '%goldpro%'
		left join tmp_all_services_purchased_twin_store d on bb.clientid = d.clientid and d.date = aa.date 
		where aa.date >= date_sub(date(itz(now())), interval 14 day) and cc.patientname not like '%obsolete%'
		and cc.status not in ('registration')
		group by aa.date
	;

	-- GPP Four Weeks purchases
		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
		SELECT aa.date,'TSA OPS Review' as category,'GPP Four Weeks' as Measure_Name, 
		count(distinct case when d.clientid is not null and d.cf_service_purchased= 'FOUR_WEEKS' then bb.clientid else null end)  as measure
		FROM
		twins.v_date_to_date aa 
		left join twins.bi_patient_status bb on aa.date between bb.status_start_date and bb.status_end_date and bb.status = 'active'
		inner join twins.v_allClient_list cc on bb.clientid = cc.clientid and  cc.plancode like '%goldpro%'
		left join tmp_all_services_purchased_twin_store d on bb.clientid = d.clientid and d.date = aa.date 
		where aa.date >= date_sub(date(itz(now())), interval 14 day) and cc.patientname not like '%obsolete%'
		and cc.status not in ('registration')
		group by aa.date
	 ;  
	  
	-- GPP Eight Weeks purchases
		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure) 
		SELECT aa.date,'TSA OPS Review' as category,'GPP Eight Weeks' as Measure_Name, 
		count(distinct case when d.clientid is not null and d.cf_service_purchased= 'EIGHT_WEEKS' then bb.clientid else null end)  as measure
		FROM
		twins.v_date_to_date aa 
		left join twins.bi_patient_status bb on aa.date between bb.status_start_date and bb.status_end_date and bb.status = 'active'
		inner join twins.v_allClient_list cc on bb.clientid = cc.clientid and  cc.plancode like '%goldpro%'
		left join tmp_all_services_purchased_twin_store d on bb.clientid = d.clientid and d.date = aa.date 
		where aa.date >= date_sub(date(itz(now())), interval 14 day) and cc.patientname not like '%obsolete%'
		and cc.status not in ('registration')
		group by aa.date
	;    

	drop temporary table if exists tmp_all_services_purchased_twin_store
	;


		drop temporary table if exists tmp_daily_TSA_Calculations; 

		
		create temporary table tmp_daily_TSA_Calculations 
		(
		report_date date not null,
		category varchar(50) not null,
		measure_name varchar(100) not null,
		measure decimal(10,1) null
		); 

		insert into tmp_daily_TSA_Calculations
		select * from tmp_daily_onboarding_sla where category = 'TSA OPS Review';

		insert into tmp_daily_onboarding_sla (report_date, category, measure_name, measure)
        select date, 'TSA OPS Review' as category, 'Total Visit needed' as measure_name, 
		(max(case when tmp.measure_name= 'Sensor Activations' then tmp.measure else null end)) + 
		(max(case when tmp.measure_name= 'Total Active PT && Non CBG && Insulin' then tmp.measure else null end)) + 
		(max(case when tmp.measure_name= 'Total Active PT && Non CBG && Non-Insulin' then tmp.measure else null end)*(3/7)) - 
		(max(case when tmp.measure_name= 'Pending Active Reading Uploaded' then tmp.measure else null end)) -
        (max(case when tmp.measure_name='GPP Members >39 Days Not On CBG' then tmp.measure else null end)) +
        ((
           max(case when tmp.measure_name='GPP Two Weeks' then tmp.measure else null end) +
           max(case when tmp.measure_name='GPP Four Weeks' then tmp.measure else null end) +
           max(case when tmp.measure_name='GPP Eight Weeks' then tmp.measure else null end) 
        ) * (3/7)) as measure 
		from v_date_to_date a left join tmp_daily_TSA_Calculations tmp on (a.date=tmp.report_date)
		where date >= date_sub(date(itz(now())), interval 14 day)
		group by date ;			  

		drop temporary table if exists tmp_daily_TSA_Calculations; 



create index idx_tmp_daily_onboarding_sla on tmp_daily_onboarding_sla (report_date asc, category, measure_name, measure); 


insert into bi_daily_onboarding_sla 
select report_date, category, measure_name, measure
from tmp_daily_onboarding_sla a 
where not exists ( select 1 from bi_daily_onboarding_sla b where a.report_date = b.report_date and a.category = b.category and a.measure_name = b.measure_name)
;


update bi_daily_onboarding_sla a 
inner join tmp_daily_onboarding_sla b on (a.report_date = b.report_date and a.category = b.category and a.measure_name = b.measure_name)
set a.measure = b.measure
;

drop temporary table if exists tmp_daily_onboarding_sla
;

commit
;


			drop temporary table if exists tmp_daily_onboarding_sla_poc; 

			create temporary table tmp_daily_onboarding_sla_poc 
			(
			report_date date not null,
			POC varchar(50) null,
			category varchar(50) not null,
			measure_name varchar(100) not null,
			measure decimal(10,1) null
			); 

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Pending Active' as measure_name, count(distinct b.clientid) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and (b.status = 'pending_active' or b.status = 'on_hold')
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc; 

			
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Pending Active E5-E8' as measure_name, count(distinct b.clientid) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
			where date >= date_sub(date(itz(now())), interval 14 day)
			and datediff(a.date, b.status_start_date) between 6 and 8	
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Pending Active > E8' as measure_name, count(distinct b.clientid) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
			where date >= date_sub(date(itz(now())), interval 14 day)
			and datediff(a.date, b.status_start_date) > 8	
			group by date, b.poc;
			
            
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
            select date, b.poc, 'Overall' as category, 'Pending Active < E5' as measure_name, count(distinct b.clientid) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
			where date >= date_sub(date(itz(now())), interval 14 day)
			and datediff(a.date, b.status_start_date) < 5
			group by date , b.poc
			;

			
			
			
			


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc,  'Overall' as category, 'New Enrollments' as measure_name, count(distinct case when def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a left join bi_patient_status b on a.date = b.status_start_date and b.status = 'pending_active'
								  inner join v_allClient_list c on b.clientid = c.clientid 
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where c.status not in ('registration')
			and c.patientname not like '%obsolete%'
			and date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			
			
			
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Deffered' as measure_name, count(distinct case when def.clientid is not null then b.clientid else null end) as measure
			from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason, labels from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 10 day)
			group by date, b.poc 
			;
            
			
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Disenrolled' as measure_name, count(distinct case when d.clientid is not null and def.clientid is null and ex.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a left join bi_patient_status b on a.date = b.status_start_date and b.status = 'discharged'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
									left join 
									(		
											select distinct clientid from bi_patient_status a 
											where 
											status = 'pending_active' 
											and not exists (select 1 from bi_patient_status b where a.clientid = b.clientid and b.status = 'active')
											and exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'discharged' and c.status_start_date >= a.status_end_date)
									) d on b.clientid = d.clientid
                                    left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
								left join dim_client ex on b.clientid = ex.clientid and ex.is_medical_exclusion = 'Yes'
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Activations' as measure_name, 
            count(distinct case when d.clientid is not null and dis.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a left join (
										select clientid, poc, max(status_start_date) as status_start_date from bi_patient_status 
										where status = 'active'
										group by clientid 
                                ) b on a.date = b.status_start_date 
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
									left join (
											
											select a.clientid from 
											(
												select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a 
												where 
												status = 'pending_active' 
												group by clientid 
											) a
											where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
									) d on b.clientid = d.clientid 
                        left join
                        (
									select a.clientid, a.status_end_date from
									(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status in ( 'discharged', 'inactive')
									group by clientid
                                    ) a
                                   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
                        ) dis on b.clientid = dis.clientid
			where date >= date_sub(date(itz(now())), interval 20 day)
			group by date , b.poc
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc , 'Overall' as category, 'Activations within SLA' as measure_name, 
            count(distinct case when d.clientid is not null and dis.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a left join (
										select clientid, poc, max(status_start_date) as status_start_date from bi_patient_status 
										where status = 'active'
										group by clientid 
                                ) b on a.date = b.status_start_date 
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
									left join (
											
												select a.clientid from
												(
													select distinct clientid, max(status_end_date) as status_end_date, max(status_start_date) status_start_date from bi_patient_status a
													where
													status = 'pending_active'
													group by clientid
												) a
												where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
												and datediff(a.status_end_date, a.status_start_date) <= 9
									) d on b.clientid = d.clientid 
									left join
									( 
												select a.clientid, a.status_end_date from
												(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
												where
												status in ( 'discharged', 'inactive')
												group by clientid
												) a
											   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
									) dis on b.clientid = dis.clientid
			where date >= date_sub(date(itz(now())), interval 20 day)
			group by date, b.poc 
			;
            
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Activations within SLA < E5' as measure_name, 
			count(distinct case when d.clientid is not null and dis.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a left join (
													select clientid, poc, max(status_start_date) as status_start_date from bi_patient_status 
													where status = 'active'
													group by clientid 
											) b on a.date = b.status_start_date 
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join (
											
												select a.clientid from
												(
													select distinct clientid, max(status_end_date) as status_end_date, max(status_start_date) status_start_date from bi_patient_status a
													where
													status = 'pending_active'
													group by clientid
												) a
												where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
												and datediff(a.status_end_date, a.status_start_date) < 5
									) d on b.clientid = d.clientid 
									left join
									(
												select a.clientid, a.status_end_date from
												(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
												where
												status in ( 'discharged', 'inactive')
												group by clientid
												) a
											   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
									) dis on b.clientid = dis.clientid
			where date >= date_sub(date(itz(now())), interval 20 day)
			group by date, b.poc; 

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Activations - Deferred Cases' as measure_name, 
			count(distinct case when d.clientid is not null and dis.clientid is null and (def.clientid is not null or oh.clientid is not null) then b.clientid else null end) as measure
			from v_date_to_date a left join (
													select clientid, poc, max(status_start_date) as status_start_date from bi_patient_status 
													where status = 'active'
													group by clientid 
											) b on a.date = b.status_start_date 
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
								  left join (
											
											select a.clientid from
											(
												select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
												where
												status = 'pending_active'
												group by clientid
											) a
											where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
									) d on b.clientid = d.clientid
									left join
									(
												select a.clientid, a.status_end_date from
												(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
												where
												status in ( 'discharged', 'inactive')
												group by clientid
												) a
											   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
									) dis on b.clientid = dis.clientid
									left join (
													select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
													where category = 'Approval' and custom_subtype = 'SPL'
											 ) def on b.clientid = def.clientid
									left join (select distinct clientid from bi_patient_status where status = 'on_hold') oh on b.clientid = oh.clientid
			where date >= date_sub(date(itz(now())), interval 20 day)
			group by date, b.poc
			;
            
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Overall' as category, 'Escalation Open' as measure_name, count(distinct case when def.clientid is null then d.incidentId else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join (
												select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date 
												from bi_incident_mgmt where category not in ('Approval')
											) d on b.clientid = d.clientid and a.date >= report_date and a.date < ifnull(resolved_date,date(itz(now())))
								left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;
            

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc , 'Overall' as category, 'Escalation Closed' as measure_name, count(distinct case when def.clientid is null then d.incidentId else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join (
												select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date 
												from bi_incident_mgmt where category not in ('Approval')
											) d on b.clientid = d.clientid and a.date = d.resolved_date
								   left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date,b.poc, 'Overall' as category, 'Escalation closed AVG time (Hrs)' as measure_name, floor(avg(close_duration_hours)) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join (
												select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date, close_duration_hours
												from bi_incident_mgmt where category not in ('Approval') and status = 'RESOLVED'
											) d on b.clientid = d.clientid and a.date = d.resolved_date
								   left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day) and def.clientid is null
			group by date ,b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc , 'Overall' as category, '% Escalation Closed SLA' as measure_name, cast(count(distinct case when close_duration_hours <= 8 and def.clientid is null then d.incidentid else null end)/count(distinct d.clientid) as decimal(10,2)) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join (
												select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date, close_duration_hours
												from bi_incident_mgmt where category not in ('Approval') and status = 'RESOLVED'
											) d on b.clientid = d.clientid and a.date = d.resolved_date
								   left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day) 
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Blood Draw Schedule' as category, 'Schedule Backlog' as measure_name, 
			count(distinct case when (d.bloodWork_scheduled_status is null or d.bloodWork_scheduled_status not in ('SCHEDULED','COMPLETED')) and d.first_Investigation_upload_date is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc , 'Blood Draw Schedule' as category, 'Schedule Backlog >2 Days' as measure_name, 
			count(distinct case when (d.bloodWork_scheduled_status is null or d.bloodWork_scheduled_status not in ('SCHEDULED','COMPLETED')) and d.first_Investigation_upload_date is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where datediff(a.date, b.status_start_date) > 2
			and date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Blood Draw Schedule' as category, 'Blood Draw Schedule Completed' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status = 'SCHEDULED' and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid and a.date = date(d.Bloodwork_Schedule_created_Date)
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Blood Draw Schedule' as category, 'Blood Draw Schedule Completed in SLA' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status in ('PENDING','CANCELLED') and date(d.bloodwork_scheduled_date) = b.status_start_date and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date,b.poc , 'Blood Draw Schedule' as category, 'Blood Draw Schedule beyond E2 SLA' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status not in ('PENDING','CANCELLED') and date(d.bloodwork_scheduled_date) = date_add(b.status_start_date, interval 2 day) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                   left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;
            
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, null as POC, 'Blood Draw Schedule' as category, 'Schedule IMT Open' as measure_name, null as measure
			from v_date_to_date a 
			where date >= date_sub(date(itz(now())), interval 14 day)
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc, 'Blood Draw' as category, 'Scheduled & Blood Draw Pending' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status = 'SCHEDULED' and first_Investigation_upload_date is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date,b.poc, 'Blood Draw' as category, 'Blood Draw Completed' as measure_name, 
			count(distinct case when (d.bloodWork_scheduled_status = 'COMPLETED' or (d.bloodWork_scheduled_status = 'SCHEDULED' and first_Investigation_upload_date is not null)) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, null as POC, 'Blood Draw' as category, 'Blood Draw Completed in SLA' as measure_name, null as measure
			from v_date_to_date a 
			where date >= date_sub(date(itz(now())), interval 14 day)
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc, 'Blood Draw' as category, 'Patient Rescheduled Blood Draw' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status in ('PENDING','CANCELLED') and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date,b.poc, 'Blood Draw' as category, 'Blood Draw IMT Open' as measure_name, count(distinct case when def.clientid is null then d.incidentId else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join (select distinct incidentId, clientid, date(report_time) as report_date, date(resolved_time) as resolved_date from bi_incident_mgmt where category in ('BLOOD_WORK')) d on b.clientid = d.clientid and a.date between report_date and ifnull(date_sub(resolved_date, interval 1 day), date(now()))
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Vitals & Condition' as category, 'V&C Backlog' as measure_name, 
			count(distinct case when d.vitals_conds_allergies_meds_addInfor_captured is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Vitals & Condition' as category, 'V&C Completed - Total' as measure_name, 
			count(distinct case when d.vitals_conds_allergies_meds_addInfor_captured is not null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Vitals & Condition' as category, 'V&C Completed - Daily' as measure_name, 
			count(distinct case when d.vitals_conds_allergies_meds_addInfor_captured is not null and date(d.vitals_conds_allergies_meds_addInfor_captured) = a.date and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc , 'Vitals & Condition' as category, 'V&C Completed in SLA' as measure_name, 
			count(distinct case when date(d.vitals_conds_allergies_meds_addInfor_captured) <= date_add(b.status_start_date, interval 4 day) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
								  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Vitals & Condition' as category, 'V&C Patient Rescheduled' as measure_name, 
			count(distinct case when date(d.vitals_conds_allergies_meds_addInfor_captured) > date_add(b.status_start_date, interval 4 day) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date,b.poc, 'Blood Report Upload' as category, 'Blood Report Backlog' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' and d.first_Investigation_upload_date is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;
            
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Blood Report Upload' as category, 'Blood Report Completed' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' and d.first_Investigation_upload_date is not null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date,b.poc, 'Blood Report Upload' as category, 'Blood Report Completed in SLA' as measure_name, 
			count(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' and d.first_Investigation_upload_date is not null and date(d.first_Investigation_upload_date) <= date_add(b.status_start_date, interval 4 day) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;



			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date,b.poc, 'Blood Report Upload' as category, 'Blood Report Avg Time (Hrs)' as measure_name, 
			floor(avg(distinct case when d.bloodWork_scheduled_status = 'COMPLETED' 
							and d.first_Investigation_upload_date is not null 
							and d.firstBloodWorkDateITZ is not null and def.clientid is null
							then timestampdiff(hour, d.firstBloodWorkDateITZ, d.first_Investigation_upload_date) else null end)) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, null as POC, 'Blood Report Upload' as category, 'Blood Report IMT Open' as measure_name, null as measure
			from v_date_to_date a 
			where date >= date_sub(date(itz(now())), interval 14 day)
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc, 'Doctor Consult' as category, 'Doctor Consult Backlog' as measure_name, 
			
			count(distinct case when d.first_Investigation_upload_date is not null and first_bmi is not null and (First_Medicine_Published_Date is null) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where  date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, null as POC, 'Doctor Consult' as category, 'Doctors Active' as measure_name, count(distinct b.id) as measure
			from v_date_to_date a inner join doctors b on b.test is false and date(b.dateadded) <= a.date
			where  date >= date_sub(date(itz(now())), interval 14 day)
			group by date 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Doctor Consult' as category, 'Doctors Consulted' as measure_name, 
			count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED','SCHEDULED') and def.clientid is null then c.doctorname else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Doctor Consult' as category, 'Consult Scheduled' as measure_name, 
			count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('SCHEDULED', 'COMPLETED') and def.clientid is null then clientAppointmentId else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date,b.poc, 'Doctor Consult' as category, 'Consult Completed' as measure_name, 
			count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED') and def.clientid is null then clientAppointmentId else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date,b.poc, 'Doctor Consult' as category, 'Consult in SLA (Consult + Med Publish)' as measure_name, 
			count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED') and First_Medicine_Published_Date is not null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;

						


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc, 'Doctor Consult' as category, 'Pending Meds Published Backlog' as measure_name, 
			count(distinct case when doctorAppointment_status = 'COMPLETED' and First_Medicine_Published_Date is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc , 'Doctor Consult' as category, 'Consult completed with manual cohort' as measure_name, 
			count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('COMPLETED') and First_Medicine_Published_Date_but_Manual is not null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
								  left join (
															select a.clientid, a.First_Medicine_Published_Date_but_Manual
															from
															(
															Select clientid, min(itz(dateadded)) as First_Medicine_Published_Date_but_Manual from medicationhistories group by clientid
															) a inner join medicationhistories b on a.clientid = b.clientid and a.First_Medicine_Published_Date_but_Manual = itz(dateadded) and changeType = 'MANUAL'
											) e on b.clientid = e.clientid
									left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;


								
								
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)                      
			select date, b.poc, 'Doctor Consult' as category, 'Doctor Rescheduled' as measure_name, 
			count(distinct case when b1.type = 'DOCTOR_APPOINTMENT' and b1.status in ('CANCELLED') and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, '' as POC, 'Doctor Consult' as category, 'Member Rescheduled' as measure_name, null as measure
			from v_date_to_date a 
			where date >= date_sub(date(itz(now())), interval 14 day)
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, '' as POC, 'Doctor Consult' as category, 'Doctor IMT Open' as measure_name, null as measure
			from v_date_to_date a 
			where date >= date_sub(date(itz(now())), interval 14 day)
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Sensor Activation' as category, 'Sensor Activation Backlog' as measure_name, 
			count(distinct case when First_Medicine_Published_Date is not null and d.sensorActivation_schedule_date is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc 
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, b.poc, 'Sensor Activation' as category, 'Sensor Activation Scheduled' as measure_name, 
			count(distinct case when First_Medicine_Published_Date is not null and sensorActivation_schedule_status in ('SCHEDULED','COMPLETED') and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc, 'Sensor Activation' as category, 'Sensor Activation Completed' as measure_name, 
			count(distinct case when d.sensorActivation_schedule_status in ('SCHEDULED', 'COMPLETED') and First_Medicine_Published_Date is not null and date(First_Fitbit_Sync_Date) is not null and def.clientid is null then b.clientid else null end) as measure 
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and (b.status = 'pending_active' or b.status = 'active') 
								  
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date, b.poc
			;

						

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date, b.poc, 'Sensor Activation' as category, 'Sensor Activation Completed in SLA' as measure_name, 
			count(distinct case when d.sensorActivation_schedule_status in ('SCHEDULED', 'COMPLETED')
							
								and date(sensorActivation_schedule_date) <= date_add(b.status_start_date, interval 8 day) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and (b.status = 'pending_active' or b.status = 'active')
								  
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date , b.poc
			;
						



			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)  
			select date,b.poc, 'Sensor Activation' as category, 'Sensor Activation Rescheduled' as measure_name, 
			count(distinct case when b1.type = 'HOME_VISIT' and b1.status in ('PENDING') and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join clientappointments b1 on b.clientid = b1.clientid and a.date = date(itz(eventtime))
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;


						

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date,b.poc, 'Treatment Activation' as category, 'Treatment Activation Backlog' as measure_name, 
			count(distinct case when First_Medicine_Published_Date is not null and First_Fitbit_Sync_Date is not null and date(First_Medicine_Published_Date) <= date and date(First_Fitbit_Sync_Date) <= date and c.status = 'pending_active' and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, doctorname, dischargeReason, status from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where  date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;
            
			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date,b.poc, 'Treatment Activation' as category, 'Treatment Activation Completed' as measure_name, 
			count(distinct case when First_Medicine_Published_Date is not null and First_Fitbit_Sync_Date is not null and e.clientid is not null and dis.clientid is null and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date = b.status_start_date and b.status = 'active'
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
								  left join (
											
											select a.clientid from 
											(
												select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a 
												where 
												status = 'pending_active' 
												group by clientid 
											) a
											where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
								  ) e on b.clientid = e.clientid 
								  left join
									(
												select a.clientid, a.status_end_date from
												(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
												where
												status in ( 'discharged', 'inactive')
												group by clientid
												) a
											   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date < a.status_end_date)
									) dis on b.clientid = dis.clientid
                                    left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where  date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;
            


			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date,b.poc, 'Treatment Activation' as category, 'Treatment Activation Completed within SLA' as measure_name, 
			count(distinct case when First_Medicine_Published_Date is not null and First_Fitbit_Sync_Date is not null and e.active_Status_start_Date <= date_add(b.status_start_date, interval 8 day) and def.clientid is null then b.clientid else null end) as measure
			from v_date_to_date a inner join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
								  inner join (select clientid, doctorname, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
								  left join bi_patient_initial_setup_x_dates d on b.clientid = d.clientid 
								  left join (select clientid, max(status_start_date) as active_status_start_date from bi_patient_status where status = 'active' group by clientid) e on b.clientid = e.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                   ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
			where date >= date_sub(date(itz(now())), interval 14 day)
			group by date ,b.poc
			;

			insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
			select date, '' as POC, 'Treatment Activation' as category, 'Treatment Activation IMT Open' as measure_name, null as measure
			from v_date_to_date a 
			where date >= date_sub(date(itz(now())), interval 14 day)
			;




insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
select a.date, b.poc, 'POC_Score' as category, 'Open_IMTS_BLOOD_WORK_SENSOR_TWIN_PRODUCT' as measure_name, count(distinct case when op.clientid is not null then op.incidentid else null end) as measure
from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
inner join (select clientid, dischargeReason, labels from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category in ('BLOOD_WORK', 'SENSORS','TWINS_PRODUCT')  
                                 ) op on b.clientid = op.clientid and a.date between op.openDate and op.resolvedDate			
where date >= date_sub(date(itz(now())), interval 21 day)
group by a.date, b.poc;


insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
select a.date, b.poc,'POC_Score' as category, 'Disenrollment_risk_cases' as measure_name ,  count(distinct case when dis.clientid is not null then b.clientid else null end) as measure
from v_date_to_date a left join bi_patient_status b on a.date between b.status_start_date and b.status_end_date and b.status = 'pending_active'
inner join (select clientid, dischargeReason, labels from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'DISENROLLMENT'  
                                 ) dis on b.clientid = dis.clientid and a.date between dis.openDate and dis.resolvedDate			
where date >= date_sub(date(itz(now())), interval 21 day)
group by a.date, b.poc ;

insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
select a.date, poc, 'POC_Score' as category, 'PA-7day Before' as measure_name,   count(distinct clientid) as totalPA
from
(select date, date_sub(date, interval 7 day) as sprintFirstDay from v_date_to_date) a													
			left join (							
								select distinct date, poc, case when def.clientid is null then b.clientid else null end clientid
								from v_date_to_date a left join 							
								(		
										select distinct clientid, poc, status, status_start_date, status_end_date from
										bi_patient_status
										where status in ('pending_active')
								) b on a.date between b.status_start_date and b.status_end_date
                                inner join (select clientid, dischargeReason, labels from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
                                  left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and a.date between def.openDate and def.resolvedDate
								where date >= date_sub(date(itz(now())), interval 30 day) and date <= date(itz(now()))
					  ) act on act.date = a.sprintFirstDay	
			where a.date >= date_sub(date(itz(now())), interval 21 day) and a.date <= date(itz(now()))     							
		group by a.date, poc;
        
insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure) 
select a.date, b.poc, 'POC_Score' as category, 'Discharged_7d' as measure_name, count(distinct case when pa_patient is not null and def_patient is null and ex_patient is null then dis_patient else null end) as measure
			from (select date, date_sub(date, interval 7 day) as sprintFirstDay from v_date_to_date) a left join
            (select b.poc, b.status_start_date, d.clientid as pa_patient, def.clientid as def_patient, ex.clientid as ex_patient, b.clientid as dis_patient
            from 
             bi_patient_status b  
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid  and b.status = 'discharged'
									left join 
									(		
											select distinct clientid from bi_patient_status a 
											where 
											status = 'pending_active' 
											and not exists (select 1 from bi_patient_status b where a.clientid = b.clientid and b.status = 'active')
											and exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'discharged' and c.status_start_date >= a.status_end_date)
									) d on b.clientid = d.clientid
                                    left join (
										select distinct incidentid, clientid, date(itz(report_time)) as openDate, ifnull(date(itz(resolved_time)), date(itz(now()))) as resolvedDate from bi_incident_mgmt
										where category = 'Approval' and custom_subtype = 'SPL'
                                 ) def on b.clientid = def.clientid and b.status_start_date between def.openDate and def.resolvedDate
								left join dim_client ex on b.clientid = ex.clientid and ex.is_medical_exclusion = 'Yes'
                                 where b.status_start_date >= date_sub(date(itz(now())), interval 30 day)) b on (b.status_start_date between date_add(a.sprintFirstDay, interval 1 day) and a.date)
			where date >= date_sub(date(itz(now())), interval 21 day)
			group by date, b.poc ;
            
insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)
select a.date, b.poc , 'POC_Score' as category, 'SLA_D0+5' as measure_name, 
            count(distinct case when d_clientid is not null and dis_clientid is null then act_patient else null end) as measure
			from (select date, date_sub(date, interval 7 day) as sprintFirstDay from v_date_to_date) a left join 
            ( select b.clientid as act_patient, b.poc, b.status_start_date, d.clientid as d_clientid, dis.clientid as dis_clientid
									    from 
										(select clientid, poc, max(status_start_date) as status_start_date from bi_patient_status 
										where status = 'active'
										group by clientid 
                                ) b 
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid 
									left join (
											
												select a.clientid from
												(
													select distinct clientid, max(status_end_date) as status_end_date, max(status_start_date) status_start_date from bi_patient_status a
													where
													status = 'pending_active'
													group by clientid
												) a
												where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
												and datediff(a.status_end_date, a.status_start_date) <= 5
									) d on b.clientid = d.clientid 
									left join
									( 
												select a.clientid, a.status_end_date from
												(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
												where
												status in ( 'discharged', 'inactive')
												group by clientid
												) a
											   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
									) dis on b.clientid = dis.clientid
                                    where b.status_start_date >= date_sub(date(itz(now())), interval 30 day)) b on (b.status_start_date between date_add(a.sprintFirstDay, interval 1 day) and a.date)
			where date >= date_sub(date(itz(now())), interval 21 day)
			group by date, b.poc 
			;
            
insert into tmp_daily_onboarding_sla_poc (report_date, POC, category, measure_name, measure)            
 select date, b.poc, 'POC_Score' as category, 'Activations_7d' as measure_name,
            count(distinct case when pen_clientid is not null and dis_clientid is null then act_patient else null end) as measure
			from (select date, date_sub(date, interval 7 day) as sprintFirstDay from v_date_to_date) a left join
            ( select b.clientid as act_patient, b.poc, b.status_start_date, d.clientid as pen_clientid, dis.clientid as dis_clientid
              from (
										select clientid, poc, max(status_start_date) as status_start_date from bi_patient_status
										where status = 'active'
										group by clientid
                                ) b
								  inner join (select clientid, dischargeReason from v_allClient_list where status not in ('registration') and patientname not like '%obsolete%') c on b.clientid = c.clientid
									left join (
											
											select a.clientid from
											(
												select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
												where
												status = 'pending_active'
												group by clientid
											) a
											where exists (select 1 from bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date >= a.status_end_date)
									) d on b.clientid = d.clientid
                        left join
                        (
									select a.clientid, a.status_end_date from
									(select distinct clientid, max(status_end_date) as status_end_date from bi_patient_status a
									where
									status in ( 'discharged', 'inactive')
									group by clientid
                                    ) a
                                   where exists (select 1 from  bi_patient_status c where a.clientid = c.clientid and c.status = 'active' and c.status_start_date > a.status_end_date)
                        ) dis on b.clientid = dis.clientid
			where b.status_start_date >= date_sub(date(itz(now())), interval 30 day)) b on (b.status_start_date between date_add(a.sprintFirstDay, interval 1 day) and a.date)
			where date >= date_sub(date(itz(now())), interval 21 day)
			group by date , b.poc
			;            
        




			create index idx_tmp_daily_onboarding_sla_poc on tmp_daily_onboarding_sla_poc(report_date asc, poc, category, measure_name, measure)
			;

			insert into bi_daily_onboarding_sla_x_poc (report_date, poc, category, measure_name, measure)
			select report_date, ifnull(poc,'') as poc, category, measure_name, measure 
			from tmp_daily_onboarding_sla_poc a 
			where not exists 
			(select 1 from bi_daily_onboarding_sla_x_poc b where a.report_date = b.report_date and ifnull(a.poc,'') = ifnull(b.poc,'') and a.category = b.category and a.measure_name = b.measure_name)
			;

			update bi_daily_onboarding_sla_x_poc a inner join tmp_daily_onboarding_sla_poc b on 
			(a.report_date = b.report_date and ifnull(a.poc,'') = ifnull(b.poc,'') and a.category = b.category and a.measure_name = b.measure_name)
			set a.measure = b.measure
			;

			drop temporary table if exists tmp_daily_onboarding_sla_poc
			; 


commit
;


update dailyProcessLog
set updateDate = now()
where processName = 'sp_load_bi_daily_onboarding_sla'
;

END